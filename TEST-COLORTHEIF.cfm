<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
<style>
   .clearfix { display: inline-block; }
	* html .clearfix { height: 1%; }
	.clearfix { display: block; }
</style>
<script language="javascript" src="JS/color-thief.min.js"></script>
</head>

<body>

   <img src="images/sampleImage.jpg" id="myimg">
   
    <div id="mydiv" style="width: 200px; height: 200px"></div>
	
    <div style="width: 800px" class="clearfix">
    	<div id="pal0" style="width: 40px; height: 40px; float: left"></div>
    	<div id="pal1" style="width: 40px; height: 40px; float: left"></div>
    	<div id="pal2" style="width: 40px; height: 40px; float: left"></div>
    	<div id="pal3" style="width: 40px; height: 40px; float: left"></div>
    	<div id="pal4" style="width: 40px; height: 40px; float: left"></div>
    	<div id="pal5" style="width: 40px; height: 40px; float: left"></div>
    	<div id="pal6" style="width: 40px; height: 40px; float: left"></div>
    	<div id="pal7" style="width: 40px; height: 40px; float: left"></div>
    </div>

    <script>
		var sourceImage = document.getElementById("myimg");
		var colorThief = new ColorThief();
		var color = colorThief.getColor(sourceImage);
		console.log(color);
		document.getElementById("mydiv").style.backgroundColor = "rgb(" + color + ")";
		var colorPallette = colorThief.getPalette(sourceImage, 8);
		console.log(colorPallette);
		
		colorPallette.forEach(function(theColor, i) {
			document.getElementById("pal"+i).style.backgroundColor = "rgb(" + theColor + ")";
		})
		
    </script>
    
    
</body>
</html>