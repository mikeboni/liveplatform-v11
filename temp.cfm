
<style type="text/css">
	.bodytext {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14px;
		color: #333;
		line-height: 18px
	}
	.subheading {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 16px;
		color: #333;
		line-height: 18px;
	}
	
	.heading {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 22px;
		color: #333;
	}
	.state {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #FFF;
	background-color: #CD2234;
	padding-top: 8px;
	padding-right: 10px;
	padding-bottom: 5px;
	padding-left: 10px;
	text-transform: uppercase;
	float:left;
	margin-top:10px
	}
	.fieldName {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	float:left;
	width:70px; 
	text-align:right; 
	}
	.fieldContent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	float:left;
	width:100px; 
	text-align:left; 
	width:230px;
	padding-left:4px;
	}
	
	.barHeading {
	background:#666; 
	float:left; 
	height:42px; 
	width:812px; 
	line-height: 40px; 
	padding-left:5px;
	color:#FFF
	}
	
	.barSmallHeading {
	font-size: 16px;
	color:#FFF; 
	padding-left:5px
	}
	
	.noteText {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 11px;
		color: #333;
		line-height: 18px;
		text-align:center;
	}
	
	.barTableHead {
		font-size: 14px;
		color: #666;
		line-height: 18px;
		text-align:left;
		text-transform:uppercase
	}
	
	ul li{
		margin-bottom:10px;
	}
	
	
</style>


    

<cfsavecontent variable = "formHeader"> 
	<cfoutput>
    <div style="width:400px; float:left;">
                
      <div style="width:70px; height:70; padding-right:10px; float:left">
          <img src="../../eladcanada/images/elad-icon.png" alt="" name="" width="70" height="70" border="1" />
      </div>
              
      <div class="bodytext" style="width:310px; height:70px; float:left; text-transform:capitalize"><b>Mike<!--- #user.name# ---></b><br />
            2355 Sheppard Avenue, Suite 1700<br />
            Toronto, Ontario Canada M2N-6P6<br />
            <div>
            <span style="float:left; width:130px">(416) 223-4403</span>
            <span style="float:left; width:130px">(416) 223-4403</span>
            </div>
      </div>
     
    </div>
        
    <div class="subheading" style="width:310px; float:right;">
    
      <div style="height:70px;text-align:right">
            <b>JAN 12, 2016</b><br />
            N°00002
       </div>
       
    </div>
	</cfoutput>
</cfsavecontent>

<cfsavecontent variable = "custInfo"> 
	<cfoutput>
    <div style="width:400px; float:left; margin-top:30px">
                
      <div style="width:390px; float:left; padding:5px; height:95px" class="heading">
          <b>The Colors of Emerald City</b><br />
          Building 1 - Suite 1000, Model A<br />
          <div class="state">pending </div>
     </div>
     
    </div>

    <div class="subheading" style="width:315px; height:95px; float:right; border:thin; border-style:solid; border-color:##999; padding:8px 0px 0px 8px; margin-top:30px">
        <span class="fieldName"><b>Name:</b></span><span class="fieldContent">Name<!--- #cust.name# ---></span>
        <span class="fieldName"><b>Address:</b></span><span class="fieldContent">Address<!--- #cust.company_address# ---><br />
        </span>
        
        <span class="fieldName"><b>E-mail:</b></span><span class="fieldContent">someone@somone.com<!--- #cust.email# ---></span>
        <span class="fieldName"><b>Phone:</b></span><span class="fieldContent">905-303-2320<!--- #cust.companyphone# ---></span>
   </div>
	</cfoutput>
</cfsavecontent>   




<cfsavecontent variable = "custOptions"> 
	<cfoutput>
    <div style="float:left; width:816px; margin-top:32px">
      
          <div class="barHeading heading">
            <!--- #theSpace.title# --->Kitchen
          </div>
          
          <div style="width:400px; height:268px; margin-top:10px; float:left; border:thin; border-style:solid; border-color:##333">
            <cfimage source="#finalImage#" action="resize" width="400" height="268" name="theRoomImg"> 
            <cfimage source="#theRoomImg#" action="writeToBrowser">
          </div>
          
          <div style="width:400px; height:268px; margin-top:10px; float:right" class="noteText">
          
          <div class="barTableHead" style="text-align:left"><b>Options</b></div>
            
            
            <cfloop collection="#custSelection[theRoom]#" item="theOption">
                <cfoutput>
                <cfset theMaterial = custSelection[theRoom][theOption]>
            
                 <cfinvoke component="CFC.Configurator" method="getOptions" returnvariable="options">
                    <cfinvokeargument name="optionID" value="#theOption#"/>
                </cfinvoke>  
            
                <cfinvoke component="CFC.Configurator" method="getMaterials" returnvariable="material">
                    <cfinvokeargument name="optionID" value="#theOption#"/>
                    <cfinvokeargument name="assetID" value="#theMaterial#"/>
                </cfinvoke>
                
                <cfinvoke component="CFC.Modules" method="getContentAsset" returnvariable="asset">
                    <cfinvokeargument name="assetID" value="#material.asset_id#"/>
                </cfinvoke>
                
                <cfloop collection="#asset#" item="theKey"></cfloop>
                        
                <cfset theMaterial = asset[theKey].thumb.xdpi.url>
                <cfset theImage = asset[theKey].image>
                
                <!--- Material Item --->
                <div style="float:left; padding-right:3px; margin-top:10px">
                    <img src="#theMaterial#" alt="" name="" width="94" height="67" border="1" /><br />
                    #options.title#
                </div>
                
                </cfoutput>
            </cfloop>
    
          </div>
          
          <div style="float:left; width:816px;">
          
          <!--- <div class="barSmallHeading bodytext">
            Standard Options 
          </div> --->
          
          <!--- Standard --->
          <div style="padding-top:20px">
          <table width="100%" border="0" cellpadding="4" cellspacing="1" class="bodytext" bgcolor="##333">
          <tr>
            <td height="32" colspan="4" bgcolor="##888" class="barSmallHeading">Standard Options</td>
            </tr>
          <tr class="barTableHead">
            <td width="200" height="32" bgcolor="##DDD" class="barTableHead">OPTION</td>
            <td width="45" align="center" bgcolor="##DDD">CODE</td>
            <td width="200" bgcolor="##DDD">MATERIAL</td>
            <td bgcolor="##DDD">DESCRIPTION</td>
          </tr>
          <tr class="bodytext">
            <td height="32" valign="top" bgcolor="##FFF">Cabinets</td>
            <td align="center" valign="top" bgcolor="##FFF">CW112</td>
            <td valign="top" bgcolor="##FFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="20">
                <img src="../../images/icons/greyblank.png" alt="" name="" width="30" height="22" />
                </td>
                <td><span style="padding-left:5px">Maple FInish</span></td>
              </tr>
            </table></td>
            <td valign="top" bgcolor="##FFF">This is the sample paint color description that should be included here, if it has more then one line then include the desciption</td>
          </tr>
          </table>
          </div>
          
          <!--- upgrades --->
          <div style="padding-top:20px">
          
           <table width="100%" border="0" cellpadding="4" cellspacing="1" class="bodytext" bgcolor="##333">
                  <tr>
                    <td height="32" colspan="5" bgcolor="##888" class="barSmallHeading">Customer Request for Extras (Upgrades)</td>
                    </tr>
                  <tr class="barTableHead">
                    <td width="200" height="32" bgcolor="##DDD" class="barTableHead">OPTION</td>
                    <td width="45" align="center" bgcolor="##DDD">CODE</td>
                    <td width="200" bgcolor="##DDD">MATERIAL</td>
                    <td bgcolor="##DDD">DESCRIPTION</td>
                    <td width="100" align="right" bgcolor="##DDD">COST</td>
                  </tr>
                  
             <cfloop collection="#custSelection[theRoom]#" item="theOption">
        
                <cfset theMaterial = custSelection[theRoom][theOption]>
    
                <cfinvoke component="CFC.Configurator" method="getOptions" returnvariable="options">
                    <cfinvokeargument name="optionID" value="#theOption#"/>
                </cfinvoke>  
            
                <cfinvoke component="CFC.Configurator" method="getMaterials" returnvariable="material">
                    <cfinvokeargument name="optionID" value="#theOption#"/>
                    <cfinvokeargument name="assetID" value="#theMaterial#"/>
                </cfinvoke>
                
                <cfinvoke component="CFC.Modules" method="getContentAsset" returnvariable="asset">
                    <cfinvokeargument name="assetID" value="#material.asset_id#"/>
                </cfinvoke>
    
             <!--- Get Root Key --->
             <cfloop collection="#asset#" item="theKey"></cfloop>
            
                <cfset theMaterial = asset[theKey].thumb.mdpi.url>
                <cfset theImage = asset[theKey].image>
                <cfset arrayAppend(optionImages,theImage)>
                
                
               <!--- Line Item --->
               
                  <tr class="bodytext">
                    <td height="32" valign="top" bgcolor="##FFF" style="padding-top:12px; padding-bottom:0">option<!--- #options.title# ---></td>
                    <td align="center" valign="top" bgcolor="##FFF" style="padding-top:12px; padding-bottom:0">W1223<!--- #material.code# ---></td>
                    <td valign="top" bgcolor="##FFF" style="padding-top:10px; padding-bottom:0">
                    
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="20">
                        <img src="#theMaterial#" alt="" name="" width="30" height="22" />
                        </td>
                        <td><span style="padding-left:5px">material.title<!--- #material.title# ---></span></td>
                      </tr>
                    </table>
                    
                    </td>
                    <td valign="top" bgcolor="##FFF" style="padding-top:12px; padding-bottom:0">description<!--- #material.description# ---></td>
                    <td align="right" valign="top" bgcolor="##FFF" style="padding-top:12px; padding-bottom:0">1,233.00<!--- #numberFormat(material.cost,'__,__.__')# ---></td>
                  </tr>
                  
                <!---  --->
                  <cfset subTotalCost +=material.cost>
                  <cfset totalCost +=material.cost>
                       
            </cfloop>
         
         <tr class="bodytext">
                    <td height="32" colspan="4" align="right" valign="top" bgcolor="##FFF" style="padding-top:12px; padding-bottom:0"><b>Subtotal</b></td>
                    <td align="right" valign="top" bgcolor="##FFF" style="padding-top:12px; padding-bottom:0"><b>#numberFormat(subTotalCost,"__,__.__")#</b></td>
                  </tr>
         </table>   
          </div>
          
          </div>
    
      </div>
    
    </div>
</cfoutput>
</cfsavecontent> 


<cfsavecontent variable = "summary"> 
<cfoutput>
<div style="float:left; width:816px; margin-top:32px">
  
    <div class="barHeading heading">
      Summary 
    </div>
    
    <div style="float:left; width:816px;">
    
    <!--- upgrades --->
    <div style="padding-top:10px">
    <table width="100%" border="0" cellpadding="4" cellspacing="1" class="bodytext" bgcolor="##333">
    <tr class="bodytext">
      <td width="693" height="32" valign="top" bgcolor="##FFF">Kichen</td>
      <td width="100" align="right" valign="top" bgcolor="##FFF">1,200.00</td>
    </tr>
    <tr class="bodytext">
      <td width="693" height="32" valign="top" bgcolor="##FFF">Bathroom</td>
      <td width="100" align="right" valign="top" bgcolor="##FFF">1,200.00</td>
    </tr>
    <tr class="bodytext">
      <td width="693" height="32" valign="top" bgcolor="##FFF">Living Area</td>
      <td width="100" align="right" valign="top" bgcolor="##FFF">1,200.00</td>
    </tr>
    <tr class="bodytext">
      <td colspan="2" align="right" valign="top" bgcolor="##EEE">&nbsp;</td>
      </tr>
    <tr class="bodytext">
      <td height="32" align="right" valign="top" bgcolor="##FFF">Subtotal</td>
      <td align="right" valign="top" bgcolor="##FFF">12,112.00</td>
    </tr>
    <tr class="bodytext">
      <td height="32" align="right" valign="top" bgcolor="##FFF">Total HST</td>
      <td align="right" valign="top" bgcolor="##FFF">5,232.00</td>
    </tr>
    <tr class="bodytext">
      <td height="32" align="right" valign="top" bgcolor="##FFF"><b>Grand Total</b></td>
      <td align="right" valign="top" bgcolor="##FFF"><b>22,000.00</b></td>
    </tr>
    </table>
    </div>
    
    <div style="padding-top:20px">
    <table width="100%" border="0" cellpadding="5" cellspacing="1" class="bodytext" bgcolor="##333">
    <tr>
      <td height="32" bgcolor="##888" class="barSmallHeading">Notes</td>
      </tr>
    <tr class="bodytext">
      <td height="100" valign="top" bgcolor="##FFF">&nbsp;</td>
      </tr>
    </table>
    </div>
    
    <div style="padding-top:20px">
     <table width="100%" border="0" cellpadding="4" cellspacing="1" class="bodytext" bgcolor="##333">
    <tr>
      <td colspan="2" height="32" bgcolor="##888" class="barSmallHeading">Terms and Conditions</td>
      </tr>
    <tr class="noteText" style="text-align:left">
      <td width="50%" align="left" valign="top" bgcolor="##FFF">
     <ul style="padding-left: 20px; padding-right:10px">
      <li>
      The Purchaser acknowledges having compared their selections to the samples provided and understands that dye lots on samples may vary and that the above selections are final and no changes are possible when this form has been signed by the purchaser.<br />
       </li>
       <li>
       In the case of a discontinued item, or an item that is not available to be installed prior to the closing date, the Purchaser will be notified immediately and asked to re-select within forty-eight (48) hours. If no re-selection has been made within that time frame, it is at the Vendor’s discretion to re-select on behalf of the Purchaser, in accordance with the terms of the Agreement of Purchase and Sale.<br />
       </li>
        </ul>
      </td>
      <td width="50%" align="left" valign="top" bgcolor="##FFF">
     <ul style="padding-left: 20px; padding-right:10px">
       <li>
       Items selected above which have not been accepted in writing by the Vendor and do not form part of the Agreement of Purchase and Sale, any Schedules or Request for Extras form, will be rejected.<br />
        </li>
        <li>
        Upgraded selections include an internal credit for standard selections. The Vendor will not supply any standard selection materials when an upgrade has been selected. The Vendor will not supply only any of the above selections.
        </li>
        </ul>
      </td>
      </tr>
    <tr class="noteText" style="text-align:left">
      <td colspan="2" align="left" valign="top" bgcolor="##FFF"><table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td colspan="2" class="subheading">I HEREBY CONFIRM THAT I HAVE READ ALL THE INFORMATION CONTAINED IN THIS DOCUMENT INCLUDING THE TERMS AND CONDITIONS LISTED ABOVE</td>
            </tr>
            <tr>
            <td height="20" colspan="2" class="subheading">&nbsp;</td>
            </tr>
          <tr>
            <td>
            <!--- sig purchase --->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="right">Purchaser: <b>X</b></td>
                <td><div style="padding-top: 10px;"><hr size="1" /></div></td>
              </tr>
            </table>
            
            </td>
            <td>
             <!--- sig vwndor --->
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="70" align="right">Vendor: <b>X</b></td>
                <td><div style="padding-top: 10px;"><hr size="1" /></div></td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="300" align="center">Signature</td>
                <td align="center">Date</td>
              </tr>
            </table></td>
            <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="300" align="center">Signature</td>
                <td align="center">Date</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
     </table>
    </div>
    
    </div>

</div>
</cfoutput>
</cfsavecontent>
 

<div style="width:816px; padding:48px; height:100px">
	<cfoutput>
    #formHeader#
    #custInfo#
    #custOptions#
    #summary#
    </cfoutput>
</div>





