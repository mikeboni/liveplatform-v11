<cfset groupID = 0>
<cfparam name="movedTo" default="0">

<cfinvoke  component="CFC.Apps" method="getClientApps" returnvariable="clientApps">
    <cfinvokeargument name="clientID" value="#session.clientID#"/>
</cfinvoke>

<cfinvoke  component="CFC.Modules" method="getGroups" returnvariable="appProjects">
  <cfinvokeargument name="appID" value="#session.appID#"/>
  <cfinvokeargument name="subgroupID" value="0"/>
</cfinvoke>


<link href="styles.css" rel="stylesheet" type="text/css" />

<style>
.sqButton
	{ 
	border: 1px solid #666;  
	height:32px;
	background-color:#FFF;
	padding-top:2px;
	}
</style>

<script language="javascript">

var jsContentAccess = new contentAccess();

function moveProject()	{
	
	//
	var theProj = document.getElementById("projID");
	theProjectID = theProj.options[theProj.selectedIndex].value;
	
	if(theProjectID == -1)	{
		alert('Please Select a Project to Move');
		return false	
	}
	
	//
	var theApp = document.getElementById("appID");
	theAppID = theApp.options[theApp.selectedIndex].value;
	
	if(theAppID == -1)	{
		alert('Please Select App to Import the Project Into');
		return false	
	}
	
	jsContentAccess.setSyncMode();
	jsContentAccess.setCallbackHandler(moveContentSuccess);
	jsContentAccess.setMoveContent(theProjectID, <cfoutput>#session.appID#</cfoutput>, theAppID);
	
}

function moveContentSuccess(appID)	{
		document.location.href = "http://liveplatform.net/API/v11/AppsView.cfm?assetTypeTab=6&movedTo="+ appID.toString()
}

</script>

<cfif movedTo GT 0>
	
    <div style="width:auto; height:auto; margin-top:20px; padding:10px" class="contentLinkGrey">
        <p>Project has been moved to another app that was selected </p>
        <cfoutput>
        <p><a href="http://liveplatform.net/API/v11/AppsView.cfm?appID=#movedTo#">Click this link to go to this project now</a></p>
        </cfoutput>
    </div>
    
<cfelse>
    
    <div style="width:auto; height:auto; margin-top:20px; padding:10px" class="contentLinkGrey">
        1) Select the Project you would like to move and then 2) Select the App to move it to
    </div>
    
    
    <div style="width:auto; height:auto; margin-top:20px; float:left" class="formText">
    
        <div style="height:32px">
    
            <!--- Project --->
            <div style="width:80px; float:left; padding-top:6px; padding-left: 10px;" class="contentLinkGrey">Move Project</div>
            
            <div style="float:left; padding-left:10px">
            <select id="projID" name="projID" onchange="setProjID(this.options[this.selectedIndex].value);" style="padding:0 0 0 0; text-indent: 4px;; height:32px; width:200px">
              <option value="-1" style="color:#999">Select a Project</option>
              <cfloop index="project" array="#appProjects#">
                <cfoutput>
                  <option value="#project.group_id#" <cfif groupID IS project.group_id> selected</cfif>>#project.name#</option>
                </cfoutput>
              </cfloop>
            </select>
            </div>
            
          <!--- to app --->
            <div style="width:60px; float:left; text-align:right; padding-top:6px; padding-right:4px" class="contentLinkGrey">To App</div>
            
            <div style="float:left">
            <select id="appID" name="appID" onchange="setAppID(this.options[this.selectedIndex].value);" style="padding:0 0 0 0; text-indent: 4px;; height:32px; width:200px">
              <option value="-1" style="color:#999">Move to App</option>
              <cfloop query="clientApps">
              <cfif session.appID NEQ app_id>
                <cfoutput>
                  <option value="#app_id#">#appName#</option>
                </cfoutput>
              </cfif>
              </cfloop>
            </select>
            </div>
    
    <input type="submit" value="Move Selected Project" class="sqButton contentLinkGrey" style="height:32px; margin-left:20px; padding:4 4 4 4; background-color:#EEE" onClick="moveProject()">
    
        </div>
    
    </div>
    
</cfif>