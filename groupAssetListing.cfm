<cfparam name="assetGroupID" default="0">
<cfparam name="assetTypeID" default="0">
<cfparam name="subgroupID" default="0">
<cfparam name="filter" default="0">
<cfparam name="action" default="">

<cfparam name="viewType" default="0">

<!--- <cfif isDefined('subgroupid')>
	<cfset session.subgroupid = subgroupid>
<cfelse>
	<cfset session.subgroupid = ''>
</cfif>

<cfif isDefined('assetid')>
	<cfset session.assetid = assetid>
<cfelse>
	<cfset session.assetid = ''>
</cfif>

<cfif isDefined('action')>
	<cfset session.action = action>
<cfelse>
	<cfset session.action = ''>
</cfif> --->

<!--- <cfdump var="#session.subgroupid#">-<cfdump var="#session.assetid#">-<cfdump var="#session.action#"> --->


<link href="styles.css" rel="stylesheet" type="text/css">

<style>
.center-cropped {
  object-fit:cover ; /* Do not scale the image */
  object-position: center; /* Center the image within the element */
  height: 32px;
  width: 32px;
}
.center-cropped-thumb {
  object-fit:cover ; /* Do not scale the image */
  object-position: center; /* Center the image within the element */
  height: 66px;
  width: 66px;
}
</style>

<!--- GroupedAssets --->
<cfajaxproxy cfc="CFC.GroupedAssets" jsclassname="groupedAssets">

<script type="text/javascript">

var jsGroupedAssets = new groupedAssets();

function setCheckmarkState(theObj,className)	{
	
	objClass = theObj.className;
	
	if(objClass === className+'Selected') {
		theObj.className = className+'Normal';
	}else{
		theObj.className = className+'Selected';
	}
	
}


function getAllSelected()	{

	var theSelObjs = document.getElementsByClassName("checkmarkSelected");
	var allSelected = [];
	
	for(i=0; i < theSelObjs.length; i++)	{
		allSelected.push(theSelObjs[i].name);
	}
	
	return allSelected
	
}

function getAllSelectedGroups()	{

	var theSelObjs = document.getElementsByClassName("checkmarkGrpsSelected");
	var allSelected = [];
	
	for(i=0; i < theSelObjs.length; i++)	{
		allSelected.push(theSelObjs[i].name);
	}
	
	return allSelected
	
}

function moveGroup(groupID)	{
	if(confirm('Are you sure you wish to Move the Selected Asset(s)?'))
	{
	var selectedAssets = getAllSelected();
	var selectedGroups = getAllSelectedGroups();
	
	jsGroupedAssets.setSyncMode();
	jsGroupedAssets.setCallbackHandler(moveGroupSucess);
	jsGroupedAssets.moveAllGroupAssets(groupID, selectedAssets, selectedGroups);
	
	}else{
		//nothing
	}
}

function moveGroupSucess(result) {
		location.reload();
}

function syncAllAssets()	{
	
	if(confirm('Are you sure you wish to Sync All Assets?'))
	{
		jsGroupedAssets.setSyncMode();
		jsGroupedAssets.setCallbackHandler(syncAssetsSucess);
		jsGroupedAssets.syncAssets(<cfoutput>#session.appID#</cfoutput>);
		
	}else{ 
		//nothing
	}
	
}

function syncAssetsSucess(result) {
		location.reload();
}


//delete assets and groups

function deleteSelected()	{
	
	if(confirm('Are you sure you wish to Delete the Selected Asset(s)?'))
	{
		var allSelected = getAllSelected();
		var allGroupsSelected = getAllSelectedGroups();
	
		jsGroupedAssets.setSyncMode();
		jsGroupedAssets.setCallbackHandler(deleteAllSelectedSucess);
		jsGroupedAssets.deleteAssets(<cfoutput>#session.appID#</cfoutput>, allSelected, allGroupsSelected);
		
	}else{ 
		//nothing
	}
	
}

function deleteAllSelectedSucess(result) {
		location.reload();
}


function createAssetGroup()	{
	
	var theGroup = document.getElementById("groupName").value;
	
	if(theGroup === '')	{
		
		createNewGroup("hidden");
		alert("A Name for the Group must be specified");
		
	}else{
		theGroupName = theGroup;
		
		jsGroupedAssets.setSyncMode();
		jsGroupedAssets.setCallbackHandler(createAssetGroupSucess);
		jsGroupedAssets.createAssetGroup(<cfoutput>#session.appID#</cfoutput>, <cfoutput>#subgroupID#</cfoutput>, theGroupName);
	}

}

function createAssetGroupSucess(result) {
		createNewGroup("hidden");
		location.reload();
}


function createNewAsset(assetType)	{
	
	//location.href = "Asset.cfm?assetTypeID="+ assetType; //New assets
	location.href = "updateAsset.cfm?assetTypeID="+ assetType + "&subgroupID=" + <cfoutput>#subgroupID#</cfoutput>; //Old assets

}


function createNewGroup(state)	{
	
	var theGroup = document.getElementById("newGroup");
	var theState;
	
	if(state == undefined)	{
		//current state
	 	theState = theGroup.style.visibility;
		
		if(theState == "hidden")	{
			theGroup.style.visibility = "visible";
		}else{
			theGroup.style.visibility = "hidden";
		}
		
	}else{
		//controlled state
		theGroup.style.visibility = state;
		
	}
	
}


function editGroupName(groupID)	{
	
	var theGroupOld = document.getElementById("folder_"+groupID);
	var theGroupNew = document.getElementById("folderName_"+groupID);
	
	theGroupOld.style.display = "none";
	theGroupNew.style.display = "block";
}

function cancelAssetGroup(groupID)	{
	
	var theGroupOld = document.getElementById("folder_"+groupID);
	var theGroupNew = document.getElementById("folderName_"+groupID);
	
	theGroupOld.style.display = "block";
	theGroupNew.style.display = "none";
}


function updateAssetGroup(theGroupID)	{
	
	var theGroup = document.getElementById("groupAssetName_"+theGroupID).value;
	
	if(theGroup === '')	{
		
		cancelAssetGroup(theGroupID);
		alert("A Name for the Group must be specified");
		
	}else{
		theGroupName = theGroup;
		
		jsGroupedAssets.setSyncMode();
		jsGroupedAssets.setCallbackHandler(updateAssetGroupSucess);
		jsGroupedAssets.updateAssetGroup(theGroupID, theGroupName);
	}
	
}

function updateAssetGroupSucess(result) {
		location.reload();
}


function importAssetsToContent()	{
		
		var allSelected = getAllSelected();
		var allGroupsSelected = getAllSelectedGroups();
		
		jsGroupedAssets.setSyncMode();
		
		<cfif isDefined('session.subgroupid') OR isDefined('session.assetid')>
		
			<cfif session.subgroupid GT 0>
				<!--- import into content group --->
				jsGroupedAssets.setCallbackHandler(importedAssetsSucess);
				jsGroupedAssets.importAllAssetsIntoGroup(<cfoutput>#session.appID#</cfoutput>,<cfoutput>#session.subgroupID#</cfoutput>, allSelected, allGroupsSelected);
			</cfif>
		
			<cfif session.assetID GT 0>
				<!--- import into assets - actions or assets --->
				<cfif action NEQ ''>
				jsGroupedAssets.setCallbackHandler(importedAssetsSucess);
				jsGroupedAssets.importAssetAssetsActions(<cfoutput>#session.assetID#</cfoutput>, allSelected, allGroupsSelected,<cfoutput>#action#</cfoutput>);
				</cfif>
			</cfif>
			
		</cfif>
	
}

function importedAssetsSucess(result)	{	

		var refreshLink = '';
		
		if(result.assetID == undefined)	{
			refreshLink = "AppsView.cfm?subgroupID="+result.groupID+"&error="+'Imported ('+ result.total +') Assets';
		}else{
			refreshLink = "AssetsView.cfm?assetID="+result.assetID+"&assetTypeID="+result.assetTypeID;
		}
		location.href = refreshLink;
}


function setListView(theObj)	{
		
		<cfif viewType>
			var theType = 0;
		<cfelse>
			var theType = 1;
		</cfif>

		document.location.href = 'AppsView.cfm?assetTypeTab=<cfoutput>#assetTypeTab#</cfoutput>&subgroupID=<cfoutput>#subgroupID#</cfoutput>&viewType=' + theType;
		
}	

</script>


<cfinvoke  component="CFC.Misc" method="QueryToStruct" returnvariable="allTypes">
    <cfinvokeargument name="query" value="#session.AssetsTypes#"/>
    <cfinvokeargument name="forceArray" value="true"/>
</cfinvoke>


<cfinvoke component="CFC.GroupedAssets" method="getAssetsAndGroups" returnvariable="assets">
  <cfinvokeargument name="appID" value="#session.appID#"/>
  <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
  <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
</cfinvoke>

<cfinvoke  component="CFC.GroupedAssets" method="getAllAssetGroups" returnvariable="allGroups">
    <cfinvokeargument name="appID" value="#session.appID#"/>
    <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
</cfinvoke>

<div style="width:805px;">

<cfoutput>
		
    <div class="contentLinkGrey" style="width:810px; height:auto; margin-top:2px; background-color:##DDD; float:left">
        <cfinvoke  component="CFC.GroupedAssets" method="getBreadcrumb" >
            <cfinvokeargument name="groupID" value="#subgroupID#"/>
          </cfinvoke> 
        <div style="float:right; width:auto;">
        	<div style="float:left; width:auto;visibility:hidden" id="newGroup">
            	
                <div style="padding-top:12px; padding-left:5px; padding-right:10px; width:auto; float:left">
                New Group:
                </div>
                <div style="float:left; width:auto; padding-top:5px">
                <input name="groupName" id="groupName" type="text" class="formText" style="width:200px">
                </div>
                <div style="float:left; width:auto;">
                <img src="images/saveupdate.png" style="cursor:pointer;vertical-align: sub" onClick="createAssetGroup()" />
                </div>
                
            </div>
            <div style="float:right; width:auto;">
            <img src="images/folder.png" style="cursor:pointer;vertical-align: sub" onClick="createNewGroup()" />
            </div>
      </div>
    </div>
        
        <div class="contentLinkGrey" style="width:808px;padding: 0px 0px 0px 2px; height:44px; margin-top:2px; background-color:##DDD; float:left">
          <div style="float:left; width:550px">
          <cfif viewType>
		  		<img src="images/grid-view.png" id="viewType" style="cursor:pointer; float:left" onClick="setListView(this);" />
		  <cfelse>
          		<img src="images/list-view.png" id="viewType" style="cursor:pointer; float:left" onClick="setListView(this);" />
          </cfif>
          <div style="padding-top:12px; padding-left:5px; padding-right:10px; width:auto; float:left">
          <a href="AppsView.cfm?assetTypeTab=#assetTypeTab#&subgroupID=#subgroupID#" class="contentLink">All</a> |
          <a href="AppsView.cfm?assetTypeTab=#assetTypeTab#&subgroupID=#subgroupID#&filter=1" class="contentLink">Content</a> | 
          <a href="AppsView.cfm?assetTypeTab=#assetTypeTab#&subgroupID=#subgroupID#&filter=2" class="contentLink">Groups</a>
          </div>
          <div style="width:auto; float:left">
          <cfif subgroupID IS 0>
          	<img src="images/replace.png" style="cursor:pointer" onClick="syncAllAssets();" />
          </cfif>
          <cfif session.assetID GT 0 OR session.subgroupID GT 0>
          	<img src="images/importAsset.png" style="cursor:pointer" onClick="importAssetsToContent();" />
          </cfif>
          </div>
          </div>
              <div style="float:right">
              <select name="moveAsset" class="itemShow" id="moveAsset" style="background:url(images/moveAsset.png) no-repeat; border:0; width:20px; height:55px;color:rgba(0,0,0,0);" type="button" onchange="moveGroup(this.options[this.selectedIndex].value)" />
              	<option value="0" style="color:##333; padding:5px">Root</option>
                <cfloop query="allGroups">
                  <option value="#group_id#" style="color:##333; padding:5px">#name#</option>
                </cfloop>
            </select>  
            
   			<img src="images/remove.png" style="cursor:pointer;vertical-align: sub" onClick="deleteSelected()" />
            
              <select name="createAsset" id="createAsset" style="background:url(images/include-sm.png) no-repeat; border:0; width:32px; height:50px;color:rgba(0,0,0,0);" type="button" onchange="createNewAsset(this.options[this.selectedIndex].value)" />
                <option value="0" style="color:##AAA; padding:5px" selected>Create New Asset</option>
                <cfloop index="aType" array="#allTypes#">
                  <option value="#aType.type#" style="color:##333; padding:5px">#aType.name#</option>
                </cfloop>
            </select>
              
                 
              </div>
          </div>
      
      <div class="contentLinkGrey" style="width:810px; height:auto; float:left;">

        <cfif assets.recordCount GT 0>
		
        <cfif viewType IS 0>
        
       	 <table width="100%" border="0" cellspacing="1" cellpadding="0" class="contentLink material">
       
        <cfloop query="assets">
        
        <cfif assetName IS ''>
        	<cfset name = 'null asset (delete)'>
        <cfelse>
        	<cfset name = assetName>
        </cfif>
        
        
        <cfif assetType_id NEQ ''>
        <cfif structKeyExists(assetPathsCur.types, assetType_id)>
        <!--- assets --->
        <cfset assetPath = assetPathsCur.types[assetType_id]>
        <cfif filter IS 1 OR filter IS 0>
          <tr>
            <td style="padding-left:8px">
            <div style="width:auto; float:left">
            <cfset thumbIMG = assetPath & 'thumbs/'& image>
            <a href="AppsView.cfm?assetTypeTab=#assetTypeTab#&assetTypeID=#assetType_id#&subgroupID=#subgroupID#" style="margin-right:2px">
            <img src="images/create/#icon#" width="32" height="32" border="0" style="background-color:##666;"/></a><cfif fileExists(ExpandPath(thumbIMG))><img src="#thumbIMG#" border="0" class="center-cropped" style="background-color:##666"></cfif>
            </div>
            <div style="padding-top:6px;">
            <a href="AssetsView.cfm?assetID=#asset_ID#&assetTypeID=#assetType_id#" style="padding-left: 10px;" class="contentLink">#name#</a>
            </div>
            </td>
            <td width="44"><input name="#asset_ID#" id="sel" class="checkmarkNormal" onclick="setCheckmarkState(this,'checkmark');" type="button"></td>
            </tr>
          </cfif>
          </cfif>
          
          <cfelse>
          <!--- groups --->
          <cfif filter IS 2 OR filter IS 0>
          <tr>
            <td style="padding-left:8px">
            <cfif groupType>
            <div style="width:auto; float:left">
            <img src="images/folder.png" width="32" height="32" border="0" ondblclick="editGroupName(#group_id#)" />
            </div>
            </cfif>
            <div style="padding-top:6px;float:left" id="folder_#group_id#">
            <a href="AppsView.cfm?assetTypeTab=4&subgroupID=#group_id#" class="contentLink">#name#</a>
            </div>
            <div id="folderName_#group_id#" style="float:left; width:auto;display:none">
            	
                <div style="float:left; width:auto; padding-top:5px">
                <input type="text" class="formText" id="groupAssetName_#group_id#" style="width:300px;padding:0px 0px 0px 0px;" value="#name#">
                </div>
                <div style="float:left; width:auto;">
                <img src="images/saveupdate.png" width="32" style="cursor:pointer;vertical-align: sub" onClick="updateAssetGroup(#group_id#)" />
                <img src="images/remove.png" width="32" style="cursor:pointer;vertical-align: sub" onClick="cancelAssetGroup(#group_id#)" />
                </div>
            
            </div>
            </td>
            <td width="44"><input name="#group_id#" id="sel" class="checkmarkGrpsNormal" onclick="setCheckmarkState(this,'checkmarkGrps');" type="button"></td>
            </tr>
          </cfif>
       </cfif>
          </cfloop>
          </table>
        
        <cfelse>

            <div style="margin:2px; padding:2px">
            <cfloop query="assets">
            
            <cfif structKeyExists(assetPathsCur.types, assetType_id)>
            
				<!--- Get Asset --->
                <cfinvoke  component="CFC.GroupedAssets" method="getVisualAsset" returnvariable="assetData">
                    <cfinvokeargument name="assetID" value="#asset_id#"/>
                    <cfinvokeargument name="assetType" value="#assetType_id#"/>
                </cfinvoke>
                    
                <div style="width:auto; float:left; padding:2px">
                
                <a href="AssetsView.cfm?assetID=#asset_ID#&assetTypeID=#assetType_id#">
					<cfif NOT fileExists(ExpandPath(assetData.mdpi))>
                        <img src="images/create/#icon#" width="68" height="68" border="0" style="background-color:##666;"/>
                    <cfelse>
                        <img src="#assetData.mdpi#" width="68" height="68" border="0" class="center-cropped-thumb" style="background-color:##666;"/>
                    </cfif>
                </a>
                </div>
                
            </cfif>    
                
            </cfloop>
            </div>
            
        </cfif>
        
          <cfelse>
          <table width="100%" border="0" cellspacing="1" cellpadding="0" class="contentLink material">
          <tr><td height="44" class="contentLinkDisabled" style="padding-left:10px">No Assets in Category</td></tr>
          </table>
          </cfif>
        
    	
    </div>

      
      
  </cfoutput>


</div>


