<cfparam name="clientID" default="0">

<cfparam name="name" default="">
<cfparam name="folder" default="">
<cfparam name="rootPath" default="">
<cfparam name="active" default="true">
<cfparam name="icon" default="">
<cfparam name="clientID" default="0">
<cfparam name="supportEMail" default="">
<cfparam name="clientEmail" default="">

<cfset tempPath = GetTempDirectory()>

<cfif imageFile NEQ ''>
	<cffile action="upload" filefield="imageFile" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset imageFile = uploadFile.serverFile>
<cfelse>
	<cfset imageFile = ''>
</cfif>

<cfif session.clientID GT '0'>
	<!---Update Client--->
	
    <cfif name NEQ ''>
    
	<cfinvoke component="CFC.clients" method="updateClient" returnvariable="cientID">
		<cfinvokeargument name="clientID" value="#session.clientID#"/>
		<cfinvokeargument name="company" value="#name#"/>
		<cfinvokeargument name="path" value="#folder#"/>
		<cfinvokeargument name="rootPath" value="#rootPath#"/>
		<cfinvokeargument name="image" value="#imageFile#"/>
		<cfinvokeargument name="active" value="#active#"/>
        <cfinvokeargument name="support" value="#supportEMail#"/>
        <cfinvokeargument name="clientEmail" value="#clientEmail#"/>
	</cfinvoke>
	
    <cfif session.clientID GT '0'>
		<cfset error = 'Client Updated'>
		<cflocation url="ClentsView.cfm?clientID=#session.clientID#" addtoken="no">
	<cfelse>
		<cfset error = 'Client Already Not Updated'>
		<cflocation url="ClentsView.cfm?clientID=0&amp;error=#error#" addtoken="no">
	</cfif>
    
    <cfelse>
    	<cfset error = 'Company Name Cannot be Empty. Client NOT Updated.'>
        <cflocation url="ClentsView.cfm?clientID=0&amp;error=#error#" addtoken="no">
    </cfif>
	
<cfelse>
	
    <cfif name NEQ '' AND folder NEQ ''>
		<!---New Client--->
        <cfinvoke component="CFC.clients" method="createClient" returnvariable="cientID">
            <cfinvokeargument name="company" value="#name#"/>
            <cfinvokeargument name="path" value="#folder#"/>
            <cfinvokeargument name="image" value="#imageFile#"/>
            <cfinvokeargument name="active" value="#active#"/>
            <cfinvokeargument name="support" value="#supportEMail#"/>
            <cfinvokeargument name="clientEmail" value="#clientEmail#"/>
        </cfinvoke>

        <cfif cientID GT '0'>
            <cfset error = 'Client Created'>
            <cflocation url="ClentsView.cfm?clientID=#cientID#" addtoken="no">
        <cfelse>
            <cfset error = 'Client Already Exists'>
            <cflocation url="ClentsView.cfm?clientID=0&amp;error=#error#" addtoken="no">
        </cfif>
    <cfelse>
    		<cfset error = 'Company Name OR Folder Path Cannot be Empty. Client NOT created.'>
            <cflocation url="ClentsView.cfm" addtoken="no">
	</cfif>
	
</cfif>
            

