<link href="JS/spectre.min.css" rel="stylesheet" type="text/css" />
<link href="JS/font/style.css" rel="stylesheet" type="text/css">

<script src="https://code.jquery.com/jquery-2.2.3.min.js" integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo=" crossorigin="anonymous"></script>
<script type="text/javascript" src="JS/underscore.js"></script>

<!--- <script language="javascript">
	
	function person(firstName, lastName, age, eyeColor) {
		
		this.firstName = firstName || 'Unknown'; 
		this.lastName = lastName || 'Unknown';
		this.age = age || 18;
		this.eyeColor = eyeColor || 'Brown';
		
		this.changeName = function (name) {
			this.firstName = name;
		};
		this.changeAge = function (age) {
			this.age = age;
		};
	}
	
	var Boy = new person('Mike','Bonifacio');
	var Girl = new person('Mike','Bonifacio',0,'brown');
	
	Girl.changeName('Tammy');
	Girl.changeAge(49);
	
	console.log(Boy);
	console.log(Girl);
	
	
	var people = function(firstName, lastName, age, eyeColor) {
		
		return {
		firstName : firstName || 'Unknown', lastName : lastName || 'Unknown', age : age || 18, eyeColor : eyeColor || 'Brown',
		changeName : function(firstName) { firstName = firstName; },
		changeAge : function(age) { age = age; },
		getName : function() { conosle.log(firstName +' '+ lastName); }
		}
	
	}
	
	people.changeName= 'Tammy';
	people.changeAge = 49;
	
	console.log(getName);
	
</script> --->

<script type="text/javascript">

$( document ).ready(function() {
	
	allObjs = ["info","cancel","save"];
	
	_.each(allObjs, function(anObj)	{
		
	   var theObj = '#'+ anObj;
		
	  $(theObj).click( function(anObj) { 
	  	
		_.each(allObjs, function(aObj)	{
			var theObj = '#'+ aObj;
			$(theObj).removeClass().addClass("btn");
		});
	
	  	$(theObj).toggleClass("btn-primary");
		
		//save selected
		if($(theObj).attr('id') === 'save')	{
			$("#myModal").toggleClass("active");	
			
		}
		
	  } );
	
	});
	
	//init events
	$("#close").click( function() { 
		console.log('closeX');
		$("#myModal").toggleClass("active");
	});
	
	$("#closex").click( function() { 
		console.log('close');
		$("#myModal").toggleClass("active");
	});
	
	$("#share").click( function() { 
		console.log('share');
		$("#myModal").toggleClass("active");
	});
		
});



</script>
<!--- //clear all states
//
console.log(anObj);
//set the selected
//theObj = theObj.currentTarget;
//$(anObj).toggleClass("btn-primary");	 --->
              
<body>

<h2>H2 Title <small class="label">4rem</small></h2>

<p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>

<!-- description list -->
<dl>
    <dt>description list term</dt>
    <dd>description list description</dd>
</dl>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>name</th>
            <th>duration</th>
            <th>genre</th>
            <th>release date</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>The Shawshank Redemption</td>
            <td>2h 22min</td>
            <td>Crime, Drama</td>
            <td>14 October 1994 <span class="label">USA</span></td>
        </tr>
        <tr>
            <td>The Godfather</td>
            <td>2h 55min</td>
            <td>Crime, Drama</td>
            <td>24 March 1972 <span class="label">USA</span></td>
        </tr>
        <tr>
            <td>Schindler's List</td>
            <td>3h 15min</td>
            <td>Biography, Drama, History</td>
            <td>4 February 1994 <span class="label">USA</span></td>
        </tr>
        <tr>
            <td>Se7en</td>
            <td>2h 7min</td>
            <td>Crime, Drama, Mystery</td>
            <td>24 March 1972 <span class="label">USA</span></td>
        </tr>
    </tbody>
</table>

<button class="btn">default button</button>
<button class="btn loading">default button</button>

<button class="btn btn-primary"><span class="icon icon-markunread"></span> normal</button>


<div class="btn-group btn-group-block">
    <button class="btn">first button</button>
    <button class="btn">second button</button>
    <button class="btn">third button</button>
</div>


<img src="http://liveplatform.net/eq/greystone/assets/videos/thumbs/uptowne-collection_video.jpg" class="img-responsive rounded" />

<div class="video-responsive">
    <iframe src="https://www.youtube.com/embed/vBfBa-gCMQk" width="560" height="315" frameborder="0" allowfullscreen></iframe>
</div>


<header class="navbar">
    <section class="navbar-section">
        <a href="#" class="btn btn-link btn-lg">
            <i class="icon icon-people"></i>
        </a>
        <a href="#" class="navbar-brand">Spectre.css</a>
    </section>
    <section class="navbar-section">
        <input type="text" class="form-input input-inline" placeholder="search" />
        <a href="#" class="btn btn-link">documents</a>
        <a href="#" class="btn btn-link">login</a>
        <a href="#" class="btn btn-primary">signup</a>
    </section>
</header>


<div class="empty">
    <i class="icon icon-people"></i>
    <p class="empty-title">You have no new messages</p>
    <p class="empty-meta">Click the button to start a conversation.</p>
    <button class="btn btn-primary">Send a message</button>
</div>


<figure class="avatar avatar-xl">
    <img src="http://liveplatform.net/eq/greystone/assets/videos/thumbs/uptowne-collection_video.jpg" />
</figure>

<figure class="avatar avatar-xl">
    <img src="http://liveplatform.net/eq/greystone/assets/videos/thumbs/uptowne-collection_video.jpg" />
    <img src="http://liveplatform.net/eq/greystone/assets/videos/thumbs/uptowne-collection_video.jpg" class="avatar-icon" />
</figure>

<figure class="avatar avatar-xl" data-initial="YZ" style="background-color: #5764c6;"></figure>


<div class="chip-sm">
    <span class="chip-name">Crime</span>
    <button class="btn btn-clear"></button>
</div>


<button class="btn tooltip" data-tooltip="Lorem ipsum dolor sit amet">top tooltip</button>

<span class="badge" data-badge="99">
    Notifications
</span>


<div class="toast">
    <button class="btn btn-clear float-right"></button>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
</div>


<ul class="menu">
    <!-- menu header -->
    <li class="menu-header">
        <span class="menu-header-text">
            Go to
        </span>
    </li>
    <!-- menu item -->
    <li class="menu-item">
        <a href="#">
            <span class="icon icon-link"></span> Slack
        </a>
    </li>
    <li class="menu-item">
        <a href="#">
            <span class="icon icon-link"></span> Hipchat
        </a>
    </li>
    <li class="menu-item">
        <a href="#">
            <span class="icon icon-link"></span> Skype
        </a>
    </li>
    <!-- menu divider -->
    <li class="divider"></li>
    <li class="menu-item">
        <a href="#">
            <span class="icon icon-link"></span> Settings
        </a>
    </li>
</ul>


<div class="columns">
                <div class="column">
                    <ul class="menu">
                        <li class="menu-item">
                            <div class="chip">
                                <div class="chip-icon">
                                    <img src="http://liveplatform.net/eq/greystone/assets/videos/thumbs/uptowne-collection_video.jpg" class="avatar">
                                </div>
                                <div class="chip-content">
                                    Steve Rogers
                                </div>
                                <div class="chip-action">

                                </div>
                            </div>
                        </li>
                        <li class="menu-item">
                            <a href="#menus" class="active">
                                My profile
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li class="menu-item">
                            <a href="#menus">
                                Settings
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="#menus">
                                Logout
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="column">
                    <ul class="menu">
                        <li class="menu-header">
                            <span class="menu-header-text">
                                Go to
                            </span>
                        </li>
                        <li class="menu-item">
                            <a href="#menus">
                                <span class="icon icon-link"></span> Slack
                            </a>
                        </li>
                        <li class="menu-item">
                            <a href="#menus">
                                <span class="icon icon-link"></span> Hipchat
                            </a>
                        </li>
                        <li class="menu-item">
                            <span class="icon icon-link"></span> Skype
                        </li>
                    </ul>
                </div>
            </div>


<ul class="breadcrumb">
    <li class="breadcrumb-item">
        <a href="#">
            Home
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">
            Profile
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">
            Change avatar
        </a>
    </li>
</ul>

<ul class="tab tab-block">
    <li class="tab-item active">
        <a href="#">
            Music
        </a>
    </li>
    <li class="tab-item">
        <a href="#">
            Playlists
        </a>
    </li>
    <li class="tab-item">
        <a href="#">
            Radio
        </a>
    </li>
    <li class="tab-item">
        <a href="#">
            Connect
        </a>
    </li>
</ul>


<div class="modal" id="myModal"><!--- active --->
    <div class="modal-overlay"></div>
    <div class="modal-container">
        <div class="modal-header">
            <button class="btn btn-clear float-right" id="closex"></button>
            <div class="modal-title">Modal title</div>
        </div>
        <div class="modal-body">
            <div class="content">
                <!-- content here -->
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-link" id="close">Close</button>
            <button class="btn btn-primary" id="share">Share</button>
        </div>
    </div>
</div>

<div class="column">
    <div class="modal-temp modal-sm">
        <div class="modal-overlay"></div>
        <div class="modal-container" role="document">
            <div class="modal-header">
                <button type="button" class="btn btn-clear float-right" aria-label="Close"></button>
                <div class="modal-title">Modal title</div>
            </div>
            <div class="modal-body">
                <div class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-link">Close</button>
                <button class="btn btn-primary">Share</button>
            </div>
        </div>
    </div>
</div> 

<div class="modal-temp modal-sm">
    <div class="modal-overlay"></div>
    <div class="modal-container" role="document">
        <div class="modal-header">
            <button type="button" class="btn btn-clear float-right" aria-label="Close"></button>
            <div class="modal-title">Modal title</div>
        </div>
        <div class="modal-body">
            <div class="content">
                <form>
                    <div class="form-group">
                        <label class="form-label" for="input-example-7">Name</label>
                        <input class="form-input" id="input-example-7" placeholder="Name" type="text">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Gender</label>
                        <label class="form-radio">
                            <input name="gender" type="radio">
                            <i class="form-icon"></i> Male
                        </label>
                        <label class="form-radio">
                            <input name="gender" checked="" type="radio">
                            <i class="form-icon"></i> Female
                        </label>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-link">Close</button>
            <button class="btn btn-primary">Submit</button>
        </div>
    </div>
</div>

<div class="columns">
      <div class="column col-4">
          <div class="card">
              <div class="card-image">
                  <img class="img-responsive" src="http://liveplatform.net/eq/greystone/assets/videos/thumbs/uptowne-collection_video.jpg">
              </div>
              <div class="card-header">
                  <h4 class="card-title">Microsoft</h4>
                  <h6 class="card-meta">Software and hardware</h6>
              </div>
              <div class="card-body">
                  Empower every person and every organization on the planet to achieve more.
              </div>
              <div class="card-footer">
                  <a href="#" class="btn btn-primary">Do</a>
              </div>
          </div>
      </div>
      <div class="column col-4">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Liberty Development</h4>
                  <h6 class="card-meta">Hardware and software</h6>
              </div>
              <div class="card-image">
              		
                  <img class="img-responsive" src="../../liberty/images/liberty-normal-icon.png" width="150" style="padding-left:15px">
              </div>
              <div class="card-body">
                  To make a contribution to the world by making tools for the mind that advance humankind.
              </div>
              <div class="card-footer">
                  <div class="btn-group btn-group-block">
                      <button class="btn btn-primary" id="info">Info</button>
                      <button class="btn" id="cancel">Cancel</button>
                      <button class="btn" id="save">Save</button>
                  </div>
              </div>
          </div>
      </div>
      <div class="column col-4">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Google</h4>
                  <h6 class="card-meta">Software and hardware</h6>
              </div>
              <div class="card-body">
                  Organize the world’s information and make it universally accessible and useful.
              </div>
              <div class="card-image">
                  <img class="img-responsive" src="http://liveplatform.net/eq/greystone/assets/videos/thumbs/uptowne-collection_video.jpg">
              </div>
          </div>
      </div>
  </div>
  
  
  <div class="divider"></div>
  
  


Position utilities are used for useful layout and position things, including clearfix, float, position and margin/padding utilities.

<!-- clear float -->
<div class="clearfix"></div>
<!-- float: left and right-->
<div class="float-left"></div>
<div class="float-right"></div>
<!-- position: relative, absolute and fixed-->
<div class="rel"></div>
<div class="abs"></div>
<div class="fixed"></div>
<!-- centered block -->
<div class="centered"></div>
<!-- margin: 10px and 5px in 4 directions. mt-10 = margin-top: 10px; -->
<div class="mt-10"></div>
<div class="mt-5"></div>
<!-- padding: 10px and 5px in 4 directions. pt-10 = padding-top: 10px; -->
<div class="pt-10"></div>
<div class="pt-5"></div>

Display utilities are used for display and hidden things.

<!-- display: block; -->
<div class="block"></div>
<!-- display: inline; -->
<div class="inline"></div>
<!-- display: inline-block; -->
<div class="inline-block"></div>
<!-- display: flex; -->
<div class="flex"></div>
<!-- display: inline-flex; -->
<div class="inline-flex"></div>
<!-- display: none; -->
<div class="hide"></div>
<!-- visibility: visible; -->
<div class="visible"></div>
<!-- visibility: hidden; -->
<div class="invisible"></div>
<!-- hide text contents -->
<div class="text-hide"></div>

Text utilities are used for text alignment, styles and overflow things.

<!-- left-aligned text -->
<div class="text-left"></div>
<!-- center-aligned text -->
<div class="text-center"></div>
<!-- right-aligned text -->
<div class="text-right"></div>
<!-- justified text -->
<div class="text-justify"></div>

<!-- Lowercased text -->
<div class="text-lowercase"></div>
<!-- Uppercased text -->
<div class="text-uppercase"></div>
<!-- Capitalized text -->
<div class="text-capitalize"></div>

<!-- Normal weight text -->
<div class="text-normal"></div>
<!-- Bold text -->
<div class="text-bold"></div>
<!-- Italicized text -->
<div class="text-italic"></div>

<!-- Overflow behavior: display an ellipsis to represent clipped text -->
<div class="text-ellipsis"></div>
<!-- Overflow behavior: truncate the text -->
<div class="text-clip"></div>
<!-- Text may be broken at arbitrary points -->
<div class="text-break"></div>


</body>
  