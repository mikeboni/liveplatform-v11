<cfparam name="supportEmail" default="support@liveplatform.net">
<cfparam name="supportSubject" default="Support Notification">
<cfparam name="supportMessage" default="">
<cfparam name="accessLevel" default="">

<cfinvoke component="CFC.Users" method="getActiveUsers" returnvariable="users">
	<cfinvokeargument name="clientID" value="#session.clientID#"/>
    <cfinvokeargument name="appID" value="#session.appID#"/>
    <cfinvokeargument name="accessLevel" value="#accessLevel#"/>
</cfinvoke>

<cfinvoke component="CFC.Clients" method="getClientInfo" returnvariable="clientInfo">
	<cfinvokeargument name="clientID" value="#session.clientID#"/>
</cfinvoke>

<!--- Get Paths --->
<cfinvoke component="CFC.Apps" method="getPaths" returnvariable="assetPaths">
    <cfinvokeargument name="clientID" value="#session.clientID#"/>
    <cfinvokeargument name="appID" value="#session.appID#"/>
    <cfinvokeargument name="server" value="yes"/>
</cfinvoke>

<cfset info = {"email":supportEmail,"subject":supportSubject,"message":supportMessage}>

<!---  <cfdump var="#assetPaths#">
<cfdump var="#info#">
<cfdump var="#users#"> 
 --->
<cfoutput query="users">

    <cfmail server="cudaout.media3.net"
      username="support@wavecoders.ca"
      from="#assetPaths.client.name# - Support <#info.email#>"
      to="#users.name# <#users.email#>"
      subject="#info.subject# "
      replyto="#assetPaths.client.name# - Support <#info.email#>"
      type="HTML">     

	  <cfinclude template="CFC/emailSupport.cfm">
                 
	</cfmail>
 
 </cfoutput>
 
<cflocation url="usersView.cfm?assetTypeTab=0" addtoken="no">
 
</body>
</html>