<cfparam name="assetID" default="0">

<cfinvoke component="CFC.Assets" method="deleteAssetDetails" returnvariable="deleted">
    <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<!--- <cflocation url="AppsView.cfm?assetTypeTab=3&error=Deleted Asset" addtoken="no"> --->


<cflocation url="AppsView.cfm?assetTypeTab=#assetTypeTab#&assetTypeID=#assetTypeID#&error=Updated Asset Details" addtoken="no"> 