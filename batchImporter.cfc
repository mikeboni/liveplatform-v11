<cfcomponent>

	<cffunction name="importAssets" access="remote" output="yes" returntype="string">
    	<cfargument	name="appID" type="numeric" required="no" default="0" />
    	<cfargument	name="importFile" type="string" required="no" default="" />
        <cfargument	name="importType" type="numeric" required="no" default="0" />
  		
        
        
        <cfif importFile IS '' OR appID IS 0>
        	AppID:<cfdump var="#appID#">-ImportFile:<cfdump var="#importFile#">
        	<cfreturn "missing (zip) file that contains data and import instructions">
        </cfif>
        
        <cfif importType IS -1>
        	<cfreturn "Import Type was NOT selected">
        </cfif>
        
        <!--- Read and Parse CSV File --->
		<cfset tempDir = GetTempDirectory()>
        
        <cfset error = "ok">
        
        <cffile action="upload" fileField="importFile" destination="#tempDir#" nameconflict="overwrite" result="newFile">
       
        <cfset theFileLoc = newFile.serverDirectory &'/'& newFile.serverFile>
        <cfset theFileZipLoc = newFile.serverDirectory &'/'& newFile.serverFileName &'/'>
        
        <!--- check if zip directory exists else create --->
        <cfif NOT directoryExists(theFileZipLoc)>
        	<cfdirectory action="create" directory="#theFileZipLoc#" mode="777">
        </cfif>
        
		<!--- unzip file --->
        <cfzip action = "unzip" destination = "#newFile.serverDirectory#" file = "#theFileLoc#" overwrite = "true" recurse = "true" storePath = "true">

        <!--- <cffile action = "read" file = "#theFileLoc#" variable = "data"> --->
        
        <cfset importFileXLSPath1 = theFileZipLoc &"/Import.xls">
        <cfset importFileXLSPath2 = theFileZipLoc &"/Import.xlsx">
        
        <cfif fileExists(importFileXLSPath1)>
        	<cfset importFileXLS = importFileXLSPath1>
        <cfelseif fileExists(importFileXLSPath2)>
        	<cfset importFileXLS = importFileXLSPath2>
        <cfelse>
        	<cfset importFileXLS = "">
		</cfif>
        
        
        <cfif fileExists(importFileXLS)>
        
			<!--- Read spreadsheet --->
            <cfspreadsheet action="read" src="#importFileXLS#" headerrow="1" excludeHeaderRow="true" query="importXLS">
            
            <cfset failed = false>

            <cfquery dbtype="query" name="importQuery">
                SELECT *
                FROM importXLS
                WHERE typeID <> ''
                ORDER BY typeID
            </cfquery>
                  
			<cfset importCnt = importQuery.recordCount>

            <!--- Import New Assets --->
            <cfoutput query="importQuery">
                
                <cfif importQuery.typeID NEQ ''>
                
                    <cfif path IS ''>
                        <cfset img = theFileZipLoc & image>
                        <cfset thm = theFileZipLoc & thumb>
                    <cfelse>
                        <cfset img = theFileZipLoc & path &"/"& image>
                        <cfset thm = theFileZipLoc & path &"/"& thumb>
                    </cfif>
                    
                    <cfif image NEQ '' OR thumb NEQ ''>
                    
                        <cfif image NEQ ''>
                            <!--- check ext image--->
                            <cfset foundExtP = FindNoCase('.png', image)>
                            <cfset foundExtJ = FindNoCase('.jpg', image)>	
                            
                            <cfif foundExtP GT 0 OR foundExtJ GT 0>
                            
                                <cfif NOT fileExists(img)>
    
                                    <cfif image NEQ ''>
                                        File Not Exists on Image:<cfdump var="#img#"><br>
                                        <cfset failed = true>
                                    </cfif>
                                    
                                </cfif>
                            
                            <cfelse>
                                File Ext is Missing on Image:<cfdump var="#image#"><br>
                                <cfset failed = true>
                            </cfif>
                        </cfif>
                        
                        <cfif thumb NEQ ''>  
                            <!--- check ext thumb--->
                            <cfset foundExtP = FindNoCase('.png', thumb)>
                            <cfset foundExtJ = FindNoCase('.jpg', thumb)>	
                            
                            <cfif foundExtP GT 0 OR foundExtJ GT 0>
                            
                                <cfif NOT fileExists(thm)>
                                
                                    <cfif thumb NEQ ''>
                                        File Not Exists on Thumb:<cfdump var="#thumb#"><br>
                                        <cfset failed = true>
                                    </cfif>
                                    
                                </cfif>
                            
                            <cfelse>
                                File Ext is Missing on Thumb:<cfdump var="#thumb#"><br>
                                <cfset failed = true>
                            </cfif>
                        </cfif>
    
                    </cfif>
    
                </cfif>
               
            </cfoutput>
            
            <cfif failed IS false>
            
				<!--- All Assets Good :) --->
                
                <cfoutput query="importQuery">
                
                    <cfif path IS ''>
                        <cfset img = theFileZipLoc & image>
                        <cfset thm = theFileZipLoc & thumb>
                    <cfelse>
                        <cfset img = theFileZipLoc & path &"/"& image>
                        <cfset thm = theFileZipLoc & path &"/"& thumb>
                    </cfif>
                    
                    <cfif fileExists(img)> 
                        <cfinvoke component="CFC.Misc" method="getImageSpecs" returnvariable="imgSpec">
                            <cfinvokeargument name="imageSrc" value="#img#"/>
                        </cfinvoke>
                    <cfelse>
                        <cfset img = "">
                        <cfset image = "">
                        <cfset imgSpec = {"width":0,"height":0}>
                    </cfif>
                    
                    <cfset assetData = {"appID":appID, "name":name, "assetTypeID":typeID, "assetID":0, "image":{"file":image,"size":imgSpec, "src":img}}>
    
                    <cfset detailData = {"title":title, "subtitle":subtitle, "description":description, "titleColor":titlecolor, "subtitleColor":subtitlecolor, "descriptionColor":descriptioncolor, "other":""}>
                    
                    <cfif fileExists(thm)> 
                        <cfinvoke component="CFC.Misc" method="getImageSpecs" returnvariable="thmSpec">
                            <cfinvokeargument name="imageSrc" value="#thm#"/>
                        </cfinvoke>
                    <cfelse>
                        <cfset thm = "">
                        <cfset thumb = "">
                        <cfset thmSpec = {"width":0,"height":0}>
                    </cfif>
                    
                    <cfset thumbData = {"thumb":{"file":thumb, "size":thmSpec, "src":thm},"thumbnail":thumb}>
    
                    <cfset colorData = {}>
                    
                    <cfif importType IS 0>
                    
						<!--- New Asset --->
                        <cfinvoke component="CFC.Assets" method="InsertNewAsset" returnvariable="insertedAsset">
                            <cfinvokeargument name="assetData" value="#assetData#"/>
                            <cfinvokeargument name="detailData" value="#detailData#"/>
                            <cfinvokeargument name="thumbData" value="#thumbData#"/>
                            <cfinvokeargument name="colorData" value="#colorData#"/>
                            <cfinvokeargument name="groupAssetName" value="#path#"/>
                        </cfinvoke>
                    
                    <cfelse>
                    
						<!--- Replace Existing Asset --->
                        <cfinvoke component="CFC.Assets" method="ReplaceWithNewAsset" returnvariable="replacedAsset">
                            <cfinvokeargument name="assetData" value="#assetData#"/>
                            <cfinvokeargument name="detailData" value="#detailData#"/>
                            <cfinvokeargument name="thumbData" value="#thumbData#"/>
                            <cfinvokeargument name="colorData" value="#colorData#"/>
                            <cfinvokeargument name="groupAssetName" value="#path#"/>
                        </cfinvoke>
                    
                    </cfif>
                    
                </cfoutput>
 
            <cfelse>      
            	<cfset error = "Assets are not matching up to import list - please check XLS and files">
            </cfif>
            
        <cfelse>
        	   <cfset error = "Import XLS file missing">
        </cfif>
      

        <!--- delete original zip --->
        <cffile action="delete" file="#theFileLoc#">
        
        <!--- Delete all Files After Import --->
        <cfdirectory directory="#theFileZipLoc#" name="allFiles" action="LIST" />

		<cfoutput query="allFiles">
        
            <cfset theFile = theFileZipLoc & name>

            <cfinvoke component="CFC.File" method="getFileSpecs" returnvariable="fileSpecs">
                <cfinvokeargument name="fullPath" value="#theFile#"/>
            </cfinvoke>
            
            <cfif fileSpecs.file IS '.DS_Store'>
                <cffile action="delete" file="#theFile#">
            </cfif>
            
            <cfif fileSpecs.type IS 'file'>
            	<cffile action="delete" file="#theFile#">
            <cfelse>
            	<cfif directoryExists(theFile)>
                	<cfdirectory action="delete" directory="#theFile#" recurse="true">
                </cfif>
			</cfif>
  
        </cfoutput>
        
        <cfif directoryExists(theFileZipLoc)>
        	<cffile action="delete" file="#theFileZipLoc#">
        </cfif>
        <!---  --->
        <cfif error NEQ "ok">
        	<cfdump var="#error#"><cfabort>
        <cfelse>
			<!--- <cflocation url="../batchImport.cfm"> --->
        </cfif>
        
        <cfreturn error>
        
     </cffunction>   
    
</cfcomponent>