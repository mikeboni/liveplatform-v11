<cfparam name="assetID" default="0">
<cfparam name="optionID" default="0">
<cfparam name="viewDefaults" default="0">

<!--- configurator --->
<cfajaxproxy cfc="CFC.Configurator" jsclassname="configurator">

<script type="text/javascript">

	var jsConfig = new configurator();
	
	//save config material
	function saveConfig(assetID)
	{
		jsConfig.setSyncMode();
		jsConfig.setCallbackHandler(successConfigSaved);
		jsConfig.addMaterial(assetID, <cfoutput>#assetID#</cfoutput>);
	}
	
	function successConfigSaved(result)
	{
		location.reload();
	}
	
	//delete config material
	function deleteConfig(assetID)
	{
		jsConfig.setSyncMode();
		jsConfig.setCallbackHandler(successConfigDeleted);
		jsConfig.removeMaterial(assetID, <cfoutput>#assetID#</cfoutput>);
	}
	
	function successConfigDeleted(result)
	{
		location.reload();
	}
	
	//update config material
	function updateConfig(assetID, theData)
	{
		jsConfig.setSyncMode();
		jsConfig.setCallbackHandler(successConfigUpdated);
		jsConfig.updateMaterial(assetID, <cfoutput>#assetID#</cfoutput>, theData);
	}
	
	function successConfigUpdated(result)
	{
		//location.reload();
	}

	function addItemMaterial(obj,id) {
		
		//if(confirm('Are you sure you wish to Add this Material?'))
		//{
		//	console.log('add:'+id, obj);
			saveConfig(id);
			
		//}else{ 
			//nothing
		//}
		
	}
	
	//remove material
	function removeItemMaterial(obj,id) {
		
		var theForm = document.getElementById(id);
		
		var fn = obj.src.split("/")[obj.src.split("/").length-1];
	
		if(fn === "saveupdate.png")
		{
			obj.src = "images/remove.png"
			//do update image on callback
			
			theData = {
				"code": theForm.matCode.value, 
				"cost": theForm.matCost.value, 
				"default":theForm.defaultOption.value,
				"group":theForm.group[theForm.group.selectedIndex].value,
				"order":theForm.sortOrder[theForm.sortOrder.selectedIndex].value
				};
			//
			updateConfig(id,theData);
		
			//console.log('save:'+id);
			
			
		}else{
			
			if(confirm('Are you sure you wish to Delete this Material?'))
			{
				console.log('remove:'+id, obj);
				deleteConfig(id);
				
			}else{ 
				//nothing
			}
			
		}
		
	}
	
	function changedOptions(obj,id) {
		
		var btnObj = document.getElementById("materialState_"+id);
		btnObj.src = "images/saveupdate.png"
	
	}
	

</script>

<body>

<cfif assetID IS 0>

<cfinvoke component="CFC.Configurator" method="getConfigAssetsAvailable" returnvariable="theAssets">
    <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>  

<table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="#EEE" style="padding-top:5px">
  <tr>
    <td width="100" height="44" align="right">
      <p class="content">Spaces</p>
    </td>
    <td height="44"><span class="content">
    	<form name="project" action="AppsView.cfm">
        <input name="edit" type="hidden" id="edit" value="true">
        <input name="tab" type="hidden" id="edit" value="3">
      <cfoutput>
      <select name="assetID" class="sectionHeaderText" style="width:260px;padding-left:0px; padding-top:0px;margin-bottom: 10px">
          <cfloop query="theAssets">
            <option value="#asset_id#" <cfif assetID IS asset_id>selected</cfif>>#name#</option>
          </cfloop>
      </select>
      </cfoutput>
      <input name="button" type="submit" class="formfieldcontent" id="button" value="Open Space" />		
      </form>
    </span></td>
  </tr>
  <tr>
</table>

<cfelse>

    <cfinvoke component="CFC.Configurator" method="getOptions" returnvariable="options">
        <cfinvokeargument name="assetID" value="#assetID#"/>
    </cfinvoke>  
    
    <cfinvoke component="CFC.Configurator" method="getSpace" returnvariable="theSpace">
        <cfinvokeargument name="assetID" value="#assetID#"/>
    </cfinvoke>  


  <cfif optionID IS 0>
  	 <cfset optionID = options.materialID>
  </cfif>
  
  <cfif optionID IS ''>
    <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#FFF" class="contentLinkGrey" style="padding-bottom:10px">
      <tr>
        <td height="44" colspan="4" align="center" bgcolor="#66CC99">Material Asset Sturcture</td>
      </tr>
      <tr>
        <td colspan="4"><p>The following is the prefered structure for the materials. The current asset entered does not correspond to this structure. Make the following changes and try again.</p>
        <form action="AppsView.cfm">
        <input name="edit" type="hidden" id="edit" value="true">
        <input name="tab" type="hidden" id="edit" value="3">
        <input type="submit" value="Try Again" class="formfieldcontent" style="width:100px">
        </form>
      </tr>
      </table>
      <!--- structure help --->
    <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#FFF" class="contentLinkGrey material" style="border:thin solid #999">
      <tr>
        <td align="left" valign="top" bgcolor="#CCC">imageAsset</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top" bgcolor="#eee">Assets</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top">pointAsset -&gt;</td>
        <td align="left" valign="top" bgcolor="#CCC">pointAsset</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#eee">Actions</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">GroupAsset -&gt;</td>
        <td align="left" valign="top" bgcolor="#CCC">GroupAsset</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#eee">Assets</td>
        <td align="left" valign="top">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">imageAsset -&gt;</td>
        <td align="left" valign="top" bgcolor="#CCC">imageAsset</td>
      </tr>
      <tr>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">&nbsp;</td>
        <td align="left" valign="top">Image is Image View<br />
Thumb is Material Texture</td>
      </tr>
    </table>
    
  <cfelse>
  
    <cfinvoke component="CFC.Configurator" method="getMaterials" returnvariable="materials">
      <cfinvokeargument name="optionID" value="#optionID#"/>
      <cfinvokeargument name="contentID" value="#assetID#"/>
    </cfinvoke>

    <cfif viewDefaults>
        
        <cfinvoke component="CFC.Configurator" method="getMaxDefaultOptions" returnvariable="maxOptions">
          <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>

  	</cfif>
  	
  
<cfoutput>
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="##FFF">
<cfif viewDefaults>
	<tr>
    <td>
    <div class="sectionHeaderText" style="padding:0; width:100%; float:none; background-color:##EEE; margin-bottom:5px;">
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="##eee">
      <tr>
        <td height="54"><span style="margin-top:10px; margin-left:10px">Materials for &quot;#theSpace.title#&quot; Space</span></td>
        <td width="44" align="right"><a href="AppsView.cfm?edit=true&tab=3&assetID=#assetID#"><img src="images/remove.png" alt="" width="44" height="44" border="0" /></a></td>
    </tr>
    </table>
    </div>
    <cfloop index="theOption" from="1" to="#maxOptions#">
    
    <cfinvoke component="CFC.Configurator" method="getDefaultOptions" returnvariable="defaultOptions">
      <cfinvokeargument name="assetID" value="#assetID#"/>
      <cfinvokeargument name="defaultOption" value="#theOption#"/>
    </cfinvoke>
    <!--- display default summary --->
    <table width="100%" border="0" cellpadding="8" cellspacing="1" class="content" bgcolor="##AAA" style="margin-bottom:10px">
          <tr class="contentLinkWhite">
            <td colspan="5" bgcolor="##666">Default Option #theOption#</td>
            </tr>
               <cfloop query="defaultOptions">
               	<!--- get room name --->
                <cfinvoke component="CFC.Configurator" method="getRoomFromMaterial" returnvariable="theRoom">
                  <cfinvokeargument name="assetID" value="#asset_id#"/>
                </cfinvoke>
               
              <tr bgcolor="##fff" class="contentLinkGrey">
                <td width="200" align="left" valign="top" bgcolor="##fff" class="contentLinkDisabled">#theRoom.title#</td>
                <td width="200" align="left" valign="top" bgcolor="##fff">#title#</td>
                <td align="left" valign="top">#description#</td>
                <td width="100" align="center" valign="top" bgcolor="##fff">#code#</td>
                <td width="100" align="right" valign="bottom" bgcolor="##fff">#NumberFormat(cost,'___.__')#</td>
              </tr>
              </cfloop>
          </table>
          </cfloop>
   </td>
   </tr>
<cfelse>
  <tr>
    <td>
    <!--- options selection --->
        <div class="sectionHeaderText" style="padding:0; width:100%; float:none; background-color:##EEE; margin-bottom:5px">
            <form name="project" action="AppsView.cfm">
              <input name="assetID" type="hidden" id="assetID" value="#assetID#" />
              <input name="edit" type="hidden" id="edit" value="true">
        		<input name="tab" type="hidden" id="edit" value="3">
        		<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="##eee">
        		  <tr>
        		    <td width="44"><a href="AppsView.cfm?edit=true&tab=3&assetID=#assetID#&viewDefaults=1"><img src="images/options.png" alt="" width="44" height="44" border="0" /></a></td>
        		    <td height="44"><span style="margin-top:10px; margin-left:10px">Materials for &quot;#theSpace.title#&quot; Space</span></td>
                  <td height="40" align="right"><p style="margin-top:10px">
                    <select name="optionID" class="sectionHeaderText" id="optionID" style="width:260px;padding-left:0px; padding-top:0px;margin-bottom:10px; float:right" onChange="this.form.submit()">
                      <cfloop query="options">
                        <option value="#materialID#" <cfif optionID IS materialID>selected</cfif>>#title#</option>
                        <cfif optionID IS materialID>
                          <cfset materailName = title>
                        </cfif>
                      </cfloop>
                  </select>
                  </p></td>
                  <td width="44" align="right"><a href="AppsView.cfm?edit=true&tab=3"><img src="images/remove.png" alt="" width="44" height="44" border="0" /></a></td>
                </tr>
                </table>
            </form>
          </div>
		<!--- materials list --->
        <table width="100%" border="0" cellpadding="8" cellspacing="1" class="content" bgcolor="##AAA">
          <tr class="contentLinkWhite">
            <td width="200" bgcolor="##666">Name</td>
            <td width="44" align="center" bgcolor="##666">Order</td>
            <td bgcolor="##666">Description</td>
            <td width="40" align="center" bgcolor="##666">Option</td>
            <td width="100" align="center" bgcolor="##666">Code</td>
            <td width="40" align="center" bgcolor="##666">Group</td>
            <td width="155" align="center" bgcolor="##666">Cost</td>
            </tr>
          </table>
        <cfset groupLevel = 0>
        <cfloop query="materials">
          <div class="<cfif NOT connected>materialItem-disabled</cfif>" id="materialItem">
          <form id="#asset_id#">
            <table width="100%" border="0" cellpadding="5" cellspacing="1" class="material">
              <cfif grouping NEQ groupLevel>
              <tr>
              <td colspan="9" bgcolor="##999" class="contentGreyed">
              <cfif grouping IS 1>
              STANDARD
              <cfelse>
              LEVEL #grouping-1#
              </cfif>
              </td>
              </tr>
              <cfset groupLevel = grouping>
              </cfif>
              <tr>
                <td width="200" class="contentLinkGrey" style="padding-left:5px">
               		<span class="contentLinkGrey"><a href="http://liveplatform.net/API/v11/AssetsView.cfm?assetID=#asset_id#&assetTypeID=#assetType#" class="contentLink">#title#</a></span>
                  </td>
                <td width="44" align="center" class="contentLinkGrey" style="padding-left:10px">
                <!--- sort order --->
                <cfset sortOrder = 0>
                <select name="defaultSortOrder" class="contentLinkGrey" id="sortOrder" style="width:25px;padding-left:0px; padding-top:0px; text-indent:2px; float:none" onChange="changedOptions(this.form,#asset_id#);">
                   <option value="0" <cfif sortOrder IS 0>selected</cfif>>-</option>
                   <cfset maxVal = 8>
                   <cfloop index="z" from="1" to="#maxVal#">
                   <option value="#z#" <cfif sortOrder IS z>selected</cfif>>#z#</option>
                   </cfloop>
                 </select>
                </td>
                <td class="contentLinkGrey" style="padding-left:10px">
                	<span class="contentLinkGrey">#description#</span>
                </td>
                <td width="40" align="center" class="contentLinkGrey">
                <!--- option --->
                <cfif connected>
                 <select name="defaultOption" class="contentLinkGrey" id="optionID" style="width:25px;padding-left:0px; padding-top:0px; text-indent:2px; float:none" onChange="changedOptions(this.form,#asset_id#);">
                   <option value="0" <cfif default IS 0>selected</cfif>>-</option>
                   <cfset maxVal = 5>
                   <cfloop index="z" from="1" to="#maxVal#">
                   <option value="#z#" <cfif default IS z>selected</cfif>>#z#</option>
                   </cfloop>
                 </select>
                </cfif>
                </td>
                <td width="100" align="center" class="contentLinkGrey">
                <!--- code --->
                <cfif NOT connected>
                	NA
                	<cfelse>
                	<input name="matCode" type="text" class="formfieldcontent" id="matCode" style="width:100px; font-size:14px; border:hidden; text-align:center" value="#trim(code)#" onChange="changedOptions(this.form,#asset_id#)" />
                </cfif>
                </td>
                <td width="40" align="center" class="contentLinkGrey">
                <!--- Grouping --->
                <cfif connected>
                 <select name="group" class="contentLinkGrey" id="group" style="width:25px;padding-left:0px; padding-top:0px; text-indent:2px; float:none" onChange="changedOptions(this.form,#asset_id#);">
                 <option value="0" <cfif grouping IS 0>selected</cfif>>-</option>
                 <cfset maxVal = 10>
                 <cfloop index="z" from="1" to="#maxVal#">
                 	<option value="#z#" <cfif grouping IS z>selected</cfif>>#z#</option>
                 </cfloop>
                 </select>
                </cfif>
                </td>
                <td width="100" align="right" class="contentLinkGrey">
                <!--- cost --->
                <cfif NOT connected>
                  NA
                  <cfelse>
                	<input name="matCost" type="text" class="formfieldcontent" id="matCost" style="width:100px; font-size:14px; border:hidden; text-align:right" value="#cost#" onChange="changedOptions(this.form,#asset_id#)" />
                </cfif>
                </td>
                <td width="40">
                <cfif NOT connected>
                	<img src="images/include.png" name="materialState" id="materialState_#asset_id#" onClick="addItemMaterial(this,#asset_id#)" />            
                <cfelse>
                	<img src="images/remove.png" name="materialState" id="materialState_#asset_id#" onClick="removeItemMaterial(this,#asset_id#)" />
                </cfif>
                </td>
                </tr>
              </table>
          </form>
          </div>
        </cfloop>

    </td>
  </tr>
  </cfif>
</table>
</cfoutput>
</cfif>

</cfif>