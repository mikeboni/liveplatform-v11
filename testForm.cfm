<cfparam name="email" default="yes">

<style type="text/css">
	.bodytext {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 11pt;
		line-height: 14pt;
	}
	.subheading {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 12pt;
		color: #333;
		line-height: 18px;
	}
	
	.heading {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 18pt;
		color: #222;
	}
	.smHeading {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 14pt;
	}
	
	.state {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12pt;
	color: #FFF;
	background-color: #CD2234;
	padding-top: 8px;
	padding-right: 10px;
	padding-bottom: 7px;
	padding-left: 10px;
	text-transform: uppercase;
	float:left;
	margin-top:10px
	}
	.fieldName {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14pt;
	float:left;
	width:20%; 
	text-align:right; 
	padding-top:2px;
	}
	.fieldContent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14pt;
	float:left;
	text-align:left; 
	width:75%;
	padding-left:4px;
	padding-top:2px;
	}
	
	.barHeading {
	background:#666; 
	float:left; 
	height:42px; 
	width:600px; 
	line-height: 40px; 
	padding-left:5px;
	color:#FFF
	}
	
	.barSmallHeading {
	font-size: 14pt;
	color:#FFF; 
	padding-left:5px
	}
	
	.noteText {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 8pt;
		color: #333;
		line-height: 18px;
		text-align:center;
	}
	.materialText {
		font-family: Arial, Helvetica, sans-serif;
		font-size: 8pt;
		color: #333;
		text-align:center;
		text-transform:uppercase
	}
	
	.barTableHead {
		font-size: 11pt;
		color: #666;
		line-height: 18px;
		text-align:left;
		text-transform:uppercase
	}
	
	ul li{
		margin-bottom:10px;
	}
	
	.page-break	{ page-break-before: always; }
	
	@media print and (color) {
	  * {
		-webkit-print-color-adjust: exact; 
		print-color-adjust: exact;
	  }
	}
	
</style>

<cfparam name="unitID" default="3">

<cfset page = 1>
<cfset pageCount = 0>

<!--- get all config data --->
<cfinvoke component="CFC.Configurator" method="getUnitConfiguration" returnvariable="theConfiguration">
    <cfinvokeargument name="unitID" value="#unitID#"/>
</cfinvoke>

    
   <!--- get all config data --->
    <cfinvoke component="CFC.Configurator" method="createRoomImage" returnvariable="theRoomImage">
    	<cfinvokeargument name="baseImage" value="#theConfiguration.rooms.kitchen.base#"/>
        <cfinvokeargument name="optionImages" value="#theConfiguration.rooms.kitchen.default#"/>
    </cfinvoke>


    <div style="width:60%; margin-top:10px; float:left; border:thin; border-style:solid; border-color:##333">
        <cfimage source="#theRoomImage#" action="writeToBrowser" width="100%" height="">
    </div>
    
    
    <!--- get all config data --->
    <cfinvoke component="CFC.Configurator" method="createRoomImage" returnvariable="theRoomImage">
    	<cfinvokeargument name="baseImage" value="#theConfiguration.rooms.kitchen.base#"/>
        <cfinvokeargument name="optionImages" value="#theConfiguration.rooms.kitchen.selection#"/>
    </cfinvoke>
    
    <div style="width:60%; margin-top:10px; float:left; border:thin; border-style:solid; border-color:##333">
        <cfimage source="#theRoomImage#" action="writeToBrowser" width="100%" height="">
    </div>
    
    
    <cfdump var="#theConfiguration#">



<cfabort>
    
    <!--- PAGE --->
    <div style="width:100%; padding:0 0 0 0; height:960px; margin-top:20px; min-width:985px;">
    
    <cfoutput>	
    
		<cfif email AND pageCount GT 0>
            <!--- nothing --->
        <cfelse>
        
            <!---header--->
            <div style="height:72px">
                  
                  <!---Project information--->
                    <div style="width:50%; float:left; padding:0px 0px 0px 5px;" class="heading">
                      <b>#info.project#</b> - #info.unit# <br />
                      <span class="smHeading">#info.group# - #info.type# (#info.code#)</span><br />
                      <div class="state">
                      <cfif info.state IS 0>PENDING<cfelse>CLOSED</cfif>
                      </div>
                    </div>
                    
                  <!--- date, inv# --->
                  <div class="subheading" style="width:40%; text-align:right; float:right">
                        <b>#dateFormat(info.modified,'DDD, MMM, DD/YY')#</b><br />
                  N&deg; #info.ref#
                  <cfif NOT email>
                      <p class="noteText" style="text-align:right">
                      PAGE #page#
                      </p>
                  </cfif>
                  </div>
                    
              </div>
            <cfset pageCount++>
  
                <!---2nd line of details--->
                <div style="height:auto; margin-top:20px">
              
                    
                    
                    <!---client information --->
                    <div class="subheading" style="width:50%; height:80px; float:left; padding:5px 0px 5px 5px; border:thin; border-style:solid; border-color:##999">
                    
                        <span class="fieldName"><b>Name:</b></span><span class="fieldContent">#cust.name#</span>
                        <span class="fieldName"><b>Address:</b></span><span class="fieldContent">#cust.company_address#<br />
                        </span>
                        
                        <span class="fieldName"><b>E-mail:</b></span><span class="fieldContent">#cust.email#</span>
                        <span class="fieldName"><b>Phone:</b></span><span class="fieldContent">#cust.companyphone#</span>
                    
                    </div>
                      
               </div>
     
     	</cfif>
     
    </cfoutput>   
      
      <!---Space Details--->
      <div style="float:left; margin-top:32px; width:100%">
          
          
          
          
          <cfoutput>
          <!---space title--->
          <div class="heading" style="float:left; width:100%; height:44px; background-color:##666; padding-left:5px; padding-top:10px; margin-top:30px; color:##FFF">
            #theSpace.title#
          </div>
          
          <cfset optionImages = structNew()>
      
    	  <cfloop collection="#custSelection[theRoom]#" item="theOption">
          
			  <cfset theMaterial = custSelection[theRoom][theOption]>

        
              <cfinvoke component="CFC.Configurator" method="getOptions" returnvariable="options">
                <cfinvokeargument name="optionID" value="#theOption#"/>
              </cfinvoke>  
 
              <cfinvoke component="CFC.Configurator" method="getMaterials" returnvariable="material">
                <cfinvokeargument name="optionID" value="#theOption#"/>
                <cfinvokeargument name="assetID" value="#theMaterial#"/>
              </cfinvoke>

              <cfinvoke component="CFC.Modules" method="getContentAsset" returnvariable="asset">
                <cfinvokeargument name="assetID" value="#material.asset_id#"/>
              </cfinvoke>
               
              <!--- get reference image --->
              <cfloop collection="#asset#" item="theKey"></cfloop>
   
              <cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="materialDetails">
                <cfinvokeargument name="query" value="#material#"/>
              </cfinvoke>

              <cfset theImage = asset[theKey].image>
              <cfset structAppend(theImage,{"thumb":asset[theKey].thumb.xdpi.url})>
              <cfset structAppend(theImage,{"option":options.title})>
              <cfset structAppend(theImage,materialDetails)>
              <cfset structAppend(optionImages,{"#theImage.asset_id#":theImage})>
              
              <!--- Get Default Option for Space --->
              <cfinvoke component="CFC.Configurator" method="getDefaultOptions" returnvariable="theOptions">
                  <cfinvokeargument name="assetID" value="#theSpace.asset_id#"/>
                  <cfinvokeargument name="defaultOption" value="1"/>
              </cfinvoke>
   
              <cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="defaultOptions">
                <cfinvokeargument name="query" value="#theOptions#"/>
                <cfinvokeargument name="forceArray" value="true"/>
              </cfinvoke>
              
              <!--- Option Defaults --->
			  <cfset allDefaultOptions = structNew()>

              <cfset cnt = 1>
              <cfloop index="defaultOption" array="#defaultOptions#">
              
                  <!--- get asset details --->
                  <cfinvoke component="CFC.Modules" method="getContentAsset" returnvariable="asset">
                    <cfinvokeargument name="assetID" value="#defaultOption.asset_id#"/>
                  </cfinvoke>
                  
                  <!--- get room name --->
                  <cfinvoke component="CFC.Configurator" method="getRoomFromMaterial" returnvariable="theOptionTitle">
                    <cfinvokeargument name="assetID" value="#defaultOption.asset_id#"/>
                  </cfinvoke>
               
                  <!--- get reference image --->
                  <cfloop collection="#asset#" item="theKey"></cfloop>
                  <cfset data = asset[theKey]>
                  
                  <cfset theDefaultOptionData = {"thumb":"", "code":"", "cost":"", "description":"", "material":"", "option":theOptionTitle.title}>
    
                  <cfif structKeyExists(data,"thumb")>
                    <cfset structAppend(theDefaultOptionData,{"thumb":data.thumb.mdpi.url})>
                  </cfif>
                  <cfif structKeyExists(defaultOptions[cnt],"code")>
                    <cfset theDefaultOptionData.code = defaultOptions[cnt].code>
                  </cfif>
                  <cfif structKeyExists(defaultOptions[cnt],"cost")>
                    <cfset theDefaultOptionData.code = defaultOptions[cnt].cost>
                  </cfif>
                  
                  <cfif structKeyExists(data,"details")>
                    <cfif structKeyExists(data.details,"description")>
                        <cfset theDefaultOptionData.description = data.details.description>
                    </cfif>
                    <cfif structKeyExists(data.details,"title")>
                        <cfset theDefaultOptionData.material = data.details.title>
                    </cfif>
                  </cfif>
                  
                  <cfset structAppend(allDefaultOptions,{"#defaultOptions[cnt].asset_id#":theDefaultOptionData})>
                
                  <cfset cnt++>
                  
              </cfloop>

              <!--- create composite image --->
              <cfloop collection="#optionImages#" item="theFirstIMG"></cfloop>
            
              <cfimage source="#optionImages[theFirstIMG].url.mdpi#" name="compImage">
                
              <cfset finalImage = ImageNew("",compImage.width,compImage.height,"argb")>
              <cfset ImageDrawRect(finalImage,0,0,compImage.width,compImage.height,"yes")> 
                
              <!--- get a layer --->
              <cfloop collection="#optionImages#" item="anIMG">
                  <cfimage source="#optionImages[anIMG].url.mdpi#" name="anImage">
                  <cfset ImageOverlay(finalImage,anImage)>
              </cfloop>
                
              <!--- <cfset ImageSetDrawingColor(finalImage,"333333")> 
              <cfset ImageDrawRect(finalImage,0,0,compImage.width-1,compImage.height-1,"no")> --->
          
          </cfloop>
          
          <cfset StructSort( optionImages, "textnocase", "asc", "option" )> 
          <cfset StructSort( allDefaultOptions, "textnocase", "asc", "option" )> 
 
          <!---comp image of room--->
          <div style="width:60%; margin-top:10px; float:left; border:thin; border-style:solid; border-color:##333">
                <!--- <cfimage source="#finalImage#" action="resize" width="380" height="253" name="theRoomImg">  --->
                <cfimage source="#finalImage#" action="writeToBrowser" width="100%" height="">
          </div>
              
          <!---options selected, chips--->
          <div style="width:39%; height:auto; margin-top:10px; margin-left:5px; float:right" class="noteText">
              
              <div class="barTableHead" style="text-align:left; width:100%; color:##000">Selection</div>
        			
                <cfloop collection="#optionImages#" item="anOption">
                        <!--- Material Item --->
                  <div style="float:left; padding-right:4px; margin-top:5px">
                            
                    <div class="materialText" style="color:##FFF; background-color: ##333;">							
                    #optionImages[anOption].option#
                    </div>
                    <img src="#optionImages[anOption].thumb#" width="80" height="57" alt="#optionImages[anOption].title#" style="border:thin; border-color:##666; border-style:solid" />
                   <div class="materialText">#optionImages[anOption].title#</div> </div>
                </cfloop>
            
              </div>
        
          </div>
          </cfoutput>

          <!---Details--->
          <div style="float:left; width:100%;">
        	  
              <!--- Standard --->
              <div style="padding-top:20px">
              
              <table width="100%" border="0" cellpadding="4" cellspacing="1" class="bodytext" bgcolor="#333">
              <tr>
                <td height="32" colspan="4" bgcolor="#888" class="barSmallHeading">Standard Options</td>
                </tr>
              <tr class="barTableHead">
                <td width="200" height="32" bgcolor="#DDD" class="barTableHead">OPTION</td>
                <td width="80" align="center" bgcolor="#DDD">CODE</td>
                <td width="200" bgcolor="#DDD">MATERIAL</td>
                <td bgcolor="#DDD">DESCRIPTION</td>
              </tr>
              
              <!---line item--->
			  <cfoutput>
              <cfloop item="defaultItem" collection="#allDefaultOptions#">

                <tr class="bodytext">
                    <td height="32" valign="top" bgcolor="##FFF" style="padding-top:10px">#allDefaultOptions[defaultItem].option#</td>
                    <td align="center" valign="top" bgcolor="##FFF" style="padding-top:10px">#allDefaultOptions[defaultItem].code#</td>
                    <td valign="top" bgcolor="##FFF" style="padding:8px">
                    
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="20" valign="top">
                        <img src="#allDefaultOptions[defaultItem].thumb#" alt="" name="" width="30" height="22" style="border:thin; border-color:##666; border-style:solid" />
                        </td>
                        <td class="bodytext" style="padding-left:5px">#allDefaultOptions[defaultItem].material#</td>
                      </tr>
                    </table>
                    
                    </td>
                    <td valign="top" bgcolor="##FFF" style="padding-top:10px">#allDefaultOptions[defaultItem].description#</td>
                  </tr>
                  
              </cfloop>
              </cfoutput>
              
              </table>
              
              </div>
              
              <!--- filter defaults --->
               <cfloop collection="#allDefaultOptions#" item="assetID">
                    <cfset StructDelete(optionImages,assetID,false)>
               </cfloop>
                
               <cfset subTotal = 0> 
               
               <cfif structCount(optionImages) GT 0>
        
            <!--- upgrades --->
              <div style="padding-top:20px; float:left; width:100%">
              
               <table width="100%" border="0" cellpadding="4" cellspacing="1" class="bodytext" bgcolor="#333">
                      <tr>
                        <td height="32" colspan="5" bgcolor="#888" class="barSmallHeading">Customer Request for Extras (Upgrades)</td>
                        </tr>
                      <tr class="barTableHead">
                        <td width="200" height="32" bgcolor="#DDD" class="barTableHead">OPTION</td>
                        <td width="80" align="center" bgcolor="#DDD">CODE</td>
                        <td width="200" bgcolor="#DDD">MATERIAL</td>
                        <td bgcolor="#DDD">DESCRIPTION</td>
                        <td width="100" align="right" bgcolor="#DDD">COST</td>
                      </tr>
                    
                    
                   <!--- Line Item --->
                   <cfoutput>
                   
                   <cfloop item="anItem" collection="#optionImages#">

                      <tr class="bodytext">
                        <td height="32" valign="top" bgcolor="##FFF" style="padding-top:10px;">#optionImages[anItem].option#</td>
                        <td align="center" valign="top" bgcolor="##FFF" style="padding-top:10px;">#optionImages[anItem].code#</td>
                        <td valign="top" bgcolor="##FFF" style="padding:8px;">
                        
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="20">
                            <img src="#optionImages[anItem].thumb#" alt="" name="" width="30" height="22" style="border:thin; border-color:##666; border-style:solid" />
                            </td>
                            <td class="bodytext" style="padding-left:5px">#optionImages[anItem].title#</td>
                          </tr>
                        </table>
                        
                        </td>
                        <td valign="top" bgcolor="##FFF" style="padding-top:10px;">#optionImages[anItem].description#</td>
                        <td align="right" valign="top" bgcolor="##FFF" style="padding-top:10px;">#numberFormat(optionImages[anItem].cost,'__,__.__')#</td>
                      </tr>
                    <cfset subTotal+= optionImages[anItem].cost>
        			</cfloop>
                    </cfoutput>
                    
                    <!---subtotal--->
                    <cfoutput>
                    <tr class="bodytext">
                      <td height="44" colspan="4" align="right" valign="top" bgcolor="##FFF" style="padding-top:12px;"><b>Subtotal</b></td>
                      <td align="right" valign="top" bgcolor="##FFF" style="padding-top:12px;"><b>#numberFormat(subTotal,'__,__.__')#</b></td>
                    </tr>
                    </cfoutput>
                    
             </table>  
             
              </div>
              
              <cfelse>
              <!--- no upgrades --->
              </cfif>
        
          </div>
    
      </div>
    
    </div>

<cfif NOT email>
    <div class="page-break"></div>
    <cfset page++>
</cfif>
    
<!--- </cfloop> --->

<!--- 
<!---Summary--->
<div style="width:816px; padding:48px; height:1056px">

    <!---Summary Details--->
    <div class="barHeading heading" style="margin-bottom:10px">
      Summary 
    </div>
    
    <!--- Notes --->
    <div style="padding-top:20px; width:100%">
    <table width="100%" border="0" cellpadding="5" cellspacing="1" class="bodytext" bgcolor="#333">
    <tr>
      <td height="32" bgcolor="#888" class="barSmallHeading">Notes</td>
      </tr>
    <tr class="bodytext">
      <td height="100" valign="top" bgcolor="#FFF">&nbsp;</td>
      </tr>
    </table>
    </div>
    
    <!--- space upgrades subtotals breakdown --->
    <div style="padding-top:10px; width:100%">
    <table width="100%" border="0" cellpadding="4" cellspacing="1" class="bodytext" bgcolor="#333">
    <tr>
    <td height="32" colspan="5" bgcolor="#888" class="barSmallHeading">Configured Spaces</td>
    </tr>
    
    <!---Spaces--->
    <tr class="bodytext">
      <td width="693" height="32" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0">Kichen</td>
      <td width="100" align="right" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0">1,200.00</td>
    </tr>
    <tr class="bodytext">
      <td width="693" height="32" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0">Bathroom</td>
      <td width="100" align="right" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0">1,200.00</td>
    </tr>
    <tr class="bodytext">
      <td width="693" height="32" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0">Living Area</td>
      <td width="100" align="right" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0">1,200.00</td>
    </tr>
    
    <tr class="bodytext">
      <td colspan="2" align="right" valign="top" bgcolor="#EEE" style="padding-top:12px; padding-bottom:0">&nbsp;</td>
      </tr>
      
      <!---Totals--->
    <tr class="bodytext">
      <td height="32" align="right" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0">Subtotal</td>
      <td align="right" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0">12,112.00</td>
    </tr>
    <tr class="bodytext">
      <td height="32" align="right" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0">Total HST</td>
      <td align="right" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0">5,232.00</td>
    </tr>
    <tr class="bodytext">
      <td height="32" align="right" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0"><b>Grand Total</b></td>
      <td align="right" valign="top" bgcolor="#FFF" style="padding-top:12px; padding-bottom:0"><b>22,000.00</b></td>
    </tr>
    
    </table>
    </div>
    
    <!---terms and conditions--->
    <div style="padding-top:20px; width:100%">
     <table width="100%" border="0" cellpadding="4" cellspacing="1" class="bodytext" bgcolor="##333">
    <tr>
      <td colspan="2" height="32" bgcolor="#888" class="barSmallHeading">Terms and Conditions</td>
      </tr>
    <tr class="noteText" style="text-align:left">
      <td width="50%" align="left" valign="top" bgcolor="#FFF">
     <ul style="padding-left: 20px; padding-right:10px">
      <li>
      The Purchaser acknowledges having compared their selections to the samples provided and understands that dye lots on samples may vary and that the above selections are final and no changes are possible when this form has been signed by the purchaser.<br />
       </li>
       <li>
       In the case of a discontinued item, or an item that is not available to be installed prior to the closing date, the Purchaser will be notified immediately and asked to re-select within forty-eight (48) hours. If no re-selection has been made within that time frame, it is at the Vendor’s discretion to re-select on behalf of the Purchaser, in accordance with the terms of the Agreement of Purchase and Sale.<br />
       </li>
        </ul>
      </td>
      <td width="50%" align="left" valign="top" bgcolor="#FFF">
     <ul style="padding-left: 20px; padding-right:10px">
       <li>
       Items selected above which have not been accepted in writing by the Vendor and do not form part of the Agreement of Purchase and Sale, any Schedules or Request for Extras form, will be rejected.<br />
        </li>
        <li>
        Upgraded selections include an internal credit for standard selections. The Vendor will not supply any standard selection materials when an upgrade has been selected. The Vendor will not supply only any of the above selections.
        </li>
        </ul>
      </td>
      </tr>
    <tr class="noteText" style="text-align:left">
      <td colspan="2" align="left" valign="top" bgcolor="#FFF">
      
      <table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td colspan="2" class="subheading">I HEREBY CONFIRM THAT I HAVE READ ALL THE INFORMATION CONTAINED IN THIS DOCUMENT INCLUDING THE TERMS AND CONDITIONS LISTED ABOVE</td>
            </tr>
            <tr>
            <td height="20" colspan="2" class="subheading">&nbsp;</td>
            </tr>
          <tr>
            <td>
            
            <!--- sig purchase --->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="noteText">
              <tr>
                <td width="70" align="right">Purchaser: <b>X</b></td>
                <td><div style="padding-top: 10px;"><hr size="1" /></div></td>
              </tr>
            </table>
            
            </td>
            <td>
            
             <!--- sig vwndor --->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="noteText">
              <tr>
                <td width="70" align="right">Vendor: <b>X</b></td>
                <td><div style="padding-top: 10px;"><hr size="1" /></div></td>
              </tr>
            </table>
            
            </td>
          </tr>
          <tr>
            <td>
            
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="noteText">
              <tr>
                <td width="300" align="center">Signature</td>
                <td align="center">Date</td>
              </tr>
            </table>
            
            </td>
            <td>
            
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="noteText">
              <tr>
                <td width="300" align="center">Signature</td>
                <td align="center">Date</td>
              </tr>
            </table>
            
            </td>
          </tr>
        </table>
        
        </td>
      </tr>
     </table>
    </div>

</div> --->
