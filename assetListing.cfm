<cfparam name="assetGroupID" default="0">
<cfparam name="assetTypeID" default="0">
<cfparam name="subgroupID" default="-1">
<cfparam name="filter" default="0">
<cfparam name="action" default="">

<cfparam name="order" default="0">

<cfset curOrder = order>
<cfif order IS 0><cfset order=1><cfelseif order IS 1><cfset order=2><cfelse><cfset order=0></cfif>

<cfparam name="AssetLibAppID" default="#session.appID#">

<link href="styles.css" rel="stylesheet" type="text/css">

<style>
.center-cropped {
  object-fit:cover ; /* Do not scale the image */
  object-position: center; /* Center the image within the element */
  height: 32px;
  width: 32px;
}
</style>

<!--- GroupedAssets --->
<cfajaxproxy cfc="CFC.GroupedAssets" jsclassname="groupedAssets">

<script type="text/javascript">

var jsGroupedAssets = new groupedAssets();

function setCheckmarkState(theObj,className)	{
	
	objClass = theObj.className;
	
	if(objClass === className+'Selected') {
		theObj.className = className+'Normal';
	}else{
		theObj.className = className+'Selected';
	}
	
}


function getAllSelected()	{

	var theSelObjs = document.getElementsByClassName("checkmarkSelected");
	var allSelected = [];
	
	for(i=0; i < theSelObjs.length; i++)	{
		allSelected.push(theSelObjs[i].name);
	}
	
	return allSelected
	
}

function getAllSelectedGroups()	{

	var theSelObjs = document.getElementsByClassName("checkmarkGrpsSelected");
	var allSelected = [];
	
	for(i=0; i < theSelObjs.length; i++)	{
		allSelected.push(theSelObjs[i].name);
	}
	
	return allSelected
	
}

function moveGroup(groupID)	{
	if(confirm('Are you sure you wish to Move the Selected Asset(s)?'))
	{
	var selectedAssets = getAllSelected();
	var selectedGroups = getAllSelectedGroups();
	
	jsGroupedAssets.setSyncMode();
	jsGroupedAssets.setCallbackHandler(moveGroupSucess);
	jsGroupedAssets.moveAllGroupAssets(groupID, selectedAssets, selectedGroups);
	
	}else{
		//nothing
	}
}

function moveGroupSucess(result) {
		location.reload();
}

function syncAllAssets()	{
	
	if(confirm('Are you sure you wish to Sync All Assets?'))
	{
		jsGroupedAssets.setSyncMode();
		jsGroupedAssets.setCallbackHandler(syncAssetsSucess);
		jsGroupedAssets.syncAssets(<cfoutput>#session.appID#</cfoutput>);
		
	}else{ 
		//nothing
	}
	
}

function syncAssetsSucess(result) {
		location.reload();
}


//delete assets and groups

function deleteSelected()	{
	
	if(confirm('Are you sure you wish to Delete the Selected Asset(s)?'))
	{
		var allSelected = getAllSelected();
		var allGroupsSelected = getAllSelectedGroups();
	
		jsGroupedAssets.setSyncMode();
		jsGroupedAssets.setCallbackHandler(deleteAllSelectedSucess);
		jsGroupedAssets.deleteAssets(<cfoutput>#session.appID#</cfoutput>, allSelected, allGroupsSelected);
		
	}else{ 
		//nothing
	}
	
}

function deleteAllSelectedSucess(result) {
		location.reload();
}


function createNewAsset(assetType)	{
	
	//location.href = "Asset.cfm?assetTypeID="+ assetType; //New assets
	location.href = "updateAsset.cfm?assetTypeID="+ assetType + "&subgroupID=" + <cfoutput>#subgroupID#</cfoutput>; //Old assets

}


function createNewGroup(state)	{
	
	var theGroup = document.getElementById("newGroup");
	var theState;
	
	if(state == undefined)	{
		//current state
	 	theState = theGroup.style.visibility;
		
		if(theState == "hidden")	{
			theGroup.style.visibility = "visible";
		}else{
			theGroup.style.visibility = "hidden";
		}
		
	}else{
		//controlled state
		theGroup.style.visibility = state;
		
	}
	
}


function editGroupName(groupID)	{
	
	var theGroupOld = document.getElementById("folder_"+groupID);
	var theGroupNew = document.getElementById("folderName_"+groupID);
	
	theGroupOld.style.display = "none";
	theGroupNew.style.display = "block";
}

function cancelAssetGroup(groupID)	{
	
	var theGroupOld = document.getElementById("folder_"+groupID);
	var theGroupNew = document.getElementById("folderName_"+groupID);
	
	theGroupOld.style.display = "block";
	theGroupNew.style.display = "none";
}


function updateAssetGroup(theGroupID)	{
	
	var theGroup = document.getElementById("groupAssetName_"+theGroupID).value;
	
	if(theGroup === '')	{
		
		cancelAssetGroup(theGroupID);
		alert("A Name for the Group must be specified");
		
	}else{
		theGroupName = theGroup;
		
		jsGroupedAssets.setSyncMode();
		jsGroupedAssets.setCallbackHandler(updateAssetGroupSucess);
		jsGroupedAssets.updateAssetGroup(theGroupID, theGroupName);
	}
	
}

function updateAssetGroupSucess(result) {
		location.reload();
}


function importAssetsToContent()	{
		
		var allSelected = getAllSelected();
		var allGroupsSelected = getAllSelectedGroups();
		
		jsGroupedAssets.setSyncMode();
		
		<cfif isDefined('session.subgroupid') OR isDefined('session.assetid')>
		
			<cfif session.subgroupid GT 0>
				<!--- import into content group --->
				jsGroupedAssets.setCallbackHandler(importedAssetsSucess);
				jsGroupedAssets.importAllAssetsIntoGroup(<cfoutput>#session.appID#</cfoutput>,<cfoutput>#session.subgroupID#</cfoutput>, allSelected, allGroupsSelected);
			</cfif>
		
			<cfif session.assetID GT 0>
				<!--- import into assets - actions or assets --->
				<cfif action NEQ ''>
				jsGroupedAssets.setCallbackHandler(importedAssetsSucess);
				jsGroupedAssets.importAssetAssetsActions(<cfoutput>#session.assetID#</cfoutput>, allSelected, allGroupsSelected,<cfoutput>#action#</cfoutput>);
				</cfif>
			</cfif>
			
		</cfif>
	
}

function importedAssetsSucess(result)	{	

		var refreshLink = '';
		
		if(result.assetID == undefined)	{
			refreshLink = "AppsView.cfm?subgroupID="+result.groupID+"&error="+'Imported ('+ result.total +') Assets';
		}else{
			refreshLink = "AssetsView.cfm?assetID="+result.assetID+"&assetTypeID="+result.assetTypeID;
		}
		location.href = refreshLink;
}


function viewSubgroup(subgroupID)	{ 
	location.href = "AppsView.cfm?assetTypeTab=3&subgroupID=" + subgroupID;
}

function viewAssets(appID)	{ 
	location.href = "AppsView.cfm?assetTypeTab=3&AssetLibAppID=" + appID;
}


function displayPath(groupID)	{
	
	jsGroupedAssets.setSyncMode();
	jsGroupedAssets.setCallbackHandler(updateDisplayPath);
	jsGroupedAssets.getAssetGroupPaths(groupID);
	
}

function updateDisplayPath(result)	{
//	console.log(result);
	var theObj = document.getElementById(result.id+"_assetPath");
	theObj.innerHTML = result.text;
	
}

function verifyGroupPath(id, path)	{

	var theObj = document.getElementById(id);
	
	jsGroupedAssets.setSyncMode();
	jsGroupedAssets.setCallbackHandler((result) => {
		console.log(result, theObj)
	});
	jsGroupedAssets.groupPathVerified(<cfoutput>#session.appID#</cfoutput>, path);
	
}

</script>


<cfinvoke  component="CFC.Misc" method="QueryToStruct" returnvariable="allTypes">
    <cfinvokeargument name="query" value="#session.AssetsTypes#"/>
    <cfinvokeargument name="forceArray" value="true"/>
</cfinvoke>

<cfif subgroupID GTE 0>

    <cfinvoke component="CFC.GroupedAssets" method="getAssetsAndGroups" returnvariable="assets">
      <cfinvokeargument name="appID" value="#AssetLibAppID#"/>
      <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
      <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
      <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
      <cfinvokeargument name="search" value="#search#"/>
      <cfinvokeargument name="order" value="#curOrder#"/>
    </cfinvoke>

<cfelse>

    <cfinvoke component="CFC.GroupedAssets" method="getAssetsAndGroups" returnvariable="assets">
      <cfinvokeargument name="appID" value="#AssetLibAppID#"/>
      <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
      <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
      <cfinvokeargument name="search" value="#search#"/>
      <cfinvokeargument name="order" value="#curOrder#"/>
    </cfinvoke>

</cfif>

<cfinvoke  component="CFC.GroupedAssets" method="getAllAssetGroups" returnvariable="allGroups">
    <cfinvokeargument name="appID" value="#AssetLibAppID#"/>
    <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
    <cfinvokeargument name="orderByName" value="true"/>
</cfinvoke>


<cfinvoke  component="CFC.Assets" method="getUnusedAssets" returnvariable="unusedAssets">
    <cfinvokeargument name="appID" value="#AssetLibAppID#"/>
</cfinvoke>



<div style="width:805px;">

<cfoutput>
		
    <div class="contentLinkGrey" style="width:810px; height:auto; margin-top:2px; background-color:##DDD; float:left">
        <cfinvoke  component="CFC.GroupedAssets" method="getBreadcrumb" >
            <cfinvokeargument name="groupID" value="#subgroupID#"/>
          </cfinvoke> 
        <div style="float:right; width:auto;">
        	<div style="float:left; width:auto;visibility:hidden" id="newGroup">
            	
                <div style="padding-top:12px; padding-left:5px; padding-right:10px; width:auto; float:left">
                New Group:
                </div>
                <div style="float:left; width:auto; padding-top:5px">
                <input name="groupName" id="groupName" type="text" class="formText" style="width:200px">
                </div>
                <div style="float:left; width:auto;">
                <img src="images/saveupdate.png" style="cursor:pointer;vertical-align: sub" onClick="createAssetGroup()" />
                </div>
                
            </div>
            <div style="float:right; width:auto;">
				<!--- apps listing --->
                <cfif apps.recordCount GT 1>
                    App
                    <select style="width:100px" onchange="viewAssets(this.options[this.selectedIndex].value)">
                    <cfloop query="#apps#">
                        <option value="#APP_ID#" <cfif AssetLibAppID IS APP_ID> selected</cfif>>#APPNAME#</option>
                    </cfloop>
                    </select>
				</cfif>
            <span class="contentLinkGrey" style="padding-bottom:8px">View Group Assets</span>
<select name="moveAsset" class="itemShow" id="moveAsset" style="background:url(images/folder.png) no-repeat; border:0; width:45px; height:45px;color:rgba(0,0,0,0);" type="button" onchange="viewSubgroup(this.options[this.selectedIndex].value)" />
              	<option value="0"></option>
                <option value="-1" style="color:##06C; padding:5px">All Assets</option>
                <cfloop query="allGroups">
                  <option value="#group_id#" style="color:##333; padding:5px">#name#</option>
                </cfloop>
            </select>
          </div>
      </div>
    </div>
        
        <div class="contentLinkGrey" style="width:800px;padding: 0px 0px 0px 10px; height:44px; margin-top:2px; background-color:##DDD; float:left">
          <div style="float:left; width:550px">
          <div style="padding-top:12px; padding-left:5px; padding-right:10px; width:auto; float:left">
          <a href="AppsView.cfm?assetTypeTab=#assetTypeTab#&subgroupID=#subgroupID#" class="contentLink">All Types</a>
          </div>
          <div style="width:auto; float:left">
          <cfif subgroupID IS 0>
          	<img src="images/replace.png" style="cursor:pointer" onClick="syncAllAssets();" />
          </cfif>
          <cfif session.assetID GT 0 OR session.subgroupID GT 0>
          	<img src="images/importAsset.png" style="cursor:pointer" onClick="importAssetsToContent();" />
          </cfif>
          </div>
          </div>
              <div style="float:right">
   			<img src="images/remove.png" style="cursor:pointer;vertical-align: sub" onClick="deleteSelected()" />
            
              <select name="createAsset" id="createAsset" style="background:url(images/include-sm.png) no-repeat; border:0; width:32px; height:50px;color:rgba(0,0,0,0);" type="button" onchange="createNewAsset(this.options[this.selectedIndex].value)" />
                <option value="0" style="color:##AAA; padding:5px" selected>Create New Asset</option>
                <cfloop index="aType" array="#allTypes#">
                  <option value="#aType.type#" style="color:##333; padding:5px">#aType.name#</option>
                </cfloop>
            </select>
              
                 
              </div>
              <div style="float:right; padding-top: 15px;">
				  <a href="AppsView.cfm?assetTypeTab=#assetTypeTab#&assetTypeID=#assetTypeID#&subgroupID=#subgroupID#&order=#order#" class="content">
				  <cfif curOrder IS 0>
				  	NEWEST
				  <cfelseif curOrder IS 1>
					ASCEND
				  <cfelse>
				  	DESEND
				  </cfif>
				  </a>
			  </div>
          </div>
      
      <div class="contentLinkGrey" style="width:810px; height:auto; float:left;">

        <cfif assets.recordCount GT 0>
		
			
       
       	 <table width="100%" border="0" cellspacing="1" cellpadding="0" class="contentLink material">
       
        <cfloop query="assets">
        
        <cfif assetName IS ''>
        	<cfset name = 'null asset (delete)'>
        <cfelse>
        	<cfset name = assetName>
        </cfif>
   
        <!--- link path for asset item --->
        <cfinvoke component="CFC.GroupedAssets" method="getGrouptPath" returnvariable="crumb">
            <cfinvokeargument name="groupID" value="2726"/> 
        </cfinvoke>
        
        <cfset aPath = "">
        
        <cfloop index="anItem" array="#crumb#">
            <cfset hPath = '<a href="AppsView.cfm?assetTypeTab=4&subgroupID=' & anItem.id &'">' & anItem.name & '</a>'>
            <cfset aPath = hPath &"/"& aPath>
        </cfloop>

        
        <cfif assetType_id NEQ ''>
        <cfif structKeyExists(assetPathsCur.types, assetType_id)>
        <!--- assets --->
        <cfset assetPath = assetPathsCur.types[assetType_id]>
        <cfif filter IS 1 OR filter IS 0>
          <tr>
            <td style="padding-left:8px">
            <div style="width:auto; float:left">
            <cfset thumbIMG = assetPath & 'thumbs/'& image>
            <cfset pathStyle = "contentLink">
				<cfif assetType_id IS 18>
				
					<cfquery name="contentAsset">
						SELECT  instanceName AS path
						FROM 	ContentAssets
						WHERE asset_id = #asset_ID# AND displayType = 6
					</cfquery>
					
					<cfif contentAsset.recordCount GT 0>
					
						<cfset groupPath = contentAsset.path>

						<cfinvoke component="CFC.GroupedAssets" method="groupPathVerified" returnvariable="pathOK">
							<cfinvokeargument name="appID" value="#session.appID#"/>
							<cfinvokeargument name="groupPath" value="#groupPath#"/>
						</cfinvoke>

						<cfif pathOK>
							<cfset pathStyle = "contentLink">
						<cfelse>
							<cfset pathStyle = "contentLinkGreen">
						</cfif>
					
					</cfif>
					
				</cfif>
            <a href="AppsView.cfm?assetTypeTab=#assetTypeTab#&assetTypeID=#assetType_id#&subgroupID=#subgroupID#&search=#search#" style="margin-right:2px"><img src="images/create/#icon#" width="32" height="32" border="0" style="background-color:##666;"/></a>
            <cfif fileExists(ExpandPath(thumbIMG))>
            	<img src="#thumbIMG#" border="0" class="center-cropped" style="background-color:##666">
            </cfif>
            </div>
            <div style="padding-top:6px;">
                <div style="width:300px; float:left"><a href="AssetsView.cfm?assetID=#asset_ID#&assetTypeID=#assetType_id#" style="padding-left: 10px;" class="#pathStyle#">#name#</a></div>
                
                <!--- <cfif ArrayFind(unusedAssets, assets.asset_id) GT 0>
					<div style="text-align: right;color:##AAA;">Unused</div>
                </cfif> --->
                <div style="float:right; width:330px; text-align:right; color:##AAA; padding-top:2px; margin-right:4px" id="#group_id#_assetPath">
                	<cfif ArrayFind(unusedAssets, assets.asset_id) GT 0>
						(not used)
					</cfif>
                	
                </div>
                
            </div>
            </td>
            <td width="44"><img src="images/display-path.png" style="cursor:pointer;vertical-align: sub" onClick="displayPath(#group_id#)" /></td>
            <td width="44"><input name="#asset_ID#" id="sel" class="checkmarkNormal" onclick="setCheckmarkState(this,'checkmark');" type="button"></td>
            </tr>
          </cfif>
          </cfif>
          
       </cfif>
          </cfloop>
          </table>
        
          <cfelse>
          <table width="100%" border="0" cellspacing="1" cellpadding="0" class="contentLink material">
          <tr><td height="44" class="contentLinkDisabled" style="padding-left:10px">No Assets in Category</td></tr>
          </table>
          </cfif>
        
    	
    </div>

      
      
  </cfoutput>


</div>


