<cfparam name="groupID" default="0">
<cfparam name="assetID" default="0">
<cfparam name="moveGroupID" default="0">
<cfparam name="subgroupID" default="0">

<!--- Move Group and All Assets and or Subgroups --->
<cfif groupID GT '0'>

 	<cfinvoke component="CFC.Modules" method="moveGroup" returnvariable="moved">
        <cfinvokeargument name="groupID" value="#groupID#"/>
        <cfinvokeargument name="moveToGroupID" value="#moveGroupID#"/>
    </cfinvoke> 
	
	<cflocation url="AppsView.cfm?subgroupID=#subgroupID#&amp;error=Asset Group and All Assets in Group have been Moved" addtoken="no">

</cfif>

<!--- Move Asset --->
<cfif assetID GT '0'>

	<cfinvoke component="CFC.Modules" method="moveGroupAsset" returnvariable="moved">
        <cfinvokeargument name="assetID" value="#assetID#"/>
        <cfinvokeargument name="moveToGroupID" value="#moveGroupID#"/>
    </cfinvoke>
	
    <cflocation url="AppsView.cfm?subgroupID=#subgroupID#&amp;error=Asset has been Moved" addtoken="no">
    
</cfif>

