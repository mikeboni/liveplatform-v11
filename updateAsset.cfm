<!--- Define Structs for Data --->
<cfset assetData = structNew()>
<cfset detailData = structNew()>
<cfset thumbData = structNew()>
<cfset colorData = structNew()>

<!---Temp Path for Upload--->
<cfset tempPath = GetTempDirectory()>

<!--- defaults --->
<cfparam name="assetPath" default="">
<cfparam name="assetID" default="0">

<cfparam name="assetTypeID" default="0">
<cfparam name="sharable" default="0">

<cfparam name="subgroupID" default="0">

<!--- Asset Details Common --->
<cfparam name="assetName" default="">

<!--- URLAsset --->
<cfparam name="URLLink" default="">

<!--- ImageAsset --->
<cfparam name="imageAsset" default="">
<cfparam name="webURL" default="">

<!--- VideoAsset --->
<cfparam name="VideoAsset" default="">

<!--- MarkerAsset --->
<cfparam name="markerAsset" default="">
<cfparam name="APIKey" default="">
<cfparam name="APISecret" default="">

<cfparam name="ckvisualizer" default="false">
<cfparam name="moodstocks" default="false">
<cfparam name="vuforia" default="false">

<!--- Scene Asset 3D --->
<cfparam name="model3dScene" default="0">
<cfparam name="skyboxType" default="0">
<!--- <cfparam name="useReflection" default="false"> --->
<cfparam name="useFog" default="false">
<cfparam name="useTimeOfDay" default="false">


<cfparam name="zoomMin" default="0">
<cfparam name="zoomMax" default="0">
<cfparam name="zoomStart" default="50">
<cfparam name="zoom" default="0">

<cfparam name="rotationHorizontal" default="0">
<cfparam name="rotationVertical" default="0">

<cfparam name="cameraTarget_X" default="0">
<cfparam name="cameraTarget_Y" default="0">
<cfparam name="cameraTarget_Z" default="0">

<cfparam name="cameraLookAt_X" default="0">
<cfparam name="cameraLookAt_Y" default="0">
<cfparam name="cameraLookAt_Z" default="0">

<cfparam name="lightingIntensity" default="0">


<!--- <cfparam name="cameraTarget" default="0">
<cfparam name="cameraLookAt" default="0"> --->

<cfparam name="positionSpeed" default="0">

<cfparam name="fogStart" default="0">
<cfparam name="fogEnd" default="0">

<cfparam name="lookAtSpeed" default="0">
<cfparam name="defaultVAngle" default="0">
<cfparam name="defaultHAngle" default="0">
<cfparam name="heightOffset" default="0">
<cfparam name="renderingPath" default="0">

<cfparam name="glow" default="0">

<cfparam name="ambientOccl" default="0">
	<cfparam name="intensityOccl" default="0">
	<cfparam name="radiusOccl" default="0">

<cfparam name="depthOfField" default="0">
	<cfparam name="focusDistance" default="10">
	<cfparam name="aperture" default="5.6">
	<cfparam name="focalLength" default="50">

<cfparam name="colorGrading" default="0">
	<cfparam name="GradingSaturation" default="1">
	<cfparam name="GradingContrast" default="1">
	
	<cfparam name="whiteIn" default="10">
	<cfparam name="blackIn" default="0.02">
	
	<cfparam name="exposure" default="1">

	<cfparam name="GradingMixerR" default="1">
	<cfparam name="GradingMixerG" default="1">
	<cfparam name="GradingMixerB" default="1">

<cfparam name="Bloom" default="0">
	<cfparam name="bloomSoftKnee" default="0.5">
	<cfparam name="bloomRadius" default="4">
	<cfparam name="bloomAntiFlicker" default="0">
	<cfparam name="bloomIntensity" default="0">

<cfparam name="vignette" default="0">
	<cfparam name="vignetteIntensity" default="0.45">
	<cfparam name="vignetteSmoothness" default="0.2">
	<cfparam name="vignetteRoundness" default="1">
	<cfparam name="vignetteColor" default="1">

<cfparam name="antiAlias" default="0">
<cfparam name="scrReflection" default="0">
<cfparam name="motionBlur" default="0">

<cfparam name="farClippingPlane" default="1000">

<cfparam name="hero" default="0">

<cfparam name="dismiss" default="0">

<cfparam name="flipLR" default="0">

<!--- <cfparam name="viewPoint" default="false">
<cfparam name="useHDR" default="false"> --->


<cfparam name="lightColor" default="">
<cfparam name="shadowIntensity" default="1">

<cfparam name="intensity" default="1">
<cfparam name="shadowType" default="2">
<cfparam name="indirectMultiplier" default="1">
<cfparam name="lightRotationX" default="0">
<cfparam name="lightRotationY" default="0">

<!--- Dynamic --->
<cfparam name="useDynamic" default="0">

<!--- 3DModelAsset --->
<cfparam name="FBXAsset" default="">
<cfparam name="OS" default="0">

<cfparam name="locX" default="0">
<cfparam name="locY" default="0">
<cfparam name="locZ" default="0">

<cfparam name="scaleToMap" default="0">
<cfparam name="absolutePos" default="0">
<cfparam name="hideBehindGeometry" default="0">
<cfparam name="disabled" default="0">

<cfparam name="rotX" default="0">
<cfparam name="rotY" default="0">
<cfparam name="rotZ" default="0">

<cfparam name="scale" default="1">
<cfparam name="includeAR" default="0">

<cfparam name="CamX" default="0">
<cfparam name="CamY" default="0">
<cfparam name="CamZ" default="0">

<cfparam name="CPan" default="0">
<cfparam name="CZoom" default="0">
<cfparam name="CRotation" default="0">

<cfparam name="TargetX" default="0">
<cfparam name="TargetY" default="0">
<cfparam name="TargetZ" default="0">

<cfparam name="FOV" default="1">

<!--- AR defaults --->
<cfparam name="ar" default="0">
<cfparam name="ar_scale" default="0">
<cfparam name="scaleRange" default="0">
<cfparam name="ar_rotationRange" default="0">

<cfparam name="ar_Smin" default="0">
<cfparam name="ar_Smax" default="0">

<cfparam name="ar_xml" default="">
<cfparam name="ar_dat" default="">
<cfparam name="ar_img" default="">


<cfparam name="ambientOccl" default="false">
<cfparam name="bloom" default="false">
<cfparam name="antialias" default="false">
<cfparam name="scrReflection" default="false">
<cfparam name="motionBlur" default="false">
<cfparam name="vignette" default="false">

<cfparam name="colums" default="0">

<cfparam name="sceneSettings" default="0">

<!--- Bascic 3D Asset --->
<cfif assetTypeID IS 25>
	
	<cfset position = structNew()>
	
	<cfif locX IS ''><cfset locX = '0'></cfif>
    <cfif locY IS ''><cfset locY = '0'></cfif>
    <cfif locZ IS ''><cfset locZ = '0'></cfif>
	
	<cfset structAppend(position,{"x" : locX})>
	<cfset structAppend(position,{"y" : locY})>
    <cfset structAppend(position,{"z" : locZ})>
    
    <cfset rotation = structNew()>
    
    <cfif rotX IS ''><cfset rotX = '0'></cfif>
    <cfif rotY IS ''><cfset rotY = '0'></cfif>
    <cfif rotZ IS ''><cfset rotZ = '0'></cfif>
    
	<cfset structAppend(rotation,{"x" : rotX})>
	<cfset structAppend(rotation,{"y" : rotY})>
    <cfset structAppend(rotation,{"z" : rotZ})>
    
    <cfif scale IS ''><cfset scale = 1></cfif>
    <cfif includeAR IS ''><cfset includeAR = 0></cfif>
    
    <cfset structAppend(assetData,{"model" : {"rotation":rotation, "position": position, "scale": scale, "ar":includeAR} })> 
    
</cfif>

<!--- Full 3D OLD Asset --->
<cfif isDefined("FORM.CamX") OR isDefined("FORM.TargetX") OR isDefined("FORM.locX") AND assetTypeID IS 6>
	
	<cfset position = structNew()>
    
    <cfif locX IS ''><cfset locX = '0'></cfif>
    <cfif locY IS ''><cfset locY = '0'></cfif>
    <cfif locZ IS ''><cfset locZ = '0'></cfif>
    
	<cfset structAppend(position,{"x" : locX})>
	<cfset structAppend(position,{"y" : locY})>
    <cfset structAppend(position,{"z" : locZ})>
    
    <cfset rotation = structNew()>
    
    <cfif rotX IS ''><cfset rotX = '0'></cfif>
    <cfif rotY IS ''><cfset rotY = '0'></cfif>
    <cfif rotZ IS ''><cfset rotZ = '0'></cfif>
    
	<cfset structAppend(rotation,{"x" : rotX})>
	<cfset structAppend(rotation,{"y" : rotY})>
    <cfset structAppend(rotation,{"z" : rotZ})>
    
    <cfset camera = structNew()>
    
    <cfif CamX IS ''><cfset CamX = '0'></cfif>
    <cfif CamY IS ''><cfset CamY = '0'></cfif>
    <cfif CamZ IS ''><cfset CamZ = '0'></cfif>
    
	<cfset structAppend(camera,{"x" : CamX})>
	<cfset structAppend(camera,{"y" : CamY})>
    <cfset structAppend(camera,{"z" : CamZ})>
    
    <cfset control = structNew()>
    
	<cfset structAppend(control,{"pan" : CPan})>
	<cfset structAppend(control,{"zoom" : CZoom})>
    <cfset structAppend(control,{"rotation" : CRotation})>
    
    <cfset cameraConst = structNew()>
    
    <cfif CamMin IS ''><cfset CamMin = '0'></cfif>
    <cfif CamMax IS ''><cfset CamMax = '0'></cfif>
    
    <cfif CamTiltMin IS ''><cfset CamTiltMin = '0'></cfif>
    <cfif CamTiltMax IS ''><cfset CamTiltMax = '0'></cfif>
    
    <cfif CamPanMin IS ''><cfset CamPanMin = '0'></cfif>
    <cfif CamPanMax IS ''><cfset CamPanMax = '0'></cfif>
    
	<cfset structAppend(cameraConst,{"min" : CamMin})>
	<cfset structAppend(cameraConst,{"max" : CamMax})>
    
    <cfset structAppend(cameraConst,{"tiltMin" : CamTiltMin})>
	<cfset structAppend(cameraConst,{"tiltMax" : CamTiltMax})>
    
    <cfset structAppend(cameraConst,{"panMin" : CamPanMin})>
	<cfset structAppend(cameraConst,{"panMax" : CamPanMax})>

    
    <cfset target = structNew()>
    
    <cfif TargetX IS ''><cfset TargetX = '0'></cfif>
    <cfif TargetY IS ''><cfset TargetY = '0'></cfif>
    <cfif TargetZ IS ''><cfset TargetZ = '0'></cfif>
    
	<cfset structAppend(target,{"x" : TargetX})>
	<cfset structAppend(target,{"y" : TargetY})>
    <cfset structAppend(target,{"z" : TargetZ})>
    
    <cfif FOV IS ''><cfset FOV = '0'></cfif>
    <cfif scale IS ''><cfset scale = '1'></cfif>
    
    <cfset structAppend(assetData,{"camera" : {"position": camera, "target": target, "fov": FOV, 'limits':cameraConst, 'control':control}, "model" : {"rotation":rotation, "position": position, "scale": scale, "ar": ar} })> 
    
</cfif>

<!--- MarkerAsset --->
<cfif isDefined("FORM.moodstocks")>
	<cfif moodstocks>
		<cfset structAppend(assetData,{"moodstocks" : {"key":#APIKey#, "secret":#APISecret#} })> 
    </cfif>
</cfif>

<cfif isDefined("FORM.ckvisualizer")>
	<cfif ckvisualizer>
    	
        <cfif markerAsset NEQ ''>
            
            <cffile action="upload" filefield="markerAsset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
            <cfset imageFile = uploadFile.serverFile>
        
            <cfset structAppend(assetData,{"visualizer" : {"url":imageFile} })>
        
        <cfelse>
        
        	<cfset structAppend(assetData,{"visualizer" : {"webURL":webURL} })>
            <cfset webURL = ''>
            
        </cfif>
        
        <cfset structAppend(assetData.visualizer,{"keySet":keySet, "keyData":keyData})>
      
    </cfif>
</cfif>

<cfif isDefined("FORM.vuforia")>
	<cfif vuforia>
	
        <cfif ar_xml NEQ ''>
            <cffile action="upload" filefield="ar_xml" destination="#tempPath#" nameconflict="overwrite" result="uploadFile">
            <cfset xmlFile = uploadFile.serverFile>
            <cfset ar_xml = xmlFile>
        </cfif>
        
        <cfif ar_dat NEQ ''>
            <cffile action="upload" filefield="ar_dat" destination="#tempPath#" nameconflict="overwrite" result="uploadFile">
            <cfset datFile = uploadFile.serverFile>
            <cfset ar_dat = datFile>
        </cfif>
        
        <cfset structAppend(assetData,{"vuforia" : {"xml":#ar_xml#, "dat":#ar_dat#} })> 
        
    </cfif>
</cfif>

<!--- 3D Point --->
<cfparam name="x_pos" default="">
<cfparam name="y_pos" default="">
<cfparam name="z_pos" default="">

<cfparam name="alignH" default="0">
<cfparam name="alignV" default="0">
<cfparam name="touchArea" default="0">

<cfif isDefined("FORM.objectRef")>
		<cfset structAppend(assetData,{"objectRef" : #FORM.objectRef #})> 
</cfif>

<cfif isDefined("FORM.objectRef")>
		<cfset structAppend(assetData,{"objectRef" : #FORM.objectRef#})> 
</cfif>

<cfif isDefined("FORM.hideBehindGeometry")>
	<cfset structAppend(assetData,{"hideBehindGeometry" : #FORM.hideBehindGeometry#})> 
<cfelse>
	<cfset structAppend(assetData,{"hideBehindGeometry" : 0})> 
</cfif>

<cfif isDefined("FORM.disabled")>
	<cfset structAppend(assetData,{"disabled" : #FORM.disabled#})> 
<cfelse>
	<cfset structAppend(assetData,{"disabled" : 0})> 
</cfif>

<cfif isDefined("FORM.cameraBehaviour")>
		<cfset structAppend(assetData,{"cameraBehaviour" : #FORM.cameraBehaviour#})> 
</cfif>

<!--- GPSAsset --->
<cfparam name="gps_long" default="">
<cfparam name="gps_latt" default="">
<cfparam name="gps_radius" default="">
<cfparam name="xpos" default="">
<cfparam name="ypos" default="">
<cfparam name="gps_alt" default="">

<cfif isDefined("FORM.gps_long") OR isDefined("FORM.gps_latt")>
	
    <cfset gps = structNew()>
	<cfset loc = structNew()>
    
    <cfset structAppend(gps,{"long" : 0})>
    <cfset structAppend(gps,{"latt" : 0})>
    <cfset structAppend(gps,{"alt" : 0})>
    
	<cfset structAppend(loc,{"x" : 0})>
    <cfset structAppend(loc,{"y" : 0})>
    
    <cfset structAppend(assetData,{"gps" : {"coords": gps, "position": loc, "radius": 0, "zoom":0, "styles": ""} })> 
    
</cfif>

<cfif gps_long NEQ "" OR gps_latt NEQ "">
	
    <cfset assetData.gps.coords.long = gps_long>
    <cfset assetData.gps.coords.latt = gps_latt>
    <cfset assetData.gps.coords.alt = gps_alt>
    
    <cfset assetData.gps.position.x = xpos>
    <cfset assetData.gps.position.y = ypos>
    
    <cfset assetData.gps.radius = gps_radius>
    
    <cfif zoom IS ''><cfset zoom = 0></cfif>
    
    <cfset assetData.gps.zoom = zoom>
    <cfset assetData.gps.styles = styles>
       
</cfif>


<!--- Animation Asset --->
<cfif isDefined("FORM.animationSeq")>
	
	<cfset animation = structNew()>
    
    <cfset structAppend(animation,{"modelName" : model3D})>
    <cfset structAppend(animation,{"sequenceName" : animationSeq})>
    
    <cfset structAppend(assetData,{"animation":animation})>
	
</cfif>

<!--- 3D Material --->
<cfif isDefined("FORM.material")>

    <cfset model3DMaterial = structNew()>
    
    <cfset structAppend(model3DMaterial,{"modelName" : model3D})>
    <cfset structAppend(model3DMaterial,{"material" : material})>
    
    <cfset structAppend(assetData,{"shaderMaterial":model3DMaterial})>
    
</cfif>

<!--- DocumentAsset --->
<cfparam name="PDFAsset" default="">

<!--- PanoramaAsset --->
<cfparam name="pano360Asset" default="">

<cfparam name="panoLAsset" default="">
<cfparam name="panoRAsset" default="">
<cfparam name="panoUAsset" default="">
<cfparam name="panoFAsset" default="">
<cfparam name="panoBAsset" default="">
<cfparam name="panoDAsset" default="">

<cfparam name="panoLAssetVR" default="">
<cfparam name="panoRAssetVR" default="">
<cfparam name="panoUAssetVR" default="">
<cfparam name="panoFAssetVR" default="">
<cfparam name="panoBAssetVR" default="">
<cfparam name="panoDAssetVR" default="">

<cfif isDefined('vr')>
	<cfset structAppend(assetData,{'vr':vr})>
<cfelse>
	<cfset structAppend(assetData,{'vr':0})>
</cfif>

<cfif panoLAsset NEQ "" OR panoRAsset NEQ "" OR panoUAsset NEQ "" OR panoFAsset NEQ "" OR panoBAsset NEQ "" OR panoDAsset NEQ "" OR pano360Asset NEQ "" OR panoLAssetVR NEQ "" OR panoRAssetVR NEQ "" OR panoUAssetVR NEQ "" OR panoFAssetVR NEQ "" OR panoBAssetVR NEQ "" OR panoDAssetVR NEQ "">

	<cfset pano = structNew()>
	 
    <!--- Pano Image 360 --->
    <cfif pano360Asset NEQ "">
	
		<cfset structAppend(pano,{"pano360Asset" : pano360Asset})> 

        <cffile action="upload" filefield="pano360Asset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
        <cfset imagePanoFile = uploadFile.serverFile>

        <!--- Image Size --->
        <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imagePanoSize">
            <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
        </cfinvoke>
         
    </cfif>
    
	<cfset structAppend(pano,{"panoLAsset" : panoLAsset})>
	<cfset structAppend(pano,{"panoRAsset" : panoRAsset})>
	<cfset structAppend(pano,{"panoUAsset" : panoUAsset})>
	<cfset structAppend(pano,{"panoFAsset" : panoFAsset})>
	<cfset structAppend(pano,{"panoBAsset" : panoBAsset})>
	<cfset structAppend(pano,{"panoDAsset" : panoDAsset})>
    
    <cfset structAppend(pano,{"panoLAssetVR" : panoLAssetVR})>
	<cfset structAppend(pano,{"panoRAssetVR" : panoRAssetVR})>
	<cfset structAppend(pano,{"panoUAssetVR" : panoUAssetVR})>
	<cfset structAppend(pano,{"panoFAssetVR" : panoFAssetVR})>
	<cfset structAppend(pano,{"panoBAssetVR" : panoBAssetVR})>
	<cfset structAppend(pano,{"panoDAssetVR" : panoDAssetVR})>     

    <cfset panoImageFiles = arrayNew(1)>
	
    <cfloop collection="#pano#" item="images">
    
		<cfif pano[images] NEQ ''>

            <cffile action="upload" filefield="#images#" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
            <cfset imageFile = uploadFile.serverFile>

            <!--- Image Size --->
            <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageSize">
                <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
            </cfinvoke>
         
            <cfswitch expression="#images#">
            	<!--- Back --->
                <cfcase value="panoBAsset">
                	<cfset side = "back">
                </cfcase>
                <!--- Front --->
                <cfcase value="panoFAsset">
                	<cfset side = "front">
                </cfcase>
                <!--- Left --->
                <cfcase value="panoLAsset">
                	<cfset side = "left">
                </cfcase>
                <!--- Right --->
                <cfcase value="panoRAsset">
                	<cfset side = "right">
                </cfcase>
                <!--- Up --->
                <cfcase value="panoUAsset">
                	<cfset side = "up">
                </cfcase>
                <!--- Down --->
                <cfcase value="panoDAsset">
                	<cfset side = "down">
                </cfcase>
            	<!--- 360 pano --->
                <cfdefaultcase>
                	<cfset side = "pano">
                </cfdefaultcase>

            </cfswitch>
            
            <cfswitch expression="#images#">
                <!--- VR --->
       
                <!--- Back VR --->
                <cfcase value="panoBAssetVR">
                	<cfset side = "back_vr">
                </cfcase>
                <!--- Front VR --->
                <cfcase value="panoFAssetVR">
                	<cfset side = "front_vr">
                </cfcase>
                <!--- Left VR --->
                <cfcase value="panoLAssetVR">
                	<cfset side = "left_vr">
                </cfcase>
                <!--- Right VR --->
                <cfcase value="panoRAssetVR">
                	<cfset side = "right_vr">
                </cfcase>
                <!--- Up VR --->
                <cfcase value="panoUAssetVR">
                	<cfset side = "up_vr">
                </cfcase>
                <!--- Down VR --->
                <cfcase value="panoDAssetVR">
                	<cfset side = "down_vr">
                </cfcase>
            </cfswitch>
         
            <cfif side IS 'pano'> 
            	<cfset imageFile = {"image" : {"file":imagePanoFile, "size":imagePanoSize , "side":side}}>
            	<cfset arrayAppend(panoImageFiles, structCopy(imageFile))>
            <cfelse>
            	<cfset imageFile = {"image" : {"file":uploadFile.serverFile, "size":imageSize , "side":side}}>
            	<cfset arrayAppend(panoImageFiles,structCopy(imageFile))>
         	</cfif>
            
        </cfif>
       
    </cfloop>
    
    <cfset structAppend(assetData,{"panorama" : panoImageFiles})>
    
</cfif>

<!--- BalconyAsset --->
<cfparam name="BalconyNAsset" default="">
<cfparam name="BalconySAsset" default="">
<cfparam name="BalconyWAsset" default="">
<cfparam name="BalconyEAsset" default="">
<!--- <cfparam name="FloorName" default=""> --->

<cfif BalconyNAsset NEQ "" OR BalconySAsset NEQ "" OR BalconyWAsset NEQ "" OR BalconyEAsset NEQ "">

	<cfset views = structNew()>
    
    <cfset structAppend(views,{"BalconyNAsset" : BalconyNAsset})>
    <cfset structAppend(views,{"BalconySAsset" : BalconySAsset})>
    <cfset structAppend(views,{"BalconyWAsset" :  BalconyWAsset})>
    <cfset structAppend(views,{"BalconyEAsset" : BalconyEAsset})>
    
    <cfset balconyImageFiles = arrayNew(1)>
	
    <cfloop collection="#views#" item="images">
    
		<cfif views[images] NEQ ''>

            <cffile action="upload" filefield="#images#" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
            <cfset imageFile = uploadFile.serverFile>

            <!--- Image Size --->
            <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageSize">
                <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
            </cfinvoke>
            
            <cfswitch expression="#images#">
            	<!--- North --->
                <cfcase value="BalconyNAsset">
                	<cfset side = "north">
                </cfcase>
                <!--- South --->
                <cfcase value="BalconySAsset">
                	<cfset side = "south">
                </cfcase>
                <!--- West --->
                <cfcase value="BalconyWAsset">
                	<cfset side = "west">
                </cfcase>
                <!--- East --->
                <cfcase value="BalconyEAsset">
                	<cfset side = "east">
                </cfcase>
            
            </cfswitch>
            
            <cfset imageFile = {"image" : {"file":uploadFile.serverFile, "size":imageSize, "side":side}}>
            <cfset arrayAppend(balconyImageFiles,imageFile)>
         
        </cfif>
       
    </cfloop>

    <cfset structAppend(assetData,{"balcony" : balconyImageFiles})>
    
</cfif>

<cfif isDefined('FloorName')>

	<cfif NOT isDefined('assetData.balcony')>
    	<cfset structAppend(assetData,{"balcony" :[]})>
    </cfif>
    
    <cfset structAppend(assetData,{"floor": FloorName})>
    
</cfif>

<cfif isDefined('viewFacing')>

	<cfif NOT isDefined('assetData.balcony')>
    	<cfset structAppend(assetData,{"balcony" :[]})>
    </cfif>
    
    <cfset structAppend(assetData,{"viewFacing": viewFacing})>
    
</cfif>


<!--- Thumbnail --->
<cfparam name="useThumb" default="false">
<cfparam name="imageThumb" default="">

<!--- Colors --->
<cfparam name="useColors" default="false">
<cfparam name="displayAssetID" default="0">

<!--- Details --->
<cfparam name="useDetails" default="false">
<cfparam name="contentColor" default="">
<cfparam name="contentBGColor" default="">
<cfparam name="title" default="">
<cfparam name="subtitle" default="">
<cfparam name="description" default="">
<cfparam name="titleColor" default="">
<cfparam name="subtitleColor" default="">
<cfparam name="descriptionColor" default="">
<cfparam name="other" default="">

<!--- WebURL --->
<cfif webURL NEQ "">
	<cfset structAppend(assetData,{"webURL" : webURL})>
</cfif>

<!--- Dynamic Property --->
<cfset structAppend(assetData,{"dynamic" : useDynamic})>

<!--- Image Master --->
<cfset imageFile = structNew()>

<cfif imageAsset NEQ "">

	<cffile action="upload" filefield="imageAsset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset imageFile = uploadFile.serverFile>
    
	<!--- Image Size --->
    <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageSize">
        <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
    </cfinvoke>
    
	<cfset imageFile = {"file":uploadFile.serverFile, "size":imageSize}>
    
    <cfset structAppend(assetData,{"image" : imageFile})>
    
</cfif>

<!--- Image Thumbnail --->
<cfset imageThumbFile = structNew()>

<cfif imageThumb NEQ "">

	<cffile action="upload" filefield="imageThumb" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">

	<!--- Image Size --->
    <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageThumbSize">
        <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
    </cfinvoke>
    
    <cfset imageThumbFile = {"file":uploadFile.serverFile, "size":imageThumbSize}>
    
<cfelse>
	<cfset imageThumbFile = {"file":""}>
</cfif>


<!---FBX Data--->
<cfif FBXAsset NEQ "">

	<cffile action="upload" filefield="FBXAsset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset FBXFile = uploadFile.serverFile>
    
    <cfset structAppend(assetData,{"model3D" : FBXFile})>
    <cfset structAppend(assetData,{"os" : OS})>

</cfif>

<!---PDF Data--->
<cfif PDFAsset NEQ "">

	<cffile action="upload" filefield="PDFAsset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset PDFFile = uploadFile.serverFile>
    
    <cfset structAppend(assetData,{"pdf" : PDFFile})>

</cfif>

<!---URL Data--->
<cfif URLLink NEQ "">
    <cfset structAppend(assetData,{"url" : URLLink})>
</cfif>

<!---Video Data--->
<cfif VideoAsset NEQ "">

	<cffile action="upload" filefield="VideoAsset" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset MPGFile = uploadFile.serverFile>
    
    <cfset structAppend(assetData,{"video" : MPGFile})>

</cfif>

<!--- 3D Model --->
<cfif isDefined("assetData.model.ar") AND assetTypeID IS 6>
	
    <cfset arData = structNew()>
    
    <cfif ar_xml NEQ ''>
    	<cffile action="upload" filefield="ar_xml" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
        <cfset xmlFile = uploadFile.serverFile>
        <cfset structAppend(arData,{"xml" : xmlFile})>
    </cfif>
    
    <cfif ar_dat NEQ ''>
    	<cffile action="upload" filefield="ar_dat" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
        <cfset datFile = uploadFile.serverFile>
        <cfset structAppend(arData,{"dat" : datFile})>
    </cfif>
    
    <cfif ar_img NEQ ''>
    	<cffile action="upload" filefield="ar_img" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
        <cfset imgFile = uploadFile.serverFile>
        <cfset structAppend(arData,{"img" : imgFile})>
    </cfif>
    
	<cfset structAppend(assetData,{"target":arData})>

</cfif>

<!--- AR --->
<cfif isDefined("MarkerAssetID")>
    
    <cfset structAppend(assetData, {"ar":{"markerAssetID":markerAssetID, "markerID":arType}})> 
    
</cfif>


<!--- XYZ Point --->

	
<!--- <cfif isDefined("z_pos")> --->

<cfif isDefined("PointType")>

	<cfif x_pos IS ''><cfset x_pos = 0></cfif>
    <cfif y_pos IS ''><cfset y_pos = 0></cfif>
    <cfif z_pos IS ''><cfset z_pos = 0></cfif>
    
    <cfif alignH IS ''><cfset alignH = 0></cfif>
    <cfif alignV IS ''><cfset alignV = 0></cfif>
    <cfif touchArea IS ''><cfset touchArea = 0></cfif>
	<cfif absolutePos IS ''><cfset absolutePos = 0></cfif>
   
    <cfif PointType IS 0 OR PointType IS 1>
   		
   		<cfif PointType IS 0>
   			<cfset z_pos = 0>
		</cfif>
		
		<cfset assetData.objectRef = ''>
		<cfset assetData.cameraBehaviour = 0>
		
	</cfif>
   
   <cfif PointType IS 2>
   		<cfset x_pos = 0>
  		<cfset y_pos = 0>
   		<cfset z_pos = 0>
	</cfif>
    
    <cfset structAppend(assetData, {"x":x_pos, "y":y_pos, "z":z_pos, "alignH": alignH, "alignV": alignV, "touchArea":touchArea,"scaleToMap":scaleToMap, "absolutePos":absolutePos})> 
    
</cfif>


<!--- Quiz Asset --->
<cfif isDefined("questionMessage")>

<cfparam name="needsToBeCompleted" default="0">
<cfparam name="movable" default="0">
<cfparam name="displayCorrect" default="0">
<cfparam name="tryAgain" default="0">
<cfparam name="selectionID" default="0">
<cfparam name="needResponse" default="0">

<cfset quiz = {"needsToBeCompleted": #needsToBeCompleted#, "movable": #movable# , "displayCorrect": #displayCorrect# }>
<cfset question = {"questionMessage": #questionMessage#}>
<cfset gridOptions = {"padding": #gridPadding#, "spacing": #gridSpacing#, "colums": #colums#, "selectionID":#selectionID#}>

<cfif needResponse>
	<cfset structAppend(question,{"needResponse": #needResponse#, "tryAgain":#numberOfTries#, "correctMessage":'#correctMessage#', "incorrectMessage":'#incorrectMessage#'})>
<cfelse>
	<cfset structAppend(question,{"needResponse": 0, "tryAgain":0, "correctMessage":'', "incorrectMessage":''})>
</cfif>

<cfset assetData = {"quiz":#quiz#, "question":#question#, "selection":gridOptions}>

</cfif>

<!--- Content Group --->
<cfif isDefined("behaviourType")>
	
	<!--- Not 3D or Img--->
	<cfif behaviourType IS 1 OR behaviourType IS 5 OR behaviourType IS 6  OR behaviourType IS 8 OR behaviourType IS 9>
   
   		<!--- container properties --->
		<cfif behaviourType IS 8 AND isDefined('alignment')>
			<cfset h = listGetAt(alignment,1)>
			<cfset v = listGetAt(alignment,2)>
			<cfif h IS 0>
				<cfset alignment = v>
			<cfelse>
				<cfset alignment = h>
			</cfif>
			<cfset align = {'container': {'align': align, 'alignment': alignment}}>

			<cfset structAppend(assetData,align)>
		</cfif>
   
    <cfelse>
    	<cfif NOT behaviourType IS 7>
        	<cfset useAssetID = '0'>
        </cfif>
        
		<cfset instanceNameAsset = "">
   
    </cfif>
	
    <cfif isDefined('useGroupID')>
		<cfif useGroupID GT 0>
            <cfset useAssetID = useGroupID>
        </cfif>
    </cfif>
    
    <cfset structAppend(assetData, {'useAssetID':useAssetID})>

    <cfset structAppend(assetData, {"behaviourType":behaviourType, "instanceName":instanceNameAsset})> 
    
    <cfif behaviourType IS 9>
    	<cfset structAppend(assetData, {"className":className, "methodName":methodName, "params":params})>
	</cfif>
   
   <cfif behaviourType IS 4>
   		<cfif isDefined('popoverType')>
    		<cfset structAppend(assetData, {"popoverType":popoverType})>
		</cfif>
	</cfif>

   <cfif behaviourType IS 3>
	    <cfif isDefined('colums')>
	    	<cfset structAppend(assetData, {"hero":hero, "cols": colums})>
	    </cfif>
   		<cfif isDefined('hero')>
    		<cfset structAppend(assetData, {"hero":hero, "cols": colums})>
		</cfif>
		<cfset structAppend(assetData, {"dismiss":dismiss})>
	</cfif>
 
</cfif>

<!--- Colors --->
<cfif useColors>
    <cfset colorData = {"forecolor": '#contentColor#', "backcolor": '#contentBGColor#', "othercolor": '#contentOtherColor#' , "displayAssetID": '#displayAssetID#' }>
</cfif>

<!--- Colors --->
<cfif isDefined('selectionColor')>

	<cfif selectionColor NEQ '' OR selectionBGColor NEQ '' OR selectionBKImg NEQ ''>
        <cfset colorSelectionData = {"forecolor": '#selectionColor#', "backcolor": '#selectionBGColor#', "othercolor": '#contentOtherColor#', "displayAssetID": '#displayAssetID#' }>
    <cfelse>
    	<cfset colorSelectionData = {"forecolor": '', "backcolor": '' , "background": '', "othercolor":'' }>
    </cfif>
    
    <cfset structAppend(assetData.selection,{'colors':colorSelectionData})>
    
</cfif>

<!--- Game --->
<cfif isDefined("gStartDate") OR isDefined("gEndDate")>

	<cfparam name="sendPlayerEmail" default="false">
    <cfparam name="sendClientEmail" default="false">
    
    <cfparam name="includeCode" default="false">
    <cfparam name="includeContent" default="false">
    
    <cfparam name="clientEmailAddress" default="">
    
    <cfparam name="silentMode" default="false">
	
	<cfset gameData = {"startDate": '#gStartDate#', "endDate": '#gEndDate#', "silentMode":#silentMode# }>
    
    <cfset messages = {"start":'#gameInitalMessage#', "end":'#gameCompletionMessage#'}>
    <cfset structAppend(gameData,{"message":messages})>
    
    <cfset structAppend(gameData,{"useCode":#includeCode#, "sendTargets":#includeContent#})>
    
    <cfset email = {"sendPlayer":#sendPlayerEmail#, "sendClient":#sendClientEmail#}>

    <cfif clientEmailAddress NEQ ''>
        <cfset structAppend(email,{"email":'#clientEmailAddress#'})>
    <cfelse>
    	<cfset email.sendClient = false>
    </cfif>
    
    <cfif sendPlayerEmail>
    	<cfset structAppend(email,{"emailMessage":'#EmailMessage#'})>
    <cfelse>
    	<cfset structAppend(email,{"emailMessage":''})>
    </cfif>
    
    <cfset structAppend(gameData,{"email":email})>
    
    <cfset structAppend(assetData,gameData)>
</cfif>

<!--- Construct Data --->
<cfset structAppend(assetData,{"assetID":assetID})><!--- , "path":#assetPath# --->

<cfif useDetails>
	<cfset detailData = {"title": #title#, "subtitle": #subtitle#, "description": #description#, "other": #other#, "titleColor": #titleColor#, "subtitleColor": #subtitleColor#, "descriptionColor": #descriptionColor#}>
</cfif>

<cfif useThumb>
	<cfset thumbData = {"thumbnail": #imageThumb#, "thumb":#imageThumbFile#}>
</cfif>

<cfset structAppend(assetData,{"name" : #assetName# , "sharable" : #sharable#})>

<!--- Mapper Asset Map Object --->
<cfif isDefined("mapAssetID")>
	<cfset structAppend(assetData,{"mapAssetID" : #mapAssetID#})>
</cfif>

<!--- Update Asset Details and Thumbs --->
<cfif assetID IS '0' AND assetTypeID GT '0'>
	
	<!--- Build Asset Path --->
	<cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
		<cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
		<cfinvokeargument name="appID" value="#session.appID#"/>
	</cfinvoke>

	<cfset assetData.path = assetPath>
	<cfset structAppend(assetData,{"assetTypeID" : #assetTypeID#})>
	<cfset structAppend(assetData,{"appID" : #session.appID#})>

</cfif>

<!--- PANO ANGLE --->
<cfif isDefined("angle")>
	<cfset structAppend(assetData, {"angle":angle})>
    <cfset structAppend(assetData, {"viewRange":viewRange})>
</cfif>

<cfif isDefined("defaultView")>
	<cfset structAppend(assetData, {"defaultView":defaultView})>
</cfif>

<cfif isDefined("flipLR")>
	<cfset structAppend(assetData, {"flipLR":flipLR})>
</cfif>

<!--- AR Target --->
<cfif isDefined("arMode")>
	<cfset structAppend(assetData, {"arMode":arMode})>
</cfif>

<!--- offset for image --->
<cfif isDefined("offset_x") OR isDefined("offset_x")>
    
    <cfif  offset_x IS ''><cfset  offset_x = 0></cfif>
    <cfif  offset_y IS ''><cfset  offset_y = 0></cfif>
    
    <cfset structAppend(assetData, {"offset_x":offset_x})>
    <cfset structAppend(assetData, {"offset_y":offset_y})>

</cfif>
<!--- Use Pinch and Zoom Scaling --->
<cfif isDefined("useScaling")>
    <cfif  useScaling IS ''><cfset  useScaling = 0></cfif>
    <cfset structAppend(assetData, {"useScaling":useScaling})>
</cfif>

<cfif isDefined("videow") OR isDefined("videoh")>
    <cfset structAppend(assetData, {"video-ratio":videow/videoh})>
    <cfset structAppend(assetData, {"width":videow})>
    <cfset structAppend(assetData, {"height":videoh})>
</cfif>

<!--- Scene Asset --->

<cfif assetTypeID IS 23>
	
	<cfset structAppend(assetData, {"Model3DScene":#Model3DScene#})>
	
	<cfset structAppend(assetData, {"sceneSettings":#sceneSettings#})>
	
	<cfset skybox = {'type':#skyboxType#}>
	
	<cfif imageRotation IS ''><cfset imageRotation = 0></cfif>
	<cfset structAppend(assetData, {"imageRotation":#imageRotation#})>
	
	<cfif skyboxType GT 0>

		<cfswitch expression="#skyboxType#">
			<!--- Color --->
			<cfcase value="1">
				<cfset structAppend(skybox, {'bgColor':'#bgColor#'})>
			</cfcase>
			<!--- Image --->
			<cfcase value="2">
				<cfset structAppend(skybox, {'assetID':'#bkImg#'})>
			</cfcase>
			<!--- 360 Skybox --->
			<cfcase value="3">
				<cfset structAppend(skybox, {'assetID':'#img360#'})>
			</cfcase>
			<!--- Dynamic Sky --->
			<cfcase value="4">
				<!--- <cfset structAppend(skybox, {'assetID':'#img360#'})> --->
			</cfcase>

		</cfswitch>
		
	</cfif>

	<cfif useFog>
	
		<cfset fog = {}>
		
		<cfset structAppend(fog, {'useFog':#useFog#})>
		
		<cfif isDefined('fogColor')>
			<cfset structAppend(fog, {'color':#fogColor#})>
		</cfif>
		
		<cfset structAppend(fog, {'density':#density#})>
		
		<cfset structAppend(fog, {'fogStart':#fogStart#})>
		<cfset structAppend(fog, {'fogEnd':#fogEnd#})>
		
		<cfset structAppend(assetData, {'fog': #fog#})>
		
	</cfif>
	
	<cfset structAppend(assetData, {'ambientLightColor': '#ambientLightColor#'})>
	<cfset structAppend(assetData, {'exposure': '#exposure#'})>	
	<cfset structAppend(assetData, {'skybox': #skybox#})>
	
	<cfset light = {}>
	<cfset structAppend(light, {'shadowIntensity': #shadowIntensity#})>
	
	<cfset structAppend(light, {'intensity': #intensity#})>
	<cfset structAppend(light, {'shadowType': #shadowType#})>
	<cfset structAppend(light, {'indirectMultiplier': #indirectMultiplier#})>
	<cfset structAppend(light, {'lightRotation': {"x":#lightRotationX#, "y":#lightRotationY#}})>
	
	
	<cfset structAppend(light, {'useTimeOfDay': #useTimeOfDay#})>
	
	<cfif NOT useTimeOfDay>
		<cfset structAppend(light, {'color': #lightColor#})>
	</cfif>
	
	<cfset structAppend(assetData, {'light': #light#})>
	
	<cfset camera = {}>
	
	<!--- <cfset structAppend(camera, {'viewPoint': #viewPoint#})> --->
	<!--- <cfset structAppend(camera, {'useHDR': #useHDR#})> --->
	
	<cfset structAppend(camera, {'zoomMin': #zoomMin#})>
	<cfset structAppend(camera, {'zoomMax': #zoomMax#})>
	<cfset structAppend(camera, {'zoomStart': #zoomStart#})>
	<cfset structAppend(camera, {'zoom': #zoom#})>
	
	<cfset structAppend(camera, {'rotationHorizontal': #rotationHorizontal#})>
	<cfset structAppend(camera, {'rotationVertical': #rotationVertical#})>
	
	<cfset structAppend(assetData, {'lightingIntensity': #lightingIntensity#})>
	
	<cfset structAppend(camera, {'cameraTarget_X': #cameraTarget_X#})>
	<cfset structAppend(camera, {'cameraTarget_Y': #cameraTarget_Y#})>
	<cfset structAppend(camera, {'cameraTarget_Z': #cameraTarget_Z#})>

	<cfset structAppend(camera, {'cameraLookAt_X': #cameraLookAt_X#})>
	<cfset structAppend(camera, {'cameraLookAt_Y': #cameraLookAt_Y#})>
	<cfset structAppend(camera, {'cameraLookAt_Z': #cameraLookAt_Z#})>
	
	<cfset structAppend(camera, {'positionSpeed': #positionSpeed#})>

	<cfset structAppend(camera, {'lookAtSpeed': #lookAtSpeed#})>
	<cfset structAppend(camera, {'renderingPath': #renderingPath#})>
	
	<cfset structAppend(camera, {'defaultVAngle': #defaultVAngle#})>
	<cfset structAppend(camera, {'defaultHAngle': #defaultHAngle#})>
	<cfset structAppend(camera, {'heightOffset': #heightOffset#})>
	
	<cfset structAppend(camera, {'other': {}})>
	<cfset structAppend(camera.other, {'glow': #glow#})>
	<cfset structAppend(camera.other, {'farClippingPlane': #farClippingPlane#})>
	
	<cfset postProcessing = {}>
	
	<cfif ambientOccl>
		<cfset structAppend(postProcessing, {'ambientOccl': #ambientOccl#})>
		<cfset structAppend(postProcessing, {'intensityOccl': #intensityOccl#})>
		<cfset structAppend(postProcessing, {'radiusOccl': #radiusOccl#})>
	</cfif>
	
	<cfif Bloom>
		<cfset structAppend(postProcessing, {'bloom': #bloom#})>
		<cfset structAppend(postProcessing, {'bloomIntensity': #bloomIntensity#})>
		<cfset structAppend(postProcessing, {'bloomSoftKnee': #bloomSoftKnee#})>
		<cfset structAppend(postProcessing, {'bloomRadius': #bloomRadius#})>
		<cfset structAppend(postProcessing, {'bloomAntiFlicker': #bloomAntiFlicker#})>
	</cfif>
	
	<cfif vignette>
		<cfset structAppend(postProcessing, {'vignette': #vignette#})>
		<cfset structAppend(postProcessing, {'vignetteIntensity': #vignetteIntensity#})>
		<cfset structAppend(postProcessing, {'vignetteSmoothness': #vignetteSmoothness#})>
		<cfset structAppend(postProcessing, {'vignetteRoundness': #vignetteRoundness#})>
		<cfset structAppend(postProcessing, {'vignetteColor': #vignetteColor#})>
	</cfif>
	
	<cfif depthOfField>
		<cfset structAppend(postProcessing, {'depthOfField': #depthOfField#})>
		<cfset structAppend(postProcessing, {'focusDistance': #focusDistance#})>
		<cfset structAppend(postProcessing, {'aperture': #aperture#})>
		<cfset structAppend(postProcessing, {'focalLength': #focalLength#})>
	</cfif>
	
	<cfif colorGrading>
		<cfset structAppend(postProcessing, {'colorGrading': #colorGrading#})>
		<cfset structAppend(postProcessing, {'gradingSaturation': #GradingSaturation#})>
		<cfset structAppend(postProcessing, {'gradingContrast': #GradingContrast#})>
		<cfset rgbColor = {"r":GradingMixerR, "g": GradingMixerG, "b":GradingMixerB}>
		<cfset structAppend(postProcessing, {'gradingMixer': #rgbColor#})>
		<cfset structAppend(postProcessing, {'blackIn': #blackIn#, 'whiteIn': #whiteIn#})>
	</cfif>
	
	<cfif antiAlias>
		<cfset structAppend(postProcessing, {'antiAlias': #antiAlias#})>
	</cfif>
	
	<cfif scrReflection>
		<cfset structAppend(postProcessing, {'screenReflection': #scrReflection#})>
	</cfif>
	
	<cfif motionBlur>
		<cfset structAppend(postProcessing, {'motionBlur': #motionBlur#})>
	</cfif>
	
	<cfset structAppend(camera, {'postProcessing': #postProcessing#})>
	<cfset structAppend(assetData, {'camera': #camera#})>
	
	<cfset cameraProcessing = {}>
	
	<cfif ar IS 1>
	
		<cfset augmented = {}>
		
		<cfset structAppend(augmented, {'ar': #ar#})>
		
		<cfset structAppend(augmented, {'ar': #ar#})>
		<cfset structAppend(augmented, {'scale': #ar_scale#})>
		
		<cfif scaleRange IS 1>
			<cfset structAppend(augmented, {'scaleRange': #scaleRange#})>
			<cfset structAppend(augmented, {'ar_Smin': #ar_Smin#})>
			<cfset structAppend(augmented, {'ar_Smax': #ar_Smax#})>
		<cfelse>
			<cfset structAppend(augmented, {'scaleRange': 0})>
		</cfif>
		
		<cfif ar_rotationRange IS 1>
			<cfset structAppend(augmented, {'rotationRange': #ar_rotationRange#})>
		<cfelse>
			<cfset structAppend(augmented, {'rotationRange': 0})>
		</cfif>
			
		<cfset structAppend(assetData, {'ar': #augmented#})>
		
	</cfif>
	
</cfif>

<!--- Update Asset --->
<cfinvoke component="CFC.Assets" method="updateAssetDetails" returnvariable="assetTypeID">
	<cfinvokeargument name="assetData" value="#assetData#"/>
	<cfinvokeargument name="detailData" value="#detailData#"/>
	<cfinvokeargument name="thumbData" value="#thumbData#"/>
    <cfinvokeargument name="colorData" value="#colorData#"/>
    <cfinvokeargument name="groupID" value="#subgroupID#"/>
</cfinvoke>


<!--- If Dynamic Content, Refresh Data for Asset --->
<cfif useDynamic IS 1>
    <cfinvoke component="CFC.Modules" method="generateAssetData" returnvariable="success">
          <cfinvokeargument name="appID" value="#session.appID#"/>
          <cfinvokeargument name="assetID" value="#assetID#"/>
     </cfinvoke>
</cfif>

<cflocation url="AssetsView.cfm?assetID=#assetID#&assetTypeID=#assetTypeID#&error=Updated Asset Details" addtoken="no">
    

