<link href="../styles.css" rel="stylesheet" type="text/css">

<script type="text/javascript">

var assets3DFiles;

var js3DModel = new model3DManager();

function delete3DAssetConfirm() {
		if(confirm('Are you sure you wish to Delete this Asset?'))
		{
			return true;
		}else{ 
			return false;
		}
}

function delete3DTargetAssetConfirm() {
		if(confirm('Are you sure you wish to Delete this Target from the AR Model?'))
		{
			return true;
		}else{ 
			return false;
		}
}

function delete3DAsset(theForm)
{
	if(delete3DAssetConfirm())
	{
		the3DType = theForm.OS.selectedIndex;
	
		js3DModel.setSyncMode();
		js3DModel.setCallbackHandler(successDeleted3DAsset);
		
		js3DModel.delete3DModel(<cfoutput>#assetID#</cfoutput>,the3DType);
	}
	
}

function deleteARAsset(theType)
{
	if(delete3DTargetAssetConfirm())
	{
		js3DModel.setSyncMode();
		js3DModel.setCallbackHandler(successARDeleted3DAsset);
		
		js3DModel.delete3DModelTarget(<cfoutput>#assetID#</cfoutput>,theType);
	}
	
}

function successARDeleted3DAsset(theInfo)
{
	location.reload();
}


function successDeleted3DAsset(theInfo)
{
	assets3DFiles = theInfo;	
	the3DType = document.getElementById('OS');
	updateFileModel();
}


function setOSType()
{
		the3DType = document.getElementById('OS');
		theObj = the3DType[the3DType.selectedIndex].value;
		
		osTypes = {0:'ios.png',1:'android.png',2:'osx.png',3:'pc.png'};
		document.osType.src = 'images/'+ osTypes[theObj];
		
		updateFileModel();
}

function updateFileModel()
{
		the3DType = document.getElementById('OS');
		theObj = the3DType[the3DType.selectedIndex].value;
		
		Element3DFIle = document.getElementById('asset3D');
		theFile = assets3DFiles[theObj];

		if(theFile == ''){
			theFile = 'No File Exists';
		}
		
		Element3DFIle.innerHTML = theFile;
}


function showAR()	{
	
	checkboxObj = document.getElementById('ar');

	var theObj = document.querySelector("#arView");
	<!--- var theObjCam = document.querySelector("#cameraView"); --->
	
	if(checkboxObj.checked)
	{
		removeClass(theObj,"arHide");
		addClass(theObj, "arShow");
	
		removeClass(theObjCam,"arShow");
		addClass(theObjCam, "arHide");
		
	}else{
		removeClass(theObj,"arShow");
		addClass(theObj, "arHide");
		
		removeClass(theObjCam,"arHide");
		addClass(theObjCam, "arShow");
	}
}

function set3DControl(theObj) {
		
		theObjRef = document.getElementById(theObj);
	
		theObjState = document.getElementById('C'+theObj);

		if(theObjRef.className == "checkmarkSelected")
		{
			state = false;
			checked = 0;
		}else{
			state = true;
			checked = 1;
		}
	
		theObjState.value = checked;
		setCheckmarkState3D(theObj,state);
}

function setCheckmarkState3D(theObj,state)	{
	
	theObjRef = document.getElementById(theObj);
		
		if(state)
		{
			removeClass(theObjRef,"checkmarkNormal");
			addClass(theObjRef, "checkmarkSelected");
			
		}else{
			removeClass(theObjRef,"checkmarkSelected");
			addClass(theObjRef, "checkmarkNormal");
		}
}

function setARType(arAssetID)
{
	js3DModel.setSyncMode();
	js3DModel.setCallbackHandler(successAR3DAsset);
	js3DModel.set3DModelARMarker(<cfoutput>#assetID#</cfoutput>,arAssetID);
}

function successAR3DAsset(result)
{
	if(result){
		location.reload();
	}
}

function update3DObjectDate()
{
	if(confirm('Are you sure you wish to Update the 3D Asset?'))
	{
		js3DModel.setSyncMode();
		js3DModel.setCallbackHandler(update3DObjectDateSuccess);
		js3DModel.set3DModelDate(<cfoutput>#assetID#</cfoutput>);
	
	}else{ 
		//nothing
	}
}

function update3DObjectDateSuccess(result)
{
	if(result == true)
	{
		alert('3D Asset Updated');
	}else{
		alert('Already Updated');
	}
}


</script>

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfinvoke component="CFC.Assets" method="get3DModelARMarkers" returnvariable="arAssets">
        <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>


<script type="text/javascript">
	<cfoutput>
	assets3DFiles = ['#asset.url#','#asset.url_android#','#asset.url_osx#','#asset.url_windows#'];
	</cfoutput>
</script>

<script type="text/javascript" src="instanceName.js"></script>

<cfinvoke  component="CFC.Apps" method="getAssetPath" returnvariable="assetPath">
  <cfinvokeargument name="assetID" value="#assetID#"/>
  <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
</cfinvoke>

<cfset theModel = ''>
<cfif asset.url NEQ ''>
	<cfset theModel = '#assetPath##asset.url#'>
</cfif>

<cfparam name="ar" default="no">

<cfif structKeyExists(asset,'ar')>
	<cfif asset.ar NEQ ''>
        <cfset ar = asset.ar>
    <cfelse>
        <cfset ar = 0>
    </cfif>
</cfif>

<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td align="right" valign="middle">Asset Name</td>
      <td valign="middle"><label for="OS"></label>        <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128" /></td>
    </tr>
    <tr>
      <td colspan="2" align="right" valign="middle"><hr size="1" /></td>
    </tr>
    <tr>
      <td width="80" align="right" valign="middle">OS</td>
      <td>
      <table width="100%" border="0">
          <tr>
          <td width="32"><img src="images/ios.png" name="osType" width="32" height="32" id="osType" /></td>
          <td width="200"><select name="OS" id="OS" onchange="setOSType()">
            <option value="0">Apple Mobile IOS</option>
            <option value="1">Google Mobile Android</option>
            <option value="2">Mac OSX</option>
            <option value="3">PC Windows</option>
          </select></td>
          <td>
          <input name="delete3D" type="button" id="delete3D" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="delete3DAsset(this.form);" value="" />
          <input name="update3D" type="button" id="update3D" style="background:url(images/update.png) no-repeat; border:0; width:44px; height:44px;" onClick="update3DObjectDate();" value="" />
          </td>
          </tr>
    </table>
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">FBX Model</td>
      <td>
      <span id="asset3D" class="contentHilighted">#asset.url#</span>
	<cfif NOT fileExists(theModel)>
      <cfif asset.url IS ''>
        <script type="text/javascript">
			updateFileModel()
		</script>
      <cfelse>
      <span class="contentwarning">(File Missing)</span>
      </cfif>
    </cfif>
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td><input name="fbxAsset" type="file" class="formfieldcontent" id="fbxAsset" style="padding-left:0" maxlength="255" onchange="checkInstanceName(this.form,this);checkFileExtention(this.name,'.unity3d')" /></td>
    </tr>
    <tr>
      <td colspan="2" align="right" valign="bottom"><hr size="1" /></td>
    </tr>

  </table>

    <!--- <div class="<cfif ar>arHide<cfelse>arShow</cfif>" id="cameraView">   --->  
    <div class="arShow" id="cameraView" style="padding-top:10px">   
  <table width="800" border="0" cellpadding="0" cellspacing="10" class="content" bgcolor="##EEE">
    <tr>
      <td width="80" align="right" valign="top"><img src="images/camera-xyz.png" width="44" height="44" style="padding-top:26px" /></td>
      <td>
      
      <table width="100%" border="0">
          <tr>
            <td align="right" valign="top">
            
            <table width="100%" border="0">
              <tr>
                <td width="14%" class="content">&nbsp;</td>
                <td width="2%" class="content">&nbsp;</td>
                <td colspan="5" class="content">Camera Position in Scene</td>
                </tr>
              <tr>
                <td class="content"><img src="images/model-xyz.png" width="44" height="44" /></td>
                <td class="content">X</td>
                <td width="27%"><label for="CamMin"></label>
                  <input name="CamX" type="text" class="formfieldcontent" id="CamX" style="width:60px" value="#asset.camPos_x#" /></td>
                <td width="4%" class="content">Y</td>
                <td width="24%"><input name="CamY" type="text" class="formfieldcontent" id="CamY" style="width:60px" value="#asset.camPos_y#" /></td>
                <td width="4%" class="content">Z</td>
                <td width="25%"><input name="CamZ" type="text" class="formfieldcontent" id="CamZ" style="width:60px" value="#asset.camPos_z#" /></td>
                </tr>

            </table>
            <table width="100%" border="0" style="margin-top:25px">
              <tr>
                <td width="14%" class="content">&nbsp;</td>
                <td width="3%" class="content">&nbsp;</td>
                <td colspan="5" class="content">Camera Target Position</td>
                </tr>
              <tr>
                <td class="content"><img src="images/cameratarget-xyz.png" width="44" height="44" /></td>
                <td class="content">X</td>
                <td width="25%"><input name="TargetX" type="text" class="formfieldcontent" id="TargetX" style="width:60px" value="#asset.camTarget_x#" /></td>
                <td width="4%" class="content">Y</td>
                <td width="25%"><input name="TargetY" type="text" class="formfieldcontent" id="TargetY" style="width:60px" value="#asset.camTarget_y#" /></td>
                <td width="4%" class="content">Z</td>
                <td width="25%"><input name="TargetZ" type="text" class="formfieldcontent" id="TargetZ" style="width:60px" value="#asset.camTarget_z#" /></td>
                </tr>
            </table>
            
            </td>
            
            <td align="right" valign="top">&nbsp;</td>
            
            <td align="left" valign="top">
            
            <cfif asset.camPan IS 0><cfset camPan = false><cfelse><cfset camPan = true></cfif>
            <cfif asset.camZoom IS 0><cfset camZoom = false><cfelse><cfset camZoom = true></cfif>
            <cfif asset.camRotation IS 0><cfset camRotation = false><cfelse><cfset camRotation = true></cfif>
            
            
            <table width="100%" border="0">
              <tr>
                <td width="14%" class="content">&nbsp;</td>
                <td colspan="4" class="content">Camera Control (0 to 360 degrees)</td>
              </tr>
              <tr>
                <td align="right" class="content"><img src="images/camera-tilt.png" width="44" height="44" /></td>
                <td align="right" class="content">Tilt Up/Dn </td>
                <td align="right" class="content">
                <cfif camPan><cfset theState = "checkmarkSelected"><cfelse><cfset theState = "checkmarkNormal"></cfif>
                  <input name="pan" type="button" id="pan" class="#theState#" onClick="set3DControl(this.name);" />
                  <input name="Cpan" type="hidden" id="Cpan" value="#asset.camPan#" />
                </td>
                <td align="right" class="content">
                <input name="CamTiltMin" type="text" class="formfieldcontent" id="CamTiltMin" style="width:60px" value="#asset.tiltMin#" /> min
                </td>
                <td width="100" class="content">
                <input name="CamTiltMax" type="text" class="formfieldcontent" id="CamTiltMax" style="width:60px" value="#asset.tiltMax#" /> 
                max</td>
                </tr>
              <tr>
                <td align="right" class="content"><img src="images/camera-zoom.png" width="44" height="44" /></td>
                <td align="right" class="content">Zoom in/out
                
                </td>
                <td align="right" class="content">
                <cfif camZoom><cfset theState = "checkmarkSelected"><cfelse><cfset theState = "checkmarkNormal"></cfif>
                  <input name="zoom" type="button" id="zoom" class="#theState#" onClick="set3DControl(this.name);" />
                  <input name="Czoom" type="hidden" id="Czoom" value="#asset.camZoom#" />
                </td>
                <td width="100" align="right" class="content">
                <input name="CamMin" type="text" class="formfieldcontent" id="CamMin" style="width:60px" value="#asset.camMin#" /> min
                </td>
                <td width="100" class="content">
                  <input name="CamMax" type="text" class="formfieldcontent" id="CamMax" style="width:60px" value="#asset.camMax#" /> max
                </td>
                </tr>
              <tr>
                <td align="right" class="content"><img src="images/camera-pan.png" width="44" height="44" /></td>
                <td align="right" class="content">Pan left/right</td>
                <td align="right" class="content">
                <cfif camRotation><cfset theState = "checkmarkSelected"><cfelse><cfset theState = "checkmarkNormal"></cfif>
                  <input name="rotation" type="button" id="rotation" class="#theState#" onClick="set3DControl(this.name);" />
                  <input name="Crotation" type="hidden" id="Crotation" value="#asset.camRotation#" />
                </td>
                <td align="right" class="content">
                <input name="CamPanMin" type="text" class="formfieldcontent" id="CamPanMin" style="width:60px" value="#asset.panMin#" /> min
                </td>
                <td class="content">
                <input name="CamPanMax" type="text" class="formfieldcontent" id="CamPanMax" style="width:60px" value="#asset.panMax#" /> max
                </td>
                </tr>
              <tr>
                <td align="left" class="content">&nbsp;</td>
                <td height="44" colspan="4" align="left" class="content">Please note that 360 in both min and max will allow for free rotation for that axis</td>
                </tr>
            </table>
            </td>
          </tr>
      </table>
   
      </td>
    </tr>
  </table>
  </div>   
    <table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    <tr>
      <td width="80" align="right" valign="top"><img src="images/camera-xyz.png" width="44" height="44" style="padding-top:26px" /></td>
      <td>
      <table width="100%" border="0">
        <tr>
          <td class="content">&nbsp;</td>
          <td align="left" class="content">&nbsp;</td>
          <td align="left" class="content">Camera Field of View</td>
          </tr>
        <tr>
          <td width="44" class="content"><img src="images/camerafov.png" width="44" height="44" /></td>
          <td width="10" align="right" class="content">F</td>
          <td width="622" class="content"><input name="FOV" type="text" class="formfieldcontent" id="FOV" style="width:60px" value="#asset.fov#" /></td>
          </tr>
      </table>
      </td>
    </tr>
    <cfif asset.url NEQ ''>
    </cfif>
 </table>
    <table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    <tr>
      <td width="80" align="right" valign="top"><img src="images/model.png" width="44" height="44" style="padding-top:26px" /></td>
      <td><table width="100%" border="0">
        <tr>
          <td width="287" class="content"><table width="100%" border="0">
            <tr>
              <td width="44" class="content">&nbsp;</td>
              <td width="2%" class="content">&nbsp;</td>
              <td colspan="5" class="content">Model Position in Scene</td>
              </tr>
            <tr>
              <td class="content"><img src="images/model-xyz.png" alt="" width="44" height="44" /></td>
              <td class="content">X</td>
              <td><label for="TargetX"></label>
                <input name="locX" type="text" class="formfieldcontent" id="textfield4" style="width:60px" value="#asset.Loc_x#" /></td>
              <td width="4%" class="content">Y</td>
              <td><input name="locY" type="text" class="formfieldcontent" id="locY" style="width:60px" value="#asset.Loc_y#" /></td>
              <td width="4%" class="content">Z</td>
              <td><input name="locZ" type="text" class="formfieldcontent" id="textfield10" style="width:60px" value="#asset.Loc_z#" /></td>
              </tr>
            <tr>
              <td class="content">&nbsp;</td>
              <td class="content">&nbsp;</td>
              <td colspan="5" class="content">Model Rotation in Scene</td>
              </tr>
            <tr>
              <td class="content">&nbsp;</td>
              <td class="content">X</td>
              <td><input name="rotX" type="text" class="formfieldcontent" id="rotX" style="width:60px" value="#asset.Rot_x#" /></td>
              <td class="content">Y</td>
              <td><input name="rotY" type="text" class="formfieldcontent" id="rotY" style="width:60px" value="#asset.Rot_y#" /></td>
              <td class="content">Z</td>
              <td><input name="rotZ" type="text" class="formfieldcontent" id="rotZ" style="width:60px" value="#asset.Rot_z#" /></td>
              </tr>
          </table></td>
          <td width="290" valign="top" class="content"><table width="100%" border="0">
            <tr>
              <td width="44" class="content">&nbsp;</td>
              <td class="content">&nbsp;</td>
              <td class="content">Scale of Model in Scene</td>
              </tr>
            <tr>
              <td class="content"><img src="images/modelscale.png" width="44" height="44" /></td>
              <td class="content">S</td>
              <td><label for="TargetY"></label>
                <input name="scale" type="text" class="formfieldcontent" id="textfield5" style="width:60px" value="#asset.scale#" /></td>
              </tr>
          </table></td>
        </tr>
      </table>
      </td>
    </tr>
  </table>
</cfoutput>

