<cfset serverVr = 11>
<cfparam name="editP" default="NO">
<cfparam name="edit" default="NO">
<cfparam name="assetTypeTab" default="0">
<cfparam name="accessLevel" default="0">

<cfif NOT isDefined('session.serverVersion')>
	<cfset session.serverVersion = serverVr>
<cfelse>
	<cfset session.serverVersion = serverVr>
</cfif>

<style type="text/css">
body {
	margin-top: 0px;
}
</style>

<cfif NOT IsDefined("session.clientID")><cfset session.clientID = 0></cfif>

<cfinvoke component="CFC.Apps" method="getPaths" returnvariable="assetPathsCur">
    <cfinvokeargument name="clientID" value="#session.clientID#"/>
    <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>


<cfinvoke  component="CFC.Clients" method="getClientInfo" returnvariable="clientsQuery">
  <cfinvokeargument name="clientID" value="#session.clientID#"/>
  <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>

<!--- JUST FOR LOCAL --->
<cfset session.userAccess = 5>
<!---  --->

<cfif session.userAccess NEQ '' OR session.userAccess NEQ 0>

	<cfinvoke component="CFC.registration" method="authenticateUser" returnvariable="al">
		<cfinvokeargument name="token" value="#session.userAccess#"/>
	</cfinvoke>

	<cfif al.success>
		<cfset accessLevel = al.access>
	<cfelse>
		<cfset accessLevel = 0>
	</cfif>

</cfif>
	
<div style="background-color:#666; width:810px;">
<cfoutput>
<div class="mainheading" style="width:810px; height:100px; padding:0px; background-image:url('images/live-platform.png'); background-repeat:no-repeat;">
	
  <cfif error NEQ ''>
    <div class="contentwarning" style="margin-top:12px; margin-left:155px; float:left">#error#</div>
  </cfif>
  
  <cfif assetTypeTab IS 0>
   <div class="contentLinkWhite" style="margin-top:12px; margin-right:10px; float:right; text-align:right">
   	<cfif isDefined('assetPathsCur.application.api')>
    <div>
    <cfset vr = assetPathsCur.application.api>
    <cfset wrongAPI = "##C00"><cfset rightAPI = "##0C0">
     App Server: V#vr#
    </div>
    </cfif>
    <cfif session.appID GT 0 AND assetTypeTab IS 0 AND NOT editP AND NOT edit>
    	<div style="margin-top:10px">
       <a href="exportJSON.cfm" style="cursor:pointer; color:##FFFFFF; text-decoration:none">Export JSON</a>
        | Refresh
        <!--- <cfif session.userDev IS 0> --->
        <a onClick="refreshAllJSON(0,#vr#);" style="cursor:pointer">PROD</a> |
		<!--- </cfif> --->
        <a onClick="refreshAllJSON(1,#vr#);" style="cursor:pointer">DEV</a></div>
    </cfif>
   </div>
  </cfif>
  
</div>

<div style="width:790px; background-color:##333; height:32px; padding-left:10px; padding-right:10px; padding-top:8px">

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
		<a href="ClentsView.cfm?clientID=0" class="sectionActiveLink">Clients</a> | <a href="users.cfm?clientID=0" class="sectionActiveLink">Users</a>
    <cfif session.clientID GT 0>
     <span class="sectionLink">|</span>
     
     <cfif session.appID GT 0> 
     <a href="ClentsView.cfm?clientID=#session.clientID#" class="sectionActiveLink">#assetPathsCur.client.name#</a>
     <cfelse>
     <span class="sectionLink">#assetPathsCur.client.name#</span>
     </cfif>
     
    </cfif>
    <cfif session.appID GT 0>
        <span class="sectionLink">| #assetPathsCur.application.name#</span>
    </cfif>
    </td>
    <td align="right">
    	<a href="../../cms" class="sectionActiveLink">Logout</a>
	  </td>
  </tr>
</table>
	
</div>

</cfoutput>
</div>