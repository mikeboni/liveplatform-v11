<link href="../styles.css" rel="stylesheet" type="text/css">

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <tr>
      <td align="right" valign="middle">      URL</td>
      <td valign="middle">
      <input name="URLLink" type="text" class="formfieldcontent" id="URLLink" value="#asset.url#" size="80" maxlength="255" />
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td valign="middle">
      <a href="#asset.url#" target="_new" class="contentLink">View Web Link</a>
      </td>
    </tr>
 </table>
</cfoutput>