<link href="../styles.css" rel="stylesheet" type="text/css">

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfinvoke component="CFC.Assets" method="get3DModelARMarkers" returnvariable="arAssets">
        <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>

<cfif asset.markerasset_id NEQ ''>

    <cfinvoke component="CFC.Assets" method="getAssets" returnvariable="markerAsset">
            <cfinvokeargument name="appID" value="#session.appID#"/>
            <cfinvokeargument name="AssetID" value="#asset.markerasset_id#"/>
    </cfinvoke>

	<cfif markerAsset.recordCount GT 0>
    
        <cfinvoke component="CFC.CMS" method="getAssetTypeIcon" returnvariable="theTypeIcon">
                <cfinvokeargument name="assetTypeID" value="#markerAsset.assetType_id#"/>
        </cfinvoke>
    <cfelse>
    	<cfset theTypeIcon = "">
    </cfif>

<cfelse>
	<cfset theTypeIcon = "">
</cfif>

<cfif isDefined('markerAsset')> 
	<cfif markerAsset.recordCount GT 0>
		<cfset mAssetID = markerAsset.asset_id>
    <cfelse>
    	<cfset mAssetID = 0>
    </cfif>
<cfelse>
	<cfset mAssetID = 0>
</cfif>

<script type="text/javascript" src="instanceName.js"></script>

<script type="text/javascript">

var js3DModel = new model3DManager();

function setARType(arAssetID)
{
	js3DModel.setSyncMode();
	js3DModel.setCallbackHandler(successAR3DAsset);
	js3DModel.set3DModelARMarker(<cfoutput>#assetID#</cfoutput>,arAssetID);
}

function successAR3DAsset(result)
{
	if(result){
		location.reload();
	}
}


var jsAssets = new assetManager();

function displayAssets(theType,theSelectedID)
{
	jsAssets.setSyncMode();
	jsAssets.setCallbackHandler(successAssetListing);
	jsAssets.getAllAssetsSelection(<cfoutput>#session.appID#</cfoutput>,theType,<cfoutput>#mAssetID#</cfoutput>);
}

function successAssetListing(result)
{
	theAssets = document.getElementById('displayAssets');
	theAssets.innerHTML = result;
}


function setCheckmarkState(theObj,state)	{
	
	if(theObj.className == 'checkmarkNormal')
	{
		theObj.className = "checkmarkSelected";
	}else{
		theObj.className = "checkmarkNormal";
	}
	
}

function setARAsset(theForm)
{
	
	theSel = theForm.markerAsset
	theAssetSel = theSel.options[theSel.selectedIndex].value
	
	js3DModel.setSyncMode();
	js3DModel.setCallbackHandler(successSetArAsset);
	js3DModel.setARAsset(<cfoutput>#asset.asset_id#</cfoutput>,theAssetSel);
}

function successSetArAsset(result)
{
	if(result){
		location.reload();
	}
}


function displayEdit(theObj)
{
	theInfo = document.getElementById('info');
	
	if(document.getElementById('removeAsset'))
	{
		objExists = true;
		theDel = document.getElementById('removeAsset');
	}else{
		objExists = false;	
	}
	
	if(theInfo.style.display =="block")
	{
		theInfo.style.display="none";
		theObj.style.background = "url('images/modify.png')";
		if(objExists){ theDel.style.display = 'block';}
		
	}else{
		theInfo.style.display="block";
		theObj.style.background = "url('images/cancel.png')";
		if(objExists){ theDel.style.display = 'none';}
		
	}
}

function deleteAsset()
{
	
	if(confirm('Are you sure you wish to Remove this Asset?'))
	{
		jsAssets.setSyncMode();
		jsAssets.setCallbackHandler(successAssetRemoved);
		jsAssets.removeARAsset(<cfoutput>#asset.asset_id#</cfoutput>);
	
	}else{ 
		//nothing
	}
	
function successAssetRemoved()
{
	location.reload();
}	

}


</script>

<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td valign="middle"><label for="OS"></label>        <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128" /></td>
    </tr>
    <tr>
      <td colspan="2" align="right" valign="middle"><hr size="1" /></td>
    </tr>
    
    <tr>
      <td align="right"><img src="images/ar.png" width="44" height="44" /></td>
      <td>
      
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="200" valign="middle">
        <select name="arType" id="arType" onchange="setARType(this.options[this.selectedIndex].value)" style="height:44px; margin-right:10px; padding-left:0px">
        <option value="0">No Augemented Reality</option>
        <cfloop query="arAssets">
        	<option value="#asset_id#" <cfif asset.marker_id IS asset_id>selected</cfif>>#name#</option>
        </cfloop>
      </select> 
        </td>
        <cfif asset.marker_id NEQ ''>
        <td width="100" align="left" valign="middle">
          <cfinvoke component="CFC.Assets" method="get3DModelARMarker" returnvariable="arTypes">
                <cfinvokeargument name="assetID" value="#asset.marker_id#"/>
          </cfinvoke>
         
          <cfset disabled = .3>
		  <cfif arTypes.vuforia><cfset opU = 1><cfelse><cfset opU=disabled></cfif>
          <cfif arTypes.moodstocks><cfset opV = 1><cfelse><cfset opV=disabled></cfif>
          <cfif arTypes.visualizer><cfset opM = 1><cfelse><cfset opM=disabled></cfif>

          <img src="images/visualizer.png" style="padding:1px; opacity:#opV#" />
          <img src="images/moodstocks.png" style="padding:1px; opacity:#opM#" />
          <img src="images/vuforia.png" style="padding:1px; opacity:#opU#" />
        </td>
        </cfif>
        <td align="left">
        <cfif asset.marker_id NEQ ''>
          <a href="AssetsView.cfm?assetID=#asset.marker_id#&amp;assetTypeID=15" class="contentLink">Edit Marker</a> |
        </cfif>
        <a href="AppsView.cfm?assetTypeTab=3&amp;assetTypeID=15" class="contentLink">Add New Marker</a> 
          </td>
      </tr>
    </table>

      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">AR Asset</td>
      <td valign="middle">
    
        <form method="post">
          
          <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <cfif mAssetID GT 0>
              <td align="left" width="54">#theTypeIcon#</td>
            </cfif>
            <td align="left">
            <span class="contentLinkGrey">
            <cfif mAssetID IS 0>
                <span class="contentLinkGreen">No Asset Assigned</span>
            <cfelse>
                #markerAsset.assetName#
            </cfif>
            </span>
              <input name="markerAssetID" type="hidden" id="markerAssetID" value="#mAssetID#" />
              
              </td>
              <cfif mAssetID GT 0>
              <td width="44" align="right">
              <input name="removeAsset" type="button" id="removeAsset" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onclick="deleteAsset()" value="" />
              </td>
              </cfif>
              <td align="right">
              
            <div id="info" style="display:none">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="44">
                <cfset defaultAssetType = 6>
                <select name="arTypeList" id="arTypeList" onchange="displayAssets(this.options[this.selectedIndex].value)" class="itemShow" style="background:url(images/3dmodel.png) no-repeat; padding-left:24px; border:0; width:44px; height:44px;" type="button" />
                  <cfloop query="session.assetstypes">
                  <cfif type IS defaultAssetType>
                  	<option value="#type#" selected="selected">#name#</option>
                  <cfelse>
                    <option value="#type#">#name#</option>
                 </cfif>
                </cfloop>
            	</select> 
              
              </td>
              <td align="left">
              <div id="displayAssets" style="width:auto; padding-left:10px"></div>
        	   
               <!--- <cfif NOT isDefined('markerAsset')>
               		<cfset mAssetID = 0>
               </cfif>
               		<cfset mAssetID = asset.markerAsset_id> --->
               
				<script type="text/javascript">
                    displayAssets(#defaultAssetType#,#mAssetID#)
                </script>
                
              </td>
              <td width="44" align="left">
              <input value="" style="background:url(images/ok.png) no-repeat; border:0; width:44px; height:44px;" type="button" onclick="setARAsset(this.form)">
              </td>
              </tr>
            </table>
            </div>
              
              </td>
            <td width="44" align="right"><input value="" style="background:url(images/modify.png) no-repeat; border:0; width:44px; height:44px;" type="button" onclick="displayEdit(this)" /></td>
              
            </tr>
          </table>
        </form>
 
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td valign="middle" class="contentLinkGrey">&nbsp;</td>
    </tr>
  </table>

    <!--- <div class="<cfif ar>arHide<cfelse>arShow</cfif>" id="cameraView">   --->  
  <div class="arShow" id="cameraView" style="padding-top:10px"></div>
</cfoutput>

