// JavaScript Document

function checkInstanceName(form,theObj)
{
	theURL = theObj.value;
	
	theFile = theURL.split("\\").pop();
	theFileName = theFile.split(".");
	
	theName = wordCaps(theFileName[0]);
	
	if(form.assetName.value == '')
	{
		form.assetName.value = theName;
	}
}

function wordCaps(str)
{
	return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}