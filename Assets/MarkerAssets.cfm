<link href="../styles.css" rel="stylesheet" type="text/css">

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<script type="text/javascript">

function showMarkerContent(theObj)
{
		theContent = theObj + 'Content';
	
		theDiv = document.getElementById(theContent);
		console.log(theContent);
		if(theDiv != null)
		{
			
			if(theDiv.style.display == 'none')
			{
				theDiv.style.display='block';
				updateMarkerAssetType(theObj,1);
			}else{
				theDiv.style.display='none';
				updateMarkerAssetType(theObj,0);	
			}
		
		}
		
}
</script>

<script type="text/javascript">

var jsMarker = new model3DManager();


updateMarkerAssetType = function(markerType,theState) 
{
	jsMarker.setSyncMode();
	//jsMarker.setCallbackHandler(showMarkerUpdated);
	jsMarker.setMarkerAssetType(<cfoutput>#assetID#</cfoutput>,markerType,theState);
	
}

showMarkerUpdated = function() 
{
	location.reload();
}

</script>

<cfif asset.useVisualizer IS '' OR asset.useVisualizer IS 0>
	<cfset useVisualizer = false>
<cfelse>
	<cfset useVisualizer = true>
</cfif>

<cfif asset.useMoodstocks IS '' OR asset.useMoodstocks IS 0>
	<cfset useMoodstocks = false>
<cfelse>
	<cfset useMoodstocks = true>
</cfif>

<cfif asset.useVuforia IS '' OR asset.useVuforia IS 0>
	<cfset useVuforia = false>
<cfelse>
	<cfset useVuforia = true>
</cfif>

<cfinvoke  component="CFC.Apps" method="getAssetPath" returnvariable="assetPath">
  <cfinvokeargument name="assetID" value="#assetID#"/>
  <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
</cfinvoke>

<cfset theImage = ''>
<cfif asset.url NEQ ''>
	<cfset theImage = '#assetPath##asset.url#'>
</cfif>

<cfoutput>

<table width="820" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Marker Name</td>
      <td>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <tr>
    <td align="right">Default Mode</td>
    <td>
    <select name="arMode" class="formText">
    	<option value="0" <cfif asset.armode IS ''> selected</cfif>>None</option>
    	<option value="1" <cfif asset.armode IS 1> selected</cfif>>AR Mode (Default)</option>
        <option value="0" <cfif asset.armode IS 0> selected</cfif>>3D Model View</option>
    </select>
    Only for 3D to specify the Default mode (3D model or AR view)
    </td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="middle" bgcolor="##EEE" class="bundleid" style="padding:10px">
      
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td>Visualizer (Klokwerks)</td>
        <td align="right"><input name="ckvisualizer" type="checkbox" id="ckvisualizer" onchange="showMarkerContent(this.name);" value="true" <cfif useVisualizer>checked="checked"</cfif> />
      <label for="ckvisualizer"></label></td>
      </tr>
    </table>

      </td>
    </tr>
    
    <tr>
      <td colspan="2" align="right" valign="middle">

      <cfif useVisualizer>
      	<cfset showContent = 'block'>
      <cfelse>
      	<cfset showContent = 'none'>
      </cfif>
      
      <div id="ckvisualizerContent" style="display:#showContent#">
      <table width="100%" border="0" cellpadding="0" cellspacing="10" class="content">
      <cfif asset.url NEQ ''>
      <tr>
        <td width="80" align="right" valign="bottom"><input name="assetPath" type="hidden" id="assetPath" value="#assetPath#" /></td>
        <td>
      
          <cfif fileExists(theImage)>
            <a href="#theImage#" target="_new" class="contentLink">
              <img src="#theImage#" alt="#theImage#" height="221" border="1" class="imgLockedAspect" />
              </a>
            <cfelse>
            	<cfinclude template="noFile.cfm">
            </cfif>
         
          </td>
        </tr>
    </cfif> 
    <cfif asset.url NEQ ''>
    <tr>
      <td width="80" align="right" valign="middle">Target Image</td>
      <td valign="middle" class="contentHilighted">
      #asset.url#
      <cfif NOT fileExists(theImage)>
      <span class="contentwarning">(File Missing)</span>
      </cfif>
      </td>
    </tr>
    </cfif>
    <tr>
      <td width="80" align="right" valign="middle">Image </td>
      <td><input name="markerAsset" type="file" class="formfieldcontent" id="markerAsset" style="padding-left:0;width:500px" maxlength="255" onchange="checkFileExtention(this.name,'.jpg,.png')" /></td>
    </tr>
    <cfif asset.webURL NEQ ''>
    <tr>
      <td align="right" valign="middle">Web Link<br />
        Target Image</td>
      <td>
          <a href="#theImage#" target="_new" class="contentLink">
          <img src="#asset.webURL#" alt="#theImage#" height="221" border="1" class="imgLockedAspect" />
          </a> 
      </td>
    </tr>
    </cfif>
    <tr>
      <td width="80" align="right" valign="middle">Web Link </td>
      <td><input name="webURL" type="text" class="formfieldcontent" id="webURL" value="#asset.webURL#" style="width:660px" maxlength="255" onclick="this.form.markerAsset.value=''" /></td>
    </tr>
    <tr>
      <td width="80" align="right" valign="middle">Key</td>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="100"><input name="keySet" type="text" class="formfieldcontent" style="width:100px" id="keySet" value="#asset.keySet#" maxlength="128" /></td>
          <td width="50" align="right" class="content"><span style="padding-right:10px">Data</span></td>
          <td><input name="keyData" type="text" class="formfieldcontent" style="width:500px" id="keyData" value="#asset.keyData#" maxlength="128" /></td>
        </tr>
      </table></td>
    </tr>
      </table>
      </div>
    
      </td>
      </tr>
    
    
    <tr>
      <td colspan="2" align="left" valign="middle" bgcolor="##EEE" class="bundleid" style="padding:10px">
        
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>MoodStocks</td>
            <td align="right"><input name="moodstocks" type="checkbox" id="moodstocks" onchange="showMarkerContent(this.name);" value="true" <cfif useMoodstocks>checked="checked"</cfif> />
              <label for="moodstocks"></label></td>
            </tr>
          </table>
        
        </td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="middle">
      <cfif useMoodstocks>
      	<cfset showContent = 'block'>
      <cfelse>
      	<cfset showContent = 'none'>
      </cfif>
      
      <div id="moodstocksContent" style="display:#showContent#">
      <table width="100%" border="0" cellpadding="0" cellspacing="10" class="content">
          <tr>
          <td width="80" align="right" valign="middle">API Key</td>
          <td><input name="APIKey" type="text" class="formfieldcontent" id="APIKey" value="#asset.APIKey#" size="60" maxlength="128" /></td>
        </tr>
        <tr>
          <td align="right" valign="middle">API Secret</td>
          <td><input name="APISecret" type="text" class="formfieldcontent" id="APISecret" value="#asset.APISecret#" size="60" maxlength="128" /></td>
        </tr>
    </table>
      </div>
    
      </td>
      </tr>
    
     <tr>
      <td colspan="2" align="left" valign="middle" bgcolor="##EEE" class="bundleid" style="padding:10px">
        
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td>VuForia (Qualcomm)</td>
            <td align="right"><input name="vuforia" type="checkbox" id="vuforia" onchange="showMarkerContent(this.name);" value="true" <cfif useVuforia>checked="checked"</cfif> />
              <label for="vuforia"></label></td>
            </tr>
          </table>
        
        </td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="middle">
      <cfif useVuforia>
      	<cfset showContent = 'block'>
      <cfelse>
      	<cfset showContent = 'none'>
      </cfif>
      
      <cfset xmlPath = ''>
      <cfif asset.xml NEQ ''>
      	<cfset xmlPath = assetPath & asset.xml>
      </cfif>
      
      <cfset datPath = ''>
      <cfif asset.dat NEQ ''>
      	<cfset datPath = assetPath & asset.dat>
      </cfif>
      
      <div id="vuforiaContent" style="display:#showContent#">
      <table width="100%" border="0" cellpadding="0" cellspacing="10" class="content">
          <cfif asset.xml NEQ ''>
            <tr>
              <td width="80" align="right" valign="middle">&nbsp;</td>
              <td valign="middle" class="contentHilighted">
                #asset.xml#
                <cfif NOT fileExists(xmlPath)>
                  <span class="contentwarning">(File Missing)</span>
                  </cfif>
                </td>
            </tr>
    	   <cfelse>
           <tr>
              <td width="80" align="right" valign="middle">&nbsp;</td>
              <td valign="middle" class="contentHilighted">No XML File Assigned</td>
            </tr>
           </cfif>
          <tr>
          <td width="80" align="right" valign="middle">XML File</td>
          <td><input name="ar_xml" type="file" class="formfieldcontent" id="ar_xml" style="padding-left:0; width:300px" maxlength="128" onchange="checkFileExtention(this.name,'.xml')" /></td>
        </tr>
          <tr>
            <td colspan="2" align="right" valign="middle"><hr size="1" /></td>
            </tr>
          <cfif asset.dat NEQ ''>
            <tr>
              <td width="80" align="right" valign="middle">&nbsp;</td>
              <td valign="middle" class="contentHilighted">
              #asset.dat#
              <cfif NOT fileExists(datPath)>
              <span class="contentwarning">(File Missing)</span>
              </cfif>
              </td>
            </tr>
            <cfelse>
            <tr>
              <td width="80" align="right" valign="middle">&nbsp;</td>
              <td valign="middle" class="contentHilighted">No DAT File Assigned</td>
            </tr>
            </cfif>
          <tr>
          <td width="80" align="right" valign="middle">DAT File</td>
          <td><input name="ar_dat" type="file" class="formfieldcontent" id="ar_dat" style="padding-left:0; width:300px" maxlength="128" onchange="checkFileExtention(this.name,'.dat')" /></td>
        </tr>
          <tr>
            <td align="right" valign="middle">&nbsp;</td>
            <td>Vuforia License Must be entered in the AppPrefs otherwise open license is applied</td>
          </tr>
      </table>
      </div>
    
      </td>
      </tr>
    
    
 </table>

</cfoutput>