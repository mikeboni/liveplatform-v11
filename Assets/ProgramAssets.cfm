<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfif asset.displayAsset_id GT 0>
    <cfinvoke component="CFC.Assets" method="getAssetType" returnvariable="assetType">
            <cfinvokeargument name="assetID" value="#asset.displayAsset_id#"/>
    </cfinvoke>
<cfelse>
	<cfset assetType = 0>
</cfif>

<cfinvoke component="CFC.CMS" method="getProgramDisplayAssets" returnvariable="displayAssets">
        <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>


<script>
	
	function updateDisplayAssets(displayType, selectedAsset)	{
	
	var selectedAsset = selectedAsset || 0;
	var displayAssets = <cfoutput>#displayAssets#</cfoutput>;
	
	displayTypeSel = document.getElementById("displayType");
		
		if(displayType == 1) 	{
			//images
			allAssets = displayAssets.images;
			//turn duration on
			durationObj = document.getElementById("durationSetting")	
			durationObj.style.display = "block";
			
		}else if(displayType == 2) {
			//videos
			allAssets = displayAssets.videos;
			
			type = document.getElementById("programType");
			prgType = type.options[type.selectedIndex].value;
		
			//turn duration off
			durationObj = document.getElementById("durationSetting")
			
			if(prgType == 0 || prgType == 1)	{
				durationObj.style.display = "block";
			}else{
				durationObj.style.display = "none";
			}
			
		}else{
			allAssets = {}
			document.getElementById("theDisplayVideoPreview").style.display = "none";
			document.getElementById("theDisplayPreview").style.display = "none";
		}
	
		//create options
		var i = 0;

		//remove options
		theSel = ClearOptionsFast("displayAssets");
		
		//none
		var opt = document.createElement('option');
		opt.value = 0;
		opt.innerHTML = 'None';
		
		theSel.appendChild(opt)
		
		for (var key in allAssets){
				
			var obj = allAssets[key];
			
			var opt = document.createElement('option');
			opt.value = key;
			opt.innerHTML = obj;
			
			if(selectedAsset == key) {
				opt.selected = true;
			}
			
			theSel.appendChild(opt)
		}
		
		viewDisplayAsset();
		
	}
	
	function ClearOptionsFast(id)
	{
		var selectObj = document.getElementById(id);
		var selectParentNode = selectObj.parentNode;
		var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
		selectParentNode.replaceChild(newSelectObj, selectObj);
		
		return newSelectObj;
	}
	
	
	function viewDisplayAsset()	{
		
		theSel = document.getElementById("displayAssets");
		
		img = document.getElementById("theDisplayPreview")
		vid = document.getElementById("theDisplayVideoPreview");
		
		img.style.display = "none";	
		vid.style.display = "none";	
		
		if(theSel.options.length > 0)	{
			displayAssetID = theSel.options[theSel.selectedIndex].value;
			if(displayAssetID > 0) {
				getDisplayAsset(displayAssetID);
				img.style.display = "block";	
			}else{
				img.style.display = "none";
				vid.style.display = "none";		
			}	
		}
		
	}
	
	function displaySceduleSettings(isScheduled)	{
	
		theSel = document.getElementById("scheduledTime");
		theDur = document.getElementById("durationSetting");
		
		displayTypeSel = document.getElementById("displayType");
	
		if(displayTypeSel)	{
			displayType = displayTypeSel.options[displayTypeSel.selectedIndex].value;
		}else{
			displayType = 0
		}
		
		if(isScheduled==1)	{
			theSel.style.display = "block";
			dur = document.getElementById("endTime").checked;
			if( dur == false)	{
				theDur.style.display = "block";
			}else{
				theDur.style.display = "none";
			}
		}else{
			theSel.style.display = "none";
			theDur.style.display = "block";
			document.getElementById("endTime").checked = false;
			displayEndTime(false);
		}
		
		endSel = document.getElementById("endTime").checked;
		
		//if(endSel || displayType == 2)	{
		//	theDur.style.display = "none";
		//}else{
		//	theDur.style.display = "block";
		//}
	
	}
	
</script>

<link href="../styles.css" rel="stylesheet" type="text/css"> 

<script language="javascript"> 
	
var theTime = function(epochDate)	{
	
	var myDate = new Date(epochDate *1000);	
	return { "hours":myDate.getUTCHours(), "minutes":myDate.getUTCMinutes() };
	
}

function setEpochTime(hr, mn)	{
	
	var myDate = new Date();	
	myDate.setUTCHours(hr);
	myDate.setMinutes(mn);
	
	myNewDate = convertUTCDateToLocalDate(myDate);
	
	return myNewDate.getTime();
	
}


function convertUTCDateToLocalDate(date) {
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);

    return newDate;   
}


function setProgramTime(theTime)	{
	
	if(theTime == -1 )	{
		//custom
		document.getElementById("otherDuration").style.display = "block";
		document.getElementById("customDuration").value = 10;
		theTime = document.getElementById("customDuration").value;
	}else{
		//default	
		document.getElementById("otherDuration").style.display = "none";
	}
}	

function getTimeIndex(myHr, myMin)	{
	
	var hr = 0;
	var mn = 0;
	
	var hrsList = [];
	var minList = [];
	
	//if(myHr >= 1 && myHr < 12)	{ myHr+=12; }
	
	for(i=6;i<=24;i++) { hrsList.push(i); }
	for(i=0;i<=60;i++) { minList.push(i); }
	
	var hr = hrsList.indexOf(myHr);
	var mn = minList.indexOf(myMin);
	
	return {hr:hr, mn:mn}
}


var jsAssets = new assetManager();

function updateProgramAsset()
{
	var objectData;
	
	startTimeHr = document.getElementById("startHrs");
	startTimeMn = document.getElementById("startMin");
	
	if(!startTimeHr) { startTimeHr = 0; }
	if(!startTimeMn) { startTimeMn = 0; }
	
	if(startTimeHr.selectedIndex > -1 || startTimeHr.selectedIndex > -1)	{
		STHr = startTimeHr.options[startTimeHr.selectedIndex].value;
		STMn = startTimeMn.options[startTimeMn.selectedIndex].value;
	}else{
		STHr = 0;
		STMn = 0;
	}
	
	endtTimeHr = document.getElementById("endHrs");
	endTimeMn = document.getElementById("endMin");
	
	if(!endtTimeHr) { endtTimeHr = 0; }
	if(!endTimeMn) { endTimeMn = 0; }
	
	if(endtTimeHr.selectedIndex > -1 || endtTimeHr.selectedIndex > -1)	{
		ETHr = endtTimeHr.options[endtTimeHr.selectedIndex].value
		ETMn = endTimeMn.options[endTimeMn.selectedIndex].value
	}else{
		ETHr = 0
		ETMn = 0;
	}
	
	dur = document.getElementById("defaultDuration");
	
	type = document.getElementById("programType");
	prgType = type.options[type.selectedIndex].value;
	
	if(prgType == 1)	{
		endTime = document.getElementById("endTime").checked;
	}else{
		endTime = 0;	
	}
	
	if(!endTime)	{
		ETHr = 0;
		ETMn = 0;
	}
	
	var ST, ET;
	
	newStartTime = setEpochTime(STHr, STMn);
	newEndTime = setEpochTime(ETHr, ETMn);
	
	if(!endTime)	{ newEndTime = 0;};
	
	displayAsset = document.getElementById("displayAssets");
	displayAssetID = displayAsset.options[displayAsset.selectedIndex].value;

	if(dur.options[dur.selectedIndex].value == -1) {
		duration = document.getElementById("customDuration").value;	
	}else{
		duration = dur.options[dur.selectedIndex].value;
	}
	
	displayTypeSel = document.getElementById("displayType");
	selType = displayTypeSel.options[displayTypeSel.selectedIndex].value;
	
	if(displayTypeSel == 1){
		//image type
	}else{
		//video type
		if(prgType == 0)	{ duration=0; }
		//if(newEndTime == 0)	{ duration=0; }
	}
	
	if(!duration) { duration = 0; }
	if(newEndTime > 0) { duration = 0; }
	
	objectData = {
				"start":newStartTime, 
				 "end":newEndTime, 
				 "duration":duration,
				 "type":prgType,
				 "displayAssetID":displayAssetID
				 };
	
	//console.log(objectData);
	
	jsAssets.setSyncMode();
	jsAssets.setCallbackHandler(updatedProgramAsset);
	jsAssets.updateProgramAsset(<cfoutput>#assetID#</cfoutput>,objectData);
	
}

function updatedProgramAsset()	{
	console.log('updated program asset');
}


function getDisplayAsset(assetID)
{
	jsAssets.setSyncMode();
	jsAssets.setCallbackHandler(updatedDisplayAsset);
	jsAssets.getDisplayAsset(assetID);
}

function updatedDisplayAsset(assetInfo)	{
	
	img = document.getElementById("theDisplayPreview")
	vid = document.getElementById("theDisplayVideoPreview")
	vidObj = document.getElementById("theVideo")
	
	img.style.display = "none";
	vid.style.display = "none";
	//console.log(assetInfo);
	img.src = "";
	vid.src = "";
	
	if(assetInfo.assetType == 1){
		//image
		img.src = assetInfo.url;
		img.style.display = "block";
	
	}else{
		//video
		theVideo.src = assetInfo.url;
		vid.style.display = "block";
	}
	
}

function displayEndTime(theState)	{

	endTimeObj = document.getElementById("setEndTime")	
	
	displayTypeSel = document.getElementById("displayType");
	
	if(displayTypeSel)	{
		displayType = displayTypeSel.options[displayTypeSel.selectedIndex].value;
	}else{
		displayType = 0
	}
	
	durationObj = document.getElementById("durationSetting");
	
	if(displayType != 2) {
		//scehdule
	}else{
		durationObj.style.display = "none"	
	}
	
	if(theState)	{
		endTimeObj.style.display = "block";
		if(displayType != 2) { durationObj.style.display = "none";}
	}else{
		endTimeObj.style.display = "none";
		durationObj.style.display = "block";
	}
	
}

function viewAssetDetails()	{

	displayAsset = document.getElementById("displayAssets");
	displayAssetID = displayAsset.options[displayAsset.selectedIndex].value;
	
	displayTypeSel = document.getElementById("displayType");
	displayType = displayTypeSel.options[displayTypeSel.selectedIndex].value;
	
	if(displayAssetID > 0)	{
		document.location.href = 'AssetsView.cfm?assetID='+displayAssetID+'&assetTypeID='+displayType;
	}
}

</script>

<cfset isScheduled = 0>
<cfset startTime = 0>
<cfset endTime = 0>
<cfset duration = 10>
<cfset displayAsset_id = 0>

<cfoutput query="asset">

	<cfset isScheduled = isScheduled>
    <cfset startTime = startTime>
    <cfset endTime = endTime>
    <cfset duration = duration>
    <cfset displayAsset_id = displayAsset_id>

</cfoutput>

<cfoutput><!--- query="asset" --->
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td valign="middle">
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" style="width:400px" maxlength="128" />
      </td>
    </tr>
    <tr>
      <td height="32" align="left" valign="middle">Program Type</td>
      <td align="left" valign="middle">
      
        <select name="programType" id="programType" onchange="displaySceduleSettings(this.options[this.selectedIndex].value)">
          <option value="0" <cfif isScheduled IS 0> selected</cfif>>Cycle</option>
          <option value="1" <cfif isScheduled IS 1> selected</cfif>>Scheduled</option>
        </select>
        
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle"></td>
      <td align="left" valign="middle">
      
      </td>
    </tr>
  <tr>
	  <td height="32" align="right" valign="middle">Program</td>
    <td valign="middle">
      
	  <cfif isScheduled IS 0><cfset display = 'none'><cfelse><cfset display = 'block'></cfif>
    
    <table cellpadding="0" cellspacing="0"><tr>
    <td>
    <div id="scheduledTime" style="display:block">
    <table border="0" cellspacing="0" cellpadding="0" style="margin-right:10px">
      
      <tr>
        <td class="content" style="padding-right:5px">Start</td>
        <td class="content">
          <select name="startHrs" id="startHrs" style="width:60px">
            <cfloop index="hr" from="6" to="24">
              <cfif hr GTE 12>
                
                <cfset alt = hr-12 &" PM">
                
                <cfif hr IS 12>
                  <cfset alt = "#hr# PM">
                  <cfelseif hr IS 24>
                  <cfset alt = "#hr-12# AM">
                  </cfif>
                
                <cfelse>
                <cfset alt = "#hr# AM">
                </cfif>
              <option value="#hr#">#alt#</option>
              </cfloop>
            </select>
          :
          <select name="startMin" id="startMin" style="width:90px">
            <cfloop index="mn" from="0" to="60">
              <cfif mn GT 1 OR mn IS 0>
                <cfset alt = "#mn# Minutes">
                <cfelse>
                <cfset alt = "#mn# Minute">
                </cfif>
              <option value="#mn#">#alt#</option>
              </cfloop>
            </select>
          <script>
	  	
      	var myDateStart = theTime(#startTime#);
		var selectedTime = getTimeIndex(myDateStart.hours, myDateStart.minutes);
		
		if(selectedTime.hr > -1)	{
			document.getElementById("startHrs").selectedIndex = selectedTime.hr;
			document.getElementById("startMin").selectedIndex = selectedTime.mn;
		}else{
			document.getElementById("startHrs").selectedIndex = 0;//default
			document.getElementById("startMin").selectedIndex = 0;//default
		}
		
      </script>
        </td>
        <td width="40" align="right" valign="middle" class="content" style="padding-right:10px">End
          </td>
        <td align="right" class="content" style="padding-right:10px">
          <input name="endTime" type="checkbox" id="endTime" onchange="displayEndTime(this.checked)" value="1" />
          <label for="endTime" style="padding-top: 4px; padding-left: 10px;"></label>
        </td>
        <td class="content" style="padding-left:50px">
        <div style="display:none" id="setEndTime">
        <select name="endHrs" id="endHrs" style="width:60px">
        <cfloop index="hr" from="6" to="24">
        	<cfif hr GTE 12>
            
            	<cfset alt = hr-12 &" PM">
                
				<cfif hr IS 12>
                    <cfset alt = "#hr# PM">
                <cfelseif hr IS 24>
                    <cfset alt = "#hr-12# AM">
                </cfif>
            
            <cfelse>
            	<cfset alt = "#hr# AM">
            </cfif>
        	<option value="#hr#">#alt#</option>
        </cfloop>
        </select>
        :
        <select name="endMin" id="endMin" style="width:90px">
        <cfloop index="mn" from="0" to="60">
        	<cfif mn GT 1 OR mn IS 0>
            	<cfset alt = "#mn# Minutes">
            <cfelse>
            	<cfset alt = "#mn# Minute">
            </cfif>
        	<option value="#mn#">#alt#</option>
        </cfloop>
        </select>
        </div>
        
        
        </td>

      </tr>
      
    </table>
    </div>
	</td>
    <td>
    <cfif duration IS 0><cfset display = 'none'><cfelse><cfset display = 'block'></cfif>
    <cfif assetType IS 2 AND isScheduled IS 1><cfset display = 'block'><cfelse><cfset display = 'none'></cfif>
	<cfif duration IS ''><cfset duration = 0></cfif>
    <div id="durationSetting" style="display:#display#">
    <table cellpadding="0" cellspacing="0">
    <tr>
    <td align="left" class="content" style="padding-right:5px">Duration</td>
        <td class="content">
        <div>
			<cfset setTime = [0,.17,.25,.5,1,5,10,20,30,60]>
            <select name="defaultDurations" onchange="setProgramTime(this.options[this.selectedIndex].value);" style="width:110px;" id="defaultDuration">
                <cfloop index="defaultTime" array="#setTime#">
                	
                    <option value="#defaultTime#"<cfif duration IS defaultTime> selected</cfif>>
                    <cfif defaultTime LT 1 AND defaultTime NEQ 0>
                    #int(60/(1/defaultTime))# seconds
                    <cfelseif assetType IS 2 AND defaultTime IS 0>
                    Movie Length
                    <cfelseif defaultTime NEQ 0>
                    #defaultTime# minute<cfif defaultTime GT 1>s</cfif>
                    <cfelse>
                    None
                    </cfif>
					</option>
                  
                </cfloop>
                <cfif arrayFind(setTime,duration) IS 0>
                    <option value="-1" selected>Other</option>
                <cfelse>
                    <option value="-1">Other</option>
                </cfif>
             
            </select>
        
            <div id="otherDuration" style="display:none; float:left">
            <input name="customDuration" type="text" class="formText" id="customDuration" value="#duration#" maxlength="3" style="height:24px;width:30px; text-align:center;margin-right: 12px;">
            </div>
        
        </div>
        <cfif arrayFind(setTime,duration) IS 0>
        	<cfif duration IS ''><cfset duration = 'undefined'></cfif>
        	<script>setProgramTime(-1, #duration#);</script>
        </cfif> 
        </td>
    </tr>
    </table>
    </div>
    </td>
    </tr>
    </table>
    
    <script>
	  	
	  var myDateEnd = theTime(#endTime#);
	  var selectedTime = getTimeIndex(myDateEnd.hours, myDateEnd.minutes);
	  var ET;
	  
	  <cfif endTime GT 0>ET = #endTime#<cfelse>ET = 0</cfif>
	  
	  if(ET > 0)	{
		  document.getElementById("endTime").checked = true;
		  displayEndTime(true);	
	  }
	  
	  theEndHrSel = document.getElementById("endHrs");
	  theEndMnSel = document.getElementById("endMin");
	  
	  if(theEndHrSel || theEndMnSel)	{
		  
		if(selectedTime.hr > -1)	{
	  		theEndHrSel.selectedIndex = selectedTime.hr;
	  		theEndMnSel.selectedIndex = selectedTime.mn;
		}else{
			theEndHrSel.selectedIndex = 0;//default
	  		theEndMnSel.selectedIndex = 0;//default
		}
		
	  }
	  
	  displaySceduleSettings(#isScheduled#)
      
    </script>
    
    <cfif displayAsset_id IS ''><cfset displayAssetID = 0><cfelse><cfset displayAssetID = displayAsset_id></cfif>
    
    </td>
  </tr>
   <tr>
	  <td height="32" align="right" valign="middle">Asset</td>
	  <td valign="middle">
      <div style="float:left; height:44px; padding-top:5px">
	   <select name="displayType" id="displayType" onchange="updateDisplayAssets(this.options[this.selectedIndex].value);">
       	 <option value="0" <cfif assetType IS 0> selected</cfif>>None</option>
	     <option value="1" <cfif assetType IS 1> selected</cfif>>Images</option>
	     <option value="2" <cfif assetType IS 2> selected</cfif>>Videos</option>
       </select>
      
	   <select name="displayAssets" id="displayAssets" onchange="viewDisplayAsset()">
       	<option value="0">None</option>
       </select>
       </div>

      </td>
  </tr>
	
	<tr>
	  <td align="left" valign="top">&nbsp;</td>
	  <td align="left" valign="top">
      
      <table border="0" cellpadding="0" cellspacing="0">
      <tr>
      <td>
      <img src="" height="240" id="theDisplayPreview" style="display:none" />
     
      <div id="theDisplayVideoPreview" style="display:none">
      	
         <video height="240" controls id="theVideo">
          <source src="" type="video/mp4">
        </video> 
        
      </div>
      </td>
      <td>
      <div style="padding-left:10px">
      <cfif assetType IS 1>
       		<input name="rotate" type="button" id="rotate" onclick="rotateImage(#displayAssetID#,1)" value="Rotate Clockwise" style="height:32px; margin-top:5px" />
            <input name="rotate" type="button" id="rotate" onclick="rotateImage(#displayAssetID#,-1)" value="Rotate Counter-Clockwise" style="height:32px; margin-top:5px" />
       </cfif>
       </div>
      </td>
      </tr>
      </table>
      
      </td>
    </tr>
    <tr>
	  <td align="left" valign="top">&nbsp;</td>
	  <td align="left" valign="top">
      <input name="updateData" type="button" id="updateData" onclick="updateProgramAsset()" value="Save Program" style="height:32px" />
      <input name="viewAsset" type="button" id="viewAsset" onclick="viewAssetDetails()" value="View Asset Details" style="height:32px" />
      </td>
    </tr>
    <tr>
	  <td colspan="2" align="right" valign="top"><hr size="1" /></td>
  </tr>
	
  </table>
  
  
  
 <script>
     updateDisplayAssets(#assetType#,#displayAssetID#);
     viewDisplayAsset();	 
 </script>

  
</cfoutput>
 
