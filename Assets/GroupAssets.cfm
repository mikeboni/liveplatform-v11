<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<!--- <link href="../styles.css" rel="stylesheet" type="text/css"> --->

<cfif asset.recordCount GT 0>
    <cfinvoke component="CFC.Assets" method="getGroupAssets" returnvariable="groupAssets">
            <cfinvokeargument name="assetID" value="#asset.asset_id#"/>
    </cfinvoke>
 </cfif>   


       
<cfinvoke component="CFC.Assets" method="getAllAssets" returnvariable="all3DAssetsOld3D">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="assetType" value="6"/><!--- 3D OLD Type --->
</cfinvoke>
<cfinvoke component="CFC.Assets" method="getAllAssets" returnvariable="all3DAssetsNew3D">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="assetType" value="25"/><!--- 3D NEW Type --->
</cfinvoke>

<cfinvoke component="CFC.Misc" method="QueryAppend" returnvariable="all3DAssets">
        <cfinvokeargument name="QueryOne" value="#all3DAssetsOld3D#"/>
        <cfinvokeargument name="QueryTwo" value="#all3DAssetsNew3D#"/><!--- 3D Type --->
</cfinvoke>

<cfinvoke component="CFC.Assets" method="getAllAssets" returnvariable="allImgAssets">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="assetType" value="1"/><!--- Image Type --->
</cfinvoke>

<cfinvoke  component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />
<cfinvoke component="CFC.Assets" method="getAssetsTypes" returnvariable="assetTypes" />


<cfquery name="contentAsset">
    SELECT * 
    FROM 	ContentAssets
    WHERE asset_id = #assetID#
</cfquery>

<cfif contentAsset.params IS ''><cfset contentAsset.params = '[]'></cfif>

<script>
	
	function checkType(str) {
		
		v = Number(str)

		if(isInt(v)) { 
			type = "integer" 
		} else if(isFloat(v)) { 
			type = "float"
		} else {
			type = "string"
		}
		
		console.log(type)
	}
	
	function isInt(n){
    	return n === +n && n === (n|0);
	}

	function isFloat(n){
		return n === +n && n !== (n|0);
	}
	
</script>

<script type="text/javascript">
	
	var methodParamObjects = <cfoutput>#contentAsset.params#</cfoutput>

	function showDelete(theObj, state)	{

		theObjRef = document.getElementById(theObj);

		if(state)
		{
			theObjRef.className = 'itemShow';
		}else{
			theObjRef.className = 'itemHide';
		}

	}

	function deleteContent(assetID) {
			if(confirm('Are you sure you wish to Delete this Remove Asset?'))
			{
				//nothing
			}else{ 
				//nothing
			}
	}

	function updateContentAssetOptions(theObj,theAssetID) 
	{
		theLevel = null;
		theCached=null;
		theActive=null;
		theShared=null;
		theOrder=null;

		if(theObj.id == 'contentActive')
		{
			theActive = toggleState(theObj);
		}

		if(theObj.id == 'contentShared')
		{
			theShared = toggleState(theObj);
		}

		if(theObj.id == 'contentCached')
		{
			theState = theObj.style.backgroundImage;
			src = theState.substring(4, theState.length-1);

			theSrc = src.replace( /^.+\// , '' );
			srcName = theSrc.replace(/"/g, "");

			if( srcName == "cache.png")
			{
				newSrc = "cached"
				theCached = 1
			}else{
				newSrc = "cache"
				theCached = 0;
			}

			theObj.style.backgroundImage = 'url(images/'+ newSrc +'.png)';

		}

		if(theObj.id == 'accessLevel')
		{
			theLevel = parseInt(theObj.options[theObj.selectedIndex].value);
			console.log(theLevel);
			if(theLevel == 0)
			{
				lock = 'access_unlocked';  
			}else{
				lock = 'access_locked-'+theLevel; 
			}

			theObj.style.backgroundImage = 'url(images/'+ lock +'.png)';
		}

		if(theObj.id == 'sortOrder')
		{
			theOrder = parseInt(theObj.value);
		}

		theStruct = {'access':theLevel, 'cached':theCached, 'active':theActive, 'shared':theShared, 'order':theOrder};
		updateContentAsset(theAssetID,theStruct);
	}


	function toggleState(theObj)
	{
		if(theObj.className == 'contentLinkRed')
		{
			state = 0;
			theObj.className = 'contentLinkGreen';
			theObj.innerHTML = 'NO';
		}else{
			state = 1;
			theObj.className = 'contentLinkRed';
			theObj.innerHTML = 'YES';
		}

		return state;	
	}

	function clearUseAssetID()
	{
		theObjRef = document.getElementById('useAssetID');
		theObjRef.selectedIndex = 0;
	}

	function clearInstanceName()
	{
		theObjRef = document.getElementById('instanceNameAsset');
		theObjRef.value = '';
	}


	function displayExtra(theSel)
	{
		//theSel = parseInt(theForm.options[theForm.selectedIndex].value);

		switch(theSel) {
		case 1:
			if(document.getElementById('container')) { document.getElementById('container').style.display = 'none'; }
			if(document.getElementById('instanceName')) { document.getElementById('instanceName').style.display = 'block'; } 
			if(document.getElementById('instanceNameEntry')) { document.getElementById('instanceNameEntry').style.display = 'block'; }
			if(document.getElementById('instanceNameEntryList')) { document.getElementById('instanceNameEntryList').style.display = 'block'; }
			if(document.getElementById('actionProperties')) { document.getElementById('actionProperties').style.display = 'none'; }	
			console.log(theSel)
			break;

		case 5:
			if(document.getElementById('container')) { document.getElementById('container').style.display = 'none'; }
			if(document.getElementById('instanceName')) { document.getElementById('instanceName').style.display = 'block'; }
			if(document.getElementById('instanceNameEntry')) { document.getElementById('instanceNameEntry').style.display = 'block'; }
			if(document.getElementById('instanceNameEntryList')) { document.getElementById('instanceNameEntryList').style.display = 'block'; }
			if(document.getElementById('actionProperties')) { document.getElementById('actionProperties').style.display = 'none'; }	
			console.log(theSel)
			break;

		case 6:
			if(document.getElementById('container')) { document.getElementById('container').style.display = 'none'; }
			if(document.getElementById('instanceName')) { document.getElementById('instanceName').style.display = 'block'; }
			if(document.getElementById('instanceNameEntry')) { document.getElementById('instanceNameEntry').style.display = 'block'; }
			if(document.getElementById('instanceNameEntryList')) { document.getElementById('instanceNameEntryList').style.display = 'none'; }
			if(document.getElementById('actionProperties')) { document.getElementById('actionProperties').style.display = 'none'; }	
			console.log(theSel)
			break;

		case 7:
			if(document.getElementById('container')) { document.getElementById('container').style.display = 'none'; }
			if(document.getElementById('registerName')) { document.getElementById('registerName').style.display = 'block'; }
			if(document.getElementById('registerNameEntry')) { document.getElementById('registerNameEntry').style.display = 'block'; }
			if(document.getElementById('registerNameEntryList')) { document.getElementById('registerNameEntryList').style.display = 'none'; }
			if(document.getElementById('actionProperties')) { document.getElementById('actionProperties').style.display = 'none'; }	useGroupID
			console.log(theSel)
			break;		

		case 8:
			if(document.getElementById('container')) { document.getElementById('container').style.display = 'block'; }
			if(document.getElementById('instanceName')) { document.getElementById('instanceName').style.display = 'none'; }
			if(document.getElementById('instanceNameEntry')) { document.getElementById('instanceNameEntry').style.display = 'none'; }
			if(document.getElementById('instanceNameEntryList')) { document.getElementById('instanceNameEntryList').style.display = 'none'; }
			if(document.getElementById('registerNameEntryList')) { document.getElementById('registerNameEntryList').style.display = 'none'; }
			if(document.getElementById('registerName')) { document.getElementById('registerName').style.display = 'none'; }
			if(document.getElementById('registerNameEntry')) { document.getElementById('registerNameEntry').style.display = 'none'; }
			if(document.getElementById('useGroupID')) { document.getElementById('useGroupID').style.display = 'none'; }
			if(document.getElementById('actionProperties')) { document.getElementById('actionProperties').style.display = 'none'; }	
			console.log(theSel)
			break;	

		case 9:
			if(document.getElementById('container')) { document.getElementById('container').style.display = 'none'; }
			if(document.getElementById('instanceName')) { document.getElementById('instanceName').style.display = 'block'; }
			if(document.getElementById('instanceNameEntry')) { document.getElementById('instanceNameEntry').style.display = 'block'; }
			if(document.getElementById('instanceNameEntryList')) { document.getElementById('instanceNameEntryList').style.display = 'block'; }
			if(document.getElementById('actionProperties')) { document.getElementById('actionProperties').style.display = 'block'; }	
			console.log(theSel)
			break;

		default:
			if(document.getElementById('container')) { document.getElementById('container').style.display = 'none'; }
			if(document.getElementById('instanceName')) { document.getElementById('instanceName').style.display = 'none'; }
			if(document.getElementById('instanceNameEntry')) { document.getElementById('instanceNameEntry').style.display = 'none'; }
			if(document.getElementById('instanceNameEntryList')) { document.getElementById('instanceNameEntryList').style.display = 'none'; }
			if(document.getElementById('actionProperties')) { document.getElementById('actionProperties').style.display = 'none'; }	
			console.log(theSel)
		} 

	}
	
	function resetAlignment() {
		hAlign = document.getElementById('halignment');
		vAlign = document.getElementById('valignment');
		vAlign.selectedIndex = 0;
		hAlign.selectedIndex = 0;
	}
	
	function displayAlignmentOptions(type) {
		
		//v-align
		//h-align
		type = parseInt(type)
		
		switch(type) {
			case 2:
			document.getElementById('h-align').style.display = 'block';
			document.getElementById('v-align').style.display = 'none';
			break;
			
			case 4:
			document.getElementById('h-align').style.display = 'block';
			document.getElementById('v-align').style.display = 'none';
			break;
				
			case 1:
			document.getElementById('h-align').style.display = 'none';
			document.getElementById('v-align').style.display = 'block';
			break;
			case 2:
			document.getElementById('h-align').style.display = 'none';
			document.getElementById('v-align').style.display = 'block';
			break;

			default:
			document.getElementById('h-align').style.display = 'none';
			document.getElementById('v-align').style.display = 'none';
			break;
			} 

		}
	
	function getType(type) {
		
		allTypes = getAllTypes();
		theObjType = ""
		
		Object.keys(allTypes).forEach(function (key) {
			var val = parseInt(allTypes[key]);
			if(val == type) { theObjType = key }
		});
		
		return theObjType
		
	}
	function getAllTypes() {
		return {'integer':0, 'integer':1, 'string':2, 'float':3, 'Transform':4, 'GameObject':6, 'bool':7}
	}
	
	function displayParams() {
		//paramsList
		paramsList = document.getElementById('paramsList');
		paramsList.innerHTML = '';
		
		for(i=0; i < methodParamObjects.length; i++) {
			
			theObj = methodParamObjects[i];
			
			selectMenu = constructSelect(i, theObj.type);

			contentItem = '';
			contentItem += '<tr><td width="235" align="left"><input name="varName" type="text" class="formfieldcontent" id="varName" style="width:210px" value="'+ theObj.name +'" onChange="updateParamsData()"/></td>';
			contentItem += '<td width="120" align="left">'+ selectMenu +'</td>';
			contentItem += '<td width="220" align="left"><input name="varValue" type="text" class="formfieldcontent" id="varValue" style="width:220px" value="'+ theObj.value +'" onChange="updateParamsData();checkType(this.value)"/></td>';
			
			divID = "'param_" + i +"'"
			
			contentItem += '<td align="center"><a onClick="deleteParam('+ divID +')"><img src="images/remove.png"></a></td></tr>';

			theItem = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="content">'+contentItem+'</table>';
		
			theParamItem = '<div id="param_'+ i +'">'+ theItem +'</div>';
			paramsList.innerHTML += theParamItem;
			
		}
	}
	
	function constructSelect(id, selectedType) {
		
		types = getAllTypes();
		
		selectMenu = '';
		selectMenu += '<select name="'+ id +'" id="'+ id +'" style="width: 100px" onChange="updateParamsData()">';
		
		Object.keys(types)
			.forEach(function eachKey(key) { 
			
			selectObj = types[key];

			if(key == selectedType) { sel = ' selected' } else { sel = '' }
			selectMenu += '<option value="'+ selectObj +'"'+ sel +'>'+ key +'</option>';
			
		  });
		
		selectMenu += '</select>';
		
		return selectMenu

	}
	
	function updateParamsData() {
		
		newParams = []
		params = document.getElementById('params');
		currentParamsList = document.getElementById('paramsList');
		
		var paramObjs = currentParamsList.getElementsByTagName('div');

		for(i=0; i<paramObjs.length; i++) {
			
			paramId = paramObjs[i].id
			
			var inputOptions = document.getElementById(paramId).getElementsByTagName('input')
			var selectedOptions = document.getElementById(paramId).getElementsByTagName('select')
			
			var selVarName = inputOptions[0].value
			var selVarValue = inputOptions[1].value
			var selType = parseInt(selectedOptions[0][selectedOptions[0].selectedIndex].value)
			var selVarType = getType(selType)
			
			//console.log(selType, selVarType)
			paramObj = {name:selVarName, value:selVarValue, type:selVarType}
			newParams.push(paramObj)
		}
		methodParamObjects = JSON.parse(JSON.stringify(newParams));
		params.value = JSON.stringify(methodParamObjects)
		
	}
	
	function addNewParam() {
		
		paramObj = {name:"var", value:'val', type:0}
		methodParamObjects.push(paramObj)
		displayParams()
		updateParamsData()
	}
	
	function deleteParam(id) {
		
		paramElement = document.getElementById(id).remove();
		updateParamsData()
		displayParams()
		
	}

</script>

<!--- 3D --->
<cfif contentAsset.displayType IS 1 OR contentAsset.displayType IS 5 OR contentAsset.displayType IS 6 OR contentAsset.displayType IS 9>
  <cfset theState = 'block'>
<cfelse>
  <cfset theState = 'none'>
</cfif>



<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td valign="middle">
      <cfoutput>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" style="width:400px" maxlength="128" />
      </cfoutput>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="right" valign="middle"><hr size="1" /></td>
    </tr>
    <tr>
      <td align="right" valign="top" style="padding-top: 8px;">Behaviour </td>
      <td align="left" valign="middle">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="200">
			<cfoutput>
			  <cfset allTypes = {"3D Model":1, "Modal":2, "Panel":3, "PopOver":4, "Image":5, "Load":6, "Register":7, "Container" : 8, "ExecuteMethod" : 9}>
			  <label for="useAssetID"></label>
				<select name="behaviourType" id="behaviourType" class="formfieldcontent" style="padding-left:2; margin-right: 10px;" onchange="displayExtra(this.value);switchAssetTypes(this)">
				<option value="0">None</option>
				<cfloop collection="#allTypes#" item="type">
				  <option value="#allTypes[type]#" <cfif contentAsset.displayType IS allTypes[type]> selected</cfif>>#type#</option>
				</cfloop>
			  </select>
			  </cfoutput>  
            </td>
            <td width="20" align="right">
				<cfif contentAsset.displayType IS 4>Type:</cfif>
            </td>
            <td align="left">
				<cfif contentAsset.displayType IS 4>
					<cfset listTypes = {"Container":1, "List":2, "List+Thumbs":3}>
					
					<cfoutput>
						  <select name="popoverType" id="popoverType" class="formfieldcontent" style="padding-left:2; margin-right: 10px; width: 100px">
							<option value="0">Default</option>
							<cfloop collection="#listTypes#" item="type">
							  <option value="#listTypes[type]#" <cfif contentAsset.popoverType IS listTypes[type]> selected</cfif>>#type#</option>
							</cfloop>
						  </select>	  
					</cfoutput>
					
				</cfif>
           </td>
           
			<cfif contentAsset.displayType IS 3>
			   <td align="right">
					Display Dismiss
				</td>
				<td>
				  <cfif contentAsset.dismiss IS ''><cfset dismiss = 0><cfelse><cfset dismiss = contentAsset.dismiss></cfif>
					<input name="dismiss" type="checkbox" id="dismiss" value="1" <cfif dismiss IS true> checked</cfif> onChange="fogOptions(this.checked)" />
					<label for="dismiss" style="margin-bottom: 25px; float: left; margin-left: 5px;"></label>	
				</td>
 			</cfif>
 			
			<cfif contentAsset.displayType IS 3>
          </tr>
			<tr>
			<td colspan="2">

				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
				      <tr>
				        <td colspan="5">&nbsp;</td>
			        </tr>
				      <tr>
				        <td colspan="5">Grid Options</td>
			          </tr>
				      <tr>
				        <td align="right" style="padding-top: 8px;">Hero</td>
				        <td>
							<cfif contentAsset.hero IS ''><cfset hero = 1><cfelse><cfset hero = contentAsset.hero></cfif>
				        	<input name="hero" type="checkbox" id="hero" value="1" <cfif hero> checked</cfif> />
      						<label for="hero" style="margin-left: 10px;"></label> 
				        </td>
				        <td>&nbsp;</td>
				        <td align="right" style="padding-top: 10px;">Colums:</td>
				        <td style="padding-top: 10px;">
							<cfif contentAsset.cols IS ''><cfset cols = 3><cfelse><cfset cols = contentAsset.cols></cfif>
		        	  	  <cfoutput>
			        	  	  <select name="colums" id="colums" class="formfieldcontent" style="padding-left:2; margin-right: 10px; width: 30px; margin-left: 10px;">
								<cfloop from="1" to="6" index="i">
								  <option value="#i#" <cfif i IS cols> selected</cfif>><cfoutput>#i#</cfoutput></option>
								</cfloop>
							  </select>
			        		</cfoutput>
				        </td>
			          </tr>
		      </table></td>
		  </tr>
     		</cfif>
      </table>
      </td>
    </tr>
		
    <tr>
      <td align="right" valign="middle">

      <cfif contentAsset.displayType IS 7>
      Assign Group
      </cfif>
      </td>
      <td align="left" valign="middle">
      
      <cfoutput>
      <cfif contentAsset.displayType IS 8>
      
      <cfset typeAlign = mid(contentAsset.alignment,1,1)>
      <cfset typeAlignment = mid(contentAsset.alignment,2,1)>
	 <cfif typeAlign IS ''><cfset typeAlign = 0></cfif>
     <cfif typeAlignment IS ''><cfset typeAlignment = 0></cfif>

      <cfif typeAlign GT 0>
      	<cfif typeAlign IS 2 OR typeAlign IS 4>
	  	<cfset typeAlignment = 2>
       <cfelse>
       	<cfset typeAlignment = 5>
      </cfif>
		</cfif>
      <div style="margin-top:10px; margin-right:10px;float:left" id="container">
      <table border="0" cellpadding="5" class="content">
			<tr>
			  <td width="120">Align Screen Position</td>
			</tr>
		  	<tr>
			  <td>
		    	<cfset alinTypes = ['None', 'Left', 'Top', 'Right', 'Bottom']>
			    <select class="formfieldcontent" id="align" name="align" onChange="resetAlignment();displayAlignmentOptions(this.options[this.selectedIndex].value)">
			      <option value="0">None</option>
			      <option value="1" <cfif typeAlign IS 1> selected</cfif>>Left</option>
			      <option value="2" <cfif typeAlign IS 2> selected</cfif>>Top</option>
			      <option value="3" <cfif typeAlign IS 3> selected</cfif>>Right</option>
			      <option value="4" <cfif typeAlign IS 4> selected</cfif>>Bottom</option>
			      </select>	
			    </td>
			  </tr>
			<tr>
		  </table>
		  <cfif typeAlignment IS 1 OR typeAlignment IS 2 OR typeAlignment IS 3>
		  		<cfset displayA = 'block'>
		  	<cfelse>
		  		<cfset displayA = 'none'>
		  </cfif>
		  <div id="h-align" style="display: #displayA#">
		  <table border="0" cellpadding="5" class="content">
		     <tr>
			  <td width="120">Horizontal Alignment</td>
			  </tr>
			  <tr>
			  <td>
			    <select class="formfieldcontent" id="halignment" name="alignment">
			      <option value="0">None</option>
					<option value="1" <cfif typeAlignment IS 1> selected</cfif>>Left</option>
			      <option value="2" <cfif typeAlignment IS 2> selected</cfif>>Center</option>
			      <option value="3" <cfif typeAlignment IS 3> selected</cfif>>Right</option>
			      </select>
				  
			    </td>
			  </tr>
		  </table>
		  </div>
		  <cfif typeAlignment IS 4 OR typeAlignment IS 5 OR typeAlignment IS 6>
		  		<cfset displayB = 'block'>
		  	<cfelse>
		  		<cfset displayB = 'none'>
		  </cfif>
		  <div id="v-align" style="display: #displayB#">
		  <table border="0" cellpadding="5" class="content">
			  <tr>
			  <td width="120">Vertical Alignment</td>
			  </tr>
			  <tr>
			  <td>
	
			    <select class="formfieldcontent" id="valignment" name="alignment">
			      <option value="0">None</option>
			      <option value="4" <cfif typeAlignment IS 4> selected</cfif>>Top</option>
			      <option value="5" <cfif typeAlignment IS 5> selected</cfif>>Middle</option>
			      <option value="6" <cfif typeAlignment IS 6> selected</cfif>>Bottom</option>
			      </select>
				  
			    </td>
			  </tr>
		</table>
     	</div>
      </div>
	  </cfif>
      
      <div style="display:#theState#; margin-top:10px; margin-right:10px;float:left" id="instanceNameEntry">
		  <table><tr><td>
     	
      	<cfif contentAsset.displayType IS 6>
      		Ref Link Path:
      	<cfelse>
      		Object Ref:
		</cfif>
     	
      	</td></tr>
       	<tr><td>
        <input name="instanceNameAsset" type="text" class="formfieldcontent" id="instanceNameAsset" style="width:400px" value="#contentAsset.instanceName#" onchange="clearUseAssetID();"/>
			</td></tr>
		  </table>
		  
		  <cfif contentAsset.displayType IS 6><cfset theState = 'none'></cfif>
		  
		   <div style="display:#theState#; margin-top:0px" id="instanceNameEntryList">
			  <table>
				    <tr><td style="padding-top: 12px;">Use AssetID:</td></tr>
				    <tr>
				    <td>
				    	<label for="useAssetID"></label>
						<select name="useAssetID" id="useAssetID" class="formfieldcontent" style="padding-left:2;" onchange="clearInstanceName();">
						<option value="0"><-- Use Instance Name</option>
					</td>
					</tr>
				</table>
			</div>
		    
		 <cfif contentAsset.displayType IS 9><cfset theState = 'block'><cfelse><cfset theState = 'none'></cfif>    
		 <div style="display:#theState#; margin-top:10px; margin-right:10px;float:left" id="actionProperties">
			  <table>
			  <tr><td>ClassName:</td></tr>
			  <tr>
				<td>
				<input name="className" type="text" class="formfieldcontent" id="className" style="width:400px" value="#contentAsset.className#"/>
				</td>
			  </tr>
			  <tr><td>MethodName:</td></tr>
			  <tr>
				<td>
				<input name="methodName" type="text" class="formfieldcontent" id="methodName" style="width:400px" value="#contentAsset.methodName#"/>
				</td>
			  </tr>
			  <tr>
				<td style="padding-top: 10px">Params:<input name="params" type="hidden" id="params"></td>
			  </tr>
			  <tr>
			  	<td colspan="3">
			  	
			  	<table width="100%" border="0" cellpadding="8" cellspacing="1" class="content">
			  	<tr bgcolor="##eee">
					<td width="210">name</td><td width="100">type</td><td width="225">value</td><td align="center" width="40"><a onClick="addNewParam();"><img src="images/include-sm.png"></a></td>
				</tr>
				
				</table>
				
				<div id="paramsList"></div>
				
				</td>
			  </tr>
			  </table>
		  </div>
     
     
      </div>
      
	
 <script type="text/javascript">
 
 function switchAssetTypes(theForm)
 {
 		theSel = parseInt(theForm.options[theForm.selectedIndex].value);
		
 		theObjRef = document.getElementById("useAssetID");
		theObjRef.options.length = 1;
		
		<cfset cnt = 1>
		
		switch(theSel) {
    	case 1:		
			<cfloop query="#all3DAssets#">
			theObjRef.add(new Option("#name#", #asset_id#));
			
			<cfif contentAsset.instanceAsset_id IS asset_id>
				indexMatchingText(theObjRef,'#name#');
			</cfif>
	
			</cfloop>
	
			break;
			
		case 5:
			<cfloop query="#allImgAssets#">
			theObjRef.add(new Option("#name#", #asset_id#));
			
			<cfif contentAsset.instanceAsset_id IS asset_id>
				indexMatchingText(theObjRef,'#name#');
			</cfif>

			</cfloop>
			
			break;
			
		case 9:
			<cfloop query="#all3DAssets#">
			theObjRef.add(new Option("#name#", #asset_id#));
			
			<cfif contentAsset.instanceAsset_id IS asset_id>
				indexMatchingText(theObjRef,'#name#');
			</cfif>
	
			</cfloop>
			console.log('Y')
			break;
						  
		}
	
 }
 
function indexMatchingText(selectObject, text) {

	var list = selectObject.options;
	
	for(var i = 0; i<list.length; i++){
		if(list[i].text== text) { //Compare
			list[i].selected = true; //Select the option.
		}
	}
}

//update params
displayParams()
updateParamsData()

</script>
        
      </select>
      </div>
      </cfoutput>
   
      <cfif contentAsset.displayType IS 7>
      
      <cfinvoke component="CFC.Modules" method="getAccessAppGroups" returnvariable="groups">
          <cfinvokeargument name="appID" value="#session.appID#"/>
      </cfinvoke>
      
      <cfset id = contentAsset.instanceAsset_ID>
      <select class="formfieldcontent" name="useGroupID" id="useGroupID">
      <cfoutput query="groups">
      	<option value="#group_ID#" <cfif group_ID IS id> selected</cfif>>#name#</option>
      </cfoutput>
      </select>
      </cfif>
      
      </td>
    </tr>
    
</table>


<cfoutput>
	<script type="text/javascript">
     
         if (#contentAsset.displayType# == 1 || #contentAsset.displayType# == 5 || #contentAsset.displayType# == 9)
         {
             theForm = document.getElementById("behaviourType");
             switchAssetTypes(theForm);
         }
		
    </script>
</cfoutput>

<!--- <cfif active>
		<cfset state = "contentLink">
    <cfelse>
        <cfset state = "contentLinkDisabled">
</cfif> 
<div class="sectionLink" style="background-color:#333; padding-bottom:5px; height:44px; padding-left:10px; width:800px">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="44">Assets</td>
    <td width="44" align="right">
    <a onclick="displayImporter()">
    <img src="images/import.png" width="44" height="44" />
    </a>
    </td>
  </tr>
  <tr>
</table>

</div> --->

<!--- <div style="width:800px; display:none;" id="assetImporter"> 
		<cfoutput>
        <div style="margin-top: 10px;height: 50px;">
        <form method="post" style="padding-top:40px">
          <select name="assetTypes" id="assetTypes" onchange="displayAssets(this.options[this.selectedIndex].value,'displayAssets')" style="height:44px; margin:0px;">
            <option value="0">All Asset Types</option>
            <cfloop query="assetTypes">
                <option value="#type#">#name#</option>
            </cfloop>
          </select>
          <input type="button" class="formText" value="Import Content" onclick="allSelectedContent()" />
          <input name="contentIDs" type="hidden" id="contentIDs" />
          <div class="contentLinkGrey" id="totalAssets" style="width:180px; float:right; padding-top:15px"></div>
        </form>
        </div>
        </cfoutput>
        <div id="displayAssets" style="overflow:scroll; height:400px; overflow-x: hidden;border-style: solid; border-width: 1px; margin-bottom:40px; padding:10px"></div>
    </div> --->

<!--- <table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="#CCC">                  
                    <tr class="content">
                      <td height="44" style="padding-left:10px">
                      Asset Name
                      </td>
                      <td width="80" align="center" class="content">
						Access
                      </td>
                      <td width="60" align="center" class="content">  
                      Cached
                      </td>
                      <td width="60" align="center" class="content">
                       Active
                      </td>
                      <td width="60" align="center" class="content">
                      Shared</td>
                      <td width="80" align="center" class="content">Sort Order </td>
                      <td width="60" align="right" class="content"></td>
  </tr>  
</table> --->
<!--- 
<cfif asset.recordCount GT 0>

<cfoutput query="groupAssets">
<div class="rowhighlighter" onmouseover="showDelete(this,1);" onmouseout="showDelete(this,0);">
					
                    <form id="content_id">
				    <table width="810" border="0" cellpadding="5" cellspacing="0">                  
                    <!--- Assets --->
                    <tr>
                      <td width="44">
                        <img src="images/#icon#" width="44" height="44" />
                      </td>
                      <td colspan="2">
                      <a href="AssetsView.cfm?assetID=#content_id#&assetTypeID=#assetType_id#" class="#state#">
                      <div style="height:32px; padding-top:15px; padding-left:4px">#name#</div>
                      </a>
                      </td>
                      <td width="80" align="center" class="content">

                        <cfif accessLevel IS 0>
							<cfset lock = 'access_unlocked'>
                        <cfelse>
                        	<cfset lock = 'access_locked-'& accessLevel>
                        </cfif>
                        
                        <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateContentAssetOptions(this,#content_id#)" />
                            <cfloop query="accessLevels">
                            <option value="#accessLevel#" style="color:##333" <cfif groupAssets.accessLevel IS accessLevel> selected</cfif>>#levelName#</option>
                            </cfloop>
                        </select>
                      
                      </td>
                      <td width="60" align="center" class="content">  
                      <cfif cached IS '0'>
                      <cfset theCacheState = 'cache'>
                      <cfelse>
                      <cfset theCacheState = 'cached'>
                      </cfif>
                      <div id="contentCached" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer; width:28px; height:28px; background-image:url(images/#theCacheState#.png)">
                      </div>
                      </td>
                      <td width="60" align="center" class="content">
                      
                     
                      <cfif active IS '0'>
                      	<cfset stateCSS = "contentLinkGreen">
                      <cfelse>
                      	<cfset stateCSS = "contentLinkRed">
                      </cfif>
					  <div id="contentActive" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer" class="#stateCSS#">
						  <cfif active IS '0'>
                          NO
                          <cfelse>
                          YES
                          </cfif>
                      </div>
                      </td>
                      <td width="60" align="center" class="content">
                      
                      <cfif sharable IS '0'>
                      	<cfset stateCSS = "contentLinkGreen">
                      <cfelse>
                      	<cfset stateCSS = "contentLinkRed">
                      </cfif>
					  <div id="contentShared" style="cursor:pointer" onclick="updateContentAssetOptions(this,#content_id#)" class="#stateCSS#">
                      <cfif sharable IS '0'>
                      NO
                      <cfelse>
                      YES
                      </cfif>
                      </div>
                      
                      </td>
                      <td width="80" align="center" class="contentLinkDisabled">
                      <input name="sortOrder" type="text" id="sortOrder" value="#sortOrder#" size="4" maxlength="10" onchange="updateContentAssetOptions(this,#content_id#)" /></td>
                      <td width="60" align="right" class="content">
                      
                      <table border="0">
                      <tr>
                        <td>
                        <input type="button" class="itemHide" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteContentAsset(#content_id#);" value="" /> 
                        </td>
                      </tr>
                      
                      
                      
                    </table>
                      
                      
                      </td>
                    </tr>
                    
                      </table>
                    </form>
</div>
</cfoutput>   

<cfelse>
<span class="content">
No Group Content Assets
</span>
</cfif> --->