<link href="../styles.css" rel="stylesheet" type="text/css">

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<!--- Assets to support for Quiz Attachments --->
<cfinvoke component="CFC.Assets" method="getAssetsTypes" returnvariable="assetTypes" />
<cfset supportAssetTypes = [1,2]><!--- Image and Video --->


<!--- Get Quiz Assets --->
<cfinvoke component="CFC.Assets" method="getGroupAssets" returnvariable="quizAssets">
        <cfinvokeargument name="assetID" value="#assetID#"/>
        <cfinvokeargument name="quiz" value="true"/>
</cfinvoke>

<cfinvoke component="CFC.Assets" method="getGroupAssets" returnvariable="quizCorrectAssets">
        <cfinvokeargument name="assetID" value="#assetID#"/>
        <cfinvokeargument name="quizCorrect" value="true"/>
</cfinvoke>

<cfinvoke component="CFC.Assets" method="getGroupAssets" returnvariable="quizIncorrectAssets">
        <cfinvokeargument name="assetID" value="#assetID#"/>
        <cfinvokeargument name="quizIncorrect" value="true"/>
</cfinvoke>


<cfif asset.RecordCount IS 0>

	<!--- New Selection Entry --->
    <cfquery name="newSelection"> 
        INSERT INTO SelectionAssets (spacing , padding, numberOfCols)
        VALUES (10, 20, 2)
        SELECT @@IDENTITY AS selectionID
    </cfquery>
    
    <cfset selectionID = newSelection.selectionID>
    
    <!--- New DB Entry --->
    <cfquery name="newAsset"> 
        INSERT INTO QuizAssets (asset_id, selection_id)
        VALUES (#assetID#, #selectionID#)
    </cfquery>
    
    <!--- New Choice (Boolean default) Entry --->
    
    <!--- YES --->
    <cfquery name="newAsset"> 
        INSERT INTO ChoiceAssets (selection_id, text, sortOrder, correct)
        VALUES (#selectionID#, 'YES', 0, 1)
    </cfquery>
    
    <!--- NO --->
    <cfquery name="newAsset"> 
        INSERT INTO ChoiceAssets (selection_id , text, sortOrder, correct)
        VALUES (#selectionID#, 'NO', 1, 0)
    </cfquery>
	
    <cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
            <cfinvokeargument name="assetID" value="#assetID#"/>
    </cfinvoke>
    
</cfif>

<cfquery name="selectionSpecs">  
    SELECT  SelectionAssets.spacing, SelectionAssets.padding, SelectionAssets.numberOfCols, Colors.forecolor, Colors.backcolor, Colors.background
    FROM	SelectionAssets LEFT OUTER JOIN Colors ON SelectionAssets.color_id = Colors.color_id
	WHERE   SelectionAssets.selection_id = #asset.selection_id#             
</cfquery>

<cfquery name="selectionChoices">  
    SELECT  url, text, correct, sortorder, choice_id
    FROM	ChoiceAssets
	WHERE   selection_id = #asset.selection_id#    
    ORDER BY sortorder ASC         
</cfquery>

<script type="text/javascript" src="instanceName.js"></script>


<script type="text/javascript">

function displayResponseMessages(theState,theObj)
{
	theObjRef = document.getElementById(theObj);
	theObjNum = document.getElementById('numberOfTries');
	
	if(theState)
	{
		theObjRef.style.display = 'block';
	}else{
		theObjRef.style.display = 'none';
		theObjNum.value = 0;
	}	
	
}

function setChoiceState(choice,theObj)
{
	theTxtState = document.getElementById('respose_'+choice);
	theObjRef = document.getElementById('correct_'+choice);
	theState = theObjRef.checked;
	
	if(theState)
	{
		theTxtState.innerHTML = 'Correct';
		theTxtState.className = 'contentLinkRed';
	}else{
		theTxtState.innerHTML = 'Incorrect';
		theTxtState.className = 'contentLinkGreen';
	}
	
}


</script>

<script type="text/javascript">

	var jsAssets = new assetManager();
	
	function displayChoices()
	{
		//contentH = document.getElementById(theContentListing);
		
		jsAssets.setSyncMode();
		jsAssets.setCallbackHandler(successChoiceListing);
		jsAssets.getAllChoices(<cfoutput>#asset.selection_id#</cfoutput> ,'quizChoices');
	}
	
	function successChoiceListing(result)
	{
		console.log(result);
		theContainer = document.getElementById(result.contentHolder);
		theContainer.innerHTML = result.html;
	}
	
	//quiz
	var jsQuiz = new quizManager();
	successChoiceListing
	//add
	function newQuizChoice()
	{
		jsQuiz.setSyncMode();
		jsQuiz.setCallbackHandler(successQuizChoice);
		jsQuiz.newQuizChoice(<cfoutput>#asset.selection_id#</cfoutput>);
	}
	
	function successQuizChoice(result)
	{
		displayChoices();
	}
	//remove
	function removeQuizChoice(choiceID)
	{
		if(confirm('Are you sure you wish to Remove this Choice?'))
		{
			jsQuiz.setSyncMode();
			jsQuiz.setCallbackHandler(successQuizChoiceRemoved);
			jsQuiz.removeQuizChoice(<cfoutput>#asset.selection_id#</cfoutput>,choiceID);
			
		}else{ 
			//nothing
		}
		
	}
	
	function successQuizChoiceRemoved(result)
	{
		if(!result)
		{
		alert('You must have no less than 2 choices');
		}
		
		displayChoices();
	}
	
	//update
	//remove
	function updateQuizSelection(choiceID, theObj, theVal)
	{
		//console.log(choiceID, theObj, theVal);
		
		theQA = {};
		
		if(theObj == 'correct')
		{
		theQA.correct = theVal;
		updateSelection(choiceID, theQA)
		}
		if(theObj == 'sortOrder')
		{
		theQA.sortOrder = theVal;	
		updateSelection(choiceID, theQA)
		}
		if(theObj == 'choiceMessage')
		{
		theQA.text = theVal;	
		updateSelection(choiceID, theQA);
		}
		if(theObj == 'imgURL')
		{ 	
		theVal.submit();
		//ColdFusion.Ajax.submitForm(theVal.id, 'CFC/Quiz.cfc?method=uploadFile', successUpload, failedUpload);
		}

		
	}
	
	function successUpload()
	{
		console.log('Uploaded Image');	
	}
	
	function failedUpload()
	{
		console.log('Uploaded Image');	
	}
	
	
	
	function updateSelection(choiceID, theQA)
	{
		jsQuiz.setSyncMode();
		jsQuiz.setCallbackHandler(successQuizChoiceRemoved);
		jsQuiz.updateQuizChoice(<cfoutput>#asset.selection_id#</cfoutput>, choiceID, theQA);	
	}
	
	function successQuizChoiceUpdated(result)
	{
		displayChoices();
	}
	
	
	//Quiz chcoice image remove
	function deleteChoiceImage(choiceID)
	{
		if(confirm('Are you sure you wish to Remove this Image from the Choice?'))
		{
			jsQuiz.setSyncMode();
			jsQuiz.setCallbackHandler(successQuizChoiceImageRemoved);
			jsQuiz.deleteChoiceImage(choiceID);
			
		}else{ 
			//nothing
		}
		
	}
	
	function successQuizChoiceImageRemoved()
	{
		displayChoices();
	}
	
	//Quiz
	function showQuizAssets()	{
	
		checkboxObj = document.getElementById('useQuizAssets');
	
		var theObj = document.querySelector("#AssetViewQuiz");
		
		if(checkboxObj.checked)
		{
			removeClass(theObj,"detailsHide");
			addClass(theObj, "detailsShow");
		}else{
			removeClass(theObj,"detailsShow");
			addClass(theObj, "detailsHide");
		}
	}
	
	//Quiz Response, Correct
	function showQuizCorrectAssets()	{
	
		checkboxObj = document.getElementById('useQuizCorrectAssets');
	
		var theObj = document.querySelector("#AssetViewCorrect");
		
		if(checkboxObj.checked)
		{
			removeClass(theObj,"detailsHide");
			addClass(theObj, "detailsShow");
		}else{
			removeClass(theObj,"detailsShow");
			addClass(theObj, "detailsHide");
		}
	}
	
	//Quiz Response, InCorrect
	function showQuizIncorrectAssets()	{
	
		checkboxObj = document.getElementById('useQuizIncorrectAssets');
	
		var theObj = document.querySelector("#AssetViewIncorrect");
		
		if(checkboxObj.checked)
		{
			removeClass(theObj,"detailsHide");
			addClass(theObj, "detailsShow");
		}else{
			removeClass(theObj,"detailsShow");
			addClass(theObj, "detailsHide");
		}
	}
	
</script>
    
<cfoutput> 

<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="100" align="right" valign="middle">Asset Name</td>
      <td valign="middle"><input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128" /></td>
    </tr>
    <tr>
      <td colspan="2" align="right" valign="middle"><hr size="1" /></td>
    </tr>
    <tr>
      <td height="44" align="right" valign="middle" class="content">Complet Question</td>
      <td align="left" valign="middle" class="content">
      
      <input name="needsToBeCompleted" type="checkbox" id="needsToBeCompleted" value="1" <cfif asset.complete>checked</cfif> /><label for="needsToBeCompleted"></label>
      
      Question needs to be completed, cannot be passed
      
      </td>
    </tr>
    <tr>
      <td height="44" align="right" valign="middle" class="content">Movable Choices</td>
      <td align="left" valign="middle" class="content">
      <input name="movable" type="checkbox" id="movable" value="1" <cfif asset.movable>checked</cfif> /><label for="movable"></label>
      This choices are all movable, not a selection type (drag and drop - selection)</td>
    </tr>
    <tr>
      <td height="44" align="right" valign="middle" class="content">Highlight Correct</td>
      <td align="left" valign="middle" class="content">
      <input name="displayCorrect" type="checkbox" id="displayCorrect" value="1" <cfif asset.displayCorrect>checked</cfif> /><label for="displayCorrect"></label>
      This highlights the answer if the selected choice is correct. Usually for Movable type only</td>
    </tr>
</table>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content"> 
<tr><td>

<table width="800" border="0" cellpadding="0" cellspacing="10" class="content" bgcolor="##EEE">    
    <tr>
      <td height="44" colspan="2" align="left" valign="middle" bgcolor="##996633"><span class="contentLinkWhite" style="padding-left:10px">Question</span></td>
    </tr>
    <tr>
      <td align="right" valign="top"> Use Assets</td>
      <td valign="middle" class="contentLinkGrey">
      <input name="useQuizAssets" type="checkbox" id="useQuizAssets" value="true" onchange="showQuizAssets()" <cfif quizAssets.recordCount GT 0> checked</cfif> /><label for="useQuizAssets"></label>
      Attach any  assets to be display along with the question</td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="top">
        
        <div class="detailsHide" id="AssetViewQuiz">
        
  		<!--- Assets --->
        <div class="sectionLink" style="margin-left:0px; margin-top:5px; height:auto; width:810px;">
        <div class="sectionLink" style="background-color:##666; height:40px; padding-left:10px; width:800px">
          <div style="width:200px;float:left; height: 40px;line-height: 40px;">Assets</div>
          <div style="width:44px;float:right; height: 40px;line-height: 40px;" onclick="displayImporterType('AssetsImporterQuiz')">
            <img src="images/import.png" width="44" height="44" />
          </div>
        </div>
        <!--- Import Assets --->
        <div style="width:800px; display:none;margin-left:0px; margin-top:5px; height:auto; width:810px;" id="AssetsImporterQuiz"> 
          
            <div style="margin-top: 10px;height: 50px; width:100%">
              <form id="theAssets" style="height:44px">
                <select name="assetTypes" id="assetTypes" onchange="displayAssets(this.options[this.selectedIndex].value,'displayAssetsQuizAssets')" style="height:44px; margin:0px;">
                  <option value="0">All Asset Types</option>
                  <cfloop query="assetTypes">
                  	<cfif ArrayFind(supportAssetTypes,type)>
                    <option value="#type#">#name#</option>
                    </cfif>
                  </cfloop>
                </select>
                <input type="button" class="formText" value="Import Content" onclick="allSelectedContent(this)" />
                <input name="contentIDs" type="hidden" id="contentIDs" />
                <div class="contentLinkGrey" id="totalAssets" style="width:180px; float:right; padding-top:15px"></div>
              </form>
            </div>
          
          <div id="displayAssetsQuizAssets" style="overflow:scroll; margin-top: 0px; color:##666; height:400px; overflow-x: hidden;border-style: solid; border-width: 1px; margin-bottom:40px; padding:10px"></div>
        </div>

    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="content" bgcolor="##FFF">
            <tr>
              <td>
              
              <table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="##CCC">                  
                        <tr class="content">
                          <td height="44" style="padding-left:10px">
                          Asset Name
                          </td>
                          <td width="80" align="center" class="content">
                            Access
                          </td>
                          <td width="60" align="center" class="content">  
                          Cached
                          </td>
                          <td width="60" align="center" class="content">
                           Active
                          </td>
                          <td width="60" align="center" class="content">
                          Shared</td>
                          <td width="80" align="center" class="content">Sort Order </td>
                          <td width="60" align="right" class="content"></td>
      </tr>  
    </table>
    
    <cfif asset.recordCount GT 0>
	<div id="assetsQuizListing">
    <cfloop query="quizAssets">
    
    <cfif active>
			<cfset state = "contentLink">
        <cfelse>
            <cfset state = "contentLinkDisabled">
    </cfif>
    
    <div class="rowhighlighter" onmouseover="showDelete(this,1);" onmouseout="showDelete(this,0);">                 
       <form id="content_id">
                        <table width="810" border="0" cellpadding="5" cellspacing="0">                  
                        <!--- Assets --->
                        <tr>
                          <td width="44">
                            <img src="images/#icon#" width="44" height="44" />
                          </td>
                          <td colspan="2">
                          <a href="AssetsView.cfm?assetID=#content_id#&amp;assetTypeID=#assetType_id#" class="#state#">
                          <div style="height:32px; padding-top:15px; padding-left:4px">#name#</div>
                          </a>
                          </td>
                          <td width="80" align="center" class="content">
    
                            <cfif accessLevel IS 0>
                                <cfset lock = 'access_unlocked'>
                            <cfelse>
                                <cfset lock = 'access_locked-'& accessLevel>
                            </cfif>
                            
                            <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateContentAssetOptions(this,#content_id#)" />
                                <cfloop query="accessLevels">
                                <option value="#accessLevel#" style="color:##333" <cfif quizAssets.accessLevel IS accessLevel> selected</cfif>>#levelName#</option>
                                </cfloop>
                            </select>
                          
                          </td>
                          <td width="60" align="center" class="content">  
                          <cfif cached IS '0'>
                          <cfset theCacheState = 'cache'>
                          <cfelse>
                          <cfset theCacheState = 'cached'>
                          </cfif>
                          <div id="contentCached" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer; width:28px; height:28px; background-image:url(images/#theCacheState#.png)">
                          </div>
                          </td>
                          <td width="60" align="center" class="content">
                          
                         
                          <cfif active IS '0'>
                            <cfset stateCSS = "contentLinkGreen">
                          <cfelse>
                            <cfset stateCSS = "contentLinkRed">
                          </cfif>
                          <div id="contentActive" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer" class="#stateCSS#">
                              <cfif active IS '0'>
                              _NO
                              <cfelse>
                              _YES
                              </cfif>
                          </div>
                          </td>
                          <td width="60" align="center" class="content">
                          
                          <cfif sharable IS '0'>
                            <cfset stateCSS = "contentLinkGreen">
                          <cfelse>
                            <cfset stateCSS = "contentLinkRed">
                          </cfif>
                          <div id="contentShared" style="cursor:pointer" onclick="updateContentAssetOptions(this,#content_id#)" class="#stateCSS#">
                          <cfif sharable IS '0'>
                          NO
                          <cfelse>
                          YES
                          </cfif>
                          </div>
                          
                          </td>
                          <td width="80" align="center" class="contentLinkDisabled">
                          <input name="sortOrder" type="text" id="sortOrder" value="#sortOrder#" size="4" maxlength="10" onchange="updateContentAssetOptions(this,#content_id#)" /></td>
                          <td width="60" align="right" class="content">
                          
                          <table border="0">
                          <tr>
                            <td>
                            <input type="button" class="itemHide" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteContentAsset(#content_id#,this);" value="" /> 
                            </td>
                          </tr>
                          
                          
                          
                        </table>
                          
                          
                          </td>
                        </tr>
                        
                        </table>
                      </form>
    </div>
    
    </cfloop>  
    </div>
    
    <cfelse>
    <span class="content">
    No Assets
    </span>
    </cfif>

          
          </td>
      </tr>
      </table>
   	
       
  </div>      
        
        
        </td>
    </tr>
    
    <tr>
      <td colspan="2" align="left" valign="top"><hr size="1" /></td>
    </tr>
    <tr>
      <td align="right" valign="top">Question</td>
      <td valign="middle" class="contentLinkGrey">
      <textarea name="questionMessage" cols="60" rows="4" class="formfieldcontent" id="questionMessage" style="width:600px; height:100px">#asset.questionMessage#</textarea></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Need Response</td>
      <td height="44" valign="middle" class="content">
      
        <cfif asset.needResponse>
            <cfset responseOption = "block"><cfelse><cfset responseOption = "none">
          </cfif>
        <cfif asset.tryAgain GT 0>
            <cfset responseTry = "block"><cfelse><cfset responseTry = "none">
          </cfif>

        <table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td width="80"><input name="needResponse" type="checkbox" id="needResponse" value="1" onchange="displayResponseMessages(this.checked,'reponseMessages')" <cfif asset.needResponse>checked</cfif> /><label for="needResponse"></label></td>
            <td class="content">Question needs a respose for Correct/Incorrect Answers</td>
          </tr>
        </table>
        </td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="middle">
      
      <div id="reponseMessages" style="display:#responseOption#">
          <table width="100%" border="0" cellpadding="0" cellspacing="10" class="content">
          
          <tr>
      <td height="44" colspan="2" align="left" valign="middle" bgcolor="##333333" class="contentLinkWhite" style="padding-left:10px">Response Messages</td>
    </tr>
    <tr>
          <td width="90" align="right" valign="top" style="padding-top:20px">Try Again</td>
          <td valign="middle" class="content" height="44">
          
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td width="80"><input name="tryAgain" type="checkbox" id="tryAgain" value="1" onchange="displayResponseMessages(this.checked,'numberTries')" <cfif asset.tryAgain GT 0>checked</cfif> /><label for="tryAgain"></label> </td>
                <td class="content">If the question is incorrectly answered, the option is to Try Again. <br />User answer correctly to proceed - this is controlled by number of tries</td>
              </tr>
              <tr>
                <td width="80" align="right" class="content">&nbsp;</td>
                <td>
                <div id="numberTries" style="display:#responseTry#">
                <table width="100%" border="0" cellspacing="0" cellpadding="5">
                  <tr>
                    <td width="10"><input name="numberOfTries" type="text" class="formfieldcontent" id="numberOfTries" value="#asset.tryAgain#" size="4" maxlength="2" /></td>
                    <td class="content"> Number of tries to accept in order to continue. <br /> Zero (0) tries will force the user the answer correctly before proceeding </td>
                  </tr>
                </table>
                </div>
                </td>
              </tr>
            </table>
            
          </td>
        </tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="10" class="content" bgcolor="##c0fbd4">        
    <tr>
            <td height="32" colspan="2" align="left" valign="middle" bgcolor="##009933" class="contentLinkWhite" style="padding-left:10px">Correct Response</td>
            </tr>
          <tr>
            <td align="right" valign="top">Attach Assets</td>
            <td valign="middle" class="contentLinkGrey">
            
            <input name="useQuizCorrectAssets" type="checkbox" id="useQuizCorrectAssets" value="true" onchange="showQuizCorrectAssets()" <cfif quizCorrectAssets.recordCount GT 0> checked</cfif> /><label for="useQuizCorrectAssets"></label>
            
            Attach any  assets to be display along with the <span class="contentLinkRed">Correct</span> Answer</td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top" class="content">
            
             <div class="detailsHide" id="AssetViewCorrect">
        
  		<!--- Assets --->
        <div class="sectionLink" style="margin-left:0px; margin-top:5px; height:auto; width:810px;">
        <div class="sectionLink" style="background-color:##666; height:40px; padding-left:10px; width:800px">
          <div style="width:200px;float:left; height: 40px;line-height: 40px;">Assets</div>
          <div style="width:44px;float:right; height: 40px;line-height: 40px;" onclick="displayImporterType('AssetsImporterCorrect')">
            <img src="images/import.png" width="44" height="44" />
          </div>
        </div>
        <!--- Import Assets --->
        <div style="width:800px; display:none;margin-left:0px; margin-top:5px; height:auto; width:810px;" id="AssetsImporterCorrect"> 
          
            <div style="margin-top: 10px;height: 50px; width:100%">
              <form id="theAssets" style="height:44px">
                <select name="assetTypes" id="assetTypes" onchange="displayAssets(this.options[this.selectedIndex].value,'displayAssetsQuizCorrectAssets')" style="height:44px; margin:0px;">
                  <option value="0">All Asset Types</option>
                  <cfloop query="assetTypes">
                  	<cfif ArrayFind(supportAssetTypes,type)>
                    <option value="#type#">#name#</option>
                    </cfif>
                  </cfloop>
                </select>
                <input type="button" class="formText" value="Import Content" onclick="allSelectedContent(this)" />
                <input name="contentIDs" type="hidden" id="contentIDs" />
                <div class="contentLinkGrey" id="totalAssets" style="width:180px; float:right; padding-top:15px"></div>
              </form>
            </div>
          
          <div id="displayAssetsQuizCorrectAssets" style="overflow:scroll; margin-top: 0px; color:##666; height:400px; overflow-x: hidden;border-style: solid; border-width: 1px; margin-bottom:40px; padding:10px"></div>
        </div>
        
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="content" bgcolor="##FFF">
            <tr>
              <td>
              
              <table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="##CCC">                  
                        <tr class="content">
                          <td height="44" style="padding-left:10px">
                          Asset Name
                          </td>
                          <td width="80" align="center" class="content">
                            Access
                          </td>
                          <td width="60" align="center" class="content">  
                          Cached
                          </td>
                          <td width="60" align="center" class="content">
                           Active
                          </td>
                          <td width="60" align="center" class="content">
                          Shared</td>
                          <td width="80" align="center" class="content">Sort Order </td>
                          <td width="60" align="right" class="content"></td>
      </tr>  
    </table>
    
			<cfif asset.recordCount GT 0>
            <div id="assetsCorrectListing">
            <cfloop query="quizCorrectAssets">
            
            <cfif active>
                    <cfset state = "contentLink">
                <cfelse>
                    <cfset state = "contentLinkDisabled">
            </cfif>
            
            <div class="rowhighlighter" onmouseover="showDelete(this,1);" onmouseout="showDelete(this,0);">                 
               <form id="content_id">
                                <table width="810" border="0" cellpadding="5" cellspacing="0">                  
                                <!--- Assets --->
                                <tr>
                                  <td width="44">
                                    <img src="images/#icon#" width="44" height="44" />
                                  </td>
                                  <td colspan="2">
                                  <a href="AssetsView.cfm?assetID=#content_id#&amp;assetTypeID=#assetType_id#" class="#state#">
                                  <div style="height:32px; padding-top:15px; padding-left:4px">#name#</div>
                                  </a>
                                  </td>
                                  <td width="80" align="center" class="content">
            
                                    <cfif accessLevel IS 0>
                                        <cfset lock = 'access_unlocked'>
                                    <cfelse>
                                        <cfset lock = 'access_locked-'& accessLevel>
                                    </cfif>
                                    
                                    <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateContentAssetOptions(this,#content_id#)" />
                                        <cfloop query="accessLevels">
                                        <option value="#accessLevel#" style="color:##333" <cfif quizCorrectAssets.accessLevel IS accessLevel> selected</cfif>>#levelName#</option>
                                        </cfloop>
                                    </select>
                                  
                                  </td>
                                  <td width="60" align="center" class="content">  
                                  <cfif cached IS '0'>
                                  <cfset theCacheState = 'cache'>
                                  <cfelse>
                                  <cfset theCacheState = 'cached'>
                                  </cfif>
                                  <div id="contentCached" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer; width:28px; height:28px; background-image:url(images/#theCacheState#.png)">
                                  </div>
                                  </td>
                                  <td width="60" align="center" class="content">
                                  
                                 
                                  <cfif active IS '0'>
                                    <cfset stateCSS = "contentLinkGreen">
                                  <cfelse>
                                    <cfset stateCSS = "contentLinkRed">
                                  </cfif>
                                  <div id="contentActive" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer" class="#stateCSS#">
                                      <cfif active IS '0'>
                                      NO
                                      <cfelse>
                                      YES
                                      </cfif>
                                  </div>
                                  </td>
                                  <td width="60" align="center" class="content">
                                  
                                  <cfif sharable IS '0'>
                                    <cfset stateCSS = "contentLinkGreen">
                                  <cfelse>
                                    <cfset stateCSS = "contentLinkRed">
                                  </cfif>
                                  <div id="contentShared" style="cursor:pointer" onclick="updateContentAssetOptions(this,#content_id#)" class="#stateCSS#">
                                  <cfif sharable IS '0'>
                                  NO
                                  <cfelse>
                                  YES
                                  </cfif>
                                  </div>
                                  
                                  </td>
                                  <td width="80" align="center" class="contentLinkDisabled">
                                  <input name="sortOrder" type="text" id="sortOrder" value="#sortOrder#" size="4" maxlength="10" onchange="updateContentAssetOptions(this,#content_id#)" /></td>
                                  <td width="60" align="right" class="content">
                                  
                                  <table border="0">
                                  <tr>
                                    <td>
                                    <input type="button" class="itemHide" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteContentAsset(#content_id#,this);" value="" /> 
                                    </td>
                                  </tr>
                                  
                                  
                                  
                                </table>
                                  
                                  
                                  </td>
                                </tr>
                                
                                </table>
                              </form>
            </div>
            
            </cfloop>  
            </div>
            
            <cfelse>
            <span class="content">
            No Assets
            </span>
            </cfif>
        
                  
                  </td>
              </tr>
              </table>
            
               
          </div> 
            
            </td>
            </tr>
          
          <tr>
            <td colspan="2" align="left" valign="top"><hr size="1" /></td>
            </tr>
          <tr>
            <td width="90" align="right" valign="top">Correct </td>
            <td valign="middle" class="contentLinkGrey"><textarea name="correctMessage" cols="60" rows="4" class="formfieldcontent" id="correctMessage" style="width:600px; height:100px">#asset.correctMessage#</textarea></td>
          </tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="10" class="content" bgcolor="##fdcfcf">          
    <tr>
            <td height="32" colspan="2" align="left" valign="middle" bgcolor="##993333" class="contentLinkWhite" style="padding-left:10px">Incorrect Response</td>
            </tr>
          <tr>
            <td align="right" valign="top">Attach Assets</td>
            <td valign="middle" class="contentLinkGrey"><input name="useQuizIncorrectAssets" type="checkbox" id="useQuizIncorrectAssets" onchange="showQuizIncorrectAssets()" value="true" <cfif quizIncorrectAssets.recordCount GT 0> checked</cfif> /><label for="useQuizIncorrectAssets"></label> Attach any number of assets to be display along with the <span class="contentLinkGreen">Incorrect</span> Answer</td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top">
            
            <div class="detailsHide" id="AssetViewIncorrect">
        
  		<!--- Assets --->
        <div class="sectionLink" style="margin-left:0px; margin-top:5px; height:auto; width:810px;">
        <div class="sectionLink" style="background-color:##666; height:40px; padding-left:10px; width:800px">
          <div style="width:200px;float:left; height: 40px;line-height: 40px;">Assets</div>
          <div style="width:44px;float:right; height: 40px;line-height: 40px;" onclick="displayImporterType('AssetsImporterIncorrect')">
            <img src="images/import.png" width="44" height="44" />
          </div>
        </div>
        <!--- Import Assets --->
        <div style="width:800px; display:none;margin-left:0px; margin-top:5px; height:auto; width:810px;" id="AssetsImporterIncorrect"> 
          
            <div style="margin-top: 10px;height: 50px; width:100%">
              <form id="theAssets" style="height:44px">
                <select name="assetTypes" id="assetTypes" onchange="displayAssets(this.options[this.selectedIndex].value,'displayAssetsQuizIncorrectAssets')" style="height:44px; margin:0px;">
                  <option value="0">All Asset Types</option>
                  <cfloop query="assetTypes">
                  	<cfif ArrayFind(supportAssetTypes,type)>
                    <option value="#type#">#name#</option>
                    </cfif>
                  </cfloop>
                </select>
                <input type="button" class="formText" value="Import Content" onclick="allSelectedContent(this)" />
                <input name="contentIDs" type="hidden" id="contentIDs" />
                <div class="contentLinkGrey" id="totalAssets" style="width:180px; float:right; padding-top:15px"></div>
              </form>
            </div>
          
          <div id="displayAssetsQuizIncorrectAssets" style="overflow:scroll; margin-top: 0px; color:##666; height:400px; overflow-x: hidden;border-style: solid; border-width: 1px; margin-bottom:40px; padding:10px"></div>
        </div>
        
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="content" bgcolor="##FFF">
            <tr>
              <td>
              
              <table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="##CCC">                  
                        <tr class="content">
                          <td height="44" style="padding-left:10px">
                          Asset Name
                          </td>
                          <td width="80" align="center" class="content">
                            Access
                          </td>
                          <td width="60" align="center" class="content">  
                          Cached
                          </td>
                          <td width="60" align="center" class="content">
                           Active
                          </td>
                          <td width="60" align="center" class="content">
                          Shared</td>
                          <td width="80" align="center" class="content">Sort Order </td>
                          <td width="60" align="right" class="content"></td>
      </tr>  
    </table>
    
			<cfif asset.recordCount GT 0>
            <div id="assetsIncorrectListing">
            <cfloop query="quizInCorrectAssets">
            
            <cfif active>
                    <cfset state = "contentLink">
                <cfelse>
                    <cfset state = "contentLinkDisabled">
            </cfif>
            
            <div class="rowhighlighter" onmouseover="showDelete(this,1);" onmouseout="showDelete(this,0);">                 
               <form id="content_id">
                                <table width="100%" border="0" cellpadding="5" cellspacing="0">                  
                                <!--- Assets --->
                                <tr>
                                  <td width="44">
                                    <img src="images/#icon#" width="44" height="44" />
                                  </td>
                                  <td colspan="2">
                                  <a href="AssetsView.cfm?assetID=#content_id#&amp;assetTypeID=#assetType_id#" class="#state#">
                                  <div style="height:32px; padding-top:15px; padding-left:4px">#name#</div>
                                  </a>
                                  </td>
                                  <td width="80" align="center" class="content">
            
                                    <cfif accessLevel IS 0>
                                        <cfset lock = 'access_unlocked'>
                                    <cfelse>
                                        <cfset lock = 'access_locked-'& accessLevel>
                                    </cfif>
                                    
                                    <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateContentAssetOptions(this,#content_id#)" />
                                        <cfloop query="accessLevels">
                                        <option value="#accessLevel#" style="color:##333" <cfif quizInCorrectAssets.accessLevel IS accessLevel> selected</cfif>>#levelName#</option>
                                        </cfloop>
                                    </select>
                                  
                                  </td>
                                  <td width="60" align="center" class="content">  
                                  <cfif cached IS '0'>
                                  <cfset theCacheState = 'cache'>
                                  <cfelse>
                                  <cfset theCacheState = 'cached'>
                                  </cfif>
                                  <div id="contentCached" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer; width:28px; height:28px; background-image:url(images/#theCacheState#.png)">
                                  </div>
                                  </td>
                                  <td width="60" align="center" class="content">
                                  
                                 
                                  <cfif active IS '0'>
                                    <cfset stateCSS = "contentLinkGreen">
                                  <cfelse>
                                    <cfset stateCSS = "contentLinkRed">
                                  </cfif>
                                  <div id="contentActive" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer" class="#stateCSS#">
                                      <cfif active IS '0'>
                                      NO
                                      <cfelse>
                                      YES
                                      </cfif>
                                  </div>
                                  </td>
                                  <td width="60" align="center" class="content">
                                  
                                  <cfif sharable IS '0'>
                                    <cfset stateCSS = "contentLinkGreen">
                                  <cfelse>
                                    <cfset stateCSS = "contentLinkRed">
                                  </cfif>
                                  <div id="contentShared" style="cursor:pointer" onclick="updateContentAssetOptions(this,#content_id#)" class="#stateCSS#">
                                  <cfif sharable IS '0'>
                                  NO
                                  <cfelse>
                                  YES
                                  </cfif>
                                  </div>
                                  
                                  </td>
                                  <td width="80" align="center" class="contentLinkDisabled">
                                  <input name="sortOrder" type="text" id="sortOrder" value="#sortOrder#" size="4" maxlength="10" onchange="updateContentAssetOptions(this,#content_id#)" /></td>
                                  <td width="60" align="right" class="content">
                                  
                                  <table border="0">
                                  <tr>
                                    <td>
                                    <input type="button" class="itemHide" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteContentAsset(#content_id#,this);" value="" /> 
                                    </td>
                                  </tr>
                                  
                                  
                                  
                                </table>
                                  
                                  
                                  </td>
                                </tr>
                                
                                </table>
                              </form>
            </div>
            
            </cfloop>  
            </div>
            
            <cfelse>
            <span class="content">
            No Assets
            </span>
            </cfif>
        
                  
                  </td>
              </tr>
              </table>
            
               
          </div>
            
            </td>
            </tr>
          
          <tr>
            <td colspan="2" align="left" valign="top"><hr size="1" /></td>
            </tr>
          <tr>
          <td align="right" valign="top">Incorrect </td>
          <td valign="middle" class="contentLinkGrey"><textarea name="incorrectMessage" cols="60" rows="4" class="formfieldcontent" id="incorrectMessage" style="width:600px; height:100px">#asset.incorrectMessage#</textarea></td>
        </tr>
</table>
      </div>
      
      </td>
    </tr>
</table>    

<table width="100%" border="0" cellpadding="0" cellspacing="10" class="content" bgcolor="##EEE"> 
    <tr>
      <td height="44" colspan="2" align="left" valign="middle" bgcolor="##CCCCCC" class="contentLinkGrey" style="padding-left:10px">Selection Area</td>
    </tr>
    <tr>
      <td align="right" valign="middle"> <input type="hidden" name="selectionID" id="selectionID" value="#asset.selection_id#">
        Selection Layout</td>
      <td align="right" valign="middle">
      
      <table width="100%" border="0" cellspacing="0" cellpadding="5" bgcolor="##FFF">
        <tr>
          <td width="100" align="right" class="content">Padding</td>
          <td width="20"><input name="gridPadding" type="text" class="formfieldcontent" id="gridPadding" value="#selectionSpecs.padding#" size="4" maxlength="2" style="margin-left:10px" /></td>
          <td width="100" align="right" class="content">Spacing</td>
          <td width="20"><input name="gridSpacing" type="text" class="formfieldcontent" id="gridSpacing" value="#selectionSpecs.spacing#" size="4" maxlength="2" style="margin-left:10px" /></td>
          <td width="150" align="right" class="content">Number of Colums</td>
          <td width="20"><input name="colums" type="text" class="formfieldcontent" id="colums" value="#selectionSpecs.numberOfCols#" size="4" maxlength="2" style="margin-left:10px" /></td>
          <td>&nbsp;</td>
          </tr>
      </table>
      
      </td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="middle"><hr size="1"></td>
      </tr>
    <tr>
      <td align="right" valign="middle">Selection Display</td>
      <td align="left" valign="middle"><cfoutput>
        <table width="100%" border="0" cellpadding="0" cellspacing="10" class="content" bgcolor="##FFF">
          <tr>
            <td width="69" align="right">Color</td>
            <td width="681" align="left"><table width="100%" border="0" cellspacing="5">
              <tr>
                <td width="70" align="left" class="content">Foreground</td>
                <td width="120"><input name="selectionColor" type="text" class="color formfieldcontent" id="selectionColor" value="#selectionSpecs.forecolor#" size="8" maxlength="8" /></td>
                <td width="90" align="right" class="content">Background</td>
                <td width="120"><input name="selectionBGColor" type="text" class="color formfieldcontent" id="selectionBGColor" value="#selectionSpecs.backcolor#" size="8" maxlength="8" /></td>
                <td>&nbsp;</td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="right">Image</td>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="300"><input name="selectionBKImg" style="width:300px" type="text" class="formfieldcontent" id="selectionBKImg" /></td>
                <td width="44" align="right"><input type="button" value="#selectionSpecs.background#" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" /></td>
                <td align="right"><input name="selectionBackgroundImage" type="file" style="padding-left:0px" class="formfieldcontent" id="selectionBackgroundImage" /></td>
                <td width="15" align="right">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
      </cfoutput>
      
      </td>
    </tr>
    </table>
    
</cfoutput>

<script type="text/javascript">
	displayChoices();
</script>

<cfif quizAssets.recordCount GT 0>

	<script type="text/javascript">
    
	showQuizAssets();
	
    </script>

</cfif>

<cfif quizCorrectAssets.recordCount GT 0>

	<script type="text/javascript">
    
	showQuizCorrectAssets();
	
    </script>

</cfif>

<cfif quizIncorrectAssets.recordCount GT 0>

	<script type="text/javascript">
    
	showQuizIncorrectAssets();
	
    </script>

</cfif>