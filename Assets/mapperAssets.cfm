<link href="../styles.css" rel="stylesheet" type="text/css" />
<link href="JS/domtab.css" rel="stylesheet" type="text/css">


<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<script type="text/javascript" src="Assets/instanceName.js"></script>


<cfinvoke component="CFC.Assets" method="getMapperAssets" returnvariable="mapperAssets">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfajaxproxy cfc="MapperUtil" jsclassname="UpdateMapperAsset">

<script type="text/javascript" src="JS/domtab.js"></script>

<script type="text/javascript">

var imageMapObject;
var mapShow = false;

jsAssetMappper = new UpdateMapperAsset();

toggleActiveState = function(theObject,assetID)
{

    var theState = document.getElementById('activeState_'+theObject).innerHTML;
	theState = theState.trim();
	var theObjState;
	
	jsAssetMappper.setSyncMode();
	
	if(theState == 'ACTIVE')
	{
		theObjState = 'INACTIVE';
		theNewState = 0;
		CSSStyle = 'contentLinkGreen';
		CSStyleContent = 'contentLinkDisabled';
		
		jsAssetMappper.setMapperAssetState(<cfoutput>#assetID#</cfoutput>,assetID,theNewState);
	
		document.getElementById('activeState_'+theObject).innerHTML = theObjState;
		document.getElementById('activeState_'+theObject).className = CSSStyle;
		
	}else{
		
		theObjState = 'ACTIVE';
		theNewState = 1;
		CSSStyle = 'contentLinkRed';
		CSStyleContent = 'contentLink';
		
		jsAssetMappper.setMapperAssetState(<cfoutput>#assetID#</cfoutput>,assetID,theNewState);
	
		document.getElementById('activeState_'+theObject).innerHTML = theObjState;
		document.getElementById('activeState_'+theObject).className = CSSStyle;
	}
	
	document.getElementById('contentAsset_'+theObject).className = CSStyleContent;
	
}

showAssets = function(result){
	
	if(result == '') 
	{
		removePreview();
	};
	
	var select = document.getElementById("mapAssetID");
	select.options[0]=null;
	select.options.length = 0;
	
	//None
	var option = document.createElement("option");
	
	option.text = "None";
	option.value = 0;
	option.addEventListener("click", function() { removePreview();setMapAsset(), true });
	
	select.appendChild(option);
	
	//All Assets
	for(i = 0; i < result.length; i++)
	{	
	var option = document.createElement("option");
	
	option.text = result[i].name;
	option.value = result[i].asset_id;
		
	option.addEventListener("click", function() { removePreview();setMapAsset(), true });
	
	select.appendChild(option);
	
	}

}

getAllAssetsFromType = function(theType)
{
	removePreview();
	
	jsAssetMappper.setSyncMode();
	jsAssetMappper.setCallbackHandler(showAssets);
	jsAssetMappper.getAssetsFromType(<cfoutput>#session.appID#</cfoutput>,theType);
	
}

function setMapAsset()
{
	jsAssetMappper.setSyncMode();
	theAssetID = parseInt(document.getElementById("mapAssetID").value);
	jsAssetMappper.setMapAsset(<cfoutput>#assetID#</cfoutput>,theAssetID);
}

function displayAsset()
{
	theAssetID = parseInt(document.getElementById("mapAssetID").value);
	
	jsAssetMappper.setSyncMode();
	
	jsAssetMappper.setCallbackHandler(showPreview);

	jsAssetMappper.getMapImageThumb(theAssetID);
}



function removePreview()
{
	mapImage = document.getElementById('mapImageSrc');
	
	if(mapImage != null)
	{
		theDiv.removeChild(mapImage);
	}
	
	mapShow = false;
}

function showPreview(result)
{
	imageMapObject = result;
	
	theDiv = document.getElementById("mapImage");
	
	mapImage = document.getElementById('mapImageSrc');
	
	if(mapImage != null)
	{
		theDiv.removeChild(mapImage);
	}
	
	if(imageMapObject.assetTypeID != 0 && imageMapObject.url != '')
	{
		
	var elem = document.createElement("img");
	
	elem.setAttribute("id", "mapImageSrc");
	elem.setAttribute("src", imageMapObject.url);
	elem.setAttribute("name", imageMapObject.name);
	elem.setAttribute("height", "240");
	
	theDiv.appendChild(elem);
	
	}
	
	mapShow = true;
}

function showMapImage()
{
	if(mapShow){
		removePreview();
	}else{
		displayAsset();
	}
}

function addAsset(theObj,theType) {
		
		theObjRef = document.getElementById(theType+'Asset_'+theObj);
		
		theAsset = document.getElementById(theType+'ID_'+theObj);
		assetID = theAsset.value;

		if(theObjRef.className == "checkmarkSelected")
		{
			state = false;
		}else{
			state = true;
		}
		
		setCheckmarkState(theObj,state,theType);
		
		jsAssetMappper.addAssetToMapper(<cfoutput>#assetID#</cfoutput>,assetID);
	
}

function setCheckmarkState(theObj,state,theType)	{
	
	theObjRef = document.getElementById(theType+'Asset_'+theObj);
		
		if(state)
		{
			removeClass(theObjRef,"checkmarkNormal");
			addClass(theObjRef, "checkmarkSelected");
			
		}else{
			removeClass(theObjRef,"checkmarkSelected");
			addClass(theObjRef, "checkmarkNormal");
		}
	
}

</script>

<!--- Get All IDS used for Map --->
<cfinvoke component="MapperUtil" method="getMapperAssets" returnvariable="ids" />

<cfinvoke component="MapperUtil" method="getMapAsset" returnvariable="mapImg">
	<cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfif NOT importAssets>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td>
      <cfoutput>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128" />
      </cfoutput>
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">Map </td>
      <td>
      <!--- types filter --->
      

        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="160" align="left" class="content">
 
            <select name="mapAssetTypeID" style="width:150px;" id="mapAssetTypeID" onchange="getAllAssetsFromType(this.value);">

      
      <option value="0">None</option>
      
      <cfoutput query="session.AssetsTypes">
      
      <cfset found = arrayFind([1,6],type)>
		  <cfif found>
            <option value="#type#">#name#</option>
        </cfif>
      </cfoutput>
      </select>
            </td>
            <td width="410">
            <select name="mapAssetID" id="mapAssetID" style="width:400px">
                <option value="0">None</option>
              </select>
            </td>
            <td width="120"><a onclick="showMapImage();"><img src="images/image.png" width="32" height="32" /></a></td>
          </tr>
        </table>
    
      </td>
    </tr>
</table>
<div id="mapImage" style="margin-left:100px; margin-bottom:10px;"></div>
</cfif>

<cfif importAssets><cfset cb = '069'><cfelse><cfset cb = '333'></cfif>

<div style="background-color:#<cfoutput>#cb#</cfoutput>; padding-bottom: 5px; padding-left:5px; margin-top:5px; height:44px; width:805px">
<cfoutput>
<table width="800" border="0">
  <tr>
    <td class="plainLinkWhite">
    <cfif importAssets>
      Select a GPS Point or a XYZ Point to Import
    <cfelse>
     All Points Added to the Current Map
    </cfif>
    </td>
    <td align="right" valign="top" class="function" style="color:##999">
    <cfset state = NOT importAssets>
    <cfif importAssets><cfset importState = "dn"><cfelse><cfset importState = "up"></cfif>
    <a href="AssetsView.cfm?assetID=#assetID#&amp;assetTypeID=#assetTypeID#&amp;importAssets=#state#">
    <img src="images/importPan_#importState#.png" width="44" height="44" />
    </a>
    </td>
  </tr>
</table>
</cfoutput>
</div>

<cfif importAssets>

<!--- GPS Points --->
<cfinvoke component="CFC.Assets" method="getAssets" returnvariable="gpsAssets">
  <cfinvokeargument name="appID" value="#session.appID#"/>
  <cfinvokeargument name="assetTypeID" value="5"/>
</cfinvoke>

<cfset pointImgPath = assetPaths.types[5]>

<!--- XYZ Points --->
<cfinvoke component="CFC.Assets" method="getAssets" returnvariable="pointAssets">
  <cfinvokeargument name="appID" value="#session.appID#"/>
  <cfinvokeargument name="assetTypeID" value="10"/>
</cfinvoke>

<cfset gpsImgPath = assetPaths.types[10]>

 <div class="domtab" style="padding-bottom:20px; padding-top:5px;">
  <ul class="domtabs">
    <li><a href="#t1" class="contentLink" id="gps">Locations (GPS)</a></li>
    <li><a href="#t2" class="contentLink" id="pnts">XYZ Points(2D or 3D Points)</a></li>
  </ul>

  
  <div>
    <a name="t1" id="t1"></a>
    <!--- GPS Points --->
    <cfset cnt = 0>
    <cfoutput query="gpsAssets">
    

    
     <cfset dateCre = DateAdd("s", created ,DateConvert("utc2Local", "January 1 1970 00:00"))>
     <div class="rowhighlighter">
    <table width="800" border="0" cellpadding="0" cellspacing="5">
        <tr>
          <td width="44"><img src="images/#icon#" width="44" height="44" alt="#name#" /></td>
          <td colspan="2"><div style="height:32px; padding-top:15px; padding-left:4px"><a href="AssetsView.cfm?assetID=#asset_ID#&amp;assetTypeID=#assetTypeID#" class="contentLink">#assetName#
            <input name="GPSID_#cnt#" type="hidden" id="GPSID_#cnt#" value="#asset_id#" />
          </a></div></td>
          <td width="80" align="center" class="content">
            
            
            <cfif thumbnail NEQ ''>
            <a href="#pointImgPath#thumbs/#thumbnail#" target="_new">
            <img src="#pointImgPath#thumbs/#thumbnail#" name="#assetName#" width="60" height="44" class="imgLockedAspect" id="#assetName#" />
            </a>
            <cfelse>
              <span class="contentGreyed">No Thumb</span>
            </cfif>
            
            
            </td>
          <td width="80" align="right" class="content">#dateFormat(dateCre,'MMM DD, YY')#</td>
          <td width="60" align="right" class="content">
          <cfif arrayFind(ids,asset_id)><cfset checked = 'checkmarkSelected'><cfelse><cfset checked = 'checkmarkNormal'></cfif>
            <input name="GPSAsset_#cnt#" type="button" id="GPSAsset_#cnt#" class="#checked#" onclick="addAsset(#cnt#,'GPS');" />
          </td>
        </tr>
      </table>
      </div>
      <cfset cnt++>
      

      
    </cfoutput>
    
    <cfif cnt IS '0'>
    	<span class="content">No Assets to Assign</span>
    </cfif>
    
  </div>
  
  <div>
    <a name="t2" id="t2"></a>
    <!--- XYZ Points --->
    <cfset cnt = 0>
    <cfoutput query="pointAssets">
    

    
     <cfset dateCre = DateAdd("s", created ,DateConvert("utc2Local", "January 1 1970 00:00"))>
     <div class="rowhighlighter">
    <table width="800" border="0" cellpadding="0" cellspacing="5">
        <tr>
          <td width="44"><img src="images/#icon#" width="44" height="44" alt="#name#" /></td>
          <td colspan="2"><div style="height:32px; padding-top:15px; padding-left:4px"><a href="AssetsView.cfm?assetID=#asset_ID#&amp;assetTypeID=#assetTypeID#" class="contentLink">#assetName#
            <input name="PNTID_#cnt#" type="hidden" id="PNTID_#cnt#" value="#asset_id#" />
          </a></div></td>
          <td width="80" align="center" class="content">
            
            <cfif thumbnail NEQ ''>
            <a href="#gpsImgPath#thumbs/#thumbnail#" target="_new">
            <img src="#pointImgPath#thumbs/#thumbnail#" name="#assetName#" width="60" height="44" class="imgLockedAspect" id="#assetName#" />
            </a>
            <cfelse>
              <span class="contentGreyed">No Thumb</span>
            </cfif>
            
            </td>
          <td width="80" align="right" class="content">#dateFormat(dateCre,'MMM DD, YY')#</td>
          <td width="60" align="right" class="content">
          <cfif arrayFind(ids,asset_id)><cfset checked = 'checkmarkSelected'><cfelse><cfset checked = 'checkmarkNormal'></cfif>
            <input name="PNTAsset_#cnt#" type="button" id="PNTAsset_#cnt#" class="#checked#" onclick="addAsset(#cnt#,'PNT');" />
          </td>
        </tr>
      </table>
      </div>
      <cfset cnt++>
      

      
      </cfoutput>
      
      <cfif cnt IS '0'>
    	<span class="content">No Assets to Assign</span>
    </cfif>
      
  </div>
  
</div>

<cfelse>

    <table border="0" cellpadding="0" cellspacing="0" class="content" style="padding-top:4px">
    <tr>
      <td>

      <cfset z = 0>
      <cfoutput query="mapperAssets">
      <div class="rowhighlighter">
      
      <table width="100%" border="0" cellspacing="5">
      
      <tr>
        <td width="44"><img src="images/#icon#" width="44" height="44" /></td>
        <td align="left" valign="middle" class="content">
        
        <cfif active IS '0'>
          <cfset stateCSS = "contentLinkGreen">
        <cfelse>
          <cfset stateCSS = "contentLinkRed">
        </cfif>
        
        <cfif active IS '0'>
          <cfset state = "contentLinkDisabled">
        <cfelse>
          <cfset state = "contentLink">
        </cfif>
        
        <div class="#state#" id="contentAsset_#z#" style="cursor:pointer; padding-left:10px" onclick="document.location.href='AssetsView.cfm?assetID=#location_id#&assetTypeID=#assetType_id#'">
        #name#
        </div>
        </td>
        <td width="88" align="center" valign="middle" class="content">
        
        <div id="contentActive" style="cursor:pointer" onclick="toggleActiveState('#z#',#location_id#)">
        <a id="activeState_#z#" class="#stateCSS#">
        <cfif active IS '0'>
        INACTIVE
        <cfelse>
        ACTIVE
        </cfif>
        </a>
        </div>
        
        </td>
        
      </tr>
      
    </table>
    
    </div>
    <cfset z++>
      </cfoutput>
      
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>

 </table>
 
</cfif>


<script type="text/javascript">


<cfif asset.recordCount IS 0>
	
	theTypeID = 0;
	theAssetID = 0;
	
<cfelse>
	
	theTypeID = <cfoutput>#mapImg.assetTypeID#</cfoutput>;
	theAssetID = <cfoutput>#asset.mapAsset_ID#</cfoutput>;

</cfif>
	
	//Select Asset Type
	theObj = document.getElementById('mapAssetTypeID');
	cnt = 0;
	len = theObj.options.length;
	
	for(i=0; i< len; i++)
	{
		if(theObj.options[i].value == theTypeID)
		{
			theObj.selectedIndex = cnt;
			getAllAssetsFromType(theTypeID);
			break;
		}
		cnt++;
	}
	
	setTimeout(function() { updateAssetSelected(); }, 100);
	
	function updateAssetSelected()
	{
		//Select Asset
		theMObj = document.getElementById('mapAssetID');
		var mapAssets;
		
		cnt = 0;
		len = theMObj.options.length;
		
		for(i=0; i< len; i++)
		{
			//assetID = mapAssets[i].asset_id;
			theSel = parseInt(theMObj.options[i].value);
			
			if(theSel == theAssetID)
			{
				theMObj.selectedIndex = cnt;
				//displayAsset(theAssetID);
				break;
			}
			cnt++;
		}
	
	}
	
	
</script>