<script type="text/javascript">

var jsAppOptions = new appOptionsSettings();

updateLinkOption = function(theForm,theLinkID)
{
	urlLink = theForm.urlLink.value;
	urlName	= theForm.urlName.value;
	
	jsAppOptions.setSyncMode();
	jsAppOptions.setCallbackHandler(updateLinkOptiondHandler);
	jsAppOptions.updateSupportLink(<cfoutput>#session.appID#</cfoutput>, theLinkID, urlLink, urlName);
}

updateLinkOptiondHandler = function(success)
{
	location.reload();
}

deleteLinkOption = function(theLinkID)
{
	if(confirm('Are you sure you wish to Delete this Link?'))
	{
		jsAppOptions.setSyncMode();
		jsAppOptions.setCallbackHandler(deleteLinkOptionHandler);
		jsAppOptions.deleteSupportLink(<cfoutput>#session.appID#</cfoutput>,theLinkID);
		
	}else{ 
		//nothing
	}
}

deleteLinkOptionHandler = function(success)
{
	location.reload();
}

newLinkOption = function(theForm)
{
	urlLink = theForm.urlLink.value;
	urlName	= theForm.urlName.value;
	
	jsAppOptions.setSyncMode();
	jsAppOptions.setCallbackHandler(newLinkOptionHandler);
	jsAppOptions.newSupportLink(<cfoutput>#session.appID#</cfoutput>, urlLink, urlName,1);
}

newLinkOptionHandler = function(success)
{
	location.reload();
}

</script>


<!--- App Settings --->
<cfinvoke  component="CFC.Apps" method="getSupportLinks" returnvariable="links">
    <cfinvokeargument name="appID" value="#session.appID#"/>
    <cfinvokeargument name="type" value="1"/>
</cfinvoke>
        
<cfset appSupportLinks = "None,AppStore,GooglePlay,PC,MAC,Registration,Other">

  <!---Support Links--->
  
  <cfoutput>
	<table width="100%" border="0" cellspacing="10">
	  <tr class="content">
		<td width="200">Link Name</td>
		<td>URL</td>
		<td>&nbsp;</td>
	  </tr>

	  <cfloop query="links">
	  <form id="updateLink" name="updateLink(this.form,#support_id#)">
	  <tr>
		<td align="left">
		  <select name="urlName" onchange="updateLinkOption()">
		  <cfloop index="linkType" list="#appSupportLinks#" delimiters=",">
			<option value="#linkType#" <cfif urlName IS linkType> selected</cfif>>#linkType#</option>
		  </cfloop>
		  </select>
		</td>
		<td>
		  <input name="urlLink" type="text" class="formfieldcontent" id="urlLink" value="#url#" style="width:450px" maxlength="255" onchange="updateLinkOption(this.form,#support_id#)" />
          </td>
		<td align="right" class="content">
		<input type="button" value="" style="background:url('images/cancel.png') no-repeat; border:0; width:44px; height:44px;" onclick="deleteLinkOption(#support_id#);" />
		</td>
	  </tr>
	  </form>
	  
	  </cfloop>
	  
	  <form id="newLink" name="newLink">
	  <tr>
		<td>
		  <select name="urlName">
		  <cfloop index="linkType" list="#appSupportLinks#" delimiters=",">
			<option value="#linkType#" <cfif "None" IS linkType> selected</cfif>>#linkType#</option>
		  </cfloop>
		  </select>
		</td>
		<td>
		  <input name="urlLink" type="text" class="formfieldcontent" id="urlLink" style="width:450px" maxlength="255" />
        </td>
		<td align="right"><span class="content">
		<input type="button" value="" style="background:url('images/add.png') no-repeat; border:0; width:44px; height:44px;" onclick="newLinkOption(this.form);" />
		</td>
	  </tr>
	  <tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td align="right">&nbsp;</td>
	  </tr>
	  </form>
</cfoutput>
  </table>   
  
