<link href="../styles.css" rel="stylesheet" type="text/css">

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfinvoke component="CFC.Assets" method="getAllAssets" returnvariable="all3DAssets">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="assetType" value="25"/><!--- 3D Type Change to Basic Type 25 --->
</cfinvoke>

<cfinvoke component="CFC.Assets" method="getAllAssets" returnvariable="allImages">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="assetType" value="1"/><!--- Images --->
</cfinvoke>

<cfinvoke component="CFC.Assets" method="getAllAssets" returnvariable="all360Scenes">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="assetType" value="8"/><!--- 360vr scene --->
</cfinvoke>

<script type="text/javascript">

var jsAssets = new assetManager();
	
function setEnvSetting(env) {
	
	var env = parseInt(env)
	
	colorObj = document.getElementById('enviromentColor')
	imgObj = document.getElementById('enviromentImage')
	img360Obj = document.getElementById('enviroment360')
	
	colorObj.style.display = 'none'
	imgObj.style.display = 'none'
	img360Obj.style.display = 'none'
	
	switch(env) {
		case 1:
			//Color
			colorObj.style.display = 'block'
			break;

		case 2:
			//Image
			imgObj.style.display = 'block'
			break;

		case 3:
			//Pano360
			img360Obj.style.display = 'block'
			break;
	}
	
}
	
	function fogOptions(fog) {
		
		fogOpt = document.getElementById('fogOptions')
		
		if(fog) {
			fogOpt.style.display = 'block'
		} else {
			fogOpt.style.display = 'none'
		}
	}
	
	function getAssetThumb(assetID) {
		
		jsAssets.setSyncMode();
		jsAssets.setCallbackHandler(getAssetThumbSuceess);
		jsAssets.getAssetImg(assetID);
	}
	
	function getAssetThumbSuceess(data) {
		
		//console.log(data)

		imgObj = document.getElementById('planeImg')
		img360Obj = document.getElementById('pano360')
		
		skyboxType = document.getElementById('skyboxType')
		sb = skyboxType[skyboxType.selectedIndex].value

		if(sb == 2) {
			if(data.type == 0) { data.url = 'http://placehold.it/160x120'}
		} else if(sb == 3) {
			if(data.type == 0) { data.url = 'http://placehold.it/480x120'}
		}
		
		console.log(data)
		
		if(data.type === 1) {
			imgObj.src = data.url
		} else if(data.type == 8) {
			img360Obj.src = data.url
		} else {
			if(sb == 2) {
				imgObj.src = data.url
			} else if(sb == 3) {
				img360Obj.src = data.url
			}
		}
	}
	
	function TODOptions(tod)	{
		
		TODColor = document.getElementById('TODColor') 
		
		if(!tod) {
			TODColor.style.display = 'block'
		} else {
			TODColor.style.display = 'none'
		}
		
	}
	
	function goToSceneAsset() {
		
		sceneSettings = document.getElementById('sceneSettings') 
		sel = sceneSettings.options[sceneSettings.selectedIndex].value
		
		if(sel == 0) {
			alert("No Scene Asset To View")
		} else {
			document.location.href = "http://liveplatform.net/api/v11/AssetsView.cfm?assetTypeID=23&assetID="+sel
		}
	}
	
	function sceneAssetOptions(option) {
		
		option = parseInt(option)
		
		sceneSettings = document.getElementById('sceneSettings') 
		sceneOptions = document.getElementById('sceneOptions')
		
		if(option == 0) {
			sceneOptions.style.display = "block"
		} else {
			sceneOptions.style.display = "none"
		}
		
	}

</script>

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfinvoke component="CFC.Assets" method="getAssets" returnvariable="sceneAssetSettings">
        <cfinvokeargument name="AppID" value="#session.appID#"/>
        <cfinvokeargument name="AssetTypeID" value="23"/>
</cfinvoke>

<cfset allScenes = []>

<cfloop query="#sceneAssetSettings#">
	<cfif sceneAssetSettings.asset_id NEQ assetID>
		<cfset arrayAppend(allScenes, {"id":sceneAssetSettings.asset_id, "scene": sceneAssetSettings.assetName})>
	</cfif>
</cfloop>

<cfinvoke  component="CFC.Apps" method="getAssetPath" returnvariable="assetPath">
  <cfinvokeargument name="assetID" value="#assetID#"/>
  <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
</cfinvoke>

<cfoutput>

<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="100" align="right" valign="middle">Asset Name</td>
      <td valign="middle">
       
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="364"><input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128" /></td>
              <td width="200" align="right" class="content" style="padding-right: 5px">User Settings From</td>
              <td width="103" style="padding-right: 5px">
              	
              	<select name="sceneSettings" id="sceneSettings" style="width: 120px" onChange="sceneAssetOptions(this.value)">
              	  <option value="0">None</option>
				  <cfloop array="#allScenes#" index="sceneItem">
					  <option value="#sceneItem.id#" <cfif asset.sceneSettings IS sceneItem.id> selected</cfif>>#sceneItem.scene#</option>
              	  </cfloop>
                </select>
             
              </td>
              <td width="70" class="content" style="padding-left: 5px">
              	<a class="contentLink" onClick="goToSceneAsset()" style="cursor: pointer">view</a>
              </td>
            </tr>
      </table>
      
      </td>
    </tr>
    <tr>
      <td height="32" colspan="2" align="left" valign="middle" bgcolor="##EEEEEE" class="contentLinkGrey" style="padding-left: 5px">3D Model</td>
    </tr>
    <tr>
      <td align="right" valign="middle">Scene Model</td>
      <td>
       <select name="model3dScene" id="model3dScene">
       <option value="0">None</option>
       	<cfloop query="#all3DAssets#">
			<option value="#all3DAssets.asset_id#" <cfif asset.model_id IS all3DAssets.asset_id> selected</cfif>>#all3DAssets.name#</option>
		   </cfloop>
       </select>
      </td>
    </tr>
	</table>
    
	<div style="display:<cfif asset.sceneSettings GT 0>none<cfelse>block</cfif>" id="sceneOptions">
		<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    <tr>
      <td height="32" colspan="2" align="left" valign="middle" bgcolor="##EEEEEE" class="contentLinkGrey" style="padding-left: 5px">Enviroment</td>
    </tr>
       <tr>
      <td align="right" valign="middle">Skybox</td>
      <td>
      
      <table border="0" cellpadding="0">
		<tr>
		  <td>
		  <cfset skyboxTypes = ['None','Color', 'Image', '360VR Pano', 'Dynamic Sky']>
			<select name="skyboxType" id="skyboxType" onChange="setEnvSetting(this.options[this.selectedIndex].value)">
			   <cfset i=0>
				<cfloop index="z" array="#skyboxTypes#">
					<option value="#i#" <cfif asset.skyboxType IS i> selected</cfif> >#z#</option>
					<cfset i++>
				</cfloop>
			</select>
			</td>
			<td align="right" class="content" style="padding-top: 8px">
			
			<!--- BGColor --->
     	<cfif asset.skyboxType IS 1>
     		<cfset displaybgColor = 'block'>
     	<cfelse>
     		<cfset displaybgColor = 'none'>
		</cfif>
   
      <div style="display: #displaybgColor#" id="enviromentColor">
		<div style="float: left; text-align: right; width: 50px; padding-top: 15px; margin-right: 10px">Color</div>
		<div style="float: left">
		  <input name="bgColor" type="text" class="color formfieldcontent" id="bgColor" value="#asset.bgColor#" size="8" maxlength="8" />
		</div>
	  </div>
			
			</td>
	    </tr>
	</table>
     

      
    <cfif asset.skyboxAsset_id NEQ '' AND asset.skyboxType IS 2>
     		<cfset displayImg = 'block'>
     	<cfelse>
     		<cfset displayImg = 'none'>
		</cfif>
  	  
   	  <!--- Image --->
      <div style="margin-top: 10px; display: #displayImg#" id="enviromentImage">
		<div style="float: left">
  		
	  		<img src="http://placehold.it/160x120" id="planeImg" height="160" style="margin-right: 10px;"><p>
	  		
		  <select name="BKimg" id="BKimg" onChange="getAssetThumb(this.value)">
		    <option value="0">None</option>
		    <cfloop query="#allImages#">
				<option value="#allImages.asset_id#" <cfif allImages.asset_id IS asset.skyboxAsset_id> selected</cfif>>#allImages.name#</option>
			</cfloop>
          </select>
          
          <cfif asset.skyboxAsset_id NEQ '' AND asset.skyboxType IS 2>
          	  <script>
          		getAssetThumb('#asset.skyboxAsset_id#')
			  </script>
		  </cfif>
			
		</div>
		
		<div>
			Image Rotation
			<cfif asset.imageRotation IS ''><cfset rotation = 0><cfelse><cfset rotation = asset.imageRotation></cfif>
			<input name="imageRotation" type="text" class="formfieldcontent" id="imageRotation" style="width:60px" value="#rotation#" />
		</div>
				
	  </div>
     
     <cfif asset.skyboxAsset_id NEQ '' AND asset.skyboxType IS 3>
     		<cfset display360 = 'block'>
     	<cfelse>
     		<cfset display360 = 'none'>
		</cfif>
      
      <!--- Skybox 360vr --->
      <div style="margin-top: 10px; display: #display360#" id="enviroment360">
		<div style="float: left">
	  		<img src="http://placehold.it/480x120" id="pano360" height="160"><p>
		  <select name="img360" id="img360" onChange="getAssetThumb(this.value)">
		    <option value="0">None</option>
		    <cfloop query="#all360Scenes#">
		    	<option value="#all360Scenes.asset_id#" <cfif all360Scenes.asset_id IS asset.skyboxAsset_id> selected</cfif>>#all360Scenes.name#</option>
			</cfloop>
          </select>
          
          <cfif asset.skyboxAsset_id NEQ '' AND asset.skyboxType IS 3>
          	  <script>
          		getAssetThumb('#asset.skyboxAsset_id#')
			  </script>
		  </cfif>
		  
		</div>
	  </div>
     
      </td>
    </tr>
    <tr>
      <td width="100" align="right" valign="top" style="padding-top: 10px">Fog Effect</td>
      <td align="left" valign="top">
      <div style="margin-bottom: 60px;">

		<!--- FOG --->
    <cfif asset.useFog IS true>
		<cfset displayFog = 'block'>
	<cfelse>
		<cfset displayFog = 'none'>
	</cfif>
	
   <table width="100%" cellpadding="2" style="float: left">
    <tr>
      <td width="60" align="left" valign="middle" class="content">
      	<input name="useFog" type="checkbox" id="useFog" value="true" <cfif asset.useFog IS true> checked</cfif> onChange="fogOptions(this.checked)" />
    	<label for="useFog" style="margin-bottom: 25px; float: left;"></label>	
      </td>
      <td width="60" align="left" valign="middle" class="content">
      	<div id="fogOptions" style="display: #displayFog#; float: right;">
  	<table cellpadding="2">
    <tr>
      <td width="60" align="right" valign="middle" class="content"> Color</td>
      <td width="160"><input name="fogColor" type="text" class="color formfieldcontent" id="fogColor" value="#asset.fogColor#" size="8" maxlength="8" /></td>
      <td width="40" align="right" class="content">Density</td>
      <td><input name="density" type="text" class="formfieldcontent" id="density" style="width:60px" value="#asset.fogDensity#" /></td>
      <td width="50" align="right" class="content">Start</td>
      <td class="content">
        <input name="fogStart" type="text" class="formfieldcontent" id="fogStart" style="width:60px" value="#asset.fogStart#" />
      </td>
      <td width="50" align="right" class="content">End</td>
      <td class="content">
        <input name="fogEnd" type="text" class="formfieldcontent" id="fogEnd" style="width:60px" value="#asset.fogEnd#" />
      </td>
    </tr>
		  </table>
	</div>
      </td>
	</tr>
	</table>	
			
	<br>
	    
	    
	    
	    </div>  
     </td>
    </tr>
    <tr>
    <td align="right">
	Lighting
     </td>
    <td>
    	<table cellpadding="2">
		<tr>
		  <td width="100" align="right" class="content">Intensity Multiplier</td>
		  <td><input name="lightingIntensity" type="text" class="formfieldcontent" id="lightingIntensity" style="width:60px" value="#asset.lightingIntensity#" /></td>
		  <td width="150" align="right" class="content">Ambient Light Color</td>
		  <td><input name="ambientLightColor" type="text" class="color formfieldcontent" id="ambientLightColor" value="#asset.ambientLightColor#" size="8" maxlength="8" /></td>
		  <td width="80" align="right" class="content">Exposure</td>
		  <td><input name="exposure" type="text" class="formfieldcontent" id="exposure" value="#asset.exposure#" size="8" /></td>
		</tr>
	 </table>
    </td>
  </tr>
  <tr>
      <td height="32" colspan="2" align="left" valign="middle" bgcolor="##EEEEEE" class="contentLinkGrey" style="padding-left: 5px">Light</td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="middle"><table width="100%" border="0" cellpadding="5">
          <tr>
            <td width="100" align="right" class="content">Time of Day</td>
            <td width="30" align="right" valign="middle" class="content" style="padding-bottom: 45px;">
              <input name="useTimeOfDay" type="checkbox" id="useTimeOfDay" value="true" <cfif #asset.useTimeOfDay# IS true> checked</cfif> onChange="TODOptions(this.checked)" />
              <label for="useTimeOfDay" style="margin-left: 20px; margin-top: 15px; float: left;"></label>	  
            </td>
            <td width="220">
            
            <!--- Light Color --->
            <cfif #asset.useTimeOfDay# IS true>
            	<cfset lightColor = 'none'>
            <cfelse>
            	<cfset lightColor = 'block'>
			</cfif>
            
             <div style="display: #lightColor#" id="TODColor">
             <table border="0" cellpadding="0">
                <tr>
                  <td width="80" align="right" class="content" style="padding-right: 5px">Light Color</td>
                  <td width="160"><input name="lightColor" type="text" class="color formfieldcontent" id="lightColor" value="#asset.lightColor#" size="8" maxlength="8" /></td>
                </tr>
            </table>
			  </div>
            </td>
            <td width="140" align="right" class="content">Shadow Strength</td>
            <td width="100"><input name="shadowIntensity" type="text" class="formfieldcontent" id="shadowIntensity" style="width:60px" value="#asset.shadowIntensity#" size="10" /></td>
        </tr>
          <tr>
            <td align="right" class="content">&nbsp;</td>
            <td colspan="2" align="left" valign="middle" class="content" style="padding-bottom: 45px;"><table width="100%" border="0" cellpadding="5">
                <tr>
                  <td align="right" class="content">Intensity</td>
                  <td><input name="intensity" type="text" class="formfieldcontent" id="intensity" style="width:60px" value="#asset.intensity#" size="10" /></td>
                </tr>
                <tr>
                  <td align="right" class="content">Indirect Multiplier</td>
                  <td><input name="indirectMultiplier" type="text" class="formfieldcontent" id="indirectMultiplier" style="width:60px" value="#asset.indirectMultiplier#" size="10" /></td>
                </tr>
            </table></td>
            <td colspan="2" align="left" valign="top" class="content"><table width="100%" border="0" cellpadding="5">
              <tbody>
                <tr>
                  <td align="right" class="content">Shadow Type</td>
                  <td>
				   <cfif asset.shadowType IS ''><cfset asset.shadowType = 2></cfif>
                   <select name="shadowType" class="formfieldcontent" id="shadowType" style="width:150px">
					   <option value="0" <cfif asset.shadowType IS 0> selected</cfif>>None</option>
                    <option value="1" <cfif asset.shadowType IS 1> selected</cfif>>Hard</option>
                    <option value="2" <cfif asset.shadowType IS 2> selected</cfif>>Soft</option>
                  </select>
                  </td>
                </tr>
                <tr>
                  <td align="right" class="content">Light Rotation</td>
                  <td><table width="100%" border="0" cellpadding="5">
                      <tr class="content">
                        <td width="10" align="right">X:</td>
                        <td><input name="lightRotationX" type="text" class="formfieldcontent" id="lightRotationX" style="width:40px" value="#asset.lightRotationX#" size="10" /></td>
                        <td width="10" align="right">Y:</td>
                        <td><input name="lightRotationY" type="text" class="formfieldcontent" id="lightRotationY" style="width:40px" value="#asset.lightRotationY#" size="10" /></td>
                      </tr>
                  </table></td>
                </tr>
              </tbody>
            </table></td>
          </tr>
      </table></td>
    </tr>
   

    <tr>
      <td height="32" colspan="2" align="left" valign="middle" bgcolor="##eee"><span class="contentLinkGrey" style="padding-left: 5px">Camera</span></td>
    </tr>
    <tr>
      <td height="32" colspan="2" align="left" valign="top">
		
   <table border="0" cellpadding="5">
          <tr>
              <td width="100" align="right" class="content">&nbsp;</td>
              <td width="110" align="center" valign="middle" class="content">Minimum</td>
              <td width="150" align="center" valign="middle" class="content"><span class="content" style="padding-bottom: 10px;">Maximum</span></td>
              <td width="140" align="center" valign="middle" class="content">&nbsp;</td>
              <td width="150" align="left" valign="middle" class="content">&nbsp;</td>
          </tr>
          <tr>
            <td align="right" valign="top" class="content">Camera Zoom</td>
              <td align="center" valign="top" class="content"><input name="zoomMin" type="text" class="formfieldcontent" id="zoomMin" value="#asset.zoommin#" size="10" /></td>
            <td align="center" valign="top" class="content"><span class="content" style="padding-bottom: 10px;">
              <input name="zoomMax" type="text" class="formfieldcontent" id="zoomMax" value="#asset.zoommax#" size="10" />
            </span></td>
            <td align="right" valign="top" class="content" style="padding-bottom: 45px;">
				Camera Zoom<br>
			    <input name="zoom" type="checkbox" id="zoom" value="1" style="margin-top: 15px; float: right;" <cfif #asset.zoom# IS 1> checked</cfif> />
		    <label for="zoom" style="margin-top: 15px; float: right;"></label> 
           </td>
            <td align="left" valign="top" class="content" style="padding-bottom: 45px;"><p>Start Zoom<br>
              <span class="content" style="padding-bottom: 10px;">
              <input name="zoomStart" type="text" class="formfieldcontent" id="zoomStart" value="#asset.zoomStart#" size="10" />
              </span></p>
              
            </td>
          </tr>
          <tr align="right">
            <td colspan="5" valign="top" class="content"><hr size="1"></td>
          </tr>
          <tr align="right">
            <td valign="top" class="content" style="padding-top: 22px;">Camera Rotation</td>
            <td align="right" valign="top" class="content">

            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="50" align="right" valign="middle" class="content">Horizontal</td>
                  <td>
					<cfif asset.rotationHorizontal IS ''><cfset asset.rotationHorizontal = 0></cfif>
            		<input name="rotationHorizontal" type="checkbox" id="rotationHorizontal" value="1" <cfif asset.rotationHorizontal IS 1> checked</cfif> />
                 	<label for="rotationHorizontal" style="margin-left: 10px; margin-top: 15px; float: right; padding-bottom: 31px;"></label>  
                  </td>
                </tr>
            </table>
            
           </td>
            <td align="left" class="content">&nbsp;</td>
            <td align="right" valign="top" class="content">

            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tbody>
                <tr>
                  <td width="50" align="right" valign="middle" class="content"> Vertical</td>
                  <td>
				<cfif asset.rotationVertical IS ''><cfset asset.rotationVertical = 0></cfif>
            	<input name="rotationVertical" type="checkbox" id="rotationVertical" value="1" <cfif asset.rotationVertical IS 1> checked</cfif> />	
               <label for="rotationVertical" style="margin-left: 10px; margin-top: 15px; float: right; padding-bottom: 31px;"></label>
               </td>
                </tr>
              </tbody>
            </table>
            
           
            </td>
            <td align="left" class="content">&nbsp;</td>
          </tr>
          <tr align="right">
            <td class="content">Default Position</td>
            <td class="content">Target</td>
            <td align="left" class="content">
            	<table border="0" cellpadding="5" cellspacing="0">
					<tr>
					  <td width="10" class="content">X</td>
					  <td>
						<input name="cameraTarget_X" type="text" class="formfieldcontent" id="cameraTarget_X" value="#asset.cameraTarget_X#" size="10" />	
					  </td>
					</tr>
					<tr>
					  <td class="content">Y</td>
					  <td>
					  	<input name="cameraTarget_Y" type="text" class="formfieldcontent" id="cameraTarget_Y" value="#asset.cameraTarget_Y#" size="10" />
					  </td>
					</tr>
					<tr>
					  <td class="content">Z</td>
					  <td>
						<input name="cameraTarget_Z" type="text" class="formfieldcontent" id="cameraTarget_Z" value="#asset.cameraTarget_Z#" size="10" />
					  </td>
					</tr>
				</table>
            </td>
            <td class="content">LookAt</td>
            <td align="left" class="content">
				<table border="0" cellpadding="5" cellspacing="0">
						<tr>
						  <td width="10" class="content">X</td>
						  <td>
							<input name="cameraLookAt_X" type="text" class="formfieldcontent" id="cameraLookAt_X" value="#asset.cameraLookAt_X#" size="10" />	
						  </td>
						</tr>
						<tr>
						  <td class="content">Y</td>
						  <td>
							<input name="cameraLookAt_Y" type="text" class="formfieldcontent" id="cameraLookAt_Y" value="#asset.cameraLookAt_Y#" size="10" />
						  </td>
						</tr>
						<tr>
						  <td class="content">Z</td>
						  <td>
							<input name="cameraLookAt_Z" type="text" class="formfieldcontent" id="cameraLookAt_Z" value="#asset.cameraLookAt_Z#" size="10" />
						  </td>
						</tr>
			  </table>
            </td>
          </tr>
          <tr align="right">
            <td colspan="5" class="content"><hr size="1"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td align="center" valign="bottom" class="content">height Offset</td>
            <td align="center" valign="bottom" class="content">Rendering Path</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td align="right" class="content">Camera Position</td>
            <td align="center" valign="middle" class="content"><span class="content" style="padding-bottom: 45px;">
              <input name="heightOffset" type="text" class="formfieldcontent" id="heightOffset" value="#asset.heightOffset#" size="10" />
            </span></td>
            <td align="center" valign="middle" class="content"><span class="content" style="padding-bottom: 45px;">
		
            <cfif asset.renderingPath IS ''><cfset renderingPath = 0><cfelse><cfset renderingPath = #asset.renderingPath#></cfif>
              <select name="renderingPath" id="renderingPath" style="width: 100px">
				  <option value="0" <cfif renderingPath IS 0>selected</cfif>>Forward</option>
                <option value="1" <cfif renderingPath IS 1>selected</cfif>>Deferred</option>
              </select>
            </span></td>
            <td align="right" valign="middle" class="content" style="padding-bottom: 45px;">&nbsp;</td>
            <td align="left" valign="middle" class="content" style="padding-bottom: 45px;">&nbsp;</td>
          </tr>
            <tr align="right">
              <td colspan="5" class="content"><hr size="1"></td>
            </tr>
            <tr>
              <td align="right" class="content"> Camera Speed</td>
              <td align="center" class="content">Position</td>
              <td align="center" class="content">Look At Rotation Speed</td>
              <td colspan="2" align="center" class="content">&nbsp;</td>
            </tr>
            <tr>
              <td align="right" class="content">&nbsp;</td>
              <td align="center" class="content"><input name="positionSpeed" type="text" class="formfieldcontent" id="positionSpeed" value="#asset.positionspeed#" size="10" /></td>
              <td align="center" class="content"><input name="lookAtSpeed" type="text" class="formfieldcontent" id="lookAtSpeed" value="#asset.lookAtSpeed#" size="20" /></td>
              <td colspan="2" align="center" class="content">&nbsp;</td>
            </tr>
          <tr align="right">
              <td colspan="5" class="content"><hr size="1"></td>
        </tr>
          <tr align="right">
            <td class="content">&nbsp;</td>
            <td align="left" class="content">Glow</td>
            <td align="left" class="content">Far Clipping Plane</td>
            <td class="content">&nbsp;</td>
            <td class="content">&nbsp;</td>
          </tr>
          <tr align="right">
              <td valign="top" class="content">Other</td>
              <td align="center" class="content">
                <input name="glow" type="checkbox" id="glow" value="1" style="margin-top: 10px; float: right;" <cfif #asset.glow# IS true> checked</cfif> />
                  <label for="glow" style="margin-top: 15px; float: left; margin-bottom: 30px;"></label> 
              </td>
              <td align="left" class="content">
			  <cfif asset.farClippingPlane IS ''><cfset asset.farClippingPlane = 1000></cfif>
              <input name="farClippingPlane" type="text" class="formfieldcontent" id="farClippingPlane" value="#asset.farClippingPlane#" size="20" />
              </td>
              <td class="content">&nbsp;</td>
              <td class="content">&nbsp;</td>
        </tr>
          <tr align="right">
              <td colspan="5" class="content"><hr size="1"></td>
          </tr>
          <tr>
              <td align="right" valign="top" class="content"> Post Processing</td>
              <td colspan="4" align="left" valign="middle" class="content">
                
                <cfset displayAmbient = false>
                <cfif asset.occl_intensity GT 0 OR asset.occl_radius GT 0>
                  <cfset displayAmbient = true>
                </cfif>
                <table border="0" cellpadding="0" cellspacing="5" class="content">
                  <tr>
                    <td width="150" height="26" align="left">Ambient Occlusion</td>
                    <td width="150" colspan="-3" align="left">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="right">
					 <cfif asset.ambientOcclusion IS ''><cfset ambientOcclusion = 0><cfelse><cfset ambientOcclusion = asset.ambientOcclusion></cfif>
                    
                     <input name="ambientOccl" type="checkbox" id="ambientOccl" value="1" <cfif ambientOcclusion> checked</cfif> style="margin-top: 10px;" onChange="displayObj('ambientOptions')" />
                      <label for="ambientOccl" style="margin-top: 15px; float: left; margin-bottom: 40px;"></label></td>
                    <td colspan="-3" align="left" valign="top">
                    
                     <div id="ambientOptions" style="display: <cfif ambientOcclusion>block<cfelse>none</cfif>">
                      <table border="0" cellpadding="0" cellspacing="5" class="content">
                        <tr>
                          <td align="right">Intensity</td>
                          <td><input name="intensityOccl" type="text" class="formfieldcontent" id="intensityOccl" value="#asset.occl_intensity#" size="10" /></td>
                          <td width="50" align="right">Radius</td>
                          <td><input name="radiusOccl" type="text" class="formfieldcontent" id="radiusOccl" value="#asset.occl_radius#" size="10" /></td>
                        </tr>
                      </table>
                    </div>
                    
                    </td>
                  </tr>
                </table></td>
          </tr>
          <tr>
            <td align="right" class="content"></td>
            <td colspan="4" align="left" class="content"><hr size="1"></td>
          </tr>
          <tr>
            <td align="right" class="content"></td>
            <td colspan="4" align="left" class="content">
             <table border="0" cellpadding="0" cellspacing="5">
              <tr>
                <td width="250" height="26" class="content">Depth of Field</td>
                <td width="250" class="content">&nbsp;</td>
                <td width="250" class="content">&nbsp;</td>
              </tr>
              <tr class="content">
                <td>
                <cfif asset.depthOfField IS ''><cfset depthOfField = 0><cfelse><cfset depthOfField = asset.depthOfField></cfif>
                <input name="depthOfField" type="checkbox" id="depthOfField" value="1" <cfif depthOfField> checked</cfif> style="margin-top: 10px;"  onChange="displayObj('DepthOfFieldOptions')" />
                <label for="depthOfField" style="margin-top: 15px; float: left; margin-bottom: 30px;"></label>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <tr class="content">
                <td colspan="3">
					<div id="DepthOfFieldOptions" style="margin-top: 10px; display: <cfif depthOfField>block<cfelse>none</cfif>">
					 <table border="0" cellpadding="0" cellspacing="5">
					  <tr>
						<td width="250" class="content">Focus Distance</td>
						<td width="250" height="26" class="content">Aperture</td>
						<td width="250" class="content">Focal Length</td>
						</tr>
					  <tr>
						<td><span style="display: <cfif #trim(asset.VignetteColor)# NEQ ''> block<cfelse>none</cfif>">
						  <input name="focusDistance" type="text" class="formfieldcontent" id="focusDistance" value="#asset.focusDistance#" size="8" />
						</span></td>
						<td><input name="aperture" type="text" class="formfieldcontent" id="aperture" value="#asset.aperture#" size="8" /></td>
						<td><input name="focalLength" type="text" class="formfieldcontent" id="focalLength" value="#asset.focalLength#" size="8" /></td>
						</tr>
					</table>
					</div>
                </td>
              </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td align="right" class="content"></td>
            <td colspan="4" align="left" class="content"><hr size="1"></td>
          </tr>
          <tr>
            <td align="right" class="content"></td>
            <td colspan="4" align="left" class="content">
            	<table width="100%" border="0" cellpadding="0" cellspacing="5">
              <tr>
                <td height="26" class="content">Color Grading</td>
                </tr>
              <tr>
                <td width="150" height="26" class="content">
                 
                 <cfif asset.GradingMixerR NEQ 0 OR asset.GradingMixerG NEQ 0 OR asset.GradingMixerB NEQ 0>
                 	<cfset GradingColor = true>
                 <cfelse>
                 	<cfset GradingColor = false>
				 </cfif>
                  
                  <cfif asset.colorGrading IS ''><cfset colorGrading = 0><cfelse><cfset colorGrading = asset.colorGrading></cfif>
                  <input name="colorGrading" type="checkbox" id="colorGrading" value="true" <cfif colorGrading> checked</cfif> style="margin-top: 10px;"  onChange="displayObj('colorGradingOptions')" />
                  <label for="colorGrading" style="margin-top: 15px; float: left; margin-bottom: 30px;"></label>
                </td>
                </tr>
              <tr class="content">
                <td>
					<div id="colorGradingOptions" style="margin-top: 10px; display:<cfif colorGrading>block<cfelse>none</cfif>">
                 <table width="100%" border="0" cellpadding="0" cellspacing="5">
                  <tr>
                    <td width="100" class="content">Saturation</td>
                    <td height="26" class="content">Contrast</td>
                    <td width="50" align="center" class="content">&nbsp;</td>
                    <td width="100" align="center" class="content">Black In</td>
                    <td width="100" align="center" class="content">White In</td>
                    <td width="300" align="center" class="content">Channel Mixer</td>
                  </tr>
                  <tr>
                    <td>
                      <input name="gradingSaturation" type="text" class="formfieldcontent" id="gradingSaturation" value="#asset.gradingSaturation#" size="8" />
                    </td>
                    <td><input name="gradingContrast" type="text" class="formfieldcontent" id="gradingContrast" value="#asset.gradingContrast#" size="8" /></td>
                    <td align="center">&nbsp;</td>
                    <td align="center"><input name="blackIn" type="text" class="formfieldcontent" id="blackIn" value="#asset.blackIn#" size="8" /></td>
                    <td align="center"><input name="whiteIn" type="text" class="formfieldcontent" id="whiteIn" value="#asset.whiteIn#" size="8" /></td>
                    <td><table width="100%" border="0" cellpadding="0" cellspacing="5">
                      <tr class="content">
                        <td width="20" align="right" class="content">R</td>
                        <td><input name="gradingMixerR" type="text" class="formfieldcontent" id="gradingMixerR" value="#asset.gradingMixerR#" size="8" /></td>
                        <td width="20" align="right">G</td>
                        <td><input name="gradingMixerG" type="text" class="formfieldcontent" id="gradingMixerG" value="#asset.gradingMixerG#" size="8" /></td>
                        <td width="20" align="right">B</td>
                        <td><input name="gradingMixerB" type="text" class="formfieldcontent" id="gradingMixerB" value="#asset.gradingMixerB#" size="8" /></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                	</div>
                </td>
                </tr>
            </table>
            </td>
          </tr>
          <tr>
            <td align="right" class="content"></td>
            <td colspan="4" align="left" class="content"><hr size="1"></td>
          </tr>
          <tr>
            <td align="right"></td>
            <td colspan="4" align="left" valign="top" class="content"><table border="0" cellpadding="0" cellspacing="5" class="content">
                <tr>
                  <td width="100" height="26" class="content">Anti-Aliasing</td>
              </tr>
                <tr>
                   <td>
					   <cfif asset.antiAlias IS ''><cfset antiAlias = 0><cfelse><cfset antiAlias = asset.antiAlias></cfif>
                    <input name="antiAlias" type="checkbox" id="antiAlias" value="1" <cfif antiAlias> checked</cfif> />
                    <label for="antiAlias" style="margin-top: 15px; float: left; margin-bottom: 30px;"></label></td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td align="right" class="content"></td>
            <td colspan="4" align="left" class="content"><hr size="1"></td>
          </tr>
          <tr>
            <td align="right" class="content"></td>
            <td colspan="4" align="left" class="content"><table border="0" cellpadding="0" cellspacing="5" class="content">
              <tr>
                <td width="150" height="26" class="content">Screen Space Reflection</td>
              </tr>
              <tr>
                <td>
                <cfif asset.screenReflection IS ''><cfset screenReflection = 0><cfelse><cfset screenReflection = asset.screenReflection></cfif>
                 <input name="scrReflection" type="checkbox" id="scrReflection" value="1" <cfif screenReflection> checked</cfif> />
                  <label for="scrReflection" style="margin-top: 15px; float: left; margin-bottom: 30px;"></label>
                  </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="right" class="content"></td>
            <td colspan="4" align="left" class="content"><hr size="1"></td>
          </tr>
          <tr>
            <td align="right" class="content"></td>
            <td colspan="4" align="left" class="content"><table border="0" cellpadding="0" cellspacing="5" class="content">
              <tr>
                <td width="150" height="26" class="content">Motion Blur</td>
              </tr>
              <tr>
                <td>
                 	<cfif asset.motionBlur IS ''><cfset motionBlur = 0><cfelse><cfset motionBlur = asset.motionBlur></cfif>
                  <input name="motionBlur" type="checkbox" id="motionBlur" value="1" <cfif motionBlur> checked</cfif> />
                  <label for="motionBlur" style="margin-top: 15px; float: left; margin-bottom: 30px;"></label>
                  </td>
              </tr>
            </table></td>
          </tr>
          <tr>
            <td align="right" class="content"></td>
            <td colspan="4" align="left" class="content"><hr size="1"></td>
          </tr>
          <tr>
            <td align="right" valign="top"><span class="content" style="padding-bottom: 10px;">
              
            </span></td>
            <td colspan="4" align="left" valign="top" class="content"><table border="0" cellpadding="0" cellspacing="5">
              
                <tr>
                  <td width="150" height="26" class="content">Bloom</td>
                </tr>
                <tr>
                  <td>
                  	<cfif asset.Bloom IS ''><cfset Bloom = 0><cfelse><cfset Bloom = asset.Bloom></cfif>
                  	<input name="Bloom" type="checkbox" id="Bloom" value="1" <cfif Bloom> checked</cfif>  onChange="displayObj('bloomOptions')" />
                    <label for="Bloom" style="margin-top: 15px; float: left;margin-bottom: 30px;"></label>
                  </td>
                </tr>
                <tr>
                  <td>
                  	<div id="bloomOptions" style="margin-top: 10px; display: <cfif Bloom GT 0> block<cfelse>none</cfif>">
                  	  <table border="0" cellpadding="0" cellspacing="5">
                  	    <tr>
                <td width="250" class="content">Intensity</td>
                <td width="250" height="26" class="content">Soft Knee</td>
                <td width="250" class="content">Radius</td>
                <td width="250" class="content">Anti-Flicker</td>
              </tr>
              <tr>
                <td><input name="bloomIntensity" type="text" class="formfieldcontent" id="bloomIntensity" value="#asset.bloom_Intensity#" size="10" /></td>
                <td><input name="bloomSoftKnee" type="text" class="formfieldcontent" id="bloomSoftKnee" value="#asset.bloomSoftKnee#" size="8" /></td>
                <td><input name="bloomRadius" type="text" class="formfieldcontent" id="bloomRadius" value="#asset.bloomRadius#" size="8" /></td>
                <td>
                	<input name="bloomAntiFlicker" type="checkbox" id="bloomAntiFlicker" value="1" <cfif #trim(asset.bloomAntiFlicker)# NEQ ''> checked</cfif> />
                	<label for="bloomAntiFlicker"></label>
                </td>
              </tr>
          </table>	
				  </div>
                  </td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td align="right" class="content">&nbsp;</td>
            <td colspan="4" align="left" class="content"><hr size="1"></td>
          </tr>
          <tr>
            <td align="right" class="content"><span class="content" style="padding-bottom: 10px;">
              
            </span></td>
            <td colspan="4" align="left" class="content"><table border="0" cellpadding="0" cellspacing="5">
                <tr>
                  <td height="26" class="content">Vignette</td>
                </tr>
                <tr>
                  <td>
                    <cfif asset.vignette IS ''><cfset vignette = 0><cfelse><cfset vignette = asset.vignette></cfif>
                    <input name="vignette" type="checkbox" id="vignette" value="1" <cfif vignette> checked</cfif> onChange="displayObj('vignetteOptions');" />
                    <label for="vignette" style="margin-top: 15px; float: left;left;margin-bottom: 30px;"></label></td>
                </tr>
                <tr>
                  <td>
                  	<div id="vignetteOptions" style="display: <cfif vignette> block<cfelse>none</cfif>">
				 <table border="0" cellpadding="0" cellspacing="5">
              <tr>
                <td width="250" class="content">Color</td>
                <td width="250" height="26" class="content">Intensity</td>
                <td width="250" class="content">Smoothness</td>
                <td width="250" class="content">Roundness</td>
              </tr>
              <tr>
                <td><input name="vignetteColor" type="text" class="color formfieldcontent" id="vignetteColor" value="#asset.VignetteColor#" size="8" maxlength="8" /></td>
                <td>
                  <input name="vignetteIntensity" type="text" class="formfieldcontent" id="vignetteIntensity" value="#asset.vignetteIntensity#" size="8" />
                </td>
                <td><input name="vignetteSmoothness" type="text" class="formfieldcontent" id="vignetteSmoothness" value="#asset.vignetteSmoothness#" size="8" /></td>
                <td><input name="vignetteRoundness" type="text" class="formfieldcontent" id="vignetteRoundness" value="#asset.vignetteRoundness#" size="8" /></td>
              </tr>
            </table>	
					  </div>
                  </td>
                </tr>
            </table></td>
          </tr>
          <tr>
            <td colspan="5" bgcolor="##eeeeee" class="content"><span class="contentLinkGrey" style="padding-left: 5px">Augmented Reality</span></td>
          </tr>
          <tr>
            <td align="right" class="content">&nbsp;</td>
            <td colspan="4" align="left" class="content">
             <table border="0" cellpadding="0" cellspacing="5">
              <tr>
                <td width="104" height="26" class="content">Augmented Reality</td>
                <td width="471" class="content">&nbsp;</td>
              </tr>
              <tr>
                <td><cfif asset.vignette IS ''>
                  <cfset vignette = 0>
                  <cfelse>
                  <cfset vignette = asset.vignette>
                </cfif>
                  <input name="ar" type="checkbox" id="ar" value="1" <cfif asset.ar IS 1> checked</cfif> onChange="displayObj('arOptions');displayObj('arOptionsExtras')" />
                  <label for="ar" style="margin-top: 15px; float: left;left;margin-bottom: 30px;"></label>
                  </td>
                <td align="left">
					<div id="arOptions" style="display: <cfif asset.ar IS 1>block<cfelse>none</cfif>">
                       <table border="0" cellpadding="0" cellspacing="5">
                        <tr>
                          <td width="250"><table border="0" cellpadding="0" cellspacing="0" class="content">
                              <tr>
                                <td width="70">Default Scale</td>
                                <td><input name="ar_scale" type="text" class="formfieldcontent" id="ar_scale" value="#asset.ar_scale#" size="8" style="margin-left: 5px" /></td>
                              </tr>
                          </table>
                          </td>
                        </tr>
                      </table>
                      </div>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                 <div id="arOptionsExtras" style="display: <cfif asset.ar IS 1> block<cfelse>none</cfif>">
                  <table border="0" cellpadding="0" cellspacing="5">
                   <tr>
                     <td width="75" class="content">Scale Range</td>
                     <td width="150" class="content">&nbsp;</td>
                     <td width="50" class="content">&nbsp;</td>
                     <td width="75" class="content">Rotation</td>
                     <td width="150" class="content">&nbsp;</td>
                   </tr>
                    <tr>
                   <td class="content">
                   <input name="scaleRange" type="checkbox" id="scaleRange" value="1" <cfif asset.ar_scaleRange IS 1> checked</cfif> onChange="displayObj('arScalingRange');" />
                   <label for="scaleRange" style="margin-top: 15px; float: left;left;margin-bottom: 30px;"></label></td>
                   <td align="left" class="content">
                   	
					   <div id="arScalingRange" style="display: <cfif asset.ar_scaleRange IS 1>block<cfelse>none</cfif>">
                       <table border="0" cellpadding="0" cellspacing="5">
                        <tr>
                          <td>
                            <table border="0" cellpadding="0" cellspacing="0" class="content">
                              <tr>
                                <td align="center">MIN</td>
                                <td align="right">&nbsp;</td>
                                <td align="center">MAX</td>
                              </tr>
                              <tr>
                                <td align="center">
                                
                                	<input name="ar_Smin" type="text" class="formfieldcontent" id="ar_min" value="#asset.ar_ScaleMin#" size="8" style="margin-left: 5px" />
                                </td>
                                <td width="10" align="right">&nbsp;</td>
                                <td align="center">
                                	<input name="ar_Smax" type="text" class="formfieldcontent" id="ar_max" value="#asset.ar_ScaleMax#" size="8" style="margin-left: 5px" />
                                </td>
                                </tr>
                              </table>
                            </td>
                        </tr>
                      </table>
                      </div>
                   	
                   </td>
                   <td class="content">&nbsp;</td>
                   <td class="content">
                   <input name="ar_rotationRange" type="checkbox" id="ar_rotationRange" value="1" <cfif asset.ar_rotationRange IS 1> checked</cfif> />
                   <label for="ar_rotationRange" style="margin-top: 15px; float: left;left;margin-bottom: 30px;"></label></td>
                   <td align="left" class="content">&nbsp;</td>
                    </tr>
                  </table>
                </div>
                </td>
              </tr>
            </table>
            </td>
          </tr>
          </table>
   
   		</td>
    </tr>
    </table>
	</div>
   
</cfoutput>

<script>
	function displayObj(objRefId) {

		obj = document.getElementById(objRefId)
		objState = obj.style.display

		if(objState == 'none') {
			obj.style.display = 'block'
		} else {
			obj.style.display = 'none'
		}
	}
</script>

