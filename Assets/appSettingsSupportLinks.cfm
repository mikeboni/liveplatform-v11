<cfinvoke  component="CFC.Apps" method="getSupportLinks" returnvariable="links">
    <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>

<cfinvoke  component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />
        

<script type="text/javascript">

var jsAppOptions = new appOptionsSettings();

deleteLink = function(theForm,theID)
{
	if(confirm('Are you sure you wish to Delete this Link?'))
	{
		jsAppOptions.setSyncMode();
		jsAppOptions.setCallbackHandler(successDeletedHandler);
		jsAppOptions.deleteSupportLink(<cfoutput>#session.appID#</cfoutput>,theID);
		
	}else{ 
		//nothing
	}

}

updateURLLink = function(theForm,theID)
{
	theUrlName = theForm.urlName.value;
	theUrlLink = theForm.urlLink.value;
	
	if(!theForm.accessLevel)
	{
		theLevel = 0;
	}else{
		theLevel = parseInt(theForm.accessLevel[theForm.accessLevel.selectedIndex].value);
	}

	jsAppOptions.setSyncMode();
	jsAppOptions.setCallbackHandler(successUpdatedHandler);
	
	jsAppOptions.updateSupportLink(<cfoutput>#session.appID#</cfoutput>, theID, theUrlLink, theUrlName, theLevel);
	
	//updateAccessLock(theForm);
}

updateAccessLock = function(theForm) 
{

	theForm = document.forms[theForm];
	
	if(!document.getElementsByName(theForm).accessLevel)
	{
		window.location.reload(true);
	}else{
		
		theLevel = parseInt(theForm.accessLevel[theForm.accessLevel.selectedIndex].value);
		
		if(theLevel == 0)
		{
			lock = 'access_unlocked';  
		}else{
			lock = 'access_locked'; 
		}
		
		theForm.accessLevel.style.backgroundImage = 'url(images/'+ lock +'.png)'; 
	}
	
}

successUpdatedHandler = function(theForm) 
{
	location.reload();
}

successDeletedHandler = function(result) 
{
	location.reload(true)
}


newLinkURLLink = function(theForm)
{
	urlLink = theForm.urlLink.value;
	urlName	= theForm.urlName.value;
	
	if(!theForm.accessLevel)
	{
		theLevel = 0;
	}else{
		theLevel = parseInt(theForm.accessLevel[theForm.accessLevel.selectedIndex].value);
	}
	
	jsAppOptions.setSyncMode();
	jsAppOptions.setCallbackHandler(newLinkURLHandler);
	jsAppOptions.newSupportLink(<cfoutput>#session.appID#</cfoutput>, urlLink, urlName,0);
}

newLinkURLHandler = function(success)
{
	location.reload();
}

</script>




<!---Support Links--->
<cfoutput>

<table width="100%" border="0" cellspacing="10">
  <tr class="content">
    <td width="200">Link Name</td>
    <td>URL</td>
    <td width="44" align="right">&nbsp;</td>
    <td width="44" align="right">&nbsp;</td>
    </tr>
  <cfset z = 0>
  <cfloop query="links">
    <form id="updateLink_#z#" name="updateLink">
      <tr>
        <td align="right">
          <input name="urlName" type="text" class="formfieldcontent" id="urlName" value="#urlName#" size="40" maxlength="50" onchange="updateURLLink(this.form,#support_id#)" />
          </td>
        <td>
          <input name="urlLink" type="text" class="formfieldcontent" style="width:380px" id="urlLink" value="#url#" maxlength="255" onchange="updateURLLink(this.form,#support_id#)" />
          </td>
        <td align="right" class="content">
        <cfif accessLevel IS 0>
        	<cfset lock = 'access_unlocked'>
        <cfelse>
        	<cfset lock = 'access_locked'>
        </cfif>
          <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateURLLink(this.form,#links.support_id#)" />
          <cfloop query="accessLevels">
            <option value="#accessLevel#" style="color:##333" <cfif accessLevel IS links.accessLevel>selected="selected"</cfif>>#levelName#</option>
            </cfloop>
          </select>
          </td>
        <td align="right" class="content">
          
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>
                <input type="button" value="" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onclick="deleteLink(this.form,#links.support_id#);" />
                </td>
              </tr>
            </table>
          
          </td>
        </tr>
      </form>
    <cfset z++>
    </cfloop>
  
  <form id="newLink" name="newLink">
    <tr>
      <td>
        <input name="urlName" type="text" class="formfieldcontent" id="urlName" value="" size="40" maxlength="50" />
        </td>
      <td>
        <input name="urlLink" type="text" class="formfieldcontent" style="width:380px" id="urlLink" maxlength="255" /></td>
      <td align="right">&nbsp;</td>
      <td align="right"><span class="content">
        <input type="button" value="" style="background:url(images/add.png) no-repeat; border:0; width:44px; height:44px;" onclick="newLinkURLLink(this.form)" />
        </td>
      </tr>
    </form>
  
</table>

</cfoutput>