<link href="../styles.css" rel="stylesheet" type="text/css">

<script type="text/javascript">
	
	function setAlignment(theForm)	{
		
		alignH =document.getElementById('alignH');
		alignV = document.getElementById('alignV');
		
		var h = alignH.options[alignH.selectedIndex].value;
		var v = alignV.options[alignV.selectedIndex].value;

		displayAlignment(+h,+v);
		
	}
	
	function displayAlignment(alignH,alignV)	{
		
		TL = document.getElementById('TL');
		TC = document.getElementById('TC');
		TR = document.getElementById('TR');
		
		ML = document.getElementById('ML');
		MC = document.getElementById('MC');
		MR = document.getElementById('MR');
		
		BL = document.getElementById('BL');
		BC = document.getElementById('BC');
		BR = document.getElementById('BR');
		
		var allRegPoints = [TL,TC,TR,ML,MC,MR,BL,BC,BR];
		
		for(i=0; i<allRegPoints.length; i++){
			allRegPoints[i].src = "images/reg-off.png";
		}
		
		var H, V;
		
		//horizontal
		switch(alignH)	{
			case 0://center
			H = "C";
			break;
			
			case -1://left
			H = "L";
			break;
			
			case 1://right
			H = "R";
			break;
		}
		
		//vertical
		switch(alignV)	{
			case 0://middle
			V = "M";
			break;
			
			case -1://top
			V = "T";
			break;
			
			case 1://bottom
			V = "B";
			break;
		}
		console.log(H+V);
		theObj = document.getElementById(V+H);
		theObj.src = "images/reg-on.png";
	}
	
	function setClickAlignment(h,v)	{
		
		alignH =document.getElementById('alignH');
		alignV = document.getElementById('alignV');
		
		IndexH = getAngle(h);
		IndexV = getAngle(v);
		
		alignH.selectedIndex = IndexH;
		alignV.selectedIndex = IndexV;
		
		displayAlignment(h,v);
	}
	
	function getAngle(theAngle)	{
	
		switch(theAngle)	{
		
		case 0: return 1;
		break;
		
		case 1:return 2;
		break;
		
		case -1:return 0;
		break;	
			
		}
		
	}
	
	function setPointDisplay(type) {
		
		var type = parseInt(type)
		
		var objR = document.getElementById('objReference')
		objR.style.display = 'none'
		var objXY = document.getElementById('standardPoint')
		objXY.style.display = 'none'
		var objZ = document.getElementById('ZCoords')
		objZ.style.display = 'none'
			
		switch(type) {
			//2d coords
			case 0:
			var objXY = document.getElementById('standardPoint')
			objXY.style.display = 'block'	
			break
			
			//3d corrds
			case 1:
			var objXY = document.getElementById('standardPoint')
			objXY.style.display = 'block'
			var objZ = document.getElementById('ZCoords')
			objZ.style.display = 'block'
			var objRef = document.getElementById('objectRef')
			objRef.style.display = 'block'
			break
			
			//obj ref
			case 2:
			var objR = document.getElementById('objReference')
			objR.style.display = 'block'
			break
		}
		
	}
	
</script>



<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfif asset.objectRef NEQ ''>
	<cfset pointType = 2>
<cfelse>	
	<cfif asset.z_pos NEQ '' AND asset.z_pos NEQ 0>
		<cfset pointType = 1>
	<cfelse>
		<cfset pointType = 0>
	</cfif>
</cfif>
			
<cfif asset.regPointH IS ''><cfset asset.regPointH = 0></cfif>
<cfif asset.regPointV IS ''><cfset asset.regPointV = 0></cfif>

<cfif asset.scaleToMap IS ''>
	<cfset scaleToMap = false>
<cfelse>
    <cfset scaleToMap = asset.scaleToMap>
</cfif>

<cfif asset.absolutePos IS ''>
	<cfset absolutePos = false>
<cfelse>
    <cfset absolutePos = asset.absolutePos>
</cfif>

<cfif asset.hideBehindGeometry IS '' OR asset.hideBehindGeometry IS 0>
	<cfset hiddenBehindGeometry = false>
<cfelse>
    <cfset hiddenBehindGeometry = true>
</cfif>

<cfif asset.Disabled IS '' OR asset.Disabled IS 0>
	<cfset Disabled = false>
<cfelse>
    <cfset Disabled = true>
</cfif>


<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Location Name</td>
      <td>
        <p>
          <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128">
      </p></td>
      <td>
				
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				  <td align="right" class="content" style="padding-right: 10px; padding-top: 10px;width: 160px;">Disabled</td>
				  <td>
					  <input name="Disabled" type="checkbox" id="Disabled" value="1" <cfif Disabled IS 1>checked="checked"</cfif> />
					  <label for="Disabled"></label>
				  </td>
				</tr>
			</table>
			  
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">Type</td>
      <td>
		<select name="PointType" id="PointType" onChange="setPointDisplay(this.value)">
			<option value="0" <cfif pointType IS 0> selected</cfif>>2D Point (X-Y)</option>
			<option value="1" <cfif pointType IS 1> selected</cfif>>3D Point (X-Y-Z)</option>
			<option value="2" <cfif pointType IS 2> selected</cfif>>3D Reference (REF)</option>
		  </select>
      </td>
      <td align="center"></td>
    </tr>
    <tr>
      <td align="right" valign="top" style="padding-top: 18px">XYZ Position </td>
      <td valign="top">
      
		  <div id="objReference" style="display: <cfif pointType IS 2>block<cfelse>none</cfif>;">
      	<table border="0" cellpadding="5">
      	<tr>
			<td width="80" align="right" class="content">Obj Ref</td>
     		<td>
      		<input name="objectRef" type="text" class="formfieldcontent" id="objectRef" value="#asset.objectRef#" size="60" maxlength="128" /></td>
			</tr>
      	<tr>
      	  <td align="right" class="content">&nbsp;</td>
      	  <td class="content">Get 3D Object XYZ position
      	    using the object name. This will then use XYZ os an offset to this XYZ Position </td>
      	  </tr>
      	<tr>
      	  <td align="right" class="content">Camera Behaviour</td>
      	  <td class="content">
     	    <select name="cameraBehaviour" id="cameraBehaviour" class="formfieldcontent" style="margin-left:5px">
      	    <option value="0" <cfif asset.cameraBehaviour IS 0> selected</cfif>>None</option>
      	    <option value="1" <cfif asset.cameraBehaviour IS 1> selected</cfif>>Bird Eye</option>
      	    <option value="2" <cfif asset.cameraBehaviour IS 2> selected</cfif>>First Person</option>
      	    <option value="3" <cfif asset.cameraBehaviour IS 3> selected</cfif>>Walker</option>
    	    </select></td>
    	  </tr>
      	<tr>
      	  <td align="right" class="content">
		<input name="hideBehindGeometry" type="checkbox" id="hideBehindGeometry" value="1" <cfif hiddenBehindGeometry IS 1>checked="checked"</cfif> />
			  <label for="hideBehindGeometry"></label>	
     	  </td>
      	  <td class="content">	
			<span style="padding-top: 4px">Hide Behind Geometry</span>
   	     </td>
    	  </tr>
		  </table>
		  </div>
      <!--- Standard 2D 3D Points --->
       <div id="standardPoint" style="display: <cfif pointType IS 0 OR pointType IS 1>block<cfelse>none</cfif>">
        <table width="100%" border="0" cellspacing="5">
          <tr>
            <td width="40" align="right" class="content">X Pos</td>
            <td width="40" align="right" class="content"><img src="images/gps_latt.png" width="44" height="44" /><br /></td>
            <td width="40"><label for="x_pos"></label>
              <input name="x_pos" type="text" class="formfieldcontent" id="x_pos" value="#asset.x_pos#" size="10" maxlength="10" /></td>
            <td width="40" align="right" class="content">Y Pos</td>
            <td width="40" align="right"><p class="content"><img src="images/gps_long.png" alt="" width="44" height="44" /><br />
            </p></td>
            <td><input name="y_pos" type="text" class="formfieldcontent" id="y_pos" value="#asset.y_pos#" size="10" maxlength="10" /></td>
          </tr>
          <tr>
            <td colspan="6">
            <!--- Z Coords --->
            <div id="ZCoords" style="display: <cfif pointType IS 1>block<cfelse>none</cfif>">
            <table border="0" width="100%">
				<tr>
					<td align="right" class="content">Z Pos</td>
					<td align="right" class="content"><img src="images/z_pos.png" alt="" width="44" height="44" /></td>
					<td><input name="z_pos" type="text" class="formfieldcontent" id="z_pos" value="#asset.z_pos#" size="10" maxlength="10" /></td>
					<td colspan="3" align="left" class="content"></td>
				</tr>
		   </table>
				</div>
				
			  </td>
          </tr>
          <tr>
            <!--- <td height="40" colspan="3" align="right" valign="middle" class="content" style="padding-top:6px">Absolute Positioning</td>
            <td colspan="3" align="left" valign="middle" class="content">
              <span style="padding-bottom:12px">
              <input name="absolutePos" type="checkbox" id="absolutePos" value="1" <cfif absolutePos IS 1>checked="checked"</cfif> />
              <label for="absolutePos"></label>
              </span>
            </td> TO DELETE--->
          </tr>
        </table>
		</div>
       
        </td>
      <td width="200" align="center">
      <div style="width:200px; height:200px; border: 2px solid ##666666">
      <table width="200" height="200" border="0" cellspacing="0" cellpadding="10">
          <tr>
            <td valign="top"><img src="images/reg-off.png" id="TL" onclick="setClickAlignment(-1,-1)" style="cursor:pointer"></td>
            <td align="center" valign="top"><img src="images/reg-off.png" id="TC" onclick="setClickAlignment(0,-1)" style="cursor:pointer"></td>
            <td align="right" valign="top"><img src="images/reg-off.png" id="TR" onclick="setClickAlignment(1,-1)" style="cursor:pointer"></td>
          </tr>
          <tr>
            <td valign="middle"><img src="images/reg-off.png" id="ML" onclick="setClickAlignment(-1,0)" style="cursor:pointer"></td>
            <td align="center" valign="middle"><img src="images/reg-off.png" id="MC" onclick="setClickAlignment(0,0)" style="cursor:pointer"></td>
            <td align="right" valign="middle"><img src="images/reg-off.png" id="MR" onclick="setClickAlignment(1,0)" style="cursor:pointer"></td>
          </tr>
          <tr>
            <td valign="bottom"><img src="images/reg-off.png" id="BL" onclick="setClickAlignment(-1,1)" style="cursor:pointer"></td>
            <td align="center" valign="bottom"><img src="images/reg-off.png" id="BC" onclick="setClickAlignment(0,1)" style="cursor:pointer"></td>
            <td align="right" valign="bottom"><img src="images/reg-off.png" id="BR" onclick="setClickAlignment(1,1)" style="cursor:pointer"></td>
          </tr>
        </table>
      </div>
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">Reg Point</td>
      <td valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr class="content">
          <td width="60">
            <select name="alignH" id="alignH" class="formfieldcontent" style="margin-left:5px; width:60px" onchange="setAlignment(this)">
              <option value="-1" <cfif asset.regPointH IS -1> selected</cfif>>Left</option>
              <option value="0" <cfif asset.regPointH IS 0> selected</cfif>>Center</option>
              <option value="1" <cfif asset.regPointH IS 1> selected</cfif>>Right</option>
            </select>
          </td>
          <td width="60">
            <select name="alignV" id="alignV" class="formfieldcontent" style="margin-left:5px;width:60px" onchange="setAlignment(this)">
              <option value="-1" <cfif asset.regPointV IS -1> selected</cfif>>Top</option>
              <option value="0" <cfif asset.regPointV IS 0> selected</cfif>>Middle</option>
              <option value="1" <cfif asset.regPointV IS 1> selected</cfif>>Bottom</option>
            </select> 
          </td>
          <td width="100" align="right"> Expand Touch</td>
          <td><input name="touchArea" type="text" class="formfieldcontent" id="touchArea" style="margin-left:5px" value="#asset.touchRegion#" size="10" maxlength="3" /></td>
          <td width="50" align="right" style="padding-right:5px">Scale</td>
          <td style="padding-bottom:12px">
            <input name="scaleToMap" type="checkbox" id="scaleToMap" value="1" <cfif scaleToMap IS 1>checked="checked"</cfif> />
          <label for="scaleToMap"></label></td>
        </tr>
      </table></td>
      <td align="center" valign="middle">Alignment</td>
    </tr>
    <tr>
      <td colspan="3" align="right" valign="middle"><hr size="1" /></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Object Ref</td>
      <td valign="middle">
        
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="300" valign="middle">
            
            </td>
          </tr>
      </table></td>
      <td valign="middle">&nbsp;</td>
    </tr>
 </table>
<p class="content">&nbsp;</p>

<script>
	displayAlignment(#asset.regPointH#,#asset.regPointV#);
</script>

</cfoutput>