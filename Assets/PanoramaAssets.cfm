<link href="../styles.css" rel="stylesheet" type="text/css">

<style>

.leftImage {
	border:thick; 
	color:rgba(36,234,127,1.00); 
	border-style:solid
}

.rightImage {
	border:thick; 
	color:rgba(36,234,127,1.00); 
	border-style:solid
}

.frontImage {
	border:thick; 
	color:rgba(36,234,127,1.00); 
	border-style:solid
}

.backImage {
	border:thick; 
	color:rgba(36,234,127,1.00); 
	border-style:solid
}
</style>


<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="queryAsset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="assetStruct">
    <cfinvokeargument name="query" value="#queryAsset#"/>
</cfinvoke> 

<cfinvoke  component="CFC.Apps" method="getAssetPath" returnvariable="assetPath">
  <cfinvokeargument name="assetID" value="#assetID#"/>
  <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
</cfinvoke>

<cfif isArray(assetStruct)>
	<cfset asset = structNew()>
<cfelse>
	<cfset asset = assetStruct>
</cfif>

<cfset panoViews = ['back','down','front','left','right','up']>
<cfset viewNames = ['theImageB','theImageD','theImageF','theImageL','theImageR','theImageU']>
<cfset Views = {}>

<cfif structIsEmpty(asset)>
    <cfloop index="view" from="1" to="6">
    	<cfset structAppend(Views,{'#viewNames[view]#' : ''})>
    </cfloop>
<cfelse>
	<cfloop index="view" from="1" to="6">
        <cfif asset[panoViews[view]] NEQ ''>
            <cfset structAppend(Views,{'#viewNames[view]#' : '#assetPath##asset[panoViews[view]]#'})>
        <cfelse>
            <cfset structAppend(Views,{'#viewNames[view]#' : ''})>
        </cfif>
    </cfloop>
</cfif>

<!--- VR --->
<cfset panoVRViews = ['back_vr','down_vr','front_vr','left_vr','right_vr','up_vr']>
<cfset viewNames = ['theImageBVR','theImageDVR','theImageFVR','theImageLVR','theImageRVR','theImageUVR']>
<cfset VRViews = {}>

<cfif structIsEmpty(asset)>
    <cfloop index="view" from="1" to="6">
    	<cfset structAppend(VRViews,{'#viewNames[view]#' : ''})>
    </cfloop>
 <cfelse>
	<cfloop index="view" from="1" to="6">
        <cfif asset[panoVRViews[view]] NEQ ''>
            <cfset structAppend(VRViews,{'#viewNames[view]#' : '#assetPath##asset[panoVRViews[view]]#'})>
        <cfelse>
            <cfset structAppend(VRViews,{'#viewNames[view]#' : ''})>
        </cfif>
    </cfloop>
</cfif>

<cfset theImagePano = ''>

<cfif isDefined('asset.pano')>
	<cfif asset.pano NEQ ''>
	<cfset theImagePano = '#assetPath##asset.pano#'>
    </cfif>
</cfif>

<cfset vr = 0>
    <cfif VRViews.theImageLVR NEQ '' OR VRViews.theImageRVR NEQ '' OR VRViews.theImageUVR NEQ '' OR VRViews.theImageFVR NEQ '' OR VRViews.theImageBVR NEQ '' OR VRViews.theImageDVR NEQ ''><cfset vr = 1></cfif>

<cfoutput>

<cfif queryAsset.viewRange IS ''>
	<cfset queryAsset.viewRange = 0>
</cfif>    

<cfif queryAsset.angle IS ''>
	<cfset queryAsset.angle = 0>
</cfif> 

<script>
function dipslayVROption(state = 0)	{
	
	if(state)	{
		document.getElementById("vrheadset").style.display = "block";
		document.getElementById("vrheadset-help").style.display = "block";
	}else{
		document.getElementById("vrheadset").style.display = "none";
		document.getElementById("vrheadset-help").style.display = "none";
	}
}

function findAngle(angle) {
	var myAngle = parseInt(angle);
    var defaultAngles = [-1, 0, 90, 180, 270];
	var foundAngle;
	defaultAngles.forEach(function(a,idx) {
		if (a === myAngle) foundAngle = idx
	})
	return foundAngle
}

function changeAngle(angle)	{
		
		var theAngleSel = document.getElementById("startViewAt");
		var theAngleCalc = document.getElementById("angle");
		
		if (angle) {
			var foundAngle = findAngle(angle);
			if (foundAngle) {
				theAngleSel.selectedIndex = foundAngle;
			} else {
				theAngleSel.selectedIndex = 0;
			}
			theAngleCalc.value = angle;
		} else {
			var theAngle = parseInt(theAngleSel.options[theAngleSel.selectedIndex].value);
			theAngleCalc.value = theAngle;
		}
}

function lookupAngle(view)	{
	
	var directions;
	
	if (view) {
		directions = { "front":0, "left":90, "back":180, "right":270 };
		return directions[view];
	}
}

</script>

<table border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td><label for="assetName"></label>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <cfif theImagePano NEQ ''>
    <tr>
      <td align="right" valign="middle">Image</td>
      <td valign="middle">
            <a href="#theImagePano#" target="_new" class="contentLink">
          	<img src="#theImagePano#" alt="#theImagePano#" width="228" height="167" border="1" class="imgLockedAspect" />
            </a> 
      </td>
    </tr>
    </cfif>
    <tr>
      <td align="right" valign="middle">360 Pano</td>
      <td valign="middle"><input name="pano360Asset" type="file" class="formfieldcontent" id="pano360Asset" style="padding-left:0; width:500px" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
    </tr>
    <tr>
    <td align="right">POV</td>
    <td>
    
    <table width="100%%" border="0" cellpadding="0" cellspacing="0" class="content">
  <tbody>
    <tr>
      <td width="200">
      <select id="startViewAt" name="startViewAt" class="formText" style="width:80px" onChange="changeAngle()">
      <option>Custom</option>
      <option value="0" selected="selected">Front</option>
      <option value="90">Left</option>
      <option value="180">Back</option>
      <option value="270">Right</option>
    </select>
    <input name="angle" type="text" class="formText" id="angle" style="margin-left:5px" value="0" size="3" maxlength="3" onChange="changeAngle(this.value)">
    Degrees
      </td>
      <td>
         View Range
        <select id="viewRange" name="viewRange" class="formText" style="width:80px">
          <option value="0" <cfif queryAsset.viewRange IS 0>selected</cfif>>360</option>
          <option value="1" <cfif queryAsset.viewRange IS 1>selected</cfif>>180</option>
        </select>
      </td>
      <td width="130" align="right" style="padding-top: 8px;">Flip Left/Right Views</td>
      <td>
		<cfif queryAsset.flipLR IS ''><cfset flipLRState = 0><cfelse><cfset flipLRState = queryAsset.flipLR></cfif>
      	<input name="flipLR" type="checkbox" id="flipLR" value="1" <cfif flipLRState IS 1>checked</cfif> />
        <label for="flipLR" style="margin-left: 5px;"></label>
      </td>
    </tr>
  </tbody>
</table>
    
    </td>
    </tr>
    <tr>
      <td height="32" colspan="2" align="left" valign="middle" bgcolor="##eee" style="padding-left:5px">
      <cfif vr IS 1>
      	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="44"><img src="images/l-cardboard.png" width="37" height="24" /></td>
            <td class="content">VR 360 Scene Files</td>
          </tr>
        </table>
      <cfelse>
        VR 360 Scene Files
      </cfif>
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">FrontView</td>
      <td>
      <Cfif queryAsset.frontView IS ''>
	  	<cfset df = 0>
      <cfelse>
	  	<cfset df = queryAsset.frontView>
      </Cfif>
      <select name="defaultView" id="defaultView">
        <option value="0" <cfif df IS 0> selected</cfif>>Front (Default)</option>
        <option value="1" <cfif df IS 1> selected</cfif>>Left</option>
        <option value="2" <cfif df IS 2> selected</cfif>>Right</option>
        <option value="3" <cfif df IS 3> selected</cfif>>Back</option>
      
      </select> 
      Change Front View to another side
      - default is front</td>
  </tr>
    <tr>
      <td width="80" align="right" valign="top">Front = 0<br />
Left = 1<br />
Back = 2<br />
Right = 3<br />
Up = 4<br />
Down= 5</td>
      <td>

        <table border="0">
          <tr class="contentHilighted">
            <td align="center">Left Image <cfif vr IS 1>(LEFT EYE)</cfif></td>
            <td align="center">Right Image <cfif vr IS 1>(LEFT EYE)</cfif></td>
            <td align="center">Up Image <cfif vr IS 1>(LEFT EYE)</cfif></td>
          </tr>
          <tr>
            <td align="center">
            <cfif fileExists(Views.theImageL)>
            <a href="#Views.theImageL#" target="_new" class="contentLink">
          	<img src="#Views.theImageL#" width="228" height="167" border="1" class="imgLockedAspect <cfif df IS 1>leftImage</cfif>" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(Views.theImageR)>
            <a href="#Views.theImageR#" target="_new" class="contentLink">
          	<img src="#Views.theImageR#" width="228" height="167" border="1" class="imgLockedAspect <cfif df IS 2>rightImage</cfif>" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(Views.theImageU)>
            <a href="#Views.theImageU#" target="_new" class="contentLink">
          	<img src="#Views.theImageU#" width="228" height="167" border="1" class="imgLockedAspect" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
          </tr>
          <tr>
            <td align="center" class="contentHilighted">#listLast(Views.theImageL,'/')#</td>
            <td align="center" class="contentHilighted">#listLast(Views.theImageR,'/')#</td>
            <td align="center" class="contentHilighted">#listLast(Views.theImageU,'/')#</td>
          </tr>
          <tr>
            <td><input name="panoLAsset" type="file" class="formfieldcontent" id="panoLAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoRAsset" type="file" class="formfieldcontent" id="panoRAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoUAsset" type="file" class="formfieldcontent" id="panoUAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
          </tr>
          <tr>
            <td height="10" colspan="3">&nbsp;</td>
          </tr>
          <tr class="contentHilighted">
            <td align="center">Front Image <cfif vr IS 1>(LEFT EYE)</cfif></td>
            <td align="center">Back Image <cfif vr IS 1>(LEFT EYE)</cfif></td>
            <td align="center">Down Image <cfif vr IS 1>(LEFT EYE)</cfif></td>
          </tr>
          <tr>
            <td align="center">
            <cfif fileExists(Views.theImageF)>
            <a href="#Views.theImageF#" target="_new" class="contentLink">
          	<img src="#Views.theImageF#" width="228" height="167" border="1" class="imgLockedAspect <cfif df IS 0>frontImage</cfif>" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(Views.theImageB)>
            <a href="#Views.theImageB#" target="_new" class="contentLink">
          	<img src="#Views.theImageB#" width="228" height="167" border="1" class="imgLockedAspect <cfif df IS 3>backImage</cfif>" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(Views.theImageD)>
            <a href="#Views.theImageD#" target="_new" class="contentLink">
          	<img src="#Views.theImageD#" width="228" height="167" border="1" class="imgLockedAspect" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
          </tr>
          <tr>
            <td align="center" class="contentHilighted">#listLast(Views.theImageF,'/')#</td>
            <td align="center" class="contentHilighted">#listLast(Views.theImageB,'/')#</td>
            <td align="center" class="contentHilighted">#listLast(Views.theImageD,'/')#</td>
          </tr>
          <tr>
            <td><input name="panoFAsset" type="file" class="formfieldcontent" id="panoFAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoBAsset" type="file" class="formfieldcontent" id="panoBAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoDAsset" type="file" class="formfieldcontent" id="panoDAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
          </tr>
        </table>

      </td>
    </tr>
    <tr>
    <cfif vr IS 1>
    	<cfset displayVR = 'block'>
    <cfelse>
    	<cfset displayVR = 'none'>
    </cfif>
      <td height="38" colspan="2" align="left" valign="middle" bgcolor="##eee" style="padding-bottom:10px; padding-left:5px">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
          <td width="38" valign="middle" style="padding-top:6px; padding-right:5px">
          <cfif vr IS 1>
          	<img src="images/r-cardboard.png" width="37" height="24" />
          <cfelse>
          	<img src="images/cardboard.png" width="37" height="24" />
          </cfif>
          </td>
          <td width="80"><input name="VR" type="checkbox" id="VR" onclick="dipslayVROption(this.checked)" value="1" <cfif vr IS 1>checked="CHECKED"</cfif> />
        <label for="VR"></label></td>
          
          <td class="content" style="padding-top:8px">VR Headset - Right Eye -VR Scene Files</td>
        </tr>
    </table></td>
    </tr>
    <tr>  
    <td width="80" align="right" valign="top">
        <div id="vrheadset-help" style="display:#displayVR#">
        Front = 0<br />
        Left = 1<br />
        Back = 2<br />
        Right = 3<br />
        Up = 4<br />
        Down= 5
        </div>
</td>
      <td>
      	<div id="vrheadset" style="display:#displayVR#">
      	<table border="0">
          <tr class="contentHilighted">
            <td align="center">Left Image (RIGHT EYE)</td>
            <td align="center">Right Image (RIGHT EYE)</td>
            <td align="center">Up Image (RIGHT EYE)</td>
          </tr>
          <tr>
            <td align="center">
            <cfif fileExists(VRViews.theImageLVR)>
            <a href="#VRViews.theImageLVR#" target="_new" class="contentLink">
          	<img src="#VRViews.theImageLVR#" width="228" height="167" border="1" class="imgLockedAspect <cfif df IS 1>leftImage</cfif>" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(VRViews.theImageRVR)>
            <a href="#VRViews.theImageRVR#" target="_new" class="contentLink">
          	<img src="#VRViews.theImageRVR#" width="228" height="167" border="1" class="imgLockedAspect <cfif df IS 2>rightImage</cfif>" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(VRViews.theImageUVR)>
            <a href="#VRViews.theImageUVR#" target="_new" class="contentLink">
          	<img src="#VRViews.theImageUVR#" width="228" height="167" border="1" class="imgLockedAspect" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
          </tr>
          <tr>
            <td align="center" class="contentHilighted">#listLast(VRViews.theImageLVR,'/')#</td>
            <td align="center" class="contentHilighted">#listLast(VRViews.theImageRVR,'/')#</td>
            <td align="center" class="contentHilighted">#listLast(VRViews.theImageUVR,'/')#</td>
          </tr>
          <tr>
            <td><input name="panoLAssetVR" type="file" class="formfieldcontent" id="panoLAssetVR" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoRAssetVR" type="file" class="formfieldcontent" id="panoRAssetVR" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoUAssetVR" type="file" class="formfieldcontent" id="panoUAssetVR" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
          </tr>
          <tr>
            <td height="10" colspan="3">&nbsp;</td>
          </tr>
          <tr class="contentHilighted">
            <td align="center">Front Image (RIGHT EYE)</td>
            <td align="center">Back Image (RIGHT EYE)</td>
            <td align="center">Down Image (RIGHT EYE)</td>
          </tr>
          <tr>
            <td align="center">
            <cfif fileExists(VRViews.theImageFVR)>
            <a href="#VRViews.theImageFVR#" target="_new" class="contentLink">
          	<img src="#VRViews.theImageFVR#" width="228" height="167" border="1" class="imgLockedAspect <cfif df IS 0>frontImage</cfif>" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(VRViews.theImageBVR)>
            <a href="#VRViews.theImageBVR#" target="_new" class="contentLink">
          	<img src="#VRViews.theImageBVR#" width="228" height="167" border="1" class="imgLockedAspect <cfif df IS 3>backImage</cfif>" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(VRViews.theImageDVR)>
            <a href="#VRViews.theImageDVR#" target="_new" class="contentLink">
          	<img src="#VRViews.theImageDVR#" width="228" height="167" border="1" class="imgLockedAspect" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
          </tr>
          <tr>
            <td align="center" class="contentHilighted">#listLast(VRViews.theImageFVR,'/')#</td>
            <td align="center" class="contentHilighted">#listLast(VRViews.theImageBVR,'/')#</td>
            <td align="center" class="contentHilighted">#listLast(VRViews.theImageDVR,'/')#</td>
          </tr>
          <tr>
            <td><input name="panoFAssetVR" type="file" class="formfieldcontent" id="panoFAssetVR" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoBAssetVR" type="file" class="formfieldcontent" id="panoBAssetVR" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="panoDAssetVR" type="file" class="formfieldcontent" id="panoDAssetVR" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
          </tr>
        </table>
        </div>
      </td>
    </tr>
 </table>
</cfoutput>
<script language="javascript">
<cfoutput>changeAngle(#queryAsset.angle#)</cfoutput>
</script>