<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<script type="text/javascript" src="Assets/instanceName.js"></script>

<cfif asset.offset_x IS "">
	<cfset offset_x = 0>
<cfelse>
	<cfset offset_x = asset.offset_x>
</cfif>

<cfif asset.offset_y IS "">
	<cfset offset_y = 0>
<cfelse>
	<cfset offset_y = asset.offset_y>
</cfif>

<cfinvoke  component="CFC.Apps" method="getAssetPath" returnvariable="assetPath">
  <cfinvokeargument name="assetID" value="#assetID#"/>
  <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
</cfinvoke>

<cfset theImage = '#assetPath##asset.url#'>
<cfset theImageN = '#assetPath#nonretina/#asset.url#'>

<cfif assets.dynamic IS ''>
	<cfset dynamic = 0>
<cfelse>
	<cfset dynamic = assets.dynamic>
</cfif>

<cfif asset.useScaling IS ''>
	<cfset scaling = 0>
<cfelse>
	<cfset scaling = asset.useScaling>
</cfif>

<cfoutput>
<table width="810" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td>
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="content">
        <tr>
          <td>
          <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128">
          </td>
          <td align="right" style="padding-bottom:10px">
          <input name="useDynamic" type="checkbox" id="useDynamic" value="1" <cfif dynamic IS 1> checked</cfif> />
          <label for="useDynamic" style="padding-top:5px">Dynamic Content</label></td>
        </tr>
      </table></td>
    </tr>
    <cfif asset.url NEQ '' OR asset.webURL NEQ ''>
    <tr>
      <td align="right" valign="top">Resolution</td>
      <td> Width: #asset.width#, Height: #asset.height# </td>
    </tr>
    <tr>
      <td width="80" align="right" valign="top">&nbsp;</td>
      <td>
      	  <cfif asset.webURL NEQ ''>
          
          <a href="#asset.webURL#" target="_new" class="contentLink">
                <img src="#asset.webURL#" alt="#name#" width="298" height="221" border="1" class="imgLockedAspect" />
                </a>
          
          <cfelse>
             
            <cfif NOT fileExists(theImage)>
              HIRES MISSING
            </cfif>
              
            <cfif fileExists(theImage)>
                <a href="#theImage#" target="_new" class="contentLink">
                <img src="#theImage#" alt="#theImage#" width="298" height="221" border="1" class="imgLockedAspect" />
                </a>
              <cfelse>
                <cfinclude template="noFile.cfm">
              </cfif>
          
          </cfif>
      	  

      </td>
    </tr>
    </cfif>
    <cfif asset.url NEQ ''>
    <tr>
      <td align="right" valign="middle">Image</td>
      <td valign="middle" class="contentHilighted">
      #asset.url#
      <cfif NOT fileExists(theImage)>
      <span class="contentwarning">(File Missing)</span>
      </cfif>
      </td>
    </tr>
    </cfif>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td><input name="imageAsset" type="file" class="formfieldcontent" id="imageAsset" style="padding-left:0" maxlength="255" onchange="this.form.webURL.value='';checkInstanceName(this.form,this);checkFileExtention(this.name,'.jpg,.png')" /></td>
    </tr>

    <tr>
      <td align="right" valign="middle">WebLink</td>
      <td><label for="webURL"></label>
        <table border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td valign="middle"><input name="webURL" type="text" class="formfieldcontent" id="webURL" value="#asset.webURL#" size="95" maxlength="255" onclick="this.form.imageAsset.value=''" /></td>
            <td valign="middle" style="padding-left:10px">URL Overrides Local Asset<br />
Asset Not Stored on Server</td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Offset Center</td>
      <td>
      <table width="100%" border="0" cellspacing="5">
        <tr>
          <td width="40" align="right" class="content">X Pos</td>
          <td width="40" align="right" class="content"><img src="images/gps_latt.png" width="44" height="44" /><br /></td>
          <td width="40"><label for="offset_x"></label>
            <input name="offset_x" type="text" class="formfieldcontent" id="offset_x" value="#offset_x#" size="10" maxlength="10" /></td>
          <td width="40" align="right" class="content">Y Pos</td>
          <td width="40" align="right"><p class="content"><img src="images/gps_long.png" alt="" width="44" height="44" /><br />
          </p></td>
          <td><input name="offset_y" type="text" class="formfieldcontent" id="offset_y" value="#offset_y#" size="10" maxlength="10" /></td>
          <td>&nbsp;</td>
          <td>
          <input name="useScaling" type="checkbox" id="useScaling" value="1" <cfif scaling IS 1> checked</cfif> />
          <label for="useScaling" style="padding-top:5px">Pinch and Zoom Scaling</label>
          </td>
        </tr>
      </table>
      </td>
    </tr>
    <cfif asset.webURL IS ''>
    </cfif> 
 </table>
</cfoutput>
