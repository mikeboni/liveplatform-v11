<cfcomponent>

	<cffunction name="addAssetToMapper" access="remote" returntype="boolean">
    
		<cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="locationID" type="numeric" required="yes">
        
		
		<!--- Find if Exists --->
        <cfquery name="assets">
        	SELECT COUNT(*) AS found
            FROM MapperPoints
            WHERE asset_id = #assetID# AND location_id = #locationID#
        </cfquery>
        
        <cfset deleted = false>
        
        <cfif assets.found GT '0'>
        
			<!--- If Exists Remove Record --->
            <cfinvoke component="MapperUtil" method="removeAssetToMapper" returnvariable="deleted">
                <cfinvokeargument name="assetID" value="#assetID#"/>
                <cfinvokeargument name="locationID" value="#locationID#"/>
        </cfinvoke>
        
        <cfreturn deleted>
        
        <cfelse>
        
			<!--- New Record --->
            <cfquery name="addAsset">
                 INSERT INTO MapperPoints (asset_id, location_id, active)
                 VALUES (#assetID#, #locationID#,1) 
            </cfquery>

		</cfif>
        
        <cfreturn true>
        
	</cffunction>
    
    
    <!--- Get Asset IDs Linked --->
    <cffunction name="getMapperAssets" access="remote" returntype="array">
    
        <cfinvoke component="CFC.Assets" method="getMapperAssets" returnvariable="mapperAssets">
                <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfset ids = arrayNew(1)>
        
        <cfoutput query="mapperAssets">
		
        	<cfset arrayAppend(ids,location_id)>
		
		</cfoutput>
		
        <cfreturn ids>

	</cffunction>
    
    
    
    <!--- Get Map Asset --->
    <cffunction name="getMapAsset" access="remote" returntype="struct">
    	<cfargument name="assetID" type="numeric" required="yes">
        
        <cfquery name="mapAsset">
        	SELECT mapAsset_id
            FROM MapperAssets
            WHERE asset_id = #assetID#
        </cfquery>
		
        <cfif mapAsset.recordCount IS 0>
        	<cfset assetID = 0>
        <cfelse>
        	<cfset assetID = mapAsset.mapAsset_id>
        </cfif>
        
        <cfinvoke component="MapperUtil" method="getMapImageThumb" returnvariable="asset">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfreturn asset>
        
	</cffunction>
    
    
    
    
    <!--- Set Map Asset --->
    <cffunction name="setMapAsset" access="remote" returntype="boolean">
    	<cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="mapAssetID" type="numeric" required="yes">
        
        <cfquery name="mapAsset">
        	UPDATE MapperAssets 
            SET mapAsset_id = #mapAssetID#
            WHERE asset_id = #assetID#
        </cfquery>
        
        <cfreturn true>
        
	</cffunction>
    
    
    
    
    <!--- Set Asset Active State --->
    <cffunction name="setMapperAssetState" access="remote" returntype="boolean">
    
    	<cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="locationID" type="numeric" required="yes">
        
        <cfargument name="state" type="numeric" required="no" default="0">

        <!--- Update Record --->
        <cfquery name="addAsset">
             UPDATE MapperPoints 
             SET active = #state#
             WHERE asset_id = #assetID# AND location_id = #locationID#
        </cfquery>
		
        <cfreturn true>

	</cffunction>
    
    
    
    
    <!--- Remove Asset --->
    <cffunction name="removeAssetToMapper" access="remote" returntype="boolean">
    
		<cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="locationID" type="numeric" required="yes">
        
		
		<!--- Find if Exists --->
        <cfquery name="assets">
        	SELECT COUNT(*) AS found
            FROM MapperPoints
            WHERE asset_id = #assetID# AND location_id = #locationID#
        </cfquery>
        
        <cfif assets.found GT '0'>
        
			<!--- If Exists Remove Record --->
            <cfquery name="addAsset">
                DELETE FROM MapperPoints
                WHERE asset_id = #assetID# AND location_id = #locationID#
            </cfquery>

		</cfif>
        
        <cfreturn true>
        
	</cffunction>
    
    
    
    <!--- Get Asset All Asset of AssetType--->
    <cffunction name="getAssetsFromType" access="remote" returntype="array">
        
        <cfargument name="AppID" type="numeric" required="yes" default="0">
        <cfargument name="AssetTypeID" type="numeric" required="yes" default="0">
        
        <cfquery name="allAssets"> 
        	<!---Details--->
            SELECT       Assets.name, Assets.asset_id,  Details.title
                        
            FROM            Assets INNER JOIN
                                     AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id LEFT OUTER JOIN
                                     Details ON Assets.detail_id = Details.detail_id
            WHERE			(Assets.app_id = #AppID#) 
                            AND (Assets.assetType_id = #AssetTypeID#)
            ORDER BY 		AssetTypes.assetType_id ASC

        </cfquery>
        
       
       <cfset theData = arrayNew(1)>
       
       <cfoutput query="allAssets">
	   
       <cfif title NEQ ''>
	   		<cfset name = title>
       </cfif>

       	<cfset arrayAppend(theData,{"name":name, "asset_id":asset_id})>
       
	   </cfoutput>
      
        
        <cfreturn theData>
        
	</cffunction>



    <!--- Get Asset Thumb Img --->
    <cffunction name="getMapImageThumb" access="remote" returntype="struct">
    	
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <cfinvoke component="CFC.Assets" method="getAssetsImage" returnvariable="asset">
                <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
		
        <cfreturn asset>
        
	</cffunction>
    
    
</cfcomponent>