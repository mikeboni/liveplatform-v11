<!--- Prefs --->  
<form action="updateApp.cfm" method="post" enctype="multipart/form-data" id="updateApp">
<cfoutput>
<table width="100%" border="0" cellspacing="10">
   <tr>
    <td width="120" align="right" valign="bottom"><span class="content">App Icon</span></td>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
          <td><img src="#iconPath#" width="68" height="68" /></td>
          <td width="44" align="right" valign="top">
          <a href="##" class="function" onclick="updateApp(1);">
          <img src="images/ok.png" width="44" height="44" />
          </a>
          </td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td align="right"></td>
    <td><input name="imageFile" type="file" class="formfieldcontent" id="imageFile" size="60" maxlength="50" style="padding-left:0" /></td>
  </tr>
  <tr>
    <td width="120" align="right" class="content">App Name</td>
    <td><label for="name"></label>
    <input name="name" type="text" class="formfieldcontent" id="name" value="#appName#" size="60" maxlength="50" />
    <input name="clientID" type="hidden" id="clientID" value="#session.clientID#" />
    <input name="appID" type="hidden" id="appID" value="#session.appID#" /></td>
  </tr>
  <tr>
	  <td align="right" valign="middle" class="content">Product</td>
	  <td valign="middle">
      <select name="productID" id="productID">
      	  <option value="0">None</option>
          <cfloop query="products">
            <option value="#product_id#" <cfif productID IS products.product_id> selected</cfif>>#name# (#abbr#)</option>
          </cfloop> 
	  </select>
      </td>
	  </tr>
  <tr>
    <td align="right" class="content">Bundle ID</td>
    <td><input name="bundleID" type="text" class="formfieldcontent" id="bundleID" value="#bundleID#" size="60" maxlength="50" /></td>
  </tr>
  <tr>
    <td align="right" class="content">Server Path</td>
    <td>
    <input type="text" disabled="disabled" class="formfieldcontent" value="#appPath#" size="60" maxlength="50" /><input name="folder" type="hidden" id="folder" value="#appPath#" />
    </td>
  </tr>
  <tr>
    <td align="right" class="content">App Version</td>
    <td><input name="version" type="text" class="formfieldcontent" id="version" value="#version#" size="60" maxlength="50" /></td>
  </tr>
  <tr>
    <td align="right" class="content">Support EMail</td>
    <td><input name="supportEMail" type="text" class="formfieldcontent" id="supportEMail" value="#support#" size="60" maxlength="50" /></td>
  </tr>
  <tr>
    <td align="right" class="content">Client Default Email</td>
    <td><input name="clientEMail" type="text" class="formfieldcontent" id="clientEMail" value="#clientEmail#" size="60" maxlength="50" /></td>
  </tr>
  <tr>
    <td align="right" class="content">&nbsp;</td>
    <td>
    
    <table width="100%" border="0">
      <tr>
          <td width="80" align="right" class="content">App Active</td>
          <td width="90"><select name="active" id="active2" style="width:80px">
            <option value="yes" <cfif active IS 1>selected</cfif>>Yes</option>
            <option value="no" <cfif active IS 0>selected</cfif>>No</option>
          </select></td>
          <td width="80" align="right" class="content">Force Update</td>
          <td><select name="forceUpdate" id="active" style="width:80px">
            <option value="yes" <cfif forced IS 1>selected</cfif>>Yes</option>
            <option value="no" <cfif forced IS 0>selected</cfif>>No</option>
          </select></td>
          <td width="150" align="right" class="content">Download Available</td>
          <td><select name="download" id="download" style="width:80px">
            <option value="yes" <cfif download IS 1>selected</cfif>>Yes</option>
            <option value="no" <cfif download IS 0>selected</cfif>>No</option>
          </select></td>
      </tr>
    </table>
    
    </td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td>
</table>
</cfoutput>
</form>