<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<script type="text/javascript" src="Assets/instanceName.js"></script>

<cfinvoke  component="CFC.Apps" method="getAssetPath" returnvariable="assetPath">
  <cfinvokeargument name="assetID" value="#assetID#"/>
  <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
</cfinvoke>

<cfset thePDF = ''>
<cfif asset.url NEQ ''>
	<cfset thePDF = '#assetPath##asset.url#'>
</cfif>

<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <cfif asset.url NEQ '' OR asset.webURL NEQ ''>
    <tr>
      <td align="right" valign="top">&nbsp;</td>
      <td class="contentHilighted">
      <cfinclude template="PDFDoc.cfm">
      </td>
    </tr>
    </cfif>
    <cfif asset.url NEQ '' OR asset.webURL NEQ ''>
    <tr>
      <td align="right" valign="top">Document</td>
      <td class="contentHilighted">
      <cfif asset.webURL NEQ "">
      	<a href="#asset.webURL#" target="_new" class="contentLink">#asset.webURL#</a>
<cfelse>
      	#asset.url#
      </cfif>
      <cfif NOT fileExists(thePDF) AND asset.webURL IS ''>
      <span class="contentwarning">(File Missing)</span>
      </cfif>
      </td>
    </tr>
    </cfif>
    <tr>
      <td align="right" valign="top">&nbsp;</td>
      <td><input name="pdfAsset" type="file" class="formfieldcontent" id="pdfAsset" style="padding-left:0" maxlength="255" onchange="this.form.webURL.value='';checkInstanceName(this.form,this);checkFileExtention(this.name,'.pdf')" /></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Web URL</td>
      <td><table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td valign="middle"><input name="webURL" type="text" class="formfieldcontent" id="webURL" value="#asset.webURL#" size="95" maxlength="255" onclick="this.form.pdfAsset.value=''" /></td>
          <td valign="middle" style="padding-left:10px">URL Overrides Local Asset<br />
            Asset Not Stored on Server</td>
        </tr>
      </table></td>
    </tr>
 </table>
</cfoutput>