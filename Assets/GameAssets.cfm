<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>


<cfif asset.recordCount GT 0>

    <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="startDate">
        <cfinvokeargument name="TheEpoch" value="#asset.dateStart#"/>
    </cfinvoke>
    
    <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="endDate">
        <cfinvokeargument name="TheEpoch" value="#asset.dateEnd#"/>
    </cfinvoke>

<cfelse>
	<cfparam name="startDate" default="">
    <cfparam name="endDate" default="">
</cfif>

  <link href="../styles.css" rel="stylesheet" type="text/css"> 

<link rel="stylesheet" href="JS/pikaday.css">
<link rel="stylesheet" href="JS/site.css">


<script type="text/javascript">

function displayContent(theState,theObj)
{
	theObjRef = document.getElementById(theObj);
	
	if(theState)
	{
		theObjRef.style.display = 'block';
	}else{
		theObjRef.style.display = 'none';
	}	
		
}

</script>

<cfif asset.silentMode IS ''><cfset silentMode = false><cfelse><cfset silentMode = asset.silentMode></cfif>
<cfif asset.sendPlayerEMail IS ''><cfset sendPlayerEMail = false><cfelse><cfset sendPlayerEMail = asset.sendPlayerEMail></cfif>
<cfif asset.sendClientEMail IS ''><cfset sendClientEMail = false><cfelse><cfset sendClientEMail = asset.sendClientEMail></cfif>
<cfif asset.useCode IS ''><cfset useCode = false><cfelse><cfset useCode = asset.useCode></cfif>
<cfif asset.sendTargets IS ''><cfset sendTargets = false><cfelse><cfset sendTargets = asset.sendTargets></cfif>


<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td valign="middle">
      <cfoutput>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" style="width:400px" maxlength="128" />
      </cfoutput>
      </td>
    </tr>
    <tr>
      <td height="54" colspan="2" align="left" valign="middle">
      <table width="100%" border="0" cellpadding="5" cellspacing="0" class="sectionHeader" style="padding-bottom:10px;">
        <tr>
            <td width="200" style="padding-left:10px; height:44px">Games Details</td>
            <td align="right" valign="middle" class="content">Slient Mode</td>
            <td width="22" valign="middle" class="content">
            <input name="silentMode" type="checkbox" id="silentMode" value="true" <cfif silentMode>checked</cfif> />
	        <label for="silentMode"></label>
            </td>
          </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle"></td>
      <td align="left" valign="middle">
      
      </td>
    </tr>
	<tr>
	  <td align="right" valign="middle" style="padding-bottom: 30px;">Start Date</td>
	  <td valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="150" valign="middle"><input type="text" id="dateStart" class="formfieldcontent" style="width:100px" />
          <cfoutput>
          <input name="gStartDate" type="hidden" id="gStartDate" value="#dateFormat(startDate,'YYYY-MM-DD')#" />
          </cfoutput>
          </td>
          <td width="80" align="right" valign="middle" class="content" style="padding-right:10px;padding-bottom: 30px;">End Date</td>
          <td valign="middle"><input type="text" id="dateEnd" class="formfieldcontent" style="width:100px" />
          <cfoutput>
          <input name="gEndDate" type="hidden" id="gEndDate" value="#dateFormat(endDate,'YYYY-MM-DD')#" /></cfoutput>
          </td>
        </tr>
      </table></td>
  </tr>
	<tr>
	  <td colspan="2" align="right" valign="middle"><hr size="1" /></td>
  </tr>
	<tr>
	  <td align="right" valign="middle">Send Email</td>
	  <td valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="5">
	    <tr>
	      <td width="22">
          <input name="sendPlayerEmail" type="checkbox" id="sendPlayerEmail" value="true" onchange="displayContent(this.checked, 'emailMessage')" <cfif sendPlayerEMail>checked</cfif> />
      <label for="sendPlayerEmail"></label> 
          </td>
	      <td valign="middle" class="content">Send Player an Email on  Completion</td>
        </tr>
      </table></td>
  </tr>
	<tr>
	  <td align="right" valign="middle">&nbsp;</td>
	  <td valign="middle">
      <div style="display:none" id="emailMessage">
      <table width="100%" border="0" cellspacing="0" cellpadding="5">
	    <tr>
        <td>EMail Message</td>
        </tr>
        <tr>
        <td>
      <cfoutput>
          <textArea name="EmailMessage" rows="5" style="width:450px;" class="content" id="EmailMessage" />#asset.emailMessage#</textArea>
          </cfoutput>
          </td>
          </tr>
          </table>
      </div>
      </td>
  </tr>
	<tr>
	  <td align="right" valign="middle">&nbsp;</td>
	  <td valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="5">
	    <tr>
	      <td width="22"><input name="sendClientEmail" type="checkbox" id="sendClientEmail" value="true" <cfif sendClientEMail>checked</cfif> onchange="displayContent(this.checked, 'clientEmail')" />
	        <label for="sendClientEmail"></label>
            </td>
	      <td valign="middle" class="content">Send Client an Email on Completion</td>
        </tr>
      </table></td>
  </tr>
	<tr>
	  <td align="right" valign="middle">&nbsp;</td>
	  <td valign="middle">
      <div style="display:none" id="clientEmail">
      <table width="100%" border="0" cellspacing="0" cellpadding="5">
	    <tr>
	      <td width="80" align="left">Send an EMail to the Following Client Email</td>
        </tr>
        <tr>
	      <td>
          <cfoutput>
          <input name="clientEmailAddress" type="text" class="formfieldcontent" id="clientEmailAddress" style="width:400px" value="#asset.email#" maxlength="128" />
          </cfoutput>
          </td>
        </tr>
      </table>
      </div>
      </td>
  </tr>
	<tr>
	  <td colspan="2" align="right" valign="middle"><hr size="1" /></td>
  </tr>
	<tr>
	  <td align="right" valign="middle">Include</td>
	  <td valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="5">
	    <tr>
	      <td width="22">
          <input name="includeCode" type="checkbox" id="includeCode" <cfif useCode>checked</cfif> value="true" />
      <label for="includeCode"></label> 
          </td>
	      <td valign="middle" class="content">Completion Code</td>
        </tr>
      </table></td>
  </tr>
	<tr>
	  <td align="right" valign="middle">&nbsp;</td>
	  <td valign="middle"><table width="100%" border="0" cellspacing="0" cellpadding="5">
	    <tr>
	      <td width="22">
          <input name="includeContent" type="checkbox" id="includeContent" value="true" <cfif sendTargets>checked</cfif> />
      <label for="includeContent"></label> 
          </td>
	      <td valign="middle" class="content">Send All  Tagged Assets</td>
        </tr>
      </table></td>
  </tr>
	<tr>
	  <td colspan="2" align="right" valign="top"><hr size="1" /></td>
  </tr>
	<tr>
	  <td colspan="2" align="left" valign="top" class="sectionHeader" style="padding-left:10px; padding-top:12px">Game Messages</td>
  </tr>
	<tr>
	  <td align="right" valign="top">Initial</td>
	  <td valign="middle">
      <cfoutput>
      <textarea name="gameInitalMessage" id="gameInitalMessage" rows="5" style="width:450px;" class="content">#asset.messageStart#</textarea>
     </cfoutput>
      </td>
  </tr>
	<tr>
	  <td align="right" valign="top">Completion</td>
	  <td valign="middle">
      <cfoutput>
      <textarea name="gameCompletionMessage" id="gameCompletionMessage" rows="5" style="width:450px;" class="content">#asset.messageEnd#</textarea>
      </cfoutput>
      </td>
  </tr>
    
</table>
  
  <script src="JS/pikaday.js"></script>
  <script src="JS/moment.js"></script>
  
  
  <cfif sendClientEMail>
	  <script type="text/javascript">
          displayContent(true,'clientEmail');
      </script>
  </cfif>
  
<cfif sendPlayerEMail>
	  <script type="text/javascript">
          displayContent(true,'emailMessage');
      </script>
  </cfif>
  
  
  <script type="text/javascript">
	function getStartEndDate()
	{
		sDate = (pickerStart.getDate() + 1);
		eDate = (pickerEnd.getDate() + 1);
		
		if(eDate == null){ eDate = sDate; };
		
		document.getElementById('gStartDate').value = sDate;
		document.getElementById('gEndDate').value = eDate;
	}
</script>
  
  <script>
  
  <cfoutput>
  	startDate = '#dateFormat(startDate,"MM/DD/YYYY")#';
	endDate = '#dateFormat(endDate,"MM/DD/YYYY")#';
  </cfoutput>
  
    var pickerStart = new Pikaday( { format: 'MM/DD/YYYY', field: document.getElementById('dateStart'), onSelect: function() { getStartEndDate(); } });
	var pickerEnd = new Pikaday( { format: 'MM/DD/YYYY', field: document.getElementById('dateEnd'), onSelect: function() { getStartEndDate(); } });
		
	pickerStart.setDate(new Date(startDate));
	pickerEnd.setDate(new Date(endDate));
	
</script>

</p>
