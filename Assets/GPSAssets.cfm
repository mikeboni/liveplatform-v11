<link href="../styles.css" rel="stylesheet" type="text/css">

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Location Name</td>
      <td>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <tr>
      <td align="right" valign="middle">GPS </td>
      <td><cfoutput>
        <table border="0" cellspacing="5">
          <tr>
            <td width="55" align="right" class="content">Longitude</td>
            <td align="right" class="content"><img src="images/gps_long.png" width="44" height="44" /><br /></td>
            <td width="40"><label for="gps_long"></label>
              <input name="gps_long" type="text" class="formfieldcontent" id="gps_long" value="#asset.gps_long#" /></td>
            <td width="55" align="right" class="content">Lattitude</td>
            <td align="right"><p class="content"><img src="images/gps_latt.png" alt="" width="44" height="44" /><br />
            </p></td>
            <td width="40"><input name="gps_latt" type="text" class="formfieldcontent" id="gps_latt" value="#asset.gps_latt#" /></td>
            <td width="60" align="right" class="content">Radius</td>
            <td class="content"><img src="images/gps_radius.png" alt="" width="44" height="44" /><br /></td>
            <td width="40"><input name="gps_radius" type="text" class="formfieldcontent" id="gps_radius" value="#asset.radius#" size="10" maxlength="4" /></td>
            <td width="5">ft</td>
          </tr>
        </table>
      </cfoutput></td>
    </tr>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td><table border="0" cellspacing="5">
        <tr>
          <td width="55" align="right" class="content">Altitude</td>
          <td width="44" align="right" class="content"><img src="images/gps_long.png" alt="" width="44" height="44" /></td>
          <td width="40"><label for="gps_long2"></label>
            <input name="gps_alt" type="text" class="formfieldcontent" id="textfield2" value="#asset.gps_alt#" /></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td>
	
    <table width="100%" border="0" cellpadding="0" cellspacing="5">
      <tbody>
        <tr>
          <td width="100" valign="top" class="content" style="padding-top: 40px;">Maps Options</td>
          <td width="620">
		<input name="rotationVertical" type="checkbox" id="rotationVertical" value="true" <cfif #asset.zoom# GT 0 AND #asset.style# NEQ ''> checked</cfif> onChange="displayObj('options'); clearData(['zoom','styles'])" />
    <label for="rotationVertical" style="margin-left: 10px; margin-top: 15px; float: left;"></label>	
       </td>
        </tr>
      </tbody>
    </table>	
      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">&nbsp;</td>
      <td>
		  <div id="options" style="display: <cfif #asset.zoom# GT 0 AND #asset.style# NEQ ''>block<cfelse>none</cfif>">
		   <table width="100%" border="0" cellspacing="5">
			<tr>
			  <td colspan="3" align="right" valign="middle" class="content"><hr size="1"></td>
		     </tr>
			<tr>
			  <td align="right" valign="middle" class="content">Zoom</td>
			  <td align="right" valign="top" class="content"><img src="images/zoom.png" alt="" width="44" height="44" /></td>
			  <td> <cfif asset.zoom IS ''><cfset asset.zoom = 0></cfif>
				<input name="zoom" type="text" class="formfieldcontent" id="zoom" value="#asset.zoom#" />
				</td>
			</tr>
			<tr>
			  <td width="55" align="right" valign="top" class="content" style="padding-top: 16px">Styles</td>
			  <td width="44" align="right" valign="top" class="content"><img src="images/styles.png" alt="" width="44" height="44" /></td>
			  <td width="585">
				<textarea name="styles" cols="70" rows="10" id="styles">#asset.style#</textarea>
				</td>
			</tr>
		  </table>
		  </div>
      </td>
    </tr>
 </table>
</cfoutput>


<script>
	function displayObj(objRefId) {

		obj = document.getElementById(objRefId)
		objState = obj.style.display

		if(objState == 'none') {
			obj.style.display = 'block'
		} else {
			obj.style.display = 'none'
		}
	}
	
	function clearData(allFields) {
		
		for(i=0; i< allFields.length; i++) {
			theField = allFields[i]
			obj = document.getElementById(theField)
			obj.value = ''
		}
		
	}
</script>