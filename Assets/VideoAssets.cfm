<style type="text/css">
.playButton {
	width: 92px;
 	height:92px;
 	display:block;
	background:transparent url('images/video/play.png') center top no-repeat;
}

.playButton:hover {
	background-image: url('images/video/pause.png');
}

.scrubbar {
	width: 100%;
 	height:44px;
 	display:block;
	background:transparent url('images/video/scrub-max.png') center top no-repeat;
}

</style>

<script type="text/javascript">

function setController(theType)
{
	scrubBar = document.getElementById('scrubbar');

	if(theType)
	{
		scrubBar.style.backgroundImage = "url('images/video/scrub-max.png')";
		document.getElementsByName("interactive")[0].checked = false;
	}else{
		scrubBar.style.backgroundImage = "url('images/video/scrub-min.png')";
	}
	
	scrubBar.style.display = 'block';
	scrubBar.style.visibility = 'visible';
}

function setAutoplay(theState)
{
	theControl = document.getElementById("autoplay");
	
	if(theState)
	{
		theControl.style.display = 'block';
		theControl.style.visibility = 'visible';
		
		document.getElementsByName("interactive")[0].checked = false;
		document.getElementById("interactive").style.display = 'none';
		
	}else{
	
		theControl.style.display = 'none';
		theControl.style.visibility = 'hidden';
	}
	
}

function setInteractive(theState)
{
	theDetails = document.getElementById('scrubDetails');
	
	if(theState)
	{
		disableOptions = ["rewind","zoom","playpause","speed","autoplay","releaseControls"];
		
		for(i=0;i<disableOptions.length;i++)	{
			setOptions(disableOptions[i], 0)
		}
		
		setController(0);
		
		document.getElementsByName("scrubbar")[0].checked = false;
		
		document.getElementById("interactive").style.display = 'block';
		document.getElementById("autoplay").style.display = 'none';
		theDetails.style.display = 'block';
		
	}else{
		document.getElementsByName("interactive")[0].checked = false;
		theDetails.style.display = 'none';
		document.getElementById("interactive").style.display = 'none';
		clearContainerValues();
	}
	
}

function setOptions(theObjName, theState)
{
	theControl = document.getElementById(theObjName);
	
	if(theState)
	{
		if( theControl != null)	{
			if( theControl === 'interactive' || theControl === 'autoplay')	{
				theControl.style.display="block";
			}else{
				theControl.style.visibility="visible";
			}
		}
		document.getElementsByName(theObjName)[0].checked = true;
	}else{
		if( theControl != null)	{
			if( theControl === 'interactive' || theControl === 'autoplay')	{
				theControl.style.display="none";
			}else{
				theControl.style.visibility="hidden";
			}
		}
		document.getElementsByName(theObjName)[0].checked = false;
	} 
	
}

function setAspectRatio(aspect)	{
	
	aspect = parseInt(aspect);
	
	switch(aspect) {
    case 0:
        calcRatio(1, 1);
        break;
    case 1:
        calcRatio(1920, 1080)
        break;
	case 2:
        calcRatio(1024, 768)
        break;
    default:
        //calcRatio(1, 1)
}
	
}

function calcRatio(w, h)	{
	
	if(w || h)	{
		document.getElementById('videow').value = w;
		document.getElementById('videoh').value = h;
	}else{
		w = document.getElementById('videow').value;
		h = document.getElementById('videoh').value;
	}
	
	r = w/h;
	
	document.getElementById('videor').value = r.toFixed(4);
}

</script>


<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfif asset.recordCount IS 0>

	<cfset rewind = 0>
    <cfset playpause = 1>
    <cfset zoom = 1>
    <cfset loop = 0>
    <cfset pan = 0>
    <cfset speedControls = 0>
    <cfset scrubbar = 1>
    <cfset autoplay = 1>
    <cfset releaseControls = 0>
    <cfset interactive = 0>
    
<cfelse>

	<cfset rewind = asset.rewind>
    <cfset playpause = asset.playpause>
    <cfset zoom = asset.zoom>
    <cfset loop = asset.loop>
    <cfset pan = asset.pan>
    <cfset speedControls = asset.speedControls>
    <cfset scrubbar = asset.scrubbar>
    <cfset autoplay = asset.autoplay>
    <cfset releaseControls = asset.releaseControls>
	<cfset interactive = asset.interactive>
    
</cfif>

<cfif asset.playpause IS ''><cfset asset.playpause = 0></cfif>
<cfif asset.rewind IS ''><cfset asset.rewind = 0></cfif>
<cfif asset.zoom IS ''><cfset asset.zoom = 0></cfif>
<cfif asset.pan IS ''><cfset asset.pan = 0></cfif>
<cfif asset.loop IS ''><cfset asset.loop = 0></cfif>
<cfif asset.speedControls IS ''><cfset asset.speedControls = 0></cfif>
<cfif asset.autoplay IS ''><cfset asset.autoplay = 0></cfif>
<cfif asset.scrubbar IS ''><cfset asset.scrubbar = 0></cfif>
<cfif asset.interactive IS ''><cfset asset.interactive = 0></cfif>

<script type="text/javascript">

	var jsVideo = new model3DManager();
	
	function updateControllerOptions()
	{
		rewindState = document.getElementsByName("rewind")[0].checked;
		playpauseState = document.getElementsByName("playpause")[0].checked;
		zoomState = document.getElementsByName("zoom")[0].checked;
		loopState = document.getElementsByName("loop")[0].checked;
		speedState = document.getElementsByName("speed")[0].checked;
		panState = document.getElementsByName("pan")[0].checked;
		seekState = document.getElementsByName("scrubbar")[0].checked;
		autoplayState = document.getElementsByName("autoplay")[0].checked;
		releaseState = document.getElementsByName("releaseControls")[0].checked;
		interactiveState = document.getElementsByName("interactive")[0].checked;
		
		theOptions = {'rewind':rewindState, 'playpause':playpauseState, 'zoom':zoomState, 'loop':loopState, 'speed':speedState, 'seek':seekState, 'autoplay':autoplayState, 'release':releaseState, 'interactive':interactiveState, 'pan':panState};
	
		jsVideo.setSyncMode();
		jsVideo.setCallbackHandler(setVideoControllerOptionsSuccess);
		jsVideo.setVideoControllerOptions(<cfoutput>#asset.asset_id#</cfoutput>, theOptions);	
	}
	
	function setVideoControllerOptionsSuccess(result)
	{
		console.log('updated options');
	}
	
	//container update
	function saveContainerDetails()	{
	
		xpos = document.getElementById("xpos").value || 0;
		ypos = document.getElementById("ypos").value || 0;
		wsize = document.getElementById("wsize").value || 0;
		hsize = document.getElementById("hsize").value || 0;
		
		theOptions = {'x':xpos, 'y':ypos, 'w': wsize, 'h': hsize};
		
		updateContainerObj = document.getElementById("containerUpdate")
		updateContainerObj.src = 'images/saveupdate.png';
	
		jsVideo.setSyncMode();
		jsVideo.setCallbackHandler(setContainerSuccess);
		jsVideo.setContainerOptions(<cfoutput>#asset.asset_id#</cfoutput>, theOptions);	
		
	}
	
	function setContainerSuccess() {
		updateContainerObj = document.getElementById("containerUpdate")
		updateContainerObj.src = 'images/replace.png';
	}
	
	function clearContainerValues()	{
	
		document.getElementById("xpos").value = '';
		document.getElementById("ypos").value = '';
		document.getElementById("wsize").value = '';
		document.getElementById("hsize").value = '';
		
	}
	
	
</script>

<cfinvoke  component="CFC.Apps" method="getAssetPath" returnvariable="assetPath">
  <cfinvokeargument name="assetID" value="#assetID#"/>
  <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
</cfinvoke>

<cfset theVideo = ''>
<cfif asset.url NEQ ''>
	<cfset theVideo = '#assetPath##asset.url#'>
</cfif>

<cfset theImage = ''>
<cfif asset.placeholder NEQ ''>
	<cfset theImage = '#assetPath##asset.placeholder#'>
</cfif>

<cfif assets.dynamic IS ''>
	<cfset dynamic = 0>
<cfelse>
	<cfset dynamic = assets.dynamic>
</cfif>

<script type="text/javascript" src="Assets/instanceName.js"></script>

<cfoutput>

<table width="810" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      
      <td>
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="content">
        <tr>
          <td>
          <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128">
          </td>
          <td align="right" style="padding-bottom:10px">
          <input name="useDynamic" type="checkbox" id="useDynamic" value="1" <cfif dynamic IS 1> checked</cfif> />
          <label for="useDynamic" style="padding-top:5px">Dynamic Content</label></td>
        </tr>
      </table>
      
    </tr>
    <tr>
      <td align="right" valign="bottom">&nbsp;</td>
      <td>
      
      <table cellpadding="0" cellspacing="0">
        <tr>
          <td valign="top">
			  <cfif fileExists(theImage)>
              <a href="#theImage#" target="_new" class="contentLink">
                <img src="#theImage#" width="200" height="200" border="1" class="imgLockedAspect" />
              </a>
              <cfelse>
                <cfinclude template="noFile.cfm">
              </cfif>
          </td>
          <td width="10" valign="top"><spacer wdith=10></td>
          <td valign="top">
          <cfif asset.webURL NEQ "">
          
          <!--- <cfmediaplayer name="player_html" source="#asset.webURL#" type="flash" width=320 height=240 align="left" title="#assets.assetName#" > --->
          <video height="240" controls id="theVideo">
          	<source src="#asset.webURL#" type="video/mp4">
          </video> 
          
          <cfelse>
          
          <cfif fileExists(theVideo)>
          	<!--- <cfmediaplayer name="player_html" source="#theVideo#" type="flash" width=320 height=240 align="left" title="#assets.assetName#" > --->
            
            <video height="240" controls id="theVideo">
          	<source src="#theVideo#" type="video/mp4">
          </video> 
          
          <cfelse>
          	<cfinclude template="noFile.cfm">
          </cfif>
          
          </cfif>
          </td>
        </tr>
        <tr>
          <td class="content">
          Image 
            Placeholder
            <cfif fileExists(theImage)><cfelse>
            <span class="contentwarning">(File Missing)</span>
          </cfif>
          </td>
          <td>&nbsp;</td>
          <td class="content">
          Video
          <cfif fileExists(theVideo)><cfelse>
            <span class="contentwarning">(File Missing)</span>
          </cfif>
          </td>
        </tr>
        <tr>
          <td class="content">&nbsp;</td>
          <td>&nbsp;</td>
          <td class="content">&nbsp;</td>
        </tr>
        <tr>
          <td class="content"><span class="contentHilighted">#asset.placeholder#</span></td>
          <td>&nbsp;</td>
          <td class="content"><span class="contentHilighted">#asset.url#</span></td>
        </tr>
        <tr>
          <td height="44" valign="bottom" class="content"><input name="imageAsset" type="file" class="formfieldcontent" id="imageAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.jpg,.png')" /></td>
          <td height="44" valign="bottom">&nbsp;</td>
          <td height="44" valign="bottom" class="content"><input name="VideoAsset" type="file" class="formfieldcontent" id="VideoAsset" style="padding-left:0" maxlength="255" onChange="this.form.webURL.value = '';checkInstanceName(this.form,this);checkFileExtention(this.name,'.mp4')" /></td>
        </tr>
        <tr>
          <td align="right" valign="middle" class="content">Video Dimentions</br></td>
          <td valign="middle">&nbsp;</td>
          
          <cfif asset.ratio IS ''>
            <cfset videoh = ''>
            <cfset videow = ''>
            <cfset asset.ratio = 1>
            <cfset aspect = 0>
            <cfelse>
            <cfif asset.ratio GTE 1.7>
              <cfset videoh = 1080>
              <cfset videow = 1920>
              <cfset aspect = 1>
              <cfelseif asset.ratio GTE 1.3>
              <cfset videoh = 768>
              <cfset videow = 1024>
              <cfset aspect = 2>
              <cfelse>
              <cfset videoh = 0>
              <cfset videow = 0>
              <cfset aspect = -1>
              </cfif>
            </cfif>
          
          <td height="60" valign="middle" class="content">
            <select name="aspect" id="aspect" class="formfieldcontent" style="width:50px" onchange="setAspectRatio(this.value)">
              <option value="1" <cfif aspect IS 1 OR aspect IS 0>selected</cfif>>16:9</option>
              <option value="2" <cfif aspect IS 2>selected</cfif>>4:3</option>
              <option value="5" <cfif aspect IS -1>selected</cfif>>Other</option>
              </select>
            W:
            <cfif videow IS ''><cfset videow = 1920></cfif>
            <input name="videow" type="text" class="formfieldcontent" id="videow" style="width:50px" onChange="calcRatio()" value="#videow#">
            H:
            <cfif videoh IS ''><cfset videoh = 1080></cfif>
            <input name="videoh" type="text" class="formfieldcontent" id="videoh" style="width:50px" onChange="calcRatio()" value="#videoh#">
            Ratio:
            <cfif asset.ratio IS ''><cfset videor = 1.77777777778><cfelse><cfset videor = asset.ratio></cfif>
            <input name="videor" type="text" class="formfieldcontent" id="videor" style="width:100px" value="#videor#">
            </td>
        </tr>
        <tr>
          <td colspan="3" class="content"><label for="webURL"></label>
            <table border="0" style="margin-top:10px">
              <tr>
                <td>Video URL</td>
                <td>
                <!--- <input name="webURL" type="text" class="formfieldcontent" id="webURL" onclick="this.form.VideoAsset.value = ''" value="#asset.webURL#" size="80" /> --->
                <textarea style="width: 450px;height: 50px;" name="webURL" type="text" class="formfieldcontent" id="webURL" onclick="this.form.VideoAsset.value="">#asset.webURL#</textarea>
                </td>
                <td style="padding-left:10px">URL Overrides Local Asset<br />Asset Not Stored on Server</td>
              </tr>
          </table></td>
        </tr>
      </table>
      
      </td>
    </tr>
    <tr>
      <td colspan="2" align="right" valign="top"><hr size="1"></td>
    </tr>
    <!--- video controls --->
    <cfif asset.asset_id GT 0>
    <tr>
      <td height="44" colspan="2" align="left" valign="middle" bgcolor="##666666" style="padding-left:10px" class="contentLinkWhite">Video Controller Options</td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="top">
      <div>

    <div style="background:##000; width:495px; border:2px solid ##021a40; float:left; text-align:center">
    
    <div id="loop" style="width:44px; height:44px; text-align: right; float:left"><img src="images/video/loop.png" width="44" height="44"></div>
    <div id="pan" style="width:44px; height:44px; text-align: right; float:left"><img src="images/video/pan.png" width="44" height="44"></div>
    
    <div id="speed" style="width:70; height:32px; text-align: right; padding-top:5px; padding-right:5px; float:right"><img src="images/video/speed.png" width="62" height="32"></div>
    
    <div id="controls" style="width:100%; height:150px; text-align: center; margin-top:90px">
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
      <tr>
        <td align="center" valign="middle">&nbsp;</td>
        <td width="60" align="center" valign="middle">
        <div id="rewind">
        <img src="images/video/rewind.png" width="44" height="44">
        </div>
        </td>
        <td width="100" align="center" valign="middle">
        <div id="playpause"><a href="##" title="playButton" class="playButton"></a></div>
        </td>
        <td width="60" align="center">
        <div id="zoom">
        <img src="images/video/zoom.png" width="44" height="44">
        </div>
        </td>
        <td align="center">&nbsp;</td>
      </tr>
    </table>
    </div>
    <div id="autoplay" style="text-align: center; height:32px;vertical-align: top; display:none" class="contentLinkWhite">autoplay</div>
    <div id="interactive" style="text-align: center; height:32px;vertical-align: top; display:none" class="contentLinkWhite">interactive</div>
    <div class="scrubbar" id="scrubbar"></div>
    </div>
    
    <div style="width:270px; float:left; margin-left:10px; height:auto">
 		<cfif rewind IS ''><cfset rewind = false></cfif>
        <div style="height:32px; padding-top:6px; padding-left:10px; margin-top:10px" class="contentLinkGrey">
        <input name="rewind" id="rewindID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif rewind>checked</cfif>>
        <label for="rewindID">Rewind</label>
        </div>
        <cfif playpause IS ''><cfset playpause = false></cfif>
        <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
        <input name="playpause" id="playpauseID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif playpause>checked</cfif>>
        <label for="playpauseID">Play/Pause</label>
        </div>
        <cfif zoom IS ''><cfset zoom = false></cfif>
        <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
        <input name="zoom" id="zoomID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif zoom>checked</cfif>>
        <label for="zoomID">Full Screen</label>
        </div>
        <cfif loop IS ''><cfset loop = false></cfif>
        <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
        <input name="loop" id="loopID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif loop>checked</cfif>>
        <label for="loopID">Loop</label>
        </div>
        <cfif speedControls IS ''><cfset speedControls = false></cfif>
        <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
        <input name="speed" id="speedID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif speedControls>checked</cfif>>
        <label for="speedID">Speed</label>
        </div>
        <cfif pan IS ''><cfset pan = false></cfif>
        <div style="height:32px; padding-top:6px; padding-left:10px; margin-top:10px" class="contentLinkGrey">
        <input name="pan" id="panID" type="checkbox" onChange="setOptions(this.name,this.checked);updateControllerOptions();" value="1" <cfif pan>checked</cfif>>
        <label for="panID">Pan</label>
        </div>
        <cfif scrubbar IS ''><cfset scrubbar = false></cfif>
    <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
    <input name="scrubbar" id="scrubbarID" type="checkbox" onChange="setController(this.checked);updateControllerOptions();" value="1" <cfif scrubbar>checked</cfif>>
    <label for="scrubbarID">Seek</label>
    </div>
    <cfif autoplay IS ''><cfset autoplay = false></cfif>
    <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
    <input name="autoplay" id="autoplayID" type="checkbox" onChange="setAutoplay(this.checked);updateControllerOptions();" value="1" <cfif autoplay>checked</cfif>>
    <label for="autoplayID">Autoplay</label>
    </div>
    <cfif releaseControls IS ''><cfset releaseControls = false></cfif>
    <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
    <input name="releaseControls" id="releaseControlsID" type="checkbox" onChange="updateControllerOptions();" value="1" <cfif releaseControls>checked</cfif>>
    <label for="releaseControlsID">Release controls after played</label>
    </div>
    <!--- interactive --->
	<cfif interactive IS ''><cfset interactive = false></cfif>
    <div style="height:32px; padding-top:6px; padding-left:10px" class="contentLinkGrey">
		<div>
        <input name="interactive" id="interactiveID" type="checkbox" onChange="setInteractive(this.checked);updateControllerOptions();setOptions(this.name,this.checked)" value="1" <cfif interactive>checked</cfif>><label for="interactiveID">Gesture  forward/backward</label>
     </div>
     <!--- container info --->
     

     	
     
     	<div style="display:<cfif interactive>block<cfelse>none</cfif>" id="scrubDetails">
          <div style="height:32px; margin-top:20px;">
          	
            <cfif asset.container NEQ ''>
            <cfset theRect = listToArray(asset.container)>
          	  <div style="padding-top:4px; float:left">
               Rect
              <input type="text" class="formfieldcontent" id="xpos" placeholder="X" style="width:30px; text-align:center; margin-left:2px" value="#theRect[1]#" maxlength="4" />
              <input type="text" class="formfieldcontent" id="ypos" placeholder="Y" style="width:30px; text-align:center; margin-left:2px" value="#theRect[2]#" maxlength="4" />
              <input type="text" class="formfieldcontent" id="wsize" placeholder="W" style="width:30px; text-align:center; margin-left:2px" value="#theRect[3]#" maxlength="4" />
              <input type="text" class="formfieldcontent" id="hsize" placeholder="H" style="width:30px; text-align:center; margin-left:2px" value="#theRect[4]#" maxlength="4" />
              </div>
             <cfelse> 
              <div style="padding-top:4px; float:left">
               Rect
              <input type="text" class="formfieldcontent" id="xpos" placeholder="X" style="width:30px; text-align:center; margin-left:2px" value="" maxlength="4" />
              <input type="text" class="formfieldcontent" id="ypos" placeholder="Y" style="width:30px; text-align:center; margin-left:2px" value="" maxlength="4" />
              <input type="text" class="formfieldcontent" id="wsize" placeholder="W" style="width:30px; text-align:center; margin-left:2px" value="" maxlength="4" />
              <input type="text" class="formfieldcontent" id="hsize" placeholder="H" style="width:30px; text-align:center; margin-left:2px" maxlength="4" />
              </div>
             </cfif> 
              <div style="width:44px; float:right">
                <img src="images/replace.png" style="cursor:pointer" onClick="saveContainerDetails();" id="containerUpdate" />
              </div>
          </div>
          
      	</div>
    </div>
    
    </div>

</div>
      </td>
  </tr>
  </cfif>
</table>

<script type="text/javascript">
	setOptions('playpause',#asset.playpause#);
	setOptions('rewind',#asset.rewind#);
	setOptions('zoom',#asset.zoom#);
	setOptions('pan',#asset.pan#);
	setOptions('loop',#asset.loop#);
	setOptions('speed',#asset.speedControls#);
	setAutoplay(#asset.autoplay#);
	setController(#asset.scrubbar#);
	setInteractive(#asset.interactive#)
</script>

</cfoutput>