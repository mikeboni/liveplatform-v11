<link href="../styles.css" rel="stylesheet" type="text/css">

<script type="text/javascript">

	var jsCMS = new assetManager();
	
	//save balcony comp
	function genComp(assetID, height)
	{
		if(assetID != '')	{
		theHeight = document.getElementById('height');
	
		jsCMS.setSyncMode();
		jsCMS.setCallbackHandler(genBalconyCompSuccess);
		jsCMS.generateBalconyComp(assetID, theHeight.value);
		
		}else{
			alert('No Images are available to create Comp')	
		}
	}
	
	function genBalconyCompSuccess(result)
	{
		location.reload();
	}
	
</script>


<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<cfinvoke  component="CFC.Apps" method="getAssetPath" returnvariable="assetPath">
  <cfinvokeargument name="assetID" value="#assetID#"/>
  <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
</cfinvoke>


<cfset theImageN = ''>
<cfset theImageS = ''>
<cfset theImageW = ''>
<cfset theImageE = ''>

<cfif asset.north NEQ ''>
	<cfset theImageN = '#assetPath##asset.north#'>
</cfif>

<cfif asset.south NEQ ''>
	<cfset theImageS = '#assetPath##asset.south#'>
</cfif>

<cfif asset.west NEQ ''>
	<cfset theImageW = '#assetPath##asset.west#'>
</cfif>

<cfif asset.east NEQ ''>
	<cfset theImageE = '#assetPath##asset.east#'>
</cfif>

<cfif asset.north NEQ ''>
	<cfset compName = "#LCase(listGetAt(asset.north,1,'_'))#_comp">
    <cfset theImageFull = '#assetPath##compName#.jpg'>
<cfelse>
	<cfset theImageFull = ''>
</cfif>

<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td align="right" valign="middle">Asset Name</td>
      <td>
      <input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128"></td>
    </tr>
    <tr>
      <td align="right" valign="top">Composite</td>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
          	<cfif fileExists(theImageFull)>
            <td colspan="4" class="content"><a href="#theImageFull#" target="_new" class="contentLink"><img src="#theImageFull#" height="70" border="1" class="imgLockedAspect" /></a>
            </td>
            <cfelse>
            <td colspan="4" class="content">
            	<span class="contentwarning">No Comp Image Exists</span> - Select "Generate Image" to create or update a comp image
            </td>
            </cfif>
          </tr>
          <tr>
            <td width="40" align="right" valign="middle" class="content" style="padding-right:5px">Height
            <label for="height"></label></td>
            <td width="40"><input name="height" type="text" class="formfieldcontent" id="height" value="320" maxlength="3" style="width:40px; text-align:center; padding:0" /></td>
            <td width="44" align="left"><img src="images/replace.png" width="44" height="44" onclick="genComp('#asset.asset_id#')" /></td>
            <td class="content">Generate Image</td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td width="80" align="right" valign="top">&nbsp;</td>
      <td>

        <table border="0">
          <tr class="contentHilighted">
            <td align="center">North View Image</td>
            <td align="center">South View Image</td>
          </tr>
          <tr>
            <td align="center">
            <cfif fileExists(theImageN)>
            <a href="#theImageN#" target="_new" class="contentLink">
          	<img src="#theImageN#" alt="#theImageN#" width="313" height="133" border="1" class="imgLockedAspect" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(theImageS)>
            <a href="#theImageS#" target="_new" class="contentLink">
            <img src="#theImageS#" alt="#theImageS#" width="313" height="133" border="1" class="imgLockedAspect" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
          </tr>
          <tr>
            <td>
            <span class="contentHilighted">#asset.north#</span>
            <cfif NOT fileExists(theImageN)>
              <span class="contentwarning">(File Missing)</span>
            </cfif>
            </td>
            <td>
            <span class="contentHilighted">#asset.south#</span>
            <cfif NOT fileExists(theImageS)>
              <span class="contentwarning">(File Missing)</span>
            </cfif>
            </td>
          </tr>
          <tr>
            <td><input name="BalconyNAsset" type="file" class="formfieldcontent" id="BalconyNAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="BalconySAsset" type="file" class="formfieldcontent" id="BalconySAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
          </tr>
          <tr>
            <td height="10" colspan="3">&nbsp;</td>
          </tr>
          <tr class="contentHilighted">
            <td align="center">West View Image</td>
            <td align="center">East View Image</td>
          </tr>
          <tr>
            <td align="center">
            <cfif fileExists(theImageW)>
            <a href="#theImageW#" target="_new" class="contentLink">
            <img src="#theImageW#" alt="#theImageW#" width="313" height="133" border="1" class="imgLockedAspect" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
            <td align="center">
            <cfif fileExists(theImageE)>
            <a href="#theImageE#" target="_new" class="contentLink">
            <img src="#theImageE#" alt="#theImageE#" width="313" height="133" border="1" class="imgLockedAspect" />
            </a>
          <cfelse>
            <cfinclude template="noFile.cfm">
        </cfif>
            </td>
          </tr>
          <tr>
            <td>
            <span class="contentHilighted">#asset.west#</span>
            <cfif NOT fileExists(theImageW)>
              <span class="contentwarning">(File Missing)</span>
            </cfif>
            </td>
            <td>
            <span class="contentHilighted">#asset.east#</span>
            <cfif NOT fileExists(theImageE)>
              <span class="contentwarning">(File Missing)</span>
            </cfif>
            </td>
          </tr>
          <tr>
            <td><input name="BalconyWAsset" type="file" class="formfieldcontent" id="BalconyWAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
            <td><input name="BalconyEAsset" type="file" class="formfieldcontent" id="BalconyEAsset" style="padding-left:0" maxlength="255" onchange="checkFileExtention(this.name,'.png,.jpg')" /></td>
          </tr>
        </table>

      </td>
    </tr>
    <tr>
      <td align="right" valign="middle">Floor Name</td>
      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td width="170"><input name="floorName" type="text" class="formfieldcontent" style="width:150px" id="floorName" value="#asset.floorName#" size="60" maxlength="20" /></td>
              <td width="70" class="content">Default View</td>
              <td class="content">

                <select name="viewFacing" id="viewFacing" style="width:100px">
					<option value="0" <cfif asset.facing IS 0> selected</cfif>>Default</option>
                  <option value="1" <cfif asset.facing IS 1> selected</cfif>>North</option>
                  <option value="2" <cfif asset.facing IS 2> selected</cfif>>West</option>
                  <option value="3" <cfif asset.facing IS 3> selected</cfif>>East</option>
                  <option value="4" <cfif asset.facing IS 4> selected</cfif>>South</option>
                
                </select>
              </td>
            </tr>
          </tbody>
      </table></td>
    </tr>
 </table>
</cfoutput>