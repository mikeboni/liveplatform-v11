<link href="../styles.css" rel="stylesheet" type="text/css" />
<script language="javascript"> 
	
var theTime = function(epochDate)	{
	
	if(epochDate == 0) {
		return { "hours":0, "minutes":0 };
	}else{
		var myDate = new Date(epochDate *1000);	
		return { "hours":myDate.getHours(), "minutes":myDate.getMinutes() };
	}
}

function padDigits(number, digits) {
    return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}

function setEpochTime(hr, mn)	{
	
	hr = padDigits(hr, 2);
	mn = padDigits(mn, 2);
	
	var myTime = "Wed Jun 29 "+hr+":"+mn+":00 2016 GMT";
	
	var myDate = new Date("Wed Jun 29 "+hr+":"+mn+":00 2016 GMT");

	epochDate = (myDate.getTime() + myDate.getTimezoneOffset()*60*1000).toString();
	epochMyDate = epochDate.substring(0,epochDate.length-3);
	
	return epochMyDate;
	
}

function convertUTCDateToLocalDate(date) {
	
    var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();

    newDate.setHours(hours - offset);

    return newDate;   
}

function getTimeIndex(myHr, myMin)	{
	
	var hr = 0;
	var mn = 0;
	
	var hrsList = [];
	var minList = [];
	
	for(i=6;i<=24;i++) { hrsList.push(i); }
	for(i=0;i<=60;i++) { minList.push(i); }
	
	var hr = hrsList.indexOf(myHr);
	var mn = minList.indexOf(myMin);
	
	return {hr:hr, mn:mn}
}



</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="5" class="sectionHeader" style="padding-left:10px">Performer Settings</td>
  </tr>
  <tr class="content">
    <td style="padding:5px">
    <img src="images/programinit.png" width="44" height="44" id="interactive" />
    </td>
    <td colspan="3" align="left">
     <cfoutput>
    <div id="scheduledTime" style="display:block">
    <table border="0" cellspacing="5" cellpadding="0" style="margin-right:10px; margin-top:10px">
      
      <tr>
        <td class="content" style="padding-right:5px"><span class="bundleid">Stanby</span></td>
        <td class="content" style="padding-right:5px">Start</td>
        <td class="content">
        
          <select name="startHrs" id="startHrs" style="width:60px" onchange="getSetTime('startTime');">
          <option value="-1">None</option>
            <cfloop index="hr" from="6" to="24">
              <cfif hr GTE 12>
                
                <cfset alt = hr-12 &" PM">
                
                <cfif hr IS 12>
                  <cfset alt = "#hr# PM">
                <cfelseif hr IS 24>
                  <cfset alt = "#hr-12# AM">
                </cfif>
                
              <cfelse>
              
              	<cfset alt = "#hr# AM">
                
              </cfif>
              <option value="#hr#">#alt#</option>
              </cfloop>
            </select>
          :
          <select name="startMin" id="startMin" style="width:90px"  onchange="getSetTime('startTime');">
            <cfloop index="mn" from="0" to="60">
              <cfif mn GT 1 OR mn IS 0>
                <cfset alt = "#mn# Minutes">
                <cfelse>
                <cfset alt = "#mn# Minute">
                </cfif>
              <option value="#mn#">#alt#</option>
              </cfloop>
            </select>
          
          </td>
        <td width="40" align="right" valign="middle" class="content" style="padding-right:10px">End
          </td>
        <td class="content">
          
          <div id="setEndTime">
            <select name="endHrs" id="endHrs" style="width:60px"  onchange="getSetTime('endTime');">
            <option value="-1">Infinite</option>
              <cfloop index="hr" from="6" to="24">
                <cfif hr GTE 12>
                  
                  <cfset alt = hr-12 &" PM">
                  
                  <cfif hr IS 12>
                    <cfset alt = "#hr# PM">
                    <cfelseif hr IS 24>
                    <cfset alt = "#hr-12# AM">
                    </cfif>
                  
                  <cfelse>
                  <cfset alt = "#hr# AM">
                  </cfif>
                <option value="#hr#">#alt#</option>
                </cfloop>
              </select>
            :
            <select name="endMin" id="endMin" style="width:90px"  onchange="getSetTime('endTime');">
              <cfloop index="mn" from="0" to="60">
                <cfif mn GT 1 OR mn IS 0>
                  <cfset alt = "#mn# Minutes">
                  <cfelse>
                  <cfset alt = "#mn# Minute">
                  </cfif>
                <option value="#mn#">#alt#</option>
                </cfloop>
              </select>
            </div>
          
          </td>
        
      </tr>
      
    </table>
    <cfif NOT StructKeyExists(theObjOptions,'endtime')>
    	<cfset structAppend(theObjOptions,{'endtime':0})>
    </cfif>
    
    </div>
    <script>
	var myDateStart = theTime(#theObjOptions.startTime#);
	var selectedTimeStart = getTimeIndex(myDateStart.hours + 1, myDateStart.minutes);
	
	if(selectedTimeStart.hr > -1)	{
		document.getElementById("startHrs").selectedIndex = selectedTimeStart.hr;
		document.getElementById("startMin").selectedIndex = selectedTimeStart.mn;
	}else{
		document.getElementById("startHrs").selectedIndex = 0;//default
		document.getElementById("startMin").selectedIndex = 0;//default
	};
	
	var myDateEnd
	var selectedTimeEnd;
	
	<cfif theObjOptions.endTime GT 0>
		myDateEnd = theTime(#theObjOptions.endTime#);
		selectedTimeEnd = getTimeIndex(myDateEnd.hours, myDateEnd.minutes);
	<cfelse>
		selectedTimeEnd = {hr:-1,mn:0};
	</cfif>
	
	if(selectedTimeEnd.hr > -1)	{
		document.getElementById("endHrs").selectedIndex = selectedTimeEnd.hr+1;
		document.getElementById("endMin").selectedIndex = selectedTimeEnd.mn;
	}else{
		document.getElementById("endHrs").selectedIndex = 0;//default
		document.getElementById("endMin").selectedIndex = 0;//default
	};
	
	</script>

    </cfoutput>
    
    </td>
    <td align="left">&nbsp;</td>
    <td align="left">&nbsp;</td>
  </tr>
  <tr>
    <td width="50" style="padding:5px">
    <cfif theObjOptions.interactive IS 1>
    	<img src="images/interactive-on.png" width="44" height="44" id="interactive" />
    <cfelse>
    	<img src="images/interactive-off.png" width="44" height="44" id="interactive" />
    </cfif>
    </td>
    <td width="80" class="bundleid"> <input name="useInteractive" type="checkbox" id="useInteractive" value="1" onchange="changeOptionState('interactive', this.checked)" <cfif theObjOptions.interactive IS 1> checked</cfif> /><label for="useInteractive"><span class="ui"></span></label></td>
    <td class="bundleid">Interactive</td>
    <td width="200" align="center"><span class="bundleid">View Orientation</span></td>
    <td width="100" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td style="padding:5px">
    <cfif theObjOptions.audio IS 1>
    	<img src="images/audio-on.png" width="44" height="44" id="audio" />
    <cfelse>
    	<img src="images/audio-off.png" width="44" height="44" id="audio" />
    </cfif>
    </td>
    <td class="bundleid"><input name="useAudio" type="checkbox" id="useAudio" value="1" onchange="changeOptionState('audio', this.checked)" <cfif theObjOptions.audio IS 1> checked</cfif> /><label for="useAudio"><span class="ui"></span></label></td>
    <td class="bundleid">Use Audio</td>
    <td rowspan="3" align="center">
    <cfif theObjOptions.isPortrait IS 1>
    	<img src="images/portrait-view.png" width="200" height="200" id="orientation" />
    <cfelse>
    	<img src="images/landscape-view.png" width="200" height="200" id="orientation" />
    </cfif>
    </td>
    <td rowspan="3" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td style="padding:5px">
    <cfif theObjOptions.israndom IS 1>
    	<img src="images/random-on.png" width="44" height="44" id="cycle" />
    <cfelse>
    	<img src="images/random-off.png" width="44" height="44" id="cycle" />
    </cfif>
    </td>
    <td class="bundleid"><input name="useRandomizer" type="checkbox" id="useRandomizer" value="1" onchange="changeOptionState('cycle', this.checked)" <cfif theObjOptions.israndom IS 1> checked</cfif> /><label for="useRandomizer"><span class="ui"></span></label></td>
    <td class="bundleid">Randomize Cycle</td>
  </tr>
  <tr>
    <td style="padding:5px">
    <cfif theObjOptions.repeat IS 1>
    	<img src="images/cycle-on.png" width="44" height="44" id="repeat" />
    <cfelse>
    	<img src="images/cycle-off.png" width="44" height="44" id="repeat" />
    </cfif>
    
    </td>
    <td class="bundleid">
    <input name="useRepeatCycle" type="checkbox" id="useRepeatCycle" value="1" onchange="changeOptionState('repeat', this.checked)" <cfif theObjOptions.repeat IS 1> checked</cfif> /><label for="useRepeatCycle"><span class="ui"></span></label>
    </td>
    <td class="bundleid">Repeat Cycle</td>
  </tr>
  <tr>
    <td style="padding:5px">&nbsp;</td>
    <td class="bundleid"></td>
    <td class="bundleid">&nbsp;</td>
    <td>
      <div style="padding-left:32px; margin-top:10px">
        <input name="usePortrait" type="checkbox" id="usePortrait" value="1" onchange="changeOptionState('orientation', this.checked)" <cfif theObjOptions.isPortrait IS 1> checked</cfif> />
        <label for="usePortrait"><span class="ui"></span><div style="padding-top:5px; padding-right:5px; float:left">Portrait View</div></label>
      </div>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="5" align="left" style="padding:10px"><hr size="1" /></td>
  </tr>
  <tr>
    <td colspan="5" align="left" style="padding:10px">
	<cfoutput>
      <textarea name="other" id="other">#assets.other#</textarea>
    </cfoutput>
    </td>
  </tr>
</table>
        
<script>
<cfoutput>
var myOptions = {
	"isPortrait":#theObjOptions.isPortrait#,
	"interactive":#theObjOptions.interactive#,
	"audio":#theObjOptions.audio#,
	"isRandom":#theObjOptions.israndom#,
	"repeat":#theObjOptions.repeat#,
	"app":"performer",
	"startTime":#theObjOptions.startTime#,
	"endTime":#theObjOptions.endTime#
	};
</cfoutput>
function changeOptionState(theOption, theValue)	{
	
	if(typeof(theValue) === "boolean"){
		
		if(theValue) {
			theValue = 1;
		}else{
			theValue = 0;	
		}
		
	}
	//console.log(theOption);
	var theObj = document.getElementById(theOption);
	
	if(theOption === 'audio')	{
		if(theValue)	{
		theObj.src = 'images/audio-on.png';
		}else{
			theObj.src = 'images/audio-off.png';
		}
		myOptions.audio = theValue;
	}
	
	if(theOption === 'cycle')	{
		if(theValue)	{
		theObj.src = 'images/random-on.png';
		}else{
			theObj.src = 'images/random-off.png';
		}
		myOptions.isRandom = theValue;
	}
	
	if(theOption === 'interactive')	{
		if(theValue)	{
		theObj.src = 'images/interactive-on.png';
		}else{
			theObj.src = 'images/interactive-off.png';
		}
		myOptions.interactive = theValue;
	}
	
	if(theOption === 'repeat')	{
		if(theValue)	{
		theObj.src = 'images/cycle-on.png';
		}else{
			theObj.src = 'images/cycle-off.png';
		}
		myOptions.repeat = theValue;
	}
	
	if(theOption === 'orientation')	{
		if(theValue)	{
		theObj.src = 'images/portrait-view.png';
		}else{
			theObj.src = 'images/landscape-view.png';
		}
		myOptions.isPortrait = theValue;
	}
	
	if(theOption === 'startTime')	{
		myOptions.startTime = theValue;
	}
	
	if(theOption === 'endTime')	{
		if(theValue == 0)	{
			delete myOptions['endTime'];
		}else{
			myOptions.endTime = theValue;
		}
	}
	
	document.getElementById('other').value = JSON.stringify(myOptions);
		
}

function getSetTime(theType)	{
	
	var hrs, mins;
	
	if(theType == 'startTime')	{
		
		hrs = document.getElementById('startHrs')
		mins = document.getElementById('startMin')
	
	}else{
		
		hrs = document.getElementById('endHrs')
		mins = document.getElementById('endMin')
		
		if(hrs.value == -1){
	
			hrsStart = document.getElementById('startHrs')
			minsStart = document.getElementById('startMin')
			
			h = hrsStart.options[hrsStart.selectedIndex].value;
			m = minsStart.options[minsStart.selectedIndex].value;
			
			changeOptionState('startTime', 0);
		}
	}
	
	h = hrs.options[hrs.selectedIndex].value;
	m = mins.options[mins.selectedIndex].value;
	
	if(h > 0)	{
		theEpochTime = setEpochTime(h, m);
	}else{
		theEpochTime = 0;
	}
	
	changeOptionState(theType, theEpochTime)
	
}

</script>



 

 

 

         
