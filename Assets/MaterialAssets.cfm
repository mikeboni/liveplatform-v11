<link href="../styles.css" rel="stylesheet" type="text/css">

<cfinvoke component="CFC.Assets" method="getAsset" returnvariable="asset">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<script type="text/javascript" src="instanceName.js"></script>

<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="10" class="content">
    
    <tr>
      <td width="80" align="right" valign="middle">Asset Name</td>
      <td valign="middle"><input name="assetName" type="text" class="formfieldcontent" id="assetName" value="#assets.assetName#" size="60" maxlength="128" /></td>
    </tr>
    <tr>
      <td colspan="2" align="right" valign="middle"><hr size="1" /></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Model Name</td>
      <td valign="middle" class="contentLinkGrey"><input name="model3D" type="text" class="formfieldcontent" id="model3D" value="#asset.model3d#" size="60" maxlength="128" /></td>
    </tr>
    <tr>
      <td align="right" valign="middle">Material Name</td>
      <td valign="middle" class="contentLinkGrey"><input name="material" type="text" class="formfieldcontent" id="material" value="#asset.material#" size="60" maxlength="128" /></td>
    </tr>
  </table>
</cfoutput>

