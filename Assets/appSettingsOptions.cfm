<link href="../styles.css" rel="stylesheet" type="text/css" />         

<script type="text/javascript">

var jsEMail = new appEMailSettings();

//Social
function updateSocial(sn)
{
	var socialNetworks = {facebook : 0, twitter: 1, pintrest : 2, instagram : 3};

	theForm = document.updateSettings;
	
	theNetwork = socialNetworks[sn];
	theMessage = theForm[sn + "Message"].value;
	
	socialInfo = {
		 "groupID":groupID,
		 "message":theMessage,
		 "type":theNetwork
	 };
	 
	jsAppOptions.setSyncMode();
	jsAppOptions.updateSocialNetwork(<cfoutput>#session.appID#</cfoutput>,socialInfo);
	
}


function updateVuforiaLicense(sn)
{
	jsAppOptions.setSyncMode();
	jsAppOptions.updateVuforiaLicense(<cfoutput>#session.appID#</cfoutput>,sn);
}

function updateCustReg(reg)
{
	if(reg === true)	{
		theState = 1;
	}else{
		theState = 0;
	}
	
	jsAppOptions.setSyncMode();
	jsAppOptions.projectRegistration(<cfoutput>#session.appID#</cfoutput>,theState);
}



function displaySocialInfo()
{
	theForm = document.updateSettings;

	groupID = theForm.theGroupSocial[theForm.theGroupSocial.selectedIndex].value;
	
	jsAppOptions.setSyncMode();
	jsAppOptions.setCallbackHandler(successSocialInfoHandler);
	jsAppOptions.getSocialNetworks(<cfoutput>#session.appID#</cfoutput>,groupID);
	
}

function successSocialInfoHandler(theInfo)
{
	clearSocial();
	
	theForm = document.updateSettings;
	
	for(i=0;i<theInfo.length;i++)
	{
		theObj = theInfo[i];
		sn = theObj.networkName.toLowerCase();
		theForm[sn + "Message"].value = theObj.message;
	}
}

function clearSocial()
{
	var socialNetworks = {facebook : 0, twitter: 1, pintrest : 2, instagram : 3};
	theForm = document.updateSettings;
	
	Object.keys(socialNetworks).forEach(function(key) 
	{
		sn = key.toLowerCase();
		theForm[sn + "Message"].value = '';
	});

}

//EMail
function updateEmailInfo()
{
	theForm = document.updateSettings;
	groupID = theForm.theGroupEMail[theForm.theGroupEMail.selectedIndex].value;
	
	emailInfo = {
		"cc":theForm.emailCC.value, 
		"subject":theForm.emailSubject.value, 
		"message":theForm.emailMessage.value, 
		"disclaimer":theForm.emailDisclaimer.value
		};
	
	jsEMail.setSyncMode();
	jsEMail.updateEMailConfig(<cfoutput>#session.appID#</cfoutput>, groupID, emailInfo);
	
}

function displayEmailInfo()
{
	theForm = document.updateSettings;
	groupID = theForm.theGroupEMail[theForm.theGroupEMail.selectedIndex].value;
	
	jsEMail.setSyncMode();
	jsEMail.setCallbackHandler(successEMailInfoHandler);
	jsEMail.getEMailConfig(<cfoutput>#session.appID#</cfoutput>,groupID);
	
}

function successEMailInfoHandler(theInfo)
{
	theForm = document.updateSettings;
	theForm.emailCC.value = theInfo.cc;
	theForm.emailSubject.value = theInfo.subject;
	theForm.emailMessage.value = theInfo.message;
	theForm.emailDisclaimer.value = theInfo.disclaimer;
}

</script>

<!--- Access Levels --->
<cfinvoke component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />

<script type="text/javascript">

function updateAppOptions(theObj)
{
	theAccess = theObj.name + '_accessLevel';
	theDivAccess = document.getElementById(theAccess);
	
	if(theAccess === 'sendEmail_accessLevel'){ 
		theAccess = 'email_accessLevel';
	}
	
	theObjName = theObj.id;
	
	if(theObj.checked)
	{
		theState = 1;
	}else{
		theState = 0;
	}
	
	if(theObjName === 'registration')	{
		
		document.getElementById('verification').checked = 0;
		appOptions['verification'] = 0;
		
	} else if(theObjName === 'verification')	{
		
		document.getElementById('registration').checked = 0;
		appOptions['registration'] = 0;
		
	}
	
	if(theDivAccess != null && theState == 0){
		theDivAccess.selectedIndex = 0;
		theDivAccess.style.backgroundImage = "url('images/access_locked-"+ 0 +".png')"
		jsAppOptions.updateAccessOption(<cfoutput>#session.appID#</cfoutput>, theObj.name, 0);
	}
	
	appOptions[theObjName] = theState;
	
	jsAppOptions.setSyncMode();
	jsAppOptions.setAppOptions(<cfoutput>#session.appID#</cfoutput>,appOptions);
}

function showContent(theObj)
{
		theContent = theObj + 'Content';
		theMessage = theObj + 'Description';
		theAccess = theObj + '_accessLevel';
		
		theDiv = document.getElementById(theContent);
		theDivM = document.getElementById(theMessage);

		if(theDiv != null)
		{
			
			if(theDiv.style.display == 'none')
			{
				theDiv.style.display='block';
				theDivM.style.display = 'none';
			}else{
				theDiv.style.display='none';
				theDivM.style.display = 'block';	
			}
		
		}
		
}
</script>

<!--- Options DB --->
<script type="text/javascript">

var jsAppOptions = new appOptionsSettings();

getAppOptions = function(theOption)
{
	jsAppOptions.setSyncMode();
	jsAppOptions.setCallbackHandler(displayAppOptionsdHandler);
	jsAppOptions.getAppOptions(<cfoutput>#session.appID#</cfoutput>);
}

var appOptions;

displayAppOptionsdHandler = function(currentAppOptions) 
{
	appOptions = currentAppOptions;
console.log(appOptions);
	for (var theObj in appOptions)
	{
		theObjState = appOptions[theObj];
	
		theObjDOM = document.updateSettings[theObj]
		
		if(theObjDOM.type == 'checkbox')
		{
		document.updateSettings[theObj].checked = theObjState;
			
			if(theObjState)
			{
				showContent(theObj);
				if(theObj == 'sendEmail')
				{
					displayEmailInfo();
					displaySocialInfo();
				}
			}
			
		}else if(theObjDOM.type == 'select-one'){
			document.updateSettings[theObj].selectedIndex = theObjState;
			document.updateSettings[theObj].style.backgroundImage = "url('images/access_locked-"+ theObjState +".png')"
		}
	}
	
	document.forms['updateSettings']['vuforiaLicense'].value = appOptions.vuforiaLicense;
}

<!--- User AccessLevel for Options --->
updateUserAccess = function(theForm, theType, userID)
{
	
	theSel = theForm.selectedIndex;
	accessLevel = parseInt(theForm[theSel].value);
	
	theForm.style.backgroundImage = "url('images/access_locked-"+ accessLevel +".png')"
	
	switch(theType) {
	case 'email_accessLevel':
        type = 'email'
        break;	
    case 'facebook_accessLevel':
        type = 'facebook'
        break;
    case 'twitter_accessLevel':
    	type = 'twitter'
        break;
	case 'pintrest_accessLevel':
    	type = 'pintrest'
        break;
	case 'instagram_accessLevel':
    	type = 'instagram'
        break;
	case 'addressBook_accessLevel':
    	type = 'addressbook'
        break;	
    default:
        type = 'none'
	} 

	if(type != 'none')
	{
		jsAppOptions.setSyncMode();
		jsAppOptions.setCallbackHandler(accessUpdatedHandler);
		jsAppOptions.updateAccessOption(<cfoutput>#session.appID#</cfoutput>, type, accessLevel);
	}

}

accessUpdatedHandler = function(sucess) 
{
	console.log("Updated:"+ sucess);	
}

getAppOptions();

userAccessUpdatedSuccess = function(result){
	location.reload();
}

</script>

<!--- Get Modules --->
<cfinvoke component="CFC.Modules" method="getGroups" returnvariable="mainGroups">
	<cfinvokeargument name="appID" value="#session.appID#"/>
	<cfinvokeargument name="groupType" value="6"/><!--- Projects --->
</cfinvoke>

  <form id="updateSettings" name="updateSettings">      
  <table width="800" border="0" cellspacing="5" class="contentLinkGrey">
  <tr>
	  <td height="32" colspan="4" align="left" valign="middle" bgcolor="#DDD" class="contentLinkGrey">Client Features</td>
    </tr>
    <tr>
       <td height="44" align="right" valign="top"><img src="images/userStats.png" alt="" width="32" height="32" /></td>
       <td valign="top" style="padding-top:5px">
      <input name="registration" type="checkbox" id="registration" onchange="updateAppOptions(this)" value="true" />
      <label for="registration"></label> 
      </td>
       <td>
       <div id="Description" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Registration Request (Sign in/out required for app)</span>
      </div>
       </td>
       <td>&nbsp;</td>
    </tr>
    <tr>
       <td height="44" align="right" valign="top"><img src="images/userStats.png" alt="" width="32" height="32" /></td>
       <td valign="top" style="padding-top:5px">
      <input name="verification" type="checkbox" id="verification" onchange="updateAppOptions(this)" value="true" />
      <label for="verification"></label> 
      </td>
       <td>
       <div id="Description" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Registration Verification Process (import XLS users list) <a href="images/sample.xlsx" target="_new">sample import file</a></span>
      </div>
       </td>
       <td>&nbsp;</td>
    </tr>
	<tr>
	  <td height="32" colspan="4" align="left" valign="middle" bgcolor="#DDD" class="contentLinkGrey"><span style="padding-left:10px">User Features</span></td>
    </tr>
    <tr>
       <td height="44" align="right" valign="top"><img src="images/userStats.png" width="32" height="32" /></td>
       <td valign="top" style="padding-top:5px">
      <input name="userSettings" type="checkbox" id="userSettings" onchange="updateAppOptions(this)" value="true" />
      <label for="userSettings"></label> 
      </td>
       <td>
       <div id="Description" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Allow Users Access to Account Settings</span>
      </div>
       </td>
       <td>&nbsp;</td>
    </tr>
     <tr>
       <td height="44" align="right" valign="top"><img src="images/userStats.png" width="32" height="32" /></td>
       <td valign="top" style="padding-top:5px">
       <input name="userReports" type="checkbox" id="userReports" onchange="updateAppOptions(this)" value="true" />
      <label for="userReports"></label>
      </td>
       <td>
       <div id="Description" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Allow Users to View Personal Stats and Report Activity</span>
      </div>
       </td>
       <td>&nbsp;</td>
     </tr>
    <tr>
	  <td height="32" colspan="4" align="left" valign="middle" bgcolor="#DDD" class="contentLinkGrey"><span style="padding-left:10px">Stats and Reports</span></td>
    </tr>
     <tr>
       <td height="44" align="right" valign="top"><img src="images/userStats.png" width="32" height="32" /></td>
       <td valign="top" style="padding-top:5px"><input name="customerRegistration" type="checkbox" id="customerRegistration" onchange="updateCustReg(this.checked)" /><label for="customerRegistration"></label></td>
       <td><span style="display: inline-block; vertical-align: middle; line-height: normal;">Customer Project Registration</span></td>
       <td>&nbsp;</td>
     </tr>
     <tr>
	  <td width="80" height="44" align="right" valign="top"><img src="images/statsReports.png" alt="" name="basicReports" width="32" height="32" id="email" /></td>
	  <td width="32" valign="top" style="padding-top:5px"><input name="basicReports" type="checkbox" id="basicReports" onchange="showContent(this.name);updateAppOptions(this)" value="true" />
      <label for="basicReports"></label></td>
	  <td>
      <div id="basicReportsDescription" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Basic Reporting</span>
      </div>
      
      <div id="basicReportsContent" style="display:none;">
          <table width="100%" border="0" cellspacing="10" bgcolor="#DDD">
          <tr valign="top">
            <td><span style="color:#666">The Following includes a list of all report types</span></td>
          </tr>
          <tr>
            <td>
            <ol>
                <li>Total Potential Customers</li>
                <li>Number of Cart Views</li>
                <li>Conversion Rate - Assets sent vs viewedv
                <li>Total Email Carts sent vs total viewed</li>
                <li>Total Assets Sent vs total viewed</li>
                <li>Top Trending Assets - Email, In App and Sent</li>
                <li>List of all emails that have been sent carts of assets</li>
                <li>View a carts assets sent to a customer and what assets where viewed from the cart</li>
            </ol>
            </td>
          </tr>
        </table>
      </div>
      </td>
	  <td>&nbsp;</td>
    </tr>
    <tr>
	  <td width="80" height="44" align="right" valign="top"><img src="images/statsReportsPlus.png" alt="" name="advancedReports" width="32" height="32" id="email" /></td>
	  <td width="32" valign="top" style="padding-top:5px"><input name="advancedReports" type="checkbox" id="advancedReports" onchange="showContent(this.name);updateAppOptions(this)" value="true" />
      <label for="advancedReports"></label></td>
	  <td>
      <div id="advancedReportsDescription" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Advanced Reporting</span>
      </div>
      <div id="advancedReportsContent" style="display:none;">
          <table width="100%" border="0" cellspacing="10" bgcolor="#DDD">
          <tr valign="top">
            <td><span style="color:#666">The Following includes a list of all report types</span></td>
          </tr>
          <tr>
            <td>
            <ol>
            <li>Top Trending Assets - Email, In App and Sent</li>
            <li>List of all emails that have been sent carts of assets</li>
            <li>View a carts assets sent to a customer and what assets where viewed from the cart</li>
            </ol>
            </td>
          </tr>
        </table>
      </div>
      </td>
	  <td>&nbsp;</td>
    </tr>
	<tr>
	  <td height="32" colspan="4" align="left" valign="middle" bgcolor="#DDD" class="contentLinkGrey"><span style="padding-left:10px">EMail</span></td>
    </tr>
	<tr>
	  <td height="44" align="right" valign="top"><img src="images/emailcart.png" alt="" name="sendEmail" width="32" height="32" id="email2" /></td>
	  <td valign="top" style="padding-top:5px"><input name="addressBook" type="checkbox" id="addressBook" onchange="showContent(this.name);updateAppOptions(this);" value="true" /><label for="addressBook"></label></td>
	  <td><div id="sendEmailDescription2" style="height: 35px; line-height: 35px; text-align: left;"> <span style="display: inline-block; vertical-align: middle; line-height: normal;">Allow Users to Connect <span class="contentLink">EMail Address Book Contacts</span></span> </div></td>
	  <td align="left" valign="top">
      <select name="addressBook_accessLevel" id="addressBook_accessLevel" style="background:url(images/access_locked-0.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateUserAccess(this,this.name,0)" />
              <cfoutput query="accessLevels">
              <option value="#accessLevel#" style="color:##333" <cfif levelName IS 'User'>selected="selected"</cfif>>#levelName#</option>
              </cfoutput>
          </select>
      </td>
    </tr>
	<tr>
	  <td width="80" height="44" align="right" valign="top"><img src="images/emailcart.png" alt="" name="sendEmail" width="32" height="32" id="email" /></td>
	  <td width="32" valign="top" style="padding-top:5px"><input name="sendEmail" type="checkbox" id="sendEmail" onchange="showContent(this.name);updateAppOptions(this);displayEmailInfo();" value="true" />
      <label for="sendEmail"></label></td>
	  <td>
      <div id="sendEmailDescription" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Allow Users to <span class="contentLink">EMail</span> a cart of assets to a customer</span>
      </div>
      <div id="sendEmailContent" style="display:none;">
          <table width="100%" border="0" cellspacing="10" bgcolor="#DDD">
          <tr>
              <td align="right"><span style="color:#666">Project</span></td>
              <td>
              <!--- FORM for EMail --->
              <select name="theGroupEMail" id="theGroupEMail" style="width:450px" onchange="displayEmailInfo()">
                <cfloop index="z" from="1" to="#arrayLen(mainGroups)#">
                <cfset theGroup = mainGroups[z]>
                <cfoutput>
                <option value="#theGroup.group_id#">#theGroup.name#</option>
                </cfoutput>
                </cfloop>
              </select>
              </td>
            </tr>
            <tr align="right" valign="top">
              <td colspan="2"><hr width="100%" size="1"></td>
            </tr>
            <tr>
              <td align="right"><span style="color:#666">CC</span></td>
              <td><input name="emailCC" id="emailCC" style="width:450px;" class="content" onchange="updateEmailInfo()"></td>
            </tr>
            <tr>
              <td align="right"><span style="color:#666">Subject</span></td>
              <td><input name="emailSubject" id="emailSubject" style="width:450px;" class="content" onchange="updateEmailInfo()"></td>
            </tr>
            <tr align="right" valign="top">
              <td colspan="2"><hr width="100%" size="1"></td>
            </tr>
            <tr>
              <td align="right" valign="top"><span style="color:#666">Message</span></td>
              <td><textarea name="emailMessage" id="emailMessage" rows="5" style="width:450px;" class="content" onchange="updateEmailInfo()"></textarea></td>
            </tr>
            <tr align="right" valign="top">
              <td colspan="2"><hr width="100%" size="1"></td>
            </tr>
            <tr>
              <td align="right" valign="top"><span style="color:#666">Disclaimer</span></td>
              <td><textarea name="emailDisclaimer" id="emailDisclaimer" rows="5" style="width:450px;" class="content" onchange="updateEmailInfo()"></textarea></td>
            </tr>
            </table>
      </div>
      </td>
	  <td align="left" valign="top">
      <select name="email_accessLevel" id="email_accessLevel" style="background:url(images/access_locked-0.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateUserAccess(this,this.name,0)" />
              <cfoutput query="accessLevels">
              <option value="#accessLevel#" style="color:##333" <cfif levelName IS 'User'>selected="selected"</cfif>>#levelName#</option>
              </cfoutput>
          </select>
      </td>
    </tr>
    <tr>
      <td height="32" colspan="4" align="left" valign="middle" bgcolor="#DDD" class="contentLinkGrey"><span style="padding-left:10px">Social Networking</span></td>
    </tr>
    <tr>
      <td height="44" colspan="2" align="right" valign="middle">Select a Project</td>
      <td valign="middle">
        <!--- FORM for Social Networking --->
        <select name="theGroupSocial" id="theGroupSocial" style="width:450px" onchange="displaySocialInfo()">
          <cfloop index="z" from="1" to="#arrayLen(mainGroups)#">
            <cfset theGroup = mainGroups[z]>
            <cfoutput>
              <option value="#theGroup.group_id#">#theGroup.name#</option>
            </cfoutput>
          </cfloop>
        </select>
      </td>
      <td align="left" valign="top">&nbsp;</td>
    </tr>
    <tr>
	  <td height="44" align="right" valign="top"><img src="images/facebook.png" alt="" name="Facebook" width="32" height="32" id="Facebook" /></td>
	  <td valign="top" style="padding-top:5px"><input name="facebook" type="checkbox" id="facebook" onchange="showContent(this.name);updateAppOptions(this)" value="true" />
      <label for="facebook"></label></td>
	  <td valign="middle">
      <div id="facebookDescription" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Allow Users to post a cart of assets to the <span class="contentLink">Facebook</span> Social Network</span>
      </div>
      <div id="facebookContent" style="display:none;">
          <table width="100%" border="0" cellspacing="10" bgcolor="#DDD">
          <tr valign="top">
            <td><span style="color:#666">Type the Default Message to be appended to the posting</span></td>
          </tr>
          <tr>
            <td><input name="facebookMessage" id="facebookMessage" style="width:550px;" size="128" onchange="updateSocial('facebook')"></td>
          </tr>
        </table>
      </div>
      </td>
	  <td align="left" valign="top">
      <select name="facebook_accessLevel" id="facebook_accessLevel" style="background:url(images/access_locked-0.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateUserAccess(this,this.name,0)" />
              <cfoutput query="accessLevels">
              <option value="#accessLevel#" style="color:##333" <cfif levelName IS 'User'>selected="selected"</cfif>>#levelName#</option>
              </cfoutput>
          </select>
      </td>
    </tr>
    <tr>
      <td height="44" align="right" valign="top"><img src="images/twitter.png" alt="" name="Twitter" width="32" height="32" id="Twitter" /></td>
      <td valign="top" style="padding-top:5px"><input name="twitter" type="checkbox" id="twitter" onchange="showContent(this.name);updateAppOptions(this)" value="true" />
      <label for="twitter"></label></td>
      <td valign="middle">
      <div id="twitterDescription" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Allow Users to post a cart of assets to the <span class="contentLink">Twitter</span> Social Network</span>
      </div>
      <div id="twitterContent" style="display:none;">
          <table width="100%" border="0" cellspacing="10" bgcolor="#DDD">
          <tr valign="top">
            <td><span style="color:#666">Type the Default Message to be appended to the posting</span></td>
          </tr>
          <tr>
            <td><input name="twitterMessage" id="twitterMessage" style="width:550px;" size="128" onchange="updateSocial('twitter')"></td>
          </tr>
        </table>
      </div>
      </td>
      <td align="left" valign="top">
      <select name="twitter_accessLevel" id="twitter_accessLevel" style="background:url(images/access_locked-0.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateUserAccess(this,this.name,0)" />
              <cfoutput query="accessLevels">
              <option value="#accessLevel#" style="color:##333" <cfif levelName IS 'User'>selected="selected"</cfif>>#levelName#</option>
              </cfoutput>
          </select>
      </td>
    </tr>
    <tr>
      <td height="44" align="right" valign="top"><img src="images/pintrest.png" alt="" name="Pintrest" width="32" height="32" id="Pintrest" /></td>
      <td valign="top" style="padding-top:5px"><input name="pintrest" type="checkbox" disabled="disabled" id="pintrest" onchange="showContent(this.name);updateAppOptions(this)" value="true" />
      <label for="pintrest"></label></td>
      <td valign="middle">
      <div id="pintrestDescription" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Allow Users to post a cart of assets to the <span class="contentLink">Pintrest</span> Network (not available)</span>
      </div>
      <div id="pintrestContent" style="display:none;">
          <table width="100%" border="0" cellspacing="10" bgcolor="#DDD">
          <tr valign="top">
            <td><span style="color:#666">Type the Default Message to be appended to the posting</span></td>
          </tr>
          <tr>
            <td><input name="pintrestMessage" id="pintrestMessage" style="width:550px;" size="128" onchange="updateSocial('pintrest')"></td>
          </tr>
        </table>
      </div>
      </td>
      <td align="left" valign="top">
      <select name="pintrest_accessLevel" id="pintrest_accessLevel" style="background:url(images/access_locked-0.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateUserAccess(this,this.name,0)" />
              <cfoutput query="accessLevels">
              <option value="#accessLevel#" style="color:##333" <cfif levelName IS 'User'>selected="selected"</cfif>>#levelName#</option>
              </cfoutput>
          </select>
      </td>
    </tr>
    <tr>
      <td height="44" align="right" valign="top"><img src="images/instagram.png" alt="" name="Instagram" width="32" height="32" id="Instagram" /></td>
      <td valign="top" style="padding-top:5px"><input name="instagram" type="checkbox" disabled="disabled" id="instagram" onchange="showContent(this.name);updateAppOptions(this)" value="true" />
      <label for="instagram"></label></td>
      <td valign="middle">
      <div id="instagramDescription" style="height: 35px; line-height: 35px; text-align: left;">
      <span style="display: inline-block; vertical-align: middle; line-height: normal;">Allow Users to post a cart of assets to the <span class="contentLink">Instagram</span> Network (not available)</span>
      </div>
      <div id="instagramContent" style="display:none;">
          <table width="100%" border="0" cellspacing="10" bgcolor="#DDD">
          <tr valign="top">
            <td><span style="color:#666">Type the Default Message to be appended to the posting</span></td>
          </tr>
          <tr>
            <td><input name="instagramMessage" id="instagramMessage" style="width:550px;" size="128" onchange="updateSocial('instagram')"></td>
          </tr>
        </table>
      </div>
      </td>
      <td align="left" valign="top">
      <select name="instagram_accessLevel" id="instagram_accessLevel" style="background:url(images/access_locked-0.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateUserAccess(this,this.name,0)" />
              <cfoutput query="accessLevels">
              <option value="#accessLevel#" style="color:##333" <cfif levelName IS 'User'>selected="selected"</cfif>>#levelName#</option>
              </cfoutput>
          </select>
      </td>
    </tr>
    <tr>
      <td height="32" colspan="4" align="left" valign="middle" bgcolor="#DDD" class="contentLinkGrey"><span style="padding-left:10px">VuForia License</span></td>
    </tr>
    <tr>
      <td colspan="2" align="right" valign="top">License</td>
      <td align="left" valign="top">
        <table width="100%" border="0" cellpadding="0" cellspacing="5">
          <tr>
            <td width="100"><textarea name="vuforiaLicense" rows="6" id="vuforiaLicense" style="width:550px; padding:5px" type="text" value="" /></textarea></td>
            <td align="left" valign="top"><input type="button" style="background:url('images/ok.png') no-repeat; border:0; width:44px; height:44px;" onClick="updateVuforiaLicense(this.form['vuforiaLicense'].value)" value="" /></td>
          </tr>
    	</table>
      </td>
      <td align="left" valign="top">&nbsp;</td>
    </tr>
    </table>
  </form>
