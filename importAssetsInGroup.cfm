<cfparam name="assetsIDs" default="">
 
<cfif assetsIDs NEQ '' AND session.subgroupid GTE '0'>
<!--- 
	<cfdump var="#session.appID#"><br>
    <cfdump var="#session.subgroupid#"><br>
    <cfdump var="#assetsIDs#"> --->
   
    <cfinvoke component="CFC.Modules" method="addGroupMultipleAssets" returnvariable="items">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="subgroupID" value="#session.subgroupid#"/>
        <cfinvokeargument name="assetIDs" value="#assetsIDs#"/>
    </cfinvoke>

</cfif>

<cflocation url="AppsView.cfm?subgroupID=#session.subgroupid#" addtoken="no">
