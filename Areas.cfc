<cfcomponent>


	<!--- CREATE --->
	<cffunction name="create" access="public" returntype="struct">
		<cfargument name="props" type="struct" required="yes">
		
		<cfset id = 0>
		
		<cfinvoke  component="areas" method="modelDefaults" returnvariable="data">
            <cfinvokeargument name="props" value="#props#"/>
        </cfinvoke>
		
		<cfquery name="newAsset">
			 INSERT INTO liveinventory.dbo.Areas (
				 name, areatype_id, floor, premium 
			 )
			 VALUES (
			 	 '#trim(props.name)#', #data.areaType#, #data.floor#, #data.premium#
			 ) 
			 SELECT @@IDENTITY AS ID
		</cfquery>
		
		<cfset id = newAsset.id>
		
		<cfinvoke  component="API.V11.CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <cfset result = {}>
        
        <cfset structAppend(result,{"error":#error#})> 
        <cfset structAppend(result,{"id":#id#})> 
        
		<cfreturn result>
		
	</cffunction>
	
	
	
	
	<!--- UPDATE --->
	
	<cffunction name="update" access="public" returntype="struct">
		<cfargument name="props" type="struct" required="yes">
		
		<!--- Rec Exists --->
		<cfinvoke  component="areas" method="recExists" returnvariable="recExists">
            <cfinvokeargument name="id" value="#props.assetID#"/>
        </cfinvoke>
        
        <cfinvoke  component="areas" method="modelDefaults" returnvariable="data">
            <cfinvokeargument name="props" value="#props#"/>
        </cfinvoke>
        
        <cfif recExists>
        
			<cfquery name="updateAsset">
				 UPDATE 		liveinventory.dbo.Areas
				 
				 SET			name = '#trim(props.name)#',
				 				areatype_id = #data.areaType#, 
				 				floor = #data.floor#, 
				 				premium = #data.premium#

				 WHERE			Area_id = #data.assetID#
			</cfquery>

			<cfinvoke  component="API.V11.CFC.Errors" method="getError" returnvariable="error">
				<cfinvokeargument name="error_code" value="1000"/>
			</cfinvoke>

        <cfelse>
        	
        	<!--- No Record Found --->
        	<cfinvoke  component="API.V11.CFC.Errors" method="getError" returnvariable="error">
				<cfinvokeargument name="error_code" value="5000"/><!--- Record Not Found --->
			</cfinvoke>
			
		</cfif>
			
		<cfset result = {}>
		<cfset structAppend(result,{"error":#error#})> 
		
		<cfreturn result>
		
	</cffunction>
	
	
	
	<!--- DELETE --->
	
	<cffunction name="delete" access="public" returntype="struct">
		<cfargument name="props" type="struct" required="yes">
		
		<!--- Rec Exists --->
		<cfinvoke  component="areas" method="recExists" returnvariable="recExists">
            <cfinvokeargument name="id" value="#props.assetID#"/>
        </cfinvoke>

		<cfif recExists>
		
			<!--- Record Found --->
			<cfquery name="deleteAsset">
				 DELETE FROM	liveinventory.dbo.Areas

				 WHERE			Area_id = #props.assetID#
			</cfquery>

			<cfinvoke  component="API.V11.CFC.Errors" method="getError" returnvariable="error">
				<cfinvokeargument name="error_code" value="1000"/>
			</cfinvoke>
        
        <cfelse>
        
        	<!--- No Record Found --->
        	<cfinvoke  component="API.V11.CFC.Errors" method="getError" returnvariable="error">
				<cfinvokeargument name="error_code" value="5000"/><!--- Record Not Found --->
			</cfinvoke>
        	
		</cfif>
        
		<cfset result = {}>
		<cfset structAppend(result,{"error":#error#})> 
		
		<cfreturn result>
		
	</cffunction>
	
	
	
	
	<!--- GET ASSET PROPERTIES --->
	
	<cffunction name="getObject" access="public" returntype="struct">
		<cfargument name="props" type="struct" required="yes">

		<!--- Record Found --->
		<cfquery name="assetProperties">
			SELECT 		name, areaType_id AS areaType, floor, premium
			FROM 		liveinventory.dbo.Areas
			WHERE 		area_id = #props.assetID#
		</cfquery>
		
		<cfinvoke component="API.V11.CFC.Misc" method="QueryToStruct" returnvariable="content">
			<cfinvokeargument name="query" value="#assetProperties#"/>
		</cfinvoke>
		
		<!--- Define Model --->
		<cfinvoke component="areas" method="modelDefaults" returnvariable="content">
			<cfinvokeargument name="props" value="#content#"/>
		</cfinvoke>
		
		<cfreturn content>
		
	</cffunction>
	
	
	
	
	<!--- Record Exists --->
	
	<cffunction name="recExists" access="public" returntype="boolean">
		<cfargument name="id" type="numeric" required="yes">
		
		<cfquery name="found">
			 SELECT 	Area_id
			 FROM		liveinventory.dbo.Areas
	
             WHERE		Area_id = #id#
		</cfquery>
		
		<cfif found.recordCount GT 0>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>

		
	</cffunction>
	
	
	
	<!--- Model with Defaults --->
	<cffunction name="dataModel" access="public" returntype="struct">

		<cfset dataModel = {}>

		<cfset structAppend(dataModel, {"areaType": 0})>
		<cfset structAppend(dataModel, {"floor": 0})>
		<cfset structAppend(dataModel, {"premium": 0})>
		
		<cfreturn dataModel>
		
	</cffunction>
	
	
	<!--- Set Model Defaults --->
	<cffunction name="modelDefaults" access="public" returntype="struct">
		<cfargument name="props" type="struct" required="yes">
		
		<cfinvoke component="areas" method="dataModel" returnvariable="data"/>
		
		<cfloop collection = #props# item = "key"> 
			<cfif structKeyExists(data,key)>
				<cfset data[key] = props[key]>
			</cfif>
		</cfloop>
		
		<cfreturn data>
		
	</cffunction>
		
</cfcomponent>