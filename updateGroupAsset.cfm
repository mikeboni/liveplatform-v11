<cfparam name="groupName" default="">
<cfparam name="groupID" default="0">
<cfparam name="filter" default="">
<cfparam name="contentBackgroundImage" default="">
<cfparam name="viewType" default="">

<!--- Group Filter Update --->
<cfif filter NEQ '' AND session.appID GT 0>

    <cfquery name="UpdateAppPrefsFilter">
            UPDATE Applications
            SET filterGroups = '#filter#'
            WHERE	app_id = #session.appID#
    </cfquery>
	
    <cfset error = "Updated Filtered Items">
    
<cfelse>

	<!--- Define Structs for Data --->
    <cfset assetData = structNew()>
    <cfset detailData = structNew()>
    <cfset thumbData = structNew()>
    <cfset colorData = structNew()>
    
    <!--- Build Asset Path --->
    <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
        <cfinvokeargument name="appID" value="#session.appID#"/>
    </cfinvoke>
    
    <!---Temp Path for Upload--->
    <cfset tempPath = GetTempDirectory()>
    
    <cfset groupPath = "#assetPath#images/">
    
    <!--- Thumbnail --->
    <cfparam name="useThumb" default="false">
    <cfparam name="imageThumb" default="">
    
    <!--- Colors --->
    <cfparam name="useColors" default="false">
    
    <!--- Details --->
    <cfparam name="useDetails" default="false">
    <cfparam name="contentColor" default="">
    <cfparam name="contentBGColor" default="">
    <cfparam name="contentOtherColor" default="">
    <cfparam name="title" default="">
    <cfparam name="subtitle" default="">
    <cfparam name="description" default="">
    <cfparam name="other" default="">
    <cfparam name="titleColor" default="">
    <cfparam name="subtitleColor" default="">
    <cfparam name="descriptionColor" default="">
    
    <!--- Image Thumbnail --->
    <cfset imageThumbFile = structNew()>
    
    <cfif imageThumb NEQ "">
    
        <cffile action="upload" filefield="imageThumb" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    
        <!--- Image Size --->
        <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageThumbSize">
            <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
        </cfinvoke>
        
        <cfset imageThumbFile = {"file":uploadFile.serverFile, "size":imageThumbSize}>
        
    <cfelse>
        <cfset imageThumbFile = {"file":""}>
    </cfif>
    
    
    <!--- Colors Background Image --->
    <cfset imageBackgroundFile = structNew()>
    
    <cfif contentBackgroundImage NEQ "">
    
        <cffile action="upload" filefield="contentBackgroundImage" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    
        <!--- Image Size --->
        <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageThumbSize">
            <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
        </cfinvoke>
        
        <cfset imageBackgroundFile = {"file":uploadFile.serverFile, "size":imageThumbSize}>
        
    <cfelse>
        <cfset imageBackgroundFile = {"file":""}>
    </cfif>
    
    
    <!--- Construct Data --->
    <cfset structAppend(assetData,{"groupID":groupID, "path":#groupPath#})>
    
    <cfif useDetails>
        <cfset detailData = {"title": #title#, "subtitle": #subtitle#, "description": #description#, "other": #other#, "titleColor": #titleColor#, "subtitleColor": #subtitleColor#, "descriptionColor": #descriptionColor#}>
    </cfif>
    
    <cfif useDetails>
        <cfset detailData = {"title": #title#, "subtitle": #subtitle#, "description": #description#, "other": #other#, "titleColor": #titleColor#, "subtitleColor": #subtitleColor#, "descriptionColor": #descriptionColor#}>
    </cfif>
    
    <!--- Colors --->
    <cfif useColors>
		<cfset colorData = {"othercolor":'#contentOtherColor#', "forecolor": '#contentColor#', "backcolor": '#contentBGColor#' , "background": '#imageBackgroundFile#', "displayAssetID": '#displayAssetID#'}>
    </cfif>
   
    
    
    <cfif useThumb>
        <cfset thumbData = {"thumbnail": #imageThumb#, "thumb":#imageThumbFile#}>
    </cfif>
    
    <cfset structAppend(assetData,{"name" : #groupName#})>
    <cfset structAppend(assetData,{"viewType" : #viewType#})>
    
    <cfif isDefined('reservedType')>
  		<cfset structAppend(assetData,{"reservedType" : #reservedType#})>
	</cfif>
   
    <cfif (groupName NEQ '') OR (groupName IS '' AND session.subgroupID IS '0')>
      
        <cfinvoke component="CFC.Modules" method="updateGroupDetails" returnvariable="updated">
            <cfinvokeargument name="assetData" value="#assetData#"/>
            <cfinvokeargument name="detailData" value="#detailData#"/>
            <cfinvokeargument name="thumbData" value="#thumbData#"/>
            <cfinvokeargument name="colorData" value="#colorData#"/>
            <cfinvokeargument name="appID" value="#session.appID#"/>
        </cfinvoke>
        
        <cfset error = "Group Information Updated">
        
    <cfelse>
        <cfset error = "Group Must have a Name. Info Not Updated">
        
    </cfif>

</cfif>

<cflocation url="AppsView.cfm?subgroupID=#session.subgroupID#&error=#error#" addtoken="no"> 