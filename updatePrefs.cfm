<cfparam name="prefsID" default="0">
<cfparam name="serverID" default="0">
<cfparam name="sessionT" default="300">
<cfparam name="connectionT" default="45">
<cfparam name="deviceT" default="240">
<cfparam name="heartbeatT" default="20">
<cfparam name="tokenExpiryT" default="0">
<cfparam name="maxTokens" default="0">
<cfparam name="tokenExpires" default="0">
<cfparam name="serverAPI" default="0">
<cfparam name="refresh" default="900000">
<cfparam name="signinExpires" default="15">
<cfparam name="guestSignin" default="0">
<cfparam name="guestRegister" default="0">
<cfparam name="codeSignin" default="0">
<cfparam name="notificationTime" default="15">
<cfparam name="authentication" default="0">

<cfif authentication IS 1>

	<cfset auth = {"client":{}, "grantType":{}, "authentication": 1}>

	<cfset structAppend(auth.client, {"request": clientID_request})>
	<cfset structAppend(auth.client, {"validate": clientID_validate})>
	<cfset structAppend(auth.client, {"refresh": clientID_refresh})>

	<cfset structAppend(auth.grantType, {"request": grant_request})>
	<cfset structAppend(auth.grantType, {"validate": grant_validate})>
	<cfset structAppend(auth.grantType, {"refresh": grant_refresh})>
	
	<cfset structAppend(auth, {"url": authUrl})>
	<cfset structAppend(auth, {"clientSecret": clientSecret})>
	<cfset structAppend(auth, {"validator_id": validateID})>
	
<cfelse>
	<cfset auth = {}>
</cfif>

<cfif notificationTime IS ''><cfset notificationTime = 15></cfif>

    <cfinvoke component="CFC.Apps" method="updateAppPrefs" returnvariable="appID">
            <cfinvokeargument name="prefsID" value="#prefsID#"/>
            <cfinvokeargument name="serverID" value="#serverID#"/>
            <cfinvokeargument name="sessionT" value="#sessionT#"/>
            <cfinvokeargument name="connectionT" value="#connectionT#"/>
            <cfinvokeargument name="deviceT" value="#deviceT#"/>
            <cfinvokeargument name="heartbeatT" value="#heartbeatT#"/>
            <cfinvokeargument name="tokenExpiryT" value="#tokenExpiryT#"/>
            <cfinvokeargument name="maxTokens" value="#maxTokens#"/>
            <cfinvokeargument name="tokenExpires" value="#tokenExpires#">
            <cfinvokeargument name="serverAPI" value="#serverAPI#">
            <cfinvokeargument name="refresh" value="#refresh#">
            <cfinvokeargument name="signinExpires" value="#signinExpires#">
            <cfinvokeargument name="guestSignin" value="#guestSignin#">
            <cfinvokeargument name="guestRegister" value="#guestRegister#">
            <cfinvokeargument name="codeSignin" value="#codeSignin#">
            <cfinvokeargument name="notificationTime" value="#notificationTime#">
            <cfinvokeargument name="authentication" value="#auth#">
    </cfinvoke>

	<!--- Update Prefs JSON --->
    <cfinvoke component="LiveAPI" method="getAppPrefs" returnvariable="success">
        <cfinvokeargument name="appID" value="#session.appID#">
        <cfinvokeargument name="update" value="yes">
        <cfinvokeargument name="addPath" value="../..">
    </cfinvoke>
 
<cflocation url="AppsView.cfm?error=Preferences Updated" addtoken="no">