<cfparam name="clientID" default="0">
<cfparam name="appID" default="0">

<cfparam name="name" default="">
<cfparam name="folder" default="">
<cfparam name="active" default="true">
<cfparam name="bundleID" default="">
<cfparam name="forceUpdate" default="false">
<cfparam name="version" default="100000">
<cfparam name="supportEMail" default="">
<cfparam name="clientEMail" default="">
<cfparam name="download" default="false">
<cfparam name="productID" default="0">

<!---<cfparam name="icon" default="">--->

<cfparam name="clientID" default="0">

<cfparam name="delete" default="0">

<cfset tempPath = GetTempDirectory()>

<cfif imageFile NEQ ''>
	<cffile action="upload" filefield="imageFile" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
    <cfset imageFile = uploadFile.serverFile>
<cfelse>
	<cfset imageFile = ''>
</cfif>

<!---Delete App--->
<cfif delete IS '1' AND clientID GT '0'>
        
</cfif>

<cfif appID GT '0'>
	<!---Update App--->
	
    <cfif name NEQ ''>
    
	<cfinvoke component="CFC.Apps" method="updateApp" returnvariable="appID">
		<cfinvokeargument name="appID" value="#appID#"/>
		<cfinvokeargument name="appName" value="#name#"/>
		<cfinvokeargument name="path" value="#folder#"/>
		<cfinvokeargument name="image" value="#imageFile#"/>
        <cfinvokeargument name="bundleID" value="#bundleID#"/>
        <cfinvokeargument name="active" value="#active#"/>
        <cfinvokeargument name="forced" value="#forceUpdate#"/>
        <cfinvokeargument name="version" value="#version#"/>
        <cfinvokeargument name="support" value="#supportEMail#"/>
        <cfinvokeargument name="clientEmail" value="#clientEmail#"/>
        <cfinvokeargument name="download" value="#download#"/>
        <cfinvokeargument name="productID" value="#productID#"/>
	</cfinvoke>
    
    <!--- Update Prefs JSON --->
    <cfinvoke component="LiveAPI" method="getAppPrefs" returnvariable="success">
        <cfinvokeargument name="appID" value="#session.appID#">
        <cfinvokeargument name="update" value="yes">
        <cfinvokeargument name="addPath" value="../../../">
    </cfinvoke>
	
    <cfif clientID GT '0'>
		<cfset error = 'Client Application Updated'>
		<cflocation url="AppsView.cfm?appID=#appID#" addtoken="no">
	<cfelse>
		<cfset error = 'Client Application Already Not Updated'>
		<cflocation url="AppsView.cfm?appID=0&amp;error=#error#" addtoken="no">
	</cfif>
    
    <cfelse>
    	<cfset error = 'Application Name Cannot be Empty. Application NOT Updated.'>
        <cflocation url="AppsView.cfm?appID=#appID#&amp;error=#error#" addtoken="no">
    </cfif>
	
<cfelse>
	
    <cfif name NEQ '' AND folder NEQ ''>
		<!---New App--->
        <cfinvoke component="CFC.Apps" method="createNewApp" returnvariable="appID">
        	<cfinvokeargument name="clientID" value="#clientID#"/>
            <cfinvokeargument name="appName" value="#name#"/>
            <cfinvokeargument name="path" value="#folder#"/>
            <cfinvokeargument name="image" value="#imageFile#"/>
            <cfinvokeargument name="bundleID" value="#bundleID#"/>
            <cfinvokeargument name="active" value="#active#"/>
            <cfinvokeargument name="forced" value="#forceUpdate#"/>
            <cfinvokeargument name="version" value="#version#"/>
            <cfinvokeargument name="support" value="#supportEMail#"/>
            <cfinvokeargument name="clientEmail" value="#clientEmail#"/>
            <cfinvokeargument name="productID" value="#productID#"/>
        </cfinvoke>
        
        <!--- Update Prefs JSON --->
        <cfinvoke component="LiveAPI" method="getAppPrefs" returnvariable="success">
            <cfinvokeargument name="appID" value="#session.appID#">
            <cfinvokeargument name="update" value="yes">
        </cfinvoke>
        
        <cfif appID GT '0'>
            <cfset error = 'App Created'>
            <cflocation url="AppsView.cfm?appID=#appID#" addtoken="no">
        <cfelse>
            <cfset error = 'App Already Exists'>
            <cflocation url="ClentsView.cfm?clientID=#clientID#&amp;error=#error#" addtoken="no">
        </cfif>
        
    <cfelse>
    		<cfset error = 'App Name OR Folder Path Cannot be Empty. App NOT created.'>
            <!---<cflocation url="CientsView.cfm?clientID=#clientID" addtoken="no">--->
	</cfif>
	
</cfif>