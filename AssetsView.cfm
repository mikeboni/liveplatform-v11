﻿<cfparam name="error" default="">
<cfparam name="assetID" default="0">
<cfparam name="appID" default="">
<cfparam name="assetTypeTab" default="3">
<cfparam name="assetTypeID" default="0">
<cfparam name="importAssets" default="false">


<cfif NOT IsDefined("session.appID")><cfset session.appID = '0'></cfif>
<cfif appID NEQ ''><cfset session.appID = appID></cfif>


<cfif assetID NEQ ''><cfset session.assetID = assetID></cfif>


<cfif NOT IsDefined("session.clientID")><cflocation url="ClentsView.cfm" addtoken="no"></cfif>

<cfinvoke  component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Live-Assets</title>
<link href="styles.css" rel="stylesheet" type="text/css" />

<link href="JS/mcColorPicker/mcColorPicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="JS/mcColorPicker/mcColorPicker.js"></script>
<script type="text/javascript" src="JS/classSelector.js"></script>

<!--- Quiz --->
<cfajaxproxy cfc="CFC.Quiz" jsclassname="quizManager">

<!--- LiveAPI EMail --->
<cfajaxproxy cfc="CFC.Assets" jsclassname="model3DManager">

<!--- LiveAPI Assets --->
<cfajaxproxy cfc="CFC.CMS" jsclassname="assetManager">

<!--- LiveAPI Assets --->
<cfajaxproxy cfc="CFC.Modules" jsclassname="assetImage">


<script type="text/javascript">

//Asset
function updateAsset(form)	{
	if(form==0)
	{
		//new app
		document.forms['newAsset'].submit();
			
	}else{
		//update app
		document.forms['assetDetails'].submit();
		
	}
}
</script>

<!--- Check File Extension --->
<script type="text/javascript">

function hasExtension(inputID, exts) {
    var fileName = document.getElementById(inputID).value;
    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
}

function checkFileExtention(theObj,theExt)
{
	allFileTypes = theExt.split(",");
	
	if (!hasExtension(theObj, allFileTypes) ) 
	{
	   alert('This File Extention does NOT match the requested Extentions - '+theExt);
	   document.getElementById(theObj).value = '';
		
	}

}
</script>

<!--- Content Asset Viewer --->
<cfajaxproxy cfc="CFC.CMS" jsclassname="assetManager">

<script type="text/javascript">

var importAssets = {"assets":[], "actions":[], "quiz":[], "correct":[], "incorrect":[]};

var jsAssets = new assetManager();

var jsModules = new model3DManager();

function displayAssets(theForm,theContentListing)
{
	theTypeSel = theForm.assetTypes;
	theAppSel = theForm.app;
	
	theApp = theAppSel.options[theAppSel.selectedIndex].value
	theAssetType = theTypeSel.options[theTypeSel.selectedIndex].value
	
	if (theAssetType == 0) return false

	contentH = document.getElementById(theContentListing);
	
	theContainerType = checkInside(contentH);
	
	if( theContainerType == 'assets')
	{
		importContentAssets = importAssets['assets'];
		importContainer = 'Assets';
	}
	else if(theContainerType == 'quiz') 
	{
		importContentAssets = importAssets['quiz'];
		importContainer = 'QuizAssets';
	}
	else if(theContainerType == 'quizCorrect')
	{
		importContentAssets = importAssets['correct'];
		importContainer = 'QuizCorrectAssets';
	}
	else if(theContainerType == 'quizIncorrect')
	{
		importContentAssets = importAssets['incorrect'];
		importContainer = 'QuizIncorrectAssets';	
	}
	else if(theContainerType == 'actions')
	{
		importContentAssets = importAssets['actions'];
		importContainer = 'Actions';
	}	
	
	jsAssets.setSyncMode();
	jsAssets.setCallbackHandler(successAssetListing);
	jsAssets.getAllAssets(theApp, theAssetType, importContentAssets, importContainer,<cfoutput>#assetID#</cfoutput>);
	
}

function successAssetListing(result)
{
	theContainer = document.getElementById('displayAssets'+result.contentHolder);
	theContainer.innerHTML = result.html;
}
			
function AssetsImporter(theObj)	{
	
	if(theObj.className == "checkmarkNormal")
	{
		theObj.className = "checkmarkSelected";
		setContentID(theObj.id,1,theObj);
	}else{
		theObj.className = "checkmarkNormal";
		setContentID(theObj.id,0,theObj);
	}

}

function setContentID(theObj,state,varObj)
{
	theIds = document.getElementById('contentIDs');
	
	theObj = parseInt(theObj);
	
	foundObj = importAssets[varObj].indexOf(theObj);
	
	if(state == 1)
	{
		//add
		if(foundObj == -1)
		{
			importAssets[varObj].push(theObj);
		}
		
	}else{
		//remove
		importAssets[varObj].splice(foundObj, 1);
	}
	
	theAssetCnt = document.getElementById('totalAssets');
	theAssetCnt.innerHTML = "Total Assets to Import: "+ importAssets[varObj].length;
}

var jsAssetObject = new assetImage();

function allSelectedContent(theObj)
{
	var jsModules = new model3DManager();
	//containerType = checkInsideAssets(theObj);
	containerType = checkInside(theObj);
	
	action = 0;
	action = 0;
	
	quiz = 0;
	correct = 0;
	incorrect = 0;
	
	//console.log('Type:',containerType);
	
	if(containerType == "quiz"){
	
		importContentAssets = importAssets['quiz'];
		quiz = 1;
	
	}else if(containerType == "quizCorrect"){
		
		importContentAssets = importAssets['correct'];
		correct = 1;
		
	}else if(containerType == "quizIncorrect"){
		
		importContentAssets = importAssets['incorrect'];
		incorrect = 1;
	
	}else if(containerType == "assets"){
		importContentAssets = importAssets['assets'];
		action = 0;
		
	}else{
		
		importContentAssets = importAssets['actions'];
		action = 1;
	}
	
	jsModules.setSyncMode();
	jsModules.setCallbackHandler(successAssetsImport);
	jsModules.setGroupAssets(<cfoutput>#assetID#</cfoutput>, importContentAssets, action, quiz, correct, incorrect);
	
	console.log(<cfoutput>#assetID#</cfoutput>, importContentAssets, action, quiz, correct, incorrect)
	//displayImporter(containerType);
	
}

function successAssetsImport(result)
{
	location.reload();
}

<!--- function displayImporter(theObj)
{
	if(theObj.style.display == "none")
	{
		theObj.style.display = "block";
		
	}else{
		theObj.style.display = "none";
	}
	
	resetAssetImport(theObj);
} --->


<!--- function importActionAssetIntoAsset(theObj)	{
	
	containerType = checkInside(theObj);
	
	action = 0;
	action = 0;
	
	if(containerType == "assets"){
		importContentAssets = importAssets['assets'];
		action = 0;
		
	}else{
		
		importContentAssets = importAssets['actions'];
		action = 1;
	}
	
	console.log(<cfoutput>#assetID#</cfoutput>, importContentAssets, action);
	
	//jsModules.setSyncMode();
	//jsModules.setCallbackHandler(successAssetsImport);
	//jsModules.setGroupAssets(<cfoutput>#assetID#</cfoutput>, importContentAssets, action, quiz, correct, incorrect);
	
} --->



<!--- function displayImporter(theObj)
{
	if(theObj.style.display == "none")
	{
		theObj.style.display = "block";
		
	}else{
		theObj.style.display = "none";
	}
	
	resetAssetImport(theObj);
} --->
 
function resetAssetImport(theObj)
{
	theAssetTypeSel = document.getElementById(theObj.id).getElementsByTagName('select');
	
	console.log('resetAssetImport:'+theAssetTypeSel);
	
	if(0==0)
	{
		
	}
	else if(theObj.id == 'AssetsImporter')
	{
		importAssets['assets'] = [];
	}else{
		importAssets['actions'] = [];
	}

	theAssetTypeSel.assetTypes.selectedIndex = 0;
	
	//theAssetHolder = document.getElementById(theObj.id).childNodes[1]
	//theAssetContent = theAssetHolder.lastElementChild;
	//theAssetContent.innerHTML = "No Assets to Import";
	
	displayAssets(0,theObj.id);
}

function deleteContentAsset(theID,theObj)
{
	var jsModules = new model3DManager();
	
	if(confirm('Are you sure you wish to Remove this Asset?'))
		{
			var action;
			var groupAssetType = checkInside(theObj);
			
			if( groupAssetType === "assets")	{
				action = 0;
			}else{
				action = 1;
			}
			
			jsModules.setSyncMode();
			jsModules.setCallbackHandler(successAssetsDeleted);
			jsModules.removeAssetAction(<cfoutput>#assetID#</cfoutput>,[theID],action);

		}
}


function successAssetsDeleted()
{
	location.reload();
}


function rotateImage(theAssetID,rotate)
{
	if(rotate>0)	{
		rotationClockwise = true;
	}else{
		rotationClockwise = false;
	}
	jsAssets.setSyncMode();
	jsAssets.setCallbackHandler(updatedImageRotation);
	jsAssets.rotateImage(theAssetID,rotationClockwise);
}


function updatedImageRotation()
{
		location.reload();
}


/*function checkInsideAssets(theObj)
{
	actionImporter = document.getElementById('ActionsImporter');
    assetImporter = document.getElementById('AssetsImporter');
	
	actionAssets = document.getElementById('actionListing');
    assetAssets = document.getElementById('assetsListing');
	
	insideAssets = isDescendant(assetImporter, theObj);
	insideActions = isDescendant(actionImporter, theObj);
	
	insideAssetsListing = isDescendant(assetAssets, theObj);
	insideActionsListing = isDescendant(actionAssets, theObj);
	
	console.log(checkInside(theObj));
	
	if(insideAssets || theObj.id == 'AssetsImporter' || insideAssetsListing)
	{
		return true;
	}else{
		return false;
	}
		
}*/


function checkInside(theObj)
{
	//assets and actions
	actionImporter = document.getElementById('ActionsImporter');
    assetImporter = document.getElementById('AssetsImporter');
	
	actionAssets = document.getElementById('actionListing');
    assetAssets = document.getElementById('assetsListing');
	
	insideAssets = isDescendant(assetImporter, theObj);
	insideActions = isDescendant(actionImporter, theObj);
	
	insideAssetsListing = isDescendant(assetAssets, theObj);
	insideActionsListing = isDescendant(actionAssets, theObj);
	
	
	//Quiz
    assetImporterQuiz = document.getElementById('AssetsImporterQuiz');
	assetImporterCorrect = document.getElementById('AssetsImporterCorrect');
	assetImporterIncorrect = document.getElementById('AssetsImporterIncorrect');
	
    assetQuizAssets = document.getElementById('assetsQuizListing');
	assetCorrectAssets = document.getElementById('assetsCorrectListing');
	assetIncorrectAssets = document.getElementById('assetsIncorrectListing');
	
	insideAssetsQuiz = isDescendant(assetImporterQuiz, theObj);
	insideAssetsCorrect = isDescendant(assetImporterCorrect, theObj);
	insideAssetsIncorrect = isDescendant(assetImporterIncorrect, theObj);
	
	insideAssetsQuizListing = isDescendant(assetQuizAssets, theObj);
	insideAssetsCorrectListing = isDescendant(assetCorrectAssets, theObj);
	insideAssetsIncorrectListing = isDescendant(assetIncorrectAssets, theObj);
	
	theType = "";
	
	if(insideAssetsQuiz || theObj.id == 'AssetsImporterQuiz' || insideAssetsQuizListing)
	{
		theType = "quiz";
	}
	else if(insideAssetsCorrect || theObj.id == 'AssetsImporterCorrect' || insideAssetsCorrectListing)
	{
		theType = "quizCorrect";
	}
	else if(insideAssetsIncorrect || theObj.id == 'AssetsImporterIncorrect' || insideAssetsIncorrectListing)
	{
		theType = "quizIncorrect";
	}
	else if(insideAssets || theObj.id == 'AssetsImporter' || insideAssetsListing)
	{
		theType = "assets";
	}
	else if(insideActions || theObj.id == 'ActionsImporter' || insideActionsListing)
	{
		theType = "actions";
	}
	
	return theType;
		
}


var jsAssetObject = new assetImage();

function regenerateNonRetina(theSource)	{
	
	jsAssetObject.setSyncMode();
	jsAssetObject.setCallbackHandler(updatedNonRetina);
	jsAssetObject.createNonRetina(theSource);
	
}

function updatedNonRetina(result)	{
	
	setTimeout(function(){ location.reload();; }, 2000);
	
}

//image rotate
function regenerateNonRetina(theSource)	{
	
	jsAssetObject.setSyncMode();
	jsAssetObject.setCallbackHandler(updatedNonRetina);
	jsAssetObject.createNonRetina(theSource);
	
}

function updatedNonRetina(result)	{
	
	setTimeout(function(){ location.reload();; }, 2000);
	
}

function updateContentAsset(theAssetID,theStuct)
{
	var jsModules = new model3DManager();
	
	jsModules.setSyncMode();
	jsModules.setCallbackHandler(updatedContentOptions);
	jsModules.updatedContentOptions(<cfoutput>#assetID#</cfoutput>,theAssetID,theStuct);
}


function updatedContentOptions(reloadPage)
{
	if(reloadPage){
		location.reload();
	}
}

</script>
<!---  --->

</head>

<body>

<cfinclude template="Assets/header.cfm">

<cfset clientAppPath = assetPathsCur.application.path>
<cfset active = assetPathsCur.application.active>
<cfset iconPath = assetPathsCur.application.icon>
<cfset appName = assetPathsCur.application.name>
<cfset bundleID = assetPathsCur.application.bundleID>
<cfset assetPath = assetPathsCur.types[assetTypeID]>

<cfinvoke  component="CFC.Apps" method="getAssetPath" returnvariable="assetPath">
  <cfinvokeargument name="assetID" value="#assetID#"/>
  <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
</cfinvoke>


    <div style="width:800px; background-color:##333; height:32px; padding-left:0px; padding-top:8px; height:80px;">
		
	<cfoutput>
      <div>
        <cfif active>
          <img src="#iconPath#" width="68" height="68" style="float:left; padding-right:10px" />
        <cfelse>
        	<div style="width:68px; height:68px; float: left; background-image:url(#iconPath#); background-repeat: no-repeat; background-size:contain; background-position: 50% 50%; margin-right:10px"><img src="images/inactive-app.png" width="68" height="68" style="float:left; padding-right:10px" /></div>
        </cfif>  
              <div class="bannerHeading" style="padding-top:10px;">#appName#<span class="bundleid"> (#bundleID#)</span></div>
              <div class="content"><span class="contentHilighted">#clientAppPath#</span></div>
      </div>
 
    </cfoutput>
    
    </div>

<div style="background-color:#666; margin-top:5px; height:32px; padding-left:10px; padding-top:8px; width:800px">
    <span class="sectionLink">
        <a href="AppsView.cfm?assetTypeTab=0" class="<cfif assetTypeTab IS 0>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Application Content</a> | 
        <a href="AppsView.cfm?assetTypeTab=3" class="<cfif assetTypeTab IS 3>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Asset Listing</a> |
        <a href="AppsView.cfm?assetTypeTab=4" class="<cfif assetTypeTab IS 4>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Asset Groups</a>
    </span>
</div>

<cfinvoke component="CFC.Assets" method="getAssets" returnvariable="assets">
    <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<div style="background-color:#069; margin-top:5px; height:32px; padding-left:10px; padding-top:10px; padding-right:10px; width:790px">
<div style="width:400px;float:left"><span class="sectionLink" style="color:#FFFFFF">Asset - <cfoutput>#assets.name#</cfoutput></span></div>
<div class="sectionLink" style="float:right"><a href="#" class="function" onclick="updateAsset(1);">Update</a> | <a href="AppsView.cfm?assetTypeTab=4" class="function">Cancel</a></div>
</div>

<cfoutput query="assets">
    
    <cfset theThumbnailImage = ''>
    <cfif thumbnail NEQ ''>
    	<cfset theThumbnailImage = '#assetPath#thumbs/#thumbnail#'>
  	</cfif>
    
    <form action="updateAsset.cfm?assetTypeID=#assetTypeID#&amp;assetID=#assetID#" method="post" enctype="multipart/form-data" name="assetDetails" id="assetDetails">

        <cfif assetID GT '0' AND assetID IS asset_id>
			<cfset session.subgroupid = 0>
            <cfinclude template="Assets/#dbTable#.cfm">
 
            <cfif importAssets>
            <cfelse>
            	<cfinclude template="thumbsDetails.cfm">
        	</cfif>
            
      </cfif>
    
  	</form>
 
    <cfif assets.dbTable IS 'QuizAssets'>
    
                <table width="100%" border="0" cellpadding="0" cellspacing="10" class="content">
                 <td height="32" colspan="2" align="left" valign="middle" bgcolor="##CCCCCC" class="contentLinkGrey">
              
              <table width="100%" border="0" cellspacing="10" cellpadding="0" bgcolor="##EEE">
                  <tr>
                    <td valign="middle">Choices - Multiple Choice</td>
                    <td width="44" align="right">
                    <input name="newChoice" type="button" id="newChoice" style="background:url(images/add.png) no-repeat; border:0; width:44px; height:44px;" onclick="newQuizChoice()" value="" />
                    </td>
                  </tr>
              </table>
              
              </td>
            </tr>
            <tr>
              <td colspan="2" align="right" valign="middle">
              
              <input name="assetID" type="hidden" id="assetID" value="#assetID#" />
              <div id="quizChoices"></div>
              
              </td>
                </table>
        </cfif>
        
</cfoutput>   

</body>
</html>