<cfparam name="subgroupID" default="0">
<cfparam name="groupID" default="0">
<cfparam name="editGroupName" default="false">
<cfparam name="show" default="">
<!--- <cfparam name="viewType" default="0"> --->
<cfparam name="groupType" default="">


<cfif NOT IsDefined("session.subgroupID")><cfset session.subgroupID = '0'></cfif>
<cfif subgroupID NEQ ''><cfset session.subgroupID = subgroupID></cfif>
<cfinvoke component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />

<cfinvoke component="CFC.Modules" method="getCMSAppGroups" returnvariable="groups">
    <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>

<!--- <cfset groupPaths = []>

<cfoutput query="groups">

    <cfinvoke component="CFC.GroupedAssets" method="getGroupPathContent" returnvariable="theGroupPath">
        <cfinvokeargument name="groupID" value="#group_id#"/>
    </cfinvoke>
    
    <cfif theGroupPath NEQ ''>
    	<cfset theP = name &' ['& theGroupPath &']'>
    	<cfset arrayAppend(groupPaths,theP)>
    <cfelse>
    	<cfset arrayAppend(groupPaths,name)>
    </cfif>
    
</cfoutput>

<cfset QueryAddColumn(groups, "path" , "VarChar", groupPaths)> --->

<!--- Group Listing --->
<link href="styles.css" rel="stylesheet" type="text/css">

<!--- Active State and Order--->
<cfajaxproxy cfc="CFC.Modules" jsclassname="setMyActiveState">

<!--- Active State and Order--->
<cfajaxproxy cfc="CFC.Assets" jsclassname="modifyAssets">

<!--- Apps --->
<cfajaxproxy cfc="CFC.Apps" jsclassname="refreshJSONData">

<script type="text/javascript">

var jsAssets = new modifyAssets();
var jsContentAccess = new contentAccess();
var jsState = new setMyActiveState();
var jsRefresh = new refreshJSONData();

function GetElementInsideContainer(containerID, childID) {
    var elm = document.getElementById(childID);
    var parent = elm ? elm.parentNode : {};
    return (parent.id && parent.id === containerID) ? elm : {};
}



 function showDelete(theObj, state)	{

	theObjD = theObj.getElementsByTagName("input");
	theObjM = theObj.getElementsByTagName("select");
 	
	for(i=0; i < theObjD.length; i++) {
				
		if(theObjD[i].id == "deleteAsset")	{
			delBut = theObjD[i];
			break;
		}
	}
	
	for(i=0; i < theObjM.length; i++) {
				
		if(theObjM[i].id == "moveAsset")	{
			movBut = theObjM[i];
			break;
		}
	}
	
	if(state)	{
		movBut.className = "itemShow";
		delBut.className = "itemShow";
 	}else{

		movBut.className = "itemHide";
		delBut.className = "itemHide";
 	}
 }

function removeAsset(assetID) {
		if(confirm('Are you sure you wish to Remove Asset?'))
		{
			jsState.setSyncMode();
			jsState.setCallbackHandler(removeAssetSuccess);
			jsState.deleteAssetInGroup(assetID,<cfoutput>#subgroupID#</cfoutput>);
			
		}else{ 
			//nothing
		}
}

function removeAssetSuccess(result)	{
	location.reload();
	
}

function removeGroup(groupID) {

		if(confirm('Are you sure you wish to Remove the Group?'))
		{
			jsState.setSyncMode();
			jsState.setCallbackHandler(removeGroupSuccess);
			jsState.deleteGroupAssets(groupID);
			
		}else{ 
			//nothing
		}
}

function removeGroupSuccess(result)	{
	location.reload();
}

updateAssetAccess = function(theSel,groupID) 
{
	
	theLevel = parseInt(theSel.options[theSel.selectedIndex].value);
	
	if(theLevel == 0)
	{
		lock = 'access_unlocked';  
	}else{
		lock = 'access_locked-'+theLevel; 
	}
	
	theSel.style.backgroundImage = 'url(images/'+ lock +'.png)';
	
	jsContentAccess.setSyncMode();
	jsContentAccess.setContentAccess(groupID,theLevel);
	
}




toggleActiveState = function(theObject,groupID,assetID)
{

    var theState = document.getElementById('activeState_'+theObject).innerHTML;
	theState = theState.trim();
	var theObjState;

	if(theState == 'YES')
	{
		theObjState = 'NO';
		theNewState = 0;
		CSSStyle = 'contentLinkGreen';
		CSStyleContent = 'contentLinkDisabled';
		
		jsState.setSyncMode();
		jsState.setAssetShareState(assetID,groupID,theNewState);
	
		document.getElementById('shareState_'+theObject).innerHTML = theObjState;
		document.getElementById('shareState_'+theObject).className = CSSStyle;
		
	}else{
		theObjState = 'YES';
		theNewState = 1;
		CSSStyle = 'contentLinkRed';
		CSStyleContent = 'contentLink';
	}
	
	jsState.setSyncMode();
	jsState.setCallbackHandler(showStateResult);
	jsState.setAssetActiveState(assetID,groupID,theNewState);
	
	document.getElementById('activeState_'+theObject).innerHTML = theObjState;
	
	document.getElementById('activeState_'+theObject).className = CSSStyle;
	
	document.getElementById('contentAsset_'+theObject).className = CSStyleContent;
	
}

//Share

toggleShareState = function(theObject,groupID,assetID,theState)
{
	if(theState == undefined)
	{
		var theState = document.getElementById('shareState_'+theObject).innerHTML;
		theState = theState.trim();
	}
	var theObjState;
	
	if(theState == 'YES')
	{
		theObjState = 'NO';
		theNewState = 0;
		CSSStyle = 'contentLinkGreen';
		CSStyleContent = 'contentLinkDisabled';
	}else{
		theObjState = 'YES';
		theNewState = 1;
		CSSStyle = 'contentLinkRed';
		CSStyleContent = 'contentLink';
	}
	
	//jsState.setCallbackHandler(showStateResult);
	jsState.setSyncMode();
	jsState.setAssetShareState(assetID,groupID,theNewState);
	
	document.getElementById('shareState_'+theObject).innerHTML = theObjState;
	
	document.getElementById('shareState_'+theObject).className = CSSStyle;
	
}

setSortOrder = function(groupID, assetID, theOrder)
{	
	theOrderNum = parseInt(theOrder);
	
	jsState.setSyncMode();
	jsState.setCallbackHandler(successSortUpdated);
	jsState.setSortOrder(assetID,groupID,theOrderNum);
}

function successSortUpdated()	{
	//location.reload();
}

showStateResult = function(result){
	
}



toggleCachedState = function(theObject,groupID,assetID)
{
	for(i = j = 0; i < theObject.childNodes.length; i++)
    if(theObject.childNodes[i].nodeName == 'IMG'){
        j++;
        var theObj = theObject.childNodes[i];
        break;
    }
	
	theFile = theObj.src.replace(/^.*[\\\/]/, '');
	
	if(theFile == 'cache.png')
	{
		theObj.src = 'images/cached.png';
		theNewState = 1;
	}else{
		theObj.src = 'images/cache.png';
		theNewState = 0;
	}
	
	jsState.setSyncMode();
	jsState.setAssetCachedState(assetID,groupID,theNewState);
	
}


function moveAsset(assetID,moveGroupID,groupName) {
		
		if(confirm('Are you sure you wish to Move Group Asset to "'+ groupName +'"?'))
		{
			jsState.setSyncMode();
			jsState.setCallbackHandler(updateMoveAssetSuccess);
			jsState.moveGroupAsset(assetID,moveGroupID,groupName);
			
		}else{ 
			//nothing
		}
}

function moveGroup(groupID,moveGroupID,groupName) {
		
		if(confirm('Are you sure you wish to Move Group and All Group Assets to "'+ groupName +'"?'))
		{
			jsState.setSyncMode();
			jsState.setCallbackHandler(updateMoveGroupSuccess);
			jsState.moveGroup(groupID,moveGroupID,groupName);
			
			
		}else{ 
			//nothing
		}
}

function updateMoveGroupSuccess(result)	{
	location.reload();
}

function updateMoveAssetSuccess(result)	{
	location.reload();
}


refreshJSON = function()
{	
	theObjRef = document.getElementById('refresh');
	theObjRef.style.opacity = ".2";
	
	//jsRefresh.(showUpdated);
	
	jsRefresh.setSyncMode();
	
	jsRefresh.setCallbackHandler(showUpdated);
	
	<cfoutput>
	jsRefresh.generateContentJSON(#session.subgroupid#, #session.appid#);
	</cfoutput>
	
}

refreshAllJSON = function(dev,vr)	{	
	
	
	var svr = document.getElementById('serverVr');
	//var selectedServerVr = svr.options[svr.selectedIndex].value;
	
	theLinkPaths = document.location.href.split('/');
	
	var foundObj = -1;
	
	for(i=0;i<theLinkPaths.length;i++)	{
			
		if(theLinkPaths[i] == 'API' || theLinkPaths[i] == 'api')	{
			foundObj = i+1;
			break;	
		}
			
	}
	
	curVr = theLinkPaths[foundObj];
	
	if(curVr != ('v'+vr))	{
		//not the same server
		message = 'Current Application does NOT match Server Version!\nYou MUST BE in'+vr+' to Generate this Data!';
	
		if(foundObj > -1)	{
			alert("Wrong CMS to Generate this version - Switching to 'V-"+vr+"' CMS");	
			theLinkPaths[foundObj] = 'v'+vr;
			newURLPath = theLinkPaths.join('/');
			document.location.href = newURLPath;
		}else{
			alert("server version not supported");	
		}
	
		return;
	
	}else{
		//everthing is ok
	}
	
	if(dev === undefined)	{ dev = 0; }
	
	if(dev === 1)	{
		message = 'Refresh and Rebuild JSON on DEV SERVER?';
	}else{
		message = 'Refresh and Rebuild JSON on PROD SERVER?';
	}
	
	if(confirm(message))
	{
		jsRefresh.setSyncMode();
	
		<cfoutput>
		jsRefresh.generateAllContentJSON(#session.appid#,dev);
		</cfoutput>
	}else{ 
		//nothing
	}
}

showUpdated = function(result){
	
	theObjRef = document.getElementById('refresh');
	theObjRef.style.opacity = "1.0";
}


function refreshProgramData(groupID, groupName)	{
	
	//console.log(groupID);
	
	if(confirm('Refresh data for this Group "'+ groupName +'"?'))
		{
			
			jsState.setSyncMode();
			jsState.setCallbackHandler(updateGenerateGroupData);
			jsState.generateGroupData(groupID,<cfoutput>#session.appID#</cfoutput>);
			
			
		}else{ 
			//nothing
		}
		
}

function updateGenerateGroupData(result)	{
	console.log("Group Data Generated");
}
			
function displayOptions(theOption) {
	
	console.log(theOption)
	theOption = parseInt(theOption)
	var option
	
	switch (theOption) {
		case 1: 
			option = 'themeOption'
			break
		case 12: 
			option = 'sectionOption'
			break
	}
	console.log(option)
	document.getElementById(option).style.display = 'block'
	
}


</script>

<!--- Group Content --->
<cfinvoke component="CFC.Modules" method="getGroups" returnvariable="contents">
    <cfinvokeargument name="appID" value="#session.appID#"/>
    <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
    <cfinvokeargument name="active" value="#show#"/>
</cfinvoke>

<!--- Group --->
<cfinvoke component="CFC.Modules" method="getGroupName" returnvariable="group">
    <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
</cfinvoke>

<!--- Access Levels --->
<cfinvoke component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />

<style type="text/css">
.sample {
	font-size: 9px;
}
</style>

<cfif NOT structKeyExists(group,'reservedType')>
  <cfset revType = 0>
  <cfset revDesc = ''>
<cfelse>
  <cfset revType = group.reservedType>
  <cfset revDesc = group.reservedDescription>
</cfif>

<!--- get reserved items --->
<cfinvoke component="CFC.Modules" method="getReserved" returnvariable="reservedTerms" />


<div class="rowhighlighter" style="margin-top:5px">

<form action="updateGroupAsset.cfm" method="post" enctype="multipart/form-data" id="groupDetails">
  <table width="810" border="0" cellpadding="0" cellspacing="0" class="content" bgcolor="#AAA">
    <tr>
      <td height="32" colspan="5" bgcolor="#333" class="sectionLinkHighlighted">
      
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="44" align="left" valign="middle">
          
          <cfoutput>
          <table border="0" cellspacing="5">
              <tr>
              <td height="44">
              
              <cfif group.id GT '0'>
                <a href="AppsView.cfm"><img src="images/home.png" width="44" height="44" border="0" /></a>
              </cfif>
              
              </td>
              <td>
              <cfoutput>
                  <cfif editGroupName>
                  <!--- Application Root --->
                      <cfif subgroupID GT '0'>
                      <input name="groupName" type="text" class="formText" id="groupName" value="#group.name#" maxlength="128" style="width:400px">
                      <input name="groupID" type="hidden" id="groupID" value="#group.id#" />
                       <cfelse>
                       <!--- get app pref filters --->
                       <cfquery name="filter">
                            SELECT filterGroups
                            FROM Applications
                            WHERE	app_id = #session.appID#
                        </cfquery>
                        
                        <cfset theFilters = filter.filterGroups>
                       
                       Application Root<br />
                       <div style="margin-top:10px">
                       <span class="contentLinkWhite"> Filter Groups From JSON:</span>
                       <input name="filter" type="text" class="formText" id="filter" style="width:500px" value="#theFilters#" /></div>
                       </cfif>
                   <!---  --->
                  <cfelse>
                      <cfif group.id GT '0'>
                         
                          <cfinvoke component="CFC.Modules" method="getBreadcrumb">
                              <cfinvokeargument name="groupID" value="#subgroupID#"/>
                          </cfinvoke>
                         
                          <cfif group.active IS '0'>
                           <span class="contentwarning"> (NOT ACTIVE)</span>
                         </cfif>
                         
                      <cfelse>
                          <span class="contentLinkWhite">Root Group</span>
                      </cfif>
                </cfif>
              </cfoutput>
              <td>
              <cfif editGroupName AND subgroupID NEQ 0>
                  <select name="reservedType" style="width:100px" onChange="displayOptions(this.value)">
                  <option value="0">Default</option>
                  <cfloop collection="#reservedTerms#" item="theTerm">
                     <option value="#theTerm#" <cfif revType IS theTerm> selected</cfif>>#reservedTerms[theTerm].term#</option>
                  </cfloop>
                  </select>   
              </cfif>
              </td>									
              
               <td>
              <cfif subgroupID NEQ 0>
           
              	<table border="0" cellpadding="0" cellspacing="0" class="content">
                <tr>
                    <td width="60" style="color:##fff; padding-right:5px; padding-left:10px">View Type:</td>
                    <td width="100" class="contentGreyed">
                    <cfif editGroupName>
                    <cfset viewTypes = {0:"Default", 1:"Root View", 2:"List View", 3:"Unit View", 4:"Condo View", 5:"List Pop View", 6:"Design View"}>
                    
                    <select name="viewType" style="width:100px">
                    	<cfloop collection="#viewTypes#" item="theView">
                    		<cfset theType = viewTypes[theView]>
                    		<option value="#theView#" <cfif group.viewType IS theView> selected</cfif>>#theType#</option>
						</cfloop>
                    	<!--- <option value="0" <cfif group.viewType IS 0> selected</cfif>>Default</option>
                        <option value="1" <cfif group.viewType IS 1> selected</cfif>>Root View</option>
                        <option value="2" <cfif group.viewType IS 2> selected</cfif>>List View</option>
                        <option value="3" <cfif group.viewType IS 3> selected</cfif>>Unit View</option>
                        <option value="4" <cfif group.viewType IS 4> selected</cfif>>Condo View</option>
                        <option value="5" <cfif group.viewType IS 5> selected</cfif>>List Pop View</option>
                        <option value="6" <cfif group.viewType IS 6> selected</cfif>>Design View</option> --->
                    </select>
                    <cfelse>
                    	<cfswitch expression="#group.viewType#">
                            <cfcase value="1">
                            	Root View
                            </cfcase>
                            <cfcase value="2">
                            	List View
                            </cfcase>
                            <cfcase value="3">
                            	Unit View
                            </cfcase>
                             <cfcase value="4">
                            	Condo View
                            </cfcase>
                             <cfcase value="5">
                            	List Pop View
                            </cfcase>
                             <cfcase value="6">
                            	Design View
                            </cfcase>
                            <cfdefaultcase> 
                            	Default View Type
                            </cfdefaultcase>
                        </cfswitch>
                    	
                    </cfif>
                    </td>
                </tr>
                </table>
               </cfif> 
              </td>
              
              </tr>
              
			<tr>
             <td>&nbsp;</td>
             <td colspan="3">
             <!--- options start --->
             	<div id="themeOption" style="display: none">
             		Sample Options Go Here - Theme
				</div>
            	<div id="sectionOption" style="display: none">
             		Sample Options Go Here - Sections
				</div>
             <!--- options end --->
             </td>
             </tr>
          </table>
          </cfoutput>
          </td>
          <cfif editGroupName>

          <td width="44" align="right" valign="top">
          <input type="submit" value="" style="background:url(images/ok.png) no-repeat; border:0; width:44px; height:44px; cursor:pointer; margin-top: 8px;" />
          </td>
          </cfif>
         
          <cfoutput>
          <td align="right">
            <cfif editGroupName>
              <cfelse>
                  <a href="AppsView.cfm?subgroupID=#subgroupID#&editGroupName=true"><img src="images/edit.png" width="44" height="44" /></a>
                  <a href="createGroupAsset.cfm?subgroupID=#session.subgroupID#"><img src="images/groups.png" width="44" height="44" /></a>
              </cfif>
          </td>
          </tr>
		  <tr>
		  <td class="contentLinkWhite" style="padding-left:55px; padding-right:10px; color:##AAA">
          
          <table width="100%%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 10px;">
          <tbody>
            <tr>
              <td>
              <cfif revType GT 0>          
				  <cfset icon = reservedTerms[revType].icon>
                  <img src="images/#icon#.png" width="22" height="18" />
              </cfif>
              </td>
              <td>#revDesc#</td>
            </tr>
          </tbody>
        </table>
          
          </td>
          </tr>
		  </cfoutput>
        
        
          <cfif editGroupName>
          
              <cfif subgroupID IS '0'><cfset subgroupID = '-1'></cfif>
              
              <!--- Get Group Info --->
              <cfinvoke component="CFC.Modules" method="getGroupDetails" returnvariable="assets">
                  <cfinvokeargument name="groupID" value="#subgroupID#"/>
              </cfinvoke>
   
              <!--- Get All Module Data - Thumbs and Details like Assets --->
              <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                  <cfinvokeargument name="appID" value="#assets.app_ID#"/>
                  <cfinvokeargument name="images" value="yes"/>
                  <cfinvokeargument name="absolute" value="yes"/>
              </cfinvoke> 
			  
              <cfif assets.thumbnail NEQ ''>
              	<cfset theThumbnailImage = 'http://liveplatform.net/#assetPath#thumbs/#assets.thumbnail#'>
  		      <cfelse>
              	<cfset theThumbnailImage = ''>
              </cfif>
              
             <tr>
              <td height="44" colspan="2">
                  <cfinclude template="thumbsDetails.cfm">
              </td>
             </tr>
  
          </cfif>
        
        
        
      </table>
      
      </td>
    </tr>
  </table>
</form>
   
</div>

<!--- Show if no details for group is displayed --->
<cfif editGroupName>
<cfelse>  
    
<table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="#CCC">
  <tr>
    <td height="32" class="subheading"></td>
    <td width="44" align="center" class="subheading">Access</td>
    <td width="44" align="center" class="subheading">Cache</td>
    <td width="44" align="center" class="subheading">Active</td>
    <td width="44" align="center" class="subheading">Sharable</td>
    <td width="44" align="center" class="subheading">Order</td>
    <td width="44" align="right" class="subheading"></td>
    <td width="44" align="center" class="subheading">
    <cfoutput>
	<cfif show IS ""><cfset show=1><cfelseif show IS 1><cfset show=0><cfelse><cfset show=""></cfif>
    <a href="AppsView.cfm?subgroupID=#subgroupID#&show=#show#" class="content">
    </cfoutput>
    
    <cfif show IS "">
    INACTIVE
    <cfelseif show IS 1>
    ALL
    <cfelse>
    ACTIVE
	</cfif>
	</a>
    </td>
  </tr>
</table>

    <cfinvoke  component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />
    <cfset typeAlt = false>
	<cfloop index="z" from="1" to="#arrayLen(contents)#">
    	
      <cfset content = contents[z]>
        
	  <cfif (z MOD 2)>
          <cfset colColor = "FFF">
        <cfelse>
          <cfset colColor = "EEE">
      </cfif>
        
      <cfset dateCre = DateAdd("s", content.created ,DateConvert("utc2Local", "January 1 1970 00:00"))>
      <cfset dateMod = DateAdd("s", content.modified ,DateConvert("utc2Local", "January 1 1970 00:00"))>

      <!--- Get Asset Name --->
      <cfif content.asset_id GT 0>
          <cfinvoke component="CFC.Assets" method="getAssetName" returnvariable="assetInfo">
            <cfinvokeargument name="assetID" value="#content.asset_id#"/>
          </cfinvoke>
      </cfif>
	  
      <cfif content.active IS ''>
      	<cfset content.active = -1>
      </cfif>
      
        <cfoutput>
        <cfif content.active>
        	<cfset state = "contentLink">
        <cfelse>
        	<cfset state = "contentLinkDisabled">
        </cfif>
        
        
        <cfif structKeyExists(content,'reservedType')>
           <!--- <cfif content.reservedType IS 11> --->
				<div>
				<cfif groupType NEQ content.reservedType>
					<hr size="1" width="795" style="margin-left:15px">
					<cfset groupType = content.reservedType>
				</cfif>

				<cfinvoke component="CFC.Modules" method="getCMSAppGroups" returnvariable="groups">
					<cfinvokeargument name="appID" value="#session.appID#"/>
				</cfinvoke>
				
				</div>
			<!--- </cfif> --->
		</cfif>
        
   <!---      <cfif structKeyExists(content,'reservedType') AND NOT typeAlt>
        	<cfset typeAlt = true>
            <div>
            <hr size="4" width="795" style="margin-left:15px">
            </div>
		</cfif> --->
        
             <!--- All Assets --->  
             <div class="rowhighlighter" onmouseover="showDelete(this,1)" onmouseout="showDelete(this,0)">
            
                  <table width="810" border="0" cellpadding="5" cellspacing="0">
                    <tr>
                      <td width="44">
                      <cfif content.asset_id GT '0'>
                        <img src="images/#assetInfo.icon#" width="44" height="44" />
                      <cfelseif content.subgroup_id GT '0'>
                        <img src="images/groups.png" width="44" height="44" />
                      <cfelse>
                        <img src="images/modules.png" width="44" height="44" />
                      </cfif>
                      </td>
                      <td colspan="2">
                      <cfif content.asset_id GT '0'>
                      <a href="AssetsView.cfm?assetID=#content.asset_id#&amp;assetTypeID=#assetInfo.assetType_id#" class="#state#">
                      <div id="contentAsset_#z#" style="height:32px; padding-top:15px; padding-left:4px">#assetInfo.name#</div>
                      </a>
                      <cfelse>
                      <a href="AppsView.cfm?subgroupID=#content.group_id#" class="#state#">
                      <div id="contentAsset_#z#" style="height:32px; padding-top:15px; padding-left:4px">#content.name#</div>
                      </a>
                      </cfif>
                      </td>
                      </td>
                      <td width="40" align="right" class="content">
                      <cfif content.asset_id NEQ 0>
                      
						  <cfif structKeyExists(content,'reservedType')>
                          
                              <cfset icon = reservedTerms[content.reservedType].icon>
                              <img src="images/#icon#.png" width="22" height="18" />
    
                          </cfif>
                      
                      </cfif>
                      
                      </td>
                      <cfif content.asset_id GT '0'>
                      <td width="100" align="right" class="content">
                      <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="assetDate">
                        <cfinvokeargument name="TheEpoch" value="#content.modified#"/>
                      </cfinvoke>
                      <cfif LSDateFormat(assetDate,'MM/DD') IS LSDateFormat(NOW(),'MM/DD')>
						TODAY - #TimeFormat(assetDate,'HH:MM')#
                      <cfelse>
                      	#DateFormat(assetDate,'MMM DD')#
                      </cfif>
                      
                      </cfif>
                      <cfif assetPathsCur.application.productID IS 5 AND content.subgroup_id IS 0>
                      <td width="40" align="right" class="content">                    
                      <img src="images/replace.png" width="44" height="44" onclick="refreshProgramData(#content.group_id#,'#content.name#')" />
                      </td>
                      </cfif>
                      <td width="44" align="right" class="content">
                      
                      <cfif content.access IS 0>
							<cfset lock = 'access_unlocked'>
                        <cfelse>
                        	<cfset lock = 'access_locked-'& content.access>
                        </cfif>
                        <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateAssetAccess(this,#content.group_id#)" />
                            <cfloop query="accessLevels">
                            <option value="#accessLevel#" style="color:##333" <cfif accessLevel IS content.access>selected="selected"</cfif>>#levelName#</option>
                            </cfloop>
                        </select>
                      </td>
                      <td width="44" align="center" class="content">
                      
                      <cfif content.asset_id IS ''><cfset content.asset_id = 0></cfif>
                      <cfif content.group_id IS ''><cfset content.group_id = 0></cfif>
                      
                      <div id="contentCached" onclick="toggleCachedState(this,#content.group_id#,#content.asset_id#)" style="cursor:pointer">
                      <cfif content.cached IS '0'>
                      <cfset theCacheState = 'cache'>
                      <cfelse>
                      <cfset theCacheState = 'cached'>
                      </cfif>
                      <img src="images/#theCacheState#.png" name="cachedState" id="cachedState" />
                      </div>                    </td>
                      <td width="44" align="center" class="content">
                      
                      <cfif content.asset_id IS ''><cfset content.asset_id = 0></cfif>
                      <cfif content.group_id IS ''><cfset content.group_id = 0></cfif>
                      <cfif content.active IS '0'>
                      	<cfset stateCSS = "contentLinkGreen">
                      <cfelse>
                      	<cfset stateCSS = "contentLinkRed">
                      </cfif>
					  <div id="contentActive" style="cursor:pointer" onclick="toggleActiveState(#z#,#content.group_id#,#content.asset_id#)">
                      <a id="activeState_#z#" class="#stateCSS#">
                      <cfif content.active IS '0'>
                      NO
                      <cfelse>
                      YES
                      </cfif>
                      </a>
                      </div>
                      
                      </td>
                      <td width="44" align="center" class="content">
                      
                      <cfif content.asset_id IS ''><cfset content.asset_id = 0></cfif>
                      <cfif content.group_id IS ''><cfset content.group_id = 0></cfif>
                      <cfif content.sharable IS '0'>
                      	<cfset stateCSS = "contentLinkGreen">
                      <cfelse>
                      	<cfset stateCSS = "contentLinkRed">
                      </cfif>
					  <div id="contentShare" style="cursor:pointer" onclick="toggleShareState('#z#',#content.group_id#,#content.asset_id#)">
                      <span id="shareState_#z#" class="#stateCSS#" />
                      <cfif content.sharable IS '0'>
                      NO
                      <cfelse>
                      YES
                      </cfif>
                      </a>
                      </div>
                      
                      </td>
                      <td width="44" align="center" class="contentLinkDisabled">
                      
                      <select name="sortOrder" id="sortOrder" style="width:36px; text-align:center; text-indent:0; height:32px;" type="button" onchange="setSortOrder(#content.group_id#,#content.asset_id#,this.options[this.selectedIndex].value)" />
                      <option value="0" <cfif content.order IS 0> selected</cfif>>0</option>
                          <cfloop index="z" from="1" to="#arrayLen(contents)#">
                            <option value="#z#" <cfif content.order IS z> selected</cfif>>#z#</option>
                          </cfloop>
                      </select>
                      
                      <!--- <input name="sortOrder_#z#" type="text" id="sortOrder_#z#" value="#content.order#" size="4" maxlength="10" onchange="setSortOrder(#content.group_id#,#content.asset_id#,this.value)" /> --->
                      
                      </td>
                      <td width="60" align="right" class="content">

                      
                      <table border="0">
                      <tr>
                        <td>
                        <cfif content.asset_id GT '0'>
                        <select class="itemHide" id="moveAsset" style="background:url(images/moveItem.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onChange="moveAsset(#content.asset_id#,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);" />
                        <option value="0" style="color:##333">Root</option>
						<cfloop query="groups">
                          <option value="#group_id#" style="color:##333">#name#</option>
                        </cfloop>
                        </select>
                      <cfelse>
                      <select class="itemHide" id="moveAsset" style="background:url(images/moveItem.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="moveGroup(#content.group_id#,this.options[this.selectedIndex].value,this.options[this.selectedIndex].text);" />
                      <option value="0" style="color:##333">Root</option>
						<cfloop query="groups">
                          <option value="#group_id#" style="color:##333">#name#</option>
                        </cfloop>
                        </select>                   
                      </cfif>
                        </td>
                        <td>
                        <cfif content.asset_id GT '0'>
                        <input type="button" class="itemHide" id="deleteAsset" style="background:url(images/remove.png) no-repeat; border:0; width:44px; height:44px;" onClick="removeAsset('#content.asset_id#');" value="" /> 
                      <cfelse>
                      <input type="button" class="itemHide" id="deleteAsset" style="background:url(images/remove.png) no-repeat; border:0; width:44px; height:44px;" onClick="removeGroup('#content.group_id#');" value="" />
                      </cfif>
                        </td>
                      </tr>
                    </table>
                      
                      
                      </td>
                    </tr>
                  </table>
                  
             </div> 

		</cfoutput>
      	
  </cfloop>
    
</cfif>


     

   


