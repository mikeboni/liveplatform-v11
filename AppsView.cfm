﻿<cfparam name="appID" default="">
<cfparam name="error" default="">
<cfparam name="edit" default="false">
<cfparam name="editP" default="false">
<cfparam name="tab" default="0">
<cfparam name="assetTypeTab" default="0">

<cfparam name="userDev" default="1">

<cfparam name="assetID" default="0">
<cfparam name="assetTypeID" default="0">

<cfparam name="search" default="">
<cfparam name="import" default="false">

<!--- <cfif NOT IsDefined("session.userDev")><cfset session.userDev = 1></cfif> --->

<cfif NOT IsDefined("session.appID")><cfset session.appID = '0'></cfif>
<cfif appID NEQ ''><cfset session.appID = appID></cfif>

<cfif NOT IsDefined("session.assetID")><cfset session.assetID = '0'></cfif>
<!--- <cfif assetID NEQ ''><cfset session.assetID = assetID><cfelse><cfset session.assetID = 0></cfif> --->

<cfif NOT IsDefined("session.subgroupid")><cfset session.subgroupid = '0'></cfif>
<!--- <cfif subgroupid NEQ ''><cfset session.subgroupid = subgroupid><cfelse><cfset session.subgroupid = 0></cfif> --->

<cfif NOT IsDefined("session.clientID")><cflocation url="ClentsView.cfm" addtoken="no"></cfif>

<cfinvoke  component="CFC.Apps" method="getClientApps" returnvariable="apps">
  <cfinvokeargument name="clientID" value="#session.clientID#"/>
</cfinvoke>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Live-Applications</title>
<link href="styles.css" rel="stylesheet" type="text/css" />

<link href="JS/mcColorPicker/mcColorPicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="JS/mcColorPicker/mcColorPicker.js"></script>
<script type="text/javascript" src="JS/classSelector.js"></script>

<!--- LiveAPI --->
<cfajaxproxy cfc="CFC.Apps" jsclassname="updateSupportLink">

<!--- LiveAPI AppOptions --->
<cfajaxproxy cfc="CFC.Options" jsclassname="appOptionsSettings">

<!--- LiveAPI EMail --->
<cfajaxproxy cfc="CFC.EMail" jsclassname="appEMailSettings">

<!--- LiveAPI Content --->
<cfajaxproxy cfc="CFC.Modules" jsclassname="contentAccess"> 

<!--- LiveAPI Markers --->
<!--- <cfajaxproxy cfc="CFC.Markers" jsclassname="contentMarkers"> --->

<!--- Active State and Order--->
<cfajaxproxy cfc="CFC.Modules" jsclassname="setMyActiveState">

<script type="text/javascript">

//App
function updateApp(form)	{
	if(form==0)
	{
		//new app
		document.forms['newApp'].submit();
			
	}else{
		//update app
		document.forms['updateApp'].submit();
		
	}
}

//Prefs
function updatePrefs()	{
		document.forms['updatePrefs'].submit();
}		

function createForm() { 
	document.getElementById("formContainer").innerHTML = '<input type="text" id="cinput2" class="color" value="#00aa00" />'; 
	MC.ColorPicker.reload();
}

</script>


</head>

<body>

<cfinvoke  component="CFC.Apps" method="getClientApps" returnvariable="apps">
  <cfinvokeargument name="clientID" value="#session.clientID#"/>
</cfinvoke>


<!--- <cfdump var="#appID#"><cfdump var="#session.appID#"> --->
<!--- Main Header --->
<cfinclude template="Assets/header.cfm">

<!--- <cfif NOT IsDefined("session.assetPaths")><cflocation url="CientsView.cfm" addtoken="no"></cfif> --->

<cfset active = assetPathsCur.application.active>
<cfset iconPath = assetPathsCur.application.icon>
<cfset bundleID = assetPathsCur.application.bundleID>
<cfset appName = assetPathsCur.application.name>
<cfset appPath = assetPathsCur.application.path>
<cfset version = assetPathsCur.application.version>
<cfset forced = assetPathsCur.application.forced>
<cfset support = assetPathsCur.application.support>
<cfset clientEmail = assetPathsCur.application.clientEmail>
<cfset download = assetPathsCur.application.download>
<cfset productID = assetPathsCur.application.productID>


<!--- AGO UPdated --->
<cfinvoke  component="LiveAPI" method="getJSONDate" returnvariable="data">
	<cfinvokeargument name="bundleID" value="#bundleID#"/>
	<cfinvokeargument name="version" value="11"/>
	<cfinvokeargument name="override" value="true"/>
</cfinvoke>

<cfif active IS ''><cfset active = false></cfif>

    <div style="width:800px; background-color:##333; height:32px; padding-left:0px; padding-top:4px; margin-bottom:10px; height:80px; float:none">
		
	<cfoutput>
      
      <div>
      
        <cfif active>
        
          <img src="#iconPath#" width="68" height="68" style="float:left; padding-right:10px;padding-top: 10px;" />
          
        <cfelse>
        
        	<div style="width:68px; height:68px; float: left; background-image:url(#iconPath#); background-repeat: no-repeat; background-size:contain; background-position: 50% 50%; margin-right:10px">
            <img src="images/inactive-app.png" width="68" height="68" style="float:left; padding-right:10px" />
            </div>
            
        </cfif> 
         
        	<div class="bannerHeading" style="padding-top:0px; width:auto">#appName#<br /><span class="bundleid"> (#bundleID#)</span></div>
        	<div class="content"><span class="contentHilighted">#appPath#</span></div>
            <div class="formText" style="text-align: right; text-transform: uppercase; font-size:8pt">
			 Updated - Prod: #data.prod# , Dev: #data.dev#
			</div> 
      </div>

      <cfset editState = NOT edit>
      
      <div class="optionButtons">
          <a href="AppsView.cfm?edit=#editState#"><img src="images/info.png" width="44" height="44" alt="users" /></a>
      </div>
          
          
      <cfset editPrefs = NOT editP>
      
      <div class="optionButtons">
          <a href="AppsView.cfm?editP=#editPrefs#"><img src="images/settings.png" width="44" height="44" alt="users" /></a>
      </div>
      
        <!--- <div class="optionButtons">
          <a href="usersView.cfm"><img src="images/users.png" width="44" height="44" alt="users" /></a>
      </div>   ---> 
		  
    </cfoutput>
    
    </div>
<cfif edit>
	
	<!---Edit Application Info--->

	<div style="width:810px; background-color:#FFF;">

	<div style="background-color:#069; margin-top:5px; height:36px; padding-left:10px; padding-top:10px; padding-right:10px; width:790px">
  
	<div style="width:620px;float:left" class="sectionLink">
	<a href="AppsView.cfm?edit=true&amp;tab=0" class="<cfif tab IS '0'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Application</a> | <a href="AppsView.cfm?edit=true&amp;tab=1" class="<cfif tab IS '1'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Update Links</a> | <a href="AppsView.cfm?edit=true&amp;tab=2" class="<cfif tab IS '2'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Options</a> | <a href="AppsView.cfm?edit=true&tab=3" class="<cfif tab IS '3'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Bindings</a> | <a href="AppsView.cfm?edit=true&tab=5" class="<cfif tab IS '5'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Units</a> | <a href="AppsView.cfm?edit=true&tab=6" class="<cfif tab IS '6'>sectionLinkHighlighted<cfelse>sectionLink</cfif>"> Override</a></div>

	</div>   

	<!--- <cfoutput query="theApp"> --->
          
	 <cfif tab IS '0'>
     	<!--- Settings - App Prefs --->
        <cfinvoke  component="CFC.Apps" method="getAppProducts" returnvariable="products" />
        <cfinclude template="Assets/appPrefs.cfm">
     </cfif>
            
     <cfif tab IS '1'>
     	<!--- Settings - App Links --->
     	<cfinclude template="Assets/appSettingsLinks.cfm">
     </cfif>
     
     <cfif tab IS '2'>
     	<!--- Settings - App Links --->
     	<cfinclude template="Assets/appSettingsOptions.cfm">
     </cfif>
     
     <cfif tab IS '3'>
     	<!--- Settings - App Links --->
     	<cfinclude template="buildingAssets.cfm">
     </cfif>
     
     <cfif tab IS '5'>
     	<!--- Settings - App Links --->
     	<cfinclude template="unitAssets.cfm">
     </cfif>
     
     <cfif tab IS '6'>
     	<!--- Asset Overides --->
     	<cfinclude template="assetOverides.cfm">
     </cfif>
     
     <!--- <cfif tab IS '6'>
     	<!--- Settings - App Links --->
     	<cfinclude template="Assets/appSettingsAdmin.cfm">
     </cfif> --->
     
  <!--- </cfoutput> --->
  
 	</div>
 
</cfif>

<cfif editP>

    <cfinvoke  component="CFC.Apps" method="getAppPrefs" returnvariable="prefs">
        <cfinvokeargument name="appID" value="#session.appID#"/>
    </cfinvoke>
  
    <!---Edit Preferences--->

	<div style="background-color:#EEE; width:810px">
        
	<div style="background-color:#069; margin-top:5px; height:36px; padding-left:10px; padding-top:10px; padding-right:10px; width:790px">
	<div style="width:400px;float:left" class="sectionLink">
    	<a href="AppsView.cfm?editP=true&amp;tab=0" class="<cfif tab IS '0'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Preferences</a> | <a href="AppsView.cfm?editP=true&amp;tab=1" class="<cfif tab IS '1'>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Support Links</a>
    </div>
	</div>
            
    <cfif tab IS '0'>
    	  <!--- Settings - General --->
          <cfinclude template="Assets/appSettingsGeneral.cfm">
          
    <cfelseif tab IS '1'>
        	<!--- Settings - Support Links --->
          <cfinclude template="Assets/appSettingsSupportLinks.cfm">
	</cfif>

   </div>
   
 <cfelse>   

	<cfif NOT edit>  
      
		<!---Groups List--->
        <div style="background-color:#666; padding-left:5px; margin-top:5px; height:46px; width:805px" class="sectionLink">
    
    <table width="800" border="0" style="position: absolute;">
      <tr>
        <td height="44">
        <a href="AppsView.cfm?assetTypeTab=0" class="<cfif assetTypeTab IS 0>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Application Content</a> | 
    <a href="AppsView.cfm?assetTypeTab=3" class="<cfif assetTypeTab IS 3>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Assets Listing</a> | <a href="AppsView.cfm?assetTypeTab=4" class="<cfif assetTypeTab IS 4>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Asset Groups</a>
        | <a href="AppsView.cfm?assetTypeTab=5" class="<cfif assetTypeTab IS 5>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Batch</a>
        | <a href="AppsView.cfm?assetTypeTab=6" class="<cfif assetTypeTab IS 6>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Move </a>
        | <a href="AppsView.cfm?assetTypeTab=7" class="<cfif assetTypeTab IS 7>sectionLinkHighlighted<cfelse>sectionLink</cfif>">RSVP </a>
        <cfif assetTypeTab IS 4 OR  assetTypeTab IS 3>
        | <cfoutput>
        <select name="appid" id="appid" style="background:url(images/appSel.png) no-repeat; border:0; width:44px; height:45px;color:rgba(0,0,0,0);" type="button" onchange="" />
            <cfloop query="apps">
            <option value="app_id" style="color:##333" <cfif app_id IS session.appID>selected="selected"</cfif>>#appName#</option>
            </cfloop>
        </select>
        </cfoutput>
        </cfif>
        </td>
        <td align="right" class="function" style="color:#999">
        <cfif assetTypeTab IS 3>
        <table border="0">
          <tr>
            <td class="content" style="color:white">Search</td>
            <td>
            <cfoutput>
            <form action="AppsView.cfm?assetTypeTab=3&assetTypeID=#assetTypeID#" method="post" name="searchAssets">
            <input name="search" type="text" id="search" value="#search#" style="width:80px" />
			</form>
            </cfoutput>
            </td>
          </tr>
        </table>
        </cfif>
        </td>
      </tr>
    </table>

      </div>
        
        <cfif assetTypeTab LT 3>
        
       	   <!--- nothing --->
    
          <cfif assetTypeTab IS 0>
          
            	<!--- Contents --->
            	<cfinclude template="groupListing.cfm"> 
                
         </cfif>
         
         <cfelseif assetTypeTab IS 4>
         	   <!--- new asset listing with group --->
			   <cfinclude template="groupAssetListing.cfm">

        <cfelseif assetTypeTab IS 5>
         	   <!--- batch import --->
			   <cfinclude template="batchImport.cfm">

        <cfelseif assetTypeTab IS 6>
         	   <!--- batch import --->
			   <cfinclude template="moveProject.cfm">
        
        <cfelseif assetTypeTab IS 7>
         	   <!--- batch import --->
			   <cfinclude template="rsvp.cfm">

        <cfelse>
          
			  <!--- Asset Listing --->
              <cfinclude template="assetListing.cfm">
            
        </cfif>
		
      
  </cfif>

</cfif>

</body>
</html>