<cfcomponent>
	
	<!--- Get ASSET DATA --->
	<cffunction name="appIsValid" access="remote" returntype="string" returnformat="plain" output="no" hint="Get Asset Data">
	   <cfargument name="bundleID" type="string" required="yes">

		<cfinvoke  component="CFC.Apps" method="getAppID" returnvariable="app">
			<cfinvokeargument name="bundleID" value="#bundleID#"/>
		</cfinvoke>

		<cfif app.error.error_code IS 1000>      
			<cfreturn true>
		 <cfelse>
		 	<cfreturn false>
		 </cfif>
		
		<cfset JSON = serializeJSON(assetData)>
		<cfreturn JSON>

	</cffunction>
	
	
	<!--- Get ASSET DATA --->
	<cffunction name="getAsset" access="remote" returntype="string" returnformat="plain" output="no" hint="Get Asset Data">
	   <cfargument name="assetID" type="numeric" required="yes">

		<cfinvoke  component="CFC.Content" method="getAssetContent" returnvariable="assetData">
			<cfinvokeargument name="assetID" value="#assetID#"/>
		</cfinvoke>

		<cfset JSON = serializeJSON(assetData)>
		<cfreturn JSON>

	</cffunction>
	
	
	<!--- Get MAP ASSET IDS --->
	<cffunction name="getMappedAssets" access="remote" returntype="string" returnformat="plain" output="no" hint="Get Asset Data">
	   <cfargument name="bundleID" type="string" required="yes">
	
		 <cfinvoke component="liveAssets" method="getAppID" returnvariable="appID">
		  <cfinvokeargument name="bundleID" value="#bundleID#"/>
		</cfinvoke>
		 
		 <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
			<cfinvokeargument name="error_code" value="1000"/>
		</cfinvoke>


		<cfinvoke component="CFC.Assets" method="getAllAssets" returnvariable="assets">
		  <cfinvokeargument name="appID" value="#appID#"/>
		  <cfinvokeargument name="assetType" value="1"/>
		</cfinvoke>

		<cfset mappedAssets = {}>

		<cfloop query="#assets#">

			<cfinvoke component="CFC.GroupedAssets" method="AssetsActionExists" returnvariable="assetsAttached">
				<cfinvokeargument name="assetID" value="#asset_id#"/>
			</cfinvoke>

			<cfif assetsAttached>
				<cfset structAppend(mappedAssets, {#asset_id#: name})>
			</cfif>

		</cfloop>
		
		<cfset structDelete(mappedAssets, 'error_code')>
		<cfset structDelete(mappedAssets, 'error_message')>

		<cfset JSON = serializeJSON(mappedAssets)>
		<cfreturn JSON>

	</cffunction>
	
	
		<!--- Create All (MULTIPLE) PINs and Attach to Map Image --->
	<cffunction name="syncAllPins" access="remote" returntype="string" returnformat="plain" output="no" hint="Sync Asset Data">
	   <cfargument name="bundleID" type="string" required="yes">
	   <cfargument name="imageAssetID" type="numeric" required="no" default="0">
	   <cfargument name="data" type="sturct" required="no" default="">
	   
	   <cfset data = deserializeJSON(data)>
	   
	   <cfset assetData = {}>
	   
	   <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
			<cfinvokeargument name="error_code" value="1000"/>
		</cfinvoke>
   
	   <cfloop array="#data#" index="pin">
   		

	   		<!--- Create PIN (SINGLE) and Attach to Map Image --->
		   <cfinvoke component="liveAssets" method="setPin" returnvariable="createdPin">
			  <cfinvokeargument name="bundleID" value="#bundleID#"/>
			  <cfinvokeargument name="imageAssetID" value="#imageAssetID#"/>
			  <cfinvokeargument name="data" value="#pin#"/>
			</cfinvoke>
			
			<cfset structAppend(assetData, {"assetID": createdPin, "name":pin.name})>
			
		</cfloop>
		
		<cfset JSON = serializeJSON(assetData)> 
		
		<cfsavecontent variable="strText">
			<cfoutput>#JSON#</cfoutput>
		</cfsavecontent>
		
		<cfcontent type="text/plain" reset="true" variable="#ToBinary(ToBase64( strText ))#"></cfcontent>

	</cffunction>
	
	
	<!--- Create PIN (SINGLE) and Attach to Map Image --->
	<cffunction name="setPin" access="remote" returntype="numeric" output="no" hint="Get Asset Data">
	   <cfargument name="bundleID" type="string" required="yes">
	   <cfargument name="imageAssetID" type="numeric" required="no" default="0">
	   <cfargument name="data" type="struct" required="no" default="#structNew()#">
	   
	    <cfset assetData = {}>
	    
	    <cfinvoke component="liveAssets" method="getAppID" returnvariable="appID">
		  <cfinvokeargument name="bundleID" value="#bundleID#"/>
		</cfinvoke>
		
		<cfset XYdata = {"x":0, "y":0, "alignH":0, "alignV":0, "name":'untitled', "assetID":0}>
		
		<cfif structKeyExists(data,"x")><cfset XYdata.x = data.x></cfif>
		<cfif structKeyExists(data,"y")><cfset XYdata.y = data.y></cfif>
		<cfif structKeyExists(data,"alignH")><cfset XYdata.alignH = data.alignH></cfif>
		<cfif structKeyExists(data,"alignV")><cfset XYdata.alignV = data.alignV></cfif>
		<cfif structKeyExists(data,"name")><cfset XYdata.name = data.name></cfif>
		<cfif structKeyExists(data,"assetID")><cfset XYdata.assetID = data.assetID></cfif>
		
		<cfset assetData = {"appID":appID, "assetTypeID":10, "name":"#XYdata.name#", "assetID": XYdata.assetID, "x":XYdata.x, "y":XYdata.y, "alignH":XYdata.alignH, "alignV":XYdata.alignV}>
		<cfset thumbData = {'thumb':{'file':""}, 'thumbnail':""}>
		<cfset colorData = {}>
		
		<cfif structKeyExists(data, 'details')>
				
				<cfset detailData = {"title": "", "subtitle": "", "description": "", "other": "", "titleColor":"", "subtitleColor":"", "descriptionColor":""}>
				
				<cfif structKeyExists(data.details, 'title')>
					<cfset detailData.title = data.details.title>
				</cfif>
				<cfif structKeyExists(data.details, 'subtitle')>
					<cfset detailData.subtitle = data.details.subtitle>
				</cfif>
				<cfif structKeyExists(data.details, 'description')>
					<cfset detailData.description = data.details.description>
				</cfif>
				<cfif structKeyExists(data.details, 'other')>
					<cfset detailData.other = data.details.other>
				</cfif>
				
		<cfelse>
			<cfset detailData = {}>
		</cfif>
		
		<cfif structKeyExists(data, "color")>
		
			<cfset colorData = {"backColor":"", "foreColor":"", "otherColor":"", "displayAssetID":0}>
			 <cfif structKeyExists(data.color, "backColor")>
				<cfset colorData.backColor = data.color.backColor>
			</cfif>
		   <cfif structKeyExists(data.color, 'foreColor')>
				<cfset colorData.foreColor = data.color.foreColor>
			</cfif>
		   <cfif structKeyExists(data.color, 'otherColor')>
				<cfset colorData.otherColor = data.color.otherColor>
			</cfif>
		<cfelse>
			<cfset colorData = {}>	   
		</cfif>
		
		<cfinvoke component="CFC.Assets" method="updateAssetDetails" returnvariable="assetID">
			<cfinvokeargument name="assetData" value="#assetData#"/>
			<cfinvokeargument name="detailData" value="#detailData#"/>
			<cfinvokeargument name="thumbData" value="#thumbData#"/>
			<cfinvokeargument name="colorData" value="#colorData#"/>
			<cfinvokeargument name="groupID" value="-1"/>
			<cfinvokeargument name="silent" value="true"/>
		</cfinvoke>
	
		<!--- <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
			<cfinvokeargument name="error_code" value="1000"/>
		</cfinvoke> --->
		
		<cfif assetID IS 0>
			<!--- no such asset --->
			<!--- <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
				<cfinvokeargument name="error_code" value="6000"/>
			</cfinvoke>--->
		
			<!--- <cfset structAppend(assetData, error)>
			<cfset JSON = serializeJSON(assetData)>
			<cfreturn JSON>  --->
			
			<cfreturn 0>
			
		</cfif>
		
		<!--- if no imageAssetID to attach to, then return - updated asset --->
		<cfif imageAssetID IS 0>
		
			<!--- updated Asset --->
			<!--- <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
				<cfinvokeargument name="error_code" value="6001"/>
			</cfinvoke> --->

		<cfelse>
		
			<!--- Check if Asset is already attached --->
			<cfinvoke  component="CFC.Assets" method="groupAssetExists" returnvariable="assetExists">
				<cfinvokeargument name="groupAssetID" value="#imageAssetID#"/>
				<cfinvokeargument name="assetID" value="#assetID#"/>
			</cfinvoke>

			<cfif NOT assetExists>
				
				<cfset allAssetsToAttach = [assetID]>
				
				<!--- Attach Asset to Image Map --->
				<cfinvoke  component="CFC.Assets" method="setGroupAssets" returnvariable="assetAttached">
					<cfinvokeargument name="groupAssetID" value="#imageAssetID#"/>
					<cfinvokeargument name="assetIDs" value="#allAssetsToAttach#"/>
					<cfinvokeargument name="active" value="true"/>
				</cfinvoke>

			</cfif>
		
		</cfif>
			
		<!--- <cfset structAppend(assetData, error)> --->
		<!--- <cfset assetData = {"assetID":assetID}> --->

		<!--- <cfset JSON = serializeJSON(assetData)>
		<cfreturn JSON> --->
		
		<cfreturn assetID>

	</cffunction>
	
	
	<!--- Delete Assets --->
	<cffunction name="deletePin" access="remote" returntype="string" returnformat="plain" output="no" hint="Delete Asset Data">
	   <cfargument name="bundleID" type="string" required="yes">
	   <cfargument name="authToken" type="string" required="no"><!--- NEED TO DO --->
	   <cfargument name="assetIDs" type="array" required="yes">
	   
	   	<cfset assetData = {}>
	   	
	   	<!--- Check if authToken is Admin --->
	   	
		<cfinvoke component="liveAssets" method="getAppID" returnvariable="appID">
		  <cfinvokeargument name="bundleID" value="#bundleID#"/>
		</cfinvoke>
		
		<cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
			<cfinvokeargument name="error_code" value="1000"/>
		</cfinvoke>

		<cfinvoke component="CFC.GroupedAssets" method="deleteAssets" returnvariable="deletedAssets">
			<cfinvokeargument name="appID" value="#appID#"/>
			<cfinvokeargument name="assetIDs" value="#assetIDs#"/>
		</cfinvoke>
		
		<cfset structAppend(assetData, error)>
		<cfset structAppend(assetData,{"deleted":deletedAssets})>

		<cfset JSON = serializeJSON(assetData)>
		<cfreturn JSON>
		
	</cffunction>
	
	
	<!--- Get AppID From BundleID --->
	<cffunction name="getAppID" access="remote" returntype="numeric">
	   <cfargument name="bundleID" type="string" required="yes">
		
		<cfinvoke  component="CFC.Apps" method="getAppID" returnvariable="app">
			<cfinvokeargument name="bundleID" value="#bundleID#"/>
		</cfinvoke>

		 <cfset appID = 0>

		  <cfif app.error.error_code IS 1000>      
			<cfset appID = app.app_id>
		 <cfelse>
			<cfreturn app.error>
		 </cfif>
		 
		 <cfreturn appID>
	
	</cffunction>
	
</cfcomponent>




