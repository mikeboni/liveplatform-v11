<cfparam name="unitID" default="0">
<cfparam name="roomID" default="0">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<link href="styles.css" rel="stylesheet" type="text/css" />
</head>


<body>


<cfinvoke component="CFC.units" method="getUnit" returnvariable="unitData">
    <cfinvokeargument name="unitID" value="#unitID#"/>
</cfinvoke>

	<cfset config = unitData.data>
	
    <cfif unitData.created IS ''>
    	<cfset createdDate = 0>
    <cfelse>
    	<cfset createdDate = unitData.created>
    </cfif>
    
    <cfif unitData.modified IS ''>
    	<cfset modifiedDate = 0>
    <cfelse>
    	<cfset modifiedDate = unitData.modified>
    </cfif>
    
	<!--- convert epoch --->
     <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="dateCreated">
        <cfinvokeargument name="TheEpoch" value="#createdDate#">
    </cfinvoke>
    
    <!--- convert epoch --->
     <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="dateModified">
        <cfinvokeargument name="TheEpoch" value="#modifiedDate#">
    </cfinvoke>
    
    <!--- get base asset for room --->
    <cfinvoke  component="CFC.Content" method="getAssetContent" returnvariable="theBaseImage">
        <cfinvokeargument name="assetID" value="#roomID#"/>
        <cfinvokeargument name="includeActionsAssets" value="no"/>
    </cfinvoke>
    
    <!--- Get Root Key --->
    <cfloop collection="#theBaseImage#" item="theKey"></cfloop>
         
    <cfset baseImage = theBaseImage[theKey].image.url.xdpi>
    
    <cfset custSelection = structNew()>
    
	<cfloop collection="#config#" item="theKey">
     
        <cfset theSel = config[theKey].favouredConfigIndex + 1>
        <cfset theConfig = config[theKey].configs[theSel]>
        
        <cfset structAppend(custSelection,{#theKey#:theConfig})>
    
    </cfloop>
     
    <cfset theRoomContent = structFind(custSelection,roomID)>
    
    <cfinvoke component="CFC.Configurator" method="getSpace" returnvariable="theSpace">
        <cfinvokeargument name="assetID" value="#roomID#"/>
    </cfinvoke>

	<cfset materials = arrayNew(1)>
    
    <cfset cnt = 0>    
	<cfloop collection="#theRoomContent#" item="theOptionID">

		<cfset theAssetID = theRoomContent[theOptionID]>
 
        <cfinvoke component="CFC.Modules" method="getContentAsset" returnvariable="asset">
            <cfinvokeargument name="assetID" value="#theAssetID#"/>
        </cfinvoke> 


         <!--- Get Root Key --->
         <cfloop collection="#asset#" item="theKey"></cfloop>
   
        <cfset theImage = asset[theKey].image.url.xdpi>
        
        <cfset theMaterial = {"url":asset[theKey].thumb.xdpi.url, "name": asset[theKey].details.title}>
        
  <cfset arrayAppend(materials,theMaterial)>
        
  	<!--- create composite image --->
  	<cfimage source="#theImage#" name="compImage"><!--- baseImage --->
      
  	 <cfif cnt IS 0>
			<cfset finalImage = ImageNew("",compImage.width,compImage.height,"argb")>
            <cfset ImageDrawRect(finalImage,0,0,compImage.width,compImage.height,"yes")>
            
            <cfimage source="#baseImage#" name="anImage">
            <cfset ImageOverlay(finalImage,anImage)> 
        </cfif>
        
        <cfimage source="#theImage#" name="anImage">
        <cfset ImageOverlay(finalImage,anImage)>
         
        <cfset ImageSetDrawingColor(finalImage,"333333")> 
        <cfset ImageDrawRect(finalImage,0,0,compImage.width-1,compImage.height-1,"no")> 

         <cfset cnt++>
         
     </cfloop>
     
     
     <cfoutput>
     
     <table width="800" border="0" cellspacing="0" cellpadding="10">
     <tr>
     <td bgcolor="##CCC">
     <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td width="50%" height="32" class="contentLink">Modified: #dateFormat(dateModified,'DDD MMM, DD/YY')#</td>
         <td align="right" class="contentLinkDisabled">Created: #dateFormat(dateCreated,'DDD MMM, DD/YY')#</td>
       </tr>
     </table>
     </td>
     </tr>
      <tr>
        <td height="32" bgcolor="##EEE" class="bannerHeading"><table width="100%" border="0" cellspacing="0" cellpadding="2">
          <tr>
              <td rowspan="2">#theSpace.title#</td>
              <td align="right" class="contentLinkGrey">#unitData.project# - #unitData.unit#</td>
            </tr>
            <tr>
              <td align="right" class="contentLinkGrey">#unitData.unitName# -  #unitData.unitType#(#unitData.unitCode#)</td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td height="29" style="margin:5px"><cfimage source="#finalImage#" action="writeToBrowser"></td>
      </tr>
      <tr>
        <td bgcolor="##666" class="contentLinkWhite">Options Selected</td>
      </tr>
      <tr>
        <td>
        
        <cfloop index="theMat" array="#materials#">
        <div style="float:left; margin-right:10px">
        <table width="100" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="center"><img src="#theMat.url#" width="100" height="73" style="border:thin; border-color:##666; border-style:solid" /></td>
          </tr>
          <tr>
            <td align="center" class="content">#theMat.name#</td>
          </tr>
        </table>
        </div>
        </cfloop>
        
        </td>
      </tr>
    </table>

</cfoutput>

</body>
</html>