<cfset assetType = 0>

<cfif assets.displayAsset_id GT 0>
    <cfinvoke component="CFC.Assets" method="getAssetType" returnvariable="assetType">
            <cfinvokeargument name="assetID" value="#assets.displayAsset_id#"/>
    </cfinvoke>
</cfif> 


<cfinvoke component="CFC.CMS" method="getProgramDisplayAssets" returnvariable="displayAssets">
        <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke> 

<script type="text/javascript" src="JS/domtab.js"></script>

<!--- Date Picker --->
<script src="JS/pikaday.js"></script>
<script src="JS/moment.js"></script>

<link href="styles.css" rel="stylesheet" type="text/css" />
<link href="JS/domtab.css" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="JS/pikaday.css">

<cfinvoke component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />

<!--- Content Asset Viewer --->
<cfajaxproxy cfc="CFC.CMS" jsclassname="assetManager">

<!--- LiveAPI Assets --->
<cfajaxproxy cfc="CFC.Access" jsclassname="groupAccessManager">

<script type="text/javascript">

//server functions
var jsAssets = new assetManager();
var jsAccess = new groupAccessManager();

<cfif isDefined('subgroupID')>
function setCode(theForm, theItem) {
	
	var codeID = document.getElementById('accessCodeID_'+ theItem).value;
	var code = document.getElementById('code_'+ theItem).value;
	
	var gps = document.getElementById('gpsID_'+ theItem);
	var gpsID = gps.options[gps.selectedIndex].value;
	
	var initVisDate = document.getElementById('dateStart_'+ theItem).value;
	var expireVisDate = document.getElementById('dateEnd_'+ theItem).value;
	
	var initDate;
	var expireDate;
	
	if(initVisDate != ''){
		initDate = document.getElementById('startDate_'+ theItem).value;
	}else{
		initDate = null;
	}
	
	if(expireVisDate != ''){
		expireDate = document.getElementById('endDate_'+ theItem).value;
	}else{
		expireDate = null;
	}
	
	var theObj = document.getElementById('accessLevel_'+ theItem);
	var theLevel = theObj[theObj.selectedIndex].value;
	
	var expireLen = document.getElementById('expLength_'+ theItem).value;
	
	theObj = document.getElementById('saveCode_'+ theItem);
	theObj.style.background = "url('images/saveupdate.png')";
	
	jsAccess.setSyncMode();
	jsAccess.setCallbackHandler(setCodeSuccess);
	jsAccess.setGroupAccess(codeID, theItem, code, initDate, expireDate, expireLen, gpsID, theLevel);
	
}
</cfif>

function setCodeSuccess(theItem)
{
	theObj = document.getElementById('saveCode_'+ theItem);
	theObj.style.background = "url('images/replace.png')";
}

function updateAssetAccess(theItem) {
	
	theObj = document.getElementById('accessLevel_'+ theItem);
	
	theLevel = parseInt(theObj[theObj.selectedIndex].value);
	
	lock = 'access_locked-'+theLevel; 
	
	theObj.style.backgroundImage = 'url(images/'+ lock +'.png)';
	
}

function showDetails()	{
	
	checkboxObj = document.getElementById('useDetails');

	var theObj = document.querySelector("#detailView");
	
	if(checkboxObj.checked)
	{
		removeClass(theObj,"detailsHide");
		addClass(theObj, "detailsShow");
	}else{
		removeClass(theObj,"detailsShow");
		addClass(theObj, "detailsHide");
	}
}


//standard functions

function showDetails()	{
	
	checkboxObj = document.getElementById('useDetails');

	var theObj = document.querySelector("#detailView");
	
	if(checkboxObj.checked)
	{
		removeClass(theObj,"detailsHide");
		addClass(theObj, "detailsShow");
	}else{
		removeClass(theObj,"detailsShow");
		addClass(theObj, "detailsHide");
	}
}

function showThumb()	{
	
	checkboxObj = document.getElementById('useThumb');

	var theObj = document.querySelector("#thumbView");
	
	if(checkboxObj.checked)
	{
		removeClass(theObj,"detailsHide");
		addClass(theObj, "detailsShow");
	}else{
		removeClass(theObj,"detailsShow");
		addClass(theObj, "detailsHide");
	}
}

function showAssets()	{
	
	checkboxObj = document.getElementById('useAssets');

	var theObj = document.querySelector("#AssetView");
	
	if(checkboxObj.checked)
	{
		removeClass(theObj,"detailsHide");
		addClass(theObj, "detailsShow");
	}else{
		removeClass(theObj,"detailsShow");
		addClass(theObj, "detailsHide");
	}
}

function showTargets()	{
	
	checkboxObj = document.getElementById('useTargets');

	var theObj = document.querySelector("#TargetView");
	
	if(checkboxObj.checked)
	{
		removeClass(theObj,"detailsHide");
		addClass(theObj, "detailsShow");
	}else{
		removeClass(theObj,"detailsShow");
		addClass(theObj, "detailsHide");
	}
}

<cfif isDefined('subgroupID')>
function showAccess()	{
	
	checkboxObj = document.getElementById('useAccess');

	var theObj = document.querySelector("#AccessView");
	
	if(checkboxObj.checked)
	{
		removeClass(theObj,"detailsHide");
		addClass(theObj, "detailsShow");
	}else{
		
		if(confirm('Are you sure you wish to Remove Access?'))
		{
			jsAccess.setSyncMode();
			jsAccess.setCallbackHandler(removeAccessSuccess);
			jsAccess.clearGroupAccess(<cfoutput>#subgroupID#</cfoutput>);
		}else{ 
			checkboxObj.checked = 1;
		}
		
	}
}
</cfif>

function removeAccessSuccess(result) {
	
	var theObj = document.querySelector("#AccessView");
	
	removeClass(theObj,"detailsShow");
	addClass(theObj, "detailsHide");
	
	location.reload();
	
}

function showColors()	{
	
	checkboxObj = document.getElementById('useColors');

	var theObj = document.querySelector("#ColorsView");
	
	if(checkboxObj.checked)
	{
		removeClass(theObj,"detailsHide");
		addClass(theObj, "detailsShow");
	}else{
		removeClass(theObj,"detailsShow");
		addClass(theObj, "detailsHide");
	}
}

function displayImporterType(importObject)
{
	importerView = document.getElementById(importObject);

	if(importerView.style.display == "none")
	{
		importerView.style.display = "block";
		
	}else{
		importerView.style.display = "none";
	}
	
}

 
<!--- function importActions()	{
	 displayAssets(1);
 }
 
 function importAssets()	{
	 displayAssets(0);
 }
 
 function displayAssets(action)	{
	 document.location.href = "AppsView.cfm?assetTypeTab=4&assetID=<cfoutput>#assetID#</cfoutput>&action="+action;
 } --->
 
 
function setCheckmarkState(theObj)	{
	
	theContentListing = theObj;
	
	actionImporter = document.getElementById('ActionsImporter');
    assetImporter = document.getElementById('AssetsImporter');
	
	insideAssets = isDescendant(assetImporter, theContentListing);
	insideActions = isDescendant(actionImporter, theContentListing);
	
	//quiz
	quizImporter = document.getElementById('AssetsImporterQuiz');
    correctImporter = document.getElementById('AssetsImporterCorrect');
	IncorrectImporter = document.getElementById('AssetsImporterIncorrect');
	
	quizAssets = isDescendant(quizImporter, theContentListing);
	correctAssets = isDescendant(correctImporter, theContentListing);
	incorrectAssets = isDescendant(IncorrectImporter, theContentListing);
	
	if(insideAssets == true)
	{
		varObj = 'assets';
	}
	if(insideActions == true)
	{
		varObj = 'actions';
	}
	
	//Quiz
	if(quizAssets == true)
	{
		varObj = 'quiz';
	}
	if(correctAssets == true)
	{
		varObj = 'correct';
	}
	if(incorrectAssets == true)
	{
		varObj = 'incorrect';
	}

	//set checkmark	
	if(theObj.className == "checkmarkNormal")
	{
		theObj.className = "checkmarkSelected";
		setContentID(theObj.id,1,varObj);
	}else{
		theObj.className = "checkmarkNormal";
		setContentID(theObj.id,0,varObj);
	}

}

function updateContentAssetOptions(theObj,theAssetID) 
{
	theLevel = null;
	theCached=null;
	theActive=null;
	theShared=null;
	theOrder=null;
	
	if(theObj.id == 'contentActive')
	{
		theActive = toggleState(theObj);
		makeActiveLink(theObj,theActive);
	}
	
	if(theObj.id == 'contentShared')
	{
		theShared = toggleState(theObj);
	}
	
	if(theObj.id == 'contentCached')
	{
		theState = theObj.style.backgroundImage;
		src = theState.substring(4, theState.length-1);
		
		theSrc = src.replace( /^.+\// , '' );
		srcName = theSrc.replace(/"/g, "");
		
		if( srcName == "cache.png")
		{
			newSrc = "cached"
			theCached = 1
		}else{
			newSrc = "cache"
			theCached = 0;
		}
		
		theObj.style.backgroundImage = 'url(images/'+ newSrc +'.png)';
		
	}
	
	if(theObj.id == 'accessLevel')
	{
		theLevel = parseInt(theObj.options[theObj.selectedIndex].value);
		
		if(theLevel == 0)
		{
			lock = 'access_unlocked';  
		}else{
			lock = 'access_locked-'+theLevel; 
		}
		
		theObj.style.backgroundImage = 'url(images/'+ lock +'.png)';
	}
	
	if(theObj.id == 'sortOrder')
	{
		theOrder = parseInt(theObj.value);
	}
	
	theStruct = {'access':theLevel, 'cached':theCached, 'active':theActive, 'shared':theShared, 'order':theOrder};
	updateContentAsset(theAssetID,theStruct);
}

function makeActiveLink(theObj, theState)
{
	mainParentDiv = upTo(theObj,'div');
	
	theAsset = mainParentDiv.getElementsByTagName('a');
	
	if(theState)
	{
		theAsset[0].className = 'contentLink';
	}else{
		theAsset[0].className = 'contentLinkDisabled';
	}
	
}

function upTo(el, tagName) {
	
  tagName = tagName.toLowerCase();

  while (el && el.parentNode) {
    el = el.parentNode;
    if (el.tagName && el.tagName.toLowerCase() == tagName) {
      return el;
    }
  }
  return null;
}


function toggleState(theObj)
{
	if(theObj.className == 'contentLinkRed')
	{
		state = 0;
		theObj.className = 'contentLinkGreen';
		theObj.innerHTML = 'NO';
	}else{
		state = 1;
		theObj.className = 'contentLinkRed';
		theObj.innerHTML = 'YES';
	}
		
	return state;	
}

function showDelete(theObj, state)	{
	
	theObjRef = theObj.childNodes[1][2];
	
	if(state)
	{
		theObjRef.className = 'itemShow';
	}else{
		theObjRef.className = 'itemHide';
	}
	
}

function deleteContent(assetID) {
		if(confirm('Are you sure you wish to Delete this Remove Asset?'))
		{
			//nothing
		}else{ 
			//nothing
		}
}


function deleteColorBackground(groupID,assetID) {
	if(confirm('Are you sure you wish to Delete this Background from this Asset?'))
	{
		//delete background
		ok = jsAssets.removeBackgroundImage(groupID,assetID);
		if(ok){ location.reload(); }
	}else{ 
		//cancel
	}
}


function isDescendant(parent, child) {
     var node = child.parentNode;
     while (node != null) {
         if (node == parent) {
             return true;
         }
         node = node.parentNode;
     }
     return false;
}


function createAccess(groupID)	{
	
	if(groupID > 0)	{
		
		jsAccess.setSyncMode();
		jsAccess.setCallbackHandler(createAccessSuccess);
		jsAccess.createGroupAccess(groupID);
	
	}

}

function createAccessSuccess(result) {
	location.reload();
}

function deleteAccess(accessCodeID)	{
	
	if(accessCodeID > 0)	{
		
		if(confirm('Are you sure you wish to Delete Access?'))
		{
			jsAccess.setSyncMode();
			jsAccess.setCallbackHandler(deleteAccessSuccess);
			jsAccess.deleteGroupAccess(accessCodeID);
		}else{ 
			//nothing
		}
	
	}

}

function deleteAccessSuccess(result) {
	location.reload();
}

function selectOtherOption(theOption)	{
	
	var myOptions;
	
	if(theOption == 1)	{
		myOptions = {"isPortrait":0,"interactive":0,"audio":0,"isRandom":0,"repeat":0,"app":"performer","startTime":0,"endTime":0};	
	}else{
		myOptions = "";
	}
	
	if(myOptions == "")	{
		document.getElementById('other').value = "";
	}else{
		document.getElementById('other').value = JSON.stringify(myOptions);
	}
}


</script>

<script>
	
	function updateDisplayBKAssets(displayType, selectedAsset)	{
	
	var selectedAsset = selectedAsset || 0;
	var displayAssets = <cfoutput>#displayAssets#</cfoutput>;
	
	displayTypeSel = document.getElementById("displayType");
		
		if(displayType == 1) 	{
			//images
			allAssets = displayAssets.images;
			
		}else if(displayType == 2) {
			//videos
			allAssets = displayAssets.videos;

		}else{
			allAssets = {}
			document.getElementById("theDisplayVideoBKPreview").style.display = "none";
			document.getElementById("theDisplayBKPreview").style.display = "none";
		}
	
		//create options
		var i = 0;

		//remove options
		theSel = ClearOptionsFast("displayBKAssets");
		
		//none
		var opt = document.createElement('option');
		opt.value = 0;
		opt.innerHTML = 'None';
		
		theSel.appendChild(opt)
		
		for (var key in allAssets){
				
			var obj = allAssets[key];
			
			var opt = document.createElement('option');
			opt.value = key;
			opt.innerHTML = obj;
			
			if(selectedAsset == key) {
				opt.selected = true;
			}
			
			theSel.appendChild(opt)
		}
		
		viewDisplayBKAsset();
		
	}
	
	function ClearOptionsFast(id)
	{
		var selectObj = document.getElementById(id);
		var selectParentNode = selectObj.parentNode;
		var newSelectObj = selectObj.cloneNode(false); // Make a shallow copy
		selectParentNode.replaceChild(newSelectObj, selectObj);
		
		return newSelectObj;
	}
	
	
	function viewDisplayBKAsset()	{
		
		theSel = document.getElementById("displayBKAssets");
		
		img = document.getElementById("theDisplayBKPreview")
		vid = document.getElementById("theDisplayVideoBKPreview");
		
		img.style.display = "none";	
		vid.style.display = "none";	
		
		if(theSel.options.length > 0)	{
			displayAssetID = theSel.options[theSel.selectedIndex].value;
			if(displayAssetID > 0) {
				getDisplayBKAsset(displayAssetID);
				img.style.display = "block";	
			}else{
				img.style.display = "none";
				vid.style.display = "none";		
			}	
		}
		
	}
	
	//get asset call
	function getDisplayBKAsset(assetID)
	{
		jsAssets.setSyncMode();
		jsAssets.setCallbackHandler(updatedDisplayBKAsset);
		jsAssets.getDisplayAsset(assetID);
	}
	
	function updatedDisplayBKAsset(assetInfo)	{
		
		img = document.getElementById("theDisplayBKPreview")
		vid = document.getElementById("theDisplayVideoBKPreview")
		vidObj = document.getElementById("theVideoBK")
		
		img.style.display = "none";
		vid.style.display = "none";
		//console.log(assetInfo);
		img.src = "";
		vid.src = "";
		
		if(assetInfo.assetType == 1){
			//image
			img.src = assetInfo.url;
			img.style.display = "block";
		
		}else{
			//video
			theVideoBK.src = assetInfo.url;
			vid.style.display = "block";
		}
		
	}
		
</script>

<!--- Group Date Code Start and End --->
<cfif isDefined('subgroupID') AND editGroupName>
	
    <!--- get all access code for group --->
	<cfinvoke component="CFC.Access" method="getGroupAccessCodes" returnvariable="accessCodeEntries">
        <cfinvokeargument name="groupID" value="#subgroupID#"/>
    </cfinvoke>
    
</cfif>


<cfinvoke component="CFC.Assets" method="getGroupAssets" returnvariable="groupAssets">
        <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>


<cfinvoke component="CFC.Assets" method="getGroupAssets" returnvariable="groupActions">
        <cfinvokeargument name="assetID" value="#assetID#"/>
        <cfinvokeargument name="action" value="true"/>
</cfinvoke>

<cfset fixNonRetina = arrayNew(1)>

	<cfoutput>
    <table width="810" border="0" cellpadding="0" cellspacing="0" class="content" style="margin-top:22px">
      <tr>
        <td height="60" align="center" valign="middle" bgcolor="##EEE">
        <!--- Options --->
        <table width="100%" border="0" cellspacing="10">
          <tr>
          <!--- Thumbs --->
            <td width="150" valign="middle">
              <input name="useThumb" type="checkbox" id="useThumb" value="true" onchange="showThumb()" <cfif assets.thumbnail NEQ ''>checked</cfif> />
              <label for="useThumb"><span class="ui"></span> Thumb</label></td>
            <!--- Details --->
            <td width="150" valign="middle">
            <input name="useDetails" type="checkbox" id="useDetails" value="true" onchange="showDetails()" <cfif assets.detail_id GT '0'>checked</cfif> />
            <label for="useDetails"><span class="ui"></span> Details</label>
            </td>
            <!--- Colors --->
            <td width="150" align="left" valign="middle">
            <input name="useColors" type="checkbox" id="useColors" value="true" onchange="showColors()" <cfif assets.color_id GT '0'>checked</cfif> />
            <label for="useColors"><span class="ui"></span>Colors</label>
            </td>
            
            <cfif assetID GT 0>
				<!--- Assets --->
                <td width="150" align="left" valign="middle">
                <input name="useAssets" type="checkbox" id="useAssets" value="true" onchange="showAssets()" <cfif groupAssets.recordCount GT 0> checked</cfif> />
                <label for="useAssets"><span class="ui"></span>Assets</label>
                </td>
                <!--- Targets-Actions --->
                <td width="150" align="left" valign="middle">
                <input name="useTargets" type="checkbox" id="useTargets" value="true" onchange="showTargets()" <cfif groupActions.recordCount GT 0> checked</cfif> />
                <label for="useTargets"><span class="ui"></span>Actions</label>
                </td>
            </cfif>
            
            <cfif isDefined('subgroupID')>
            <cfif subgroupID GT 0>
				<!--- Access --->
                <td width="150" align="left" valign="middle">
                <input name="useAccess" type="checkbox" id="useAccess" value="true" onchange="showAccess()" <cfif accessCodeEntries.recordCount GT 0> checked</cfif> />
                <label for="useAccess"><span class="ui"></span>Access</label>
                </td>
			</cfif>
            </cfif>
          </tr>
        </table>
        
        </td>
      </tr>
    </table>
    </cfoutput>


<!--- Access --->
<cfif isDefined('subgroupID')>
<cfif subgroupID GT 0>
    <div style="width:810px" class="<cfif accessCodeEntries.recordCount IS 0>detailsHide<cfelse>detailsShow</cfif>" id="AccessView">
        <cfoutput>
        
      <div class="sectionLink" style="margin-left:0px; height:auto; width:100%">
        <div class="sectionLink" style="background-color:##666; height:32px; padding-left:10px; padding-right:-10px; padding-top:4px; padding-bottom:6px">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><span class="sectionLink" style="background-color:##666; height:32px; padding-left:10px; padding-right:-10px; padding-top:8px;">Access</span></td>
              <td width="44" align="left">
              <input name="newCode" type="button" class="itemShow" id="newCode" style="background:url(images/addAccess.png) no-repeat; border:0; width:32px; height:32px;" onclick="createAccess(#subgroupID#);" value="" />
              </td>
            </tr>
          </table>
        </div>
        
        
        <!--- get GPS LIst --->
        <cfinvoke component="CFC.Access" method="getGPSLocations" returnvariable="gpsLocations">
            <cfinvokeargument name="appID" value="#session.appID#"/>
        </cfinvoke>
        
        <cfset cnt = 0>
        
        <script type="text/javascript">
        
        var pickerStart = [];
        var pickerEnd = [];
        
          function getStartEndDate(theItem)
          {
              
              sDate = (pickerStart[theItem].getDate() + 1);
              eDate = (pickerEnd[theItem].getDate() + 1);
              
              if(eDate == null){ eDate = sDate; };
              
              st = 'startDate_'+theItem;
              en = 'endDate_'+theItem;
              le = 'expLength_'+theItem;
                 
              //document.getElementById(le).value = '';
              
              document.getElementById(st).value = sDate;
              document.getElementById(en).value = eDate;
        
          }
        </script>
            
        <cfloop query="accessCodeEntries">
        
    
        <cfif accessDate NEQ ''>
            
            <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="startDate">
                <cfinvokeargument name="TheEpoch" value="#accessDate#"/>
            </cfinvoke>
        <cfelse>
            <cfset startDate = "">
        </cfif>
        
        <cfif accessExpires NEQ ''>
            
            <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="endDate">
                <cfinvokeargument name="TheEpoch" value="#accessExpires#"/>
            </cfinvoke>
        <cfelse>
            <cfset endDate = "">
        </cfif>
    
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="content" bgcolor="##FFF" style="padding-right:5px; border-top:thin solid ##999">
            <tr>
              <td width="30" align="right" style="padding-right:5px;"><input name="accessCodeID" type="hidden" id="accessCodeID_#cnt#" value="#accessCode_id#" />Code</td>
              <td width="681" align="left">
              <!--- Access --->
              <table width="100%" border="0" cellspacing="0">
                <tr>
                  <td width="50">
                    <input name="code" type="text" class="formfieldcontent" id="code_#cnt#" value="#accessCode#" style="width:100px" maxlength="10" />
                  </td>
                  <td width="30" align="right" class="content" style="padding-right:5px;">GPS</td>
                  <td width="100">
                  <cfif gps_id IS ''><cfset gpsID = 0><cfelse><cfset gpsID = gps_id></cfif>
                  
                    <select name="gpsID" class="formfieldcontent" id="gpsID_#cnt#" style="width:100px; margin:0; padding:0">
                      <option value="0" selected="selected">None</option>
                      <cfloop query="gpsLocations">
                      <option value="#asset_ID#" <cfif gpsID IS asset_id> selected</cfif>>#name#</option>
                      </cfloop>
                    </select>
                    
                    <cfif startDate NEQ ''>
                        <cfset sDate = dateFormat(startDate,'YYYY-MM-DD')>
                    <cfelse>
                        <cfset sDate = ''>
                    </cfif>
                      
                    <cfif endDate NEQ ''>
                      <cfset eDate = dateFormat(endDate,'YYYY-MM-DD')>
                    <cfelse>
                      <cfset eDate = ''>
                    </cfif>
                      
                    </td>
                  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="50" align="right" valign="middle" class="content" style="padding-right:5px;">
                      Initial
                      </td>
                      <td valign="middle"><input name="dateStart" type="text" class="formfieldcontent" id="dateStart_#cnt#" style="width:100px" />
                      
                        <input name="startDate" type="hidden" id="startDate_#cnt#" value="#sDate#" /></td>
                      <td width="50" align="right" valign="middle" class="content" style="padding-right:5px;">
                      Expire</td>
                      <td valign="middle"><input name="dateEnd" type="text" class="formfieldcontent" id="dateEnd_#cnt#" style="width:100px" />
                      
                        <input name="endDate" type="hidden" id="endDate_#cnt#" value="#eDate#" />
                        </td>
                      <td width="50" align="right" valign="middle" class="content" style="padding-right:5px;">
                      <cfset curAccess = accessLevel>
                      <select name="accessLevel" id="accessLevel_#cnt#" style="background:url(images/access_locked-#accessLevel#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateAssetAccess(#cnt#)" />
                                <cfloop query="accessLevels">
                                <option value="#accessLevel#" style="color:##333" <cfif curAccess IS accessLevel> selected</cfif>>#levelName#</option>
                                </cfloop>
                            </select>
                      
                      </td>
                      <td width="50" align="right" valign="middle" class="content" style="padding-right:5px;">Length</td>
                      
                      <td valign="middle">
                      <cfif accessLength IS ''><cfset accessLen = 0><cfelse><cfset accessLen = accessLength></cfif>
                      <input name="expLength" type="text" class="formfieldcontent" id="expLength_#cnt#" style="width:25px" value="#accessLen#" maxlength="3" />
                      </td>
                      <td valign="middle" class="content" style="padding-left:5px">d</td>
                    </tr>
                  </table></td>
                  <td>
                  <input name="saveCode" type="button" class="itemShow" id="saveCode_#cnt#" style="background:url(images/replace.png) no-repeat; border:0; width:44px; height:44px;" onClick="setCode(this.form,#cnt#);" value="" />
                  </td>
                  <td width="44"><input name="deleteCode" type="button" class="itemShow" id="deleteCode" style="background:url(images/remove.png) no-repeat; border:0; width:44px; height:44px;" onclick="deleteAccess(#accessCode_id#)" value="" /></td>
                  </tr>
              </table>
              
              </td>
           </tr>
          </table>
          
          <!--- Date Picker --->
        <script type="text/javascript">
        
          var startDate;
          var endDate;
          
          <cfif startDate NEQ ''>
            startDate = '#dateFormat(startDate,"MM/DD/YYYY")#';
          <cfelse>
            startDate = '';
          </cfif>
          
          <cfif endDate NEQ ''>
            endDate = '#dateFormat(endDate,"MM/DD/YYYY")#';
          <cfelse>
            endDate = '';
          </cfif>
        
          pickerStart[#cnt#] = new Pikaday( { format: 'MM/DD/YYYY', field: document.getElementById('dateStart_#cnt#'), onSelect: function() { getStartEndDate(#cnt#); } });
          pickerEnd[#cnt#] = new Pikaday( { format: 'MM/DD/YYYY', field: document.getElementById('dateEnd_#cnt#'), onSelect: function() { getStartEndDate(#cnt#); } });
              
          pickerStart[#cnt#].setDate(new Date(startDate));
          pickerEnd[#cnt#].setDate(new Date(endDate));
          
        </script>
        <!---  --->
          
          
          <cfset cnt++>
          </cfloop>
      </div>
        </cfoutput>
    </div>
</cfif>
</cfif>

<div style="width:810px" class="<cfif assets.color_id IS ''>detailsHide<cfelse>detailsShow</cfif>" id="ColorsView">
	<cfoutput>
  <!--- get group other path --->
<cfif isDefined('subgroupID')>
    <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="colorPath">
      <cfinvokeargument name="groupID" value="#subgroupID#"/>
      <cfinvokeargument name="server" value="true"/>
    </cfinvoke>
</cfif>

<!--- Colors --->
  <div class="sectionLink" style="margin-left:0px; margin-top:5px; height:auto; width:100%">
    <div class="sectionLink" style="background-color:##666; height:32px; padding-left:10px; padding-right:-10px; padding-top:8px;">Colors</div>

    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="content" bgcolor="##FFF" style="background-color:##fff">
        <tr>
          <td width="681" align="left">
            
            <table width="100%" border="0" cellspacing="5">
              <tr class="contentLinkGrey">
                  <td colspan="2" align="left" class="content">Colors</td>
                <td width="524" class="content">
                <cfif assets.name NEQ "theme">
                Background
                 </cfif>
                </td>
              </tr>
              <tr>
                <td width="72" align="left" class="content">Foreground</td>
                <td width="168"><input name="contentColor" type="text" class="color formfieldcontent" id="contentColor" value="#assets.forecolor#" size="8" maxlength="8" /></td>
                <td rowspan="4" align="left" valign="top">
                
                <div style="float:left; height:44px; padding-top:5px">
               <select name="displayType" id="displayType" onchange="updateDisplayBKAssets(this.options[this.selectedIndex].value);">
                 <option value="0" <cfif assetType IS 0> selected</cfif>>None</option>
                 <option value="1" <cfif assetType IS 1> selected</cfif>>Images</option>
                 <option value="2" <cfif assetType IS 2> selected</cfif>>Videos</option>
               </select>
              
               <select name="displayAssetID" id="displayBKAssets" onchange="viewDisplayBKAsset()">
                <option value="0">None</option>
               </select>
               </div>
                
                <img src="" height="160" id="theDisplayBKPreview" style="display:none" />
     
                  <div id="theDisplayVideoBKPreview" style="display:none">
                    
                     <video height="160" controls id="theVideoBK">
                      <source src="" type="video/mp4">
                    </video> 
                  </div>
                
                <!--- <cfif assets.name NEQ "theme">
                <table width="100%">
                  <tr>
                    <td width="160" class="content">
                    <cfif isDefined('subgroupID')>
						<cfif assets.background NEQ ''>
                        
						<cfset background = "#colorPath##assets.background#">
                        
                        <cfif fileExists(background)><cfelse>
                          <span style="color: ##FF0004">File Missing</span>
                        </cfif>
                        
                        <cfset background_nonRetina = "#colorPath#nonretina/#assets.background#">
                        <cfif fileExists(background_nonRetina)><cfset border = 0><cfelse>
						<cfset arrayAppend(fixNonRetina,{'src':expandPath(background),'des':expandPath(background_nonRetina)})>
						<cfset border = 2><span style="color: ##FF0004">NonRetina Missing</span></cfif>
                        <div class="contentLinkDisabled" style="background-color: ##EEE; border: 2px solid ##727272; width: 160px; height: 120px; text-align: center;" id="backgroundObject">
                        <img src="#colorPath##assets.background#" width="160" height="120" style="border: solid ##FF0004;border-width: #border#px; float:left"><p id="textColor" style="position:absolute; width:160px">No Background</p>
						</div>
                        <cfelse>
                    <div class="contentLinkDisabled" style="background-color: ##EEE; border: 2px solid ##727272; width: 160px; height: 120px; text-align: center;" id="backgroundObject"><p id="textColor">No Background</p>
                    </div>
                        </cfif>
                    <cfelse>
                    	<cfif assets.background NEQ ''>
                        <cfset background = "#assetPath##assets.background#">
                        <cfif fileExists(background)><cfelse>
                          <span style="color: ##FF0004">File Missing</span>
                        </cfif>
                        <cfset background_nonRetina = "#assetPath#nonretina/#assets.background#">
                        <cfif fileExists(background_nonRetina)><cfset border = 0><cfelse>
						<cfset arrayAppend(fixNonRetina,{'src':expandPath(background),'des':expandPath(background_nonRetina)})>
						<cfset border = 2><span style="color: ##FF0004">NonRetina Missing</span></cfif>
                        
                        <img src="#assetPath##assets.background#" width="160" height="120" style="border: solid ##FF0004;border-width: #border#px;">
    
                        <cfelse>
                    		<div class="contentLinkDisabled" style="background-color: ##EEE; border: thin solid ##727272; width: 160px; height: 120px; text-align: center;" id="backgroundObject"><p id="textColor">No Background</p>
                    </div>
                        </cfif>
                    </cfif>
                    </td>
                    <td valign="top">
                    <cfif isDefined('subgroupID')>
                    <input type="button" class="itemShow" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteColorBackground(#subgroupID#,0);" value="" />
                    <cfelse>
                    <input type="button" class="itemShow" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteColorBackground(0,#asset_id#);" value="" />
                    </cfif>
                    <p>
                    <input name="contentBackgroundImage" type="file" style="margin-left:10px; margin-top:20px; padding-left:0; width:350px" class="formfieldcontent" id="contentBackgroundImage" />
                    </td>
                  </tr>
                  <cfif assets.background NEQ ''>
                  <tr>
                    <td colspan="2" class="contentHilighted">#assets.background#</td>
                    </tr>
                    </cfif>
                  </table>
                </cfif> --->
                
                </td>
              </tr>
              <tr>
                <td align="left" class="content">Background</td>
                <td><input name="contentBGColor" type="text" class="color formfieldcontent" id="contentBGColor" value="#assets.backcolor#" size="8" maxlength="8" /></td>
              </tr>
              <tr>
                <td align="left" class="content">Other</td>
                <td>
                <input name="contentOtherColor" type="text" class="color formfieldcontent" id="contentOtherColor" value="#assets.othercolor#" size="8" maxlength="8" />
               
                </td>
              </tr>
              <tr>
                <td align="left" class="content">&nbsp;</td>
                <td></td>
              </tr>
            </table>
            
          </td>
       </tr>
        <tr>
          <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="100" class="content">
			  
                </td>
              <td align="left"></td>
              </tr>
          </table></td>
        </tr>
      </table>
      
  </div>
  
	</cfoutput>
</div>
 
 
<!---  </div> --->


<!--- Thumbs --->
<div style="width:810px" class="<cfif assets.thumbnail IS ''>detailsHide<cfelse>detailsShow</cfif>" id="thumbView">
      <div class="sectionLink" style="background-color:#666; height:32px; padding-left:10px; padding-top:8px;">Thumbnail</div>
     <cfoutput>
      <table width="100%" border="0" cellspacing="10" bgcolor="##FFF">
        <tr>
          <td width="10">
          
          <cfset thumbPath = theThumbnailImage>
         
          <cfif fileExists(thumbPath)>
         
          <cfinvoke component="CFC.File" method="getImageSize" returnvariable="thumbSize">
            <cfinvokeargument name="imagePath" value="#thumbPath#">
          </cfinvoke>
          
          <a href="#theThumbnailImage#" target="_new">
          <img src="#theThumbnailImage#" alt="#theThumbnailImage#" width="120" height="120" border="0" class="imgLockedAspect" style="background-color:<cfif assets.backcolor IS ''>##999<cfelse>#assets.backcolor#</cfif>" /></a>
          <p>
          <cfelse>
            <cfinclude template="Assets/noFileThumb.cfm">
          </cfif>
          </td>
          <td valign="bottom">
          <cfif fileExists(thumbPath)>
           <p><span class="content">Width: #thumbSize.width#, Height: #thumbSize.height#</span></p>
           </cfif>
           <p><span class="contentHilighted">#assets.thumbnail#</span>
             <cfif assets.thumbnail NEQ ''>
               
               <cfif fileExists(theThumbnailImage)>
                 <cfelse>
                 <span class="contentwarning">(File Missing)</span>
               </cfif>
               
               <cfelse>
               
               <span class="contentHilighted">No thumbnail</span>
               
             </cfif>
           </p>                   <p>
    <cfif isDefined("editGroupName")>
    <!--- <input name="groupID" type="hidden" id="assetID" value="#assets.group_id#" /> --->
    <cfelse>
    <!--- <input name="assetID" type="hidden" id="assetID" value="#assetID#" /> --->
    </cfif>
          <input name="assetPath" type="hidden" id="assetPath" value="#assetPath#" />
          <input name="imageThumb" type="file" class="formfieldcontent" id="imageThumb" style="color:##000; padding-left:0" maxlength="255" />
          </td>
        </tr>
        </table>
     </cfoutput>
</div>

<!--- Details ---> 
<div style="width:810px" class="<cfif assets.detail_id GT '0'>detailsShow<cfelse>detailsHide</cfif>" id="detailView">

<!--- Details --->
  <div class="sectionLink" style="margin-left:0px; height:auto; width:810px">
  <cfoutput>
    <div class="sectionLink" style="background-color:##666; height:32px; padding-left:10px; padding-top:8px;">Details</div>

    <table width="100%" border="0" cellpadding="0" cellspacing="10" class="content" bgcolor="##FFF" style="background-color:##fff">
        <tr>
          <td width="69" align="right">Title</td>
          <td><label for="title"></label>
            <input name="title" type="text" class="formfieldcontent" id="title" value="#assets.title#" style="width:550px" maxlength="128"/></td>
          <td width="300" align="left"><input name="titleColor" type="text" class="color formfieldcontent" id="titleColor" value="#assets.titleColor#" size="8" maxlength="8" /></td>
        </tr>
        <tr>
          <td align="right">Subtitle</td>
          <td><input name="subtitle" type="text" class="formfieldcontent" id="subtitle" value="#assets.subtitle#" style="width:550px;" maxlength="128" /></td>
          <td align="left"><input name="subtitleColor" type="text" class="color formfieldcontent" id="subtitleColor" value="#assets.subtitleColor#" size="8" maxlength="8" /></td>
      </tr>
       <tr>
          <td align="right" valign="top"><div style="padding-top:20px">Description</div></td>
         <td><textarea name="description" id="description" style="width:550px;height:80px" rows="5" class="formText">#assets.description#</textarea></td>
         <td align="left" valign="top"><input name="descriptionColor" type="text" class="color formfieldcontent" id="descriptionColor" value="#assets.descriptionColor#" size="8" maxlength="8" /></td>
      </tr>
       <tr>
          <td align="right" valign="top" style="padding-top:8px">None</td>
          <td colspan="2">
           <cfif isJson(assets.other) && assets.other NEQ '' && findNoCase(assets.other, '{') GT 0>
           
				<cfset theObjOptions = deserializeJson(assets.other)>
                <cfif theObjOptions.app IS "performer">
                    <cfset selectedOption = 1>
                <cfelse>
                    <cfset selectedOption = 0>
                </cfif>
            
            <cfelse>
            	<cfset selectedOption = 0>
          </cfif>
          
          <div style="margin-bottom:10px">
          <select name="objectTpes" style="width:200px" onchange="selectOtherOption(this.options[this.selectedIndex].value)">
          	<option value="0" <cfif selectedOption IS 0>selected</cfif>>None</option>
            <option value="1" <cfif selectedOption IS 1>selected</cfif>>Performer Settings</option>
          	
          </select>
          </div>
          <cfif isJson(assets.other) && assets.other NEQ '' && findNoCase(assets.other, '{') GT 0>
          
          	<cfset theObjOptions = deserializeJson(assets.other)>
			         
            <cfif selectedOption IS 1>
            	<cfinclude template="Assets/performerSettings.cfm">
            </cfif>
            
          <cfelse>
          
          <textarea name="other" cols="128" rows="5" class="content" id="other" style="width:550px;">#assets.other#</textarea>
          
          
          </cfif>
          </td>
        </tr>
    </table>
  </div>
    </cfoutput>
 </div>
</div>

<cfif isDefined('asset')>

<cfinvoke component="CFC.Assets" method="getAssetsTypes" returnvariable="assetTypes" />

<cfinclude template="assets-actions.cfm">

<!--- <div class="detailsHide" id="AssetView">

<!--- Assets --->
  <div class="sectionLink" style="margin-left:0px; margin-top:5px; height:auto; width:810px;">
    <div class="sectionLink" style="background-color:#666; height:40px; padding-left:10px; width:800px">
    	<div style="width:200px;float:left; height: 40px;line-height: 40px;">Assets</div>
        <div style="width:44px;float:right; height: 40px;line-height: 40px;" onclick="importAssets()"><!--- displayImporterType('AssetsImporter') --->
        <img src="images/import.png" width="44" height="44" />
        </div>
    </div>
    <!--- Import Assets --->
    <div style="width:800px; display:none;margin-left:0px; margin-top:5px; height:auto; width:810px;" id="AssetsImporter"> 
		<cfoutput>
        <div style="margin-top: 10px;height: 50px; width:100%">
        <form id="theAssets" style="height:44px">
          <select name="assetTypes" id="assetTypes" onchange="displayAssets(this.options[this.selectedIndex].value,'displayAssetsAssets')" style="height:44px; margin:0px;">
            <option value="0">All Asset Types</option>
            <cfloop query="assetTypes">
                <option value="#type#">#name#</option>
            </cfloop>
          </select>
          <input type="button" class="formText" value="Import Content" onclick="importActionAssetIntoAsset(this)" />
          <input name="contentIDs" type="hidden" id="contentIDs" />
          <div class="contentLinkGrey" id="totalAssets" style="width:180px; float:right; padding-top:15px"></div>
        </form>
        </div>
        </cfoutput>
        <div id="displayAssetsAssets" style="overflow:scroll; margin-top: 0px; color:#666; height:400px; overflow-x: hidden;border-style: solid; border-width: 1px; margin-bottom:40px; padding:10px"></div>
     </div>
    
    
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="content" bgcolor="#FFF">
            <tr>
              <td>
              
              <table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="#CCC">                  
                        <tr class="content">
                          <td height="44" style="padding-left:10px">
                          Asset Name
                          </td>
                          <td width="80" align="center" class="content">
                            Access
                          </td>
                          <td width="60" align="center" class="content">  
                          Cached
                          </td>
                          <td width="60" align="center" class="content">
                           Active
                          </td>
                          <td width="60" align="center" class="content">
                          Shared</td>
                          <td width="80" align="center" class="content">Sort Order </td>
                          <td width="60" align="right" class="content"></td>
      </tr>  
    </table>
    
    <cfif groupAssets.recordCount GT 0>
	<div id="assetsListing">
    <cfoutput query="groupAssets">
    
    <cfif active>
			<cfset state = "contentLink">
        <cfelse>
            <cfset state = "contentLinkDisabled">
    </cfif>
    
    <div class="rowhighlighter" onmouseover="showDelete(this,1);" onmouseout="showDelete(this,0);">                
    	<form id="content_id">
                        <table width="810" border="0" cellpadding="5" cellspacing="0">                  
                        <!--- Assets --->
                        <tr>
                          <td width="44">
                            <img src="images/#icon#" width="44" height="44" />
                          </td>
                          <td colspan="2">
                          <a href="AssetsView.cfm?assetID=#content_id#&amp;assetTypeID=#assetType_id#" class="#state#">
                          <div style="height:32px; padding-top:15px; padding-left:4px">#name#</div>
                          </a>
                          </td>
                          <td width="80" align="center" class="content">
    
                            <cfif accessLevel IS 0>
                                <cfset lock = 'access_unlocked'>
                            <cfelse>
                                <cfset lock = 'access_locked-'& accessLevel>
                            </cfif>
                            
                            <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateContentAssetOptions(this,#content_id#)" />
                                <cfloop query="accessLevels">
                                <option value="#accessLevel#" style="color:##333" <cfif groupAssets.accessLevel IS accessLevel> selected</cfif>>#levelName#</option>
                                </cfloop>
                            </select>
                          
                          </td>
                          <td width="60" align="center" class="content">  
                          <cfif cached IS '0'>
                          <cfset theCacheState = 'cache'>
                          <cfelse>
                          <cfset theCacheState = 'cached'>
                          </cfif>
                          <div id="contentCached" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer; width:28px; height:28px; background-image:url(images/#theCacheState#.png)">
                          </div>
                          </td>
                          <td width="60" align="center" class="content">
                          
                         
                          <cfif active IS '0'>
                            <cfset stateCSS = "contentLinkGreen">
                          <cfelse>
                            <cfset stateCSS = "contentLinkRed">
                          </cfif>
                          <div id="contentActive" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer" class="#stateCSS#">
                              <cfif active IS '0'>
                              NO
                              <cfelse>
                              YES
                              </cfif>
                          </div>
                          </td>
                          <td width="60" align="center" class="content">
                          
                          <cfif sharable IS '0'>
                            <cfset stateCSS = "contentLinkGreen">
                          <cfelse>
                            <cfset stateCSS = "contentLinkRed">
                          </cfif>
                          <div id="contentShared" style="cursor:pointer" onclick="updateContentAssetOptions(this,#content_id#)" class="#stateCSS#">
                          <cfif sharable IS '0'>
                          NO
                          <cfelse>
                          YES
                          </cfif>
                          </div>
                          
                          </td>
                          <td width="80" align="center" class="contentLinkDisabled">
                          <input name="sortOrder" type="text" id="sortOrder" value="#sortOrder#" size="4" maxlength="10" onchange="updateContentAssetOptions(this,#content_id#)" /></td>
                          <td width="60" align="right" class="content">
                          
                          <table border="0">
                          <tr>
                            <td>
                            <input type="button" class="itemHide" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteContentAsset(#content_id#,this);" value="" /> 
                            </td>
                          </tr>
                          
                          
                          
                        </table>
                          
                          
                          </td>
                        </tr>
                        
                          </table>
                        </form>
    </div>
    
    </cfoutput>   
    </div>
    
    <cfelse>
    <span class="content">
  
    </span>
    </cfif>

          
          </td>
      </tr>
      </table>
   	
       
  </div>
  
</div> ---> 
<!--- <div class="detailsHide" id="TargetView">
<!--- Actions --->
  <div class="sectionLink" style="margin-left:0px; margin-top:5px; height:auto; width:810px;">
    <div class="sectionLink" style="background-color:#666; height:40px; padding-left:10px; width:800px">
    	<div style="width:200px;float:left; height: 40px;line-height: 40px;">Actions</div>
        <div style="width:44px;float:right; height: 40px;line-height: 40px;" onclick="importActions()"><!--- displayImporterType('ActionsImporter') --->
        <img src="images/import.png" width="44" height="44" />
        </div>
    </div>
 	<!--- Importer --->
    <div style="width:800px; display:none;margin-left:0px; margin-top:5px; height:auto; width:810px;" id="ActionsImporter"> 
		<cfoutput>
        <div style="margin-top: 10px;height: 50px; width:100%">
        <form id="theActions" style="height:44px">
          <select name="assetTypes" id="assetTypes" onchange="displayAssets(this.options[this.selectedIndex].value,'displayAssetsActions')" style="height:44px; margin:0px;">
            <option value="0">All Asset Types</option>
            <cfloop query="assetTypes">
                <option value="#type#">#name#</option>
            </cfloop>
          </select>
          <input type="button" class="formText" value="Import Content" onclick="importActionAssetIntoAsset(this)" />
          <input name="contentIDs" type="hidden" id="contentIDs" />
          <div class="contentLinkGrey" id="totalAssets" style="width:180px; float:right; padding-top:15px"></div>
        </form>
        </div>
        </cfoutput>
        <div id="displayAssetsActions" style="overflow:scroll; margin-top: 0px; color:#666; height:400px; overflow-x: hidden;border-style: solid; border-width: 1px; margin-bottom:40px; padding:10px"></div>
     </div>


    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="content" bgcolor="#FFF">
            <tr>
              <td>
              
              <table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="#CCC">                  
                        <tr class="content">
                          <td height="44" style="padding-left:10px">
                          Asset Name
                          </td>
                          <td width="80" align="center" class="content">
                            Access
                          </td>
                          <td width="60" align="center" class="content">  
                          Cached
                          </td>
                          <td width="60" align="center" class="content">
                           Active
                          </td>
                          <td width="60" align="center" class="content">
                          Shared</td>
                          <td width="80" align="center" class="content">Sort Order </td>
                          <td width="60" align="right" class="content"></td>
      </tr>  
    </table>
    
    <cfif groupActions.recordCount GT 0>
    <div id="actionListing">
    <cfoutput query="groupActions">
    
    <cfif active>
			<cfset state = "contentLink">
        <cfelse>
            <cfset state = "contentLinkDisabled">
    </cfif>
    
    <div class="rowhighlighter" onmouseover="showDelete(this,1);" onmouseout="showDelete(this,0);">
                        
                        <form id="content_id">
                        <table width="810" border="0" cellpadding="5" cellspacing="0">                  
                        <!--- Assets --->
                        <tr>
                          <td width="44">
                            <img src="images/#icon#" width="44" height="44" />
                          </td>
                          <td colspan="2">
                          <a href="AssetsView.cfm?assetID=#content_id#&amp;assetTypeID=#assetType_id#" class="#state#">
                          <div style="height:32px; padding-top:15px; padding-left:4px">#name#</div>
                          </a>
                          </td>
                          <td width="80" align="center" class="content">
    
                            <cfif accessLevel IS 0>
                                <cfset lock = 'access_unlocked'>
                            <cfelse>
                                <cfset lock = 'access_locked-'& accessLevel>
                            </cfif>
                            
                            <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateContentAssetOptions(this,#content_id#)" />
                                <cfloop query="accessLevels">
                                <option value="#accessLevel#" style="color:##333" <cfif groupAssets.accessLevel IS accessLevel> selected</cfif>>#levelName#</option>
                                </cfloop>
                            </select>
                          
                          </td>
                          <td width="60" align="center" class="content">  
                          <cfif cached IS '0'>
                          <cfset theCacheState = 'cache'>
                          <cfelse>
                          <cfset theCacheState = 'cached'>
                          </cfif>
                          <div id="contentCached" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer; width:28px; height:28px; background-image:url(images/#theCacheState#.png)">
                          </div>
                          </td>
                          <td width="60" align="center" class="content">
                          
                         
                          <cfif active IS '0'>
                            <cfset stateCSS = "contentLinkGreen">
                          <cfelse>
                            <cfset stateCSS = "contentLinkRed">
                          </cfif>
                          <div id="contentActive" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer" class="#stateCSS#">
                              <cfif active IS '0'>
                              NO
                              <cfelse>
                              YES
                              </cfif>
                          </div>
                          </td>
                          <td width="60" align="center" class="content">
                          
                          <cfif sharable IS '0'>
                            <cfset stateCSS = "contentLinkGreen">
                          <cfelse>
                            <cfset stateCSS = "contentLinkRed">
                          </cfif>
                          <div id="contentShared" style="cursor:pointer" onclick="updateContentAssetOptions(this,#content_id#)" class="#stateCSS#">
                          <cfif sharable IS '0'>
                          NO
                          <cfelse>
                          YES
                          </cfif>
                          </div>
                          
                          </td>
                          <td width="80" align="center" class="contentLinkDisabled">
                          <input name="sortOrder" type="text" id="sortOrder" value="#sortOrder#" size="4" maxlength="10" onchange="updateContentAssetOptions(this,#content_id#)" /></td>
                          <td width="60" align="right" class="content">
                          
                          <table border="0">
                          <tr>
                            <td>
                            <input type="button" class="itemHide" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteContentAsset(#content_id#,this);" value="" /> 
                            </td>
                          </tr>
                          
                          
                          
                        </table>
                          
                          
                          </td>
                        </tr>
                        
                          </table>
                        </form>
    </div>
    
    </cfoutput>   
    </div>
    <cfelse>
    <span class="content">
    No Actions
    </span>
    </cfif>

          
          </td>
      </tr>
      </table>
   	
       
  </div>
  
  </div> ---> 

 <cfif groupActions.recordCount GT 0>

	<script type="text/javascript">
    
	showTargets();
    
    </script>

</cfif>

 <cfif groupAssets.recordCount GT 0>

	<script type="text/javascript">
    
	showAssets();
	
    </script>

</cfif>

</cfif>

<script type="text/javascript">
	
	//var el = document.getElementById("contentBGColor");
	//el.addEventListener("blur", setBackgroundColor, false);
	
	function OnColorChanged(selectedColor, colorPickerIndex) {
	
			bk = document.getElementById('backgroundObject');
			if(bk)	{	
				bkColor = document.getElementById('contentBGColor').value; 
				bk.style.backgroundColor = bkColor;
				bcColor = document.getElementById('contentOtherColor').value;
				bk.style.borderColor = bcColor;
			}
			
			tx = document.getElementById('textColor');
			
			if(tx)	{
				frColor = document.getElementById('contentColor').value; 
				tx.style.color = frColor;
			}

	}
	
	OnColorChanged();
	
</script>

<cfif assets.displayAsset_id IS ''><cfset displayAssetID = 0><cfelse><cfset displayAssetID = assets.displayAsset_id></cfif>
 
 <cfoutput>
	 <script>
         updateDisplayBKAssets(#assetType#,#displayAssetID#);
         viewDisplayBKAsset();	 
     </script>
</cfoutput>
