var xmlhttp = new XMLHttpRequest();

var rootURL = "http://localhost:8501/Live-Platform/api/v10/LiveAPI.cfc?method=getContent&";
var authToken = "auth_token=696B62A1-FD4F-B3E7-FEDE01247FDC707C&";
var url = rootURL + authToken + "groupID=1439";

var liveData = {};

xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        var myArr = JSON.parse(xmlhttp.responseText);
        parseLiveData(myArr);
    }
}
xmlhttp.open("GET", url, true);
xmlhttp.send();

function setImage(groupAsset,theAssetName,theObject) {
	
	theAssets = getGroupAssets(groupAsset);
	
    for(i = 0; i < theAssets.length; i++) {
		
		theKey = Object.keys(theAssets[i]);
		
		if(theKey == theAssetName)
		{
		theAsset = theAssets[i];
		key = theKey[0];
        theImg = '<img src="../v10/' + theAsset[key].url.mdpi.url + '">';
		break;
		}
		
    }
    document.getElementById(theObject).innerHTML = theImg;
}

function getGroupAssets(group)
{
	groupAssets = {};
	
	for(i = 0; i < liveData.assets.length; i++)
	{
		var theKey = Object.keys(liveData.assets[i]);
		
		if(theKey == group){
			var groupAssets = liveData.assets[i][theKey].assets;
			break;
		}
	}
	
	return groupAssets
}