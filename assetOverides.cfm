<link href="../styles.css" rel="stylesheet" type="text/css">
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/1.4.5/numeral.min.js"></script>

<cfparam name="projectID" default="0">
<cfparam name="spaceID" default="0">
<cfparam name="assetID" default="0">
<cfparam name="path" default="">

<cfparam name="optionID" default="0">
<cfparam name="viewDefaults" default="0">

<!--- configurator --->
<cfajaxproxy cfc="CFC.Configurator" jsclassname="configurator">

<script type="text/javascript">

var jsConfig = new configurator();

function addOveride(theMaterialID)	{
	
	var theData = {};
	
	<cfoutput>
		jsConfig.setSyncMode();
		jsConfig.setCallbackHandler(addOverideSuccess);
		jsConfig.addOveride(#projectID#, #assetID#, #spaceID#, theMaterialID, 1);
	</cfoutput>
}

function selectProject(projectID)	{
	document.location = "AppsView.cfm?edit=true&tab=6&projectID="+ projectID;
}

function openOveride(assetID, assetTitle)	{
	
	stateObj = checkConfigSelection(assetID, assetTitle);
	
	document.location = "AppsView.cfm?edit=true&tab=6&projectID="+ stateObj.projectID + "&assetID="+ stateObj.assetID+"&path="+ stateObj.path;
}

function checkConfigSelection(assetID, assetTitle)	{
	
	var path = "";

	var theSel = document.getElementById("theProject");
	var projectID = theSel.options[theSel.selectedIndex].value;
	
	path+= theSel.options[theSel.selectedIndex].text;
	
	if(assetID === undefined)	{
		var theSel = document.getElementById("theAsset");
		var assetID = theSel.options[theSel.selectedIndex].value;
		path+= ' | '+theSel.options[theSel.selectedIndex].text;
	}else{
		var theSel = document.getElementById("theAsset");
		path+= ' | '+ assetTitle;
	}
	
	
	var theOK = document.getElementById("openMaterials");
	
	if(projectID > 0 && assetID > 0)	{
		theOK.className = "itemShow";
	}else{
		theOK.className = "itemHide";
	}
	
	return {"projectID":projectID, "assetID":assetID, "path":path}
	
}

function closeOveride()	{
	document.location = "AppsView.cfm?edit=true&tab=6";
}

function selectRoom(spaceID)	{
	
	var theSel = document.getElementById("theSpace");
	var spaceID = theSel.options[theSel.selectedIndex].value;
	
	<cfoutput>
	document.location = "AppsView.cfm?edit=true&tab=6&projectID=#projectID#&assetID=#assetID#&spaceID="+ spaceID +"&path=#path#";
	</cfoutput>
}

function selectOption(optionID)	{
	
	<cfoutput>
	document.location = "AppsView.cfm?edit=true&tab=6&projectID=#projectID#&assetID=#assetID#&spaceID=#spaceID#&path=#path#&optionID="+optionID;
	</cfoutput>
	
}

function overideAssetTitle(assetID)	{
	
	var theAssetTitle = document.getElementById("assetTitle").value;
	console.log(assetID,theAssetTitle);
}

function removeTitleOveride()	{
	
	document.getElementById("assetTitle").value = '';
}

function setMultiplier(theCost, theMulti, theAssetID)	{
	
	var theTotal = theCost * theMulti;
	console.log(theCost, theMulti,currencyFormat(theTotal));
	document.getElementById("TCost_"+theAssetID).innerHTML = currencyFormat(theTotal);
	
	var costState = document.getElementById("costState");
	
	updateMultipler(theAssetID);
	
}

function currencyFormat (num) {
	return numeral(num).format('0,0');
}

function updateMultipler(theAssetID)	{
	
	var theBut = document.getElementById("costState_"+theAssetID);
	theBut.src = "images/replace.png";
}

function resetOK(theAssetID)	{
	
	var theBut = document.getElementById("costState_"+theAssetID);
	theBut.src = "images/remove.png";
}

function removeOveride(theAssetID, overideID)	{
	
	var theBut = document.getElementById("costState_"+theAssetID);
	var theObj = (theBut.src).split('/');
	var theImg = theObj[theObj.length-1].split('.')[0];
	
	if(theImg === 'replace')	{
		//update
		var theMultiplier = document.getElementById("multiplier_"+theAssetID).value; 
		console.log('Update:'+overideID, theMultiplier);
		<cfoutput>
			jsConfig.setSyncMode();
			jsConfig.setCallbackHandler(updateOverideSuccess);
			setTimeout(function() { resetOK(theAssetID); }, 1000);
			jsConfig.updateOveride(overideID,theMultiplier);
		</cfoutput>
		theBut.src = "images/saveupdate.png";
		
	}else{
		//delete
		if(confirm('Delete this Overide for this Material?'))
		{
			jsConfig.setSyncMode();
			jsConfig.setCallbackHandler(removeOverideSuccess);
			jsConfig.deleteOveride(overideID);
			
		}else{ 
			//nothing
		}
	
	}
}

function updateOverideSuccess(result)	{
	location.reload();
}


function removeOverideSuccess(result)	{
	location.reload();
}

function addOverideSuccess(result)	{
	location.reload();
}

</script>

<!--- get all projects --->
<cfinvoke component="CFC.Modules" method="getGroups" returnvariable="allModules">
	<cfinvokeargument name="appID" value="#session.appID#"/>
	<cfinvokeargument name="subgroupID" value="-1"/>
	<cfinvokeargument name="active" value="1"/>
</cfinvoke>
    
<cfif projectID GT 0>

    <!--- get all assets --->
    <cfinvoke component="CFC.Modules" method="getCMSAppGroups" returnvariable="groups">
      <cfinvokeargument name="appID" value="#session.appID#"/>
      <cfinvokeargument name="groupID" value="#projectID#"/>
      <cfinvokeargument name="omit" value="Theme,Background,Categories"/><!--- omit these folders --->
    </cfinvoke>
    
    <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="imagesPath">
    	<cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="relative" value="true"/>
        <cfinvokeargument name="images" value="true"/>
    </cfinvoke>
    
 	<cfset aPath = imagesPath &'thumbs/'>

 </cfif>

<cfif assetID IS 0>
    
    <cfoutput>
    <table width="100%" border="0" cellpadding="0" cellspacing="5" bgcolor="##EEE">
      <tr>
        <td width="40" height="45" align="right" valign="middle" class="sectionHeaderText" style="padding-top:0; float:none">Project</td>
        <td width="250" align="left" valign="middle">
        <select name="theProject" id="theProject" style="text-indent:1px;height:32px; width:250px" onChange="selectProject(this.options[this.selectedIndex].value);checkConfigSelection()">
        <option value="0"<cfif projectID IS 0> selected</cfif>>None</option>
          <cfloop index="project" array="#allModules#">
              <option value="#project.group_id#"<cfif projectID IS project.group_id> selected</cfif>>#project.name#</option>
          </cfloop>
        </select>
        </td>
		<cfif projectID GT 0>
        <td width="40" align="right" valign="middle" class="sectionHeaderText" style="padding-top:0; float:none">Model</td>
        <td valign="middle">
          <select name="theAsset" id="theAsset" style="text-indent:1px;height:32px; width:250px" onChange="checkConfigSelection()">
              <option value="0"<cfif assetID IS 0> selected</cfif>>None</option>
                <cfloop query="groups">
                <option value="#group_id#"<cfif assetID IS group_id> selected</cfif>>#name#</option>
                </cfloop>
          </select>
        </td>
        <td width="44" height="45">
          <input type="button" class="itemHide" id="openMaterials" style="background:url(images/saveupdate.png) no-repeat; border:0; width:44px; height:44px;" onClick="openOveride();" value="" />
        </td>
        <cfelse>
        <td></td>
        <td valign="middle"></td>
        </cfif>
        
      </tr>
      <cfif projectID GT 0>
      <tr>
        <td colspan="7">
        <cfloop query="groups">
        
			<cfset anImage = aPath & groups.image>
            
            <cfif groups.title IS ''>
            	<cfset theTitle = groups.name>
            <cfelse>
            	<cfset theTitle = groups.title>
            </cfif>
        	
            
            <div style="width:180px; float:left; margin:8px; border:thin ##CCC solid; cursor:pointer" onclick="openOveride(#groups.group_id#, '#theTitle#')">
            <div style="text-align:center; margin:0px; padding:4px; background-color:##CCC" class="content">#theTitle#</div>
            <img src="#anImage#" width="180px" />
            </div>
            
        </cfloop>
        </td>
      </tr>
      </cfif>
    </table>
    </cfoutput>

<cfelse>
	
    <!--- get all rooms --->
    <cfinvoke component="CFC.Configurator" method="getGroupAssets" returnvariable="theAssets">
        <cfinvokeargument name="groupID" value="#assetID#"/>
    </cfinvoke>

    <cfinvoke component="CFC.Modules" method="getCMSAppGroups" returnvariable="groups">
      <cfinvokeargument name="appID" value="#session.appID#"/>
      <cfinvokeargument name="groupID" value="#projectID#"/>
      <cfinvokeargument name="omit" value="Theme,Background,Categories"/><!--- omit these folders --->
    </cfinvoke>
    
    <table width="100%" border="0" cellpadding="5" cellspacing="0" class="content material">
      <tr class="plainLinkDkGrey">
        <td width="44" bgcolor="#eee">
		<cfoutput>
        <!--- <a href="AppsView.cfm?edit=true&tab=3&assetID=#assetID#&viewDefaults=1"> --->
        <img src="images/options.png" alt="" width="44" height="44" border="0" />
        <!--- </a> --->
		</cfoutput>
        </td>
        <td height="44" bgcolor="#eee" style="padding-left:5px;">
		<cfoutput><span class="sectionHeaderText" style="padding-top:0; padding-left:0; font-size:98%">#path#</span></cfoutput>
        </td>
			<!--- spaces --->
            <td width="200" align="right" bgcolor="#EEE">
            <cfoutput>
            <select name="theSpace" id="theSpace" style="text-indent:1px;height:32px; width:200px; text-align:right" onChange="selectRoom(this.options[this.selectedIndex].value);">
            <option value="0"<cfif spaceID IS 0> selected</cfif>>None</option>
              <cfloop query="theAssets">
                <option value="#asset_id#"<cfif asset_id IS spaceID> selected</cfif>>#name#</option>
              </cfloop>
            </select>
            </cfoutput>
            </td>
        
        <!---  --->
        <!--- options --->
        
        <cfif spaceID GT 0>
        
            <cfinvoke component="CFC.Configurator" method="getOptions" returnvariable="options">
                <cfinvokeargument name="assetID" value="#spaceID#"/>
            </cfinvoke> 
            
            <td width="44" bgcolor="#EEE">
            <cfoutput>
            <select name="optionID" id="optionID" style="width:150px; text-align:right; margin-left:10px" onChange="selectOption(this.options[this.selectedIndex].value)">
            <option value="0" <cfif optionID IS 0>selected</cfif>>None</option>
                <cfloop query="options">
                  <option value="#materialID#" <cfif optionID IS materialID>selected</cfif>>#title#</option>
                  <cfif optionID IS materialID>
                    <cfset materailName = title>
                  </cfif>
                </cfloop>
            </select>
            </cfoutput>
            </td>
            
        </cfif>
            
        <td width="44" bgcolor="#EEE">
          <input type="button" class="itemShow" id="theOveride" style="background:url(images/remove.png) no-repeat; border:0; width:44px; height:44px;" onClick="closeOveride();" value="" />
      </td>
      </tr>
      
    </table>
    

	<cfif spaceID GT 0>
        
        <cfinvoke component="CFC.Configurator" method="getSpace" returnvariable="theSpace">
            <cfinvokeargument name="assetID" value="#assetID#"/>
      </cfinvoke>  
        
        <!--- Overide Details - Title --->
        <!--- <cfoutput>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="content material">
      <tr class="plainLinkDkGrey">
        <td width="120" height="52" bgcolor="##eee" style="padding-left:10px">
        <cfif 0 IS 0>
        Details
        </cfif> 
        Title</td>
        <td width="200" bgcolor="##eee" style="padding-left:5px">
        
        <input name="assetTitle" type="text" class="formfieldcontent" id="assetTitle" style="width:250px; height:32px; font-size:14px;" value="#trim(theSpace.title)#" onChange="" />
        
        </td>
			<!--- spaces --->
        <!---  --->
        <!--- options --->
        
        <cfif spaceID GT 0>
        
            <cfinvoke component="CFC.Configurator" method="getOptions" returnvariable="options">
                <cfinvokeargument name="assetID" value="#spaceID#"/>
            </cfinvoke> 
            
        </cfif>
        <td width="44" bgcolor="##EEE">
          <input type="button" class="itemShow" id="theTitleRemoveOveride" style="background:url(images/include.png) no-repeat; border:0; width:44px; height:44px;" onClick="addTitleOveride(); overideAssetTitle(#theSpace.asset_id#);" value="" />
      </td>   
        <td width="44" bgcolor="##EEE">
        <input type="button" class="itemShow" id="theTitleOveride" style="background:url(images/replace.png) no-repeat; border:0; width:44px; height:44px;" onClick="updateAssetTitle(#theSpace.asset_id#);" value="" />
      </td>
      <td width="44" bgcolor="##EEE">
          <input type="button" class="itemShow" id="theTitleRemoveOveride" style="background:url(images/remove.png) no-repeat; border:0; width:44px; height:44px;" onClick="removeTitleOveride(); overideAssetTitle(#theSpace.asset_id#);" value="" />
      </td> 
        <td bgcolor="##EEE">&nbsp;</td>
      </tr>
      
    </table>
        </cfoutput> --->
   
        <!--- details --->
        <cfinvoke component="CFC.Configurator" method="getMaterials" returnvariable="materials">
          <cfinvokeargument name="optionID" value="#optionID#"/>
          <cfinvokeargument name="contentID" value="#spaceID#"/>
        </cfinvoke>
 
      <cfif viewDefaults>
            
            <cfinvoke component="CFC.Configurator" method="getMaxDefaultOptions" returnvariable="maxOptions">
              <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
    
        </cfif>
        
      
        <cfoutput>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="##FFF">
        <cfif viewDefaults>
            <tr>
            <td>
           
            <cfloop index="theOption" from="1" to="#maxOptions#">
            
            <cfinvoke component="CFC.Configurator" method="getDefaultOptions" returnvariable="defaultOptions">
              <cfinvokeargument name="assetID" value="#assetID#"/>
              <cfinvokeargument name="defaultOption" value="#theOption#"/>
            </cfinvoke>
            
            <!--- display default summary --->
            <table width="100%" border="0" cellpadding="8" cellspacing="1" class="content" bgcolor="##AAA" style="margin-bottom:10px">
                  <tr class="contentLinkWhite">
                    <td colspan="4" bgcolor="##666">Default Option #theOption#</td>
                    </tr>
                       <cfloop query="defaultOptions">
                        <!--- get room name --->
                        <cfinvoke component="CFC.Configurator" method="getRoomFromMaterial" returnvariable="theRoom">
                          <cfinvokeargument name="assetID" value="#asset_id#"/>
                        </cfinvoke>
                       
                      <tr bgcolor="##fff" class="contentLinkGrey">
                        <td width="200" align="left" valign="top" bgcolor="##fff" class="contentLinkDisabled">#theRoom.title#</td>
                        <td width="200" align="left" valign="top" bgcolor="##fff">#title#</td>
                        <td align="left" valign="top">#description#</td>
                        <td width="100" align="right" valign="bottom" bgcolor="##fff">#NumberFormat(cost,'___.__')#</td>
                      </tr>
                      </cfloop>
                  </table>
                  </cfloop>
           </td>
           </tr>
        <cfelse>
        <!--- list materials for options --->
        <cfif optionID GT 0>
          <tr>
            <td>
            
                <!--- materials list --->
                <table width="100%" border="0" cellpadding="5" cellspacing="1" class="content" bgcolor="##AAA">
                  <tr class="contentLinkWhite">
                    <td width="200" height="24" bgcolor="##666">Name</td>
                    <td bgcolor="##666">Description</td>
                    <td width="80" align="center" bgcolor="##666">Multiplier</td>
                    <td width="80" align="center" bgcolor="##666">Unit Cost</td>
                    <td width="44" align="right" bgcolor="##666" style="padding-right:60px; padding-left:42px">Total</td>
                  </tr>
                  </table>
                <cfset groupLevel = 0>
                
                <!--- getOverides --->
                <cfinvoke component="CFC.Configurator" method="getOverides" returnvariable="allOverides">
                    <cfinvokeargument name="projectID" value="#projectID#"/>
                    <cfinvokeargument name="assetID" value="#assetID#"/>
                    <cfinvokeargument name="spaceID" value="#spaceID#"/>
                </cfinvoke>
               
                <cfloop query="materials">

                  <div class="<cfif NOT structKeyExists(allOverides,material_id)>materialItem-disabled</cfif>" id="materialItem">
                  <form id="#asset_id#">
                    <table width="100%" border="0" cellpadding="5" cellspacing="1" class="material">
                      <cfif grouping NEQ groupLevel>
                      <tr>
                      <td colspan="6" bgcolor="##999" class="contentGreyed">
                      <cfif grouping IS 1>
                      STANDARD
                      <cfelse>
                      LEVEL #grouping-1#
                      </cfif>
                      </td>
                      </tr>
                      <cfset groupLevel = grouping>
                      </cfif>
                      <tr>
                        <td width="200" class="contentLinkGrey" style="padding-left:5px">
                            <span class="contentLinkGrey"><a href="http://liveplatform.net/API/v11/AssetsView.cfm?assetID=#asset_id#&assetTypeID=#assetType#" class="contentLink">#title#</a></span>
                        </td>
                        <td class="contentLinkGrey" style="padding-left:10px">
                            <span class="contentLinkGrey">#description#</span>
                        </td>
                        <td width="80" align="center" class="contentLinkGrey">
                        <cfif NOT connected>
                        NA
                        <cfelse>
							<cfif structKeyExists(allOverides,material_id)>
                            <input name="multiplier" type="text" class="formfieldcontent" id="multiplier_#asset_id#" style="width:50px; font-size:14px; text-align:center;" value="#allOverides[material_id].multiplier#" onChange="setMultiplier(#cost#,this.value,#asset_id#)" />
                            </cfif>
                        </cfif>
                        </td>
                        <td width="80" align="center" class="contentLinkGrey">
						<cfif NOT connected>
                        -<cfelse>
                        #cost#/unit
                        </cfif>
                        </td>
                        <td width="80" align="right" class="contentLinkGrey">
                         <cfif NOT connected>
                         -
                         <cfelse>
						    <cfif structKeyExists(allOverides,material_id)>
                            <cfset totalCost = allOverides[material_id].multiplier * cost>
                             <div id="TCost_#asset_id#">#numberFormat(totalCost,'__,___')#</div>
                            </cfif>
                         </cfif>
                        </td>
                        <td width="44">
                        <cfif connected>
							<cfif NOT structKeyExists(allOverides,material_id)>
                            <img src="images/include.png" id="costState_#asset_id#" onClick="addOveride(#material_id#)" />            			<cfelse>
                            <cfset overID = allOverides[material_id].overideID>
                            <img src="images/remove.png" id="costState_#asset_id#" onClick="removeOveride(#asset_id#,#overID#)" />  
                            </cfif>                     
                        </cfif>
                        </td>
                      </tr>
                    </table>
                  </form>
                  </div>
              </cfloop>
        
            </td>
          </tr>
        </cfif>
        
          </cfif>
        </table>
        </cfoutput>
    
    </cfif>

</cfif>