<cfparam name="clientID" default="0">
<cfparam name="appID" default="0">

<cfparam name="session.clientID" default="0">
<cfparam name="session.appID" default="0">

<cfinvoke component="CFC.Apps" method="getPaths" returnvariable="clients" />

<cfinvoke  component="CFC.Apps" method="getClientApps" returnvariable="clientApps">
    <cfinvokeargument name="clientID" value="#clientID#"/>
</cfinvoke>



<link href="styles.css" rel="stylesheet" type="text/css" />

<style>
.sqButton
	{ 
	border: 1px solid #666;  
	height:32px;
	background-color:#FFF;
	padding-top:2px;
	}
</style>

<script language="javascript">

function setAppID(appID)	{
	document.location.href = "batchImport.cfm?clientID="+ <cfoutput>#session.clientID#</cfoutput> + "&appID="+ session.appID;
}

function importConfirm()	{
	
	var theObjType = document.getElementById("importType");
	theType = theObjType.options[theObjType.selectedIndex].value;
	
	if(theType == -1)	{
		alert('Please Select Import Type');
		return false	
	}
	
	var theObj = document.getElementById("appID");
	theAppName = theObj.options[theObj.selectedIndex].text;
	
	var theObjC = document.getElementById("clientID");
	theClientName = theObj.options[theObjC.selectedIndex].text;
	
	if(confirm('Are you sure you wish to Import Asset(s) into "'+ theAppName +'" Application for "'+ theClientName +'"'))
	{
		return true
	}else{ 
		return false
	}
}

function setClientID(clientID)	{
	
	document.location.href = "batchImport.cfm?clientID="+ clientID;
	
}

function displayImport()	{
	
	var theObjFile = document.getElementById("importFile");
	
	if(theObjFile.value != '' && theObjFile.value.indexOf('.zip') > -1)	{
	
		var theObj = document.getElementById("import");
		theObj.style.display = "block";
		
		var theObjMes = document.getElementById("importMessage");
		theObjMes.style.display = "none";
		
	
	}else{
		theObjFile.value = "";
		alert('The file must be a ZIP file that contains all assets');	
	}
	
}

function displayHelp()	{
	
	var theObj = document.getElementById("help");
	
	if(theObj.style.display === "block")	{
		theObj.style.display = "none";
	}else{
		theObj.style.display = "block";
	}
	
}

</script>

<div class="contentLink" style="padding:10px 10px 10px 10px; cursor:pointer" onclick="displayHelp()">
Batch Importer Instructions
</div>
<div class="contentLinkGrey" style="padding:10px 10px 10px 10px; width:600px">
All batch imports will appear under AssetGroups as per the folder you defined in the import. Keep in mind that there is no support for sub-sub groups for imports.
</div>

<div style="width:auto; height:auto" class="formText">

<form action="batchImporter.cfc?method=importAssets" method="post" enctype="multipart/form-data" name="importAssets" onsubmit="return importConfirm();">

<cfif session.clientID GT 0 AND session.appID GT 0>
	<cfset clientID = session.clientID>
	<cfset appID = session.appID>
<cfelse>

    <div style="height:32px">
        <!--- client --->
        <div style="width:100px; float:left; text-align:right; padding-top:6px; padding-right:4px" class="contentLinkGrey">Select Client</div>
        
        <div style="float:left">
        <select id="clientID" name="clientID" onchange="setClientID(this.options[this.selectedIndex].value);" style="padding:0 0 0 0; text-indent: 4px;; height:32px; width:250px">
          <option value="0" style="color:#999">Select a Client</option>
          <cfloop collection="#clients#" item="app">
            <cfoutput>
              <option value="#clients[app].id#" <cfif clientID IS clients[app].id> selected</cfif>>#clients[app].name#</option>
              </cfoutput>
          </cfloop>
        </select>
        </div>
        
		<!--- App --->
        <cfif clientID GT 0>
        <div style="width:50px; float:left; text-align:right; padding-top:6px; padding-right:4px" class="contentLinkGrey">App</div>
        
        <div style="float:left">
        <select id="appID" name="appID" onchange="setAppID(this.options[this.selectedIndex].value);" style="padding:0 0 0 0; text-indent: 4px;; height:32px; width:250px">
          <option value="0" style="color:#999">Select an App</option>
          <cfloop query="clientApps">
            <cfoutput>
              <option value="#app_id#" <cfif appID IS app_id> selected</cfif>>#appName#</option>
              </cfoutput>
          </cfloop>
        </select>
        </div>
        <cfif appID GT 0>
            <div style="width:185px; float:left; text-align:left; padding-top:0px; padding-left:4px" class="contentLinkGrey">
        AppID
        <input id="appIDInt" type="text" value="<cfoutput>#appID#</cfoutput>" class="formText" style="width:40px; text-align:center" disabled="disabled">
        </div>
        </cfif>
        
        </cfif>
    </div>
    
</cfif>

<cfif appID GT 0>
    <div style="float:left; margin-top:20px">

        <div style="width:100px; float:left; text-align:right; padding-top:6px; padding-right:4px" class="contentLinkGrey">Assets ZIP file</div>
        
        <div style="float:left">
        <input name="importFile" id="importFile" type="file" class="formText sqButton" style="padding: 0px 0px 0px 0px; width:250px" onchange="displayImport();">
        <input name="appID" type="hidden" id="appID" value="<cfoutput>#session.appID#</cfoutput>" />
   		</div>
        
        <div style="width:100px; float:left; text-align:right; padding-top:0px; padding-right:4px" class="contentLinkGrey">
            <select id="importType" style="width:150px; margin-left:5px; height:34px">
               <option value="-1">Select Import Type</option>
              <option value="0">New Import</option>
              <option value="1">Replace Import</option>
            </select>
        </div>
        
        <div style="height:32px; margin-top:20px; margin-left:104px; display:none; height:auto" id="import">
        
            <div id="importMessage" class="contentLinkGrey" style="padding-top:10px; padding-left:10px">
                Please select the ZIP file that contains all assets and the XLS file to describe the import details
            </div>
 
            <input type="submit" value="Import Assets into Project" class="sqButton contentLinkGrey" style="height:38px; padding:4 4 4 4; margin-top:10px; background-color:#EEE">

        </div>
        
        
        
    </div>
</cfif>

</form>
</div>

<div style="width:800px;">
<div class="contentLinkGrey" id="help" style="margin-top:20px; display:none; float:none; margin-top:50px">
	<p>
    <div><br />
    	1) Prepare folder structure. This is a sample structure in preparation for the batch import<br />
    	<img src="images/importer-setup.png" style="margin:10px" />
    </div><p>
    <div style="margin-top:10;">
    	2) Download this template and fill in the information
        <a href="images/import.xls" target="_blank">Sample Import XLS Templpate</a>
        This is a sample of the XLS Import file that is in the root of the directory
        <img src="images/import-template.png" style="margin:10px" />
    </div>
    <p>
    <div style="margin-top:10;">
    	3) Then ZIP the file and select for import
    </div>

</div>
</div>
