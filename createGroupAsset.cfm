<cfparam name="subgroupID" default="0">
<cfparam name="assetID" default="0">

<cfif session.appID GT '0'>

	<cfif assetID GT '0'>
    	<!--- New Group Asset --->
        <cfinvoke component="CFC.Modules" method="addGroupAsset" returnvariable="newGroupID">
            <cfinvokeargument name="appID" value="#session.appID#"/>
            <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
            <cfinvokeargument name="assetID" value="60"/>
        </cfinvoke>
        
        <cflocation url="AppsView.cfm?subgroupID=#subgroupID#" addtoken="no">
        
    <cfelse>
    	<!--- New Group --->
        <cfinvoke component="CFC.Modules" method="addGroup" returnvariable="newGroupID">
            <cfinvokeargument name="appID" value="#session.appID#"/>
            <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
        </cfinvoke>
        
        <cflocation url="AppsView.cfm?subgroupID=#newGroupID#&amp;editGroupName=true" addtoken="no">
        
    </cfif>    

</cfif>

