<cfset clientID = SESSION.clientID>
<cfset appID = SESSION.appID>

<cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="curDate" />
<cfset theReportDate = dateFormat(curDate,'MMM-DD-YYYY')>

<cfinvoke component="CFC.Apps" method="getAppName" returnvariable="appName">
    <cfinvokeargument name="appID" value="#appID#"/>
</cfinvoke>

<cfinvoke component="CFC.Clients" method="getClientInfo" returnvariable="clientName">
    <cfinvokeargument name="clientID" value="#clientID#"/>
</cfinvoke>

<cfset CompanyName = clientName.company>
    

<cfif clientID GT 0 AND appID GT 0>

	<cfset filename = '#CompanyName#-#appName#.json'>
      
    <cfcontent type="text/plain"> 
    <cfheader name="Content-Disposition" value="attachment; filename=#filename#">
    <cfsetting showdebugoutput="no">
   
   <cfif NOT isDefined('session.userDev')>
   		<cfset dev = 0>
   	<cfelse>
   		<cfset dev = 1>
	</cfif>
   
    <cfinvoke component="API.v11.LiveAPI" method="getContentJSON" returnvariable="data">
        <cfinvokeargument name="groupID" value="-1"/>
        <cfinvokeargument name="appID" value="#appID#"/>
        <cfinvokeargument name="dev" value="#dev#"/>
    </cfinvoke>
  
    <cfoutput>#data#</cfoutput>

</cfif>

