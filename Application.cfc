<cfcomponent>

	<!--- <cfif NOT IsDefined("session.clientID")>
    	No Session for ClientID
    <cfelse>
		<cfdump var="#session.clientID#">
    </cfif>
    
    <cfif NOT IsDefined("session.appID")>
    	No Session for AppID
    <cfelse>
		<cfdump var="#session.appID#">
    </cfif> --->

	<!--- Set application name and enable Client and Session variables. --->
    <cfset THIS.Name="Live_Plateform">
    <cfset THIS.sessionManagement = true />
	<cfset THIS.sessionTimeout = createTimeSpan( 0, 4, 0, 0 ) /> 

    <!--- Set datasource --->
    <!---<cfset THIS.Datasource="KLOCKWERKS_DEV">--->
    <cfset THIS.Datasource="wavecoders_dev">
    <!--- <cfset THIS.Datasource="kw_dev"> --->
    
    <!--- <cfset restPath = expandPath("./API")>
    <cfset restInitApplication(restPath,"api")> --->
    
    <!--- onApplicationStart() --->
    <cffunction name="onApplicationStart">

		<!--- baseURL --->
        <cfset Application.baseURL = expandPath("../../")>

		<!---Define Client ID and App ID--->               
        <cfif NOT IsDefined("session.apps")><cfset session.apps = []></cfif>
        <cfif NOT IsDefined("session.clients")><cfset session.clients = []></cfif>
       
        <cfif NOT IsDefined("session.clientID")><cfset session.clientID = 0></cfif>
        <cfif NOT IsDefined("session.appID")><cfset session.appID = 0></cfif>
		<cfif NOT IsDefined("session.auth_token")><cfset session.auth_token = ''></cfif>
        
		<cfif NOT IsDefined("session.assetPaths")><cfset session.assetPaths = structNew()></cfif>
        <cfif NOT IsDefined("session.info")><cfset session.info = structNew()></cfif>
        <cfif NOT IsDefined("session.userInfo")><cfset session.userInfo = structNew()></cfif>
        <cfif NOT IsDefined("session.userAccess")><cfset session.userAccess = structNew()></cfif>
        
        <cfif NOT IsDefined("session.userDev")><cfset session.userDev = 1></cfif>
        
        <cfset session.serverVersion="0">
        
        <cfset session.clientID = "0">
        <cfset session.appID = "0">
        <cfset session.auth_token = "0">

        <!---Asset Types--->
        <!--- <cfinvoke component="CFC.Assets" method="getAssetsTypes" returnvariable="assetTypes" />
        <cfdump var="#assetTypes#">
        <cfset session.AssetsTypes = assetsTypes> --->

    </cffunction>
    
</cfcomponent>