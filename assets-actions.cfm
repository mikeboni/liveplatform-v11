<cfparam name="AssetLibAppID" default="#session.appID#">

<cfinvoke  component="CFC.Apps" method="getClientApps" returnvariable="apps">
  <cfinvokeargument name="clientID" value="#session.clientID#"/>
</cfinvoke>

<div class="detailsHide" id="AssetView">

<!--- Assets --->
  <div class="sectionLink" style="margin-left:0px; margin-top:5px; height:auto; width:810px;">
    <div class="sectionLink" style="background-color:#666; height:40px; padding-left:10px; width:800px">
    	<div style="width:200px;float:left; height: 40px;line-height: 40px;">Assets</div>

        <div style="width:44px;float:right; height: 40px;line-height: 40px;" onclick="displayImporterType('AssetsImporter')"><img src="images/import.png" width="44" height="44" /></div>
        
    </div>
    <!--- Import Assets --->
    <div style="width:800px; display:none;margin-left:0px; margin-top:5px; height:auto; width:810px; margin-bottom:10px" id="AssetsImporter"> 
		<cfoutput>
        <div style="margin-top: 10px;height: 50px; width:100%; margin-bottom:10px">
        <form id="theAssets" style="height:44px;">

        <select name="app" id="app" style="height:44px; width:200px; margin-top:4px;" onchange="displayAssets(this.form, 'displayAssetsAssets')">
        <option value="0">Select App</option>
        <cfloop query="#apps#">
            <option value="#APP_ID#" <cfif AssetLibAppID IS APP_ID> selected</cfif>>#APPNAME#</option>
        </cfloop>
        </select>
        
          <select name="assetTypes" id="assetTypes" onchange="displayAssets(this.form, 'displayAssetsAssets')" style="height:44px; margin:0px;">
            <option value="0">All Asset Types</option>
            <cfloop query="assetTypes">
                <option value="#type#">#name#</option>
            </cfloop>
          </select>
          <input type="button" class="formText" value="Import Content" onclick="allSelectedContent(this)" />
          <input name="contentIDs" type="hidden" id="contentIDs" />
          <div class="contentLinkGrey" id="totalAssets" style="width:180px; float:right; padding-top:15px"></div>
        </form>
        </div>
        </cfoutput>
        <div id="displayAssetsAssets" style="overflow:scroll; margin-top: 0px; color:#666; height:400px; overflow-x: hidden;border-style: solid; border-width: 1px; margin-bottom:40px; padding:10px"></div>
     </div>
    
    
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="content" bgcolor="#FFF">
            <tr>
              <td>
              
              <table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="#CCC">                  
                        <tr class="content">
                          <td height="44" style="padding-left:10px">
                          Asset Name
                          </td>
                          <td width="80" align="center" class="content">
                            Access
                          </td>
                          <td width="60" align="center" class="content">  
                          Cached
                          </td>
                          <td width="60" align="center" class="content">
                           Active
                          </td>
                          <td width="60" align="center" class="content">
                          Shared</td>
                          <td width="80" align="center" class="content">Sort Order </td>
                          <td width="60" align="right" class="content"></td>
      </tr>  
    </table>
    
    <cfif groupAssets.recordCount GT 0>
	<div id="assetsListing">
    <cfoutput query="groupAssets">
    
    <cfif active>
			<cfset state = "contentLink">
        <cfelse>
            <cfset state = "contentLinkDisabled">
    </cfif>
    
    <div class="rowhighlighter" onmouseover="showDelete(this,1);" onmouseout="showDelete(this,0);">
                        
                        <form id="content_id">
                        <table width="810" border="0" cellpadding="5" cellspacing="0">                  
                        <!--- Assets --->
                        <tr>
                          <td width="44">
                            <img src="images/#icon#" width="44" height="44" />
                          </td>
                          <td colspan="2">
                          <a href="AssetsView.cfm?assetID=#content_id#&amp;assetTypeID=#assetType_id#" class="#state#">
                          <div style="height:32px; padding-top:15px; padding-left:4px">#name#</div>
                          </a>
                          </td>
                          <td width="80" align="center" class="content">
    
                            <cfif accessLevel IS 0>
                                <cfset lock = 'access_unlocked'>
                            <cfelse>
                                <cfset lock = 'access_locked-'& groupAssets.accessLevel>
                            </cfif>
     
                            <select 
                              name="accessLevel" 
                              id="accessLevel" 
                              style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" 
                               type="button" onchange="updateContentAssetOptions(this,#content_id#)" />
                                <cfloop query="accessLevels">
                                <option value="#accessLevel#" style="color:##333" <cfif groupAssets.accessLevel IS accessLevel> selected</cfif>>#levelName#</option>
                                </cfloop>
                            </select>

                          </td>
                          <td width="60" align="center" class="content">  
                          <cfif cached IS '0'>
                          <cfset theCacheState = 'cache'>
                          <cfelse>
                          <cfset theCacheState = 'cached'>
                          </cfif>
                          <div id="contentCached" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer; width:28px; height:28px; background-image:url(images/#theCacheState#.png)">
                          </div>
                          </td>
                          <td width="60" align="center" class="content">
                          
                         
                          <cfif active IS '0'>
                            <cfset stateCSS = "contentLinkGreen">
                          <cfelse>
                            <cfset stateCSS = "contentLinkRed">
                          </cfif>
                          <div id="contentActive" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer" class="#stateCSS#">
                              <cfif active IS '0'>
                              NO
                              <cfelse>
                              YES
                              </cfif>
                          </div>
                          </td>
                          <td width="60" align="center" class="content">
                          
                          <cfif sharable IS '0'>
                            <cfset stateCSS = "contentLinkGreen">
                          <cfelse>
                            <cfset stateCSS = "contentLinkRed">
                          </cfif>
                          <div id="contentShared" style="cursor:pointer" onclick="updateContentAssetOptions(this,#content_id#)" class="#stateCSS#">
                          <cfif sharable IS '0'>
                          NO
                          <cfelse>
                          YES
                          </cfif>
                          </div>
                          
                          </td>
                          <td width="80" align="center" class="contentLinkDisabled">
                          <input name="sortOrder" type="text" id="sortOrder" value="#sortOrder#" size="4" maxlength="10" onchange="updateContentAssetOptions(this,#content_id#)" /></td>
                          <td width="60" align="right" class="content">
                          
                          <table border="0">
                          <tr>
                            <td>
                            <input type="button" class="itemHide" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteContentAsset(#content_id#,this);" value="" /> 
                            </td>
                          </tr>
                          
                          
                          
                        </table>
                          
                          
                          </td>
                        </tr>
                        
                          </table>
                        </form>
    </div>
    
    </cfoutput>   
    </div>
    
    <cfelse>
    <span class="content">
    No Assets
    </span>
    </cfif>

          
          </td>
      </tr>
      </table>
   	
       
  </div>
  
  </div> 
  
   <div class="detailsHide" id="TargetView">
<!--- Actions --->
  <div class="sectionLink" style="margin-left:0px; margin-top:5px; height:auto; width:810px;">
    <div class="sectionLink" style="background-color:#666; height:40px; padding-left:10px; width:800px">
    	<div style="width:200px;float:left; height: 40px;line-height: 40px;">Actions</div>
        
        <div style="width:44px;float:right; height: 40px;line-height: 40px;" onclick="displayImporterType('ActionsImporter')"><img src="images/import.png" width="44" height="44" /></div>
 
        </div>
    </div>
 	<!--- Importer --->
    <div style="width:800px; display:none;margin-left:0px; margin-top:5px; height:auto; width:810px;" id="ActionsImporter"> 
		<cfoutput>
        <div style="margin-top: 10px;height: 50px; width:100%; margin-bottom:10px">
        <form id="theActions" style="height:44px">
        
        <select name="app" id="app" style="height:44px; width:200px; margin-top:4px;" onchange="displayAssets(this.form, 'displayAssetsActions')">
        <option value="0">Select App</option>
        <cfloop query="#apps#">
            <option value="#APP_ID#" <cfif AssetLibAppID IS APP_ID> selected</cfif>>#APPNAME#</option>
        </cfloop>
        </select>
        
          <select name="assetTypes" id="assetTypes" onchange="displayAssets(this.form,'displayAssetsActions')" style="height:44px; margin:0px;">
            <option value="0">All Asset Types</option>
            <cfloop query="assetTypes">
                <option value="#type#">#name#</option>
            </cfloop>
          </select>
          <input type="button" class="formText" value="Import Content" onclick="allSelectedContent(this)" />
          <input name="contentIDs" type="hidden" id="contentIDs" />
          <div class="contentLinkGrey" id="totalAssets" style="width:180px; float:right; padding-top:15px"></div>
        </form>
        </div>
        </cfoutput>
        <div id="displayAssetsActions" style="overflow:scroll; margin-top: 0px; color:#666; height:400px; overflow-x: hidden;border-style: solid; border-width: 1px; margin-bottom:40px; padding:10px"></div>
     </div>


    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="content" bgcolor="#FFF">
            <tr>
              <td>
              
              <table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="#CCC">                  
                        <tr class="content">
                          <td height="44" style="padding-left:10px">
                          Asset Name
                          </td>
                          <td width="80" align="center" class="content">
                            Access
                          </td>
                          <td width="60" align="center" class="content">  
                          Cached
                          </td>
                          <td width="60" align="center" class="content">
                           Active
                          </td>
                          <td width="60" align="center" class="content">
                          Shared</td>
                          <td width="80" align="center" class="content">Sort Order </td>
                          <td width="60" align="right" class="content"></td>
      </tr>  
    </table>
    
    <cfif groupActions.recordCount GT 0>
    <div id="actionListing">
    <cfoutput query="groupActions">
    
    <cfif active>
			<cfset state = "contentLink">
        <cfelse>
            <cfset state = "contentLinkDisabled">
    </cfif>
    
    <div class="rowhighlighter" onmouseover="showDelete(this,1);" onmouseout="showDelete(this,0);">
                        
                        <form id="content_id">
                        <table width="810" border="0" cellpadding="5" cellspacing="0">                  
                        <!--- Assets --->
                        <tr>
                          <td width="44">
                            <img src="images/#icon#" width="44" height="44" />
                          </td>
                          <td colspan="2">
                          <a href="AssetsView.cfm?assetID=#content_id#&amp;assetTypeID=#assetType_id#" class="#state#">
                          <div style="height:32px; padding-top:15px; padding-left:4px">#name#</div>
                          </a>
                          </td>
                          <td width="80" align="center" class="content">
    
                            <cfif accessLevel IS 0>
                                <cfset lock = 'access_unlocked'>
                            <cfelse>
                                <cfset lock = 'access_locked-'& groupActions.accessLevel>
                            </cfif>

                            <select name="accessLevel" id="accessLevel" style="background:url(images/#lock#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="updateContentAssetOptions(this,#content_id#)" />
                                <cfloop query="accessLevels">
                                <option value="#accessLevel#" style="color:##333" <cfif groupActions.accessLevel IS accessLevel> selected</cfif>>#levelName#</option>
                                </cfloop>
                            </select>
                          
                          </td>
                          <td width="60" align="center" class="content">  
                          <cfif cached IS '0'>
                          <cfset theCacheState = 'cache'>
                          <cfelse>
                          <cfset theCacheState = 'cached'>
                          </cfif>
                          <div id="contentCached" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer; width:28px; height:28px; background-image:url(images/#theCacheState#.png)">
                          </div>
                          </td>
                          <td width="60" align="center" class="content">
                          
                         
                          <cfif active IS '0'>
                            <cfset stateCSS = "contentLinkGreen">
                          <cfelse>
                            <cfset stateCSS = "contentLinkRed">
                          </cfif>
                          <div id="contentActive" onclick="updateContentAssetOptions(this,#content_id#)" style="cursor:pointer" class="#stateCSS#">
                              <cfif active IS '0'>
                              NO
                              <cfelse>
                              YES
                              </cfif>
                          </div>
                          </td>
                          <td width="60" align="center" class="content">
                          
                          <cfif sharable IS '0'>
                            <cfset stateCSS = "contentLinkGreen">
                          <cfelse>
                            <cfset stateCSS = "contentLinkRed">
                          </cfif>
                          <div id="contentShared" style="cursor:pointer" onclick="updateContentAssetOptions(this,#content_id#)" class="#stateCSS#">
                          <cfif sharable IS '0'>
                          NO
                          <cfelse>
                          YES
                          </cfif>
                          </div>
                          
                          </td>
                          <td width="80" align="center" class="contentLinkDisabled">
                          <input name="sortOrder" type="text" id="sortOrder" value="#sortOrder#" size="4" maxlength="10" onchange="updateContentAssetOptions(this,#content_id#)" /></td>
                          <td width="60" align="right" class="content">
                          
                          <table border="0">
                          <tr>
                            <td>
                            <input type="button" class="itemHide" style="background:url(images/cancel.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteContentAsset(#content_id#,this);" value="" /> 
                            </td>
                          </tr>
                          
                          
                          
                        </table>
                          
                          
                          </td>
                        </tr>
                        
                          </table>
                        </form>
    </div>
    
    </cfoutput>   
    </div>
    <cfelse>
    <span class="content">
    No Actions
    </span>
    </cfif>

          
          </td>
      </tr>
      </table>
   	
       
  </div>
  
  </div> 