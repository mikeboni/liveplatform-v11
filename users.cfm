<cfparam name="appID" default="">
<cfparam name="assetTypeTab" default="0">
<cfparam name="newUser" default="no">
<cfparam name="error" default="">
<cfparam name="search" default="">
<cfparam name="emailsupport" default="no">
<cfparam name="activeUser" default="0">
<cfparam name="userType" default="0">

<cfparam name="access" default="4">
<cfparam name="searchByLast" default="A">

<cfif search NEQ ''>
	<cfset access = -1>
    <cfset searchByLast = 'ALL'>
</cfif>

<cfif search NEQ ''><cfset activeUser = 1></cfif>

<cfif NOT IsDefined("session.appID")><cfset session.appID = '0'></cfif>
<cfif appID NEQ ''><cfset session.appID = appID></cfif>

<cfif NOT IsDefined("session.clientID")><cflocation url="ClentsView.cfm" addtoken="no"></cfif>
<!--- <cfif appID IS '0' OR appID IS ''><cflocation url="CientsView.cfm" addtoken="no"></cfif> --->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Live-Applications</title>
<link href="styles.css" rel="stylesheet" type="text/css" />

<link href="JS/mcColorPicker/mcColorPicker.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="JS/classSelector.js"></script>
<script type="text/javascript" src="JS/randomGenerator.js"></script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="JS/multiple-dropdown/jquery.sumoselect.js"></script>
<link href="JS/multiple-dropdown/sumoselect.css" rel="stylesheet" />

<script type="text/javascript">
	$(document).ready(function () {
		window.asd = $('.SlectBox').SumoSelect({ csvDispCount: 3 });
		window.test = $('.testsel').SumoSelect({okCancelInMulti:true });
		window.testSelAll = $('.testSelAll').SumoSelect({okCancelInMulti:true, selectAll:true });
		window.testSelAll2 = $('.testSelAll2').SumoSelect({selectAll:true });
	});
</script>
<style type="text/css">
	body{
		font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color:#444;font-size:13px;
	}
	p,div,ul,li{
		padding:0px; margin:0px;
	}
	
	input[type=checkbox] {
	  transform: scale(1.5);
	}
	
	.selector {}

</style>

<script type="text/javascript">

	var newAcctDisplay = false;
	var Client;
	var Clients = [];
	var App;
	var Users;
	
	
function newAccount() {

	showNewAcct();

	if(Client && App) {
		//nothing - all good
	} else {
		alert('Select a Client and App to create a User');
		return false;
	}

	updateUserAccess(0);

	theForm = document.getElementById('newUser');
	theForm.reset();
	
	
	return true
}
	
function updateUserAccessDisplay(theSel) {
	
	theSelOption = theSel.options[theSel.selectedIndex].value;

	if(theSelOption == 0) { 
		icon = 'access_unlocked' +'.png';
	} else {
		icon = 'access_locked-'+ theSelOption  +'.png';
	}
	
	theIcon = "url(images/"+ icon +")";
	theSel.style.background = theIcon;
	
}

	
function showNewAcct(theState)	{
	
	if(theState) {
		newAcctDisplay = theState;
	} else {
		newAcctDisplay = !newAcctDisplay;
	}
	
	theAcct = document.getElementById('newAccount');
	
	if(newAcctDisplay) {
		displayType = 'block';
	} else {
		displayType = 'none';
	}
	
	theAcct.style.display = displayType;
}
	
function showDelete(theObj, state)	{
	
	theObjRef = document.getElementById(theObj);
	
	if(state)
	{
		removeClass(theObjRef,"itemHide");
		addClass(theObjRef, "itemShow");
	}else{
		removeClass(theObjRef,"itemShow");
		addClass(theObjRef, "itemHide");
	}
	
}

function deleteUser(userID) {
		if(confirm('Are you sure you wish to Delete this User?'))
		{
			deleteUserFromDB(userID);
		}else{ 
			//nothing
		}
}

function showReports(token)
{
	window.open('http://liveplatform.net/reports.cfm?auth_token='+token	, '_new');
}

</script>

<!--- Access Levels --->
<cfinvoke component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />


<!--- Group Access--->
<cfajaxproxy cfc="CFC.Access" jsclassname="accessManager">

<!--- Active State and Order--->
<cfajaxproxy cfc="CFC.Users" jsclassname="setMyActiveState">

<!--- LIVE--->
<cfajaxproxy cfc="LiveAPI" jsclassname="regApi">

<!--- USERS --->
<cfajaxproxy cfc="CFC.userAccounts" jsclassname="users">

<script>

	function displayClientList()	{
		
		var jsUsers = new users();
		
		jsUsers.setSyncMode();
		jsUsers.setCallbackHandler(displayClientsSuccess);
		jsUsers.getClients();
									  
	}
	
	function displayClientsSuccess(clientList) {

		Clients = clientList;
		
		theSel = document.getElementById('clients');
		
		var option = document.createElement("option");
		option.value = 0;
		option.text = 'Select a Client';
		theSel.appendChild(option);
		allClients = <cfoutput>#SerializeJSON(session.clients)#</cfoutput>
		
		clientList.forEach(function(client) {
			if(client.active && allClients.indexOf(client.client_id) > -1) {
				var option = document.createElement("option");
				option.value = client.client_id;
				option.text = client.companyName;
				theSel.appendChild(option);
			}
			
		});
		
	}
	
	function selectClient(theSel)	{
		
		clientID = parseInt(theSel[theSel.selectedIndex].value);
		
		Client = null;
		App = null;
		Users = null;
		
		clearUserList();
		
		Clients.forEach(function(client) {
			if(client.client_id == clientID) {
				Client = client;
			}
		})
		
		displayClientIcon();
		displayAppList();
		getUserList();
	}
	
	function displayClientIcon()	{
		
		theImg = document.getElementById('ClientIcon');
		
		if(Client) {
			thePath = 'http://liveplatform.net/'+ Client.filePath +'images/'+ Client.clientIcon;
			theImg.src = thePath;
			theImg.style.display = 'block';
		} else {
			App = null;
			theImg.style.display = 'none';
			document.getElementById('AppListing').style.display = 'none';
		}
		
	}
	
	//APP
	function selectApp(theSel)	{
		
		appID = parseInt(theSel[theSel.selectedIndex].value);
		
		App = null;
		
		Apps.forEach(function(app) {
			if(app.app_id == appID) {
				App = app;
			}
		})
		
		if(appID == 0) { App = null }
		
		getUserList();
		displayAppIcon();
	}
	
	function displayAppList() {
		
		var jsUsers = new users();
		
		jsUsers.setSyncMode();
		jsUsers.setCallbackHandler(displayAppListSuccess);
		jsUsers.getClientApps(Client.client_id);

	}
	
	function displayAppListSuccess(apps) {
		
		Apps = apps;
	
		theSel = document.getElementById('apps');
		clearSelectOptions(theSel);
		displayAppIcon();
		
		var option;
		
		apps.forEach(function(app) {
			allApps = <cfoutput>#SerializeJSON(session.apps)#</cfoutput>
			if(app.appActive && allApps.indexOf(app.app_id) > -1) {
				var option = document.createElement("option");
				option.value = app.app_id;
				option.text = app.appName;
				theSel.appendChild(option);
			}
			
		});
		
		theSelMigration = document.getElementById('migrateUsers');
		allOptions = theSel.innerHTML
		theSelMigration.innerHTML = allOptions
		theSelMigration.options[0].text = 'None'
		
		document.getElementById('AppListing').style.display = 'block';
		
	}
	
	function displayAppIcon()	{
		
		theImg = document.getElementById('AppIcon');
		theLevel = document.getElementById('userLevel');
		newAcct = document.getElementById('newAcct');
		migrateAcct = document.getElementById('migrateUsers');
		
		if(Client && App) {
			thePath = 'http://liveplatform.net/'+ Client.filePath + App.appPath +'/images/'+ App.appIcon;
			theImg.src = thePath;
			theImg.style.display = 'block';
			newAcct.style.display = 'block'
			migrateAcct.style.display = 'block'
		} else {
			theImg.style.display = 'none';
			newAcct.style.display = 'none'
			migrateAcct.style.display = 'none'
		}
		
		if(Client) {
			theLevel.style.display = 'block'
		} else {
			theLevel.style.display = 'none'
		}
		
	}
	
	function getAppIcon(appID) {
		appInfo = getApp(appID)
		thePath = 'http://liveplatform.net/'+ Client.filePath + appInfo.appPath +'/images/'+ appInfo.appIcon;
		return thePath;
	}
	
	
	function clearSelectOptions(theSel) {

		var i;
		for(i = theSel.options.length - 1 ; i >= 0 ; i--)
		{
			theSel.remove(i);
		}
		
		option = document.createElement("option");
		option.value =0;
		option.text = 'All Applications';
		theSel.appendChild(option);
		
	}
	
	function getUserList()	{
		
		var jsUsers = new users();
		
		clientID = Client.client_id;
		
		if(App) {
			appID = App.app_id;
		} else {
			appID = 0;
		}

		accessLevel = document.getElementById('userLevel');
		level = parseInt(accessLevel[accessLevel.selectedIndex].value);
		
		jsUsers.setSyncMode();
		jsUsers.setCallbackHandler(getUserListSuccess);
		jsUsers.getUserAccounts(clientID, appID, level);
			 
	}
	
	function getUserListSuccess(users)	{
		Users = users;
		displayUserList();
	}
	
	function clearUserList()	{
		var userList = document.getElementById("userList");
		userList.innerHTML = "";
	}
	
	function displayUserList()	{
		
		var userList = document.getElementById("userList");
		userList.innerHTML = "";
		
		var header = '';
		
		header+='<table width="810" border=0 cellpadding=5 cellspacing=0>'
		header+='<tr>'
		header+='<td bgcolor="#666" class="contentGreyed" width="283" height="32">User Name</td>'
		header+='<td width="37" align="center" bgcolor="#666" class="contentGreyed">DEV</td>'
		header+='<td width="50" bgcolor="#666" class="contentGreyed">&nbsp;</td>'
		header+='<td width="90" align="center" bgcolor="#666" class="contentGreyed">&nbsp;</td>'
		header+='<td width="114" align="right" bgcolor="#666" class="contentGreyed">Password</td>'
		header+='<td width="50" align="center" bgcolor="#666" class="contentGreyed">Access</td>'
		header+='<td width="50" align="center" bgcolor="#666" class="contentGreyed">Active</td>'
		header+='<td width="55" align="center" bgcolor="#666" class="contentGreyed"><input type="checkbox" name="selectAll" onClick="selectAllUsers(this.checked)" style="position: relative; left: 0px; visibility: visible;"></td>'
		header+='</tr>'
		header+='</table>'
		
		var headerItem = document.createElement('div');      
		headerItem.innerHTML = header;
		userList.appendChild(headerItem);
		
		var selAccess = generateAccessSelect();
		var userAppID = 0;
		
		if(Users.length == 0) {
			
			var contents = '';
			contents+='<table width="810" border=0 cellpadding=5 cellspacing=0 class="contentLinkGrey">'
			contents+='<tr>'
			contents+='<td>No Records Found</td>'
			contents+='</tr>'
			contents+='</table>'
			
			var theUserItem = document.createElement('div');      
			theUserItem.innerHTML = contents;
			userList.appendChild(theUserItem);
		}
		
		Users.forEach(function(user) {
			
			//contents = 'name:'+ user.name +', email:'+ user.email +', id:'+ user.user_id +', dev:'+ user.dev +', active:'+ user.active, +', level:'+ user.level +', pass:'+ user.password +', activated:'+ user.activate, user.app_id;
			
			var contents = '';
			
			allApps = <cfoutput>#SerializeJSON(session.apps)#</cfoutput>
			if(allApps.indexOf(user.app_id) > -1) {

				if(user.app_id != userAppID && !App) {
					userAppID = user.app_id;

					contents+='<table width="810" border=0 cellpadding=5 cellspacing=0 class="contentLinkGrey" bgcolor="#eee" height="44" style="margin-top:20px">';

					theUserApp = getApp(user.app_id);

					contents+='<tr><td width="44" style="border-bottom: thin solid #aaa"><img src="'+ getAppIcon(user.app_id) +'" height="44"></td><td style="border-bottom: thin solid #aaa">'+ theUserApp.appName +'</td></tr>'
					contents+='</table>'
				}

				contents+='<table width="810" border=0 cellpadding=5 cellspacing=0>'
				contents+='<tr>'
				contents+='<td width="250">'
				contents+='<div style="padding: 5px; float: left">'
				contents+='<span class="contentLinkGrey">'+ user.name +'</span><br>'

				disabledContent = "contentLink";
				if(!user.active) { disabledContent = "contentLinkDisabled" }

				contents+='<a href="mailto:'+ user.email +'?subject=ONE2 Support" class="'+ disabledContent +'">'+ user.email +'</a>'
				contents+='<div>'
				contents+='</td>'
				contents+='<td width="10">'
				contents+='<div style="padding: 5px; float: left">'
				if(user.dev == 1) { checkedState = 'checked="checked"'} else { checkedState = "" }
				contents+='<input type="checkbox" id="'+ user.user_id +'" value="1" onclick="setDevState('+ user.user_id +', this.checked)" '+ checkedState +'"><label for="'+ user.user_id +'"></label>'
				contents+='<div>'
				contents+='</td>'
				contents+='<td width="44">'

				if(user.token != ''){
					contents+='<div onclick="showReports(\''+ user.token +'\')" style="width:44px">'
					contents+='<img src="images/reports.png" width="22" height="22" border="0">'	
				}

				contents+='</div>'	
				contents+='</td>'
				contents+='<td width="80">'
				contents+='<div onclick="resendRegInfo(\''+ user.email +'\')" style="width:44px">'
				contents+='<table width="100%" border="0" cellpadding="0" cellspacing="5">'
				contents+='<tr>'
				contents+='<td align="right" valign="middle">RS</td>'
				contents+='<td width="22" valign="middle"><img src="images/resend.png" width="22" height="22" border="0"></td>'
				contents+='</tr>'
				contents+='</table>'
				contents+='</div>'		
				contents+='</td>'
				contents+='<td width="100" align="right">'+ user.password +'</td>'
				contents+='<td width="44">'

				selObj = selAccess.cloneNode(true);

				if(!user.accessLevel) { 
					icon = 'access_unlocked';
					selObj[0].setAttribute('selected', true);
				} else {
					selObj[user.accessLevel].setAttribute('selected', true);
					icon = 'access_locked-'+ user.accessLevel  +'.png';
				}

				contents+='<select id="userLevel" style="background:url(images/'+ icon +') no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" onchange="updateUserAccess('+ user.user_id +', this.options[this.selectedIndex].value)">'

				contents+=selObj.innerHTML;

				contents+='</select>'

				contents+='</td>'
				contents+='<td width="44">'
				contents+='<div style="text-align: center;">'

				if(user.active) {
					isActive = "YES";
					classActive = "contentLinkRed"
				} else {
					isActive = "NO";
					classActive = "contentLinkGreen"
				}
				contents+='<a class="'+ classActive +'" style="cursor:pointer;" onclick="toggleActiveState(this,'+ user.user_id +')">'+ isActive +'</a>'

				contents+='</div>'
				contents+='</td>'
				contents+='<td width="44" style="padding-right: 17px">'
				//contents+='<input type="button" style="background:url(images/remove.png) no-repeat; border:0; width:44px; height:44px;" onclick="deleteUser('+ user.user_id +');">'
				contents+='<input name="sel" id="sel" style="position: relative; left: 21px; visibility: visible;" class="selector" type="checkbox" value="'+ user.user_id +'">'
				contents+='</td>'
				contents+='</tr>'
				contents+='</table>'

				var theUserItem = document.createElement('div');      
				theUserItem.innerHTML = contents;
				userList.appendChild(theUserItem);
			}
		})
		
		
	}
	
	function generateAccessSelect()	{
		
		var selectMenu = document.createElement('select');
		selectMenu.style = 'background: url(images/access_locked-4.png) no-repeat; border: 0; width: 44px; height: 44px; color: rgba(0,0,0,0)';
		selectMenu.onchange = 'alert("test")';
		
		var levelAccessItems = ['Guest', 'General User', 'Unique User', 'Manager', 'Admin', 'Super'];
		
		for(i=0; i < levelAccessItems.length; i++) {
			option = document.createElement( 'option' );
			option.value = i;
			option.textContent = levelAccessItems[i];
			option.style.color = '#333';
			selectMenu.appendChild(option);
		}

		return selectMenu
		
	}
	
function cloneObject(obj) {
	if (obj === null || typeof obj !== 'object') {
		return obj;
	}
}
	
function getApp(appID) {
	for(i=0; i < Apps.length; i++) {
		if(Apps[i].app_id == appID) { return Apps[i] }
	}
	return null
}


toggleActiveState = function(theObject,userID)
{
	var jsState = new setMyActiveState();
    var theState = theObject.innerHTML;
	
	if(theState == 'YES') {
		theObjState = 'NO';
		theNewState = 0;
		CSSStyle = 'contentLinkGreen';
	} else {
		theObjState = 'YES';
		theNewState = 1;
		CSSStyle = 'contentLinkRed';
	}
		
	theObject.className = CSSStyle;
	theObject.innerHTML = theObjState;
	
	jsState.setSyncMode();
	jsState.setUserActiveState(userID,null,theNewState);
	jsState.setCallbackHandler(getUserList());
	
}

deleteUserFromDB = function(userID)
{
	
	var jsState = new setMyActiveState();
	
	jsState.deleteUser(userID);
	jsState.setCallbackHandler(showStateResult);
	
	getUserList();
}


createNewUser = function(clientType)
{
	var jsState = new setMyActiveState();

	var form = document.newUser;
	
	var userName = form.userName.value;
	var userEMailAddr = form.userEmailAddress.value;
	var userLevel = parseInt(form.userLevel[form.userLevel.selectedIndex].value);
	
	var userCode = makeID(10,1);
	var userPass = form.userPass.value;
	
	var clientID = Client.client_id;
	var appID = App.app_id;
	
	if(clientType) {
		type = 1
	}else{
		type = 0
	}
	
	jsState.setSyncMode();
	jsState.registerUser(userName, userEMailAddr, userCode,'', userPass, userLevel, clientID, appID, type);
	jsState.setCallbackHandler(createNewUserSuccess());
	
	form.reset();
	updateUserAccessDisplay(form.userLevel);

}

function createNewUserSuccess() {
	getUserList(); 
	showNewAcct(0)
}

showStateResult = function(result){
	getUserList();
}

<!--- Resend Registration --->
resendRegInfo = function(email)
{
	var jsRegAccess = new regApi();
	
	jsRegAccess.setSyncMode();
	jsRegAccess.setCallbackHandler(resendSuccess);
	jsRegAccess.resendUserRegistrationInfo(Client.client_id,email);
}

resendSuccess = function(result){
	alert('Resent Registration EMail to user');
}


<!--- setPassword --->
resendSetPassword = function(email)
{
	var jsRegAccess = new regApi();
	
	jsRegAccess.setSyncMode();
	jsRegAccess.setCallbackHandler(resendPassSuccess);
	jsRegAccess.resendSetPassword(Client.client_id,email);
}

resendPassSuccess = function(result){
	alert('Resent Define Password EMail to User');
}


function addUsers(groupID)	{
	
	//console.log(groupID);
		
}

function removeUser(userID)	{
	
	//console.log(userID);
		
}

function checkmarkUser(formButton, groupID, userID)	{

	//console.log(formButton, groupID, userID);
	
	if(formButton.className == "checkmarkNormal") {
		formButton.className ="";
		formButton.classList.add("checkmarkSelected");
	}else{
		formButton.className ="";
		formButton.classList.add("checkmarkNormal");
	}
	
}

function removeUser(groupID, userID)	{
	
	var jsAccess = new accessManager();
	
	if(confirm('Are you sure you wish to Remove User from this Group Access?'))
	{
		jsAccess.setSyncMode();
		jsAccess.setCallbackHandler(removeUserSuccess);
		jsAccess.removeUsersAccess(groupID, userID);
	}else{
		//nothing	
	}
}

function removeUserSuccess(result)	{
	
	getUserList();
}

//set user access
function setGroupAccessForUser(groupID, userID)	{
	
	var jsAccess = new accessManager();
	
	if(groupID == null) { 
		groupID = [];
	}
	//console.log(groupID);
	jsAccess.setSyncMode();
	jsAccess.setCallbackHandler(setGroupAccessForUserSuccess);
	jsAccess.setUserGroupAccess(userID,groupID);
	
}

function setGroupAccessForUserSuccess(result)	{
	
	//location.reload();	
}

function importCreateAccounts()	{
	
	var jsAccess = new accessManager();
	
	jsAccess.setSyncMode();
	jsAccess.setCallbackHandler(importCreateNewUsersSuccess);
	jsAccess.setUserGroupAccess(userID,groupID);
}	

function importCreateNewUsersSuccess()	{
	getUserList();
}

//dev state
function setDevState(userID, devState)	{
	var jsState = new setMyActiveState();

	if(devState) { devState = 1 } else { devState = 0 }
	jsState.setSyncMode();
	jsState.setCallbackHandler(setDevStateSuccess);
	jsState.setUserDevState(userID, devState);
}

function setDevStateSuccess(result)	{
	//console.log(result)	
}


function displayImport()	{

	var theObjFile = document.getElementById("importFile");
	
	if(theObjFile.value != '' && theObjFile.value.indexOf('.xlsx') > -1 || theObjFile.value.indexOf('.xls') > -1)	{
	
		var theObj = document.getElementById("import");
		theObj.style.display = "block";
		theObjFile.style.display = "none";
	
	}else{
		theObjFile.value = "";
		alert('The file must be a XLS (Excel) file');	
	}
	
}

function importConfirm()	{
	
	if(confirm('Are you sure you wish to Import and Create these users for this client/app'))
	{
		return true
	}else{ 
		return false
	}
}

<!--- User Access --->
updateUserAccess = function(userID, accessLevel) {
	
	var jsState = new setMyActiveState();

	if(userID > 0)
	{
		jsState.setSyncMode();
		jsState.setCallbackHandler(userAccessUpdatedSuccess);
		jsState.setUserAccessState(null,userID,parseInt(accessLevel));
	}
	
}
								   
function updateAccessLevelIcon (theSel) {
	accessLevel = parseInt(theSel[theSel.selectedIndex].value);
	theSel.style.backgroundImage = "url('images/access_locked-"+ accessLevel +".png')";
	//console.log(accessLevel)
}

userAccessUpdatedSuccess = function(result){
	getUserList();
	//console.log('updating list');
}

function getClients(theSel) {
	var jsUsers = new setMyActiveState();
	
	clientID = parseInt(theSel[theSel.selectedIndex].value);
	
	jsUsers.setSyncMode();
	jsUsers.setCallbackHandler(getClientsSuccess);
	jsUsers.getClients(clientID);
	
}
	
function getUserElements() {
		
		var allElements = document.getElementsByClassName('selector');
		
		var selectedElements = []
		
		for(i=0; i < allElements.length; i++) {
			if(allElements[i].checked) {
				var v = parseInt(allElements[i].value)
				selectedElements.push(v)
			}
		}
		return selectedElements
	}
	
		
function getClientsSuccess(clients) {
	//console.log(clients)
}
	var jsState = new setMyActiveState();
	function migrateUsersList(appID) {
		
		var users = getUserElements()
		if(users.length > 0 ) {
			
			if(confirm('Are you sure you wish to Migrate All Users to this App?'))
			{
//				console.log(App.app_id, parseInt(appID), users)	
				jsState.setSyncMode();
				jsState.setCallbackHandler(migrationStateSuccess);
				jsState.migrateUsers(App.app_id, parseInt(appID), users);
			}
		}
}
						
function migrationStateSuccess(result) {
	console.log('Completed migration')
	getUserList();
}
	
function selectAllUsers(state) {
		
		var allElements = document.getElementsByClassName('selector');
		
		for(i=0; i < allElements.length; i++) {
			allElements[i].checked = state
		}
}
			
function deleteUsersList() {
		
		var jsState = new setMyActiveState();
		var users = getUserElements()
		if(users.length > 0 ) {
			
			if(confirm('Are you sure you wish to Delete Selected Users from this App?'))
			{
				jsState.setSyncMode();
				jsState.setCallbackHandler(deletionStateSuccess);
				jsState.deleteUsers(App.app_id, users);
			}
						
		}
		
}
		
function deletionStateSuccess(result) {
	console.log('Completed Deletion')
	getUserList();
}
		
</script>

</head>

<body>

<cfset serverVr = 11>
<cfparam name="editP" default="NO">
<cfparam name="edit" default="NO">
<cfparam name="assetTypeTab" default="0">

<cfif NOT isDefined('session.serverVersion')>
	<cfset session.serverVersion = serverVr>
<cfelse>
	<cfset session.serverVersion = serverVr>
</cfif>

<style type="text/css">
body {
	margin-top: 0px;
}
</style>

<cfif NOT IsDefined("session.clientID")><cfset session.clientID = '0'></cfif>

<div style="background-color:#666; width:810px;">
<cfoutput>
<div class="mainheading" style="width:810px; height:100px; padding:0px; background-image:url('images/live-platform.png'); background-repeat:no-repeat;">
	
  <cfif error NEQ ''>
    <div class="contentwarning" style="margin-top:12px; margin-left:155px; float:left">#error#</div>
  </cfif>
  
</div>

<div style="width:790px; background-color:##333; height:32px; padding-left:10px; padding-right:10px; padding-top:8px">

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
    <a href="ClentsView.cfm?clientID=0" class="sectionActiveLink">Clients</a> | <a href="users.cfm?clientID=0" class="sectionActiveLink">Users</a>
    </td>
  </tr>
</table>
	
</div>

</cfoutput>
</div>

<!--- Main Header --->

<cfif assetTypeTab IS 0 OR assetTypeTab IS 1>
      <!--- Users --->
  <cfif assetTypeTab IS 0>
  	
    	<cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="curDate" />
    
  	  <!--- Modules --->
      <!--- <cfinvoke component="CFC.Modules" method="getAccessAppGroups" returnvariable="groups">
          <cfinvokeargument name="appID" value="#session.appID#"/>
      </cfinvoke> --->
  
       
       <cfif NOT newUser>
     <table width="810" style="margin-top:5px" border="0" cellpadding="5" cellspacing="0" bgcolor="#AAA">
       <tr>
         <td width="44" align="right" bgcolor="#AAAAAA" class="contentLinkWhite">
		
		<div style="float: left; width: 70%; width: 548px;" class="contentLinkWhite">
		
		<div id="clientListing">
		 <div style="float: left; padding-right: 5px;"><img src="users.cfm" width="38" id="ClientIcon" style="padding-top: 3px; display: none"></div>
 
         <div>
          <select name="clients" id="clients" style="height:40px; margin-top: 2px; float: left; margin-right: 10px; padding-left: 10px" onChange="selectClient(this)"></select>
		  </div>	  
		</div>
	 	
	 	<div id="AppListing" style="display: none">
			 <div style="float: left; padding-right: 5px;">
			 <img src="users.cfm" width="38" id="AppIcon" style="padding-top: 3px; display: none">
			 </div>
			 <div style="float: left">
		  		<select name="apps" id="apps" style="height:40px; margin-top: 2px; float: left; margin-right: 10px; padding-left: 10px; width: 240px" onChange="selectApp(this)"></select>
			 </div>
			 
		 </div>
			 
		 </div>
        
        
         <div style="float: left; width: 138px" class="contentLinkWhite">
         <cfoutput>
         <!--- User Level --->

		 <select name="userLevel" id="userLevel" style="background:url(images/access_unlocked.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0); display: none; float: left" onChange="updateUserAccessDisplay(this);getUserList();">
			  <option value="0" style="color:##333">All Levels</option>
			  <cfloop query="accessLevels">
			  <cfif accessLevel GT 0>
			  <option value="#accessLevel#" style="color:##333">#levelName#</option>
			  </cfif>
			  </cfloop>
		  </select>
			
       		<select name="migrateUsers" id="migrateUsers" style="background:url(images/migrate.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0); display: none" onChange="migrateUsersList(this.value);"></select>
       
        </cfoutput>
         </div>
  
         	<div style="cursor: pointer; display: none" id="newAcct">
				<div style="width: 88px;">
					<select name="deleteUsers" id="deleteUsers" style="background:url(images/delete.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0); float: left" onClick="deleteUsersList(this.value);"></select>
					<div style="cursor: pointer; width: 44px;" onClick="newAccount()"><img src="images/add.png" width="44" height="44" border="0" /></div>
				</div>
			</div>
       		
        	
         </td>
       </tr>
       </table>
    </cfif>
     
     <!--- <cfif newUser> --->
    <div style="display: none" id="newAccount">
     <cfinvoke  component="CFC.Users" method="getAccessLevel" returnvariable="levels" />

     <form action="usersView.cfm" method="post" name="newUser" id="newUser">
     <table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEE" style="margin-top:1px">
       <tr>
         <td width="20" align="right" class="subheading">Name</td>
         <td width="100" align="left" class="subheading"><input name="userName" type="text" class="formfieldcontent" id="userName" style="width:150px" maxlength="255" /></td>
         <td width="60" align="right" class="subheading">EMail</td>
         <td align="left" class="subheading"><input name="userEmailAddress" type="text" class="formfieldcontent" id="userEmailAddress" style="width:175px" maxlength="255" /></td>
         <td width="60" align="right" class="subheading">Pass</td>
         <td width="75" align="left" class="subheading"><input name="userPass" type="text" class="formfieldcontent" id="userPass" style="width:75px" maxlength="20" /></td>
         <td width="75" align="left" class="subheading">
           <select name="userLevel" id="userLevel" style="background:url(images/access_locked-1.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" onchange="updateUserAccessDisplay(this)">
              <cfoutput query="accessLevels">
              <cfif accessLevel NEQ 0>
              <option value="#accessLevel#" style="color:##333" <cfif levelName IS 'User'>selected="selected"</cfif>>#levelName#</option>
              </cfif>
              </cfoutput>
          </select>
          
           </td>
        
         <td width="69" align="right" class="subheading">
         <a onclick="createNewUser(<cfoutput>#userType#</cfoutput>)"><img src="images/ok.png" width="44" height="44" border="0" /></a>
         </td>
       </tr>
       <tr>
         <td colspan="10" align="right" bgcolor="#FFFFFF" class="subheading">&nbsp;</td>
         </tr>
     </table>
     </form>
	</div>
 
		<div id="userList"></div>

     
  </cfif>


</cfif>
	
<script>
	displayClientList();
	displayClientIcon();
</script>

</body>
</html>