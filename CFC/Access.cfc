<cfcomponent>

	<!--- CMS --->

	<!--- get group accessCode, DateStart, DateExpire and gpsID --->
	<cffunction name="getGPSLocations" access="public" returntype="query">
		<cfargument name="appID" type="string" required="yes">
		
        <cfquery name="gpsList">
            SELECT        Assets.name, GPSAssets.asset_id
            FROM          GPSAssets INNER JOIN Assets ON GPSAssets.asset_id = Assets.asset_id
            WHERE        (Assets.app_id = #appID#)
        </cfquery>
        
        <cfreturn gpsList>
        
    </cffunction>
        

	<!--- clear group access --->
	<cffunction name="clearGroupAccess" access="remote" returntype="boolean">
		<cfargument name="groupID" type="numeric" required="yes">
        
        <cfquery name="updateAccess">
                DELETE FROM AccessCodes
				WHERE group_id = #groupID#    
        </cfquery>  

		<cfreturn true>
        
	</cffunction>
    
    
    <!--- set access --->
	<cffunction name="setGroupAccess" access="remote" returntype="numeric">
        <cfargument name="codeID" type="numeric" required="yes">
        <cfargument name="theItem" type="numeric" required="yes">
        
        <cfargument name="accessCode" type="string" required="no" default="">
        <cfargument name="accessStart" type="string" required="no" default="">
        <cfargument name="accessEnd" type="string" required="no" default="">
        <cfargument name="accessLen" type="string" required="no" default="">
        
        <cfargument name="gpsID" type="numeric" required="no" default="0">
        <cfargument name="accessLevel" type="numeric" required="no" default="0">

        <!--- convertDateToEpoch --->
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="dateStart">
            <cfinvokeargument name="TheDate" value="#accessStart#"/>
        </cfinvoke>
        
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="dateEnd">
            <cfinvokeargument name="TheDate" value="#accessEnd#"/>
        </cfinvoke>
        
        <!--- if no access code make one --->
        <cfif accessCode IS ''>
        
            <cfinvoke component="Misc" method="makeID" returnvariable="accessCode">
                <cfinvokeargument name="length" value="8"/>
            </cfinvoke>
        
        </cfif>
        
        <!--- update --->
        <cfquery name="updateAccess">
                UPDATE AccessCodes
                SET accessCode = '#trim(accessCode)#', 
                
                <cfif accessStart IS ''>
                	accessDate = NULL, 
                <cfelse>
                	accessDate = #dateStart#, 
                </cfif>
                
                <cfif accessEnd IS ''>
                	accessExpires = NULL, 
                <cfelse>
                	accessExpires = #dateEnd#, 
                </cfif>
                
				<cfif accessLen IS ''>
                	accessLength = NULL, 
                <cfelse>
                	accessLength = #accessLen#, 
                </cfif>
                
                	accessLevel = #accessLevel#
                
                <cfif gpsID GT 0>
                	, gps_id = #gpsID#
                <cfelse>
                	, gps_id = NULL	
                </cfif>
                
                WHERE	accessCode_id = #codeID#
                
        </cfquery>  

		<cfreturn theItem>
        
	</cffunction>
    
    
    
    <!--- get group access code entries --->
	<cffunction name="getGroupAccessCodes" access="remote" returntype="query">
		<cfargument name="groupID" type="numeric" required="yes">
    	
        <cfquery name="groupAccess">
            SELECT        accessCode_id, accessCode, GPS_id, accessDate, accessExpires, accessLength, accessLevel
            FROM            AccessCodes
            WHERE        (group_id = #groupID#)
        </cfquery>
    	
        <cfreturn groupAccess>
        
     </cffunction>
        
        
    <!--- create Group Access --->
	<cffunction name="createGroupAccess" access="remote" returntype="boolean">
		<cfargument name="groupID" type="numeric" required="yes">
    	
        <cfinvoke component="Misc" method="makeID" returnvariable="accessCode">
            <cfinvokeargument name="length" value="8"/>
        </cfinvoke>
        
        <cfquery name="UserAccess"> 
            INSERT INTO AccessCodes (accessCode, accessLevel, group_id)
            VALUES ('#accessCode#',  0, #groupID#)
        </cfquery>
    	
        <cfreturn true>
        
     </cffunction> 
     
     
     <!--- delete Group Access --->
	<cffunction name="deleteGroupAccess" access="remote" returntype="boolean">
		<cfargument name="accessCodeID" type="numeric" required="yes">
    	
        <cfinvoke component="Misc" method="makeID" returnvariable="accessCode">
            <cfinvokeargument name="length" value="8"/>
        </cfinvoke>
        
        
        <cfquery name="groupAccess">
            SELECT        group_id
            FROM          AccessCodes
            WHERE        (accessCode_id = #accessCodeID#)
        </cfquery>
        
        <cfif groupAccess.recordCount GT 0>
        
        	<cfset groupID = groupAccess.group_id>
            
            <!--- que --->
            <cfquery name="groupsAccessed">
                SELECT        COUNT(accessCode_id) AS groups
                FROM          AccessCodes
                WHERE        (group_id = #groupID#)
            </cfquery>
            
            <cfset total = groupsAccessed.groups>
            
            <cfif total GT 0>
            
            	<!--- clear any users of this groupID if they have any codes --->
                <cfinvoke  component="Access" method="removeUsersAccess" returnvariable="ok">
                    <cfinvokeargument name="groupID" value="#groupID#"/>
                </cfinvoke>
                
            </cfif>
            
        <cfelse>
        	<cfset groupID = 0>
        </cfif>
        
        <cfquery name="updateAccess">
                DELETE FROM AccessCodes
				WHERE accessCode_id = #accessCodeID#    
        </cfquery>
        
        <cfreturn true>
        
     </cffunction>
    
    
    
    
    <!--- API --->
	
	<!--- validateCode user with acces code for groupID --->
	<cffunction name="validateAccessCode" access="public" returntype="struct">
		<cfargument name="auth_token" type="string" required="yes">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="unlockCode" type="string" required="no" default="">
        <cfargument name="gpsLocation" type="struct" required="no" default="#structNew()#"><!--- {'latt':0, 'long':0} --->
        
        <cfset groupIDs = ''>
        <cfset insideLocation = true>
        
		<cfset data = structNew()>
        
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <!--- ok error --->
        <cfinvoke  component="Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <!--- get user info --->
        <cfinvoke  component="Access" method="getUserAccessInfo" returnvariable="user">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
      
        <cfif user.error.error_code NEQ 1000>
        	<cfreturn user.error>
        </cfif>
		
        <!--- get Group Access Info --->
        <cfinvoke  component="Access" method="getGroupAccessInfo" returnvariable="groupAccess">
            <cfinvokeargument name="groupID" value="#groupID#"/>
            <cfinvokeargument name="unlockCode" value="#unlockCode#"/>
        </cfinvoke>
   																																 																												
        <cfif arrayIsEmpty(groupAccess)>
        	
            <!--- no such group access - no data --->
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1013"/>
            </cfinvoke>
            
        <cfelse>
            
            <cfloop index="theAccess" array="#groupAccess#">
            
                <!--- 																									 --->
                <!--- IF GPS LOCATION - CHECK --->
                <cfif theAccess.gpsID GT 0>

                    <!--- check if inside GPS IP Location --->
                    <cfinvoke  component="Access" method="insideGPSIPLocation" returnvariable="insideLocation">
                        <cfinvokeargument name="gpsID" value="#theAccess.gpsID#"/>
                        <cfinvokeargument name="gpsLocation" value="#gpsLocation#"/>
                    </cfinvoke>
                    
                    <cfif insideLocation>
                        <!--- inside location --->
                    <cfelse>
                    
                        <!--- error - not inside GPS location error - access denied --->
                        <cfinvoke  component="Errors" method="getError" returnvariable="error">
                            <cfinvokeargument name="error_code" value="1021"/>
                        </cfinvoke> 
                    
                    </cfif>
                 
                <cfelse>
                
                    <!--- remove gps since none --->
                    <cfset structDelete(theAccess,'gpsID')>
                           
                </cfif>
                <!--- 																									 --->
        																										     
                <!--- check if user has this code for access --->
                <cfif structKeyExists(user.ids, groupID)>
                    <!--- Found override --->
                    <cfset user.ids[groupID].accessID = groupAccess[1].access_id>
                </cfif>
             
				<!--- Update access to user --->

                <cfset allIDS = arrayNew(1)>
                
                <cfloop collection="#user.ids#" item="anItem">
                    <cfset arrayAppend(allIDS,anItem)>
                </cfloop>
                
                <cfif arrayFind(allIDS,groupID) IS 0>
                	<cfset arrayAppend(allIDS,groupID)>
                </cfif>
                
                <cfset IDS = arrayToList(allIDS)>
                                                        
                <cfquery name="newAccess"> 
                    UPDATE AssetAccess
                    SET group_ids = '#IDS#', accessCode_id = #theAccess.access_id#
                    WHERE	access_id = #user.accessID#
                </cfquery>
                    
                
                
                <!--- i have the group AND code provided to unlock group --->
                <!--- <cfset data = {'access':groupAccess.accessLevel}> --->
                
            </cfloop>   
           
        </cfif>

		<cfset structAppend(data,{"error":#error#})>
            
        <cfreturn data>
        
	</cffunction>
    
    
    
    
    
    <!--- get groups for a user --->
	<cffunction name="getUserAccessGroups" access="public" returntype="struct">
		<cfargument name="auth_token" type="string" required="yes">
        
        <cfset data = structNew()>
        
		<!--- ok error --->
        <cfinvoke  component="Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <!--- get user info --->
        <cfinvoke  component="Access" method="getUserAccessInfo" returnvariable="user">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
    
        <cfif user.error.error_code IS 1000>

            <cfloop collection="#user.ids#" item="groupID">
            	 
                <!--- get group access info --->
                <cfinvoke  component="Access" method="getGroupAccessInfo" returnvariable="groupAccessInfo">
                    <cfinvokeargument name="groupID" value="#groupID#"/>
                </cfinvoke>
																																												
                <cfloop index="accessGroup" array="#groupAccessInfo#">
                																																		
                <cfif groupID IS accessGroup.group_id>
                
					<cfif structKeyExists(accessGroup,'gpsID')>
                        <cfif accessGroup.gpsID GT 0>
                        
                            <!--- get GPS info if any --->
                            <cfinvoke  component="Access" method="getGroupAccessGPSLocationInfo" returnvariable="gpsCoords">
                                <cfinvokeargument name="gpsID" value="#accessGroup.gpsID#"/>
                            </cfinvoke>
                            
                            <cfset structDelete(accessGroup,'gpsID')>
                            <cfset structAppend(accessGroup,{'gps':gpsCoords})>
                         
                        <cfelse>
                        
                            <!--- remove gps since none --->
                            <cfset structDelete(accessGroup,'gpsID')>
                                   
                        </cfif>
                    </cfif>
                    
                    <cfset structAppend(data,{'#groupID#':accessGroup})>
               		
                </cfif>
                
                </cfloop>
                
            </cfloop>
            
        <cfelse>
        	<cfreturn user.error>
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})>
        <cfreturn data>
        
	</cffunction>
    
    
   <!--- get groups accessible for a user --->
	<cffunction name="getContentByUserAccess" access="public" returntype="struct">
		<cfargument name="auth_token" type="string" required="yes">
        <cfargument name="dev" type="numeric" required="no" default="0">
        
        <cfset data = structNew()>
       
		<!--- ok error --->
        <cfinvoke  component="Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <!--- get user info --->
        <cfinvoke  component="Access" method="getUserAccessInfo" returnvariable="user">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
     
        <cfif NOT isDefined("user.appID")>
        	<cfreturn user.error>
        </cfif>
        
        <cfset appID = user.appID>
    
        <cfif user.error.error_code IS 1000>

            <cfquery name="accessGroupIDs"> 
                SELECT	group_ids
                FROM	AssetAccess
                WHERE	user_id = #user.userID#
            </cfquery>
            
            <cfif accessGroupIDs.recordCount GT 0>
				<cfset groupIDs = listToArray(accessGroupIDs.group_ids)>
            <cfelse>
            	<cfset groupIDs = arrayNew(1)>
            </cfif>
            
        <cfelse>
        	<cfreturn user.error>
        </cfif>
        
        <!--- Get JSON Path --->
        <cfinvoke  component="Apps" method="getJSONPath" returnvariable="JSONPath">
        	<cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
        
        <!--- if DEV then add DEV folder pto path --->
        <cfif dev IS 1>
        	<cfset JSONPath = JSONPath & 'DEV/'>
		</cfif>
        
        <cfinvoke component="Apps" method="getAppName" returnvariable="appName">
            <cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
        
        <!--- get group data --->
    
        <cfloop index="groupID" array="#groupIDs#">
        
        <!--- build path --->
        <cfset JSONFile = 'access_' & groupID & '.json'>
        
        <cfset JSONFilePath = JSONPath & JSONFile>
  
        <!--- check if data exists --->
        <cfif fileExists(expandPath(JSONFilePath))>
      	
            <cffile action = "read" file = "#expandPath(JSONFilePath)#" variable = "JSONData" charset="utf-8">
            <cfset data = deserializeJson(JSONData)>
            
        <cfelse>
 
        	<!--- generate JSON and Read --->
        	<cfinvoke component="Modules" method="generateGroupData" returnvariable="data">
                  <cfinvokeargument name="groupID" value="#groupID#"/>
                  <cfinvokeargument name="appID" value="#appID#"/>
             </cfinvoke>
            																													
           <!---  <cfif dataGenerated>
            	<cffile action = "read" file = "#JSONFilePath#" variable = "JSONData" charset="utf-8">
            	<cfset data = deserializeJson(JSONData)>
            </cfif> --->
         
            <!---  <cfinvoke component="Modules" method="getGroupContent" returnvariable="data">
                  <cfinvokeargument name="groupID" value="#groupID#"/>
                  <cfinvokeargument name="appID" value="#user.appID#"/>
             </cfinvoke> --->
        
        </cfif>
        
        </cfloop>
         
        <cfset structAppend(data,{"error":#error#})>
    
        <cfreturn data>
        
	</cffunction>   
    
    
    
    <!--- get Dynamic Content --->
	<cffunction name="getDynamicContent" access="public" returntype="struct">
 		<cfargument name="auth_token" type="string" required="yes">
        <cfargument name="date" type="numeric" required="no" default="0">
        <cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="dev" type="numeric" required="no" default="0">
        
        <cfset data = structNew()>
       
		<!--- ok error --->
        <cfinvoke  component="Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
		
		<!--- no change error --->
        <cfinvoke  component="Errors" method="getError" returnvariable="noChangeError">
            <cfinvokeargument name="error_code" value="1040"/>
        </cfinvoke>
        
        <!--- get user info --->
         <cfinvoke  component="Access" method="getUserAccessInfo" returnvariable="user">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
		
        <cfif user.error.error_code IS 1000>
        
			<cfset appID = user.appID> 
            
            <!--- get asset date --->
            <cfquery name="assetInfo"> 
                SELECT	modified
                FROM	Groups
                WHERE	asset_id = #assetID#
            </cfquery>
            
            <cfset assetDate = 0>
            
            <cfif assetInfo.recordCount GT 0>
            	<!--- normal asset --->
                <cfset assetDate = assetInfo.modified>
            <cfelse>
            	<!--- sub group asset --->
                <cfquery name="assetGroupInfo">
                    SELECT       asset_id
                    FROM         GroupAssets
                    WHERE        content_id = #assetID#
                </cfquery>

                <cfif assetGroupInfo.recordCount GT 0>
                
                    <!--- the asset is in groupAssets --->
                    <cfquery name="groupAssetInfo">
                        SELECT       asset_id
                        FROM         GroupAssets
                        WHERE        content_id = #assetGroupInfo.asset_id#
                    </cfquery>
             	
                    <cfset assetDate = 0>
     				              
					<cfif groupAssetInfo.recordCount GT 0>  
 						
                        <cfoutput query="groupAssetInfo">
                        
							<!--- get Asset Modified Date --->
                            <cfquery name="assetInfo">
                                SELECT       modified
                                FROM         Groups
                                WHERE        (asset_id = #groupAssetInfo.asset_id#)
                            </cfquery>
                            
                            <cfif assetInfo.modified GT assetDate>
                                <cfset assetDate = assetInfo.modified>
                            </cfif>
                            
                        </cfoutput>
                    
                    <cfelse>
                        
                        <cfset assetDate = 0>
                        
                        <cfoutput query="assetGroupInfo">
							
                            <cfquery name="assetInfo"> 
                                SELECT	modified
                                FROM	Groups
                                WHERE	asset_id = #assetGroupInfo.asset_id#
                            </cfquery>
                            
                            <cfif assetInfo.modified GT assetDate>
								<cfset assetDate = assetInfo.modified>
                            </cfif>
                        
                        </cfoutput>

                    </cfif>
                
                </cfif>
                
            </cfif>
          
            <!--- Get JSON Path --->
            <cfinvoke  component="Apps" method="getJSONPath" returnvariable="JSONPath">
                <cfinvokeargument name="appID" value="#appID#">
            </cfinvoke>
            
            <!--- if DEV then add DEV folder pto path --->
            <cfif dev IS 1>
                <cfset JSONPath = JSONPath & 'DEV/'>
            </cfif>
            
            <!--- build path --->
             <cfset JSONFile = 'asset_' & assetID & '.json'>
            
            <cfset JSONFilePath = JSONPath & JSONFile> 
				
            <!--- check if data exists --->
            <cfif fileExists(JSONFilePath)>
            
                <cffile action = "read" file = "#JSONFilePath#" variable = "JSONData" charset="utf-8">
                <cfset data = deserializeJson(JSONData)>
                
             <cfelse>
       
                <!--- generate JSON and Read --->
                <cfinvoke component="Modules" method="generateAssetData" returnvariable="data">
                      <cfinvokeargument name="assetID" value="#assetID#"/>
                      <cfinvokeargument name="appID" value="#appID#"/>
                 </cfinvoke>
            
            </cfif> 
   
            <!--- Check if Date is Newer --->
            <cfloop collection="#data#" item="theKey"></cfloop>
     
            <cfif assetDate GT date>
            	
               <cfset uniqueThread = CreateUUID()>
            	
                 <cfthread action="run" name="genDyn_#uniqueThread#" assetID="#assetID#" appID="#appID#"> 
               
					<!--- generate JSON and Read --->
					<cfinvoke component="Modules" method="generateAssetData" returnvariable="data">
						  <cfinvokeargument name="assetID" value="#assetID#"/>
						  <cfinvokeargument name="appID" value="#appID#"/>
					 </cfinvoke>
                 
				 </cfthread>
               
                <cfset structAppend(data,{"error":#error#})>
                
            <cfelse>	
                <cfset data = structNew()>
                <cfset structAppend(data,{"error":#noChangeError#})>
            </cfif>
        
            <cfreturn data>
        
        <cfelse>
        	<cfreturn user>
        </cfif>
        
	</cffunction>  
    
    
    
    
    
    
    
    
    
    
    
    <!--- PRIVATE METHODS --->
    
    <!--- get user Access info --->
	<cffunction name="getUserAccessInfo" access="public" returntype="struct">
		<cfargument name="auth_token" type="string" required="yes">
		
        <cfset data = structNew()>
        <cfset groupIDs = "">
        
        <!--- ok error --->
        <cfinvoke  component="Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <!--- get user from token and groups access --->
        <cfquery name="getTokenInfo">
            SELECT      Users.user_id, Tokens.app_id, AccessLevels.accessLevel, AssetAccess.group_ids, AssetAccess.access_id, AssetAccess.accessCode_id
			FROM        Users INNER JOIN
                        AccessLevels ON Users.access_id = AccessLevels.accessLevel INNER JOIN
                        Tokens ON Users.user_id = Tokens.user_id INNER JOIN
                        AssetAccess ON Tokens.user_id = AssetAccess.user_id
            WHERE       (Tokens.token = '#auth_token#') AND (Users.userType = 0)
        </cfquery>
   
        <!--- no token match --->
        <cfif getTokenInfo.recordCount IS 0>
        
        	<!--- maybe user token is incorrect, check token valid --->
            <cfquery name="getTokenInfo">
                SELECT        Users.user_id, Tokens.app_id, AccessLevels.accessLevel AS accessLevel
                FROM          Users INNER JOIN
                              AccessLevels ON Users.access_id = AccessLevels.accessLevel INNER JOIN
                              Tokens ON Users.user_id = Tokens.user_id
                WHERE        (Tokens.token = '#auth_token#') AND (Users.userType = 0)
            </cfquery>
            
            <cfinvoke  component="Tokens" method="tokenValid" returnvariable="tokenValid">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
        	
            <cfif NOT tokenValid>
            
            	<!--- user not exsit from token --->
                <cfinvoke  component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1009"/>
                </cfinvoke>

                <!--- nothing we can do, token not valid, no such user --->
                <cfset structAppend(data,{"error":#error#})> 

        		<cfreturn data>

            <cfelse>
            	
                <!--- get user info from token --->
                <cfinvoke  component="Tokens" method="getTokenInfo" returnvariable="userInfo">
                    <cfinvokeargument name="auth_token" value="#auth_token#"/>
                </cfinvoke>
                
                <cfset userID = userInfo.userID>
                
            	<!--- user exists, need to create a new record in access --->
                <!--- <cfquery name="newUserAccess"> 
                    INSERT INTO AssetAccess (user_id)
                    VALUES (#userID#)
                    SELECT @@IDENTITY AS accessID
                </cfquery> 
                
                <cfset accessID = newUserAccess.accessID>
                --->
                <cfset accessID = 0>
            </cfif>
        
        <cfelse>
        	
            <cfset userID = getTokenInfo.user_id>
            <cfset accessID = getTokenInfo.access_id>
        	<cfset groupIDs = getTokenInfo.group_ids>
            <cfset AccessIDs = getTokenInfo.accessCode_id>
            
        </cfif>
        

        <!--- i have a user --->
        <cfset ids = structNew()>
        <cfloop index="anItem" list="#groupIDs#">
        <cfset theAccess = {"#anItem#":{"accessID":AccessIDs}}>
        	<cfset structAppend(ids,theAccess)>
        </cfloop>
        
        <cfset user = {'userID':userID, 'appID':getTokenInfo.app_id, 'accessLevel':getTokenInfo.accessLevel, 'ids':ids, 'accessID':accessID}>
		
		<cfset structAppend(user,{"error":#error#})>
        
		<cfreturn user>
        
	</cffunction>
    
    
    
    
    <!--- remove access for a user for groupID --->
	<cffunction name="removeUsersAccess" access="remote" returntype="struct">
    	<cfargument name="groupID" type="string" required="no" default="0">
		<cfargument name="userIDs" type="array" required="no" default="#arrayNew(1)#">
        
        <cfset data = structNew()>
        
        <cfif arrayLen(userIDs) IS 0>
        
			<!--- get all users with groupIDS --->
            <cfquery name="usersWithGroups">
                DECLARE @search VARCHAR(10);
                SET @search = '#groupID#'
                
                SELECT        user_id
                FROM          AssetAccess
                WHERE        (',' + RTRIM(group_ids) + ',') LIKE '%,' + @search + ',%'
            </cfquery>
        
 
            <!--- convert query to array --->
            <cfinvoke  component="Misc" method="QueryToArray" returnvariable="userIDs">
                <cfinvokeargument name="theQuery" value="#usersWithGroups#"/>
                <cfinvokeargument name="theColumName" value="user_id"/>
            </cfinvoke>
       
        </cfif>
        
        <cfif arrayLen(userIDs) GT 0>
        
			<!--- ok error --->
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
            
            <cfloop index="userID" array="#userIDs#">
            
                <!--- get user access codes --->
                <cfquery name="getUserAccessCodes">
                    SELECT       group_ids
                    FROM         AssetAccess             
                    WHERE        user_id = #userID#
                </cfquery>
                
                <cfset IDs = listToArray(getUserAccessCodes.group_ids)>
                
                <!--- check if access code exists --->
                <cfif groupID GT 0>
                
					<cfif ArrayFind(IDs, groupID)>
                    
                        <!--- if so, delete access group --->
                        <cfset ArrayDelete(IDs,groupID)>
        
                        <!--- update DB entry --->
                        <cfquery name="newAccess"> 
                            UPDATE	AssetAccess
                            SET 	group_ids = '#arrayToList(IDS)#'
                            WHERE	user_id = #userID#
                        </cfquery>
                        
                        <cfif arrayLen(IDS) IS 0>
                        	
                        	<!--- is no ids then remove user from access list --->
                            <cfinvoke  component="Access" method="deleteUserGroupAccess" returnvariable="ok">
                                <cfinvokeargument name="userIDs" value="#userID#"/>
                            </cfinvoke>
                            
                        </cfif>
                    
                    </cfif>
                
                </cfif>
                
            </cfloop>
        
        <cfelse>
        
			<!--- no data error --->
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1013"/>
            </cfinvoke>
        
        </cfif>
        
        
        
        <cfset structAppend(data,{"error":#error#})>

		<cfreturn data>
        
	</cffunction>

    
    
    <!--- delete user(s) of all access --->
	<cffunction name="deleteUserGroupAccess" access="public" returntype="struct">
		<cfargument name="userIDs" type="string" required="no" default="">
        
		<cfset data = structNew()>
        
        <cfset userIDs = listToArray(userIDs)>
        
        <cfif arrayLen(userIDs) GT 0>
        
			<!--- ok error --->
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
           
            <cfloop index="userID" array="#userIDs#">
                
                <cfquery name="newAccess"> 
                    DELETE FROM AssetAccess
                    WHERE	user_id = #userID#
                </cfquery>
                
            </cfloop>
        
        <cfelse>
        	
            <!--- no data error --->
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1013"/>
            </cfinvoke>
            
        </cfif>
        
		<cfset structAppend(data,{"error":#error#})>
		
		<cfreturn data>
        
	</cffunction>
    
    
    
    
    <!--- add user(s) access for groupID --->
	<cffunction name="addUserGroupAccess" access="public" returntype="struct">
		<cfargument name="userIDs" type="string" required="no" default="">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        
        <cfset data = structNew()>
        
        <cfset userIDs = listToArray(userIDs)>
        
		<cfif arrayLen(userIDs) GT 0>
        
			<!--- ok error --->
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
            
            <cfloop index="userID" array="#userIDs#">
                
                <!--- get user access groups --->
                <cfquery name="getUserAccessGroups">
                    SELECT       group_ids
                    FROM         AssetAccess             
                    WHERE        user_id = #userID#
                </cfquery>
                
                <cfif getUserAccessGroups.recordCount GT 0>
                	<!--- adds to existing record --->
                	<cfif groupID GT 0>
                    
						<!--- record exists, add to group and UPDATE --->
                        <cfset groupIDs = listToArray(getUserAccessGroups.group_ids)>
                        
                        <cfset found = arrayFind(groupIDs,groupID)>
                        
                        <cfif found GT 0>
                        	<!--- already exists --->
                        <cfelse>
                        
                        	<!--- add new groupID --->
							<cfset arrayAppend(groupIDs,groupID)>
                            
                            <!--- update DB entry --->
                            <cfquery name="newAccess"> 
                                UPDATE	AssetAccess
                                SET 	group_ids = '#arrayToList(groupIDs)#'
                                WHERE	user_id = #userID#
                            </cfquery>
                            
                    	</cfif>
                        
                    <cfelse>
                    
                    	<!--- no data error --->
                        <cfinvoke  component="Errors" method="getError" returnvariable="error">
                            <cfinvokeargument name="error_code" value="1013"/>
                        </cfinvoke>
                
                    </cfif>
                        
                <cfelse>
                	<!--- creates new record and adds group access --->
                    <cfif groupID GT 0>
                    
                    	<!--- add groupID --->
						<cfset groupIDs = arrayNew(1)>
                        <cfset arrayAppend(groupIDs,groupID)>
                        
                        <!--- record NOT exist, INSERT new user and group --->
                        <cfquery name="newUserAccess"> 
                            INSERT INTO AssetAccess (user_id, group_ids)
                            VALUES (#userID#, '#arrayToList(groupIDs)#')
                        </cfquery>
                    
                    <cfelse>
                    
                    	<!--- no data error --->
                        <cfinvoke  component="Errors" method="getError" returnvariable="error">
                            <cfinvokeargument name="error_code" value="1013"/>
                        </cfinvoke>
            
                	</cfif>
                    
                </cfif>
                
            </cfloop>
        
        <cfelse>
        	
            <!--- no data error --->
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1013"/>
            </cfinvoke>
            
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})>
		
		<cfreturn data>
        
	</cffunction>
    
    
    
    <!--- get Group Access Info --->
	<cffunction name="getGroupAccessInfo" access="public" returntype="array">
		<cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="unlockCode" type="string" required="no" default="">
        
        <cfset groupAccessCodes = arrayNew(1)>
        
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
		<cfif groupID GT 0>
            
			<!--- groupID check if allowed by appID that uses unlock code --->
            <cfquery name="getGroupInfo">
                SELECT        AccessCode_id, AccessCode, AccessDate, AccessExpires, AccessLength, GPS_ID, AccessLevel
                FROM          AccessCodes
                WHERE        group_id = #groupID#
                <cfif unlockCode NEQ ''>
                AND			 AccessCode = '#unlockCode#' 
                </cfif>
                AND ((#curDate# >= AccessDate OR AccessDate IS NULL) AND (#curDate# <= AccessExpires OR AccessExpires IS NULL))
                ORDER BY 	AccessLevel DESC
            </cfquery>
 
            <cfoutput query="getGroupInfo">
            
            	<cfset groupAccess = {}>
            
                <!--- we have group info --->
                <cfif AccessDate NEQ ''>
                <cfset structAppend(groupAccess,{'start':AccessDate})>
                </cfif>
                
                <cfif AccessExpires NEQ ''>
                	<cfset structAppend(groupAccess,{'expires':AccessExpires})>
                </cfif>
                
                <cfif AccessLength NEQ ''>
                	<cfset structAppend(groupAccess,{'length':AccessLength})>
                </cfif>
                
                <cfif GPS_ID NEQ ''>
                	<cfset structAppend(groupAccess,{'gpsID':GPS_ID})>
                <cfelse>
                	<cfset structAppend(groupAccess,{'gpsID':0})>
                </cfif>
                
                <cfset structAppend(groupAccess,{'accessLevel':AccessLevel})>
				<cfset structAppend(groupAccess,{'access_id':AccessCode_id})>
                 <cfset structAppend(groupAccess,{'group_id':groupID})>
                 
                <cfset arrayAppend(groupAccessCodes,groupAccess)>
                
            </cfoutput>
	
        </cfif>
          
		<cfreturn groupAccessCodes>
        
	</cffunction>
    
    
    
      <!--- get GPS location and check if inside location --->
	<cffunction name="insideGPSIPLocation" access="public" returntype="any">
		<cfargument name="gpsID" type="numeric" required="no" default="0">
        <cfargument name="gpsLocation" type="struct" required="no" default="#structNew()#"><!--- {'latt':0, 'long':0} --->
    	<cfargument name="returnDistance" type="boolean" required="no" default="false">
        
    	<cfset insideLocation = false>
        
        <!--- get gps data --->
        <cfinvoke  component="Access" method="getGroupAccessGPSLocationInfo" returnvariable="gpsCoords">
            <cfinvokeargument name="gpsID" value="#gpsID#"/>
        </cfinvoke> 

        <cfif NOT structIsEmpty(gpsCoords)>
			
            <cfif structIsEmpty(gpsLocation)>
            
				<!--- get my location based on my ip --->
                <cfinvoke component="Misc" method="getServerGeoLocation" returnvariable="location" />
                <cfset myLocation = {'latt':location.latitude, 'long':location.longitude}>
            
            <cfelse>
            	<!--- Use GPS provided from app --->
                <cfif gpsLocation.long IS 0 OR  gpsLocation.latt IS 0>
                
                	<!--- get my location based on my ip --->
                    <cfinvoke component="Misc" method="getServerGeoLocation" returnvariable="location" />
                    <cfset myLocation = {'latt':location.latitude, 'long':location.longitude}>
                    
                <cfelse>
                	<cfset myLocation = gpsLocation>
                </cfif>
            </cfif>
           
           	<cfset unit = "ft">
           
            <cfinvoke  component="Misc" method="GetLatitudeLongitudeProximity" returnvariable="distance">
                <cfinvokeargument name="FromLatitude" value="#gpsCoords.latt#"/>
                <cfinvokeargument name="FromLongitude" value="#gpsCoords.long#"/>
                <cfinvokeargument name="ToLatitude" value="#myLocation.latt#"/>
                <cfinvokeargument name="ToLongitude" value="#myLocation.long#"/>
                <cfinvokeargument name="unit" value="#unit#"/>
            </cfinvoke> 

            <!--- check if cur location is inside gps location --->
            <!--- distance in length determines whether inside --->
       
            <cfif distance LTE gpsCoords.radius >
                <cfset insideLocation = true>
            </cfif>
                
        </cfif>
		
        <cfif returnDistance>
        	<cfreturn {"distance":distance, "unit":unit}>
        <cfelse>
			<cfreturn insideLocation>
        </cfif>
        
	</cffunction>
    
    
  
<!--- get GPS location info --->
	<cffunction name="getGroupAccessGPSLocationInfo" access="public" returntype="struct">
		<cfargument name="gpsID" type="numeric" required="no" default="0">
        
        <cfset gpsCoords = {'latt':0 , 'long':0 , 'radius': 0}>
        
		<cfif gpsID GT 0>
            																										 
                <!--- get gps data --->
                <cfset gpsData = structNew()>
                
                <!--- GPS data model --->
                <cfinvoke  component="Modules" method="getGroupAssets" returnvariable="gpsData">
                    <cfinvokeargument name="assetID" value="#gpsID#"/>
                </cfinvoke>
                
                
           																											 
                <!--- Get Root Key --->
                <cfloop collection="#gpsData#" item="theKey"></cfloop>
                
                <cfif arrayLen(gpsData[theKey]) GT 0>
                
					<cfset gpsInfo = gpsData[theKey][1]>
       
                    <cfloop collection="#gpsInfo#" item="theKey"></cfloop>
                    
                    <cfset gps = gpsInfo[theKey]>
                    
                    <cfset gpsCoords.latt = gps.gps_latt>
                    <cfset gpsCoords.long = gps.gps_long>
                    <cfset gpsCoords.radius = gps.radius>
    			
                <cfelse>
                
                	<cfset gpsCoords = structNew()>
                
                </cfif>
                
        </cfif>
        
    	<cfreturn gpsCoords>
        
	</cffunction>
    
    
    
    <!--- clean user access (removed access of groups existing and not existing --->
	<cffunction name="cleanUserAccess" access="public" returntype="struct">
		<cfargument name="userID" type="numeric" required="no" default="0">
		
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <!--- get user token --->
        <cfquery name="userToken">
            SELECT        token
            FROM          Tokens
            WHERE        (user_id = #userID#)
        </cfquery>
        
        <cfset auth_token = userToken.token>
        
        <!--- get groupIDs --->
		<cfinvoke  component="Access" method="getUserAccessGroups" returnvariable="userIDs">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
       
        <cfif userIDs.error.error_code IS 1000>
        
        	<!--- user exists --->
			<cfset structDelete(userIDs,'error')>
            
			<!--- ok error --->
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>

            <cfset groupIDs = arrayNew(1)>
            
            <!--- find expired groups --->
            <cfloop collection="#userIDs#" item="groupID">

				<cfif NOT structIsEmpty(userIDs[groupID])>
                    
                    <cfif userIDs[groupID].start NEQ '' AND userIDs[groupID].expires NEQ ''>
                    
						<cfif curDate GTE userIDs[groupID].start AND curDate LTE userIDs[groupID].expires>
                            <cfset arrayAppend(groupIDs,groupID)>
                        </cfif>

                    </cfif>

                </cfif>
                
            </cfloop>
            
            <!--- update DB entry --->
            <cfquery name="newAccess"> 
                UPDATE	AssetAccess
                SET 	group_ids = '#arrayToList(groupIDs)#'
                WHERE	user_id = #userID#
            </cfquery>
        
        <cfelse>
        
 			<!--- no such user --->
            <cfreturn userIDs>
            
        </cfif>
        
    	<cfreturn error>
        
	</cffunction>
    
    
    
    <!--- clean user access (removed access of groups existing and not existing --->
	<cffunction name="updateUserAccess" access="public" returntype="struct">
		<cfargument name="userIDs" type="string" required="no" default="">
    	
        <!--- ok error --->
        <cfinvoke  component="Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
    
    	<cfset ids = listToArray(userIDs)>
        
        <cfloop index="userID" array="#ids#">
        
        	<!--- clean users access --->
            <cfinvoke  component="Access" method="cleanUserAccess" returnvariable="error">
                <cfinvokeargument name="userID" value="#userID#"/>
            </cfinvoke>
        
        </cfloop>
    
    	<cfreturn error>
        
	</cffunction>
    
    
    
    <!--- clean user access (removed access of groups existing and not existing --->
	<cffunction name="getUserAccess" access="public" returntype="array">
    	<cfargument name="groupID" type="numeric" required="no" default="0">
    
    	<!--- Get Users Attched to Groups, if appID then sort by GroupID --->
        <cfquery name="users"> 
            SELECT        AssetAccess.user_id, AssetAccess.group_ids, Users.name, Users.email
			FROM          AssetAccess INNER JOIN Users ON AssetAccess.user_id = Users.user_id
            WHERE         AssetAccess.group_ids IS NOT NULL
    	</cfquery>
        
        <!--- convert to struct array --->
        <cfinvoke  component="Misc" method="QueryToStruct" returnvariable="allUsers">
            <cfinvokeargument name="query" value="#users#"/>
            <cfinvokeargument name="forceArray" value="true"/>
        </cfinvoke>
 
 		<!--- filter groups --->
        <cfif groupID GT 0>
        
        	<cfset selectedUsers = arrayNew(1)>
        
        	<cfloop index="aUser" array="#allUsers#">
            
            	<cfset userGroupIDS = aUser.group_ids>
                <cfset ids = listToArray(userGroupIDS)>
                
                <cfif arrayFind(ids,groupID)>
                	<cfset arrayAppend(selectedUsers,aUser)>
                </cfif>
            
            </cfloop>
            
            <cfset allUsers = selectedUsers>
       
        </cfif>
		
		<cfreturn allUsers>
        
	</cffunction>
    
    
    
    
    <!--- get user groups access --->
	<cffunction name="getUserGroupAccess" access="public" returntype="array">
		<cfargument name="userID" type="numeric" required="yes">
    
    	<!--- get user access groups --->
        <cfquery name="getUserAccessGroups">
            SELECT       group_ids
            FROM         AssetAccess             
            WHERE        user_id = #userID#
        </cfquery>
        
        <cfset groupIDs = listToArray(getUserAccessGroups.group_ids)>
        
        <cfreturn groupIDs>
        
	</cffunction>
    
    
    
    
     <!--- set user groups access --->
	<cffunction name="setUserGroupAccess" access="remote" returntype="boolean">
		<cfargument name="userID" type="numeric" required="yes">
        <cfargument name="groupIDs" type="array" required="yes">
  
        <cfset curGroupIDs = arrayNew(1)>
        
        <!--- validate user exists --->
        <cfquery name="users">
            SELECT 		 group_ids, access_id
            FROM         AssetAccess             
            WHERE        user_id = #userID#
        </cfquery>
      
        <cfif users.recordCount IS 0>
        
        	<!--- record NOT exist, INSERT new user and group --->
            <cfquery name="usersAccess"> 
                INSERT INTO AssetAccess (user_id)
                VALUES (#userID#)
                SELECT @@IDENTITY AS access_id
            </cfquery>
            
            <cfset accessID = usersAccess.access_id>
        
        <cfelse>
        	 <cfset accessID = users.access_id>
        </cfif>

        <cfset currentGroupIDs = ListToArray(users.group_ids)>
        
       
        <cfloop index="aGroup" array="#groupIDs#">
        	<cfif arrayFind(currentGroupIDs,aGroup) IS 0>
            	<cfset arrayAppend(currentGroupIDs, aGroup)>   
            </cfif>
		</cfloop>

		<cfif arrayLen(currentGroupIDs) GT 0>    
            <!--- get user access groups --->
            <cfquery name="setUserAccessGroups">
                UPDATE	AssetAccess
                SET 	group_ids = '#arrayToList(currentGroupIDs)#'
                WHERE	access_id = #accessID# AND user_id = #userID#
            </cfquery>
        </cfif>	
   
        <cfquery name="usersAccess">
            SELECT 		 group_ids
            FROM         AssetAccess             
            WHERE        access_id = #accessID# AND user_id = #userID#
        </cfquery>
           
        <cfif arrayLen(ListToArray(usersAccess.group_ids)) IS 0 OR arrayLen(groupIDs) IS 0>
 
       		<!--- remove user access groups --->
            <cfinvoke  component="Access" method="deleteUserGroupAccess" returnvariable="ok">
                <cfinvokeargument name="userIDs" value="#userID#"/>
            </cfinvoke>
            
        </cfif>

        <cfreturn true>
        
	</cffunction>
    
    

        
</cfcomponent>