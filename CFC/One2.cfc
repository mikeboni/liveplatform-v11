<cfcomponent>

<!--- Get Features --->
	<cffunction name="getModuleFromCartID" access="remote" returntype="struct">
        <cfargument name="cartID" type="numeric" required="yes">
        
        <!--- From Asset --->
        <cfquery name="content">
            SELECT	contentIDs
            FROM	SessionCart
            WHERE	cart_id = #cartID#
        </cfquery>
    
        <cfif content.contentIDs NEQ ''>
        
        	<cfset groupID = listGetAt(content.contentIDs,1)>
            
			<!--- Get  --->
            <cfinvoke component="Modules" method="getGrouptPath" returnvariable="Module">
                <cfinvokeargument name="groupID" value="#groupID#">
            </cfinvoke>
        
            <cfif arrayIsEmpty(Module)>
            	<cfset mainModule = {}>
            <cfelse>  
            	<cfset mainModule = Module[arrayLen(Module)]>
			</cfif>
		<cfelse>
        	<cfset mainModule = {}>
        </cfif>
        
        <cfreturn mainModule>
        
     </cffunction>




	<!--- Get Features --->
	<cffunction name="getFeatures" access="remote" returntype="query">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="moduleID" type="numeric" required="yes">
        <cfargument name="featureName" type="string" required="no">
  
        <!--- Get Features --->
		<cfquery name="Features"> 
        	SELECT group_id,name
            FROM Groups
            WHERE (app_id = #appID#) AND (subgroup_id = #moduleID#) AND name = 'features'
        </cfquery>
      
			<cfset groupID = Features.group_id>
            <cfif groupID IS ''><cfset groupID = 0></cfif>
            
            <!--- Get Feature --->
            <cfquery name="Feature"> 
                SELECT group_id, name, thumb_id, detail_id, sharable
                FROM Groups
                WHERE (subgroup_id = #groupID#)
                <cfif featureName NEQ ''>
                    AND name = '#featureName#'
                </cfif>
                ORDER BY sortOrder
            </cfquery>
    
            <cfreturn Feature>
        
	</cffunction>


	<!--- Get Modules --->
	<cffunction name="getModules" access="remote" returntype="query">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="moduleName" type="string" required="no">
        
		<cfquery name="Modules"> 
        	SELECT group_id, name, thumb_id, detail_id, sharable
            FROM Groups
            WHERE (app_id = #appID#) AND (subgroup_id = 0)
            <cfif moduleName NEQ ''>
            	AND name = '#moduleName#'
            </cfif>
            ORDER BY sortOrder
        </cfquery>
        
        <cfreturn Modules>
        
	</cffunction>



	<!--- Get FeatureID --->
	<cffunction name="getFeatureID" access="remote" returntype="query">
		<cfargument name="appID" type="numeric" required="yes">
        
        <cfargument name="moduleID" type="numeric" required="no" default="0">
        <cfargument name="moduleName" type="string" required="no" default="">
        
        <cfargument name="featureID" type="numeric" required="no" default="0">
        <cfargument name="featureName" type="string" required="no" default="">
        
        <cfset Contents = queryNew("asset_id","Integer")>
       
        <!--- Get Module from Name --->
        <cfif moduleName NEQ ''>
        
            <cfinvoke component="One2" method="getModules" returnvariable="Module">
                <cfinvokeargument name="appID" value="#appID#">
                <cfinvokeargument name="moduleName" value="#moduleName#">
            </cfinvoke>
			
            <cfif Module.recordCount GT '0'>
				<cfset moduleID = Module.group_id>
			<cfelse>
            	<cfset moduleID = 0>
            </cfif>
			
        </cfif>
        
        <!--- If No Module --->
        <cfif moduleID IS '0'><cfreturn Contents></cfif>
        
        <!--- Get Feature from Name--->
        <cfif featureName NEQ ''>
        
            <cfinvoke component="One2" method="getFeatures" returnvariable="Feature">
                <cfinvokeargument name="appID" value="#appID#">
                <cfinvokeargument name="moduleID" value="#moduleID#">
                <cfinvokeargument name="featureName" value="#featureName#">
            </cfinvoke>
 	
        	<cfif Feature.recordCount GT '0'>
				<cfset featureID = Feature.group_id>
			<cfelse>
            	<cfset featureID = 0>
            </cfif>
        
        </cfif>
        
        <!--- If No Feature --->
        <cfif featureID IS '0'><cfreturn Contents></cfif>
        
        <!--- Get Assets in the Feature Group --->
        <cfquery name="Contents"> 
        	SELECT asset_id
            FROM Groups
            WHERE (app_id = #appID#) AND (subgroup_id = #featureID#)
            ORDER BY sortOrder
        </cfquery>
       
        <cfreturn Contents>
        
	</cffunction>
    
</cfcomponent>