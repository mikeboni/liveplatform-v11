<cfcomponent>

<!--- GET ERROR --->
	<cffunction name="getError" access="public" returntype="struct" output="no">
    
		<cfargument name="error_code" type="numeric" default="1000">
        
        <cfset errorStruct = structNew()>
        <cfset errorStruct["error_code"] = error_code>
        
        <cfswitch expression="#error_code#">
        
        	<cfcase value=1000>
            	<cfset errorStruct["error_message"] = "Success!">
            </cfcase>
            
        	<cfcase value=1001>
            	<cfset errorStruct["error_message"] = "Application Not Active">
            </cfcase>
            
            <cfcase value=1002>
            	<cfset errorStruct["error_message"] = "Application Not Found">
            </cfcase>
            
            <cfcase value=1003>
            	<cfset errorStruct["error_message"] = "Token does not exist">
            </cfcase>
            
            <cfcase value=1004>
            	<cfset errorStruct["error_message"] = "User Account Not Active">
            </cfcase>
            
            <cfcase value=1005>
            	<cfset errorStruct["error_message"] = "Missing Paramiters"><!--- CHECK --->
            </cfcase>
            
            <cfcase value=1006>
            	<cfset errorStruct["error_message"] = "Optional version update">
            </cfcase>
            
            <cfcase value=1007>
            	<cfset errorStruct["error_message"] = "Forced Update.">
            </cfcase>

            <cfcase value=1008>
            	<cfset errorStruct["error_message"] = "No Groups">
            </cfcase>
            
            <cfcase value=1009>
            	<cfset errorStruct["error_message"] = "User Not Found"><!--- CHECK --->
            </cfcase>
            
            <cfcase value=1010>
            	<cfset errorStruct["error_message"] = "Token exist but not for the specified bundleID">
            </cfcase>
            
            <cfcase value=1011>
            	<cfset errorStruct["error_message"] = "User Code Invalid"><!--- CHECK --->
            </cfcase>
            
            <cfcase value=1012>
            	<cfset errorStruct["error_message"] = "User Information is Missing">
            </cfcase>
            
            <cfcase value=1013>
            	<cfset errorStruct["error_message"] = "No Data">
            </cfcase>
            
            <cfcase value=1014>
            	<cfset errorStruct["error_message"] = "User Already Exists">
            </cfcase>
            
            <cfcase value=1015>
            	<cfset errorStruct["error_message"] = "Missing EMail Address">
            </cfcase>
            
            <cfcase value=1016>
            	<cfset errorStruct["error_message"] = "Address Book Entry Update Failed">
            </cfcase>
            
            <cfcase value=1017>
            	<cfset errorStruct["error_message"] = "Address Book Entry Not Found">
            </cfcase>
            
            <cfcase value=1020>
            	<cfset errorStruct["error_message"] = "Unique token not registered">
            </cfcase>
            
            <cfcase value=1021>
            	<cfset errorStruct["error_message"] = "Access Denied">
            </cfcase>
            
            <cfcase value=1022>
            	<cfset errorStruct["error_message"] = "Unit not found for Customer with Assigned User">
            </cfcase>
            
            <cfcase value=1023>
            	<cfset errorStruct["error_message"] = "Project Not Specified">
            </cfcase>
            
            <cfcase value=1024>
            	<cfset errorStruct["error_message"] = "Unit has already been assigned">
            </cfcase>
            
            <cfcase value=1025>
            	<cfset errorStruct["error_message"] = "Unit not found">
            </cfcase>
            
            <cfcase value=1026>
            	<cfset errorStruct["error_message"] = "Email does not exist">
            </cfcase>
            
            <cfcase value=1030>
            	<cfset errorStruct["error_message"] = "This customer is no longer active">
            </cfcase>
            
            <cfcase value=1040>
            	<cfset errorStruct["error_message"] = "No Change in Data">
            </cfcase>
            
            <!--- Game Errors --->
            
            <cfcase value=2000>
            	<cfset errorStruct["error_message"] = "Game is already registered but has not been completed">
            </cfcase>
            
            <cfcase value=2001>
            	<cfset errorStruct["error_message"] = "Game Not Available">
            </cfcase>
            
            <cfcase value=2002>
            	<cfset errorStruct["error_message"] = "Game has been completed">
            </cfcase>
            
            <cfcase value=3000>
            	<cfset errorStruct["error_message"] = "Failed!">
            </cfcase>
            
            <cfcase value=4000>
            	<cfset errorStruct["error_message"] = "Invitation not found!">
            </cfcase>
            
            <cfcase value=5000>
            	<cfset errorStruct["error_message"] = "Record(s) not found!">
            </cfcase>
            
            <cfcase value=5010>
            	<cfset errorStruct["error_message"] = "No Option found!">
            </cfcase>
            
            <cfcase value=5001>
            	<cfset errorStruct["error_message"] = "Type not specified">
            </cfcase>
            
            <cfcase value=6000>
            	<cfset errorStruct["error_message"] = "Asset does not exists">
            </cfcase>
            
            <cfcase value=6001>
            	<cfset errorStruct["error_message"] = "Updated Asset">
            </cfcase>
            
            
        	<cfdefaultcase>
            	<cfset errorStruct["error_message"] = "No error message, please contact admin.">
            </cfdefaultcase>
            
            
        </cfswitch>
        
        <cfreturn errorStruct>
        
    </cffunction>

</cfcomponent>