<cfcomponent>

	<!--- Get Markers --->
	<cffunction name="getMarkers" access="public" returntype="query">
		<cfargument name="groupID" type="numeric" required="no" default="-1">
		<cfargument name="appID" type="numeric" required="yes">
        
        <!--- <cfif groupID IS -1><cfset groupID = 0></cfif> --->
        
		<cfquery name="allMarkers">
            SELECT       MarkerAssets.useVisualizer, Assets.asset_id, MarkerAssets.group_id, Assets.name, MarkerAssets.useVuforia, MarkerAssets.useMoodstocks, 
                         Assets.assetType_id, Assets.app_id, Groups.subgroup_id
			FROM         MarkerAssets LEFT OUTER JOIN
                         Groups ON MarkerAssets.group_id = Groups.group_id RIGHT OUTER JOIN
                         Assets ON MarkerAssets.asset_id = Assets.asset_id
            WHERE       0=0
            			<cfif groupID GTE '0'>
                        (Groups.subgroup_id = #groupID#) 
                        </cfif>
                        AND (Assets.app_id = #appID#) 
                        <!--- Only Assets GPS and Marker --->
                        AND ( Assets.assetType_id = 5 OR  Assets.assetType_id = 15)
            ORDER BY 	Assets.assetType_id
        </cfquery>

		<cfreturn allMarkers>
        
	</cffunction>
    
    
    <!--- Get Marker Info --->
	<cffunction name="getMarkerDetails" access="public" returntype="query">
		<cfargument name="groupID" type="numeric" required="yes">
		<cfargument name="assetID" type="numeric" required="yes">
        
        <cfquery name="markerAssets">
            SELECT *
            FROM   MarkerAssets
            WHERE group_id = #groupID# AND asset_id = #assetID#
        </cfquery>

		<cfreturn markerData>
        
	</cffunction>
    
    
    <!--- Set Marker Info --->
	<cffunction name="setMarkerDetails" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="subgroupID" type="numeric" required="yes">
        <cfargument name="state" type="boolean" required="yes" default="false">
        
        <cfif subgroupID IS -1><cfset subgroupID = 0></cfif>
        
        <cfquery name="group">
            SELECT group_id
            FROM   Groups
            WHERE subgroup_id = #subgroupID# AND app_id = #appID#
        </cfquery>
        
        <cfset groupID = group.group_id>
		
        <!--- Update DB --->
        <cfquery name="updateDetails">
            UPDATE MarkerAssets
            <cfif state>
            SET group_id = #groupID#
            <cfelse>
            SET group_id = NULL
            </cfif>
            WHERE (asset_id = #assetID#)
        </cfquery>
        
		<cfreturn true>
        
	</cffunction>
    
    
</cfcomponent>