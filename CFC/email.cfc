<cfcomponent>
    
    <!--- Send ASSET email --->
    <cffunction name="displayEMailAsset" access="remote" returntype="boolean" output="yes">
        <cfargument name="token" type="string" required="yes">
    	
        
        	<!--- Check if Token String is Valid --->
        	<cfinvoke component="Tokens" method="tokenStringValid" returnvariable="validToken">
                <cfinvokeargument name="token" value="#token#"/>
            </cfinvoke>
     	
            <cfif NOT validToken>
            	<cfreturn false>
            </cfif>
      
			<!--- Get Asset From Link --->
            <cfinvoke component="Carts" method="getCartAsset" returnvariable="allInfo">
                <cfinvokeargument name="token" value="#token#"/>
            </cfinvoke>

            <cfif isStruct(allInfo)>
            	<cfif structIsEmpty(allInfo)><cfreturn false></cfif>
            	<cfset allInfo = [allInfo]>
            </cfif>
			
            <cfset displayHeaderCount = 0>
			<cfset displayTotalCount = arrayLen(allInfo)>
           
            <cfloop array="#allInfo#" index="info">
            
				<cfif structIsEmpty(info)><cfreturn false></cfif>
                
                <!--- Get ClientID and AppID from AssetID --->
                <cfinvoke component="Assets" method="getAssetAppClientID" returnvariable="IDs">
                    <cfinvokeargument name="assetID" value="#info.assetID#"/>
                </cfinvoke>
        
                <cfset assetID = info.assetID>
                <cfset appID = info.appID>
                <cfset clientID = IDs.clientID>

                <cfif NOT isDefined("info.groupID")>

					<!--- Get Root Module --->
                    <cfinvoke component="Modules" method="getGroupIDFromAssetID" returnvariable="groupID">
                        <cfinvokeargument name="assetID" value="#assetID#"/>
                    </cfinvoke>
                <cfelse>
                	<cfset groupID = info.groupID>
                </cfif>
                
                <!--- Get Project --->
                <cfinvoke component="Modules" method="getGrouptPath" returnvariable="categories">
                    <cfinvokeargument name="groupID" value="#groupID#"/>
                </cfinvoke>
                
                <cfset ModuleID = categories[arrayLen(categories)].id>     
                
                <!--- Get Module Asset --->
                <cfinvoke component="Modules" method="getGroupDetails" returnvariable="moduleInfo">
                    <cfinvokeargument name="groupID" value="#ModuleID#"/>
                </cfinvoke>
                
                <cfif displayHeaderCount IS 0>
					<!--- Get Mail Config --->
                    <cfinvoke component="EMail" method="getEMailConfig" returnvariable="config">
                        <cfinvokeargument name="appID" value="#appID#"/>
                        <cfinvokeargument name="groupID" value="#moduleID#"/>
                    </cfinvoke>
                </cfif>
                
                <cfif moduleInfo.title NEQ ''>
                    <cfset ModuleName = moduleInfo.title>
                <cfelse>
                    <cfset ModuleName = moduleInfo.name>
                </cfif>
            
                <cfif isDefined("moduleInfo.color_ID")>
                
                	<cfset colorID = moduleInfo.color_ID>
                    <cfif colorID NEQ ''>
                        <cfquery name="colorData"> 
                            SELECT	*
                            FROM	colors
                            WHERE   color_id = #colorID#
                        </cfquery>
                    </cfif>
             
              <cfif isDefined('colorData')>  
               
                    <!--- QueryToStruct --->
                     <cfinvoke component="Misc" method="QueryToStruct" returnvariable="theColorInfo">
                        <cfinvokeargument name="query" value="#colorData#"/>
                        <cfinvokeargument name="forceArray" value="false"/>
                     </cfinvoke>
					
                    <cfinvoke component="Modules" method="getColorData" returnvariable="ModuleColor">
                        <cfinvokeargument name="data" value="#theColorInfo#"/>
                    </cfinvoke>
               
               </cfif>
               	
                    <cfif isDefined("ModuleColor.colors.backcolor")>
                    
                    	<!--- Convert Color RGBtoHex --->
                        <cfinvoke component="Misc" method="rgbToHex" returnvariable="ModuleColor">
                            <cfinvokeargument name="rgbColor" value="#ModuleColor.colors.backcolor#"/>
                        </cfinvoke>

                    <cfelse>
                        <cfset ModuleColor = '666'>
                    </cfif>
                    
                <cfelse>
                
					<cfif moduleInfo.color NEQ ''>
                        <cfset ModuleColor = moduleInfo.color>
                    <cfelse>
                        <cfset ModuleColor = '666'>
                    </cfif>
                
                </cfif>
                
                <!--- Get Paths --->
                <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
                    <cfinvokeargument name="clientID" value="#clientID#"/>
                    <cfinvokeargument name="appID" value="#appID#"/>
                </cfinvoke>
            
                <!--- Get Website --->
                <cfinvoke component="One2" method="getFeatureID" returnvariable="asset">
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="moduleID" value="#ModuleID#"/>
                    <cfinvokeargument name="featureName" value="website"/>
                </cfinvoke>
            
                <cfset assetID = asset.asset_id>
                
                <cfif assetID NEQ "">
                    
                    <cfinvoke component="Assets" method="getAssetInfo" returnvariable="website">
                        <cfinvokeargument name="assetID" value="#assetID#"/>
                    </cfinvoke>
                
					<cfif website.title IS ''>
                        <cfset name = website.assetName>
                    <cfelse>
                        <cfset name = website.title>
                    </cfif>
                
                	<cfset webLink = {"title":name, "url":website.url,"type":website.assetType_id}>
    			
                </cfif>
                
                <!--- Get Location --->
                <cfinvoke component="One2" method="getFeatureID" returnvariable="asset">
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="moduleID" value="#ModuleID#"/>
                    <cfinvokeargument name="featureName" value="location"/>
                </cfinvoke>
                
                <cfif assetID NEQ "">
                
                    <cfset assetID = asset.asset_id>
                
                    <cfinvoke component="Assets" method="getAssetInfo" returnvariable="location">
                        <cfinvokeargument name="assetID" value="#assetID#"/>
                    </cfinvoke>
                    
                    <cfif location.title IS ''>
                        <cfset name = location.assetName>
                    <cfelse>
                        <cfset name = location.title>
                    </cfif>
                    
                    <cfset locLink = {"title":name, "link":location.url,"type":location.assetType_id}>  
                
                </cfif>
                
                <!--- Display Asset --->
                <cfinclude template="emailViewer.cfm">
     
                <!--- TrackAsset --->
                <cfinvoke component="Tracking" method="trackContentAsset" returnvariable="tracked">
                    <cfinvokeargument name="assetID" value="#info.assetID#"/>
                    <cfinvokeargument name="groupID" value="#info.groupID#"/>
                    <cfinvokeargument name="clientID" value="#clientID#"/>
                    <cfinvokeargument name="cartID" value="#info.cartID#"/>
                    <cfinvokeargument name="length" value="5"/>
                    <cfinvokeargument name="auth_token" value="#info.auth_token#"/>
                </cfinvoke>
            
            <cfset displayHeaderCount++>
            
            </cfloop>
            
            <cfreturn tracked>
        
    </cffunction>
    
 
 
 
 
 
 
 	<!--- Send ASSET email --->
    <cffunction name="createCartPage" access="remote" returntype="string" output="yes">
        <cfargument name="cartID" type="numeric" required="yes">
        	
            <!--- Cart Valid? --->
            <cfinvoke component="Carts" method="cartValid" returnvariable="cartValid">
                <cfinvokeargument name="cartID" value="#cartID#"/>
            </cfinvoke>
            
            <cfif NOT cartValid><cfreturn false></cfif>
            
			<!--- Get Cart Assets --->
            <cfinvoke component="Carts" method="getCartAssets" returnvariable="cartInfo">
                <cfinvokeargument name="cartID" value="#cartID#"/> 
            </cfinvoke>

            <cfif structIsEmpty(cartInfo)><cfreturn false></cfif>
       
             <cfset appID = cartInfo.company.appID>
             <cfset company = cartInfo.company.name>
             <cfset categories = cartInfo.company.groupIDs>
             
            <!--- Get Module ID --->
            <!--- <cfinvoke component="One2" method="getModuleFromCartID" returnvariable="moduleID">
                <cfinvokeargument name="cartID" value="#cartID#"/>
            </cfinvoke> --->
 			
			<cfinvoke component="Modules" method="getGrouptPath" returnvariable="Module">
				<cfinvokeargument name="groupID" value="#categories[1].id#">
			</cfinvoke>

			<cfset moduleID = Module[arrayLen(Module)]>
 																								 
  																							
			 <!--- <cfinvoke component="Modules" method="getRootModuleDetails" returnvariable="details">
				<cfinvokeargument name="groupID" value="#mainModule.id#"/> 
			</cfinvoke>
			<cfdump var="#details#"><cfabort>   --->
                  
            <!--- Get Website --->
            <cfinvoke component="One2" method="getFeatureID" returnvariable="asset">
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="moduleID" value="#moduleID.id#"/>
                <cfinvokeargument name="featureName" value="website"/>
            </cfinvoke> 
            
            <cfset assetID = asset.asset_id>
            
			<cfif asset.recordCount GT 0>    
           
				<cfinvoke component="Assets" method="getAssetInfo" returnvariable="website">
					<cfinvokeargument name="assetID" value="#assetID#"/>
				</cfinvoke>

				<cfif website.title IS ''>
					<cfset name = website.assetName>
				<cfelse>
					<cfset name = website.title>
				</cfif>

				<cfset webLink = {"title":name, "url":website.url,"type":website.assetType_id}>
            
            </cfif>
            
            <!--- Get Location --->             
            <cfinvoke component="One2" method="getFeatureID" returnvariable="asset">
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="moduleID" value="#moduleID.id#"/>
                <cfinvokeargument name="featureName" value="location"/>
            </cfinvoke>
         
            <cfset assetID = asset.asset_id>
            
            <cfif asset.recordCount GT 0>
            
              <cfinvoke component="Assets" method="getAssetInfo" returnvariable="location">
                  <cfinvokeargument name="assetID" value="#assetID#"/>
              </cfinvoke>
              
              <cfif location.title IS ''>
                  <cfset name = location.assetName>
              <cfelse>
                  <cfset name = location.title>
              </cfif>
              
              <cfset urlLoc = location.url>
              <cfset infoType = location.assetType_id>
            
            <cfelse>
            	 <cfset urlLoc = ''>
                 <cfset infoType = 0>
            </cfif>
			
			<cfif NOT isDefined('name')><cfset name = ""></cfif>
            <cfset locLink = {"title":name, "url":urlLoc,"type":infoType}>     
            
            <!--- Get Mail Config --->
            <cfinvoke component="EMail" method="getEMailConfig" returnvariable="config">
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="groupID" value="#moduleID.id#"/>
            </cfinvoke>
																									              
            <cfif config.subject IS ''>
            	<cfset subject = cartInfo.email.subject>
            <cfelse>
            	<cfset subject = config.subject>
            </cfif>
            
          
           <!--- Get Paths --->
            <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
                <cfinvokeargument name="clientID" value="#cartInfo.company.clientID#"/>
                <cfinvokeargument name="appID" value="#cartInfo.company.appID#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
            
            
          
			<!--- HTML --->
            <cfsavecontent variable="cartContent">
            	
                <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <title>#company#</title>
                </head>
                <body> 

            	<cfinclude template="emailTemplate.cfm">

                </body>
                </html>
                
			</cfsavecontent>
   
        <!--- Update DB --->
         <cfquery name="result">
              UPDATE SessionCart
              SET sent = 1
              WHERE cart_id = #cartID#
          </cfquery> 
       
        <cfreturn cartContent>
        
    </cffunction>
    
 
   
    
    
    
	<!--- Send ASSET email --->
    <cffunction name="sendEmail" access="remote" returntype="boolean" output="yes">
        <cfargument name="cartID" type="numeric" required="yes">
        <cfargument name="emailVerified" type="numeric" required="no" default="0">	
        	
            
            <cfset success = false>
            
            <!--- Cart Valid? --->
            <cfinvoke component="Carts" method="cartValid" returnvariable="cartValid">
                <cfinvokeargument name="cartID" value="#cartID#"/>
            </cfinvoke>
  
            <cfif NOT cartValid><cfreturn false></cfif>
           																								  
			<!--- Get Cart Assets --->
            <cfinvoke component="Carts" method="getCartAssets" returnvariable="cartInfo">
                <cfinvokeargument name="cartID" value="#cartID#"/> 
            </cfinvoke>
     																													
            <!--- cart is empty --->
            <cfif structIsEmpty(cartInfo)>
            
                <cfquery name="deleteCart">
                      DELETE FROM SessionCart
                      WHERE cart_id = #cartID#
                  </cfquery> 
                
                <cfreturn true>
            
            </cfif>
            
            <cfset appID = cartInfo.company.appID>
            <cfset categories = cartInfo.company.groupIDs>
        
             <cfinvoke component="Modules" method="getGrouptPath" returnvariable="Module">
				<cfinvokeargument name="groupID" value="#categories[1].id#">
			</cfinvoke>

			<cfset moduleID = Module[arrayLen(Module)]>

            <!--- Get Module ID --->
<!---             <cfinvoke component="One2" method="getModuleFromCartID" returnvariable="moduleID">
                <cfinvokeargument name="cartID" value="#cartID#"/>
            </cfinvoke> --->
																									
            <!--- Get Root Module ID --->
            <cfinvoke component="Modules" method="findProjectRoot" returnvariable="moduleRootID">
                <cfinvokeargument name="assetID" value="#moduleID.id#"/>
            </cfinvoke>
            
            <cfif moduleRootID IS -1>
            
            <cfelse>
            	<cfset moduleID.id = moduleRootID>
			</cfif>
            
            <cfif NOT structIsEmpty(moduleID)>
            
				<!--- Get Website --->
                <cfinvoke component="One2" method="getFeatureID" returnvariable="asset">
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="moduleID" value="#moduleID.id#"/>
                    <cfinvokeargument name="featureName" value="website"/>
                </cfinvoke> 
         		
                <cfif asset.recordCount GT 0>
                
					<cfset assetID = asset.asset_id>
                                
                    <cfinvoke component="Assets" method="getAssetInfo" returnvariable="website">
                        <cfinvokeargument name="assetID" value="#assetID#"/>
                    </cfinvoke>
                    
                    <cfif website.title IS ''>
                        <cfset name = website.assetName>
                    <cfelse>
                        <cfset name = website.title>
                    </cfif>
                    
                    <cfset webLink = {"title":name, "url":website.url,"type":website.assetType_id}>
            	<cfelse>
                	<cfset webLink = {"title":'', "url":'',"type":0}>
                </cfif>
            
				<!--- Get Location --->             
                <cfinvoke component="One2" method="getFeatureID" returnvariable="asset">
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="moduleID" value="#moduleID.id#"/>
                    <cfinvokeargument name="featureName" value="location"/>
                </cfinvoke>
         	
                <cfif asset.recordCount GT 0>
                
					<cfset assetID = asset.asset_id>
                    
                    <cfinvoke component="Assets" method="getAssetInfo" returnvariable="location">
                        <cfinvokeargument name="assetID" value="#assetID#"/>
                    </cfinvoke>
                    
                    <cfif location.title IS ''>
                        <cfset name = location.assetName>
                    <cfelse>
                        <cfset name = location.title>
                    </cfif>
        
                    <cfset locLink = {"title":name, "url":location.url,"type":location.assetType_id}> 
                
                <cfelse>
                 	<cfset locLink = {"title":'', "url":'',"type":0}> 
                </cfif>

            <!--- Get Mail Config --->
            <cfinvoke component="EMail" method="getEMailConfig" returnvariable="config">
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="groupID" value="#moduleID.id#"/>
            </cfinvoke>
            
            <cfif config.subject IS ''>
            	<cfset subject = cartInfo.email.subject>
            <cfelse>
            	<cfset subject = config.subject>
            </cfif>
  
           <!--- Get Paths --->
            <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
                <cfinvokeargument name="clientID" value="#cartInfo.company.clientID#"/>
                <cfinvokeargument name="appID" value="#cartInfo.company.appID#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
           
           <!--- if send email missing, delete record --->
  			
           <cfset cartInfo.email.send = reReplace(cartInfo.email.send, "[-?+]", "", "ALL")>           

            <cfif cartInfo.email.send IS ''>
            
              <cfquery name="deleteCart">
                  DELETE FROM SessionCart
                  WHERE cart_id = #cartID#
              </cfquery>   
  			
		       <cfset success = true>
  
  			<cfelse>
              
			   <!--- check if guest is user and use CC email for reply --->
                <cfif cartInfo.email.name IS 'guest'>
                
                    <cfif assetPaths.application.clientEmail IS ''>
                        <cfset replyEMail = config.cc>
                    <cfelse>
                        <cfset replyEMail = assetPaths.application.clientEmail>
                    </cfif>
                   
                    <cfset replyFrom = "#cartInfo.company.name# | #cartInfo.details.title# <#replyEMail#>">
                    <cfset replyTo = "#cartInfo.company.name# | #cartInfo.details.title# <#replyEMail#>">
                    <cfset replyCC = "">
                    
                <cfelse>
                    <cfset replyFrom = "#cartInfo.email.name# <#cartInfo.email.from#>">
                    <cfset replyTo = "#cartInfo.email.name# <#cartInfo.email.from#>">
                    
                    <cfset company = cartInfo.company.name>
                    
                    <cfset title = ''>
                    <cfif structKeyExists(cartInfo,"details")>
                  
                    	<cfset title = cartInfo.details.title>
					</cfif>
                   
                    <cfif title IS ''>
                    	<cfset replyCC = "#cartInfo.company.name#">
                     <cfelse>
                     	<cfset replyCC = "#cartInfo.company.name# | #cartInfo.details.title# <#config.cc#>">
					</cfif> 
                    
                </cfif>
                            
                  <cfmail server="cudaout.media3.net"
                        username="support@wavecoders.ca"
                        from="#replyFrom#"
                        to="#cartInfo.email.send#"
                        cc="#replyCC#"
                        subject="#subject#"
                        replyto="#replyTo#"
                        type="HTML">
                    
                                                                                                            
                    <!--- HTML --->
                    <cfinclude template="emailTemplate.cfm">
  																														<!--- <cfdump var="#cartInfo#"><cfabort> --->																												
                    </cfmail>
                
                <cfif emailVerified><cfset emailVerified = 1><cfelse><cfset emailVerified = 0></cfif>
                
                <!--- Update DB --->
                 <cfquery name="result">
                      UPDATE SessionCart
                      SET sent = 1, emailVerified = #emailVerified#
                      WHERE cart_id = #cartID#
                  </cfquery> 
            
                <cfset success = true>
    
            </cfif>
        
       <cfelse>
       
       		<!--- no content - delete --->
            <cfquery name="deleteCart">
                DELETE FROM SessionCart
                WHERE cart_id = #cartID#
            </cfquery>
            
       		<cfset success = true>
            
       </cfif>
       
        <cfreturn success>
        
    </cffunction>



	<!--- Check if EMail Config Exists --->
	<cffunction name="mailConfigExists" access="remote" returntype="boolean">
        <cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfquery name="mailConfig">
            SELECT        mail_id
            FROM		  EMail
            WHERE         (app_id = #appID#) AND (group_id = #groupID#)
         </cfquery>
         
         <cfif mailConfig.recordCount GT '0'>
         	<cfreturn true>
         <cfelse>
         	<cfreturn false>
         </cfif>
         
    </cffunction>



	<!--- Get EMail Config --->
	<cffunction name="getEMailConfig" access="remote" returntype="struct">
        <cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfset configInfo = {"cc":"", "subject":"", "message":"", "disclaimer":"", 'message_id':0}>
        
        <cfquery name="mailInfo">
            SELECT        EMail.contactEmail, EMail.subject, EMailMessages.message, EMail.disclaimer, EMail.message_id
            FROM          EMail INNER JOIN
                          EMailMessages ON EMail.message_id = EMailMessages.message_id
            WHERE         (EMail.app_id = #appID#) AND (EMail.group_id = #groupID#)
         </cfquery>
  																							   
         <cfset configInfo.cc = mailInfo.contactEMail>
         <cfset configInfo.subject = mailInfo.subject>
         <cfset configInfo.message = mailInfo.message>
         <cfset configInfo.disclaimer = mailInfo.disclaimer>
         <cfset configInfo.message_id = mailInfo.message_id>
         
         <cfreturn configInfo>
         
    </cffunction>
	
    
    
    <!--- Update EMail Config --->
	<cffunction name="updateEMailConfig" access="remote" returntype="struct">
        <cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfargument name="mailInfo" type="struct" required="yes">
        
        <!--- Check if Exists --->
        <cfinvoke component="Email" method="mailConfigExists" returnvariable="configExists">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="groupID" value="#groupID#"/>
        </cfinvoke>
        
        <cfif configExists>
        
			<!--- Get Message ID --->
            <cfinvoke component="Email" method="getEMailConfig" returnvariable="mailMessage">
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="groupID" value="#groupID#"/>
            </cfinvoke>

            <cfset messageID = mailMessage.message_id>
        
			<!--- Update Config --->
            <cfquery name="mailInfoUpdate">     
                UPDATE        EMail
                SET			  contactEmail = '#mailInfo.cc#', subject = '#mailInfo.subject#', disclaimer = '#mailInfo.disclaimer#'
                WHERE         (app_id = #appID#) AND (group_id = #groupID#)
             </cfquery>
             
             <!--- Update Message --->
            <cfquery name="mailInfoUpdate">     
                UPDATE        EMailMessages
                SET			  message = '#mailInfo.message#'
                WHERE         (message_id = #messageID#)
             </cfquery>
         
         <cfelse>
         
         	<!--- Get Client ID --->
         	<cfinvoke component="Clients" method="getClientIDFromAppID" returnvariable="clientID">
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
         
			 <!--- Insert New Config --->
             <cfquery name="messageInfo">     
                INSERT INTO	EMailMessages (message)
                VALUES			  ('#mailInfo.message#')
                SELECT @@IDENTITY AS message_id
             </cfquery>
             
             <cfset messageID = messageInfo.message_id>
             
             <cfquery name="mailInfo">     
                INSERT INTO	EMail (contactEmail, subject, message_id, disclaimer, client_id, app_id, group_id)
                VALUES			  ('#mailInfo.cc#','#mailInfo.subject#',#messageID#,'#mailInfo.disclaimer#',#clientID#,#appID#,#groupID#)
             </cfquery>
         
         </cfif>
         
         <cfset emailInfo = {"cc":mailInfo.cc,"subject":mailInfo.subject,"disclaimer":mailInfo.disclaimer,"message":mailInfo.message}>
         
         <cfreturn emailInfo>
         
    </cffunction>
    
    
    <!--- Update EMail Config --->
	<!--- <cffunction name="updateEMailConfig" access="remote" returntype="boolean">
        <cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfargument name="cc" type="string" required="yes">
        <cfargument name="subject" type="string" required="yes">
        <cfargument name="message" type="string" required="yes">
        <cfargument name="note" type="string" required="yes">
        
        <!--- Check if Exists --->
        <cfinvoke component="Email" method="mailConfigExists" returnvariable="configExists">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="groupID" value="#groupID#"/>
        </cfinvoke>
        
        <cfif configExists>
        
			<!--- Get Message ID --->
            <cfinvoke component="Email" method="getEMailConfig" returnvariable="mailInfo">
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="groupID" value="#groupID#"/>
            </cfinvoke>

            <cfset messageID = mailInfo.message_id>
        
			<!--- Update Config --->
            <cfquery name="mailInfo">     
                UPDATE        EMail
                SET			  contactEmail = '#cc#', subject = '#subject#', disclaimer = '#note#'
                WHERE         (app_id = #appID#) AND (group_id = #groupID#)
             </cfquery>
             
             <!--- Update Message --->
            <cfquery name="mailInfo">     
                UPDATE        EMailMessages
                SET			  message = '#message#'
                WHERE         (message_id = #messageID#)
             </cfquery>
         
         <cfelse>
         
         	<!--- Get Client ID --->
         	<cfinvoke component="Clients" method="getClientIDFromAppID" returnvariable="clientID">
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
         
			 <!--- Insert New Config --->
             <cfquery name="messageInfo">     
                INSERT INTO	EMailMessages (message)
                VALUES			  ('#message#')
                SELECT @@IDENTITY AS message_id
             </cfquery>
             
             <cfset messageID = messageInfo.message_id>
             
             <cfquery name="mailInfo">     
                INSERT INTO	EMail (contactEmail, subject, message_id, disclaimer, client_id, app_id, group_id)
                VALUES			  ('#cc#','#subject#',#messageID#,'#note#',#clientID#,#appID#,#groupID#)
             </cfquery>
         
         </cfif>
         
         <cfreturn true>
         
    </cffunction> --->
    
   <!--- Send ASSET email --->
    <cffunction name="sendInviteEmail" access="remote" output="yes">
        <cfargument name="inviteID" type="numeric" required="yes">
			
          	<!--- Get User Info --->
           	<cfquery name="userInfo">
				SELECT        RSVP.name, RSVP.email, RSVP.app_id, RSVP.date, Applications.client_id, Clients.company AS Company
				FROM          RSVP LEFT OUTER JOIN
                        Applications ON RSVP.app_id = Applications.app_id LEFT OUTER JOIN
                        Clients ON Applications.client_id = Clients.client_id
                WHERE         rsvp_id = #inviteID#
			 </cfquery>
           
           	<cfset appID = userInfo.app_id>
           	<cfset clientID = userInfo.client_id>
           	<cfset clientName = userInfo.company>
           	<cfset emailTo = {'email': userInfo.email, 'name': userInfo.name }>
           	
           	<cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="theDate">
				<cfinvokeargument name="TheEpoch" value="#userInfo.date#"/>
			</cfinvoke>
			<cfset eventDate = "#dateFormat(theDate,'MMM DD, YYYY')# at #timeFormat(theDate,'h:MM tt')#">
           	
            <!--- Get Mail Config --->
			<cfset subject = '#emailTo.name#, #clientName# - Invites you to a Special Event on #eventDate#'>
  
           <!--- Get Paths --->
            <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
                <cfinvokeargument name="clientID" value="#clientID#"/>
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
    
            <cfset path = assetPaths.application.path>
            
            <cfset clientFolder = "#listGetAt(path,listLen(path,'/')-1,'/')#">
            <cfset appFolder = "#listGetAt(path,listLen(path,'/'),'/')#">
   			
          	<cfset rsvpFolder = "../../../#clientFolder#/#appFolder#/rsvp/">
          	<cfset imagesFolder = "http://liveplatform.net/#clientFolder#/#appFolder#/rsvp/images/">
          	
           	<cfset rsvpInviteHTML = "#rsvpFolder#/rsvpInvite.html">
           	<cfset rsvpInviteThanks = "#rsvpFolder#thanks.html">
           	
            <cfset replyTo = assetPaths.application.clientEmail>
            
            <cfset apiURL = "http://liveplatform.net/API/V11/LiveAPI.cfc?method=confirmRSVP&rsvpID=#inviteID#">
           
            <cfmail server="cudaout.media3.net"
                        username="support@wavecoders.ca"
                        from="#replyTo#"
                        to="#emailTo.email#"
                        subject="#subject#"
                        replyto="#replyTo#"
                        type="HTML">
                    
                                                                                                            
                    <!--- HTML --->
                    <cfinclude template="#rsvpInviteHTML#">
    			
              </cfmail>
             <cfdump var="#rsvpInviteThanks#"><cfabort>
             <cflocation url="#rsvpInviteThanks#">
 
			
    </cffunction>
    
</cfcomponent>