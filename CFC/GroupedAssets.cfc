<cfcomponent>


<!--- Sync Assets - Put any assets from old system to new system --->
<cffunction name="syncAssets" access="remote" returntype="boolean">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
        
        <cfif appID GT 0>
        
        	<!--- get all assets for App --->
			<cfquery name="allAssets">
                SELECT	asset_id
                FROM	Assets
                WHERE	app_id = #appID#
            </cfquery>
            
            <cfinvoke component="Misc" method="QueryToArray" returnvariable="oldAssets">
                <cfinvokeargument name="theQuery" value="#allAssets#"/>
                <cfinvokeargument name="theColumName" value="asset_id"/>
            </cfinvoke>
            
            <!--- get Assets in Groups Table to Compare --->
            <cfquery name="allGroupedAssets">
                SELECT	asset_id
                FROM	AssetGroups
                WHERE	app_id = #appID#
            </cfquery>
            
            <cfinvoke component="Misc" method="QueryToArray" returnvariable="newAssets">
                <cfinvokeargument name="theQuery" value="#allGroupedAssets#"/>
                <cfinvokeargument name="theColumName" value="asset_id"/>
            </cfinvoke>
          
            <!--- find difference --->
            <cfset missingAssets = arrayNew(1)>
            <cfloop index="anAsset" array="#oldAssets#">

				<cfif NOT ArrayContains(newAssets,anAsset)>
                   <cfset arrayAppend(missingAssets,anAsset)> 
                </cfif>
            
            </cfloop>
            
            <cfloop index="missingAsset" array="#missingAssets#">
            
				<!--- insert missing assets --->
                <cfquery name="newAssetsForGroup"> 
                    INSERT INTO AssetGroups (sortOrder, app_id, subgroup_id, asset_id)			
                    VALUES (
                    0, 
                    #appID#,
                    0,
                    #missingAsset#
                    )
                </cfquery>
            
            </cfloop>
        
        </cfif>
        
		<cfreturn true>
        
	</cffunction>

	
    
    <!--- get assets and groups --->
    <cffunction name="getAssetsAndGroups" access="remote" returntype="query">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
		<cfargument name="subgroupID" type="numeric" required="no" default="-1">
        <cfargument name="assetType" type="string" required="no" default="all"><!--- all, groups, assets --->
        <cfargument name="assetTypeID" type="numeric" required="no" default="0">
        <cfargument name="search" type="string" required="no" default="">
        <cfargument name="order" type="numeric" required="no" default="0">
     
        <!--- get Assets in Groups Table to Compare --->
        <cfquery name="allGroupedAssets">
            SELECT	AssetGroups.group_id, AssetGroups.asset_id, Thumbnails.image, Assets.modified, Assets.assetType_id, Assets.thumb_id, AssetTypes.icon,
            	    (CASE WHEN Assets.name IS NULL THEN AssetGroups.name ELSE Assets.name END) AS assetName
            <cfif assetType IS "all">
            , (CASE WHEN AssetGroups.asset_id IS NULL THEN 1 ELSE 0 END) AS GroupType
            </cfif>
            FROM   Thumbnails RIGHT OUTER JOIN
                        Assets ON Thumbnails.thumb_id = Assets.thumb_id LEFT OUTER JOIN
                        AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id RIGHT OUTER JOIN
                        AssetGroups ON Assets.asset_id = AssetGroups.asset_id
            WHERE	AssetGroups.app_id = #appID#
            <cfif assetTypeID GT 0>
            AND		Assets.assetType_id = #assetTypeID#
            </cfif>
            <cfif subgroupID GTE 0>
            AND		AssetGroups.subgroup_id = #subgroupID#
            </cfif>
            <cfif assetType IS 'groups'>
            AND 	AssetGroups.asset_id IS NULL
            </cfif>
            <cfif assetType IS 'assets'>
            AND		AssetGroups.asset_id IS NOT NULL
            </cfif>
            <cfif search NEQ ''>
            AND		Assets.name LIKE '%#search#%'
            </cfif>
            ORDER BY 
			<cfif assetType IS "all">
				AssetGroups.asset_id DESC,
			</cfif>
			<cfif order IS 0>
				<!--- AssetGroups.asset_id DESC, --->
			<cfelseif order IS 1>
				assetName ASC,
			<cfelseif order IS 2>
				assetName DESC,
			</cfif>

			AssetGroups.sortOrder ASC

        </cfquery>
     
		<cfreturn allGroupedAssets>
        
	</cffunction>




    <!--- delete Assets --->
 	<cffunction name="deleteAssets" access="remote" returntype="boolean">
    	<cfargument name="appID" type="numeric" required="yes">
    	<cfargument name="assetIDs" type="array" required="no" default="#arrayNew(1)#">
		<cfargument name="groupIDs" type="array" required="no" default="#arrayNew(1)#">
       
        <cfif arrayLen(assetIDs) GT 0>
        
			<!--- Delete asset from Group Assets --->
            <cfquery name="deleteGroupAssets">
                DELETE FROM AssetGroups
                WHERE app_id = #appID# AND asset_id IN(
                
                <cfloop index="assetID" array="#assetIDs#">
                     #assetID#,
                </cfloop>
                0)
                
                
            </cfquery>
         
            <!--- Delete Asset --->
             <cfloop index="assetID" array="#assetIDs#">
             
                <cfinvoke component="Assets" method="deleteAssetDetails" returnvariable="deleted">
                    <cfinvokeargument name="assetID" value="#assetID#"/>
                </cfinvoke>
            
            </cfloop>
        
        </cfif>
        
        <cfif arrayLen(groupIDs) GT 0>
        	
            <cfset allGroups = arrayNew(1)>
            
			<!--- delete asset groups --->	
            <cfloop index="groupID" array="#groupIDs#">
            
                <cfquery name="groupAssets">
                  SELECT	asset_id, group_id
                  FROM		AssetGroups
                  WHERE		subgroup_id = #groupID# OR group_id = #groupID#
                </cfquery>
            	
                <!--- delete assets --->
                <cfloop query="groupAssets">
                	
                    <cfif asset_id NEQ ''>
                    
						<!--- its an asset - delete asset --->
                        <cfinvoke component="Assets" method="deleteAssetDetails" returnvariable="deleted">
                            <cfinvokeargument name="assetID" value="#asset_id#"/>
                        </cfinvoke>
                        
                        <!--- delete from group assets --->
                        <cfquery name="deleteGroupAssets">
                            DELETE FROM AssetGroups
                            WHERE  group_id = #group_id#
                        </cfquery>
                    
                	<cfelse>
                    
						<!--- group content --->
                        <!--- delete from group assets --->
                        <cfquery name="deleteGroupAssets">
                            DELETE FROM AssetGroups
                            WHERE  group_id = #group_id#
                        </cfquery>
                    
                        <cfset arrayAppend(allGroups,group_id)>
                    
                    </cfif>
                    
                </cfloop>
                
                <!--- do any nested group is exists --->
                <cfif arrayLen(allGroups) GT 0>
                
                    <cfinvoke component="GroupedAssets" method="deleteAssets" returnvariable="deleted">
                        <cfinvokeargument name="appID" value="#appID#"/>
                        <cfinvokeargument name="groupIDs" value="#allGroups#"/>
                    </cfinvoke>
                    
                </cfif>
                
            </cfloop>
        
        </cfif>
        
        <cfreturn true>
        
    </cffunction>

    
    
    
     <!--- Add Group --->
 	<cffunction name="createAssetGroup" access="remote" returntype="numeric">
    	<cfargument name="appID" type="numeric" required="yes">
    	<cfargument name="subgroupID" type="numeric" required="no" default="0">
        <cfargument name="name" type="string" required="no" default="untitled">
        
        <cfset groupID = 0>
        
		<cfquery name="newGroup"> 
            INSERT INTO AssetGroups (name, app_id, subgroup_id)			
            VALUES (
            '#name#', 
            #appID# ,
            #subgroupID#
            )
            SELECT @@IDENTITY AS groupID
        </cfquery>

		<cfset groupID = newGroup.groupID>
		
        <cfreturn groupID>
        
    </cffunction>
    
    
    
    <!--- Update Group--->
  	<cffunction name="updateAssetGroup" access="remote" returntype="boolean" output="yes">
    	<cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="name" type="string" required="no" default="untitled">

            <cfquery name="group">
              UPDATE AssetGroups
              SET name = '#name#'
              WHERE	 group_id = #groupID#
          </cfquery>
        
        <cfreturn true>
        
    </cffunction>
    
    
   
   
   
    <!--- Get All Groups--->
  	<cffunction name="getAllAssetGroups" access="remote" returntype="query" output="no">
    	<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="subgroupID" type="numeric" required="no" default="0">
        <cfargument name="orderByName" type="boolean" required="no" default="false">
        
        <cfquery name="groups">
            SELECT        group_id, name
            FROM            AssetGroups
            WHERE        (app_id = #appID#) AND asset_id IS NULL
            ORDER BY
            <cfif orderByName>
            name
            <cfelse>
            sortOrder, group_id
            </cfif>
        </cfquery>
      
        <cfreturn groups>
        
    </cffunction>
    
    
    
    <!--- Move All Group Asset --->
  	<cffunction name="moveAllGroupAssets" access="remote" returntype="boolean" output="no">
    	<cfargument name="subgroupID" type="numeric" required="no" default="0">
        <cfargument name="assetIDs" type="array" required="no" default="#arrayNew(1)#">
        <cfargument name="groupIDs" type="array" required="no" default="#arrayNew(1)#">
		
        <cfif arrayLen(assetIDs) GT 0>
         <!--- assets --->
         <cfquery name="group">
              UPDATE AssetGroups
              SET subgroup_id = #subgroupID#
              
              WHERE asset_id 
              IN(
            <cfloop index="assetID" array="#assetIDs#">
            	 #assetID#,
            </cfloop>
           	   0)
          </cfquery>
          
        </cfif>
        
        <cfif arrayLen(groupIDs) GT 0>
          <!--- groups --->
          <cfquery name="group">
                UPDATE AssetGroups
                SET subgroup_id = #subgroupID#
                
                WHERE group_id 
                IN(
              <cfloop index="groupID" array="#groupIDs#">
                   #groupID#,
              </cfloop>
                 0)
            </cfquery>
          
        </cfif>
        
        <cfreturn true>
        
    </cffunction>

    
    
    <!--- Get Groups from Module --->
 	<cffunction name="getGroups" access="public" returntype="array">
    	<cfargument name="appID" type="numeric" required="no" default="0">
        
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="subgroupID" type="numeric" required="no" default="0">
        
        <cfquery name="allGroups">
            SELECT        group_id, name, subgroup_id
			FROM          AssetGroups
			WHERE		  0 = 0
            			<cfif appID GT 0>
                          AND (app_id = #appID#) 
						</cfif>
                        <cfif projectID GT 0>
                          AND (project_id = #projectID#) 
						</cfif>
                        
                        <cfif groupID GT 0>
                            AND (group_id = #groupID#)
                        <cfelse>
                        
                            <cfif subgroupID GT '0'>
                                AND (subgroup_id = #subgroupID#)
                            <cfelse>
                                AND (subgroup_id = 0)
                            </cfif>
                            
                        </cfif>
            ORDER BY sortOrder
        </cfquery>
       
        <cfset groups = arrayNew(1)>

        <cfloop query="allGroups">
            
            <cfset data = {"group_id":#group_id#, "name":"#name#", "subgroup_id":#subgroup_id#}>
                      
            <cfset arrayAppend(groups,data)>
            
        </cfloop>
        
		<cfreturn groups>
        
	</cffunction>  
   
    
    <!--- BreadCrumb Index--->
 	 <cffunction name="groupPathIndex" access="public" returntype="numeric" output="no">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
     
     	<cfinvoke component="Units" method="getGrouptPath" returnvariable="crumb">
            <cfinvokeargument name="groupID" value="#groupID#"/> 
        </cfinvoke>
        
        <cfreturn arrayLen(crumb)>
        
      </cffunction>
      
      
   
 
 
    	<!--- BreadCrumb --->
 	<cffunction name="getGrouptPath" access="public" returntype="array" output="no">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <cfset crumb = arrayNew(1)>
        
		<!--- Others --->
        <cfloop condition="groupID GT 0">
        
            <cfquery name="theGroup">
                SELECT	name, subgroup_id
                FROM	AssetGroups
                WHERE	group_id = #groupID# 
            </cfquery>
       
            <cfset arrayAppend(crumb,{"name":theGroup.name,"id":groupID})>
            
            <cfset groupID = theGroup.subgroup_id>

        </cfloop>
         
        <cfreturn crumb>
        
      </cffunction>
      
      <!--- BreadCrumb --->
 	<cffunction name="getGrouptPathContent" access="public" returntype="array" output="no">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <cfset crumb = arrayNew(1)>
        
		<!--- Others --->
        <cfloop condition="groupID GT 0">
        
            <cfquery name="theGroup">
                SELECT	name, subgroup_id
                FROM	Groups
                WHERE	group_id = #groupID# 
            </cfquery>
       
            <cfset arrayAppend(crumb,{"name":theGroup.name,"id":groupID})>
            
            <cfset groupID = theGroup.subgroup_id>

        </cfloop>
         
        <cfreturn crumb>
        
      </cffunction>
 
   
   
   	<!--- BreadCrumb --->
 	<cffunction name="getBreadcrumb" access="public" returntype="void" output="yes">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <cfinvoke component="GroupedAssets" method="getGrouptPath" returnvariable="crumb">
            <cfinvokeargument name="groupID" value="#groupID#"/> 
        </cfinvoke>
		
        <cfif arrayLen(crumb) GT 0>
        	<div style="float:left">
            <a href="AppsView.cfm?assetTypeTab=4&subgroupID=0" class="contentLink"><img src="images/root.png"></a>
            </div>
            <cfloop index="z" from="#arrayLen(crumb)#" to="1" step="-1">
                <cfoutput>
                	<div style="margin-top:14px; width:auto; float:left; padding-right:4px">
                    <a href="AppsView.cfm?assetTypeTab=4&subgroupID=#crumb[z].id#" class="contentLink">#crumb[z].name#</a> <span class="contentLinkGrey">/</span>
                    </div>
                </cfoutput>
            </cfloop>
        <cfelse>
        	<div style="float:left;width:auto; float:left">
            <a href="AppsView.cfm?assetTypeTab=4&subgroupID=0" class="contentLink"><img src="images/root.png"></a>
            </div>
        </cfif>
        
    </cffunction>  
    
    
    
    <!--- Import All Group Asset --->
  	<cffunction name="importAllAssetsIntoGroup" access="remote" returntype="struct" output="no">
    	<cfargument name="appID" type="numeric" required="no" default="0">
    	<cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="assetIDs" type="array" required="no" default="#arrayNew(1)#">
        <cfargument name="groupIDs" type="array" required="no" default="#arrayNew(1)#">
    	
        
        <cfset allAssetIDs = arrayNew(1)>
        
        <cfif appID GT 0>

            <!--- Get all assets --->
            <cfinvoke component="GroupedAssets" method="getAllAssets" returnvariable="allAssetIDs">
                <cfinvokeargument name="assetIDs" value="#assetIDs#"/>
                <cfinvokeargument name="groupIDs" value="#groupIDs#"/>
            </cfinvoke>
        
        	<!--- import assets into content --->
            <cfinvoke component="Modules" method="importAssetsIntoContent" returnvariable="items">
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="groupID" value="#groupID#"/>
                <cfinvokeargument name="assetIDs" value="#allAssetIDs#"/>
            </cfinvoke>
    	
        </cfif>
        
        <cfset result = {"total":arrayLen(allAssetIDs), "groupID":groupID}>
        
        <cfreturn result>
        
    </cffunction>
    
    
    
    
    <!--- Import All Group Assets into Action/Assets --->
  	<cffunction name="importAllAssetsIntoAssetsActions" access="remote" returntype="struct" output="no">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="action" type="numeric" required="no" default="0">
        <cfargument name="assetIDs" type="array" required="no" default="#arrayNew(1)#">
        <cfargument name="groupIDs" type="array" required="no" default="#arrayNew(1)#">
    	
        
        <cfset allAssetIDs = arrayNew(1)>
        
        <cfif appID GT 0>
        
			<!--- Get all assets --->
            <cfinvoke component="GroupedAssets" method="getAllAssets" returnvariable="allAssetIDs">
                <cfinvokeargument name="assetIDs" value="#assetIDs#"/>
                <cfinvokeargument name="groupIDs" value="#groupIDs#"/>
            </cfinvoke>
            
			<!--- import assets into asset assets/actions --->
            <cfinvoke component="Assets" method="setGroupAssets" returnvariable="items">
                <cfinvokeargument name="groupAssetID" value="#assetID#"/>
                <cfinvokeargument name="action" value="#action#"/>
                <cfinvokeargument name="assetIDs" value="#allAssetIDs#"/>
            </cfinvoke>
    	
        </cfif>
        
        <cfset result = {"total":arrayLen(allAssetIDs), "assetID":assetID}>
        
        <cfreturn result>
        
    </cffunction>
    
    
    
    
    
    <!--- Import All Group Assets into Action/Assets --->
  	<cffunction name="getAllAssets" access="public" returntype="array" output="no">
        <cfargument name="assetIDs" type="array" required="no" default="#arrayNew(1)#">
        <cfargument name="groupIDs" type="array" required="no" default="#arrayNew(1)#">
    	
        
        <cfset allAssetIDs = arrayNew(1)>

            <cfloop index="groupAssetID" array="#groupIDs#">
            
				<!--- Get all assets from Groups --->
                <cfinvoke component="GroupedAssets" method="getAllAssetsInGroup" returnvariable="data">
                    <cfinvokeargument name="groupID" value="#groupAssetID#"/>
                </cfinvoke>
                
                <!--- merge assets from groups --->
                <cfinvoke component="Misc" method="mergeArrays" returnvariable="allAssetIDs">
                    <cfinvokeargument name="array1" value="#allAssetIDs#"/>
                    <cfinvokeargument name="array2" value="#data#"/>
                </cfinvoke>
            
            </cfloop>
            
            <!--- merge assetIDs --->
            <cfinvoke component="Misc" method="mergeArrays" returnvariable="allAssetIDs">
                <cfinvokeargument name="array1" value="#allAssetIDs#"/>
                <cfinvokeargument name="array2" value="#assetIDs#"/>
            </cfinvoke>
       
        <cfreturn allAssetIDs>
 
    </cffunction>
    
    
    
    
    <!--- Import Assets and assign to Assets or actions --->
	<cffunction name="importAssetAssetsActions" access="remote" returntype="struct">
		
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        <cfargument name="assetIDs" type="array" required="no" default="#arrayNew(1)#">
        <cfargument name="groupIDs" type="array" required="no" default="#arrayNew(1)#">
        <cfargument name="action" type="numeric" required="no" default="0">
        
        <cfset assetsAdded = 0>
        <cfset assetsNotAdded = 0>
        
        <!--- Get all assets --->
        <cfinvoke component="GroupedAssets" method="getAllAssets" returnvariable="allAssetIDs">
            <cfinvokeargument name="assetIDs" value="#assetIDs#"/>
            <cfinvokeargument name="groupIDs" value="#groupIDs#"/>
        </cfinvoke>
	
        <cfloop index="anAsset" array="#allAssetIDs#">																							
            
            <!--- Check if Already Exists --->
            <cfinvoke component="GroupedAssets" method="AssetsActionExists" returnvariable="assetExists">
            	 <cfinvokeargument name="assetID" value="#assetID#"/>
                <cfinvokeargument name="groupAssetID" value="#anAsset#"/>
                <cfinvokeargument name="action" value="#action#"/>
            </cfinvoke>

            <cfif NOT assetExists>
           
                <cfinvoke component="GroupedAssets" method="getAssetAssetsActions" returnvariable="theAsset">
                    <cfinvokeargument name="assetID" value="#assetID#"/>
                    <cfinvokeargument name="action" value="#action#"/>
                </cfinvoke>	
				
                <cfif anAsset NEQ assetID>
                
				<!--- Set Assets from Group --->
                <cfquery name="groupContentAsset">
                    INSERT INTO GroupAssets (content_id , asset_id, accessLevel, cached, active, sharable, sortOrder, action)
                    VALUES (#anAsset#, #assetID#, 0, 0, 0, 0, 0, #action#) 
                </cfquery>
                
                <cfelse>
                	<cfset assetsNotAdded++>
                </cfif>
                
                <cfset assetsAdded++>

            <cfelse>
            	<cfset assetsNotAdded++>
            </cfif>
            
        </cfloop>
        
        <!--- getAssetType ID --->
        <cfinvoke component="GroupedAssets" method="getAssetTypeID" returnvariable="theAssetTypeID">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>	
        
        <cfset allAssets = {"failed": assetsNotAdded,"success": assetsAdded, "assetID":assetID, "assetTypeID":theAssetTypeID}>

          <cfreturn allAssets>
            
	</cffunction>

    
    <!--- get Assets TypeID --->
	<cffunction name="getAssetTypeID" access="public" returntype="numeric">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <cfquery name="assetsActions"> 
            SELECT  assetType_id
            FROM    assets
            WHERE   asset_id = #assetID#
        </cfquery>
        
    	<cfreturn assetsActions.assetType_id>
    
    </cffunction>
    
    
    
    <!--- get Assets and assign to Assets or actions --->
	<cffunction name="getAssetAssetsActions" access="remote" returntype="query">
		
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        <cfargument name="groupAssetID" type="numeric" required="yes" default="0">
        <cfargument name="action" type="numeric" required="no" default="0">
        
        <cfquery name="assetsActions"> 
            SELECT        GroupAssets.asset_id, GroupAssets.content_id, GroupAssets.action, Assets.name, Assets.assetType_id, GroupAssets.active, 
                                GroupAssets.sharable, GroupAssets.cached, GroupAssets.accessLevel, GroupAssets.sortOrder
            FROM            GroupAssets INNER JOIN Assets ON GroupAssets.content_id = Assets.asset_id
            WHERE        (GroupAssets.asset_id = #assetID#) AND GroupAssets.action = #action#
            <cfif groupAssetID GT 0>
            			AND GroupAssets.content_id = #groupAssetID#
            </cfif>
        </cfquery>
        
    	<cfreturn assetsActions>
    
    </cffunction>
    
    
    
    <!---  Assets or actions Exists --->
	<cffunction name="AssetsActionExists" access="remote" returntype="boolean">
		
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        <cfargument name="groupAssetID" type="numeric" required="yes" default="0">
        <cfargument name="action" type="numeric" required="no" default="0">
        
        <cfset exists = false>
        
        <cfinvoke component="GroupedAssets" method="getAssetAssetsActions" returnvariable="theAssets">
            <cfinvokeargument name="assetID" value="#assetID#"/>
            <cfinvokeargument name="groupAssetID" value="#groupAssetID#"/>
            <cfinvokeargument name="action" value="#action#"/>
        </cfinvoke>
        
        <cfif theAssets.recordCount GT 0>
        	<cfset exists = true>
        </cfif>
        
    	<cfreturn exists>
    
    </cffunction>
    
    
    
    <!--- link path for asset item --->
    <cffunction name="getAssetGroupPaths" access="remote" returntype="struct" output="yes">
    	<cfargument name="groupID" type="numeric" required="no" default="0">
        
        <cfset aPath = "">
        
        <cfif groupID GT 0>
        
            <cfinvoke component="GroupedAssets" method="getGrouptPath" returnvariable="crumb">
                <cfinvokeargument name="groupID" value="#groupID#"/> 
            </cfinvoke>
            
                <cfloop index="anItem" array="#crumb#">
                	<cfif anItem.name NEQ ''>
						<cfset hPath = '<a style="font-size=8px" class="contentLinkDisabled" href="AppsView.cfm?assetTypeTab=4&subgroupID=' & anItem.id &'">' & anItem.name & '</a>'>
                        <cfset aPath = hPath &"/"& aPath>
                    </cfif>
                </cfloop>

        </cfif>
        
        <cfif aPath IS ""><cfset aPath = 'root'></cfif>
		
        <cfset data = {'id':groupID, 'text':aPath}>
        
        <cfreturn data>
        
    </cffunction>
    
    
    <!--- select path --->
    <cffunction name="getGroupPathContent" access="remote" returntype="string" output="yes">
    	<cfargument name="groupID" type="numeric" required="no" default="0">
        
        <cfset aPath = "">
        
        <cfif groupID GT 0>
        	
            <cfinvoke component="GroupedAssets" method="getGrouptPathContent" returnvariable="crumb">
                <cfinvokeargument name="groupID" value="#groupID#"/> 
            </cfinvoke>
            
            <cfloop index="anItem" array="#crumb#">
                <cfif anItem.name NEQ ''>
                    <cfset aPath = anItem.name &"/"& aPath>
                </cfif>
            </cfloop>
           
        </cfif>
        
        <cfif aPath IS ""><cfset aPath = ''></cfif>
		
        <cfif len(aPath) GT 0>
        
        	<cfif listLen(aPath) GT 0>
        		<cfset aPath = listGetAt(aPath, listLen(aPath), '/')>
            </cfif>
        	
        </cfif>
        
        <cfreturn aPath>
        
    </cffunction>
    
    
    
    <!--- Get All Assets in Groups --->
  	<cffunction name="getAllAssetsInGroup" access="public" returntype="array" output="no">
    	<cfargument name="groupID" type="numeric" required="no" default="0">
    	
    	<cfquery name="theGroup">
             SELECT COUNT(asset_id) AS count
             FROM	AssetGroups
             WHERE	group_id = #groupID#
        </cfquery>
        
        <cfset allAssetIDs = arrayNew(1)>
        
        <!--- check if group exists --->
        <cfset GroupExists = false>
        <cfif theGroup.recordCount GT 0><cfset GroupExists = true></cfif>
         
        <cfif GroupExists>
            
            <cfquery name="theSubGroups">
                 SELECT asset_id, group_id
                 FROM	AssetGroups
                 WHERE	subgroup_id = #groupID#
            </cfquery>

            <cfoutput query="theSubGroups">
            	
                <cfif asset_id IS ''>
                	
                	<!--- this is a group --->
                    <cfinvoke component="GroupedAssets" method="getAllAssetsInGroup" returnvariable="data">
                        <cfinvokeargument name="groupID" value="#group_id#"/>
                    </cfinvoke>
                 
                    <!--- merge arrays --->
                    <cfinvoke component="Misc" method="mergeArrays" returnvariable="allAssetIDs">
                        <cfinvokeargument name="array1" value="#allAssetIDs#"/>
                        <cfinvokeargument name="array2" value="#data#"/>
                    </cfinvoke>
					
                <cfelse>
                	<cfset arrayAppend(allAssetIDs,asset_id)>
                </cfif>
              
            </cfoutput>
            
         </cfif>
         
         <cfreturn allAssetIDs>
            
    </cffunction>
    
    
    
    <!--- Get All Assets in Groups --->
  	<cffunction name="getVisualAsset" access="public" returntype="struct" output="no">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="assetType" type="numeric" required="no" default="0">
        
        <cfset thePath = ''>
        
		<!--- Get Asset Type Path --->
        <cfinvoke component="File" method="getAssetPaths" returnvariable="assetPaths">
              <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <!--- check asset and get visual rep --->
        <cfinvoke component="Assets" method="getAsset" returnvariable="assetData">
              <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
         <!--- check asset and get visual rep --->
        <cfinvoke component="Assets" method="getAssetThumb" returnvariable="thumbPath">
              <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfset thePath = ''>
        <cfset thePathN = ''>
        
        <cfswitch expression="#assetType#">
        
        <!--- image --->
        <cfcase value="1">
        
        	<cfset thePath = '../../' & assetPaths.asset.xdpi & assetData.url>
            <cfset thePathN = '../../' & assetPaths.asset.mdpi & assetData.url>
        
        </cfcase>
        
        <!--- pano --->
        <cfcase value="8">
        	
        	<cfset thePath = '../../' & assetPaths.asset.xdpi & assetData.pano>
            <cfset thePathN = '../../' & assetPaths.asset.mdpi & assetData.pano>
        
        </cfcase>
        
        <!--- video --->
        <cfcase value="2">
        
        	<cfset thePath = '../../' & assetPaths.thumb.xdpi & thumbPath>
            <cfset thePathN = '../../' & assetPaths.thumb.mdpi & thumbPath>
        
        </cfcase>
        
        <!--- pdf --->
        <cfcase value="3">
        	
            <cfset thePath = '../../' & assetPaths.thumb.xdpi & thumbPath>
            <cfset thePathN = '../../' & assetPaths.thumb.mdpi & thumbPath>
        
        </cfcase>
        
        </cfswitch>
        
        <cfset thePath = {'mdpi':thePathN, 'xdpi':thePath}>
        
        <cfreturn thePath>
    
    </cffunction>
      
      
      
    <!--- Get Group Asset and Verify Path --->
      
       
    <!--- Verify Group Asset Load Path --->
    <cffunction name="groupPathVerified" returntype="boolean" access="public" output="yes">
          <cfargument name="appID" type="numeric" required="no" default="0" />
          <cfargument name="groupPath" type="string" required="yes" default="" />
          <cfargument name="subgroupID" type="numeric" required="no" default="0" />
          
          <cfset pathObjs = ListToArray(groupPath,"/")>
			
			<!--- /Bradley Commons/BC Singles Bungalows/Site Plan --->	
			
			<!--- <cfloop array="#pathObjs#" index="path"> --->
			<cfloop from="1" to="#arrayLen(pathObjs)#" index="pathIndex">
				
				<cfset path = pathObjs[pathIndex]>
				
				<cfquery name="mainPath">
					SELECT        group_id AS groupID, name AS path
					FROM          Groups
					WHERE         name = '#trim(path)#' 
					<cfif subgroupID GT 0> AND subgroup_id = #subgroupID#</cfif>
					<cfif appID GT 0> AND (app_id = #appID#)</cfif>
				</cfquery>
				
				<cfset pathIndex++>
				
				<cfif mainPath.recordCount IS 0>
				
					<!--- check if an asset and NOT a group before returning false --->
					<cfquery name="assetPath">
						SELECT       asset_id, name AS path
						FROM         Assets
						WHERE        name = '#trim(path)#'
					</cfquery>
					
					<cfif assetPath.recordCount GT 0>
						<cfreturn true>
					<cfelse>
						<cfreturn false>
					</cfif>
					
				</cfif>
				
				<cfif pathIndex GT arrayLen(pathObjs)><cfreturn true></cfif>
				<cfset nextPath = pathObjs[pathIndex]>
				
				<cfloop query="#mainPath#">

					<cfinvoke component="GroupedAssets" method="nextValidPath" returnvariable="verified">
						<cfinvokeargument name="subgroupID" value="#groupID#"/>
						<cfinvokeargument name="subPath" value="#nextPath#"/>
					</cfinvoke>

					<cfif verified><cfbreak></cfif>
					
				</cfloop>
				
			</cfloop>
			
            <cfreturn false>
            
    </cffunction>
    
    
         <!--- Verify SubGroup Asset Load Path --->
    <cffunction name="nextValidPath" returntype="boolean" access="public" output="yes">
          <cfargument name="subgroupID" type="numeric" required="yes" />
          <cfargument name="subPath" type="string" required="yes" default="" />
         
			<cfquery name="subPathGroup">
				SELECT        subgroup_id AS groupID, name AS path
				FROM          Groups
				WHERE         name = '#trim(subPath)#' AND subgroup_id = #subgroupID#
			</cfquery>
	
			<cfif subPathGroup.recordCount GT 0>	
				<cfreturn true>
			<cfelse>
				<cfreturn false>
			</cfif>

	</cffunction>
        
</cfcomponent>