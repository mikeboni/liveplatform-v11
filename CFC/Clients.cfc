<cfcomponent>    


   <!---Get ClientID from AppID--->
	<cffunction name="getApps" access="remote" returntype="query">
        
        <cfquery name="apps">
        	SELECT       Applications.bundle_id, Applications.appName
			FROM         Applications LEFT OUTER JOIN Clients ON Applications.client_id = Clients.client_id
        </cfquery>

        <cfreturn apps>
        
	</cffunction>
    
    
    
   <!---Get ClientID from AppID--->
	<cffunction name="getClientIDFromAppID" access="remote" returntype="numeric">
    
		<cfargument name="appID" type="numeric" required="yes" default="0">
        
        <cfquery name="clientInfo">
        	SELECT Clients.client_id
            FROM Clients INNER JOIN Applications ON Clients.client_id = Applications.client_id
            WHERE Applications.app_id = #appID#
        </cfquery>
        
        <cfset clientID = 0>
        
        <cfif clientInfo.recordCount GT '0'>
        	<cfset clientID = clientInfo.client_id>
        </cfif>
        
        <cfreturn clientID>
        
	</cffunction>
    
    
    
    
   <!---Client Active--->
	<cffunction name="clientActive" access="remote" returntype="boolean">
    
		<cfargument name="bundleID" type="string" required="yes" default="">
        
        <cfif bundleID NEQ ''>
        
            <cfquery name="clientApp">
                SELECT        Applications.bundle_id, Clients.active
                FROM          Applications INNER JOIN Clients ON Applications.client_id = Clients.client_id
                WHERE        (Applications.bundle_id = '#bundleID#') AND Clients.active = 1
            </cfquery>
            
            <cfif clientApp.recordCount GT 0>
                <cfreturn true>
            </cfif>
		
        </cfif>
        
        <cfreturn false>
        
	</cffunction>
    
    
    
    
    
<!---Get Client Info--->
	<cffunction name="getClientInfo" access="public" returntype="query" output="yes">
    	
        <cfargument name="auth_token" type="string" required="no" default="">
		<cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="active" type="boolean" required="no" default="false">

        <cfif auth_token NEQ ''>
        
            <cfinvoke component="Users" method="getUserProfileIDs" returnvariable="userProfile">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfset clientID = userProfile.clientID>
            
        </cfif>
        
        <cfset clientsInfo = queryNew("client_id, company, rootPath, path, active, icon, support, contactEmail", "Integer, VarChar, VarChar, VarChar, bit, VarChar, VarChar, VarChar")>
        
        <cfif clientID GT 0>
        
            <cfquery name="clientsInfo">
                SELECT client_id, company, rootPath, path, active, icon, support, contactEmail
                FROM Clients
                
                WHERE 0=0
                <cfif active>
                AND active = #active#
                </cfif>
                <cfif clientID GT '0'>
                AND client_id = #clientID#
                </cfif>
                ORDER BY company
            </cfquery>
         
         </cfif>
            
         <cfif appID GT 0>  
          
            <cfquery name="appInfo">
                SELECT support, clientEMail
                FROM Applications
                
                WHERE 0=0
                <cfif active>
                AND active = #active#
                </cfif>
                AND client_id = #clientID#
                AND app_id = #appID#
            </cfquery>

            <cfinvoke component="Misc" method="QueryToArray" returnvariable="appSupportEmail">
                <cfinvokeargument name="theQuery" value="#appInfo#"/>
                <cfinvokeargument name="theColumName" value="support"/>
            </cfinvoke>
            
            <cfset QueryAddColumn(clientsInfo, "APP_SUPPORT", "VarChar", appSupportEmail)>
            
            <cfinvoke component="Misc" method="QueryToArray" returnvariable="clientAppEmail">
                <cfinvokeargument name="theQuery" value="#appInfo#"/>
                <cfinvokeargument name="theColumName" value="clientEmail"/>
            </cfinvoke>
            
            <cfset QueryAddColumn(clientsInfo, "APP_EMAIL", "VarChar", clientAppEmail)>
        
        </cfif>
        
        <cfreturn clientsInfo>
        
	</cffunction>

<!---Create New Client--->
	<cffunction name="createClient" access="public" returntype="numeric" output="yes">
    
		<cfargument name="company" type="string" required="yes" default="">
        <cfargument name="path" type="string" required="yes" default="">
        <cfargument name="image" type="string" required="no" default="">
        <cfargument name="active" type="boolean" required="no" default="true">
		<cfargument name="support" type="string" required="no" default="">
        <cfargument name="clientEmail" type="string" required="no" default="">
        
        <!---Active--->
        <cfif active><cfset active = '1'><cfelse><cfset active = '0'></cfif>
        
        <!---Check if Client Exists--->
        <cfinvoke component="CFC.clients" method="clientExists" returnvariable="clientID">
            <cfinvokeargument name="company" value="#trim(company)#"/>
		</cfinvoke>

        <cfif clientID IS '0'>

     		<!---Client NOT Exists--->
			<!---Create a new client entry--->
            <cfquery name="newClient">
                 INSERT INTO Clients (company, path, active, support, contactEmail <cfif image NEQ ''>, icon</cfif>)
                 VALUES ('#trim(company)#','#trim(path)#',#active#,'#support#', '#clientEmail#'<cfif image NEQ ''>, '#image#'</cfif>) 
                 SELECT @@IDENTITY AS clientID
            </cfquery>
            
            <cfset clientID = newClient.clientID>
			
 			<!---Client App Folder--->
            <cfinvoke component="CFC.file" method="folderExists" returnvariable="createdFolder">
                <cfinvokeargument name="clientID" value="#clientID#"/>
                <cfinvokeargument name="images" value="yes"/>
                <cfinvokeargument name="createFolder" value="true"/>
            </cfinvoke>           
			
            <!--- Get Client Path --->
            <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="destPath">
                <cfinvokeargument name="clientID" value="#clientID#"/>
                <cfinvokeargument name="images" value="yes"/>
            </cfinvoke>
            
			<cfset destPath = destPath & image>
			
			<!---Move to Client Folder--->
            <cfif image NEQ ''>
            	<cfset tempPath = Application.baseURL & 'temp/' />
                <cfset srcFIle = tempPath & image>
            	<!--- <cfset srcFIle = GetTempDirectory() & image> --->
            	<cffile action="move" source="#srcFIle#" destination="#destPath#" nameconflict="makeunique">
			</cfif>
            
		</cfif>
        
        <cfif image NEQ ''>
			<!---Delete Temp Image--->
            <cfinvoke component="CFC.file" method="deleteTempFile" returnvariable="deleted">
                <cfinvokeargument name="file" value="#image#"/>
            </cfinvoke> 
        </cfif>
        
        <cfreturn clientID>
        
	</cffunction>
    
    
    
    <!---Update Client--->
	<cffunction name="updateClient" access="public" returntype="numeric">
    
		<cfargument name="clientID" type="numeric" required="yes" default="0">
        
		<cfargument name="company" type="string" required="no" default="">
        <cfargument name="path" type="string" required="no" default="">
        <cfargument name="rootPath" type="string" required="no" default="">
        <cfargument name="image" type="string" required="no" default="">
        <cfargument name="active" type="boolean" required="no" default="true">
        <cfargument name="support" type="string" required="no" default="">
        <cfargument name="clientEmail" type="string" required="no" default="">
        
        <!---Active--->
        <cfif active><cfset active = '1'><cfelse><cfset active = '0'></cfif>
        
        <!--- Client and Images Folder Exists --->
		<cfinvoke  component="CFC.File" method="folderExists" returnvariable="folderCreated">
            <cfinvokeargument name="clientID" value="#clientID#"/>
            <cfinvokeargument name="images" value="true"/>
            <cfinvokeargument name="createFolder" value="true"/>
        </cfinvoke>
        
        <!---Client App Folder--->
        <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="path">
            <cfinvokeargument name="clientID" value="#clientID#"/>
            <cfinvokeargument name="images" value="true"/>
        </cfinvoke>
        
        <!---get old image--->
        <cfinvoke  component="CFC.Clients" method="getClientInfo" returnvariable="client">
          <cfinvokeargument name="clientID" value="#clientID#"/>
        </cfinvoke>
   
        <!--- Delete Old Icon --->
        <cfset oldImage = path & client.icon>
             
        <cfif fileExists(oldImage) AND image NEQ ''>
        	<cffile action="delete" file="#oldImage#">
        </cfif>
    
        
        <cfquery name="UpdateClient">
        	UPDATE Clients
            SET active = #active#
            <cfif company NEQ ''>
            ,company ='#name#'
            </cfif> 
            <cfif image NEQ ''>
            ,icon = '#image#'
            </cfif>
            <cfif support NEQ ''>
            ,support = '#support#'
            </cfif>
            <cfif clientEmail NEQ ''>
            ,contactEmail = '#clientEmail#'
            </cfif>
            <cfif rootPath NEQ ''>
            , rootPath = '#rootPath#'
			</cfif>
            WHERE client_id = #clientID#; 
        </cfquery>
    
   
        <cfif image NEQ ''>
        	<!---Move to Client Folder--->
			<cfset srcFile = GetTempDirectory() & image>
            <cfset desFile = path & image>
			
            <cfset dFile =  expandPath('/' & desFile)>
        	<cffile action="move" source="#srcFile#" destination="#dFile#" nameconflict="makeunique">

			<!--- Delete TMP File --->
    		<cfinvoke  component="CFC.File" method="deleteTempFile" returnvariable="tmpDeleted">
              <cfinvokeargument name="file" value="#image#"/>
            </cfinvoke>
            
        </cfif>
      
		<cfreturn clientID>
        
	</cffunction>
    
    <!---Delete Client--->
	<cffunction name="deleteClient" access="public" returntype="struct">
    
		<cfargument name="clientID" type="numeric" required="yes">
        
	    <!---
		<cfquery name="currentClient">
                 DELETE FROM Clients
				 WHERE client_id = #clientID#
        </cfquery>

        <cfreturn {error:true, error_message:"Client Deleted"}>
		--->
        
	</cffunction>
    
    <!---Client Exists--->
	<cffunction name="clientExists" access="public" returntype="numeric">
    
		<cfargument name="company" type="string" required="yes">
		
        <cfset clientID = '0'>
        
        <cfquery name="currentClient">
        	SELECT client_id, company
            FROM Clients
            WHERE company = '#trim(company)#'
        </cfquery>

        <cfif currentClient.recordCount GT '0'>
        	<cfset clientID = currentClient.client_id>
        </cfif>

		<cfreturn clientID>

	</cffunction>
    
    
    <!---Get Client Info--->
	<cffunction name="getClientProjects" access="public" returntype="struct">
    	
		<cfargument name="clientID" type="numeric" required="no" default="0">
        
        <cfset data = {"clients":{}, "apps":{}}>
        
        <cfquery name="clientsProjects">
            SELECT      Clients.client_id, Clients.company, Applications.app_id, Applications.appName
            FROM        Clients LEFT OUTER JOIN Applications ON Clients.client_id = Applications.client_id
            <cfif clientID GT 0>
            WHERE		Clients.client_id = #clientID#
            </cfif>
            ORDER BY	Clients.client_id, Applications.appName
        </cfquery>
         
         <cfoutput query="clientsProjects">
         
		 	<!--- clients --->
		 	<cfif NOT StructKeyExists(data.clients, company)>
            	<cfset structAppend(data.clients,{"#company#":client_id})>
            </cfif>
            
		 	<!--- projects --->
            <cfset projInfo = {"#appName#":app_id}>
            
            <cfif StructKeyExists(data.apps, "#client_id#")>
            	<cfset theObj = StructFindKey( data.apps, #client_id#)>
                <cfset structAppend(theObj[1].value, projInfo)>
            <cfelse>
            	<cfset structAppend(data.apps,{"#client_id#":projInfo})>
            </cfif>
            
		 </cfoutput>
         
         <cfreturn data>
         
    </cffunction>
    
</cfcomponent>