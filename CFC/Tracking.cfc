<cfcomponent> 
 
 <!--- Remove Bookmark --->
    <cffunction name="getBookmarks" access="remote" returntype="struct">
    
        <cfargument name="auth_token" type="string" required="yes">
		<cfargument name="groupID" type="numeric" required="no" default="0">
    	<cfargument name="assetID" type="numeric" required="no" default="0">
    
        <cfset data = structNew()>
        
        <!---ok--->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <!--- Validate Auth Token --->
        <cfinvoke component="Tokens" method="tokenStringValid" returnvariable="tokenValid">
            <cfinvokeargument name="token" value="#auth_token#">
        </cfinvoke>
  
        <cfif tokenValid>
        
			<!--- Get Auth Token info --->
            <cfinvoke component="Tokens" method="getTokenInfo" returnvariable="tokenInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>
			
            <cfset appID = tokenInfo.appID>
            <cfset userID = tokenInfo.userID>
        
        <cfelse>
			
            <cfset userID = 0>
            
        </cfif>
        
        <cfif userID IS 0>
			
			<!---failed--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1003"/>
            </cfinvoke>
            
            <cfset structAppend(data,{'error':error})>
            <cfreturn data>
            
        </cfif>
        
        
        <!--- Get All Bookmarks --->
        <cfquery name="allSessionBookmarks"> 
            SELECT	Bookmarks.bookmark, Bookmarks.watched, Groups.subgroup_id, Groups.asset_id
            FROM 	Bookmarks INNER JOIN Groups ON Bookmarks.group_id = Groups.group_id
            WHERE	Bookmarks.user_id = #userID# AND Bookmarks.app_id = #appID#
            <cfif groupID GT 0>
            AND Groups.subgroup_id = #groupID#
            </cfif>
            <cfif assetID GT 0>
            AND Groups.asset_id = #assetID#
            </cfif>
            ORDER BY Groups.subgroup_id
        </cfquery>
        
        <cfset allBookmarks = structNew()>
        <cfset theGroupID = 0>

        <cfoutput query="allSessionBookmarks">
		
			<cfif subgroup_id NEQ theGroupID>
                <cfset structAppend(allBookmarks, {'#subgroup_id#':[]})>
                <cfset theGroupID = subgroup_id>
            </cfif>
            
            <cfset arrayAppend(allBookmarks[theGroupID],{'assetID':asset_id, 'marker':bookmark, 'watched':watched})>
        
		</cfoutput>
		
        <cfset structAppend(data,{'markers':allBookmarks})>
        <cfset structAppend(data,{'error':error})>
        
 		<cfreturn data>
        
 	</cffunction>
    
    
<!--- Remove Multiple Bookmarks --->
<cffunction name="removeBookmarks" access="remote" returntype="boolean" output="no" hint="Removed Video Bookmark">
	
    <cfargument name="auth_token" type="string" required="yes">
	<cfargument name="groupID" type="numeric" required="no" default="0">
    <cfargument name="assetID" type="numeric" required="no" default="0">
    
    <!--- Validate Auth Token --->
    <cfinvoke component="Tokens" method="tokenStringValid" returnvariable="tokenValid">
        <cfinvokeargument name="token" value="#auth_token#">
    </cfinvoke>
    
    <cfif tokenValid>
        
		<!--- Get Auth Token info --->
        <cfinvoke component="Tokens" method="getTokenInfo" returnvariable="tokenInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
        
        <cfset appID = tokenInfo.appID>
        <cfset userID = tokenInfo.userID>
    
    <cfelse>

        <cfset userID = 0>
        
    </cfif>
    
    <cfif userID IS 0>
    	<cfreturn false>
    </cfif>
    
    <cfif groupID IS 0 AND assetID IS 0>
    
    	<!--- Delete bookmarks for user --->
        <cfinvoke component="Tracking" method="removeBookmark">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="userID" value="#userID#"/>
        </cfinvoke>
        
    <cfelse>
    
		<!--- Delete bookmarks for group or asset --->
        <cfquery name="groupAssets">
            SELECT       group_id
            FROM         Groups
            WHERE        0 = 0
            <cfif groupID GT 0>
            AND subgroup_id = #groupID# 
            </cfif>
            <cfif assetID GT 0>
            AND asset_id = #assetID#
            </cfif>
        </cfquery>
        
        <cfoutput query="groupAssets">
        
            <cfinvoke component="Tracking" method="removeBookmark">
                <cfinvokeargument name="groupAssetID" value="#group_id#"/>
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="userID" value="#userID#"/>
            </cfinvoke>
        
        </cfoutput>
    
    </cfif>
    
    <cfreturn true>

</cffunction>

    
 
<!--- Remove Bookmark --->
    <cffunction name="removeBookmark" access="remote" returntype="boolean">
    
        <cfargument name="userID" type="numeric" required="yes">
        <cfargument name="appID" type="numeric" required="yes">
        
        <cfargument name="groupAssetID" type="numeric" required="no" default="0">
        
		<!--- get CurDate --->
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <cfquery name="deleteBookmark">
             DELETE FROM Bookmarks
             WHERE user_id = #userID# AND app_id = #appID# 
             <cfif groupAssetID GT 0>
             AND group_id = #groupAssetID# 
             </cfif>
        </cfquery>
    	
        <cfreturn true>
        
    </cffunction>
    
        
<!--- Set Bookmarks Multiple --->
<!--- <cfset bookmarks = [{'groupID':1500, 'assetID':2631, 'marker':5000, 'watched':3},{},..]> --->
<cffunction name="setBookmarks" access="remote" returntype="boolean" output="no" hint="Adds Video Bookmarks">
	
    <cfargument name="auth_token" type="string" required="yes">
    <cfargument name="bookmarks" type="array" required="yes">
    
    <cfloop index="aBookmark" array="#bookmarks#">
    
        <cfset groupID = aBookmark.groupID>
        <cfset assetID = aBookmark.assetID>
        <cfset marker = aBookmark.marker>
        <cfset watched = aBookmark.watched>
        
        <cfinvoke component="Tracking" method="setBookmark" returnvariable="success">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
            <cfinvokeargument name="groupID" value="#groupID#"/>
            <cfinvokeargument name="assetID" value="#assetID#"/>
            <cfinvokeargument name="currentTime" value="#marker#"/>
            <cfinvokeargument name="watched" value="#watched#"/>
        </cfinvoke>
    
    </cfloop>
    
    <cfreturn true>

</cffunction>



<!--- Set Bookmark --->
    <cffunction name="setBookmark" access="remote" returntype="boolean">
    
        <cfargument name="auth_token" type="string" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="assetID" type="numeric" required="yes">
		<cfargument name="currentTime" type="numeric" required="no" default="0">
        <cfargument name="watched" type="numeric" required="no" default="0">
        
		<!--- get CurDate --->
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <!--- Validate Auth Token --->
        <cfinvoke component="Tokens" method="tokenStringValid" returnvariable="tokenValid">
            <cfinvokeargument name="token" value="#auth_token#">
        </cfinvoke>
  
        <cfif tokenValid>
        
			<!--- Get Auth Token info --->
            <cfinvoke component="Tokens" method="getTokenInfo" returnvariable="tokenInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>

            <cfset userID = tokenInfo.userID>
        	<cfset appID = tokenInfo.appID>
            
        <cfelse>

            <cfset userID = 0>
            
        </cfif>
        
        <cfif userID IS 0 OR groupID IS 0 OR assetID IS 0><cfreturn false></cfif>
        
        <cfquery name="groupAssets">
            SELECT       group_id
            FROM         Groups
            WHERE        subgroup_id = #groupID# AND asset_id = #assetID#
        </cfquery>
        
        <cfoutput query="groupAssets">
        
			<cfset groupAssetID = groupAssets.group_id>
    
            <!--- Get GroupAssetID from Group + Asset --->
            <cfquery name="currentBookmark"> 
                SELECT	bookmark, date, watched
                FROM 	Bookmarks
                WHERE	user_id = #userID# AND group_id = #groupAssetID# AND app_id = #appID#
            </cfquery>
   
            <cfif currentBookmark.recordCount GT 0>
            
                <!--- Update Bookmark --->
                <cfquery name="updateBookmark">
                    UPDATE Bookmarks
                    SET date = #curDate#, bookmark = #currentTime#, watched = #watched#
                    WHERE group_id = #groupAssetID#
                </cfquery>
                
            <cfelse>
            
                <!--- Insert Bookmark --->
                <cfquery name="newBookmark"> 
                    INSERT INTO Bookmarks (group_id, user_id, app_id, date, bookmark)
                    VALUES (#groupAssetID#, #userID#, #appID#, #curDate#, #currentTime#)
                </cfquery>
                
            </cfif>
        
		</cfoutput>
        
        <cfreturn true>

	</cffunction>


	<!--- auth_token=2BC66C70-BA9B-F3FA-FECC680E3E73E618&groupID=1888&assetID=2561&date=1436446358&duration=120&selection=133,134,117,118&numberOfTries=3 --->
    <!--- Track Quiz Responses --->
    <cffunction name="trackQuizResponse" access="remote" returntype="boolean">
    
        <cfargument name="auth_token" type="string" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="assetID" type="numeric" required="yes">
		<cfargument name="date" type="numeric" required="no" default="0">
        <cfargument name="duration" type="numeric" required="no" default="0">
        <cfargument name="selection" type="string" required="no" default="">
        <cfargument name="numberOfTries" type="numeric" required="no" default="-1">
             
        <!--- CurDate --->
        <cfif date IS '0'>
        	<cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
        <cfelse>
        	<cfset curDate = date>
        </cfif>
        
        <!--- Validate Auth Token --->
        <cfinvoke component="Tokens" method="tokenStringValid" returnvariable="tokenValid">
            <cfinvokeargument name="token" value="#auth_token#">
        </cfinvoke>
  
        <cfif tokenValid>
        
			<!--- Get Auth Token info --->
            <cfinvoke component="Tokens" method="getTokenInfo" returnvariable="tokenInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>

            <cfset userID = tokenInfo.userID>
        
        <cfelse>

            <cfset userID = 0>
            
        </cfif>
        
        <cfif userID IS 0><cfreturn false></cfif>
        
        <!--- search if there is a current record with the date --->
        <cfquery name="quizResponseExists"> 
            SELECT	quizResults_id, dateCompleted, selection_ids
            FROM 	QuizResults
            WHERE	user_id = #userID# AND selection_ids = ''
        </cfquery>
        
		<!--- Question not exist --->
        <cfif quizResponseExists.recordCount IS 0>

			<!--- Record if Complete --->
            <cfif selection NEQ ''>

				<!--- insert in quiz results --->
                <cfquery name="newQuizResponseTracked"> 
                    INSERT INTO QuizResults 
                    (
                    group_id, asset_id, user_id, dateCompleted, duration, selection_ids
                    <cfif numberOfTries GT -1>, numberOfTries</cfif>
                    )
                    VALUES (
                    #groupID#, #assetID#, #userID#, #curDate#, #duration#, '#selection#'
                    <cfif numberOfTries GT -1>, #numberOfTries#</cfif>
                    )
                </cfquery>

            </cfif>
            
		<cfelse>
        
        	<!--- Question Exists Update --->
            <cfset quizID = quizResponseExists.quizResults_id>
           
			<!--- Update Quiz Results --->
            <cfquery name="updateQuizResponseTracked">
                UPDATE QuizResults
                SET dateCompleted = #curDate#, duration = #duration#, selection_ids = '#selection#'
                <cfif numberOfTries GT -1>, numberOfTries = #numberOfTries#</cfif>
                WHERE quizResults_id = #quizID#
            </cfquery>

        </cfif>
        
       <cfreturn true>
        
    </cffunction>    
    
    <!--- http://localhost:8500/liveplatform_dev/API/v10/LiveAPI.cfc?method=resetQuizResponse&auth_token=2BC66C70-BA9B-F3FA-FECC680E3E73E618&groupID=1888&assetID=2561&dateFrom=0&dateTo=0 --->
    <!--- Reset Quiz Responses --->
    <cffunction name="resetQuizResponse" access="remote" returntype="boolean">
    
        <cfargument name="auth_token" type="string" required="yes">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="assetID" type="numeric" required="no" default="0">
		<cfargument name="dateFrom" type="numeric" required="no" default="0">
		<cfargument name="dateTo" type="numeric" required="no" default="0">
        <cfargument name="userID" type="numeric" required="no" default="0">
        
        <cfif userID GT 0>
        
			<!--- Validate Auth Token --->
            <cfinvoke component="Tokens" method="tokenStringValid" returnvariable="tokenValid">
                <cfinvokeargument name="token" value="#auth_token#">
            </cfinvoke>
            
            <cfif tokenValid>
            
                <!--- Get Auth Token info --->
                <cfinvoke component="Tokens" method="getTokenInfo" returnvariable="tokenInfo">
                    <cfinvokeargument name="auth_token" value="#auth_token#">
                </cfinvoke>

                <cfset userID = tokenInfo.userID>
            
            <cfelse>

                <cfset userID = 0>
                
            </cfif>
        
        </cfif>
        
        <cfif userID GT 0>
        
			<!--- search if there is a current record with the date --->
            <cfquery name="quizResponseExists"> 
                SELECT	quizResults_id
                FROM 	QuizResults
                WHERE	user_id = #userID#
                
                <cfif dateFrom GT 0 AND dateTo GT dateFrom>
                    AND (dateCompleted >= #dateFrom# AND dateCompleted <= #dateTo#)
                 </cfif>
                 
                 <cfif groupID GT 0>
                    AND group_id = #groupID#
                 </cfif>
                 
                 <cfif assetID GT 0>
                    AND asset_id = #assetID#
                 </cfif>
                 
            </cfquery>
            
            <cfif quizResponseExists.recordCount GT 0>
            
                <!--- Update DB --->
                <cfquery name="deleteTracking">
                    
                     DELETE FROM QuizResults
                     WHERE user_id = #userID# 
                     
                     <cfif dateFrom GT 0 AND dateTo GT dateFrom>
                        AND (dateCompleted >= #dateFrom# AND dateCompleted <= #dateTo#)
                     </cfif>
                     
                     <cfif groupID GT 0>
                        AND group_id = #groupID#
                     </cfif>
                     
                     <cfif assetID GT 0>
                        AND asset_id = #assetID#
                     </cfif>
                     
                </cfquery>
                
                <cfreturn true>
            
            <cfelse>
                 <cfreturn false>
            </cfif>
        
        </cfif>
        
        <cfreturn false>
        
    </cffunction>
    
    
    <!--- Reset Quiz by App, GroupID, AssetID or User(s) range 																			--->
    <cffunction name="resetQuizResponses" access="public" returntype="boolean">

        <cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="AssetID" type="numeric" required="no" default="0">
        <cfargument name="UserID" type="numeric" required="no" default="0">
    	<cfargument name="UsersID" type="string" required="no" default="">
        
        <!--- Validate User --->
        <cfif UsersID NEQ ''>
        	<cfset users = listToArray(UsersID)>
        <cfelseif UserID GT 0>
        	<cfset users = [UserID]>
        </cfif>
        
        <cfif arrayLen(users) GT 0>
        	
            <cfset allUsers = []>
            
            <cfloop index="aUserID" array="#users#">
            
                <cfinvoke component="Users" method="validateUserID" returnvariable="valid">
                    <cfinvokeargument name="userID" value="#aUserID#">
                </cfinvoke>
            	
                <cfif valid>
                	<cfset arrayAppend(allUsers,aUserID)>
                </cfif>
                
            </cfloop>
            
            <!--- Reset Tracking for User + App --->
            <cfloop index="aUser" array="#allUsers#">
            
                <cfinvoke component="Users" method="resetTrackingData" returnvariable="deletedResponse">
                    <cfinvokeargument name="userID" value="#aUser#">
                    <cfinvokeargument name="groupID" value="#groupID#">
                    <cfinvokeargument name="AssetID" value="#AssetID#">
                </cfinvoke>
                
            </cfloop>
            
            <cfreturn true>
            
        <cfelse>
			<cfreturn false>
        </cfif>
    
    </cffunction>
    
    
    
    
    
    
        
	<!--- Reset Tracking --->
	<cffunction name="resetTrackingData" access="remote" returntype="boolean">
    
        <cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="userID" type="numeric" required="no" default="0">
		<cfargument name="deleteAll" type="boolean" required="no" default="no">
        
        <cfif clientID GT '0' OR appID GT '0' OR userID GT '0' OR deleteAll>
        
            <cfquery name="deleteTracking">
            
                 DELETE FROM Tracking
                 <!--- SELECT track_id FROM Tracking --->
                 
                 WHERE 0 = 0
                 <cfif clientID GT '0'>
                    AND client_id = #clientID#
                 </cfif>
                 <cfif appID GT '0'>
                    AND app_id = #appID#
                 </cfif>
                 <cfif userID GT '0'>
                    AND user_id = #userID#
                 </cfif>
                 
            </cfquery>
            
            <cfreturn true>
    	
        <cfelse>
        	<cfreturn false>
        </cfif>
        
    </cffunction>
    
    
    
	<!--- Track Asset --->
	<cffunction name="trackContentAsset" access="remote" returntype="boolean">
    
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="cartID" type="numeric" required="yes" default="0">
        <cfargument name="date" type="numeric" required="no" default="0">
        <cfargument name="length" type="numeric" required="no" default="0">
        <cfargument name="auth_token" type="string" required="no">
		
        <cfset uniqueThread = CreateUUID()>
        <cfthread action="run" name="trackAsset_#uniqueThread#" assetID="#assetID#" groupID="#groupID#" clientID="#clientID#" cartID="#cartID#" date="#date#" length="#length#" auth_token="#auth_token#">
        
        <!--- Get Cart Token Info --->   
         <cfinvoke component="Carts" method="getCartTokenInfo" returnvariable="tokenInfo">
            <cfinvokeargument name="cartToken" value="#auth_token#"/>
        </cfinvoke>
	
        <cfset cartToken = auth_token>
        
        <cfset userID = 0>
        <cfset appID = 0>
   
        <!--- If Token no longer exists --->
        <cfif tokenInfo.userID IS '' OR tokenInfo.appID IS ''>
        
            <cfinvoke component="Assets" method="getAssetAppClientID" returnvariable="info">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
         	<!--- Get What you can Info --->
            <cfset appID = info.appID>
            
		<cfelse>
        	<!--- Get All Info --->
        	<cfset userID = tokenInfo.userID>
        	<cfset appID = tokenInfo.appID>
        
        </cfif>

        <!--- Get Group Asset --->
        <cfinvoke  component="Modules" method="getContentInfo" returnvariable="contentID">
            <cfinvokeargument name="groupID" value="#groupID#"/>
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>

        <!--- CurDate --->
        <cfif date IS '0'>
        	<cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
        <cfelse>
        	<cfset curDate = date>
        </cfif>
     
        <!--- Get Server IP GPS Coords --->
        <cfinvoke component="Misc" method="getServerGeoLocation" returnvariable="location" />
         
        <!--- Check if location already exists --->
        <cfinvoke component="Tracking" method="IPLocationExists" returnvariable="ip">
            <cfinvokeargument name="ip" value="#location.ip#">
        </cfinvoke>
        
        <cfif location.ip IS 'UNASSIGNED_SYSTEM_ADDRESS'><cfset location.ip = ''></cfif>
   
        <!--- If Exists --->
        <cfif ip.recordCount GT '0'>
        	<cfset locationID = ip.location_id>
        <cfelse>
        
        	<!--- Create Location Entry in DB --->
            <cfquery name="newLocation"> 
                INSERT INTO Locations (ip, city, country_code, country_name, latitude, longitude, region_code, region_name)
                VALUES ('#location.ip#', '#location.city#', '#location.country_code#', '#location.country#', #location.latitude#, #location.longitude#, '#location.region_code#', '#location.region#')
                SELECT @@IDENTITY AS locationID
            </cfquery>
            
            <cfset locationID = newLocation.locationID>
            
        </cfif>
        
        <!--- Get Auth Token --->
        <cfinvoke component="Tokens" method="getAuthToken" returnvariable="tokens">
            <cfinvokeargument name="userID" value="#userID#">
            <cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
        
        <cfset auth_token = tokens.token>
          
        <!--- Validate Auth Token --->
        <cfinvoke component="Tokens" method="tokenStringValid" returnvariable="tokenValid">
            <cfinvokeargument name="token" value="#auth_token#">
        </cfinvoke>
  
        <cfif tokenValid>
        
			<!--- Get Auth Token info --->
            <cfinvoke component="Tokens" method="getTokenInfo" returnvariable="tokenInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>

            <cfset appID = tokenInfo.appID>
            <cfset userID = tokenInfo.userID>
        
        <cfelse>
        
            <cfset userID = 0>
            
        </cfif>

        <!--- Get Device Type from Token --->
        <cfinvoke component="Users" method="getDeviceFromToken" returnvariable="deviceType">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
        
        <!--- Get User AccessLevel --->
        <cfinvoke component="Users" method="getUserAccess" returnvariable="access">
            <cfinvokeargument name="userID" value="#userID#"/>
        </cfinvoke>
        
        <cfif access.accessLevel GTE 4><cfreturn true></cfif><!--- if admin level do not record tracking --->
   
        <!--- Track Asset --->
        <cfquery name="newTrackedAsset"> 
            INSERT INTO Tracking (content_id, created, user_id, location_id, length, device_id, client_id, app_id, cart_id)
            VALUES (#contentID#, #curDate#, #userID#, #locationID#, #length#, #deviceType#, #clientID#, #appID#, #cartID#)
        </cfquery>
																																											  
        
        <!--- Get Current Views of Cart --->
        <cfquery name="allViews">
            SELECT viewes
            FROM SessionCart
            WHERE token = '#cartToken#'
        </cfquery>
  
        <cfif allViews.viewes IS ''>
			<cfset currentView = 0>
		<cfelse>
        	<cfset currentView = allViews.viewes>
        </cfif>
        
        <cfset currentView++>
        
        <!--- Update Viewed Cart --->
        <cfquery name="updateAsset">
            UPDATE SessionCart
            SET viewed = 1
            WHERE token = '#cartToken#'
        </cfquery>
        
        <cfquery name="updateAsset">
            UPDATE SessionCart
            SET viewes = #currentView#
            WHERE token = '#cartToken#'
        </cfquery>
        
	   </cfthread> 
       
        <cfreturn true>
        
	</cffunction>

	
  
    
    <!--- Asset Tracking --->
    <cffunction name="trackContentAssets" access="public" returntype="boolean">

        <cfargument name="assets" type="string" required="yes">
        <cfargument name="auth_token" type="string" required="yes">
        
        <cfset uniqueThread = RandRange( 1, 1000 )>
        
        <!--- process on a thread --->
        <cfthread action="run" name="saveTracking_#uniqueThread#" assets="#assets#" auth_token="#auth_token#">
        
			<!--- Convert JSON to Object --->
            <cfset allAssetsToTrack = deserializeJSON(assets)>
            
            <!--- Save all Tracking --->
           
            <cfloop index="z" from="1" to="#arrayLen(allAssetsToTrack)#">
            
                <cfset anAsset = allAssetsToTrack[z]>
                
                <cfif NOT structKeyExists(anAsset,"assetID")>
                    <cfset structAppend(anAsset,{"assetID":0})>
                </cfif>
                
                <cfif NOT structKeyExists(anAsset,"groupID")>
                    <cfset structAppend(anAsset,{"groupID":0})>
                </cfif>
                
                <cfif NOT structKeyExists(anAsset,"date")>
                    <cfset structAppend(anAsset,{"date":0})>
                </cfif>
                
                <cfif NOT structKeyExists(anAsset,"length")>
                    <cfset structAppend(anAsset,{"length":0})>
                </cfif>
                
                <!--- Track Asset --->
                <cfinvoke component="Tracking" method="trackContentAsset" returnvariable="success">
                    <cfinvokeargument name="assetID" value="#anAsset.assetID#">
                    <cfinvokeargument name="groupID" value="#anAsset.groupID#">
                    <cfinvokeargument name="date" value="#anAsset.date#">
                    <cfinvokeargument name="length" value="#anAsset.length#">
                    <cfinvokeargument name="auth_token" value="#auth_token#">
                </cfinvoke>
                
            </cfloop>
		
        </cfthread> 
        
        <cfreturn true>
        
	</cffunction>
 
 
     <!--- check Tracking IP--->
    <cffunction name="IPLocationExists" access="remote" returntype="query">

        <cfargument name="ip" type="string" required="yes">
		
        <cfset foundIP = queryNew("location_id","Integer")>
        
        <!--- Find IP --->
        <cfquery name="foundIP">
            SELECT        location_id
            FROM       	  Locations
            WHERE        (ip = '#trim(ip)#')
        </cfquery>
        
        <cfreturn foundIP>
        
	</cffunction>   

	
    
    
	<cffunction name="getIPLocation" access="remote" returntype="numeric">
		<cfargument name="ip" type="string" required="no" default="">
        
        <!--- Get GPS Location --->
        <cfinvoke component="Misc" method="getServerGeoLocation" returnvariable="location">
        	<cfinvokeargument name="ip" value="#ip#">
        </cfinvoke>
            
    	<!--- Check if location already exists --->
        <cfinvoke component="Tracking" method="IPLocationExists" returnvariable="ip">
            <cfinvokeargument name="ip" value="#location.ip#">
        </cfinvoke>
        
        <!--- If Exists --->
        <cfif ip.recordCount GT '0'>
        	<cfset locationID = ip.location_id>
        <cfelse>
        
        <cfif location.ip IS 'UNASSIGNED_SYSTEM_ADDRESS'><cfset location.ip = '0.0.0.0'></cfif>
        
        	<!--- Create Location Entry in DB --->
            <cfquery name="newLocation"> 
                INSERT INTO Locations (ip, city, country_code, country_name, latitude, longitude, region_code, region_name)
                VALUES ('#location.ip#', '#location.city#', '#location.country_code#', '#location.country#', #location.latitude#, #location.longitude#, '#location.region_code#', '#location.region#')
                SELECT @@IDENTITY AS locationID
            </cfquery>
            
            <cfset locationID = newLocation.locationID>
            
        </cfif>
        
        <cfreturn locationID>
    
	</cffunction>    
    
    

	<!--- Session Tracking --->
    
    <!--- Is Session Valid? --->
	<cffunction name="isSessionValid" access="public" returntype="boolean">
    
		<cfargument name="auth_token" type="string" required="no">
        
		<!--- getTokenID from AuthToken --->
        <cfinvoke component="Tokens" method="getTokenID" returnvariable="tokenInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
        
        <cfif StructIsEmpty(tokenInfo)>
        	<cfreturn 0>
        <cfelse>
        
			<!--- Find Session with TokenID--->
            <cfquery name="foundSession">
                SELECT        trackSession_id
                FROM       	  sessionTracking
                WHERE         token_id = #tokenInfo.token_id# AND location_end IS NULL
            </cfquery>
       
			<!--- Check if Session Exists --->
            <cfif foundSession.recordCount GT '0'>
                <cfreturn foundSession.trackSession_id>
            <cfelse>
                <cfreturn 0>
            </cfif>
            
		</cfif>
        
	</cffunction>
    
    
    <!--- Start Session --->
	<cffunction name="startSession" access="public" returntype="boolean">
    
		<cfargument name="auth_token" type="string" required="yes">
        <cfargument name="ip" type="string" required="no">
        
        <!--- Check if Session Exists --->
        <cfinvoke component="Tracking" method="isSessionValid" returnvariable="sessionID">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
        
        <cfif sessionID GT '0'>
        
        	<!--- Close Open Session and Start New Session --->
            <cfinvoke component="Tracking" method="endSession" returnvariable="success">
                <cfinvokeargument name="auth_token" value="#auth_token#">
                <cfinvokeargument name="sessionLength" value="900"><!--- 60*15 = 15 minutes default max session time --->
            </cfinvoke>
            
        </cfif>
        
        <!--- getToken Info from AuthToken --->
        <cfinvoke component="Tokens" method="getTokenID" returnvariable="tokenInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
        
        <!--- Current Date --->
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate"></cfinvoke>
        

		<!--- Get GPS Location --->
        <cfinvoke component="Tracking" method="getIPLocation" returnvariable="locationID">
			<cfinvokeargument name="ip" value="#ip#">
        </cfinvoke>
      
        <!--- Start New Session --->
        <cfquery name="newSession"> 
            INSERT INTO SessionTracking (user_id, created, location_start, token_id)
            VALUES (#tokenInfo.user_id#, #curDate#,#locationID#,#tokenInfo.token_id#)
        </cfquery>
        
        <cfreturn true>
        
	</cffunction>
    
    
    <!--- End Session --->
	<cffunction name="endSession" access="public" returntype="boolean" output="yes">
    
		<cfargument name="auth_token" type="string" required="yes">
        <cfargument name="sessionLength" type="numeric" required="yes" default="0">
        <cfargument name="ip" type="string" required="no">
        
        <!--- Get SessionID --->
        <cfinvoke component="Tracking" method="isSessionValid" returnvariable="sessionID">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>

		<cfif sessionID GT '0'>
			
            <!--- Get GPS Location --->
            <cfinvoke component="Tracking" method="getIPLocation" returnvariable="locationID">
                <cfinvokeargument name="ip" value="#ip#">
            </cfinvoke>
           
            <!--- Update Session and Close --->
            <cfquery name="result">
                UPDATE	SessionTracking
                SET 	length = #sessionLength#, location_end =#locationID#
                WHERE	trackSession_id = '#sessionID#' 
            </cfquery>

        </cfif>
       
       <cfreturn true>
        
	</cffunction>
    
    
    
    <!--- Save Sessions --->
    <!--- [{"date":394234324, "length":100, "location_start":"192.168.0.10", "location_end":"192.168.0.1" },{"date":394234324, "length":100, "location_start":"192.168.0.1", "location_end":"192.168.0.1" }] --->
    
	<cffunction name="saveSessions" access="public" returntype="boolean" output="yes">
    
		<cfargument name="auth_token" type="string" required="yes">
        <cfargument name="sessions" type="string" required="yes">
        
        <!--- Convert JSON to Object --->
        <cfset allSessions = deserializeJSON(sessions)>
        
        <!--- getToken Info from AuthToken --->
        <cfinvoke component="Tokens" method="getTokenID" returnvariable="tokenInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
        
        <!--- Save all Sessions --->

        <cfloop index="z" from="1" to="#arrayLen(allSessions)#">
        
        	<cfset aSession = allSessions[z]>
        	
            <cfif NOT structKeyExists(aSession,"date")>
            	<cfset structAppend(aSession,{"date":0})>
            </cfif>
            
            <cfif NOT structKeyExists(aSession,"length")>
            	<cfset structAppend(aSession,{"length":900})><!--- 60*15 = 15 minutes default max session time --->
            </cfif>
            
            <cfset locationID_start = 0>
            <cfset locationID_end = 0>
            
            <cfif structKeyExists(aSession,"location_start")>
            
                <!--- Get GPS Location START --->
                <cfinvoke component="Tracking" method="getIPLocation" returnvariable="locationID_start">
                    <cfinvokeargument name="ip" value="#aSession.location_start#">
                </cfinvoke>
                    
				<cfif NOT structKeyExists(aSession,"location_end") AND structKeyExists(aSession,"location_start")>
                    <cfset locationID_end = locationID_start>
                    
                <cfelseif structKeyExists(aSession,"location_end")>
                
                	<!--- Get GPS Location END --->
                	<cfinvoke component="Tracking" method="getIPLocation" returnvariable="locationID_end">
                        <cfinvokeargument name="ip" value="#aSession.location_end#">
                    </cfinvoke>
                    
                </cfif>
 
			</cfif>
            

            <!--- Save Session --->
            <cfquery name="newSession"> 
                INSERT INTO SessionTracking (user_id, created, length, location_start, location_end, token_id)
                VALUES (#tokenInfo.user_id#, #aSession.date#, #aSession.length#, #locationID_start#, #locationID_end#, #tokenInfo.token_id#)
            </cfquery>
            
        </cfloop>
       
       <cfreturn true>
        
	</cffunction>
    
    
    
    
    <!--- Get Tracked Assets From Cart --->
	<cffunction name="getTrackedContentFromCart" access="public" returntype="array" output="yes">
    
		<cfargument name="cartID" type="numeric" required="yes" default="0">
        <cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="clientID" type="numeric" required="no" default="0">
        
        <!--- Find Session with TokenID--->
        <cfquery name="trackedAssets">
            SELECT        content_id
            FROM       	  Tracking
            WHERE         0=0
            <cfif userID GT 0>
            AND user_id = #userID#
            </cfif>
            <cfif cartID GT 0>
            AND cart_id = #cartID#
            </cfif>
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
            <cfif clientID GT 0>
            AND client_id = #clientID#
            </cfif>
        </cfquery>
        
        <cfinvoke component="Misc" method="QueryToArray" returnvariable="contentIDs">
            <cfinvokeargument name="theQuery" value="#trackedAssets#">
            <cfinvokeargument name="theColumName" value="content_id">
        </cfinvoke>
       
       <cfreturn contentIDs>
        
	</cffunction>
    
    
</cfcomponent>