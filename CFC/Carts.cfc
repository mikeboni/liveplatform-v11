<cfcomponent>

	<!--- Create a Cart --->
	<cffunction name="createCart" access="public" returntype="numeric">
		<cfargument name="auth_token" type="string" required="yes">
        <cfargument name="email" type="string" required="no" default="">
        <cfargument name="message" type="string" required="no" default="">
        <cfargument name="content" type="string" required="yes">
        
        <cfset cartID = 0>

		<!--- Get UserID --->
        <cfinvoke component="Tokens" method="getToken" returnvariable="tokenInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
        
        <cfset appID = tokenInfo.appID>
        <cfset userID = tokenInfo.userID>
        
        <!--- Get User Info --->
        <cfinvoke component="Users" method="getUserInfo" returnvariable="userInfo">
            <cfinvokeargument name="userID" value="#userID#">
        </cfinvoke>
 
        <!--- Get Epoch --->
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
   																														
        <cfif userInfo.recordCount GT '0'>
        
			<!--- UserID, UserEmail --->
            <cfset userID = userInfo.user_id>
            <cfset userName = userInfo.name>
            <cfset userEMail = userInfo.email>

            <!--- New Token --->
            <cfinvoke component="Tokens" method="createToken" returnvariable="newToken" />
          
            <!--- Get GPS Location --->
            <cfset locationID = 0>
            <cfinvoke component="Tracking" method="getIPLocation" returnvariable="locationID" />  
            
      	
            <!--- Convert Content Struct to ContentIDs --->
            <cfinvoke component="Carts" method="getContentIDs" returnvariable="contentIDs">
                <cfinvokeargument name="content" value="#content#">
            </cfinvoke>
																																																																																																	
            <!--- Generate Short URL --->
            <cfinvoke component="Misc" method="shortUrl" returnvariable="url" />
            
			<!--- userID, email, contentIDS, date, locationID, newToken --->
        	<cfquery name="newCart"> 
                INSERT INTO SessionCart (user_id, date, contentIDs, location_id, message, email, token, app_id)
                VALUES (#userID#, #curDate#, '#contentIDs#', #locationID#, '#message#', '#email#', '#newToken#', #appID#)
                SELECT @@IDENTITY AS cartID
            </cfquery>
            
            <cfset cartID = newCart.cartID>

        </cfif>	
                
		<cfreturn cartID>
        
	</cffunction>
    


    <!--- Convert Content Struct to ContentIDS --->
    <!--- [{"groupID":316,"assetID":136},{"groupID":316,"assetID":151}] --->
	<cffunction name="getContentIDs" access="public" returntype="string">
		<cfargument name="content" type="string" required="yes">
    
    	<!--- Convert JSON to Object --->
        <cfset allAssets = deserializeJSON(content)>
        
        <cfset contentIDS = arrayNew(1)>
        
        <cfif IsStruct(allAssets)>
       	 	 <cfset allAssets = [allAssets]>
		</cfif>

   		<cfloop array="#allAssets#" index="aGroup">    
        <!--- <cfloop collection="#allAssets#" item="aGroup"> --->
         
          <cfloop collection="#aGroup#" item="theGroupID">
            
            <!--- <cfset groupAssets = structFind(aGroup, theGroupID)> --->
			<cfset groupAssets = aGroup[theGroupID]>
																		
																				
            <cfif arrayLen(groupAssets) IS 0>
                <!--- Group with all assets --->
                <!--- Get All ContentIDs for GroupID --->
				
                <!--- Get Group Assets --->
                <cfquery name="allContentFromGroup">
                    SELECT   asset_id
                    FROM     Groups
                    WHERE    (subgroup_id = #theGroupID#) AND active = 1
                    ORDER BY sortOrder
                </cfquery>
              																											
                 <!--- Convert Query to Array --->
                <cfinvoke component="Misc" method="QueryToArray" returnvariable="contentIDArray">
                    <cfinvokeargument name="theQuery" value="#allContentFromGroup#">
                    <cfinvokeargument name="theColumName" value="asset_id">
                </cfinvoke>

                <cfloop array="#contentIDArray#" index="aAsset"> 
                
                	<cfif aAsset NEQ ''>
                	
						<!--- Get ContentID --->
						<cfinvoke component="Modules" method="getContentID" returnvariable="contentID">
							<cfinvokeargument name="groupID" value="#theGroupID#">
							<cfinvokeargument name="assetID" value="#aAsset#">
						</cfinvoke>
	        		
		        		<cfset arrayAppend(contentIDS,aAsset)>
		        		
			        <cfelse>
			        	<cfif aAsset NEQ ''>
			        		<cfset arrayAppend(contentIDS,contentID)>
						</cfif>
				    </cfif>
               
                </cfloop>
                
            <cfelse>
																														<!--- GroupAssets:<cfdump var="#groupAssets#"> --->
                <!--- individual assets --->
                <cfloop array="#groupAssets#" index="aAsset">

                    <!--- Get ContentID --->
                    <cfinvoke component="Modules" method="getContentID" returnvariable="contentID">
                        <cfinvokeargument name="groupID" value="#theGroupID#">
                        <cfinvokeargument name="assetID" value="#aAsset#">
                    </cfinvoke>
                																										<!--- GroupID:<cfdump var="#theGroupID#"> --->
                																										<!--- AssetID:<cfdump var="#aAsset#"> --->
                	<!---  IF NO GroupID (Grouped Asset) then add Assets --->											<!--- ContentID:<cfdump var="#contentID#"> --->
					<cfif theGroupID IS contentID>
						<cfset contentID = aAsset>
					</cfif>																						
                    <cfset arrayAppend(contentIDS,contentID)>
                    
                </cfloop>
           
            </cfif>
            
        </cfloop>
        
	</cfloop>
       
        <cfreturn ArrayToList(contentIDS,",")>
        
    </cffunction>
    
    
    
    
    <!--- Parse Cart Asset from URL Token --->
	<cffunction name="parseCartAsset" access="public" returntype="struct">
		<cfargument name="token" type="string" required="yes">
    	
        <cfif listLen(token,"-") IS 6>
			<!--- New Token --->
            <cfset cartID = listLast(token,"-")>
            <cfset contentID = listGetAt(token, listLen(token,"-")-1,"-")>
            <cfset token = listDeleteAt(token, listLen(token,"-")-1,"-")>
            
        <cfelse>
			<!--- Old Token --->
            <cfset cartID = 0>
            <cfset contentID = listLast(token,"-")>
        
        </cfif>

        <cfset contentToken = ''>

        <cfloop index="z" from="1" to="#ListLen(token,"-")-1#">
        	<cfset contentToken = contentToken & listGetAt(token,z,"-") & "-">
        </cfloop>

        <cfset contentToken = left(contentToken,len(contentToken)-1)>
        
        <cfset contentAsset = {"token":contentToken, "cartID":cartID, "contentID":contentID}>
          
        <cfreturn contentAsset>
        
    </cffunction>




    <!--- Get Cart Assets --->
	<cffunction name="getCartAssets" access="remote" returntype="struct" output="no">
		<cfargument name="cartID" type="numeric" required="yes">

        <cfquery name="cartInfo"> 
            SELECT      Users.name, Users.email, SessionCart.message, SessionCart.email AS clientEmail, SessionCart.contentIDs, Clients.company, Applications.icon, Clients.client_id, 
                        SessionCart.app_id, SessionCart.token
			FROM        SessionCart LEFT OUTER JOIN
                        Applications ON SessionCart.app_id = Applications.app_id LEFT OUTER JOIN
                        Clients ON Applications.client_id = Clients.client_id LEFT OUTER JOIN
                        Users ON SessionCart.user_id = Users.user_id
            WHERE       SessionCart.cart_id = #cartID#
        </cfquery>

        <cfif cartInfo.recordCount GT '0'>
       
		 <!--- Get Client Path --->
         <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="clientPath">
            <cfinvokeargument name="clientID" value="#cartInfo.client_id#">
            <cfinvokeargument name="appID" value="#cartInfo.app_id#">
            <cfinvokeargument name="server" value="yes">
        </cfinvoke>

  																																		
        <cfset data = structNew()>
        
        <cfset appIcon = clientPath & "images/" & cartInfo.icon>
        <cfset structAppend(data,{"company":{"name":cartInfo.company, "icon":appIcon}})>
        
        <!--- Email Info --->
        <cfset emailInfo = structNew()>
        <cfset structAppend(emailInfo,{"send":cartInfo.clientEmail})>
        
        <cfset structAppend(emailInfo,{"name":cartInfo.name})>
       
        <cfif cartInfo.email NEQ ''>
        	<cfset structAppend(emailInfo,{"from":cartInfo.email})>
        <cfelse>
        	<cfset structAppend(emailInfo,{"from":"support@liveplatform.net"})>
        </cfif>
        
        <cfset structAppend(emailInfo,{"subject":"Requested Information from #cartInfo.name# - #cartInfo.company#"})>
        <cfset structAppend(emailInfo,{"message":cartInfo.message})>
        
        <cfset structAppend(data,{"email":emailInfo})>
        
        <cfset allContent = cartInfo.contentIDs>
        <cfset token = cartInfo.token>
        
        <cfset urlLinks = structNew()>
        <cfset groupIDs =  arrayNew(1)>
		
        <cfset theAsset =  structNew()>
    
        <cfif allContent NEQ ''>
   
       		<cfloop index="anAsset" list="#allContent#" delimiters=",">
       
        	<cfset assetToken = token &"-"& anAsset &"-"& cartID>
            <!--- get cart asset --->
            <cfinvoke component="Carts" method="getCartAsset" returnvariable="assetsInfo">
            	<cfinvokeargument name="token" value="#assetToken#">
            </cfinvoke>
																																<!--- <cfdump var="#cartInfo.contentIDs#"><cfdump var="#assetsInfo#"><cfabort>  ---> 	    				  																													
            <cfif arrayIsEmpty(assetsInfo)>
            	<!--- nothing --->
            <cfelse>
             
             
             <cfloop array="#assetsInfo#" index="info">
             
               <!--- Normal Asset --->
                <cfinvoke component="Modules" method="getGrouptPath" returnvariable="crumb">
                    <cfinvokeargument name="groupID" value="#info.groupID#">
                </cfinvoke>
             	
             	<cfif arrayLen(crumb) IS 0>
               		<!--- Get Group Asset<cfdump var="#info#"><cfabort> --->
               																														<!--- <cfdump var="#info#"><cfabort> --->
               	<cfelse>														
                	<!--- Get Root Category --->
                	<cfset groupID = crumb[arrayLen(crumb)-1]>
				</cfif>
                
                
                <cfif arrayFind(groupIDs,groupID) GT 0>
                    <!--- Match --->
                <cfelse>
                    <cfset arrayAppend(groupIDs,groupID)>
                </cfif>
   
                <cfif NOT structIsEmpty(info)>
                    <cfset groupDetailID = info.groupID> 
                </cfif>
             																									
                <cfif StructIsEmpty(info)>
                    <!--- No Asset Found --->
                <cfelse>
                
                    <cfif info.details.title IS ''>
                        <cfset groupName = info.groupName>
                    <cfelse>
                        <cfset groupName = info.details.title>
                    </cfif>

                    <cfset theAsset = {"thumbnail":info.thumb,"urlLink":assetToken,"name":groupName, "type":info.assetType}>
        
                    <!--- find group --->
                    <cfset foundGroup = StructKeyExists(urlLinks, info.groupName)>
     
                    <!--- if no group add group --->
                    <cfif foundGroup>
                        <cfset arrayAppend(urlLinks["#info.groupName#"],theAsset)>
                    <cfelse>
                        <cfset structAppend(urlLinks,{"#info.groupName#":[theAsset]})>
                    </cfif>
               	
                    <!--- If Balcony Add Assets --->
                    <cfif info.assetType IS 9>
                   
                        <cfinvoke component="Assets" method="getAssets" returnvariable="balconyAsset">
                            <cfinvokeargument name="assetID" value="#info.assetID#">
                        </cfinvoke>
                            
                        <cfset balcony = balconyAsset.name>
                        
                        <cfif balconyAsset.title NEQ ''>
                            <cfset theAsset.name = balconyAsset.title>
                        <cfelse>
                            <cfset theAsset.name = balconyAsset.assetName>
                        </cfif>

                    </cfif>
  
                </cfif>
         	
            </cfloop>
            
            </cfif>
            
        </cfloop>	
																																
        </cfif>
        
        <cfset structAppend(data.company,{"groupIDs":groupIDs, "appID":cartInfo.app_id, "clientID":cartInfo.client_id})>

        <cfset structAppend(data,{"assets":urlLinks})>
     
			<cfif NOT isDefined("groupDetailID")>
                <!--- nothing --->
            <cfelse>
            
                <!--- Get Module Details --->
                <cfinvoke component="Modules" method="getRootModuleDetails" returnvariable="details">
                    <cfinvokeargument name="groupID" value="#groupDetailID#"/> 
                </cfinvoke>
             
                <!--- Find Categories --->
                <cfloop index="z" from="1" to="#arrayLen(groupIDs)#">
                    <cfif groupIDs[z].name IS "categories">
                        <cfset contentID = groupIDs[z].id>
                        <cfbreak>
                    <cfelse>
                        <cfset contentID = 0>
                    </cfif>
                    
                </cfloop>
                <!--- Get Category Details --->
                <cfinvoke component="Modules" method="getGroupDetails" returnvariable="assets">
                    <cfinvokeargument name="groupID" value="#contentID#"/>
                </cfinvoke>
                
                
                <cfif details.description IS ''>
                    <cfset description = assets.description>
                <cfelse>
                    <cfset description = details.description><!--- Old Location for Description --->
                </cfif>        
                
                <cfset moduleIcon = clientPath & "images/thumbs/" & details.image>
                <cfset structAppend(data,{"details":{"title":details.title, "subtitle":details.subtitle ,"description":description, "image":moduleIcon}})>
                
            </cfif>    
	       
        <cfelse>
        	<cfset data = structNew()>
		</cfif>
  	 																					
    	<cfreturn data>
        
    </cffunction>
    
     
     
     
     
    <!--- Sorts EMail Assets into Main Groups (GroupIDS are the bins to sort into) --->
 	<cffunction name="sortEMailAssets" access="public" returntype="struct">
		<cfargument name="cartAssets" type="struct" required="yes">
        <cfargument name="groupIDs" type="array" required="yes">
        <cfargument name="appID" type="numeric" required="yes">
	
		<cfset newAssets = arrayNew(1)>
        <cfset data = structNew()>
        
        
        
        <cfloop index="Z" from="1" to="#arrayLen(groupIDs)#">
            
            <cfinvoke component="Modules" method="getAppGroups" returnvariable="groups">
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="groupID" value="#groupIDs[z].id#"/>
            </cfinvoke>
         																							
            <cfset groupName = groupIDs[z].name>
            
            <cfset structAppend(data, {"#groupName#":[]})>
            
            <!--- Start Sort of Assets --->
			<cfloop index="grp" array="#groups#">
			
            	<cfif StructKeyExists(cartAssets, grp.name)>
					
                    <cfset theObj = cartAssets[grp.name]>
                    
                    <cfif NOT arrayIsEmpty(theObj)>
                        <cfset arrayAppend(data[groupName],{"#grp.name#":theObj})>
					</cfif>
                                        
                </cfif>
            
            
			</cfloop>
            
        
        </cfloop>

        <cfreturn data>
        
    </cffunction>      
     
     
     
     
    <!--- Cart URL exists --->
 	<cffunction name="cartURLExists" access="public" returntype="boolean">
		<cfargument name="url" type="string" required="yes">
        
        <cfquery name="cart"> 
            SELECT cart_id
            FROM SessionCart
            WHERE tinyurl = '#url#'
        </cfquery>

		<cfif cart.recordCount GT '0'>
        	<cfreturn true>
		<cfelse>
        	<cfreturn false>
        </cfif>
        
	</cffunction> 
     
     


     <!--- Cart Valid --->
 	<cffunction name="cartValid" access="public" returntype="boolean">
		<cfargument name="cartID" type="numeric" required="yes">
        
        <cfquery name="cart"> 
            SELECT cart_id
            FROM SessionCart
            WHERE cart_id = #cartID#
        </cfquery>

		<cfif cart.recordCount GT '0'>
        	<cfreturn true>
		<cfelse>
        	<cfreturn false>
        </cfif>
        
	</cffunction>     
     

 
     <!--- Get AuthToken of User from Sent Token --->
	<cffunction name="getUserAuthToken" access="public" returntype="string">
		<cfargument name="token" type="string" required="yes">
		<cfargument name="appID" type="numeric" required="yes">
        
        <!--- Parse Token from token link --->
		<cfinvoke component="Carts" method="parseCartAsset" returnvariable="session">
            <cfinvokeargument name="token" value="#token#">
        </cfinvoke>

        <cfset token = session.token>
        
        <!--- Get UserID --->
        <cfquery name="userAuthToken"> 
            SELECT user_id
            FROM SessionCart
            WHERE token = '#trim(token)#'
        </cfquery>

        <cfif userAuthToken.recordCount GT '0'>
        	<cfset userID = userAuthToken.user_id>
        <cfelse>
        	<cfset userID = 0>
        </cfif>
        
        <!--- Get Auth Token --->
        <cfquery name="aToken"> 
            SELECT token
            FROM Tokens
            WHERE user_id = #userID# AND app_id = #appID#
        </cfquery>
       
        <cfset auth_token = aToken.token>
   
        <cfreturn auth_token>
        
    </cffunction>
    
    

 
 
    <!--- Get Cart Asset --->
	<cffunction name="getCartAsset" access="public" returntype="array" output="no">
		<cfargument name="token" type="string" required="yes">
        
        <cfset assetURL = ''>
        
    	<!--- Get GroupID from token link --->
		<cfinvoke component="Carts" method="parseCartAsset" returnvariable="contentAsset">
            <cfinvokeargument name="token" value="#token#">
        </cfinvoke>
		
        <cfset auth_token = contentAsset.token>
        <cfset cartID = contentAsset.cartID>
        
   																												   
        <!--- Get AssetID --->
        <cfinvoke component="Modules" method="getAssetIDFromContentGroup" returnvariable="asset">
            <cfinvokeargument name="groupID" value="#contentAsset.contentID#">
        </cfinvoke>
																									
		<cfif asset.assetID IS ''><cfset asset.assetID = 0></cfif>
																										<!--- getCartAsset:<cfdump var="#contentAsset#"><cfdump var="#asset#"><cfabort> --->																																																		
		<!--- Get Asset --->
        <cfinvoke  component="Modules" method="getContentAsset" returnvariable="theAssetData">
            <cfinvokeargument name="assetID" value="#asset.assetID#"/>
        </cfinvoke>
 		
        <!--- <cfinvoke  component="Modules" method="getGroupAssets" returnvariable="theAssetData">
            <cfinvokeargument name="assetID" value="#asset.assetID#"/>
            <cfinvokeargument name="appID" value="#asset.appID#"/>
        </cfinvoke> --->
  
        <!--- Get Group --->
        <cfinvoke  component="Modules" method="getGroupName" returnvariable="groupInfo">
            <cfinvokeargument name="subgroupID" value="#asset.groupID#"/>
        </cfinvoke>
        
        <cfset allAssets = arrayNew(1)>
          	
        <cfset assetInfo = structNew()>
       
        <cfif asset.assetID GT '0'>
        
            <cfloop collection="#theAssetData#" item="theKey"></cfloop>
            <cfset theAsset = theAssetData[theKey]>
           
            
             
            <!--- Group Asset --->
            <cfif theAsset.assetType IS 18>
         
                <cfif theAsset.display.type IS "None">
					<cfloop array="#theAsset.assets#" index="anAsset">

						<cfloop collection="#anAsset#" item="theKey"></cfloop>

						<cfset assetGroupInfo = structNew()>

						<!--- Build Struct for Details --->
						<cfinvoke component="Carts" method="createObjectInfo" returnvariable="assetGroupDetails">
							<cfinvokeargument name="objectData" value="#anAsset[theKey]#"/> 
							<cfinvokeargument name="groupInfo" value="#groupInfo#"/> 
						</cfinvoke>

						<cfset structAppend(assetGroupDetails,{"groupID":asset.groupID})>
						<cfset structAppend(assetGroupDetails,{"cartID":cartID})>
						<cfset structAppend(assetGroupDetails,{"appID":asset.appID})>
						<cfset structAppend(assetGroupDetails,{"auth_token":auth_token})>

						<cfset structAppend(assetGroupInfo,assetGroupDetails)>
						<cfset arrayAppend(allAssets,assetGroupInfo)>

					</cfloop>
				</cfif>
           
            <cfelse>
            <!--- Normal Assets --->	
            	
				<!--- Build Struct for Details --->
                <cfinvoke component="Carts" method="createObjectInfo" returnvariable="assetDetails">
                    <cfinvokeargument name="objectData" value="#theAsset#"/> 
                    <cfinvokeargument name="groupInfo" value="#groupInfo#"/> 
                </cfinvoke>
    
                <cfset structAppend(assetDetails,{"groupID":asset.groupID})>
                <cfset structAppend(assetDetails,{"cartID":cartID})>
                <cfset structAppend(assetDetails,{"appID":asset.appID})>
                <cfset structAppend(assetDetails,{"auth_token":auth_token})>
                
                <cfset structAppend(assetInfo,assetDetails)>    
  				<cfset arrayAppend(allAssets,assetInfo)>
            </cfif>
            
           <!---  <!--- <cfset infoDetails = structNew()>
            
            <cfif NOT structKeyExists(theAsset,"details")>
            
            	<cfset structAppend(infoDetails,{"title":groupInfo.name})>
                <cfset structAppend(infoDetails,{"subtitle":""})>
                <cfset structAppend(infoDetails,{"description":""})>
           	
            <cfelse>
            
				<cfif structKeyExists(theAsset.details,"title")>
                    <cfset structAppend(infoDetails,{"title":theAsset.details.title})>
                <cfelse>
                    <cfset structAppend(infoDetails,{"title":groupInfo.name})>
                </cfif>
                
                <cfif structKeyExists(theAsset.details,"subtitle")>
                    <cfset structAppend(infoDetails,{"subtitle":theAsset.details.subtitle})>
                <cfelse>
                    <cfset structAppend(infoDetails,{"subtitle":""})>
                </cfif>
                
                <cfif structKeyExists(theAsset.details,"description")>
                    <cfset structAppend(infoDetails,{"description":theAsset.details.description})>
                <cfelse>
                    <cfset structAppend(infoDetails,{"description":""})>
                </cfif>
            
            </cfif> --->
            
            <!--- Check AssetType Accepted --->
            <!--- <cfinvoke component="Misc" method="emailSupportType" returnvariable="emailTypeAccepted">
                <cfinvokeargument name="assetType" value="#theAsset.assetType#"/> 
            </cfinvoke>
			
            <cfset assetInfo = structNew()>
            <cfset pano = false>

            <cfif emailTypeAccepted>
          
            	<cfif NOT structKeyExists(theAsset,"thumb")>
                	<cfset thumbImg = "">
                <cfelse>
                	<cfset thumbImg = theAsset.thumb.mdpi.url>
                </cfif>

               
              <!--- MAY NEED TO CHANGE ADD TO THESE TO SUPPORT NEW OBJECT MODEL 																	--->
              <!--- SUPPORTS VIDEO, NEED TO CHECK ALL ASSET TYPES 																					--->
              
                	<cfif theAsset.assetType IS 9 OR theAsset.assetType IS 8> <!--- Balconies AND Panoramas---><!---  --->
  
						<!--- Use 360 Pano Image --->
                        <cfif isDefined('theAsset.pano')>
                            <cfset pano = true>
                            <cfset assetURLPano = theAsset.pano.url.xdpi>
                        </cfif>

                        <cfset assetURL = theAsset.thumb.url.xdpi>
                    
                    </cfif>
                      
                    <cfif theAsset.assetType IS 2>
                    	<!--- Video URL --->
                        <cfset assetURL = theAsset.video.url.xdpi>
                    </cfif>
                        
                    <cfif theAsset.assetType IS 1>
                    	<!--- Image URL --->                  
                        <cfif isDefined("theAsset.image.url.xdpi")>
                			<cfset assetURL = theAsset.image.url.xdpi>
                		<cfelseif isDefined("theAsset.image.url.mdpi")>
                			<cfset assetURL = theAsset.image.url.mdpi>
                    	</cfif>
                    
                    </cfif>
                    
                    
                    <cfif theAsset.assetType IS 18>
                    	
                        <cfloop array="#theAsset.assets#" index="anAsset">
							 
                        </cfloop>
                        Grouped Asset
                        <cfabort>
                        
             		</cfif>
                    
			 </cfif> ---> --->
         <cfelse>
         	<!--- <cfdump var="#assetInfo#"><cfabort>   --->  	
         </cfif>
                
            <!--- <cfset structAppend(assetInfo,{"url":assetURL, "assetType":theAsset.assetType, "thumb":thumbImg, "groupName":groupInfo.name, "details":infoDetails, "assetID":theAsset.assetID, "groupID":asset.groupID, "cartID":cartID, "appID":asset.appID, "auth_token":auth_token})>
                
                <cfif pano>
                	<cfset structAppend(assetInfo,{"panourl":assetURLPano})>
                </cfif> --->
                
			<!--- </cfif> 
            
        </cfif> --->

        <cfreturn allAssets>
        
    </cffunction>   
    
    
    <!--- Create EMail Object Strcut --->
    <cffunction name="createObjectInfo" access="public" returntype="struct" output="no">
    	<cfargument name="objectData" type="struct" required="yes">
    	<cfargument name="groupInfo" type="struct" required="no" default="#structNew()#">
        
        <cfset theAsset = structNew()>
        
        <cfif structIsEmpty(groupInfo)>
        	<cfset groupInfo = {"access":0, "active":1, "id":0, "name":""}>
        </cfif>
        
		<!--- Build Struct for Details --->
            <cfset infoDetails = {"title":groupInfo.name, "subtitle":"", "description":""}>
            <cfif NOT isDefined("objectData.details")>
            	<cfset structAppend(objectData,{details:infoDetails})>
            </cfif>
            
			<cfif structKeyExists(objectData.details,"title")>
                <cfset structAppend(infoDetails,{"title":objectData.details.title})>
            <cfelse>
                <cfset structAppend(infoDetails,{"title":groupInfo.name})>
            </cfif>
            
            <cfif structKeyExists(objectData.details,"subtitle")>
                <cfset structAppend(infoDetails,{"subtitle":objectData.details.subtitle})>
            <cfelse>
                <cfset structAppend(infoDetails,{"subtitle":""})>
            </cfif>
            
            <cfif structKeyExists(objectData.details,"description")>
                <cfset structAppend(infoDetails,{"description":objectData.details.description})>
            <cfelse>
                <cfset structAppend(infoDetails,{"description":""})>
            </cfif>
            
            <cfif NOT isDefined("objectData.assetType")>
            	<!--- <cfdump var="#objectData#"> --->
            <cfelse>
            
				<!--- Check AssetType Accepted --->
                <cfinvoke component="Misc" method="emailSupportType" returnvariable="emailTypeAccepted">
                    <cfinvokeargument name="assetType" value="#objectData.assetType#"/> 
                </cfinvoke>
                
                <cfset assetInfo = structNew()>
                <cfset pano = false>
    
                <cfif emailTypeAccepted>
              		
                  <cfset thumbImg = "">
                  
                  <cfif structKeyExists(objectData,"thumb")>
                   
                  <cfif isDefined("objectData.thumb.url")>
                  		
                        <cfif isDefined("objectData.thumb.url.xdpi")>
                        	<cfset thumbImg = objectData.thumb.url.xdpi>
                        <cfelse>
                        	<cfset thumbImg = objectData.thumb.url.mdpi>
						</cfif>

                  <cfelse>
                       
                        <cfif isDefined("objectData.thumb.xdpi.url")>
                        	<cfset thumbImg = objectData.thumb.xdpi.url>
                        <cfelse>
                        	<cfset thumbImg = objectData.thumb.mdpi.url>
						</cfif>
                        
                  </cfif>
                   
                  </cfif>
                 
                    <!--- MAY NEED TO CHANGE ADD TO THESE TO SUPPORT NEW OBJECT MODEL 																	--->
                    <!--- SUPPORTS VIDEO, NEED TO CHECK ALL ASSET TYPES 																					--->
                
                  <cfif objectData.assetType IS 9 OR objectData.assetType IS 8> <!--- Balconies AND Panoramas---><!---  --->
    
                      <!--- Use 360 Pano Image --->
                      <cfif isDefined('objectData.pano')>
                          <cfset pano = true>
                          <cfif structKeyExists(objectData.pano, 'url')>
                          	<cfset assetPanoURL = objectData.pano.url.xdpi>
                          <cfelse>
                          	<cfset assetPanoURL = ''>
						  </cfif>
                      </cfif>
            			
            		<cfif structKeyExists(objectData,'thumb')>   
            		           
						  <cfif NOT isDefined('objectData.thumb.xdpi')>
							<cfset assetURL = objectData.thumb.xdpi>
						  <cfelse>
							<cfset assetURL = objectData.thumb.xdpi>
						  </cfif>
					
						  <cfif isDefined('objectData.thumb.xdpi')>
							<cfset thumbImg = objectData.thumb.xdpi.url><!--- old v10--->
						  <cfelse>
							<cfset thumbImg = objectData.thumb.url.xdpi><!--- new --->
						  </cfif>
					 <cfelse>
                    	<cfset assetURL = ''>
                     </cfif> 
                     
                  </cfif>
                    
                  <cfif objectData.assetType IS 2>
                      <!--- Video URL --->
                      <cfset assetURL = objectData.video.url.xdpi>
                  </cfif>
                  
                   <cfif objectData.assetType IS 3>
                      <!--- Doc - PDF URL --->
                      <cfset assetURL = objectData.url>
                  </cfif>
                      
                  <cfif objectData.assetType IS 1>
                  
                      <!--- Image URL --->                  
                      <cfif isDefined("objectData.image.url.xdpi")>
                          <cfset assetURL = objectData.image.url.xdpi>
                      <cfelseif isDefined("objectData.image.url.mdpi")>
                          <cfset assetURL = objectData.image.url.mdpi>
                      </cfif>
                  	  
                  </cfif>
                  
                  <cfif objectData.assetType IS 4>
                  	<!--- URL --->
                  	<cfset assetURL = objectData.url>
                  </cfif>
                  
    		
                 <cfset theAsset = {"assetID":objectData.assetID,"url":assetURL, "assetType":objectData.assetType, "thumb":thumbImg, "groupName":groupInfo.name, "details":infoDetails}>
				 
				 <cfif isDefined("objectData.image")>
                 	<cfset size = {"width":objectData.image.width, "height":objectData.image.height}>
				 	<cfset structAppend(theAsset, {"size":size})>
				 </cfif>
                 
                 <cfif isDefined("objectData.pano")>
				 	<cfset structAppend(theAsset, {"size":objectData.thumb.xdpi.size})>
                    <cfset structAppend(theAsset, {"panoURL":assetPanoURL})>
				 </cfif>
            
                </cfif>
            
            </cfif>
            
        <cfreturn theAsset>
    
    </cffunction>
    
    <!--- Get Cart Asset --->
<!--- 	<cffunction name="numberOfAssetsInCart" access="public" returntype="numeric" output="no">
    	<cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">  
        <cfargument name="userID" type="numeric" required="no" default="0">  
		<cfargument name="cartID" type="numeric" required="no" default="0">    
    
        <cfquery name="numberOfAssets"> 
            SELECT      SUM(LEN(SessionCart.contentIDs) - LEN(REPLACE(SessionCart.contentIDs, ',', '')) + 1) AS assetCount
			FROM        SessionCart INNER JOIN
                		Applications ON SessionCart.app_id = Applications.app_id
            WHERE 0=0
            <cfif clientID GT '0'>
            	AND Applications.client_id = #clientID#
            </cfif>
            <cfif appID GT '0'>
            	AND Applications.app_id = #appID#
            </cfif>
            <cfif userID GT '0'>
            	AND user_id = #userID#
            </cfif>
            <cfif cartID GT '0'>
            	AND cart_id = #cartID#
            </cfif>
        </cfquery>
   
    	<cfif numberOfAssets.assetCount IS ''>
    		<cfreturn 0>
    	<cfelse>
        	<cfreturn numberOfAssets.assetCount>
        </cfif>
        
    </cffunction> --->
    
    
    
    
    
    <!--- Get Cart Asset --->
	<cffunction name="numberOfAssetsInCart" access="public" returntype="numeric" output="no">
    	<cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">  
        <cfargument name="userID" type="numeric" required="no" default="0">  
		<cfargument name="cartID" type="numeric" required="no" default="0">    
    
        <cfquery name="numberOfAssets"> 
            SELECT      SUM(LEN(SessionCart.contentIDs) - LEN(REPLACE(SessionCart.contentIDs, ',', '')) + 1) AS assetCount
			FROM        SessionCart INNER JOIN
                		Applications ON SessionCart.app_id = Applications.app_id
            WHERE 0=0
            <cfif clientID GT '0'>
            	AND Applications.client_id = #clientID#
            </cfif>
            <cfif appID GT '0'>
            	AND Applications.app_id = #appID#
            </cfif>
            <cfif userID GT '0'>
            	AND user_id = #userID#
            </cfif>
            <cfif cartID GT '0'>
            	AND cart_id = #cartID#
            </cfif>
        </cfquery>
   
    	<cfif numberOfAssets.assetCount IS ''>
    		<cfreturn 0>
    	<cfelse>
        	<cfreturn numberOfAssets.assetCount>
        </cfif>
        
    </cffunction>
    
    
    
    <!--- Get Cart Token Info --->
	<cffunction name="getCartTokenInfo" access="public" returntype="struct" output="no">
    	<cfargument name="cartToken" type="string" required="no" default="0">
    
        <cfquery name="cartInfo"> 
            SELECT user_id, app_id 
			FROM   SessionCart   
            WHERE token = '#cartToken#'
        </cfquery>
        
        <cfset info = {"userID":cartInfo.user_id, "appID":cartInfo.app_id}>
        
        <cfreturn info>
        
    </cffunction>
    
    
    
    
    <!--- Get Number of Carts  --->
	<cffunction name="getNumberOfCarts" access="public" returntype="numeric" output="no">
    	<cfargument name="userID" type="numeric" required="yes" default="0">
    
        <cfquery name="cartInfo"> 
            SELECT count(user_id) as carts
			FROM   SessionCart   
            WHERE user_id = '#userID#'
        </cfquery>
        
        <cfreturn cartInfo.carts>
        
    </cffunction>
    
    
    
    
    <!--- Get Cart Customers EMail Info --->
	<cffunction name="getCartCustomerHistory" access="public" returntype="query" output="no">
    	<cfargument name="userID" type="numeric" required="no" default="0">
    	<cfargument name="appID" type="numeric" required="no" default="0">
    	<cfargument name="viewed" type="numeric" required="no" default="0">
        
        <cfquery name="cartInfo"> 
            SELECT cart_id, date, contentIDs, message, email, viewed, viewes, city, country_name AS country, region_name AS region
			FROM   SessionCart INNER JOIN Locations ON SessionCart.location_id = Locations.location_id   
            WHERE  0=0
            <cfif userID GT 0>
            AND user_id = #userID#
            </cfif>
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
            <cfif viewed IS 1>
            AND viewed = 1
            </cfif>
            <cfif viewed IS 2>
            AND viewed = 0
            </cfif>
            ORDER BY date DESC, email
        </cfquery>
        
        <cfreturn cartInfo>
        
    </cffunction>
    
    
    
     <!--- Get Cart Customers EMail Info --->
	<cffunction name="getCartCustomerEmails" access="public" returntype="query" output="no">
    	<cfargument name="userID" type="numeric" required="no" default="0">
    	<cfargument name="appID" type="numeric" required="no" default="0">
    
        <cfquery name="cartInfo"> 
            SELECT DISTINCT email
			FROM   SessionCart   
            WHERE  0=0
            <cfif userID GT 0>
            AND user_id = #userID#
            </cfif>
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
        </cfquery>
        
        <cfreturn cartInfo>
        
    </cffunction>
    
    
    
    
    <!--- Get CartID from tinyURL --->
	<cffunction name="getCartIDFromTinyUrl" access="public" returntype="numeric" output="no">
    	<cfargument name="link" type="string" required="yes" default="">
    
        <cfquery name="cartInfo"> 
            SELECT cart_id
			FROM   SessionCart   
            WHERE  tinyurl = '#link#'
        </cfquery>
        
        <cfreturn cartInfo.cart_id>
        
    </cffunction>
    
    
</cfcomponent>