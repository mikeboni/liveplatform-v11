<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this meta tag, Half Life 3 will never be released. -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Thanks for Registering</title>
	
<link rel="stylesheet" type="text/css" href="http://liveplatform.net/css/email.css" />

</head>
 
<body bgcolor="#FFFFFF">
<cfoutput>
<!-- HEADER -->
<table class="head-wrap">
	<tr>
		<td></td>
		<td class="header container" >
				
				<div class="content">
				<table>
					<tr>
						<td width="70"><a href="#webLink.url#" title="#webLink.title#"><img src="#assetPaths.application.icon#" height="66" border="0" /></a></td>
						<td align="left"><h4 class="collapse">#emailInfo.projectName# - Registration</h4></td>
					</tr>
				</table>
				</div>
				
		</td>
		<td></td>
	</tr>
</table><!-- /HEADER -->


<!-- BODY -->
<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" bgcolor="##FFF">

			<div class="content">
			<table>
				<tr>
					<td>
						<h3>Thanks for Registering, </h3>
						<p class="lead">Thank you  <strong>#name#</strong> for registering for <strong>#emailInfo.projectName#</strong>. </p>
					  <p class="lead">Our team will be in touch with you soon.</p>
                      
                      <p class="lead">Please <a href="http://liveplatform.net/confirm.cfm?email=#email#">click here</a> to confirm your registration.</p>
                      <hr size="1"><br />
					  <!--- <p>#emailInfo.message#</p> --->
					  <p>For more information, visit our webiste <a href="#webLink.url#" title="#webLink.title#">#emailInfo.projectName#</a></p>
					  <!-- Callout Panel -->
	
					  <p>If you have questions feel free to contact us at: <a href="mailto:#emailInfo.contactEmail#">#emailInfo.contactEmail#</a>                      
					  <!-- /Callout Panel -->					
												
						<!-- social & contact --><!-- /social & contact -->
						
					</td>
				</tr>
			</table>
			</div><!-- /content -->
									
		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->

</cfoutput>
</body>
</html>