<style type="text/css">

.imgLockedAspect {
	max-width: 100%;
    height: auto;
}

.imgLockedAspectH {
	max-width: auto;
    height: 451px;
}

.bannerHeading {
	padding-top:20px;
	padding-bottom:5px;
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 36px;
	color: #333;
}
.bundleid {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 18px;
	color: #333;
	text-decoration:none;
}
.plainLinkGrey {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #999;
	text-decoration:none;
}
.content {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #333;
	text-decoration:none;
}
.note {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	color: #999;
	text-decoration:none;
}
div.centre
{
  width: 800px;
  display: block;
  margin-left: auto;
  margin-right: auto;
}

.contentLink {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #06C;
	text-decoration:none;
	cursor:pointer;
}
.image-wrapper {
	position: relative;
}
.image-wrapper p {
	position: absolute;
	left: 0;
	top: 0;
	padding: 10px;
	border: 1px solid #FFF;
	width: 218px;
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 18px;
	color: #FFF;
	margin: 10px;
	text-align:center;
}

</style>

<cfinvoke component="Assets" method="getAssetTypes" returnvariable="assetTypes" />

<div class="centre">
<cfif info.assetType NEQ 8>
<cfoutput>
<table width="800" border="0" cellpadding="0" cellspacing="0">
          
    <tr>
            <td>
            <table width="100%" border="0" cellspacing="0">
              <tr>
                <td>
                <cfif displayHeaderCount IS 0>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="100"><img src="#assetPaths.application.icon#" alt="assetPaths.client.name" width="100" height="100" border="0" class="imgLockedAspect" /></td>
                    <td width="78%" align="right" valign="top"><table border="0" cellpadding="0" cellspacing="4">
                      <tr>
                          <td width="32"><cfif isDefined("webLink")><cfset assetIcon = assetTypes['#webLink.type#'].icon>
                            <a href="#webLink.url#" target="_new"><img src="http://liveplatform.net/images/icons/#assetIcon#" width="32" height="32" border="0" /></a></cfif></td>
                          <td><cfif isDefined("webLink")><a href="#webLink.url#" target="_new" class="contentLink" style="padding-left:5px">#webLink.title#</a></cfif></td>
                        </tr>
                    </table></td>
                  </tr>
                </table>
                </cfif>
                </td>
              </tr>
              <cfif displayHeaderCount IS 0>
              <tr>
                      <td class="bannerHeading" style="padding-bottom:5px; padding-top:20px"><span style="color:##000">#ModuleName#</span></td>
              </tr>
              </cfif>
                </table>
</cfoutput>  
	</cfif>                      
                </td>
    </tr>
            <tr>
                <td>
              
         <!--- Asset Types --->
        <cfswitch expression="#info.assetType#">
      
			<!--- Image --->    
            <cfcase value="1">
            	<cfoutput>
				<img src="#info.url#" width="800" height="800" border="1" class="imgLockedAspect" />
                </cfoutput>
            </cfcase>
            
            <!--- Video --->
            <cfcase value="2">
            	<cfoutput>
                <cfmediaplayer name="player_html" source="#info.url#" type="flash" width=800 height=450 align="left" />
                </cfoutput> 
            </cfcase>
            
            <!--- Document --->
            <cfcase value="3"> 
            	<cfoutput>
                <!--- <iframe src="http://docs.google.com/gview?url=#info.url#&embedded=true" style="width:800px; height:1150px;" frameborder="0"></iframe> --->

                <!--- <object data="#info.url#" type="application/pdf" width="800px" height="1150px">
                	<img src="http://www.liveplatform.net/images/icons/document.png" width="32" height="32" border="0" /><br />
               		<a href="#info.url#" class="contentLink">If you are unable to view this PDF, Click this link - #info.details.title#</a>
                </object> --->
                <div style="padding: 5px; margin: 5px;">
                <a href="#info.url#" class="contentLink">
                <img src="#info.thumb#"><p>
                Click this link or the image above to view this PDF
				</div>
                </a>
                </cfoutput>
            </cfcase>
            
            <!--- URL --->
            <cfcase value="4">
                <cflocation url="#info.url#" addtoken="no">
            </cfcase>
            
            <!--- GPS Location --->    
            <cfcase value="5">         
				<cfdump var="#info#">1
            </cfcase>
            
            <!--- 3D Point --->    
            <cfcase value="10">         
				<cfdump var="#info#">2
            </cfcase>
    
            <!--- 3D Model --->
            <cfcase value="6">
				<cfdump var="#info#">3
            </cfcase>
            
            <!--- Panorama --->    
            <cfcase value="8"> 
            	
                
                <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imageInfo">
                    <cfinvokeargument name="appID" value="#info.url#"/>
                </cfinvoke>
                																										
                <style type="text/css">
				#scroll {
					<cfoutput>
					width:800px;height:#info.size.height#px;
					</cfoutput>
					border:1px solid #000;
					overflow:auto;
					white-space:nowrap;
				}
				</style>
 
               <cfif NOT isDefined('info.panourl')>
				
					<cfoutput>
                        <div id="scroll"><img src="#info.url.url#" width="#info.url.size.width#" height="#info.url.size.height#" border="1"></div>
                    </cfoutput>
                    
                    <cfelse>

                    <script src="three.min.js"></script>
                    
                    <style> canvas { margin: 0 auto;max-width: 100%;max-height: 100%;display: block;padding-top: 10px; width: 800px; height: 400px } </style>
                    
                    <div>
                    
                    <cfset thePano = Replace(info.panourl,"http://liveplatform.net/","","all")>
                    
                    <script>
        
						var manualControl = false;
						var longitude = 0;
						var latitude = 0;
						var savedX;
						var savedY;
						var savedLongitude;
						var savedLatitude;
						
						// panoramas background
						<cfoutput>
						var panoramasArray = ["#thePano#"];
						</cfoutput>
						var panoramaNumber = Math.floor(Math.random()*panoramasArray.length);
					
						// setting up the renderer
						renderer = new THREE.WebGLRenderer();
						<!--- renderer.setSize(window.innerWidth, window.innerHeight); --->
						renderer.setSize(800, 500);
						document.body.appendChild(renderer.domElement);
						
						// creating a new scene
						var scene = new THREE.Scene();
						
						// adding a camera
						var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 1000);
						camera.target = new THREE.Vector3(0, 0, 0);
					
						// creation of a big sphere geometry
						var sphere = new THREE.SphereGeometry(100, 100, 40);
						sphere.applyMatrix(new THREE.Matrix4().makeScale(-1, 1, 1));
					
						// creation of the sphere material
						var sphereMaterial = new THREE.MeshBasicMaterial();
						sphereMaterial.map = THREE.ImageUtils.loadTexture(panoramasArray[panoramaNumber])
					
						// geometry + material = mesh (actual object)
						var sphereMesh = new THREE.Mesh(sphere, sphereMaterial);
						scene.add(sphereMesh);
						
						if(isMobile()) {
							// listeners
							document.addEventListener("touchstart", onDocumentMouseDown, false);
							document.addEventListener("touchmove", onDocumentMouseMove, false);
							document.addEventListener("touchend", onDocumentMouseUp, false);
						} else {
							// listeners
							document.addEventListener("mousedown", onDocumentMouseDown, false);
							document.addEventListener("mousemove", onDocumentMouseMove, false);
							document.addEventListener("mouseup", onDocumentMouseUp, false);
						}
						
						
						
						
						   render();
						   
						   function render(){
							
							requestAnimationFrame(render);
							
							if(!manualControl){
								longitude += 0.1;
							}
					
							// limiting latitude from -85 to 85 (cannot point to the sky or under your feet)
								latitude = Math.max(-85, Math.min(85, latitude));
					
							// moving the camera according to current latitude (vertical movement) and longitude (horizontal movement)
							camera.target.x = 500 * Math.sin(THREE.Math.degToRad(90 - latitude)) * Math.cos(THREE.Math.degToRad(longitude));
							camera.target.y = 500 * Math.cos(THREE.Math.degToRad(90 - latitude));
							camera.target.z = 500 * Math.sin(THREE.Math.degToRad(90 - latitude)) * Math.sin(THREE.Math.degToRad(longitude));
							camera.lookAt(camera.target);
					
							// calling again render function
							renderer.render(scene, camera);
							
						}
						
						// when the mouse is pressed, we switch to manual control and save current coordinates
						function onDocumentMouseDown(event){
						
							event.preventDefault();
					
							manualControl = true;
							
							if(isMobile()) {
								savedX = (event.targetTouches[0] ? event.targetTouches[0].pageX : event.changedTouches[event.changedTouches.length-1].pageX)
								savedY = (event.targetTouches[0] ? event.targetTouches[0].pageY : event.changedTouches[event.changedTouches.length-1].pageY)
							} else {
								savedX = event.clientX;
								savedY = event.clientY;
							}
	
							savedLongitude = longitude;
							savedLatitude = latitude;
					
						}
					
						// when the mouse moves, if in manual contro we adjust coordinates
						function onDocumentMouseMove(event){
							
							if(isMobile()) {
								mx = (event.targetTouches[0] ? event.targetTouches[0].pageX : event.changedTouches[event.changedTouches.length-1].pageX)
								my = (event.targetTouches[0] ? event.targetTouches[0].pageY : event.changedTouches[event.changedTouches.length-1].pageY)
							} else {
								mx = event.clientX
								my = event.clientY
							}

							if(manualControl){
								longitude = (savedX - mx) * 0.1 + savedLongitude;
								latitude = (my - savedY) * 0.1 + savedLatitude;
							}
					
						}
					
						// when the mouse is released, we turn manual control off
						function onDocumentMouseUp(event){
					
							manualControl = false;
					
						}
						
						// pressing a key (actually releasing it) changes the texture map
						document.onkeyup = function(event){
						
							panoramaNumber = (panoramaNumber + 1) % panoramasArray.length
							sphereMaterial.map = THREE.ImageUtils.loadTexture(panoramasArray[panoramaNumber])
						
							}
                    
						function isMobile() { 
						 if( navigator.userAgent.match(/Android/i)
						 || navigator.userAgent.match(/webOS/i)
						 || navigator.userAgent.match(/iPhone/i)
						 || navigator.userAgent.match(/iPad/i)
						 || navigator.userAgent.match(/iPod/i)
						 || navigator.userAgent.match(/BlackBerry/i)
						 || navigator.userAgent.match(/Windows Phone/i)
						 ){
							return true;
						  }
						 else {
							return false;
						  }
						}
						
                	</script>
                	</div>
                	
                </cfif>
                
            </cfcase>
            
            <!--- Balcony --->    
            <cfcase value="9">
            
            
            <cfinvoke component="Modules" method="getGroupAssets" returnvariable="balconyAsset">
                <cfinvokeargument name="assetID" value="#info.assetID#">
            </cfinvoke>
            
            <cfset balcony = structNew()>
            
            <cfset balcony = balconyAsset.assets[1]>
            
            <cfloop collection="#balcony#" item="bal">
                <cfset balcony = balcony[bal]>
            </cfloop>
            
            <cfset balcony = {"north":balcony.north.xdpi.url ,"south":balcony.south.xdpi.url , "west":balcony.west.xdpi.url , "east":balcony.east.xdpi.url }>
            
            <cfinvoke component="Assets" method="getAssets" returnvariable="balconyAsset">
                <cfinvokeargument name="assetID" value="#info.assetID#">
            </cfinvoke>
            
            <cfif balconyAsset.title NEQ ''>
                <cfset balName = balconyAsset.title>
            <cfelse>
                <cfset balName = balconyAsset.assetName>
            </cfif>
            
            <cfset info.details.title = balName>
            
            <style type="text/css">
            #scroll {
				<cfoutput>
                width:800px;height:470px;
				</cfoutput>
                border:1px solid #000;
                overflow:auto;
                white-space:nowrap;
				overflow-y: hidden;
            }
            </style>
            
            <cfset h = 450>
            <cfset h = 450>
            <cfset w = 1018>
            
            <cfoutput>
				<div id="scroll">
				  <div class="image-wrapper">
				  <img src="#balcony.north#" class="imgLockedAspectH" height="#h#" border="0">
				  <p class="desc">North View</p>
				  </div>
				  <div class="image-wrapper" style="left:#w#px; top:-#h+1#px">
				  <img src="#balcony.east#" class="imgLockedAspectH" height="#h#" border="0">
				  <p class="desc">East View</p>
				  </div>
				  <div class="image-wrapper" style="left:#w*2#px; top:-#h*2+2#px">
				  <img src="#balcony.south#" class="imgLockedAspectH" height="#h#" border="0">
				  <p class="desc">South View</p>
				  </div>
				  <div class="image-wrapper" style="left:#w*3#px; top:-#h*3+3#px">
				  <img src="#balcony.west#" class="imgLockedAspectH" height="#h#" border="0">
				  <p class="desc">West View</p>
				  </div>
				</div>
            </cfoutput>
                
            </cfcase>
 
            <cfdefaultcase>
    			<!--- Nothing Error --->
            </cfdefaultcase>
    
		</cfswitch>
                
                
                </td>
            </tr>
            <cfif info.details.title NEQ ''>
              <tr>
                <td class="mainheading">
                <cfif info.assetType NEQ 8>
                <cfoutput>
                  <table width="100%" border="0" cellspacing="0">
                   <cfif displayHeaderCount IS 0>
				   <cfif info.details.title NEQ ''>
                    <tr>
                      <td style="padding-top:20px"><span class="bannerHeading" style="padding-bottom:5px; padding-top:20px">#info.details.title#</span></td>
                    </tr>
                    </cfif>
                    </cfif>
                    <cfif displayHeaderCount IS 0>
                    <cfif info.details.subtitle NEQ ''>
                    <tr>
                      <td><span class="bundleid">#info.details.subtitle#</span></td>
                    </tr>
                    </cfif>
                    </cfif>
                    <cfif displayHeaderCount IS 0>
                    <cfif info.details.description NEQ ''>
                    <tr>
                      <td style="padding-top:10px"><span class="content">#info.details.description#</span></td>
                    </tr>
                    </cfif>
                    </cfif>
                    <cfif displayHeaderCount IS 0>
                    <tr>
                      <td style="padding-top:20px; padding-bottom:20px"><span class="note">#config.disclaimer#</span></td>
                    </tr>
                    </cfif>
                  </table>
                  </cfoutput>
				 </cfif>
                </td>
              </tr>
            </cfif>
            
             <cfif info.details.subtitle NEQ ''>
             <cfoutput>
              <tr>
                <td class="contentHilighted">&nbsp;</td>
              </tr>
              </cfoutput>
            </cfif>
              <cfif info.details.description NEQ ''>
              <cfoutput>
              <tr>
                <td class="content">&nbsp;</td>
            </tr>
            </cfoutput>
              </cfif>
              <!--- Content from Content Panel --->
              <cfinvoke component="Assets" method="getPanelAssets" returnvariable="panelAssets">
                    <cfinvokeargument name="assetID" value="#info.assetID#"/>
              </cfinvoke>
              
              <cfif arrayLen(panelAssets) GT 0>
               
                <cfoutput>
				  <tr>
					<td class="content">
					
					<cfloop array="#panelAssets#" index="anAsset">
					
						<cfloop collection="#anAsset#" item="key"></cfloop>
						<cfset theObject = anAsset[key]>
						<cfset theThumb = theObject.thumb.url.xdpi>
						
						
						<cfif theObject.assetType IS 1>
							<cfset largeImage = theObject.image.url.xdpi>
						<cfelseif theObject.assetType IS 2>
							<cfset largeImage = theObject.video.url.xdpi>
						</cfif>
						
						<a href="#largeImage#" style="padding-right:2px" target="_blank"><img src = "#theThumb#" height="80"></a>
					
					</cfloop>
					
					</td>
					
				  </tr>
				</cfoutput>
             
              </cfif>
  </table>

  </div>