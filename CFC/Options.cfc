<cfcomponent>

	<!--- Update AccessLevel --->
	<cffunction name="updateAccessOption" access="remote" returntype="boolean">
    
    	<cfargument name="appID" type="numeric" required="no" default="0">
    	<cfargument name="type" type="string" required="no" default="0">
        <cfargument name="accessLevel" type="numeric" required="no" default="0">
        
        <cfif type IS ''><cfreturn false></cfif>
        
        <cfif appID GT '0'>
        
            <cfquery name="UpdateLink">
                UPDATE AppOptions
                SET 
        
            <cfswitch expression="#type#">
            
            <cfcase value="email">
            	email_accessLevel = '#accessLevel#'
            </cfcase>
            
            <cfcase value="facebook">
            	facebook_accessLevel = '#accessLevel#'
            </cfcase>
            
            <cfcase value="twitter">
            	twitter_accessLevel = #accessLevel#
            </cfcase>
            
            <cfcase value="pintrest">
            	pintrest_accessLevel = #accessLevel#
            </cfcase>
            
            <cfcase value="instagram">
            	instagram_accessLevel = #accessLevel#
            </cfcase>
            
            <cfcase value="addressBook">
            	addressBook_accessLevel = #accessLevel#
            </cfcase>
            
            </cfswitch>
            
                WHERE	app_id = #appID#
            </cfquery>

        <cfelse>
        	<cfreturn false>
        </cfif>
    
        <cfreturn true>
        
	</cffunction>
    

	<!--- Delete SupportLink --->
	<cffunction name="deleteSupportLink" access="remote" returntype="boolean">
    	<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="linkID" type="numeric" required="no" default="0">
        
        <cfif linkID GT 0 AND appID GT 0>
        
           	<!---Delete Link--->
            <cfquery name="DeleteLink">
                DELETE FROM Support
                WHERE  support_id = #linkID# AND app_id = #appID#
            </cfquery>
            
        	<cfreturn true>
       
       <cfelse>
       		<cfreturn false>
       </cfif>
        
	</cffunction>
    
    
    
    <!--- Update SupportLink --->
	<cffunction name="updateSupportLink" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="linkID" type="numeric" required="no" default="0">
        <cfargument name="urlLink" type="string" required="yes" default="0">
        <cfargument name="urlName" type="string" required="yes" default="0">
        
        <cfargument name="theLevel" type="numeric" required="no" default="0">
        
        <cfinvoke  component="Users" method="getAccessLevel" returnvariable="accessID">
          <cfinvokeargument name="accessLevel" value="#theLevel#"/>
        </cfinvoke>
        
        <cfset access_id = accessID.access_id>
        
        <!---Update Link--->
		 <cfif urlName NEQ '' OR urlName NEQ ''>
                 
            <cfquery name="UpdateLink">
                UPDATE Support
                SET url = '#urlLink#', urlName = '#urlName#', access_id = #access_id#
                WHERE	support_id = #linkID#
            </cfquery>

         </cfif>
        
         <cfreturn true>
        
	</cffunction>
    
    
    <!--- New SupportLink --->
	<cffunction name="newSupportLink" access="remote" returntype="boolean">
        
		<cfargument name="appID" type="numeric" required="yes" default="0">
        <cfargument name="urlLink" type="string" required="yes" default="0">
        <cfargument name="urlName" type="string" required="yes" default="0">
        <cfargument name="type" type="numeric" required="no" default="0">
        
        <cfargument name="theLevel" type="numeric" required="no" default="0">
        
        <cfinvoke  component="Users" method="getAccessLevel" returnvariable="accessID">
          <cfinvokeargument name="accessLevel" value="#theLevel#"/>
        </cfinvoke>
 
        <cfset access_id = accessID.access_id>
 		
		<!---New Link--->
        <cfif urlName NEQ '' OR urlName NEQ ''>
            
            <cfquery name="newLink">
                 INSERT INTO Support (url, urlName, app_id, type, access_id)
                 VALUES ('#urlLink#', '#urlName#', #appID#, #type#, #access_id#) 
            </cfquery>  

            <cfreturn true>
            
        <cfelse>
            <cfreturn false>
        </cfif>
        
	</cffunction>
    


	<!--- Get App Options --->
	<cffunction name="getAppOptions" access="remote" returntype="struct">
		<cfargument name="appID" type="numeric" required="yes">
        
        <cfset theOptions = structNew()>
        
        <!--- Check if appOptions Exists --->
		<cfinvoke component="Options" method="appOptionsExist" returnvariable="OptionsExists">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
        
        <cfif OptionsExists>
        	<!--- Get Options --->
            <cfquery name="options">
                SELECT      userReports, userSettings, basicReports, advancedReports, sendEmail, facebook, twitter, pintrest, instagram, vuforiaLicense, registration, verification, addressBook, 
                			email_accessLevel, facebook_accessLevel, twitter_accessLevel, pintrest_accessLevel, instagram_accessLevel, addressBook_accessLevel, customerRegistration
                FROM        AppOptions
                WHERE		app_id = #appID#
            </cfquery>
            
            <cfinvoke component="Misc" method="QueryToStruct" returnvariable="theOptions">
                <cfinvokeargument name="query" value="#options#"/>
            </cfinvoke>
            
            <cfif theOptions.customerRegistration IS ''><cfset theOptions.customerRegistration = 0></cfif>
		
        </cfif>
        
		<cfreturn theOptions>
        
	</cffunction>
    
    
    <!--- Set App Options --->
    <cffunction name="setAppOptions" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="options" type="struct" required="yes">
        
        <!--- Check if appOptions Exists --->
		<cfinvoke component="Options" method="appOptionsExist" returnvariable="OptionsExists">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
     
        <cfset success = false>
        
        <cfif options.addressBook IS ""><cfset options.addressBook = 0></cfif>
        <cfif options.verification IS ""><cfset options.verification = 0></cfif>
        
        <cfif OptionsExists>
        
			<!--- Update DB Options --->
            <cfquery name="options"> 
                UPDATE AppOptions
                SET userReports = #options.userReports#, userSettings = #options.userSettings#,
                    basicReports = #options.basicReports#, advancedReports = #options.advancedReports#, 
                    sendEmail = #options.sendEmail#, facebook = #options.facebook#, twitter = #options.twitter#, pintrest = #options.pintrest#, instagram = #options.instagram#, registration = #options.registration#, verification = #options.verification#, addressBook = #options.addressBook#
                WHERE app_id = #appID#  
            </cfquery>
            
            <cfset success = true>

        <cfelse>
			<cfset success = false>
        </cfif>
        
        <cfreturn success>

	</cffunction>
    
    
    
    <!--- Create App Options --->
    <cffunction name="createAppOptions" access="public" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        
        <!--- Check if AppID Exists --->
        <cfquery name="apps">
            SELECT      app_id
            FROM        Applications
            WHERE		app_id = #appID#
        </cfquery>
        
        <!--- If App Exists --->
        <cfif apps.recordCount GT 0>
        
			<!--- Create App Options for AppID --->
            <cfquery name="options"> 
                INSERT INTO AppOptions (app_id, userReports, userSettings, basicReports, advancedReports, sendEmail, facebook, twitter, pintrest, instagram)
                VALUES (#appID#, 0, 0, 0, 0, 0 , 0, 0, 0, 0)
            </cfquery>
        	
            <cfreturn true>
            
        <cfelse>
        	<cfreturn false>
        </cfif>
   
	</cffunction>
    
    
    <!--- App Options Exists? --->
    <cffunction name="appOptionsExist" access="public" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        
        <cfset success = false>
        
		<cfquery name="options">
            SELECT      option_id
            FROM        AppOptions
            WHERE		app_id = #appID#
        </cfquery>
        
        <cfif options.recordCount GT 0>
			<cfset success = true>
        <cfelse>

			<!--- Create Default Options if not exist --->
            <cfinvoke component="Options" method="createAppOptions" returnvariable="success">
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
            
        </cfif>
        
        <cfreturn success>
        
	</cffunction>
  
  
  	
    <!--- Update Social Message --->
    <cffunction name="getSocialNetworks" access="remote" returntype="array">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfset success = false>
        
		<cfquery name="socialOptions">
            SELECT		Social.message, SocialNetworks.networkName
            FROM        Social LEFT OUTER JOIN SocialNetworks ON Social.socialType = SocialNetworks.id
            WHERE		app_id = #appID# AND group_id = #groupID#
        </cfquery>
        
        <cfset socialNetworks = arrayNew(1)>
        
        <cfif socialOptions.recordCount GT 0>

            <cfoutput query="socialOptions">
            	<cfset arrayAppend(socialNetworks,{"message":message,"networkName":networkName})>
			</cfoutput>
            
        </cfif>

        <cfreturn socialNetworks>
        
	</cffunction> 
  
  
  
    <!--- Update Social Message --->
    <cffunction name="updateSocialNetwork" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="socialInfo" type="struct" required="yes">
        
        <cfset success = false>
        
        <!--- Check if Message Exists --->
        <cfinvoke component="Options" method="socialNetworkExists" returnvariable="socialMessageExists">
            <cfinvokeargument name="socialType" value="#socialInfo.type#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="groupID" value="#socialInfo.groupID#"/>
        </cfinvoke>
        
        <cfset message = trim(socialInfo.message)>
        
        <cfif socialMessageExists>
        
            <cfquery name="social">
                UPDATE Social
                <cfif message NEQ ''>
                	SET message = '#socialInfo.message#'
                <cfelse>
                	SET message = NULL
                </cfif>
                WHERE app_id = #appID# AND group_id = #socialInfo.groupID# AND socialType = #socialInfo.type#
            </cfquery>
        	
            <cfset success = true>

        </cfif>
        
        <cfreturn success>
        
	</cffunction>  
    
    
    
    <!--- Update Social Message --->
    <cffunction name="getSocialNetworkType" access="public" returntype="numeric">
		<cfargument name="socialType" type="numeric" required="yes">
        
        <cfquery name="networks">
            SELECT		social_id
            FROM        SocialNetworks
            WHERE		id = #socialType#
        </cfquery>
        
        <cfreturn networks.social_id>
        
	</cffunction>  
    
    
    
    <!--- Social Message Exists --->
    <cffunction name="socialNetworkExists" access="public" returntype="boolean">
		<cfargument name="socialType" type="numeric" required="yes">
        <cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfquery name="networks">
            SELECT		social_id
            FROM        Social
            WHERE		socialType = #socialType# AND app_id = #appID# AND group_id = #groupID#
        </cfquery>
        
        <cfset success = false>
        
        <cfif networks.recordCount GT 0>
        	<cfset success = true>
        <cfelse>
        	
		<!--- Create SocialMessage --->
            <cfquery name="options"> 
                INSERT INTO Social (socialType, app_id, group_id, message)
                VALUES (#socialType#, #appID#, #groupID#, '')
            </cfquery>
            
            <cfset success = true>
            
        </cfif>

        <cfreturn success>
        
	</cffunction> 
    
    
    <!--- Create Social Options --->
    <cffunction name="createAppSocialNetworks" access="public" returntype="boolean">
        <cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfquery name="allNetworks">
            SELECT		social_id
            FROM        SocialNetworks
        </cfquery>
        
        <cfset success = false>
        
        <cfloop query="allNetworks">
        
			<!--- Create SocialMessage --->
            <cfquery name="socialNetworks"> 
                INSERT INTO Social (socialType, app_id, group_id, message)
                VALUES (#social_id#, #appID#, #groupID#, '')
            </cfquery>
            
            <cfset success = true>
            
        </cfloop>

        <cfreturn success>
        
	</cffunction> 
    
    
    
    
    
    <!--- Get Social Message --->
    <cffunction name="getSocialMessage" access="public" returntype="query">
        <cfargument name="appID" type="numeric" required="yes" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">

		<!--- Get Options --->
 		<cfinvoke component="Options" method="getAppOptions" returnvariable="theOptions">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
 		
        <cfset socialNetworks = ["facebook","twitter","instagram","pintrest"]>
        
        <cfset supportedSocial = arrayNew(1)>
        
        <cfloop collection="#theOptions#" item="sn">
        
			<cfif ArrayFind(socialNetworks,sn)>
            	<cfif theOptions[sn] IS 1>
            		<cfset arrayAppend(supportedSocial,sn)>
                </cfif>
            </cfif>
        </cfloop> 
        
        <!--- All Networks --->
        <cfquery name="allNetworks">
            SELECT       lower(SocialNetworks.networkName)AS networkName, Social.message, Social.group_id, Social.socialType
			FROM         Social LEFT OUTER JOIN SocialNetworks ON Social.socialType = SocialNetworks.id
            WHERE		 app_id = #appID# AND message IS NOT NULL
        </cfquery>
        

        <!--- Network Messages --->
        <cfquery dbtype="query" name="allNetworkMessages">
        	SELECT		message, LOWER(networkName), socialType
            FROM        allNetworks
            
            <cfif groupID GT 0>
            
                WHERE
                (
                <cfloop index="z" from="1" to="#arrayLen(supportedSocial)#">
                    networkName = '#supportedSocial[z]#'
                    
                    <cfif z GT arrayLen(supportedSocial)-1>
                        <cfbreak>
                    <cfelse>
                        OR
                    </cfif>
                </cfloop>
                ) 
            	AND (group_id = #groupID#)
                
            </cfif>
            
        </cfquery>
        
        <!--- Find Missing --->
        <cfloop index="z" from="1" to="#arrayLen(supportedSocial)#">
            
            <cfquery dbtype="query" name="foundNetwork1">
                SELECT		message, LOWER(networkName), socialType
                FROM        allNetworks
            	WHERE		networkName = '#supportedSocial[z]#'
            </cfquery>
            
            <cfquery dbtype="query" name="foundNetwork2">
                SELECT		message, LOWER(networkName), socialType
                FROM        allNetworkMessages
            	WHERE		networkName = '#supportedSocial[z]#'
            </cfquery>

			<!--- Add Missing --->
            <cfif foundNetwork2.recordCount IS 0>
          		
                <cfif foundNetwork1.message NEQ ''>
					 <cfset temp = QueryAddRow(allNetworkMessages)>
                     <cfset temp = QuerySetCell(allNetworkMessages, "message", "#foundNetwork1.message#")>
                     <cfset temp = QuerySetCell(allNetworkMessages, "networkName", "#supportedSocial[z]#")>
			    </cfif>
                
            </cfif>
            
        </cfloop>
        
        <cfreturn allNetworkMessages>
        
	</cffunction> 
    
    
    
    
    <!--- Update Vuforia License --->
    <cffunction name="updateVuforiaLicense" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="license" type="string" required="yes">
        
        <cfset success = false>
        
		<cfquery name="UpdateLink">
            UPDATE AppOptions
            SET vuforiaLicense = <cfif license IS ''>NULL<cfelse>'#license#'</cfif>
            WHERE	app_id = #appID#
        </cfquery>

        <cfreturn true>
        
	</cffunction> 
    
    
    
    <!--- Verify EMail Account --->
    <cffunction name="verifyEmailAccount" access="remote" returntype="struct">
		<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="email" type="string" required="yes" default="">
        
        <cfset result = {'accountExists':0, 'passwordExists':0}>
        
        <cfif appID GT 0 AND email NEQ ''>
        	<!--- verifiy account --->
            <cfquery name="theUser">
                SELECT		password, name, company, email
                FROM        Users
            	WHERE		app_id = #appID# AND email = '#trim(email)#'
            </cfquery>
          
            <cfif theUser.recordCount GT 0>
            	
                <cfset result.accountExists = 1>
                
                <cfif len(theUser.password) GT 0>
					<!--- pass exists --->
                    <cfset result.passwordExists = 1>
                <cfelse>
					<!--- no pass --->
                    <cfset result.passwordExists = 0>
                    <cfset structAppend(result,{'user':{'name':'#theUser.name#', 'company':'#theUser.company#', 'email': '#theUser.email#'}})>
                </cfif>
            
            <cfelse>
            	<cfset result.accountExists = 0>
            	<cfset structDelete(result,'passwordExists')>   
            </cfif>
            
        <cfelse>
        	<cfset result.accountExists = 0>
            <cfset structDelete(result,'passwordExists')>
        </cfif>

        <cfreturn result>
        
	</cffunction> 
    
    
    <!--- Set Account Info from Registration --->
    <cffunction name="setUserInfo" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="userInfo" type="struct" required="yes">
        
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <!--- validate --->
 		<cfquery name="theUser">
            SELECT		user_id
            FROM        Users
            WHERE		app_id = #appID# AND email = '#trim(userInfo.email)#'
        </cfquery>
		
        <!--- validate user --->
        <cfif theUser.recordCount GT 0>
        
			<cfset userID = theUser.user_id>
            
            <!--- update user info --->
            <cfquery name="UpdatUser">
                UPDATE	Users
                SET		company = '#trim(userInfo.company)#', password = '#trim(userInfo.password)#', created = #curDate#, modified = #curDate#, activated = 1, active = 1
                WHERE	user_id = #userID#
            </cfquery>
            
            <!--- send thanks email --->
        
        <cfelse>
        	<cfreturn false>
        </cfif>
                
        <cfreturn true>
                
	</cffunction> 
    
    
    
    <!--- Resend Account Info from Registration --->
    <cffunction name="resendUserAccountInfo" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="userEMail" type="string" required="yes" default="">
    
    	<!--- Get CLient ID from AppID --->
        <cfinvoke  component="Clients" method="getClientIDFromAppID" returnvariable="clientID">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>

		<!--- validate --->
 		<cfquery name="userInfo">
            SELECT		user_id, password, name, company, email
            FROM        Users
            WHERE		app_id = #appID# AND email = '#trim(userEMail)#'
        </cfquery>
		
        <!--- validate user --->
        <cfif userInfo.recordCount GT 0>
        
		  <cfset userID = userInfo.user_id>
      
          <cfinvoke component="Apps" method="getClientApps" returnvariable="apps">
              <cfinvokeargument name="clientID" value="#clientID#"/>
              <cfinvokeargument name="appID" value="#appID#"/>
          </cfinvoke>
          
          <cfinvoke  component="Clients" method="getClientInfo" returnvariable="info">
            <cfinvokeargument name="clientID" value="#clientID#"/>
          </cfinvoke>
          
          <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
              <cfinvokeargument name="clientID" value="#clientID#"/>
              <cfinvokeargument name="server" value="yes"/>
          </cfinvoke>
          
          <cfinvoke component="Users" method="getUserInfo" returnvariable="userInfo">
              <cfinvokeargument name="userID" value="#userID#"/>
          </cfinvoke>
          
          <cfset name = trim(userInfo.name)>
          <cfset email = trim(userInfo.email)>
          <cfset password = trim(userInfo.password)>
          
		  <!--- Send Email to User that they are Approved --->
          <cfmail server="cudaout.media3.net"
                  username="support@wavecoders.ca"
                  from="#assetPaths.client.name# Registration <#info.support#>"
                  to="#name# <#email#>"
                  subject="User Registration - #assetPaths.client.name#"
                  replyto="Support <#info.support#>"
                  type="HTML"> 
          
                  <!--- HTML RegUser --->
                  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            		<html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <title>Registration Information</title>
                    <style type="text/css">
                    .content1 {	font-family: Tahoma, Geneva, sans-serif;
                        font-size: 14px;
                        color: ##333;
                        text-decoration:none;
                    }
                    .content1 {	font-family: Tahoma, Geneva, sans-serif;
                        font-size: 14px;
                        color: ##333;
                        text-decoration:none;
                    }
                    </style>
                    </head>
                    <style type="text/css">
                    .bannerHeading {
                        padding-top:20px;
                        padding-bottom:5px;
                        font-family: Tahoma, Geneva, sans-serif;
                        font-size: 36px;
                        color: ##333;
                    }
                    .bundleid {
                        font-family: Tahoma, Geneva, sans-serif;
                        font-size: 18px;
                        color: ##333;
                        text-decoration:none;
                    }
                    .category {
                        font-family: Tahoma, Geneva, sans-serif;
                        font-size: 16px;
                        color: ##CCC;
                        text-decoration:none;
                        background-color: ##666;
                        padding-bottom: 6px;
                        padding-left: 12px;
                        height: 26px;
                    }
                    
                    .plainLinkGrey {
                        font-family: Tahoma, Geneva, sans-serif;
                        font-size: 14px;
                        color: ##999;
                        text-decoration:none;
                    }
                    .content {
                        font-family: Tahoma, Geneva, sans-serif;
                        font-size: 14px;
                        color: ##333;
                        text-decoration:none;
                    }
                    .note {
                        font-family: Tahoma, Geneva, sans-serif;
                        font-size: 12px;
                        color: ##999;
                        text-decoration:none;
                    }
                    div.centre
                    {
                      width: 760px;
                      display: block;
                      margin-left: auto;
                      margin-right: auto;
                    }
                    
                    .contentLink {
                        font-family: Tahoma, Geneva, sans-serif;
                        font-size: 14px;
                        color: ##06C;
                        text-decoration:none;
                        cursor:pointer;
                    }
                    
                    .formfieldcontent {
                        height: 32px;
                        font-family: Tahoma, Geneva, sans-serif;
                        font-size: 12px;
                        border: thin solid ##999;
                        padding-left: 6px;
                    }
                    .functionButton {
                        height: 44px;
                        width: 44px;
                    }
                    </style>                
                    <body>
                    <table width="800" height="448" border="0" cellpadding="0" cellspacing="10" bgcolor="##EEE">
                      <tr>
                        <td height="18" colspan="2">&nbsp;</td>
                      </tr>
                      <tr>
                        <td width="130" rowspan="2" align="left" valign="bottom">
                        <cfoutput><img src="#assetPaths.client.icon#" width="100" height="100" border="0" style="padding-right:20px; padding-left:10px" /></cfoutput></td>
                        <td height="41" align="right" valign="top" class="mainHeading"><cfoutput><table border="0" cellpadding="0" cellspacing="5">
                          <tr>
                            <td><a href="#info.support#?subject=Registration Support" class="contentLink" target="_new"><img src="http://www.liveplatform.net/register/images/help.png" alt="Help" width="31" height="31" border="0" /></a></td>
                            <td width="150"><a href="mailto:#info.support#?subject=Registration Support" class="contentLink" target="_new">Registration Support</a></td>
                            </tr>
                          </table></cfoutput>
                          
                        </td>
                      </tr>
                      <tr>
                        <td width="605" height="73" valign="bottom" class="bannerHeading">Congratulations!</td>
                      </tr>
                      <tr>
                        <td height="238" align="left" valign="top"><p>&nbsp;</p></td>
                        <td align="left" valign="top">
                          <p class="content">Your account has been approved for access to  &quot;<cfoutput>#apps.appName#</cfoutput>&quot; App</p>
                          <p class="content">Please keep the following information:</p>
                          
                          <cfoutput><table width="500" border="0" cellspacing="10">
                            <tr>
                              <td width="50" align="right" class="content">Name</td>
                              <td><span class="content">#name#</span></td>
                            </tr>
                            <tr>
                              <td align="right" class="content">Email</td>
                              <td><span class="content">#email#</span></td>
                            </tr>
                            <cfif isDefined('password')>
                            <tr>
                              <td align="right" class="content">Pass</td>
                              <td><span class="content1">#password#</span></td>
                            </tr>
                            </cfif>
                          </table>
                          </cfoutput>
                          
                          <p class="content">To download the App, please visit the following URL on your tablet and select the appropriate version for your device.</p>
                          
                          <!--- Get App Links --->
                          <cfinvoke component="Apps" method="getSupportLinks" returnvariable="supportLinks">
                              <cfinvokeargument name="appID" value="#appID#"/>
                          </cfinvoke>
                          
                          <cfif supportLinks.recordCount GT 0>
                          
                              <table width="100%" border="0" cellspacing="5">
                              <cfloop query="supportLinks">
                              <cfif supportLinks.url NEQ '' OR supportLinks.url NEQ 'Registration'>
                                <tr>
                                  <td class="content"><a href="#supportLinks.url#" class="contentLink">#supportLinks.urlName#</a></td>
                                </tr>
                                </cfif>
                              </cfloop>
                              </table>
                              
                          </cfif>
                          
                          <p class="content">To sign into your account in the app, select Sign in (top right) of the main screen next to the setting icon.</p>
                          
                          
                          </td>
                      </tr>                  
                      <tr>
                        <td height="18" valign="bottom" class="message"></td>
                        <td height="50" valign="bottom" class="message">
                        <cfoutput>
                        <p class="content">For any questions, please contact support at <a href="mailto:#info.support#?subject=Registration Support" class="contentLink" target="_new">#info.support#</a></p>
                        </cfoutput>
                        </td>
                      </tr>
                    </table>  
            		</body>
                    </html>
          
           </cfmail>
            
           <cfelse>
                <cfreturn false>
           </cfif>
        
        	<cfreturn true>
        
        </cffunction>
        
        
    <!--- Customer Register for a Project --->
    <cffunction name="projectRegistration" access="remote" returntype="boolean">
    	<cfargument name="appID" type="numeric" required="no" default="0">
		<cfargument name="register" type="numeric" required="no" default="0">
        
        <cfif appID GT 0>
        
            <cfquery name="UpdateRegisterOption">
                UPDATE	AppOptions
                SET		customerRegistration = #register#
                WHERE	app_id = #appID#
            </cfquery>
        	
            <cfreturn true>
            
        </cfif>
        
        <cfreturn false>
        
	</cffunction>  
        
 
 
     <!--- Register for an Account --->
    <cffunction name="requestAccount" access="remote" returntype="boolean">
		<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="userInfo" type="struct" required="yes"> 
         
         <!--- Get CLient ID from AppID --->
        <cfinvoke  component="Clients" method="getClientIDFromAppID" returnvariable="clientID">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
         
         <!--- email app assets --->
         <cfinvoke component="Apps" method="getClientApps" returnvariable="apps">
              <cfinvokeargument name="clientID" value="#clientID#"/>
              <cfinvokeargument name="appID" value="#appID#"/>
          </cfinvoke>
          
          <cfinvoke  component="Clients" method="getClientInfo" returnvariable="info">
            <cfinvokeargument name="clientID" value="#clientID#"/>
          </cfinvoke>
          
          <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
              <cfinvokeargument name="clientID" value="#clientID#"/>
              <cfinvokeargument name="server" value="yes"/>
          </cfinvoke>

		  <!--- Send Email to User Thanks --->
          <cfmail server="cudaout.media3.net"
                  username="support@wavecoders.ca"
                  from="#assetPaths.client.name# Registration <#info.contactEmail#>"
                  to="#userInfo.name# <#userInfo.email#>"
                  subject="#apps.appName# Registration - #assetPaths.client.name#"
                  replyto="Registration Support <#info.contactEmail#>"
                  type="HTML"> 
                  
           <!--- HTML --->
           <link href="regStyles.css" rel="stylesheet" type="text/css" />
           <div class="centre" style="width:800px">
            <cfoutput>
            <table width="800" height="900" border="0" cellpadding="0" cellspacing="10" bgcolor="##EEE">
              <tr>
                <td height="18" colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td width="100" rowspan="2" align="left" valign="bottom"><a href="http://www.11wellesleycondo.ca"><img src="#assetPaths.client.icon#" width="100" height="100" border="0" class="imgLockedAspect" style="padding-right:20px; padding-left:10px" /></a></td>
                <td width="1" rowspan="6" align="right" valign="top" class="mainHeading">&nbsp;</td>
                <td width="1399" height="41" align="right" valign="top" class="mainHeading"><table border="0" cellpadding="0" cellspacing="5">
                  <tr>
                    <td><a href="#info.support#?subject=Registration Support" class="contentLink" target="_new"><img src="http://www.liveplatform.net/register/images/help.png" alt="Help" width="31" height="31" border="0" /></a></td>
                    <td width="150"><a href="mailto:#info.contactEmail#?subject=Registration Support" class="contentLink" target="_new">Registration Support</a></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td height="73" valign="bottom" class="bannerHeading">Registration Submitted</td>
              </tr>
              <tr>
                <td height="57" align="left" valign="top"><p>&nbsp;</p></td>
                <td align="left" valign="top">
                  <p class="content">Your information has been submitted and sent to our registartion process.</p>
                  <p class="content">You have been sent a confirmation email at the following address:</p>
                  <p class="message-bold"><span class="contentLink">#userInfo.email#</span></p>
                  <p class="content"><span class="message">Your registration will be reviewed and must be approved for user access.</span></p>
                  <p class="content">Please allow up to 24 hours for this process.</p>
                  <p class="content">Thanks<br />
                  </p></td>
              </tr>
              <tr>
                <td height="18" valign="bottom" class="message"></td>
                <td valign="middle" class="message">&nbsp;</td>
              </tr>
              <tr>
                <td height="19" valign="bottom" class="message"></td>
                <td valign="middle" class="message"><span class="content">For any questions, please contact support at <a href="mailto:#info.contactEmail#?subject=Registration Support" class="contentLink" target="_new">Registration Support</a></span></td>
              </tr>
              <tr>
                <td valign="bottom" class="message"></td>
                <td valign="middle" class="message">&nbsp;</td>
              </tr>
            </table>
            </cfoutput>
            </div>       
           </cfmail>
         
          <!--- Send Email to Admin --->
          <cfmail server="cudaout.media3.net"
                  username="support@wavecoders.ca"
                  from="#assetPaths.client.name# Registration <#info.contactEmail#>"
                  to="Registration Support <#info.contactEmail#>"
                  subject="User Request an Account - #assetPaths.client.name#"
                  replyto="#userInfo.name# <#userInfo.email#>"
                  type="HTML"> 
                  
          <!--- HTML --->
          <link href="regStyles.css" rel="stylesheet" type="text/css" />
			<style type="text/css">
            .imgLockedAspect {
                max-width: 100%;
                height: auto;
            }
            
            .imgLockedAspectHeight {
                max-height: 100%;
                width: auto;
            }
            
            .bannerHeading {
                padding-top:20px;
                padding-bottom:5px;
                font-family: Tahoma, Geneva, sans-serif;
                font-size: 36px;
                color: ##333;
            }
            .bundleid {
                font-family: Tahoma, Geneva, sans-serif;
                font-size: 18px;
                color: ##333;
                text-decoration:none;
            }
            .category {
                font-family: Tahoma, Geneva, sans-serif;
                font-size: 16px;
                color: ##CCC;
                text-decoration:none;
                background-color: ##666;
                padding-bottom: 6px;
                padding-left: 12px;
                height: 26px;
            }
            
            .plainLinkGrey {
                font-family: Tahoma, Geneva, sans-serif;
                font-size: 14px;
                color: ##999;
                text-decoration:none;
            }
            .content {
                font-family: Tahoma, Geneva, sans-serif;
                font-size: 14px;
                color: ##333;
                text-decoration:none;
            }
            .note {
                font-family: Tahoma, Geneva, sans-serif;
                font-size: 12px;
                color: ##999;
                text-decoration:none;
            }
            div.centre
            {
              width: 760px;
              display: block;
              margin-left: auto;
              margin-right: auto;
            }
            
            .contentLink {
                font-family: Tahoma, Geneva, sans-serif;
                font-size: 14px;
                color: ##06C;
                text-decoration:none;
                cursor:pointer;
            }
            
            .formfieldcontent {
                height: 32px;
                font-family: Tahoma, Geneva, sans-serif;
                font-size: 12px;
                border: thin solid ##999;
                padding-left: 6px;
            }
            .functionButton {
                height: 44px;
                width: 44px;
            }
            </style>
            <div class="centre" style="width:800px">
            <table width="800" height="900" border="0" cellpadding="0" cellspacing="10" bgcolor="##EEE">
              <tr>
                <td height="18" colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td width="100" rowspan="2" align="center" valign="bottom">
                <cfoutput>
                <img src="#assetPaths.client.icon#" width="100" height="100" border="0" class="imgLockedAspect" style="padding-right:20px; padding-left:10px" />
                </cfoutput>
                </td>
                <td align="right" valign="top" class="mainHeading">&nbsp;</td>
                <td height="41" align="right" valign="top" class="mainHeading"><table border="0" cellpadding="0" cellspacing="5">
                  <tr>
                    <td><a href="#info.support#?subject=Registration Support" class="contentLink" target="_new"><img src="http://www.liveplatform.net/register/images/help.png" alt="Help" width="31" height="31" border="0" /></a></td>
                    <td width="150"><a href="mailto:#info.support#?subject=Registration Support" class="contentLink" target="_new">Registration Support</a></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td width="1" valign="bottom" class="bannerHeading">&nbsp;</td>
                <td width="1339" height="18" valign="bottom" class="bannerHeading">Request for Registration!</td>
              </tr>
              <tr>
                <td height="238" align="left" valign="top"><p>&nbsp;</p></td>
                <td align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top">
                <cfoutput>
                  <p class="content">You have received this email from #userInfo.name# in which they are requesting a User Account to be created for the #assetPaths.client.name# application.</p>
                  <p class="content">Please click the following if you approve this user to access features that are only available forselected users.</p>
                  
                  <table width="500" border="0" cellspacing="10">
                    <tr>
                      <td width="50" align="right" class="content">Name</td>
                      <td><span class="content">#userInfo.name#</span></td>
                    </tr>
                    <tr>
                      <td align="right" class="content">Email</td>
                      <td><span class="content">#userInfo.email#</span></td>
                    </tr>
                    <tr>
                      <td align="right" class="content">RECO</td>
                      <td><span class="content">#userInfo.reco#</span></td>
                    </tr>
                    <tr>
                      <td align="right" class="content">Mobile</td>
                      <td><span class="content">#userInfo.phone#</span></td>
                    </tr>
                  </table>
                  </cfoutput>
                  
                  <!--- Apps --->
                  <table width="100%" border="0" cellspacing="10">
                  <cfloop query="apps">
                    <tr>
                      <td>
                  <p class="content"><a href="http://www.liveplatform.net/approveRegistration.cfm?name=#userInfo.name#&email=#userInfo.email#&reco=#userInfo.reco#&phone=#userInfo.phone#&clientID=#clientID#&appID=#apps.app_id#" class="contentLink">I authorize #userInfo.name# to access exclusive features the &quot;#apps.appName#&quot; App</a></p>
                  </td>
                  </tr>
                  </cfloop>
                  </table>
                  <!---  --->
                  
                  <cfoutput>
                  <p class="content">Please notify the user if they will NOT be approved by selecting the <br />
                  following email address <a href="mailto:#userInfo.email#?subject=Registration has not been approved" class="contentLink">#userInfo.email#</a></p>
                  </cfoutput>
                  </td>
              </tr>
              <tr>
                <td height="18" valign="bottom" class="message"></td>
                <td valign="middle" class="message">&nbsp;</td>
                <td valign="middle" class="message">
                <cfoutput>
                <p class="content">For any questions, please contact support at <a href="mailto:#info.support#?subject=Registration Support" class="contentLink" target="_new">#info.support#</a></p>
                </cfoutput>
                </td>
              </tr>
            </table> 
            </div>     
           </cfmail>
        
        <cfreturn true>
       
    </cffunction> 
    
</cfcomponent>