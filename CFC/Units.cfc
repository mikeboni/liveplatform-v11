<cfcomponent>


 <!--- Change state of asetGroup (model assign to unit) --->
 	<cffunction name="setAssetActiveState" access="remote" returntype="struct">
    	<cfargument name="assetID" type="numeric" required="yes">
    	<cfargument name="state" type="numeric" required="no" default="0">
        
        <!--- update unit active --->
        <cfquery name="assetGroup">
          UPDATE Groups
          SET active = #state#
          WHERE	 group_id = #assetID#
        </cfquery>
        
        <cfset theResult = {"assetID":assetID, "state":state}>
        
        <cfreturn theResult>
        
    </cffunction>
    

<!--- create group --->
	<cffunction name="createGroup" access="remote" returntype="boolean">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
		<cfargument name="subgroupID" type="numeric" required="no" default="-1">
        <cfargument name="groupName" type="string" required="no" default="">
        
        <cfset groupID = 0>
        
        <cfif appID GT 0>
        
                <!--- New Group --->
                <cfinvoke component="Units" method="addGroup" returnvariable="groupID">
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
                    <cfinvokeargument name="name" value="#groupName#"/>
                </cfinvoke>
        
        </cfif>
        
		<cfreturn groupID>
        
	</cffunction>
    
    
     <!--- Add Group --->
 	<cffunction name="addGroup" access="public" returntype="numeric">
    	<cfargument name="appID" type="numeric" required="yes">
    	<cfargument name="subgroupID" type="numeric" required="yes">
        <cfargument name="name" type="string" required="no" default="untitled">
        
        <cfset groupID = 0>
        
		<cfquery name="newGroup"> 
            INSERT INTO unitGroupAssets (name, app_id, subgroup_id)			
            VALUES (
            '#name#', 
            #appID# ,
            #subgroupID#
            )
            SELECT @@IDENTITY AS groupID
        </cfquery>

		<cfset groupID = newGroup.groupID>
		
        <cfreturn groupID>
        
    </cffunction>
    
    
    <!--- Group Active State --->
  	<cffunction name="setGroupActiveState" access="remote" returntype="struct" output="no">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfset groupState = 0>
        <cfset result = {"groupID":groupID,"active":groupState}>
        
        <cfif groupID GT 0>
        	<!--- get current state --->
            <cfquery name="group">
            	SELECT	active
                FROM	unitGroupAssets
                WHERE	group_id = #groupID#
            </cfquery>
            
            <cfif group.recordCount GT 0>
            
				<cfset groupState = group.active>
                
                <cfif group.active IS 0>
                    <cfset groupState = 1>
                <cfelse>
                	<cfset groupState = 0>
                </cfif>
                
                <!--- update unit active --->
                <cfquery name="unit">
                  UPDATE unitGroupAssets
                  SET active = #groupState#
                  WHERE	 group_id = #groupID#
                </cfquery>
 			
            </cfif>
            
        </cfif>
        
        <cfset result.active = groupState>
        
        <cfreturn result>
        
    </cffunction>
    
    <!--- Unit Active State --->
  	<cffunction name="setUnitActiveState" access="remote" returntype="struct" output="no">
        <cfargument name="unitID" type="numeric" required="yes">
        
        <cfset unitState = 0>
        <cfset result = {"unitID":unitID,"active":unitState}>
        
        <cfif unitID GT 0>
        	<!--- get current state --->
            <cfquery name="unit">
            	SELECT	active
                FROM	unitAssets
                WHERE	unit_id = #unitID#	
            </cfquery>
            
            <cfif unit.recordCount GT 0>
            
				<cfset unitState = unit.active>
                
                <cfif unit.active IS 0>
                    <cfset unitState = 1>
                <cfelse>
                	<cfset unitState = 0>
                </cfif>
                
                <!--- update unit active --->
                <cfquery name="unit">
                  UPDATE unitAssets
                  SET active = #unitState#
                  WHERE	 unit_id = #unitID#
                </cfquery>
            
            </cfif>
            
        </cfif>
        
        <cfset result.active = unitState>
        
        <cfreturn result>
        
    </cffunction>
    
    
    
    <!--- Delete Group --->
 	<cffunction name="deleteGroup" access="remote" returntype="boolean" output="yes">
    	<cfargument name="appID" type="numeric" required="yes">
    	<cfargument name="subgroupID" type="numeric" required="yes">
		
        <!--- Delete Group --->
        <cfquery name="deleteGroup">
            DELETE FROM unitGroupAssets
            WHERE group_id = #subgroupID#
        </cfquery>
        
        <!--- Delete Units in Group --->
        <cfquery name="deleteUnits">
            DELETE FROM unitAssets
            WHERE group_id = #subgroupID#
        </cfquery>

        <cfinvoke component="units" method="getGroups" returnvariable="subGroups">
            <cfinvokeargument name="appID" value="#session.appID#"/>
            <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
        </cfinvoke>
     
        <!--- More Groups --->
        <cfif arrayLen(subGroups) GT 0>
            
            <cfloop index="aRec" array="#subGroups#">
            	
                <cfinvoke component="units" method="deleteGroup" returnvariable="success">
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="subgroupID" value="#aRec.group_id#"/>
                </cfinvoke>
                
                <!--- Delete Units in Group --->
                <cfquery name="deleteUnits">
                    DELETE FROM unitAssets
                    WHERE group_id = #aRec.group_id#
                </cfquery>
  
            </cfloop>

        </cfif>
       
        <cfreturn true>
        
    </cffunction>
    
    
    <!--- Add Group Asset--->
  	<cffunction name="addGroupAsset" access="public" returntype="numeric" output="yes">
    	<cfargument name="appID" type="numeric" required="yes">
    	<cfargument name="subgroupID" type="numeric" required="yes">
        
        <cfset groupID = 0>

            <cfquery name="newGroup"> 
                INSERT INTO unitGroupAssets (
                name, sortOrder, app_id, subgroup_id
                )
                VALUES (
                #accessLevel#, #active#, '', #order#, #appID#, #subgroupID#
                )
                SELECT @@IDENTITY AS groupID
            </cfquery>
    
            <cfset groupID = newGroup.groupID>
        
        <cfreturn groupID>
        
    </cffunction> 
    
    
    <!--- Update Group--->
  	<cffunction name="updateGroup" access="remote" returntype="struct" output="yes">
    	<cfargument name="appID" type="numeric" required="yes">
    	<cfargument name="subgroupID" type="numeric" required="yes">
        <cfargument name="name" type="string" required="no" default="untitled">

            <cfquery name="group">
              UPDATE unitGroupAssets
              SET name = '#name#'
              WHERE	 group_id = #subgroupID#
          </cfquery>
        
        <cfset data = {"subgroupID":subgroupID, "groupName":name}>
        
        <cfreturn data>
        
    </cffunction>
    
    
   
   
   
    <!--- Get All Groups--->
  	<cffunction name="getAllGroups" access="remote" returntype="query" output="no">
    	<cfargument name="appID" type="numeric" required="yes">
        
        <cfquery name="groups">
            SELECT        group_id, name
            FROM            UnitGroupAssets
            WHERE        (app_id = #appID#)
            ORDER BY sortOrder, group_id
        </cfquery>
        
        <cfreturn groups>
        
    </cffunction>
    
    
    <!--- Move Group--->
  	<cffunction name="moveGroup" access="remote" returntype="boolean" output="no">
    	<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="subgroupID" type="numeric" required="yes">
        
        <cfquery name="group">
              UPDATE unitGroupAssets
              SET subgroup_id = #subgroupID#
              WHERE	 group_id = #groupID#
          </cfquery>
        
        <cfreturn true>
        
    </cffunction>
    
    
    
    <!--- Get Groups from Module --->
 	<cffunction name="getGroups" access="public" returntype="array">
    	<cfargument name="appID" type="numeric" required="no" default="0">
		<cfargument name="projectID" type="numeric" required="no" default="0">
        
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="subgroupID" type="numeric" required="no" default="0">
        
        <cfquery name="allGroups">
            SELECT        group_id, name, subgroup_id, project_id, balcony_id, active, sortOrder
			FROM          unitGroupAssets
			WHERE		  0 = 0
            			<cfif appID GT 0>
                          AND (app_id = #appID#) 
						</cfif>
                        <cfif projectID GT 0>
                          AND (project_id = #projectID#) 
						</cfif>
                        
                        <cfif groupID GT 0>
                            AND (group_id = #groupID#)
                        <!--- <cfelse> --->
                        
                            <!--- <cfif subgroupID GT '0'>
                                AND (subgroup_id = #subgroupID#)
                            <cfelse>
                                AND (subgroup_id = 0)
                            </cfif> --->
                            
                        </cfif>
                        AND (subgroup_id = #subgroupID#)
            ORDER BY sortOrder
        </cfquery>
       
        <cfset groups = arrayNew(1)>

        <cfloop query="allGroups">
            
            <cfset data = {"group_id":#group_id#, "name":"#name#", "subgroup_id":#subgroup_id#, "project_id":#project_id#, "balcony_id":balcony_id, "active":active, "order":sortOrder}>
                      
            <cfset arrayAppend(groups,data)>
            
        </cfloop>
        
		<cfreturn groups>
        
	</cffunction>
    
    
    
    <!--- Create Unit--->
  	<cffunction name="createUnit" access="remote" returntype="numeric" output="no">
        <cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="name" type="string" required="yes">
        
        <cfset unitID = 0>
        
		<cfquery name="newUnit"> 
            INSERT INTO unitAssets (name, group_id)			
            VALUES (
            '#name#', 
            #groupID#
            )
            SELECT @@IDENTITY AS unitID
        </cfquery>

		<cfset unitID = newUnit.unitID>
		
        <cfreturn unitID>
        
    </cffunction>
    
    
    
    
    <!--- Get Units from Group --->
 	<cffunction name="getUnits" access="public" returntype="array">
    	<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="projectID" type="numeric" required="no" default="0">
        
        
        <cfif projectID GT 0>
        
        	<!--- get appID if projectID provided --->
        	<cfquery name="apps">
                SELECT        app_id
                FROM          UnitGroupAssets
                WHERE		  (project_id = #projectID#) 
            </cfquery>
            
            <cfset appID = apps.app_id>
        
        </cfif>
        
        <cfquery name="allUnits">
            SELECT        UnitAssets.asset_id, UnitAssets.unit_id, UnitAssets.name, UnitGroupAssets.group_id, UnitAssets.modified, UnitAssets.created, UnitAssets.views, 
                            UnitAssets.unitType_id, UnitAssets.active, UnitAssets.sortOrder, Groups.active AS assetActive
            FROM            UnitAssets INNER JOIN
                                    Groups ON UnitAssets.asset_id = Groups.group_id LEFT OUTER JOIN
                                    UnitGroupAssets ON UnitAssets.group_id = UnitGroupAssets.group_id
			WHERE		  0 = 0
            			 AND (UnitGroupAssets.app_id = #appID#) 
                         AND (UnitGroupAssets.group_id = #groupID#)
            ORDER BY unitAssets.sortOrder
        </cfquery>
      
      <cfset units = arrayNew(1)>

        <cfloop query="allUnits">
            
            <cfset data = {"unit_id":#unit_id#, "name":"#name#", "groupID":#asset_id#, "views":views, "type":unitType_id, "active":active, "order":sortOrder, "assetActive":assetActive}>
                      
            <cfset arrayAppend(units,data)>
            
        </cfloop>
   
		<cfreturn units>
        
	</cffunction>
    
    
    <!--- Update Unit--->
  	<cffunction name="updateUnit" access="remote" returntype="struct" output="yes">
    	<cfargument name="appID" type="numeric" required="yes">
    	<cfargument name="unitID" type="numeric" required="yes">
        <cfargument name="name" type="string" required="no" default="untitled">

          <cfquery name="unit">
              UPDATE unitAssets
              SET name = '#name#'
              WHERE	 unit_id = #unitID#
          </cfquery>
        
        <cfset data = {"unitID":unitID, "unitName":name}>
        
        <cfreturn data>
        
    </cffunction>
    
    
    <!--- Move Unit--->
  	<cffunction name="moveUnit" access="remote" returntype="boolean" output="no">
        <cfargument name="unitID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfquery name="group">
              UPDATE unitAssets
              SET group_id = #groupID#
              WHERE	 unit_id = #unitID#
          </cfquery>
        
        <cfreturn true>
        
    </cffunction>
    
    
    
    <!--- Delete Unit --->
 	<cffunction name="deleteUnit" access="remote" returntype="boolean" output="yes">
    	<cfargument name="unitID" type="numeric" required="yes">
		
        <!--- Delete Group --->
        <cfquery name="deleteGroup">
            DELETE FROM unitAssets
            WHERE unit_id = #unitID#
        </cfquery>

        <cfreturn true>
        
    </cffunction>
    
    
    
    
    <!--- Get Unit Info--->
  	<cffunction name="getUnit" access="remote" returntype="struct" output="no">
    	<cfargument name="unitID" type="numeric" required="yes">
        <cfargument name="auth_token" type="string" required="no" default="">
        <cfargument name="noPass" type="boolean" required="no" default="true">
 
        <cfquery name="units">
            SELECT        UnitGroupAssets.name AS groupName, UnitGroupAssets.subgroup_id, UnitAssets.name AS unitName, UnitAssets.asset_id, UnitAssets.user_id, 
                        UnitAssets.refNum, UnitAssets.state, UnitAssets.views, UnitAssets.cust_id, UnitTypes.unitType_id, UnitTypes.name AS unitType, 
                        UnitTypes.code AS unitCode, UnitAssets.data, UnitAssets.created, UnitAssets.modified, Groups.active AS assetActive
			FROM            Groups INNER JOIN
                        UnitAssets ON Groups.group_id = UnitAssets.asset_id LEFT OUTER JOIN
                        UnitTypes ON UnitAssets.unitType_id = UnitTypes.unitType_id LEFT OUTER JOIN
                        UnitGroupAssets ON UnitAssets.group_id = UnitGroupAssets.group_id
            WHERE        (unitAssets.unit_id = #unitID#)

        </cfquery>
      
        <!--- assign user unit --->
        <cfset assignedUserID = units.user_id>
        
        <!--- Get UserID from Auth Token --->
        <cfinvoke component="Users" method="getUserID" returnvariable="userID">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
        
       <!--- Get Balcony Info for Unit --->
       <cfinvoke component="Units" method="getUnitFloorBalcony" returnvariable="balconyInfo">
          <cfinvokeargument name="unitID" value="#unitID#"/>
       </cfinvoke>
       
       <cfif NOT structIsEmpty(balconyInfo)>
       
		   <cfset views = balconyInfo.views>
           
           <cfset N = Mid(views,1,1)>
           <cfset E = Mid(views,2,1)>
           <cfset S = Mid(views,3,1)>
           <cfset W = Mid(views,4,1)>
           <cfset D = Mid(views,5,1)>
           
           <cfswitch expression="#D#">
               <cfcase value="S">
                    <cfset direction = "south">
               </cfcase>
               <cfcase value="W">
                    <cfset direction = "west">
               </cfcase>
               <cfcase value="E">
                    <cfset direction = "east">
               </cfcase>
               <cfdefaultcase>
                    <cfset direction = "north">
               </cfdefaultcase>
           </cfswitch>
           
           <cfset balconyViews = {"north":N, "east":E, "south":S, "west":W, "defaultView":"#direction#"}>

		   <!--- Balcony Asset --->
           <cfinvoke component="Content" method="getAssetContent" returnvariable="theBalconyAsset">
              <cfinvokeargument name="assetID" value="#balconyInfo.balcony_id#"/>
           </cfinvoke>
      
       <cfelse>
       		<cfset balconyViews = structNew()>
       </cfif>
      
        <cfset theUnits = structNew()>
        
        <cfoutput query="units">
        	
            <cfset cust = structNew()>
             
             <cfif cust_id GT 0>
            
				<!--- Customer --->
                <cfinvoke component="Users" method="getUserInfo" returnvariable="theCust">
                    <cfinvokeargument name="userID" value="#cust_id#"/>
                    <cfinvokeargument name="details" value="true"/>
                 </cfinvoke>
                 
                 <!--- QueryToStruct --->
                 <cfinvoke component="Misc" method="QueryToStruct" returnvariable="cust">
                    <cfinvokeargument name="query" value="#theCust#"/>
                    <cfinvokeargument name="forceArray" value="false"/>
                 </cfinvoke>
             
             </cfif>
             
             <cfif isArray(cust)><cfset cust = {}></cfif>
             
             <cfset user = structNew()>
             
             <cfif user_id GT 0>
             
				 <!--- User --->
                <cfinvoke component="Users" method="getUserInfo" returnvariable="theUser">
                    <cfinvokeargument name="userID" value="#user_id#"/>
                    <cfinvokeargument name="details" value="true"/>
                 </cfinvoke>
           	 
				 <!--- QueryToStruct --->
                 <cfinvoke component="Misc" method="QueryToStruct" returnvariable="user">
                    <cfinvokeargument name="query" value="#theUser#"/>
                    <cfinvokeargument name="forceArray" value="false"/>
                 </cfinvoke>
         		
                <cfif NOT isArray(user)>
                  	 
					 <!--- remove password from user --->
                     <cfif structKeyExists(user,'password') AND noPass>
                        <cfset structDelete(user,'password')>
                     </cfif>
                 
                 </cfif>
                 
             </cfif>
           
             <!--- groupPathIndex --->
             <cfinvoke component="Units" method="getGrouptPath" returnvariable="project">
                <cfinvokeargument name="groupID" value="#subgroup_id#"/>
             </cfinvoke>
             
             <cfset theProj = project[1].name>
             	
             <cfif data IS ''>
             	<cfset JSONData = {}>
             <cfelse>
             	<cfset JSONData = DeserializeJSON(data)>
             </cfif>
             
			<cfset theUnits = {"project":theProj, "unit":groupName, "unitName":unitName, "groupID": asset_id, "user":user, "cust":cust, "unitType":unitType, "unitCode":unitCode, "data":JSONData, "created":created, "modified":modified, "unitTypeID":unitType_id, "ref":refNum, "state":state, "assetActive":assetActive, "assets":[]}>
			
            <cfif assignedUserID GT 0>
            	<!--- user assigned to unit check is same user --->
				<cfif userID IS 0 OR assignedUserID NEQ userID>
                    <cfset theUnits.state = 5>
                </cfif>
            <cfelse>
            	<!--- user not assigned to unit --->
            </cfif>
            
            <cfif NOT structIsEmpty(balconyViews)>
            	<cfloop collection="#theBalconyAsset#" item="theKey"></cfloop>
            	<cfset structAppend(theBalconyAsset[theKey], {"views":balconyViews})>
	
            	<cfset arrayAppend(theUnits.assets,theBalconyAsset)>
            </cfif>
            
		</cfoutput>

        <cfreturn theUnits>
        
    </cffunction>
    
    
    
    
    
    <!--- Get Units in a Group --->
  	<cffunction name="getGroupedUnits" access="remote" returntype="array" output="no" hint="Returns floor(s) with unit data">
        <cfargument name="projectID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        
        <!--- get root project group from projectID --->
        <cfinvoke  component="CFC.Units" method="getGroups" returnvariable="rootProject">
            <cfinvokeargument name="projectID" value="#projectID#"/>
        </cfinvoke>
	
        <cfif arrayLen(rootProject) GT 0>
        
       		<cfset rootGroupID = rootProject[1].group_id>
        
			<!--- get all floors --->
            <cfinvoke  component="CFC.Units" method="getGroups" returnvariable="allGroups">
                <cfinvokeargument name="subgroupID" value="#rootGroupID#"/>
            </cfinvoke>
  			
            <!--- sort ArrayOfStructs --->
            <cfinvoke  component="CFC.Misc" method="ArrayOfStructSort" returnvariable="sortedGroups">
                <cfinvokeargument name="base" value="#allGroups#"/>
                <cfinvokeargument name="sortType" value="numeric"/>
                <cfinvokeargument name="sortOrder" value="asc"/>
                <cfinvokeargument name="pathToSubElement" value="order"/>
            </cfinvoke>
                       
            <cfset projectGroups = arrayNew(1)>
            
            <cfloop index="aGroup" array="#sortedGroups#">
            
            	<cfset theGroupID = aGroup.group_id>
                
                <!--- get all units --->
                <cfinvoke  component="CFC.Units" method="getUnits" returnvariable="allUnits">
                	<cfinvokeargument name="projectID" value="#projectID#"/>
                    <cfinvokeargument name="groupID" value="#theGroupID#"/>
                </cfinvoke>
				
                <cfset theZone = {"groupID":theGroupID,"name":aGroup.name,"units":allUnits}>
                
                <cfif groupID IS theGroupID OR groupID IS 0>
                	<cfset arrayAppend(projectGroups,theZone)>
                </cfif>
                
            </cfloop>
  
        </cfif>
        
        <cfreturn projectGroups>
        
    </cffunction>
    
    
    
    
    
    
    
    
    <!--- SEARCH --->
    
    <!--- Get Unit Info--->
  	<cffunction name="searchUnit" access="remote" returntype="any" output="yes" hint="Search units from String of Unit Name">
        <cfargument name="projectID" type="numeric" required="no" default="0">
    	<cfargument name="search" type="string" required="yes">
        
        <cfargument name="columns" type="string" required="no" default="unitName">
        
        <cfargument name="exactMatch" type="boolean" required="no" default="false">
        <cfargument name="maxReturns" type="numeric" required="no" default="0">
        
        
        
        <!--- get root project group from projectID --->
        <cfinvoke  component="CFC.Units" method="getGroups" returnvariable="rootProject">
            <cfinvokeargument name="projectID" value="#projectID#"/>
        </cfinvoke>
         
         <cfif arrayLen(rootProject) GT 0>
                
			<cfset rootGroupID = rootProject[1].group_id>
        
            <!--- get all floors --->
            <cfinvoke  component="CFC.Units" method="getGroups" returnvariable="allGroups">
                <cfinvokeargument name="subgroupID" value="#rootGroupID#"/>
            </cfinvoke>
            
            <cfset groupIDs = arrayNew(1)>
            <!--- all groupIDs to search in --->
            <cfloop index="aGroup" array="#allGroups#">
                <cfset arrayAppend(groupIDs,aGroup.group_id)>
            </cfloop>
        
         	
         	<!--- setup --->

			<cfset allUnits = []>
            <cfset cnt = 0>
            
            <cfset theCols = []>
            <cfset cols = ListToArray(columns,',')>
            
            <!--- filters incase cols are from other table --->
            <cfloop index="z" array="#cols#" >
      
                <cfswitch expression="#z#">
                    
                    <cfcase value="unitName">
                        <cfset arrayAppend(theCols,"UnitAssets.name")>
                    </cfcase>
                    
                    <cfcase value="unitType">
                        <cfset arrayAppend(theCols,"UnitTypes.name")>
                    </cfcase>
                    
                    <cfcase value="unitCode">
                        <cfset arrayAppend(theCols,"UnitTypes.code")>
                    </cfcase>
                    
                    <cfdefaultcase>
                        <cfset arrayAppend(theCols,"UnitAssets."&z)>
                    </cfdefaultcase>            
                
                </cfswitch> 
                
            </cfloop>
            
            <cfquery name="units">
            <!--- Query --->
                SELECT        UnitAssets.unit_id, UnitAssets.name, UnitAssets.state, Groups.name AS assetName, UnitTypes.name AS unitType, UnitTypes.code AS unitCode, 
                        Groups.group_id AS asset_id
				FROM            UnitAssets INNER JOIN
                        UnitTypes ON UnitAssets.unitType_id = UnitTypes.unitType_id LEFT OUTER JOIN
                        UnitGroupAssets ON UnitAssets.group_id = UnitGroupAssets.group_id LEFT OUTER JOIN
                        Groups ON UnitAssets.asset_id = Groups.group_id
                WHERE Groups.active = 1 AND UnitAssets.active = 1 AND UnitGroupAssets.active = 1
                
                AND		UnitGroupAssets.group_id IN(
                <cfloop index="groupID" array="#groupIDs#">
                     #groupID#,
                </cfloop>
                	0)
                <cfif exactMatch>
                    AND
                    <cfloop index="c" array="#theCols#">
                        <cfif cnt GT 0>OR</cfif> #c# LIKE '#search#'
                        <cfset cnt++>
                    </cfloop>
                    
                <cfelse>
                    AND
                    <cfloop index="c" array="#theCols#">
                        <cfif cnt GT 0>OR</cfif> #c# LIKE '%#search#%'
                        <cfset cnt++>
                    </cfloop>
                    
                </cfif>
                
			</cfquery>
    
            <!--- QueryToStruct --->
             <cfinvoke component="Misc" method="QueryToStruct" returnvariable="theData">
                <cfinvokeargument name="query" value="#units#"/>
                <cfinvokeargument name="forceArray" value="true"/>
             </cfinvoke>
		
        </cfif>
        
        <cfreturn theData>
        
    </cffunction>
	
    
    <!--- Search Requirment--->
  	<cffunction name="searchRequirement" access="remote" returntype="numeric" output="no" hint="search Requirement of length returned">
        <cfargument name="appID" type="numeric" required="yes">
        
        <cfset unitLen = 0>
  			
            <cfquery name="unitLen">
                SELECT        max(len(unitAssets.name)) AS length
                FROM            unitAssets INNER JOIN
                                        UnitGroupAssets ON unitAssets.group_id = UnitGroupAssets.group_id
                WHERE        (UnitGroupAssets.app_id = #appID#)
            </cfquery>
 			
            <cfset unitLen = unitLen.length>
        
        <cfreturn unitLen>
        
    </cffunction>
    
    
    
    <!--- Search Requirment--->
  	<cffunction name="getUnitAssignments" access="remote" returntype="query" output="no" hint="get all Units Assignmented">
        <cfargument name="appID" type="numeric" required="yes">
        <cfargument name="assigned" type="boolean" required="no" default="false">
        
        <cfargument name="groupName" type="string" required="no" default="">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        
        <cfargument name="unitType" type="numeric" required="no" default="0">
        
        <cfquery name="unitsFound">
            SELECT        unitAssets.unit_id, UnitTypes.name, UnitTypes.code, UnitGroupAssets.name AS groupName, UnitGroupAssets.subgroup_id, unitAssets.name AS unitName, 
                                unitAssets.asset_id, Groups.name AS assetGroup, UnitTypes.unitType_id
            FROM            unitAssets LEFT OUTER JOIN
                                    Groups ON unitAssets.asset_id = Groups.group_id LEFT OUTER JOIN
                                    UnitTypes ON unitAssets.unitType_id = UnitTypes.unitType_id LEFT OUTER JOIN
                                    UnitGroupAssets ON unitAssets.group_id = UnitGroupAssets.group_id
            
            WHERE           (unitAssets.cust_id IS <cfif assigned>NOT</cfif> NULL) 
            
            				<cfif groupName NEQ ''>
                            	AND (UnitGroupAssets.name LIKE '%#groupName#%') 
                            <cfelseif groupID GT 0>
                            	AND (UnitGroupAssets.group_id = #groupID#) 
                            </cfif>
                            <cfif unitType GT 0>
                            	AND (UnitTypes.unitType_id = #unitType#) 
                            </cfif>
                            
                            AND (UnitGroupAssets.app_id = #appID#) AND unitAssets.active
           ORDER BY 		UnitGroupAssets.group_id ASC
         </cfquery>
                
    	<cfreturn unitsFound>    
    
    </cffunction>
    
    
    
    
     <!--- Search Balcony Views for Units --->
  	<cffunction name="searchForUnits" access="remote" returntype="array" output="no" hint="get all Units that contain the following views">
        <cfargument name="projectID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="views" type="string" required="no" default="">
        <cfargument name="typeCode" type="string" required="no" default="">
        <cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="sizeMin" type="numeric" required="no" default="0">
        <cfargument name="sizeMax" type="numeric" required="no" default="0">
        
        <cfinvoke component="Units" method="getGroupedUnits" returnvariable="allFloors">
            <cfinvokeargument name="projectID" value="#projectID#"/>
            <cfinvokeargument name="groupID" value="#groupID#"/>
        </cfinvoke>
	
        <cfif views IS '0000'>
        	<cfset views = ''>
        </cfif>
        
        <cfset typeID = 0>
        
        <cfif typeCode NEQ ''>
        
            <cfquery name="unitType">
            	SELECT	unitType_id
                FROM	unitTypes
                WHERE	code = '#trim(typeCode)#'
            </cfquery>
           
            <cfif unitType.recordCount GT 0>
            	<cfset typeID = unitType.unitType_id>
			<cfelse>
            	<cfset typeID = 0>
            </cfif>
            
        </cfif>
        

        <cfset foundViews = arrayNew(1)>
       
        <!--- find units with views --->
        
        <!--- floor --->
        <cfloop index="aFloor" array="#allFloors#">
        	
            <cfset allUnits = aFloor.units>
			<cfset unitViews = arrayNew(1)>
            
			<!--- unit --->
            <cfloop index="aUnit" array="#allUnits#">
                
                <cfif aUnit.active IS 1>
                
					<cfset addUnitView = false>
                    
                    <cfset view = left(aUnit.views,4)>
                    
                    <cfif views NEQ ''>
                        <!--- views --->
                        <cfloop index="theView" from="1" to="#len(view)#">
                        
                            <cfif mid(view,theView,1) IS mid(views,theView,1) AND mid(views,theView,1) IS 1>
                                <cfset addUnitView = true>
                            </cfif>
            
                        </cfloop>
                    <cfelse>
                        <cfset addUnitView = true>
                    </cfif>
                    																						
                    <!--- is unit type --->
                    <cfif typeID GT 0>
                    
                        <cfif typeID IS aUnit.type>
                            <cfset addUnitView = true>
                        <cfelse>
                        	<cfset addUnitView = false>
                        </cfif>

                    </cfif>
               
                    <!--- is asset type --->
                    <cfif assetID GT 0>
                    
                        <cfif assetID IS aUnit.groupID>
                            <cfset addUnitView = true>
                        <cfelse>
                        	<cfset addUnitView = false>
                        </cfif>

                    </cfif>
                    
                    
                   
				   <!--- getSize from Other Field in Details --->
                    <cfquery name="unitSize">
                        SELECT	Details.other AS size
                        FROM	Details INNER JOIN Groups ON Details.detail_id = Groups.detail_id
                        WHERE	(Groups.group_id = #aUnit.groupID#)
                    </cfquery>
                    
                    <cfset theSize = unitSize.size>
                    <cfset structAppend(aUnit,{'size':theSize})>
                   
                   <!--- filter size --->
                   <cfif sizeMin NEQ 0 AND sizeMax NEQ 0>
                        
                        <cfif sizeMin GT sizeMax><cfset sizeMin = sizeMax></cfif>
                        <cfif sizeMax LT sizeMin><cfset sizeMax = sizeMin></cfif>
                        <cfif sizeMax IS 0><cfset sizeMax = 100000></cfif>
                        
                        <cfif theSize GTE sizeMin AND theSize LTE sizeMax>
                            <cfset addUnitView = true>
                        <cfelse>
                            <cfset addUnitView = false>
                        </cfif>
					
                    </cfif>
                    
                    
                    <!--- add unit to list if meets requirements --->
                    <cfif addUnitView>
                       
                        <cfif aUnit.views IS ''>
                        	<cfset structDelete(aUnit,'views')>
                        </cfif>
                        
                        <cfset structDelete(aUnit,'active')>
                        <cfset structDelete(aUnit,'order')>
                        <cfset structDelete(aUnit,'type')>
                        
                        <cfif arrayFind(unitViews,aUnit) IS 0>
                            <cfset arrayAppend(unitViews, aUnit)>
                        </cfif>
                                
                    </cfif>
        		
                </cfif>
                
            </cfloop>
            
            <cfset arrayAppend(foundViews,{'code':'#listGetAt(aFloor.name,2," ")#','name':'#aFloor.name#','groupID':aFloor.groupID,'units':unitViews})>
            
        </cfloop>

        <cfreturn foundViews>
    
    </cffunction>
    
    
   
    
    <!--- BreadCrumb Index--->
 	 <cffunction name="groupPathIndex" access="public" returntype="numeric" output="no">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
     
     	<cfinvoke component="Units" method="getGrouptPath" returnvariable="crumb">
            <cfinvokeargument name="groupID" value="#groupID#"/> 
        </cfinvoke>
        
        <cfreturn arrayLen(crumb)>
        
      </cffunction>
      
      
   
 
 
    	<!--- BreadCrumb --->
 	<cffunction name="getGrouptPath" access="public" returntype="array" output="no">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <cfset crumb = arrayNew(1)>
        
		<!--- Others --->
        <cfloop condition="groupID GT 0">
        
            <cfquery name="theGroup">
                SELECT	name, subgroup_id
                FROM	unitGroupAssets
                WHERE	group_id = #groupID# 
            </cfquery>
       
            <cfset arrayAppend(crumb,{"name":theGroup.name,"id":groupID})>
            
            <cfset groupID = theGroup.subgroup_id>

        </cfloop>
         
        <cfreturn crumb>
        
      </cffunction>
   
   
   
   
   	<!--- BreadCrumb --->
 	<cffunction name="getBreadcrumb" access="public" returntype="void" output="yes">
        <cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <cfinvoke component="Units" method="getGrouptPath" returnvariable="crumb">
            <cfinvokeargument name="groupID" value="#groupID#"/> 
        </cfinvoke>
		
        <cfif arrayLen(crumb) GT 0>
            <cfloop index="z" from="#arrayLen(crumb)#" to="1" step="-1">
                <cfoutput>
                    <a href="AppsView.cfm?edit=true&tab=5&&subgroupID=#crumb[z].id#" class="plainLinkWhite">#crumb[z].name#</a> <span class="contentGreyed">/</span>
                </cfoutput>
            </cfloop>
        <cfelse>
        	Root Directory
        </cfif>
    </cffunction>  
    
    
    <!--- Update User Assignment --->
    <cffunction name="updateUnitAssignment" access="remote" returntype="string">
    	<cfargument name="unitID" type="numeric" required="yes" default="0">
		<cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="custID" type="numeric" required="no" default="0">

        <cfif userID GT 0 OR userID IS -1>
        
            <cfquery name="group">
                UPDATE unitAssets
                SET user_id = <cfif userID IS -1>NULL<cfelse>#userID#</cfif>
                WHERE	 unit_id = #unitID#
            </cfquery> 
            
            <cfreturn "user">
        
        </cfif>
        
        <cfif custID GT 0 OR custID IS -1>
        
              <cfquery name="group">
                UPDATE unitAssets
                SET cust_id = <cfif custID IS -1>NULL<cfelse>#custID#</cfif>
                WHERE	 unit_id = #unitID#
            </cfquery>  
        	
            <cfreturn "cust">
            
        </cfif>
        
		<cfreturn ''>
        
	</cffunction> 
    
    
    <!--- Unit TYpe Select --->
    <cffunction name="getUnitTypes" access="remote" output="yes">
    	<cfargument name="selected" type="numeric" required="no" default="-1">

        <cfinvoke component="Units" method="getUnitTypeCodes" returnvariable="unitTypes" />

        <select name="unitTypes" style="width:180px">
          <option value="-1" <cfif selected IS -1> selected</cfif>>None</option>
            <cfoutput query="unitTypes">
                <option value="#unitType_id#"<cfif selected IS unitType_id> selected</cfif>>#name#-(#code#)</option>
            </cfoutput> 
        </select>
        
   </cffunction>
   
   
   <!--- Unit TYpe Codes --->
   <cffunction name="getUnitTypeCodes" access="public" output="no" returntype="query">
        
        <cfquery name="unitTypes">
            SELECT        unitType_id, name, code
            FROM          UnitTypes
            ORDER BY name ASC
        </cfquery>
   		
        <cfreturn unitTypes>
        
  </cffunction>
     
        
        
   <!--- Set Project to Unit Group --->
   <cffunction name="setProjectAssignment" access="remote" output="no" returntype="boolean">
        <cfargument name="groupID" type="numeric" required="yes">
   		<cfargument name="projectID" type="numeric" required="yes">
        	
            <cfquery name="group">
              UPDATE unitGroupAssets
              <cfif projectID IS -1>
               SET 	 project_id = NULL
              <cfelse>
              SET 	 project_id = #projectID#
              </cfif>
              WHERE	 group_id = #groupID#
          </cfquery>

   		<cfreturn true>
   
   </cffunction>
   
   
   <!--- Update UnitType Assignment --->
    <cffunction name="updateUnitTypeAssignment" access="remote" returntype="string">
    	<cfargument name="unitID" type="numeric" required="yes" default="0">
		<cfargument name="assetID" type="numeric" required="no" default="-1">

        <cfquery name="group">
            UPDATE  unitAssets
            SET 	unitType_id = <cfif assetID IS -1>NULL<cfelse>#assetID#</cfif>
            WHERE	unit_id = #unitID#
        </cfquery>  

        
	</cffunction> 
    
    
    <!--- Update Asset Assignment --->
    <cffunction name="updateAssetAssignment" access="remote" returntype="string">
    	<cfargument name="unitID" type="numeric" required="yes" default="0">
		<cfargument name="assetID" type="numeric" required="no" default="-1">

        <cfquery name="group">
            UPDATE  unitAssets
            SET 	asset_id = <cfif assetID IS -1>NULL<cfelse>#assetID#</cfif>
            WHERE	unit_id = #unitID#
        </cfquery>  

        
	</cffunction> 
    
    
    <cffunction name="getAssignments" access="remote" returntype="struct">
    	<cfargument name="projectID" type="numeric" required="yes"><!--- group 0 ID --->
		<cfargument name="assetID" type="numeric" required="no" default="0"><!--- model assetID --->
        <cfargument name="userID" type="numeric" required="no" default="0"><!--- UserID --->
        <!--- 
        <cfargument name="unitTypeID" type="numeric" required="no" default="0"><!--- refer to table --->
        <cfargument name="code" type="string" required="no" default=""><!--- 1BD --->
        
        <cfargument name="locationID" type="numeric" required="no" default="0"><!--- floor groupID --->
         --->
  
        <cfquery name="projects">
            SELECT        group_id, name
            FROM          UnitGroupAssets
            WHERE project_id = #projectID#
        </cfquery>
        
        <cfset data = {"floors":[]}>
        
        <cfloop query="projects">
            
            <cfquery name="unitGroups">    
                SELECT        group_id, name
                FROM            UnitGroupAssets
                WHERE subgroup_id = #projects.group_id#
            </cfquery>
            
            <cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="theUnitGroups">
                <cfinvokeargument name="query" value="#unitGroups#"/>
                <cfinvokeargument name="forceArray" value="false"/>
             </cfinvoke>
            
            <cfloop index="unitGroup" array="#theUnitGroups#">
            
                <cfquery name="units">    
                    SELECT      CASE WHEN unitAssets.cust_id IS NULL THEN 0 ELSE 1 END AS assigned,  
                    unitAssets.unit_id, unitAssets.name, unitAssets.asset_id, UnitTypes.name AS unitName, UnitTypes.code AS unitCode
                    FROM            unitAssets INNER JOIN
                                            UnitTypes ON unitAssets.unitType_id = UnitTypes.unitType_id
                    WHERE        (unitAssets.group_id = #unitGroup.group_id#)
                    
                    <cfif assetID GT 0>
                    AND (unitAssets.asset_id = #assetID#)
                    </cfif>
                    
                    <cfif userID GT 0>
                    AND ((unitAssets.user_id = #userID#) OR (unitAssets.user_id IS NULL))
                    </cfif>
                    
                </cfquery>
                
                <!--- QueryToStruct --->
                 <cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="theUnit">
                    <cfinvokeargument name="query" value="#units#"/>
                    <cfinvokeargument name="forceArray" value="true"/>
                 </cfinvoke>
                
                <cfif arrayLen(theUnit) GT 0>
                
                	<cfloop index="aUnit" array="#theUnit#">
                        <cfset details = {"details":{"title":aUnit.name}}>
                    	<cfset structAppend(aUnit,details)>
                    </cfloop>
                
                    <cfset arrayAppend(data.floors,{"#unitGroup.name#":{"assets":theUnit, "details":{"title":#unitGroup.name#}}})>
                    
                </cfif>
                
            </cfloop>
        
        </cfloop>

        <cfreturn data> 
        
     </cffunction>
     
     
     <!--- Set Unit Assignment --->
     <cffunction name="setAssignment" access="remote" returntype="boolean">
		<cfargument name="unitID" type="numeric" required="yes"><!--- UnitID --->
        <cfargument name="custID" type="numeric" required="yes"><!--- CustID --->
        <cfargument name="userID" type="numeric" required="yes"><!--- UserID --->
        <cfargument name="override" type="boolean" required="no" default="false"><!--- Override --->
        
        	<cfquery name="units"> 
                SELECT        user_id, cust_id, unit_id
                FROM            UnitAssets
                WHERE        (unit_id = #unitID#) AND (cust_id IS NULL)
        	</cfquery>
      
        	<cfif units.recordCount GT 0>
            	<!--- Not Assigned --->
                
                <cfquery name="group">
                    UPDATE  unitAssets
                    SET 	cust_id = #custID#, user_id = #userID#
                    WHERE	unit_id = #unitID#
                </cfquery>
  
            <cfelse>
            	<!--- Already has been assigned --->
                <cfif override>
                	<!--- override --->
                    <cfquery name="group">
                        UPDATE  unitAssets
                        SET 	cust_id = #custID#, user_id = #userID#
                        WHERE	unit_id = #unitID#
                    </cfquery>
                
                <cfelse>
                	<cfreturn false>
                </cfif>
            </cfif>

        <cfreturn true>
        
     </cffunction>
     
     
     
      <!--- Set Unit Assignment --->
     <cffunction name="UpdateRefNumber" access="remote" returntype="boolean">
		<cfargument name="unitID" type="numeric" required="no" default="0"><!--- UnitID --->
        <cfargument name="refNum" type="string" required="no" default=""><!--- CustID --->
        
        <cfset updated = false>
        
        <!---CurDate--->
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate"></cfinvoke>
        
        <cfif unitID GT 0 AND refNum NEQ ''>
        	
            <!--- unit exists --->
        	<cfinvoke component="Units" method="unitExists" returnvariable="unitExists">
              <cfinvokeargument name="unitID" value="#unitID#"/>
            </cfinvoke>
      
        	<cfif unitExists>
            
            	<!--- Update RefNum --->
                <cfquery name="group">
                    UPDATE  unitAssets
                    SET 	refNum = '#refNum#', modified = #curDate#
                    WHERE	unit_id = #unitID#
                </cfquery>
            	
                <cfset updated = true>
                
            </cfif>
		
        </cfif>
        
        <cfreturn updated>
        
     </cffunction>
     
     
      <!--- Unit exists --->
     <cffunction name="unitExists" access="remote" returntype="boolean">
		<cfargument name="unitID" type="numeric" required="no" default="0">
        
        <cfset exists = false>
        
        <cfif unitID GT 0>
        
        	<cfquery name="unit"> 
                SELECT        unit_id
                FROM          UnitAssets
                WHERE        (unit_id = #unitID#)
        	</cfquery>
      
        	<cfif unit.recordCount GT 0>
				<cfset exists = true>
            </cfif>
		
        </cfif>
        
        <cfreturn exists>
        
     </cffunction>
     
     
     
             
   <!--- Set Balcony Floor to Unit Group --->
   <cffunction name="setFloorBalconyAssignment" access="remote" output="no" returntype="boolean">
        <cfargument name="groupID" type="numeric" required="yes">
   		<cfargument name="balconyID" type="numeric" required="no" default="0">
        	
            <cfif groupID GT 0>
    
            <cfquery name="group">
              UPDATE unitGroupAssets
              <cfif balconyID IS 0>
               SET 	 balcony_id = NULL
              <cfelse>
               SET 	 balcony_id = #balconyID#
              </cfif>
              WHERE	 group_id = #groupID#
          </cfquery>

          <!--- clear views for this group --->
			<cfquery name="groupViews">
              UPDATE unitAssets
               SET 	 views = NULL
              WHERE	 group_id = #groupID#
           </cfquery>
           
           <cfelse>
           	<cfreturn false>
          </cfif>
          
   		<cfreturn true>
   
   </cffunction>
   
   
   
   <!--- get Balcony Floor for Unit  --->
   <cffunction name="getUnitFloorBalcony" access="remote" output="no" returntype="struct">
        <cfargument name="unitID" type="numeric" required="yes">

            <cfquery name="balconyInfo"> 
                SELECT        UnitGroupAssets.balcony_id, UnitAssets.unit_id, BalconyAssets.floorName, BalconyAssets.north, UnitAssets.views
                FROM            UnitAssets INNER JOIN
                                        UnitGroupAssets ON UnitAssets.group_id = UnitGroupAssets.group_id INNER JOIN
                                        BalconyAssets ON UnitGroupAssets.balcony_id = BalconyAssets.asset_id
                WHERE        (UnitAssets.unit_id = #unitID#)
        	</cfquery>
            
            <cfset balcony = structNew()>
            
            <cfif balconyInfo.recordCount GT 0>
            
				<!--- QueryToStruct --->
               <cfinvoke component="Misc" method="QueryToStruct" returnvariable="balcony">
                  <cfinvokeargument name="query" value="#balconyInfo#"/>
                  <cfinvokeargument name="forceArray" value="false"/>
               </cfinvoke>
               
                <cfset theImageFull = ''>
                
                <cfif balcony.north NEQ ''>
                    <cfset compName = "#LCase(listGetAt(balcony.north,1,'_'))#_comp">
                    <cfset theImageFull = '#compName#.jpg'>
                </cfif>
                
                <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                    <cfinvokeargument name="AssetID" value="#balconyInfo.balcony_id#"/>
                    <cfinvokeargument name="server" value="true"/>
                </cfinvoke>
                
                <cfset compImg = assetPath & theImageFull>
                
                <cfif fileExists(compImg)>
                    <cfset structAppend(balcony,{'comp':compImg})>
                </cfif>
                
                <cfset structDelete(balcony,'north')>
                
                <cfif balcony.views IS ''>
                    <cfset balcony.views = '0000N'>
                </cfif>
            
            </cfif>
            
   		<cfreturn balcony>
   
   </cffunction>
   
   
   
   <!--- update Balcony Floor for Unit  0000N --->
   <cffunction name="setBalconyViewAssignment" access="remote" output="no" returntype="boolean">
        <cfargument name="unitID" type="numeric" required="yes">
        <cfargument name="views" type="string" required="no" default="0000N">
   
   			<cfquery name="unitBalcony">
                UPDATE  unitAssets
                SET 	views = '#views#'
                WHERE	unit_id = #unitID#
            </cfquery>

   		<cfreturn true>	
   
   </cffunction>
   
   
   
   
   <!--- set group order --->
   <cffunction name="setGroupOrder" access="remote" output="no" returntype="boolean">
        <cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="order" type="numeric" required="no" default="-1">
   
   			<cfquery name="groupOrder">
                UPDATE  unitGroupAssets
                SET 	sortOrder = #order#
                WHERE	group_id = #groupID#
            </cfquery>

   		<cfreturn true>	
   
   </cffunction>
   
   
   <!--- unit order --->
   <cffunction name="setUnitOrder" access="remote" output="no" returntype="boolean">
        <cfargument name="unitID" type="numeric" required="yes">
        <cfargument name="order" type="numeric" required="no" default="-1">
   
   			<cfquery name="unitOrder">
                UPDATE  unitAssets
                SET 	sortOrder = #order#
                WHERE	unit_id = #unitID#
            </cfquery>

   		<cfreturn true>	
   
   </cffunction>
 
   
   <!--- get project plans list --->
   <cffunction name="getUnitPlans" access="remote" output="no" returntype="struct" hint="gets plans for unit or model">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="projectID" type="numeric" required="no" default="0">
        <cfargument name="unitID" type="numeric" required="no" default="0">
        <cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="active" type="numeric" required="no" default="0">
        
        <cfset allUnitPlans = structNew()>
        
        <!--- get projectID if groupID specified --->
        <cfif groupID GT 0>
        
			<!--- get projectID --->
            <cfquery name="theProject">
                SELECT	project_id, subgroup_id
                FROM	UnitGroupAssets
                WHERE	group_id = #groupID#
            </cfquery>
            
            <cfif theProject.subgroup_id GT 0>
				
				<cfquery name="theProject">
					SELECT	project_id, subgroup_id
					FROM	UnitGroupAssets
					WHERE	group_id = #theProject.subgroup_id#
				</cfquery>
                
            </cfif>
            
            <cfset projectID = theProject.project_id>
        
        </cfif>

		  <!--- get unit asset --->
          <cfif unitID GT 0>
          
              <cfquery name="theUnit">
                  SELECT	asset_id
                  FROM	UnitAssets
                  WHERE	unit_id = #unitID#
              </cfquery>
              
              <cfif theUnit.recordCount GT 0>
                  <cfset assetID = theUnit.asset_id>
              </cfif>
          
          </cfif>
      
          <cfif projectID GT 0>
            
            <!--- get content --->
            <cfquery name="allGroups">
                SELECT       group_id, name, subgroup_id, asset_id
                FROM         Groups
                WHERE        subgroup_id = #projectID#
                <cfif assetID GT 0>
                    AND group_id = #assetID#
                </cfif>
            </cfquery>
            
          <cfelse>
          	<!--- get unit asset --->
            <cfquery name="allGroups">
                SELECT       group_id, name, subgroup_id, asset_id
                FROM         Groups
                WHERE        group_id = #assetID#
            </cfquery>
           
          </cfif>

 			<!--- compile data --->
            <cfloop query="allGroups">
            
            
                <cfquery name="content">
                    SELECT        group_id, name
                    FROM          Groups
                    WHERE        (subgroup_id = #allGroups.group_id#) AND (name = N'Floor plans')
                </cfquery>
               
                
                <cfif content.recordCount GT 0>
                
                    <cfquery name="plans">
                        SELECT        Groups.group_id, Groups.name, Groups.subgroup_id, Groups.asset_id, Assets.name AS planName, Assets.asset_id AS planID
                        FROM          Groups INNER JOIN Assets ON Groups.asset_id = Assets.asset_id
                        WHERE        (Groups.subgroup_id = #content.group_id#)
                        <cfif active GT 0>
                        AND active = 1
                        </cfif>
                    </cfquery>
                    
                    <!--- <cfset fp = listToArray(ValueList(plans.planName ,', '))> --->
                    <cfset fp = arrayNew(1)>
                    
                    <cfoutput query="plans">
						<cfset arrayAppend(fp,{'plan':planName, 'assetID':planID})>
					</cfoutput>
                    
                    <!--- getSize from Other Field in Details --->
                    <cfquery name="unitSize">
                        SELECT	Details.other AS size
                        FROM	Details INNER JOIN Groups ON Details.detail_id = Groups.detail_id
                        WHERE	(Groups.group_id = #allGroups.group_id#)
					</cfquery>
                    
                    <cfset theSize = unitSize.size>
                    
                    <cfset unitData = {'assetID':allGroups.group_id,'plans':fp, 'size':theSize}><!--- content.group_id is assetGroupID for Floorplans --->
                    
                    <cfset structAppend(allUnitPlans, {'#allGroups.name#':unitData})>
                    
                </cfif>
            
            </cfloop>

        
        <cfreturn allUnitPlans>
    
    </cffunction>
    
    
   <!--- get projectID from UnitID --->
   <cffunction name="getProjectID" access="remote" output="no" returntype="numeric">
        <cfargument name="unitID" type="numeric" required="no" default="0">
   		<cfargument name="groupID" type="numeric" required="no" default="0">
        	
            <cfset projectID = 0>
            
            <cfif unitID GT 0>
            
                <cfquery name="unitGroup">
                    SELECT	group_id
                    FROM	UnitAssets
                    WHERE	unit_id = #unitID#
                </cfquery>
                
                <cfset groupID = unitGroup.group_id>
                
                </cfif>
            
            <cfif groupID GT 0>
            
                <cfquery name="group">
                    SELECT	subgroup_id, project_id
                    FROM	UnitGroupAssets
                    WHERE	group_id = #groupID#
                </cfquery>
                
                <cfif group.subgroup_id IS 0>
                    <cfreturn group.project_id>
                <cfelse>
                    
                    <cfinvoke component="Units" method="getProjectID" returnvariable="projectID">
                      <cfinvokeargument name="groupID" value="#group.subgroup_id#"/>
                    </cfinvoke>
                    
                    <cfreturn projectID>
                    
                </cfif>
            
            </cfif>

   		<cfreturn projectID>	
   
   </cffunction>
    
           
</cfcomponent>