<cfcomponent>
	<cffunction name="getUserAccounts" access="remote" returntype="array">
	
		<cfargument name="clientID" type="numeric" required="no" default="0">
		<cfargument name="appID" type="numeric" required="no" default="0">
		<cfargument name="Level" type="numeric" required="no" default="0">
		
			<cfquery name="users">				
				SELECT  Users.user_id, Users.access_id, Users.name, Users.email, Users.active, Users.app_id, Users.activated, Users.client_id, Users.code, Users.password, Users.userType, Users.dev, 
                        AccessLevels.accessLevel, Tokens.token
				FROM    Users LEFT OUTER JOIN
                        AccessLevels ON Users.access_id = AccessLevels.access_id LEFT OUTER JOIN
                        Tokens ON Users.user_id = Tokens.user_id
				
				WHERE name <> 'guest' AND Users.email <> ''
				<cfif clientID GT 0>AND Users.client_id = #clientID#</cfif>
				<cfif appID GT 0>AND Users.app_id = #appID#</cfif>
				<cfif Level GT 0>AND AccessLevels.accessLevel = #Level#</cfif>
				ORDER BY
				Users.app_id, AccessLevels.accessLevel DESC
				<cfif clientID GT 0>, Users.client_id</cfif>
				,name ASC
			</cfquery>
		
			<cfinvoke  component="Misc" method="QueryToStruct" returnvariable="allUsers">
				<cfinvokeargument name="query" value="#users#"/>
				<cfinvokeargument name="forceArray" value="true"/>
			</cfinvoke>
			
		<cfreturn allUsers>
		
	</cffunction>
	
	<!--- Get Client(s) --->
	<cffunction name="getClients" access="remote" returntype="array">
	
		<cfargument name="clientID" type="numeric" required="no" default="0">
		
			<cfquery name="allClients">
				SELECT client_id, path + '/' AS filePath, company AS companyName, icon AS clientIcon, active
				FROM  Clients	
				WHERE 0 = 0		
				<cfif clientID GT 0>AND client_id = #clientID#</cfif>
				ORDER BY company
			</cfquery>
			
			<cfinvoke  component="Misc" method="QueryToStruct" returnvariable="clients">
				<cfinvokeargument name="query" value="#allClients#"/>
			</cfinvoke>

		<cfreturn clients>
		
	</cffunction>
	
	<!--- Get Client App(s) --->
	<cffunction name="getClientApps" access="remote" returntype="array">
	
		<cfargument name="clientID" type="numeric" required="no" default="0">
		<cfargument name="appID" type="numeric" required="no" default="0">
		
			<cfinvoke  component="Apps" method="getClientApps" returnvariable="apps">
				<cfinvokeargument name="clientID" value="#clientID#"/>
				<cfinvokeargument name="appID" value="#appID#"/>
			</cfinvoke>
		
			<cfinvoke  component="Misc" method="QueryToStruct" returnvariable="clientApps">
				<cfinvokeargument name="query" value="#apps#"/>
				<cfinvokeargument name="forceArray" value="true"/>
			</cfinvoke>
		
		<cfreturn clientApps>
		
	</cffunction>
	
</cfcomponent>


