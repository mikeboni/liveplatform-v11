<cfcomponent>
	
    
    <cffunction name="GetImageData" access="public" returntype="array" output="false">
    
    <cfargument name="Image" type="any" required="true" />
    
    <!--- <cfset objImageUtils = CreateObject( "component", "imageutilsroot.imageUtils" ).Init() />
    
    <cfimage action="read" source="http://liveplatform.net/pennwell/pennwell/assets/images/thumbs/body-equipment.jpg" name="objImage" />
    
    <!--- Read in 50 x 50 pixels. --->
    <cfset arrPixels = objImageUtils.GetPixels(objImage,200,185,50,50) />
    
    <cfloop
                index="intY"
                from="1"
                to="#ArrayLen( arrPixels )#"
                step="1">
 
                <div class="row">
 
                    <!--- Loop over x axis. --->
                    <cfloop
                        index="intX"
                        from="1"
                        to="#ArrayLen( arrPixels[ intY ] )#"
                        step="1">
 
                        <!--- Get the pixel short-hand from the array. --->
                        <cfset objPixel = arrPixels[ intY ][ intX ] />
 
                        <div
                            class="pixel"
                            style="background-color: ###objPixel.Hex#;"
                            ></div>
 
                    </cfloop>
 
                </div>
 
            </cfloop> --->
    
    </cffunction>
    
    <cffunction name="GetPixels" access="public" returntype="array" output="false" hint="Returns a two dimensional array of RGBA values for the image. Array will be in the form of Pixels[ Y ][ X ] where Y is along the height axis and X is along the width axis.">
 
    <!--- Define arguments. --->
    <cfargument name="Image" type="any" required="true" hint="The ColdFusion image object whose pixel map we are returning." />
    <cfargument name="X" type="numeric" required="false" default="1" hint="The default X point where we will start getting our pixels (will be translated to 0-based system for Java interaction)." />
    <cfargument name="Y" type="numeric" required="false" default="1" hint="The default Ypoint where we will start getting our pixels (will be translated to 0-based system for Java interaction)." />
    <cfargument name="Width" type="numeric" required="false" default="#ImageGetWidth( ARGUMENTS.Image )#"hint="The width of the area from which we will be sampling pixels." />
    <cfargument name="Height" type="numeric" required="false" default="#ImageGetHeight( ARGUMENTS.Image )#" hint="The height of the area from which we will be sampling pixels." />
 
    <!--- Define the local scope. --->
    <cfset var LOCAL = {} />
 
    <!--- Define the pixels array. --->
    <cfset LOCAL.Pixels = [] />
 
    <!---
        Get the image data raster. This holds all the data
        for the buffered image and will give us access to the
        individual pixel data.
    --->
    <cfset LOCAL.Raster = ImageGetBufferedImage( ARGUMENTS.Image ).GetRaster() />
 
 
    <!---
        Now, starting at our Y position, we want to move down
        each column (Y-direction) and gather the pixel values
        for each row along the Y-axis.
    --->
    <cfloop
        index="LOCAL.Y"
        from="#ARGUMENTS.Y#"
        to="#(ARGUMENTS.Y + ARGUMENTS.Height - 1)#"
        step="1">
 
        <!---
            Create a nested array for this row of pixels.
            Remember, the data will be in the Pixel[ Y ][ X ]
            array format.
        --->
        <cfset ArrayAppend(
            LOCAL.Pixels,
            ArrayNew( 1 )
            ) />
 
        <!---
            Loop over this row of pixels at this given Y-axis
            position.
        --->
        <cfloop
            index="LOCAL.X"
            from="#ARGUMENTS.X#"
            to="#(ARGUMENTS.X + ARGUMENTS.Width - 1)#"
            step="1">
 
            <!---
                Create an In-array to be used to retreive the
                color data. Each element will be used for the
                Red, Green, Blue, and Alpha channels respectively.
                Be sure to use the JavaCast() here rather than
                when we pass in the array to the GetPixel()
                method. This way, we don't lose the reference
                to the array value.
            --->
            <cfset LOCAL.PixelArray = JavaCast(
                "int[]",
                ListToArray( "0,0,0,0" )
                ) />
 
            <!--- Get the pixel data array. --->
            <cfset LOCAL.Raster.GetPixel(
                JavaCast( "int", (LOCAL.X - 1) ),
                JavaCast( "int", (LOCAL.Y - 1) ),
                LOCAL.PixelArray
                ) />
 
            <!---
                Now that we have an index-based pixel data
                array, let's convert the the data into a keyed
                struct that will be more user friendly.
            --->
            <cfset LOCAL.Pixel = {
                Red = LOCAL.PixelArray[ 1 ],
                Green = LOCAL.PixelArray[ 2 ],
                Blue = LOCAL.PixelArray[ 3 ],
                Alpha = LOCAL.PixelArray[ 4 ],
                Hex = ""
                } />
 
            <!--- Set the HEX value based on the RGB. --->
            <cfset LOCAL.Pixel.Hex = UCase(
                Right( "0#FormatBaseN( LOCAL.Pixel.Red, '16' )#", 2 ) &
                Right( "0#FormatBaseN( LOCAL.Pixel.Green, '16' )#", 2 ) &
                Right( "0#FormatBaseN( LOCAL.Pixel.Blue, '16' )#", 2 )
                ) />
 
 
            <!--- Add this pixel data to curren row. --->
            <cfset ArrayAppend(
                LOCAL.Pixels[ ArrayLen( LOCAL.Pixels ) ],
                LOCAL.Pixel
                ) />
 
        </cfloop>
 
    </cfloop>
 
 
    <!--- Return pixels. --->
    <cfreturn LOCAL.Pixels />
</cffunction>
    
    
</cfcomponent>