<cfcomponent> 

    <cffunction	name="getAllObjectModels" access="public" returntype="struct" output="true">
    	
         <cfquery name="allTypes">
              SELECT	assetType_id,dbTable
              FROM		AssetTypes
          </cfquery> 
        
        <cfinvoke component="Misc" method="QueryToStruct" returnvariable="objectTypes">
            <cfinvokeargument name="query" value="#allTypes#">
        </cfinvoke>
        
        <cfset allObjectsTypes = structNew()>
        
        <cfloop index="z" from="1" to="#arrayLen(objectTypes)#">
        
        	<cfset structAppend(allObjectsTypes,{"#objectTypes[z].dbTable#":objectTypes[z].assetType_id})>
        
        </cfloop>
        
        <cfreturn allObjectsTypes>
        
    </cffunction>
	
    
    <cffunction	name="getObjectModel" access="public" returntype="struct" output="true">

        <cfargument	name="typeID" type="numeric" required="false" default="0" />
		<cfargument	name="typeName" type="string" required="false" default="" />
        <cfargument	name="objectData" type="any" required="no" default="#structNew()#" />
     	<cfargument	name="includeActionsAssets" type="boolean" required="no" default="yes" />
     
        <cfif isArray(objectData)>
			<cfif arrayLen(objectData) GT 0>
                  <cfset objectData = objectData[1]>
            </cfif>
        </cfif>

        <!--- Get ID from Type Name --->
       
		<!--- getID --->
        <cfquery name="theObjectType">
          SELECT	assetType_id
          FROM		AssetTypes
          WHERE
          <cfif typeID GT 0>
          			assetType_id = #typeID#
          <cfelse>
          			dbTable = '#trim(typeName)#'
          </cfif>
          		
        </cfquery> 
        
        <cfif theObjectType.recordCount GT 0>
            <cfset typeID = theObjectType.assetType_id>
        <cfelse>
            <cfset typeID = 0>
        </cfif>
        
       <cfif NOT isStruct(objectData)><cfset typeID = 0></cfif>
	
        <cfset objectModel = structNew()>
         
  		<!--- Get Type Model --->
    	<cfif typeID GT 0>
        
            <cfswitch expression="#typeID#">
            	
                <cfcase value="1">
                
				  <!--- ImageAssets 1 																										--->
                  <cfinvoke component="ObjectModels" method="imageModel" returnvariable="objectModel">
                      <cfinvokeargument name="objectData" value="#objectData#">
                  </cfinvoke> 
	
                </cfcase>
                
                
                
                <cfcase value="2">
                
                    <!--- VideoAssets 2 																										--->
                    <cfinvoke component="ObjectModels" method="videoModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
  					
                </cfcase>
                
                
                
                <cfcase value="3">
                
                    <!--- PDFAssets 3 																												--->
                    <cfinvoke component="ObjectModels" method="documentModel" returnvariable="objectModel">
						<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                
                <cfcase value="4">
                
                    <!--- URLAssets 4 																												--->
                    <cfinvoke component="ObjectModels" method="urlModel" returnvariable="objectModel">
						<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
                    
                </cfcase>
                
                
                
                
                <cfcase value="5">
                
                    <!--- GPSAssets 5 																												--->
                    <cfinvoke component="ObjectModels" method="locationtModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                
                <cfcase value="6">
                
                    <!--- Model3DAssets 6 																											--->
                    <cfinvoke component="ObjectModels" method="model3DModel" returnvariable="objectModel">
						<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                <cfcase value="25">
                
                    <!--- Basic Model3DAssets 25																											--->
                    <cfinvoke component="ObjectModels" method="model3DBasicModel" returnvariable="objectModel">
						<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                <cfcase value="23">
                
                    <!--- SceneModel 23																											--->
                    <cfinvoke component="ObjectModels" method="SceneModel" returnvariable="objectModel">
						<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                
                <cfcase value="7">
                	<!--- Nothing --->
                </cfcase>
                
                
                
                
                <cfcase value="8">
                
                    <!--- PanoramaAssets 8 																												--->
                    <cfinvoke component="ObjectModels" method="panoramaModel" returnvariable="objectModel">
                        <cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                <cfcase value="9">
                
                    <!--- BalconyAssets 9 																													--->
                    <cfinvoke component="ObjectModels" method="balconyModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
                    
                </cfcase>
    
            
                
                
                <cfcase value="10">
                
                    <!--- XYZPointAssets 10 																												--->
                    <cfinvoke component="ObjectModels" method="xyzPointModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                <cfcase value="11">
                	<!--- Nothing --->
                </cfcase>
                <cfcase value="12">
                	<!--- Nothing --->
                </cfcase>
                <cfcase value="13">
                	<!--- Nothing --->
                </cfcase>
                <cfcase value="14">
                	<!--- Nothing --->
                </cfcase>
                
                
                <cfcase value="15">
                	
                    <!--- MarkerAssets 15 																													--->
                    <cfinvoke component="ObjectModels" method="markerModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
  
                </cfcase>
                
                
                <cfcase value="16">
                	<!--- Nothing --->
                </cfcase>
                
                
                <cfcase value="17">
                
                    <!--- AnimationAssets 17 																											--->
                    <cfinvoke component="ObjectModels" method="animationModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                
                <cfcase value="18">
                
                    <!--- GroupAssets 18 																												--->
                    <cfif isStruct(objectData)>
                    	<cfif NOT structIsEmpty(objectData)>
                    		<cfset objectData = [objectData]>
                        <cfelse>
                        	<cfset objectData = []>
                        </cfif>
                    </cfif>
                     
                    <cfinvoke component="ObjectModels" method="groupModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
                    
                </cfcase>
                
                
 
                <cfcase value="19">
                
                    <!--- GameAssets 19 																												--->
                    <cfinvoke component="ObjectModels" method="gameModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>

                </cfcase>
                
                
                <cfcase value="20">
                
                    <!--- QuizAssets 20																												--->
                    <cfinvoke component="ObjectModels" method="quizModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
        
                </cfcase>
                
                
                <cfcase value="21">
                
                    <!--- MaterialAssets 21 																											--->
                    <cfinvoke component="ObjectModels" method="materialModel" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
                    
                </cfcase>
                
                
                <cfcase value="22">
               
               		<!--- ProgramAsset 22 																											--->
                    <cfinvoke component="ObjectModels" method="programSchedule" returnvariable="objectModel">
                    	<cfinvokeargument name="objectData" value="#objectData#">
                    </cfinvoke>
                    
                                 
                </cfcase>
                
                
            	<!--- <cfcase value="25"> --->
											
					<!--- GEOFenceAssets --->
					<!--- FUTURE OR COMBINE WITH GPS AS LIST OF POINTS?--->
                    <!--- <cfinvoke component="ObjectModels" method="geofenceModel" returnvariable="objectModel" />
                    <cfloop collection="#objectModel#" item="theKey"></cfloop>
					
                    <cfif NOT structIsEmpty(objectData)>
                    	
                        <cfdump var="#objectData#"><cfdump var="#objectModel#"><cfabort>

                    </cfif> --->
                    
                <!--- </cfcase> --->
                
                
            </cfswitch>
  			
			<cfif NOT isDefined("objectData.asset_id")>
				
                <cfloop collection="#objectModel#" item="theKey"></cfloop>
                <cfif isDefined('theKey')>
                	<cfset assetID = objectModel[theKey].assetID>
				</cfif>
           
            <cfelse>
            
				<cfif structKeyExists(objectData,'asset_id')>
                    <cfset assetID = objectData.asset_id>
                <cfelse>
                    <cfset assetID = 0>
                </cfif>
			
            </cfif>
        
     
			<!--- <cfif includeActionsAssets AND isDefined('objectData.asset_id')>
	
				<!--- Assets and Actions 																													--->
                <cfinvoke  component="Content" method="getAssetsActions" returnvariable="theAssetsActions">
                    <cfinvokeargument name="assetID" value="#assetID#"/>
                </cfinvoke>
      	     
                <cfif structCount(theAssetsActions) GT 0>
                    
                    <cfloop collection="#objectModel#" item="theKey"></cfloop>
                    
                    <!--- Check if Assets exists in Object --->
                    <cfif NOT structKeyExists(objectModel[theKey],'assets')>
                        <cfset structAppend(objectModel[theKey],{"assets":[]})>
                    </cfif>
                    
                    <!--- Check if Actions exists in Object --->
                    <cfif NOT structKeyExists(objectModel[theKey],'actions')>
                        <cfset structAppend(objectModel[theKey],{"actions":[]})>
                    </cfif>
                    
                    <!--- Actions --->
                    <cfif NOT arrayIsEmpty(theAssetsActions.actions)>
                        <cfloop index="anObject" array="#theAssetsActions.actions#">
                            
                            <!--- Get Asset Data --->
                            <cfinvoke  component="Content" method="getAssetContent" returnvariable="assetData">
                                <cfinvokeargument name="assetID" value="#anObject.asset_id#"/>
                            </cfinvoke>
                           
                            <!--- Set AccessLevel, Shared, Cached --->
                            <cfloop collection="#assetData#" item="theAssetKey"></cfloop>
                            <cfif structKeyExists(anObject,'accessLevel')>
                                <cfif anObject.accessLevel GT 0>
                                    <cfset assetData[theAssetKey].access = anObject.accessLevel>
                                </cfif>
                            </cfif>
                            <cfif structKeyExists(anObject,'sharable')>
                                <cfif anObject.sharable GT 0>
                                    <cfset assetData[theAssetKey].sharable = anObject.sharable>
                                </cfif>
                            </cfif>
                            
                            <cfif structKeyExists(anObject,'cached')>
                                <cfif anObject.cached GT 0>
                                    <cfset assetData[theAssetKey].cached = anObject.cached>
                                </cfif>
                            </cfif>
                          
                            <!--- Add Assets to Group Object --->
                            <cfinvoke  component="ObjectModels" method="checkIfObjectExistsInArray" returnvariable="foundObject">
                                <cfinvokeargument name="array" value="#objectModel[theKey].assets#"/>
                                <cfinvokeargument name="object" value="#assetData#"/>
                            </cfinvoke>
              
                            <cfif foundObject GT 0>
                                <cfset arrayDeleteAt(objectModel[theKey].assets,foundObject)>
                            </cfif>
                            
                            <cfset arrayAppend(objectModel[theKey].actions,assetData)>
                            
                        </cfloop>
                    </cfif>
             
                    <!--- Assets --->
                    <cfif NOT arrayIsEmpty(theAssetsActions.assets) AND typeID NEQ 20>
                        <cfloop index="anObject" array="#theAssetsActions.assets#">
                            
                            <!--- Get Asset Data --->
                           <cfinvoke  component="Content" method="getAssetContent" returnvariable="assetData">
                                <cfinvokeargument name="assetID" value="#anObject.asset_id#"/>
                            </cfinvoke>
      
                            <!--- Set AccessLevel, Shared, Cached --->
                            <cfloop collection="#assetData#" item="theAssetKey"></cfloop>
                            
                            <cfif structKeyExists(anObject,'accessLevel')>
                                <cfif anObject.accessLevel GT 0>
                                    <cfset assetData[theAssetKey].access = anObject.accessLevel>
                                </cfif>
                            </cfif>
                            
                            <cfif structKeyExists(anObject,'sharable')>
                                <cfif anObject.sharable GT 0>
                                    <cfset assetData[theAssetKey].sharable = anObject.sharable>
                                </cfif>
                            </cfif>
                            <cfif structKeyExists(anObject,'cached')>
                                <cfif anObject.cached GT 0>
                                    <cfset assetData[theAssetKey].cached = anObject.cached>
                                </cfif>
                            </cfif>
                          
                            <!--- Add Assets to Group Object --->
                            <cfinvoke  component="ObjectModels" method="checkIfObjectExistsInArray" returnvariable="foundObject">
                                <cfinvokeargument name="array" value="#objectModel[theKey].assets#"/>
                                <cfinvokeargument name="object" value="#assetData#"/>
                            </cfinvoke>
                           
                           <cfif foundObject GT 0>
                                <cfset arrayDeleteAt(objectModel[theKey].assets,foundObject)>
                            </cfif> 
                            
                            <cfset arrayAppend(objectModel[theKey].assets,assetData)>
                          
                        </cfloop> 
                        
                    </cfif>
    
                </cfif>
          	
         
				<cfif arrayIsEmpty(objectModel[theKey].assets)>
                    <cfset structDelete(objectModel[theKey],'assets')>
                </cfif>
              
                <cfif arrayIsEmpty(objectModel[theKey].actions)>
                    <cfset structDelete(objectModel[theKey],'actions')>
                </cfif>
            	
            </cfif> ---><!--- THIS IS BAD!!! --->
            
            <cfif isDefined('assetID')>   
            <!--- Assets and Actions--->
            <cfinvoke  component="Content" method="getAssetsActions" returnvariable="theAssetsActions">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
        
            <cfif structCount(theAssetsActions) GT 0>
            	
                <cfloop collection="#objectModel#" item="theKey"></cfloop>
                
                <!--- Check if Assets exists in Object --->
                <cfif NOT structKeyExists(objectModel[theKey],'assets')>
                	<cfset structAppend(objectModel[theKey],{"assets":[]})>
                </cfif>
                
                <!--- Check if Actions exists in Object --->
                <cfif NOT structKeyExists(objectModel[theKey],'actions')>
                	<cfset structAppend(objectModel[theKey],{"actions":[]})>
                </cfif>
                
                <!--- Actions --->
                <cfif NOT arrayIsEmpty(theAssetsActions.actions)>
                    <cfloop index="anObject" array="#theAssetsActions.actions#">
                        
                        <!--- Get Asset Data --->
                        <cfinvoke  component="Content" method="getAssetContent" returnvariable="assetData">
                            <cfinvokeargument name="assetID" value="#anObject.asset_id#"/>
                        </cfinvoke>
                       
                       	<!--- Set AccessLevel, Shared, Cached --->
                        <cfloop collection="#assetData#" item="theAssetKey"></cfloop>
                        <cfif structKeyExists(anObject,'accessLevel')>
                        	<cfif anObject.accessLevel GT 0>
                        		<cfset assetData[theAssetKey].access = anObject.accessLevel>
                            </cfif>
                        </cfif>
                        <cfif structKeyExists(anObject,'sharable')>
                        	<cfif anObject.sharable GT 0>
                        		<cfset assetData[theAssetKey].sharable = anObject.sharable>
                            </cfif>
                        </cfif>
                        
                        <cfif structKeyExists(anObject,'cached')>
                        	<cfif anObject.cached GT 0>
                        		<cfset assetData[theAssetKey].cached = anObject.cached>
                            </cfif>
                        </cfif>
                      
                        <!--- Add Assets to Group Object --->
                        <cfinvoke  component="ObjectModels" method="checkIfObjectExistsInArray" returnvariable="foundObject">
                        	<cfinvokeargument name="array" value="#objectModel[theKey].assets#"/>
                            <cfinvokeargument name="object" value="#assetData#"/>
                        </cfinvoke>
          
                        <cfif foundObject GT 0>
                        	<cfset arrayDeleteAt(objectModel[theKey].assets,foundObject)>
						</cfif>
                        
                        <cfset arrayAppend(objectModel[theKey].actions,assetData)>
                        
                    </cfloop>
                </cfif>
                
                <!--- Assets --->
                <cfif NOT arrayIsEmpty(theAssetsActions.assets) AND typeID NEQ 20>
                    <cfloop index="anObject" array="#theAssetsActions.assets#">
                        
                        <!--- Get Asset Data --->
                        <cfinvoke  component="Content" method="getAssetContent" returnvariable="assetData">
                            <cfinvokeargument name="assetID" value="#anObject.asset_id#"/>
                        </cfinvoke>
 
                        <!--- Set AccessLevel, Shared, Cached --->
                        <cfloop collection="#assetData#" item="theAssetKey"></cfloop>
                        <cfif structKeyExists(anObject,'accessLevel')>
                        	<cfif anObject.accessLevel GT 0>
                        		<cfset assetData[theAssetKey].access = anObject.accessLevel>
                            </cfif>
                        </cfif>
                        
                        <cfif structKeyExists(anObject,'sharable')>
                        	<cfif anObject.sharable GT 0>
                        		<cfset assetData[theAssetKey].sharable = anObject.sharable>
                            </cfif>
                        </cfif>
                        <cfif structKeyExists(anObject,'cached')>
                        	<cfif anObject.cached GT 0>
                        		<cfset assetData[theAssetKey].cached = anObject.cached>
                            </cfif>
                        </cfif>
                        
                        <!--- Add Assets to Group Object --->
                        <cfinvoke  component="ObjectModels" method="checkIfObjectExistsInArray" returnvariable="foundObject">
                        	<cfinvokeargument name="array" value="#objectModel[theKey].assets#"/>
                            <cfinvokeargument name="object" value="#assetData#"/>
                        </cfinvoke>
                      
                        <cfif foundObject GT 0>
                        	<cfset arrayDeleteAt(objectModel[theKey].assets,foundObject)>
                        </cfif>
                        
						<cfset arrayAppend(objectModel[theKey].assets,assetData)>
                      
                    </cfloop>
                </cfif>

            </cfif>
          
            <cfif arrayIsEmpty(objectModel[theKey].assets)>
				<cfset structDelete(objectModel[theKey],'assets')>
            </cfif>
          
            <cfif arrayIsEmpty(objectModel[theKey].actions)>
				<cfset structDelete(objectModel[theKey],'actions')>
            </cfif>
            
            <cfloop collection="#objectModel#" item="theKey"></cfloop>
            
            <cfif NOT isDefined("objectData.asset_id")>
            
				<!--- Group --->
                <cfif isArray(objectData)>
					<cfif NOT arrayIsEmpty(objectData)>
                    	
                        <cfset objectName = objectData[1].name>
                    	
                        <cfset objectModel = {"#objectName#":objectModel[theKey]}>
                            
                            <cfif NOT structKeyExists(objectModel[objectData[1].name],"assets")>
                            	<cfset structAppend(objectModel[objectData[1].name],{"assets":[]})>
                            </cfif>
                            
                            <cfloop index="anGroupObject" array="#objectModel[objectData[1].name].assets#">
                                
                                <cfloop collection="#anGroupObject#" item="theGroupKey"></cfloop>
                                <cfif structKeyExists(anGroupObject[theGroupKey],'cached')>
									 <cfif anGroupObject[theGroupKey].cached IS 0>
                                        <cfset structDelete(anGroupObject[theGroupKey],"cached")> 
                                     <cfelse>
                                        <cfset anGroupObject[theGroupKey].cached = 1>
                                     </cfif>
                                 </cfif>
                                 
                                 <cfif structKeyExists(anGroupObject[theGroupKey],'sharable')>
									 <cfif anGroupObject[theGroupKey].sharable IS 0> 
                                        <cfset structDelete(anGroupObject[theGroupKey],"sharable")>
                                     <cfelse>
                                        <cfset anGroupObject[theGroupKey].sharable = 1>
                                     </cfif>  
                                 </cfif>
                                 
                                 <cfif structKeyExists(anGroupObject[theGroupKey],'access')>
									 <cfif anGroupObject[theGroupKey].access IS 0> 
                                        <cfset structDelete(anGroupObject[theGroupKey],"access")>
                                     <cfelse>
                                        <cfset anGroupObject[theGroupKey].access = 1>
                                     </cfif> 
                             	 </cfif>
                                 
                            </cfloop>
                        
                    </cfif>
                </cfif>
                
            <cfelse>
            
            	<!--- Normal --->
				<cfif structKeyExists(objectData,'asset_id')>
                	<cfif structKeyExists(objectData,'name')>
                        <cfloop collection="#objectModel#" item="theKey"></cfloop>
                        <cfset objectModel = {"#objectData.name#":objectModel[theKey]}>
                    </cfif>
                </cfif>
                
            </cfif>
                
              
            <cfloop collection="#objectModel#" item="theKey"></cfloop>
            
            <cfif NOT isDefined("objectData.asset_id")>
            
				<!--- Group --->
                <cfif isArray(objectData)>
					<cfif NOT arrayIsEmpty(objectData)>
                    	
                        <cfset objectName = objectData[1].name>
                    	
                        <cfset objectModel = {"#objectName#":objectModel[theKey]}>
                            
                            <cfif NOT structKeyExists(objectModel[objectData[1].name],"assets")>
                            	<cfset structAppend(objectModel[objectData[1].name],{"assets":[]})>
                            </cfif>
                            
                            <cfloop index="anGroupObject" array="#objectModel[objectData[1].name].assets#">
                                
                                <cfloop collection="#anGroupObject#" item="theGroupKey"></cfloop>
                                <cfif structKeyExists(anGroupObject[theGroupKey],'cached')>
									 <cfif anGroupObject[theGroupKey].cached IS 0>
                                        <cfset structDelete(anGroupObject[theGroupKey],"cached")> 
                                     <cfelse>
                                        <cfset anGroupObject[theGroupKey].cached = 1>
                                     </cfif>
                                 </cfif>
                                 
                                 <cfif structKeyExists(anGroupObject[theGroupKey],'sharable')>
									 <cfif anGroupObject[theGroupKey].sharable IS 0> 
                                        <cfset structDelete(anGroupObject[theGroupKey],"sharable")>
                                     <cfelse>
                                        <cfset anGroupObject[theGroupKey].sharable = 1>
                                     </cfif>  
                                 </cfif>
                                 
                                 <cfif structKeyExists(anGroupObject[theGroupKey],'access')>
									 <cfif anGroupObject[theGroupKey].access IS 0> 
                                        <cfset structDelete(anGroupObject[theGroupKey],"access")>
                                     <cfelse>
                                        <cfset anGroupObject[theGroupKey].access = 1>
                                     </cfif> 
                             	 </cfif>
                                 
                            </cfloop>
                        
                    </cfif>
                </cfif>
                
            <cfelse>
            
            	<!--- Normal --->
				<cfif structKeyExists(objectData,'asset_id')>
                	<cfif structKeyExists(objectData,'name')>
                        <cfloop collection="#objectModel#" item="theKey"></cfloop>
                        <cfset objectModel = {"#objectData.name#":objectModel[theKey]}>
                    </cfif>
                </cfif>
                
            </cfif>
            
			</cfif>
        </cfif>
       
        <cfreturn objectModel>
    
    </cffunction>
	
    
    
   <!--- check If Object Exists In Array --->
    <cffunction	name="checkIfObjectExistsInArray" access="public" returntype="numeric" output="true">
    	<cfargument	name="array" type="array" required="true" />
        <cfargument	name="object" type="struct" required="true" />
        
        <cfloop collection="#object#" item="theObject"></cfloop>
        
        <cfset found = 1>
        <cfloop index="anObject" array="#array#">
        
        	<cfloop collection="#anObject#" item="theKey"></cfloop>
        	<cfif theKey IS theObject>
            	<cfreturn found>
            </cfif>
            
            <cfset found++>
            
        </cfloop>
        
        <cfreturn 0>
        
    </cffunction>
    
    
    
    
    <!--- Get THUMBS, DETAILS AND COLOR --->
    <cffunction	name="assetInfo" access="public" returntype="struct" output="true">
    	<cfargument	name="assetID" type="numeric" required="false" default="0" />
        <cfargument	name="groupID" type="numeric" required="false" default="0" />
        
        <cfset otherPath = "">
       
        <cfif assetID GT 0>
        
			<!--- Get Asset --->
            <cfinvoke component="Assets" method="getAssets" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
             
            <!--- Other path for group color assets --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="otherPath">
              <cfinvokeargument name="assetID" value="#assetID#"/>
              <cfinvokeargument name="server" value="true"/>
            </cfinvoke>   																										
            
        <cfelse>
        
        	<!--- Get Group --->
            <cfinvoke component="Modules" method="getGroupDetails" returnvariable="assetData">
                <cfinvokeargument name="groupID" value="#groupID#"/>
            </cfinvoke>
            
            <!--- Other path for group color assets --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="otherPath">
              <!--- <cfinvokeargument name="groupID" value="#groupID#"/> --->
              <cfinvokeargument name="appID" value="#assetData.app_id#"/>
              <cfinvokeargument name="server" value="true"/>
            </cfinvoke>
           
        </cfif>
  																																	
        <cfif assetData.recordCount GT 0>
        
			<!--- QueryToStruct --->
             <cfinvoke component="Misc" method="QueryToStruct" returnvariable="objectData">
                <cfinvokeargument name="query" value="#assetData#"/>
             </cfinvoke>
        		
            <cfif isStruct(objectData)>
            
				<!--- Get Detail --->
                <cfinvoke  component="ObjectModels" method="detailsModel" returnvariable="details">
                    <cfinvokeargument name="objectData" value="#objectData#"/>
                </cfinvoke>
                                                                                                                                                   
                <!--- Get Thumb --->
                <cfinvoke  component="ObjectModels" method="thumbsModel" returnvariable="thumb">
                    <cfinvokeargument name="objectData" value="#objectData#"/>
                </cfinvoke>
        																									   
                <!--- Colors Background Path --->
                <cfif structKeyExists(objectData,'background')>
                	<cfif objectData.background NEQ ''>
                		<cfset objectData.background = "#otherPath##objectData.background#">
                    </cfif>
                </cfif>
               
                <!--- Get Colors --->
                <cfinvoke  component="ObjectModels" method="colorsModel" returnvariable="colors">
                    <cfinvokeargument name="objectData" value="#objectData#"/>
                </cfinvoke>

                <cfset details = details.details>
                <cfset thumb = thumb.thumb>
                <cfset colors = colors.colors>
                
            <cfelse>
            
            	<cfset details = {}>
				<cfset thumb = {}>
                <cfset colors = {}>
            
            </cfif>
            
        <cfelse>
        	
            <cfset details = {}>
            <cfset thumb = {}>
            <cfset colors = {}>
            
        </cfif>
     
        <cfset assetInfo = {"details":details, "thumb":thumb, "colors":colors}>
       
        <cfreturn assetInfo>
        
    </cffunction>
    
    
    
    <!--- Image Object Container --->
    <cffunction	name="imageContainerModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
		<cfset theObjModel = structNew()>
        
        <cfset structAppend(theObjModel,{"url":{}})>
        <!--- <cfset structAppend(theObjModel.url,{"mdpi":""})> --->
        <cfset structAppend(theObjModel.url,{"xdpi":""})>
        
        <cfset structAppend(theObjModel,{"width":0})>
        <cfset structAppend(theObjModel,{"height":0})>
        <cfset structAppend(theObjModel,{"ratio":0})>
                
        <cfreturn theObjModel>
        
    </cffunction>    
    
    
    <!--- Object Models --->
    
    <!--- DETAILS --->
    <cffunction	name="detailsModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
		<cfset theObjModel = structNew()>
        
        <cfset structAppend(theObjModel,{"details":{}})>
        
        <cfset structAppend(theObjModel.details,{"title":""})>
        <cfset structAppend(theObjModel.details,{"titleColor":""})>
        
        <cfset structAppend(theObjModel.details,{"subtitle":""})>
        <cfset structAppend(theObjModel.details,{"subtitleColor":""})>
        
        <cfset structAppend(theObjModel.details,{"description":""})>
        <cfset structAppend(theObjModel.details,{"descriptionColor":""})>
        
        <cfset structAppend(theObjModel.details,{"other":""})>
		
        <cfif NOT structIsEmpty(objectData)>
            
            <cfloop collection="#theObjModel#" item="theKey"></cfloop>
            
            <cfset theObjModel[theKey].title = objectData.title>
            
            <cfif objectData.titleColor NEQ ''>
            
                <cfset theObjModel[theKey].titleColor = objectData.titleColor>

            </cfif>
            
            <cfset theObjModel[theKey].subtitle = objectData.subtitle>
            
            <cfif objectData.subtitleColor NEQ ''>
                
                <cfset theObjModel[theKey].subtitleColor = objectData.subtitleColor>

            </cfif>
            
            <cfset theObjModel[theKey].description = objectData.description>
            
            <cfif objectData.descriptionColor NEQ ''>

                <cfset theObjModel[theKey].descriptionColor = objectData.descriptionColor>

            </cfif>
            
            <cfif IsJSON(objectData.other)>
			
				<cfset otherData = deserializeJson(objectData.other)>
                <cfset objectData.other = otherData>
            
            </cfif>
            
            <cfset theObjModel[theKey].other = objectData.other>
            
        </cfif>
         
        <cfreturn theObjModel>
      
    </cffunction>
    
    
    
    <!--- THUMBS --->
    <cffunction	name="thumbsModel" access="public" returntype="struct" output="true">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
  
    	<cfset theObjModel = structNew()>
                                                                                                                  
        <cfinvoke component="ObjectModels" method="imageContainerModel" returnvariable="theImageModel" />
        <cfset structAppend(theObjModel,{"thumb":theImageModel})>
        
        <cfif NOT structIsEmpty(objectData)>

            <cfif objectData.thumbnail NEQ ''>

                <cfif objectData.asset_id NEQ ''>
                    <cfset assetID = objectData.asset_id>
                <cfelse>
                    <cfset assetID = 0>
                </cfif>
                
                <!--- Build Asset Path --->
                <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                    <cfinvokeargument name="appID" value="#objectData.app_id#"/>
                    <cfinvokeargument name="server" value="yes"/>
                </cfinvoke>
     
                <!--- ASSET Thumb --->
                <cfif StructKeyExists(objectData,"assetType_id")>
                    
                    <cfif objectData.assetType_id NEQ ''>
         
                        <!--- Get Asset Type Path --->
                        <cfinvoke component="File" method="getAssetPath" returnvariable="assetTypePath">
                              <cfinvokeargument name="assetType" value="#objectData.assetType_id#"/>
                        </cfinvoke>
                        
                        <cfset path = 'assets/#assetTypePath#/'>
                    
                    <cfelse>
                        <cfset path = 'images/'>
                    </cfif>
                    
                <cfelse>
                    <cfset path = 'images/'>
                </cfif>
    
               <!--- <cfset assetPathNomral = assetPath & "#path#thumbs/nonretina/" & objectData.thumbnail> --->
                <cfset assetPathRetina = assetPath & "#path#thumbs/" & objectData.thumbnail>
          
                <cfloop collection="#theObjModel#" item="theKey"></cfloop>
                
                <cfif NOT isDefined('objectData.width')><cfset w = 0></cfif>
                <cfif NOT isDefined('objectData.height')><cfset h = 0></cfif>
                
                <cfif isDefined('objectData.width') AND isDefined('objectData.height')>
                    
                    <cfset w = objectData.width>
                    <cfset h = objectData.height>
                    
                    <cfif w IS ''><cfset w = 0></cfif>
                    <cfif h IS ''><cfset h = 0></cfif>
                    
                    <cfset r = 0>
                    
                    <cfif w NEQ 0 OR h NEQ 0>
                        <cfset r = w/h>
                    </cfif>
                   
                    <cfset theObjModel[theKey].ratio = r>
                    <cfset theObjModel[theKey].width = w>
                    <cfset theObjModel[theKey].height = h>
                    
                <cfelse>
                	<!--- if file exists get w,h --->
                </cfif>
                
				<!--- <cfset theObjModel[theKey].url.mdpi = assetPathNomral> --->
                <cfset theObjModel[theKey].url.xdpi = assetPathRetina>
          		
               <cfif assetPathRetina IS ''>
                	<cfset theObjModel = {"thumb":{}}>  
                </cfif>
                
                <!--- <cfset theObjModel = {"thumb":{}}>  ---> 
                
            <cfelse>
            	<cfset theObjModel = {"thumb":{}}>  
            </cfif>
			
          </cfif>

        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- COLORS --->
    <cffunction	name="colorsModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
    	<cfset theObjModel = structNew()>
        
        <cfinvoke component="ObjectModels" method="imageContainerModel" returnvariable="theImageModel" />
        
        <cfset structAppend(theObjModel,{"colors":{}})>
		<cfset structAppend(theObjModel.colors,{"foreColor":''})>
        <cfset structAppend(theObjModel.colors,{"backColor":''})>
        <cfset structAppend(theObjModel.colors,{"otherColor":''})>
         
        <cfset structAppend(theObjModel.colors,{"background":[]})>
     
        <cfif NOT structIsEmpty(objectData)>
            
            <cfloop collection="#theObjModel#" item="theKey"></cfloop>

        	<cfset theObjModel[theKey].foreColor = objectData.forecolor>
            <cfset theObjModel[theKey].backColor = objectData.backcolor>
            
            <cfif isDefined('objectData.othercolor')> 
            	<cfset theObjModel[theKey].othercolor = objectData.othercolor>
    		</cfif>
            
            <cfif objectData.displayAsset_id GT 0>
                
                <cfinvoke component="Content" method="getAssetContent" returnvariable="displayAssetData">
                    <cfinvokeargument name="assetID" value="#objectData.displayAsset_id#"/>
                </cfinvoke>
           		
                <cfset arrayAppend(theObjModel[theKey].background, displayAssetData)>
     		
            </cfif>

			<!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
            <cfif arrayLen(theObjModel[theKey].background) IS 0>
            	<cfset structDelete(theObjModel[theKey],"background")>
            </cfif>
            
            <!--- <cfif NOT structKeyExists(theObjModel[theKey].background,'url')>
                <cfset structDelete(theObjModel[theKey],"background")>
            </cfif> --->
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- OPTIONS --->
    <cffunction	name="optionsModel" access="public" returntype="struct" output="false">

    	<cfset theObjModel = structNew()>
        
        <cfset structAppend(theObjModel,{"access":0})>
		<cfset structAppend(theObjModel,{"modified":0})>
        <cfset structAppend(theObjModel,{"sharable":0})>
        <cfset structAppend(theObjModel,{"cached":0})>
        
        <cfset structAppend(theObjModel,{"assetType":0})>
        <cfset structAppend(theObjModel,{"assetID":0})>
        
        <cfset structAppend(theObjModel,{"assets":[]})>
        <cfset structAppend(theObjModel,{"actions":[]})>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- BASIC --->
    <cffunction	name="basicModel" access="public" returntype="struct" output="false">

    	<cfset theObjModel = structNew()>
        
        <cfinvoke component="ObjectModels" method="optionsModel" returnvariable="optionsModel" />
        <cfinvoke component="ObjectModels" method="thumbsModel" returnvariable="thumbsModel" />
        <cfinvoke component="ObjectModels" method="colorsModel" returnvariable="colorsModel" />
        <cfinvoke component="ObjectModels" method="detailsModel" returnvariable="detailsModel" />
        
        <cfset structAppend(theObjModel,{"objectName":{}})>
        
        <cfset structAppend(theObjModel.objectName,detailsModel)>
		<cfset structAppend(theObjModel.objectName,thumbsModel)>
        <cfset structAppend(theObjModel.objectName,colorsModel)>
        <cfset structAppend(theObjModel.objectName,optionsModel)>
 
        <cfreturn theObjModel>
        
    </cffunction>
    
    <!--- 									MODELS										 --->
    
    <!--- IMAGE 1 --->
    <cffunction	name="imageModel" access="public" returntype="struct" output="true">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
                                                                                                                         
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"image":{}}>
        <cfset structAppend(theObjModel.image,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 1>
        
        <cfinvoke component="ObjectModels" method="imageContainerModel" returnvariable="theImageModel" />
        
        <cfset structAppend(theObjModel.image,{"image":theImageModel})>
        
        <cfset structAppend(theObjModel.image,{"dynamic":0})>
        <cfset structAppend(theObjModel.image,{"useScaling":0})>
        <cfset structAppend(theObjModel.image.image,{"offset":{"x":0,"y":0}})>
     
        <!--- Inject Data --->
                   
        <cfif NOT structIsEmpty(objectData)>

            <cfif structKeyExists(objectData,'width') AND structKeyExists(objectData,'height')>
            
				<cfset w = objectData.width>
                <cfset h = objectData.height>
                
                <cfif w IS ''><cfset w = 0></cfif>
                <cfif h IS ''><cfset h = 0></cfif>
                
                <cfif w IS 0 OR h IS 0>
                    <cfset r = 0>
                <cfelse>
                    <cfset r = w/h>
                </cfif>
            
				<cfset theObjModel[theKey].image.width = w>
                <cfset theObjModel[theKey].image.height = h>
                <cfset theObjModel[theKey].image.ratio = r>
            
            </cfif>

            <cfif structKeyExists(objectData,'webURL')>
                
                <cfif objectData.webURL NEQ ''>
                
					<cfset theObjModel[theKey].image.url.xdpi = objectData.webURL>
                  
                    <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecs">
                        <cfinvokeargument name="imageSrc" value="#objectData.webURL#"/>
                    </cfinvoke>
                    
                    <cfset w = imgSpecs.width>
                    <cfset h = imgSpecs.height>
                    
                    <cfif w IS 0 OR h IS 0>
                        <cfset r = 0>
                    <cfelse>
                        <cfset r = w/h>
                    </cfif>
                
                    <cfset theObjModel[theKey].image.width = w>
                    <cfset theObjModel[theKey].image.height = h>
                    <cfset theObjModel[theKey].image.ratio = r>
                
                <cfelse>
                
                	<!--- Get Image Paths for M and X dpi --->
                    <cfinvoke component="ObjectModels" method="buildImagePaths" returnvariable="imagePaths">
                        <cfinvokeargument name="assetID" value="#objectData.asset_id#">
                        <cfinvokeargument name="fileName" value="#objectData.url#">
                    </cfinvoke>
    
                    <cfif isDefined('imagePaths.xdpi')>
                        <cfset theObjModel[theKey].image.url.xdpi = imagePaths.xdpi>
                    </cfif>
                     <cfif isDefined('imagePaths.mdpi')>
                        <cfset theObjModel[theKey].image.url.mdpi = imagePaths.mdpi>
                    </cfif> 
                
               	</cfif>
                
            <cfelse>
              																														
                <!--- Get Image Paths for M and X dpi --->
                <cfinvoke component="ObjectModels" method="buildImagePaths" returnvariable="imagePaths">
                    <cfinvokeargument name="assetID" value="#objectData.asset_id#">
                    <cfinvokeargument name="fileName" value="#objectData.url#">
                </cfinvoke>

                <cfif isDefined('imagePaths.xdpi')>
                    <cfset theObjModel[theKey].image.url.xdpi = imagePaths.xdpi>
                </cfif>
                 <cfif isDefined('imagePaths.mdpi')>
                    <cfset theObjModel[theKey].image.url.mdpi = imagePaths.mdpi>
                </cfif> 
                
            </cfif>
            
             <!--- dynamic delete --->
            <cfif structKeyExists(objectData,'dynamic')>
				<cfif objectData.dynamic IS 1>
                    <cfset theObjModel[theKey].dynamic = objectData.dynamic>
                <cfelse>
                    <cfset structDelete(theObjModel[theKey],'dynamic')>
                </cfif>
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'dynamic')>
            </cfif>
            
            <!--- useScaling delete --->
            <cfif structKeyExists(objectData,'useScaling')>
            
				<cfif objectData.useScaling IS ''>
                    <cfset objectData.useScaling = 0>
                </cfif>
            
				<cfif objectData.useScaling IS 1>
                    <cfset theObjModel[theKey].useScaling = 1> 
                <cfelse>
                    <cfset structDelete(theObjModel[theKey],'useScaling')>
                </cfif>
                
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'useScaling')>
            </cfif>
            
            <!--- offset delete --->
            <cfif structKeyExists(objectData,'offset_x') OR structKeyExists(objectData,'offset_y')>
				<cfif objectData.offset_x IS 0 AND objectData.offset_y IS 0>
                    <cfset structDelete(theObjModel[theKey].image,'offset')>
                <cfelse>
                    <cfset theObjModel[theKey].image.offset.x = objectData.offset_x>
                    <cfset theObjModel[theKey].image.offset.y = objectData.offset_y>
                </cfif>
            <cfelse>
            	<cfset structDelete(theObjModel[theKey].image,'offset')>
            </cfif>       
                                                                                                         
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
         
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
            </cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>

            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
                                                                                                                     
        <cfreturn theObjModel>
        
    </cffunction>


    <!--- VIDEO 2 --->
    <cffunction	name="videoModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
      
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"video":{}}>
        <cfset structAppend(theObjModel.video,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 2>
        
         <!--- <cfset structAppend(theObjModel.video,{"width":0, "height":0, "ratio":0})>  --->
        
        <cfinvoke component="ObjectModels" method="imageContainerModel" returnvariable="theImageModel" />
        
		<cfset structAppend(theObjModel.video,{"video":Duplicate(theImageModel)})>
        <cfset structAppend(theObjModel.video.video,{"modified":0})>
        
        <cfset structAppend(theObjModel.video,{"placeholder":Duplicate(theImageModel)})>
        
        <cfset structAppend(theObjModel.video,{"controls":{'playpause':0,'rewind':0,'zoom':0,'seek':0, "speed":0}})>
        
         <cfset structAppend(theObjModel.video,{"speedControl":0})> 
        <cfset structAppend(theObjModel.video,{"loop":0})>
        <cfset structAppend(theObjModel.video,{"autoplay":0})>
        <cfset structAppend(theObjModel.video,{"release":0})>
        <cfset structAppend(theObjModel.video,{"interactive":0})>
        <cfset structAppend(theObjModel.video,{"pan":0})>
		<cfset structAppend(theObjModel.video,{"container":{'x':0, 'y':0, 'width':0, 'height':0}})>
    	
        <!--- <cfset structAppend(theObjModel.video,{"width":0})>
        <cfset structAppend(theObjModel.video,{"height":0})>
        <cfset structAppend(theObjModel.video,{"ratio":0})> --->
        
        <cfset structAppend(theObjModel.video,{"dynamic":0})>
      
        <!--- Inject Data --->
        																															  
					
        <cfif NOT structIsEmpty(objectData)>
        
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
		<cfset addDateModified = false>

			<!--- Web URL --->
          <cfset webURL = false>
           <cfif isDefined('objectData.webURL')>
            	<cfif objectData.webURL NEQ ''>
            		<cfset webURL = true>
			   </cfif>
           </cfif>
           
            <cfif webURL>
                <cfset theObjModel[theKey].video.url.xdpi = objectData.webURL>
				
                <cfif isDefined('objectData.modified')>
					<cfif objectData.modified NEQ ''>
                    	<cfset theObjModel[theKey].video.modified = objectData.modified>
                    <cfelse>
                    	<cfset addDateModified = true>
                    	<cfset theObjModel[theKey].url.modified = curDate>
                    </cfif>
                <cfelse>
                	<cfset addDateModified = true>
                	<cfset theObjModel[theKey].url.modified = curDate>
                </cfif>  
            <cfelse>
            
            <!--- Asset URL --->
															        																	       
                <!--- Get Video Paths --->
                <cfinvoke component="ObjectModels" method="buildImagePaths" returnvariable="imagePaths">
                    <cfinvokeargument name="assetID" value="#objectData.asset_id#">
                    <cfinvokeargument name="fileName" value="#objectData.url#">
                </cfinvoke>
              
                <cfif isDefined('imagePaths.xdpi')>
                	<cfif fileExists(imagePaths.xdpi)>
                    	<cfset theObjModel[theKey].video.url.xdpi = imagePaths.xdpi>
                    </cfif>
                </cfif>
                 <cfif isDefined('imagePaths.mdpi')>
                	<cfif fileExists(imagePaths.mdpi)>
                    	<cfset theObjModel[theKey].video.url.mdpi = imagePaths.mdpi>
                    </cfif>
                </cfif> 
               
                <cfif isDefined('objectData.modified')>
					<cfif objectData.modified NEQ ''>
                    	<cfset theObjModel[theKey].video.modified = objectData.modified>
                    <cfelse>
                    	<cfset addDateModified = true>
                    	<cfset theObjModel[theKey].url.modified = curDate>
                    </cfif>
                <cfelse>
                	<cfset addDateModified = true>
                	<cfset theObjModel[theKey].url.modified = curDate>
                </cfif>
                
                <cfif objectData.ratio NEQ '' OR objectData.ratio LTE 1>
					<cfset theObjModel[theKey].video.width = objectData.width>
                    <cfset theObjModel[theKey].video.height = objectData.height>
                    <cfset theObjModel[theKey].video.ratio = objectData.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].video,'width')>
					<cfset structDelete(theObjModel[theKey].video,'height')>
                    <cfset structDelete(theObjModel[theKey].video,'ratio')>
                </cfif>
          
            </cfif>
        
            <cfif structKeyExists(objectData,'placeholder')>
            
                <!--- Get Video Paths --->
                <cfinvoke component="ObjectModels" method="buildImagePaths" returnvariable="imagePlaceholderPaths">
                    <cfinvokeargument name="assetID" value="#objectData.asset_id#">
                    <cfinvokeargument name="fileName" value="#objectData.placeholder#">
                </cfinvoke>
         	
                <cfif objectData.placeholder NEQ ''>
                
					<!--- Placeholder --->
                    <cfif isDefined('imagePlaceholderPaths.xdpi')>
                        <cfset theObjModel[theKey].placeholder.url.xdpi = imagePlaceholderPaths.xdpi>
                    <cfelse>
                        <cfset structDelete(theObjModel[theKey].placeholder,'xdpi')>
                    </cfif>
                    
                     <cfif isDefined('imagePlaceholderPaths.mdpi')>
                        <cfset theObjModel[theKey].placeholder.url.mdpi = imagePlaceholderPaths.mdpi>
                    </cfif> 
                   	
                    <cfset w = objectData.width>
					<cfset h = objectData.height>
                
                    <cfif w IS ''><cfset w = 0></cfif>
                    <cfif h IS ''><cfset h = 0></cfif>
                    
                    <cfset theObjModel[theKey].placeholder.width = w>
                    <cfset theObjModel[theKey].placeholder.height = h>
                    
                    <cfif w IS 0 OR h IS 0>
                    	<cfset r = 0>
                    <cfelse>
                        <cfset r = w/h>
                    </cfif>
                    
                    <cfset theObjModel[theKey].placeholder.ratio = r>
     
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].placeholder,'width')>
                    <cfset structDelete(theObjModel[theKey].placeholder,'height')>
                    <cfset structDelete(theObjModel[theKey].placeholder,'ratio')>
                </cfif>
            
            </cfif>
            
            <!--- dynamic delete --->
            <cfif structKeyExists(objectData,'dynamic')>
				<cfif objectData.dynamic IS 1>
                    <cfset theObjModel[theKey].dynamic = objectData.dynamic>
                <cfelse>
                    <cfset structDelete(theObjModel[theKey],'dynamic')>
                </cfif>
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'dynamic')>
            </cfif>
           
            <!--- Size and Ratio --->
            <cfif structKeyExists(objectData,'width') AND structKeyExists(objectData,'height')>
            
				<cfset w = objectData.width>
                <cfset h = objectData.height>
            
				<cfif w IS ''><cfset w = 0></cfif>
                <cfif h IS ''><cfset h = 0></cfif>
                
                <cfif w IS 0 OR h IS 0>
                    <cfset r = 0>
                <cfelse>
                    <cfset r = w/h>
                </cfif>
                
                <cfset theObjModel[theKey].video.width = w>
                <cfset theObjModel[theKey].video.height = h>
                <cfset theObjModel[theKey].video.ratio = r>
            
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'width')>
                <cfset structDelete(theObjModel[theKey],'height')>
                <cfset structDelete(theObjModel[theKey],'ratio')>
            </cfif> 
            
            <!--- if curDate added, update DB for Asset --->
            <cfif addDateModified>
            	<cfquery name="info">
                      UPDATE VideoAssets
                      SET modified = #curDate#
                      WHERE	 asset_id = #objectData.asset_id#
                 </cfquery>
          
            </cfif>

          
            <!--- video options --->
            <cfif objectData.autoplay>
                <cfset theObjModel[theKey].autoplay = objectData.autoplay>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'autoplay')>
            </cfif>
            
            <cfif objectData.loop>
                <cfset theObjModel[theKey].loop = objectData.loop>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'loop')>
            </cfif>
            
            <cfif objectData.loop>
                <cfset theObjModel[theKey].pan = objectData.pan>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'pan')>
            </cfif>
            
            <!--- <cfif objectData.speedControls>
                <cfset theObjModel[theKey].speedControl = objectData.speedControls>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'speedControl')> 
            </cfif> --->
            
            <cfif objectData.releaseControls>
                <cfset theObjModel[theKey].release = objectData.releaseControls>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'release')>
            </cfif>
			
            <cfif objectData.playpause>
				 <cfset theObjModel[theKey].controls.playpause = objectData.playpause>
             </cfif>
            
            <!--- video controls --->
            <cfif objectData.playpause>
                
                <cfif objectData.rewind>
                    <cfset theObjModel[theKey].controls.rewind = objectData.rewind>
                </cfif>
                
                <cfif objectData.zoom>
                    <cfset theObjModel[theKey].controls.zoom = objectData.zoom>
                </cfif>
                
                <cfif objectData.scrubbar>
                    <cfset theObjModel[theKey].controls.seek = objectData.scrubbar>
                </cfif>
                
                <cfif objectData.speedControls>
                    <cfset theObjModel[theKey].controls.speed = objectData.speedControls>
                </cfif>
                
                <cfif objectData.playpause>
                    <cfset theObjModel[theKey].controls.playpause = objectData.playpause>
                </cfif>
            
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'controls')>
            </cfif>
      
            <!--- Interactive, Remove Controls --->
            <cfif objectData.interactive IS 1>
                <cfset theObjModel[theKey].interactive = objectData.interactive>
                
                <cfif objectData.container NEQ ''>
                	<cfset theContainer = listToArray(objectData.container)>
                	<cfset theObjModel[theKey].container = {'x':theContainer[1], 'y':theContainer[2], 'width':theContainer[3], 'height':theContainer[4]}>
				</cfif>
                
                <cfset structDelete(theObjModel[theKey],'controls')>
                
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'interactive')>
                <cfset structDelete(theObjModel[theKey],'container')>
            </cfif>
   
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
			</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>

            <cfset structAppend(theObjModel[theKey],assetData)>
            																					
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
         
        </cfif>
  
        <cfreturn theObjModel>
        
    </cffunction>
    
    
	<!--- DOCUMENTS 3 --->
    <cffunction	name="documentModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"pdf":{}}>
        <cfset structAppend(theObjModel.pdf,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 3>

        <cfset structAppend(theObjModel.pdf,{"url":""})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    
			<!--- Build PDF Document Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>

            <!--- URL --->
            <cfif objectData.webURL NEQ ''>
                <cfset theObjModel[theKey].url = objectData.webURL>
            <cfelseif objectData.url NEQ '' AND fileExists(assetPath & objectData.url)>
                <cfset theObjModel[theKey].url = assetPath & objectData.url>
            </cfif>
            
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
            </cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- URL LINK 4 --->
    <cffunction	name="urlModel" access="public" returntype="struct" output="true">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"url":{}}>
        <cfset structAppend(theObjModel.url,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 4>
        
        <cfset structAppend(theObjModel.url,{"url":""})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    	
			<cfset theObjModel[theKey].url = objectData.url>
            
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
            </cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>

        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- LOCATION GPS 5 --->
    <cffunction	name="locationtModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"location":{}}>
        <cfset structAppend(theObjModel.location,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 5>
        
        <cfset structAppend(theObjModel.location,{"gpsAlt":0})>
        <cfset structAppend(theObjModel.location,{"gpsLong":0})>
        <cfset structAppend(theObjModel.location,{"gpsLatt":0})>
        <cfset structAppend(theObjModel.location,{"radius":0})>
        <cfset structAppend(theObjModel.location,{"options":{'zoom':0, 'styles':''}})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    
			<cfset theObjModel[theKey].gpsAlt = objectData.gps_alt>
            <cfset theObjModel[theKey].gpsLatt = objectData.gps_latt>
            <cfset theObjModel[theKey].gpsLong = objectData.gps_long>
            <cfset theObjModel[theKey].radius = objectData.radius>
            
            <cfif objectData.zoom GT 0 OR trim(objectData.style) NEQ ''>
				<cfset theObjModel[theKey].options.zoom = objectData.zoom><cfdump var="#objectData.style#">
				
				<cftry> 
					<!--- Try Parse ---> 
					<cfset stylesObject = DeserializeJSON(objectData.style)>
					<cfset theObjModel[theKey].options.styles = stylesObject>

					<cfcatch type = "any"> 
					<!--- the message to display ---> 
						<!--- <cfdump var="#objectData.style#"><cfabort> --->
					</cfcatch> 
				</cftry>
				
				
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'options')>
			</cfif>
        
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
         
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- 3DMODEL 6 --->
    <cffunction	name="model3DModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"model3D":{}}>
        <cfset structAppend(theObjModel.model3D,theObjModelStuct.objectName)>

		<cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 6>

        <cfset model = {"scale":1}>
		<cfset structAppend(model,{"rotation":{"x":0,"y":0,"z":0}})>
        <cfset structAppend(model,{"url":{"android":"","ios":"","osx":"","windows":""}})>
        <cfset structAppend(model,{"position":{"x":0,"y":0,"z":0}})>
        <cfset structAppend(model,{"modified":0})>
		
		<cfset camera = {"fov":45}>
        <cfset structAppend(camera,{"position":{"x":0,"y":0,"z":0}})>
        <cfset structAppend(camera,{"target":{"x":0,"y":0,"z":0}})>
		<cfset structAppend(camera,{"limits": {"zoomMin":0, "zoomMax":0, "panMin":0, "panMax":0, "rotationMin":0, "rotationMax":0}})>
        
        <cfset structAppend(camera,{"pan":0,"zoom":0, "rotation":0})>
        
        <cfset structAppend(theObjModel.model3D,{"model":model})>
        <cfset structAppend(theObjModel.model3D,{"camera":camera})>
        	
        <!--- Inject Data --->  
        <cfif NOT structIsEmpty(objectData)>
                       	
			<!--- Build Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
       
            <cfset camLimits = {'zoomMin':objectData.camMin, 'zoomMax':objectData.camMax, 'rotationMin':objectData.panMin, 'rotationMax':objectData.panMax, 'panMin':objectData.tiltMin, 'panMax':objectData.tiltMax}>
            <cfset camPosition = {'x':objectData.camPos_x, 'y':objectData.camPos_y, 'z':objectData.camPos_z}>
            <cfset camTarget = {'x':objectData.camTarget_x, 'y':objectData.camTarget_y, 'z':objectData.camTarget_z}>
            
            <!--- Camera --->
            <cfset theObjModel[theKey].camera.limits = camLimits>
            <cfset theObjModel[theKey].camera.position = camPosition>
            <cfset theObjModel[theKey].camera.target = camTarget>
            
            <cfset theObjModel[theKey].camera.pan = objectData.camPan>
            <cfset theObjModel[theKey].camera.rotation = objectData.camRotation>
            <cfset theObjModel[theKey].camera.zoom = objectData.camZoom>

            <cfset modelPos = {'x':objectData.loc_x, 'y':objectData.loc_y, 'z':objectData.loc_z}>
            <cfset modelRot = {'x':objectData.rot_x, 'y':objectData.rot_y, 'z':objectData.rot_z}>
            
            <!--- Model --->
            <cfset modelURL = structNew()>
            
            <cfif objectData.url NEQ ''>
                <cfset structAppend(modelURL,{'ios':assetPath & objectData.url})>
            </cfif>
            
            <cfif objectData.url_android NEQ ''>
                <cfset structAppend(modelURL,{'android':assetPath & objectData.url_android})>
            </cfif>
            
            <cfif objectData.url_osx NEQ ''>
                <cfset structAppend(modelURL,{'osx':assetPath & objectData.url_osx})>
            </cfif>
            
            <cfif objectData.url_windows NEQ ''>
                <cfset structAppend(modelURL,{'windows':assetPath & objectData.url_windows})>
            </cfif>
            
            <cfset model = {'position':modelPos, 'rotation':modelRot, 'scale':objectData.scale, 'url':modelURL, 'modified':objectData.modifiedModel}>
            
            <cfset theObjModel[theKey].model = model>
            
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
   			</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    
    <!--- Model3D Basic Model --->
    <cffunction	name="model3DBasicModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"model3D":{}}>
        <cfset structAppend(theObjModel.model3D,theObjModelStuct.objectName)>

		<cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 25>

        <cfset model = {"scale":1}>
		<cfset structAppend(model,{"rotation":{"x":0,"y":0,"z":0}})>
        <cfset structAppend(model,{"url":{"android":"","ios":"","osx":"","windows":""}})>
        <cfset structAppend(model,{"position":{"x":0,"y":0,"z":0}})>
        <cfset structAppend(model,{"objectRef":""})>
        <cfset structAppend(model,{"ar":0})>
        <cfset structAppend(model,{"modified":0})>
        
        <cfset structAppend(theObjModel.model3D,{"model":model})>
        	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                       	
			<!--- Build Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>       
            
            <cfset modelPos = {'x':objectData.loc_x, 'y':objectData.loc_y, 'z':objectData.loc_z}>
            <cfset modelRot = {'x':objectData.rot_x, 'y':objectData.rot_y, 'z':objectData.rot_z}>
            
            <!--- Model --->
            <cfset modelURL = structNew()>
            
            <cfif objectData.url_ios NEQ ''>
                <cfset structAppend(modelURL,{'ios':assetPath & objectData.url_ios})>
            </cfif>
            
            <cfif objectData.url_android NEQ ''>
                <cfset structAppend(modelURL,{'android':assetPath & objectData.url_android})>
            </cfif>
            
            <cfif objectData.url_osx NEQ ''>
                <cfset structAppend(modelURL,{'osx':assetPath & objectData.url_osx})>
            </cfif>
            
            <cfif objectData.url_windows NEQ ''>
                <cfset structAppend(modelURL,{'windows':assetPath & objectData.url_windows})>
            </cfif>
            
            <cfset model = {'position':modelPos, 'rotation':modelRot, 'scale':objectData.scale, 'url':modelURL, 'modified':objectData.modifiedModel, 'objectRef': objectData.objectRef, 'ar': objectData.includeAR}>
            
            <cfset theObjModel[theKey].model = model>
            
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
   			</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
            
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
      
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- Scene Model3D --->
    <cffunction	name="SceneModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
   
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"scene3D":{}}>
        <cfset structAppend(theObjModel.scene3D,theObjModelStuct.objectName)>

		<cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 23>

		<cfset structAppend(theObjModel[theKey],{"camera":{"zoomMin":0,"zoomMax":0, "zoomStart":0, "zoom":0, "positionSpeed":0, "lookAtSpeed":0, "rotationHorizontal":0, "rotationVertical":0, "heightOffset":0}})>
   	
   		<cfset structAppend(theObjModel[theKey].camera, {"cameraTarget":{"x":0, "y":0, "z":0}})>
   		<cfset structAppend(theObjModel[theKey].camera, {"cameraLookAt":{"x":0, "y":0, "z":0}})>
    	
    	<cfset structAppend(theObjModel[theKey].camera, {"other":{"glow":0, "farClippingPlane":0}})>
     	
      	<cfset CameraProcessing = {"processing":{}}>
      	
      	<cfset Misc = {"antiAliasing":0, "screenReflection":0, "motionBlur":0}>
      	<cfset AmbientOccl = {"intensity":0, "radius":0}>
      	<cfset DepthofField = {"focusDistance":10, "aperture":5.6, "focalLength":50}>
      	<cfset Bloom = {"bloomSoftKnee":0.5, "bloomRadius":4, "bloomAntiFlicker":0, "bloomIntensity":0}>
      	<cfset Vignette = {"vignetteIntensity":0.45, "vignetteSmoothness":0.2, "vignetteRoundness":1, "vignetteColor":""}>
      	<cfset ColorGrading = {"gradingSaturation":1, "gradingContrast":1, "gradingMixer":{"r":0, "g":0, "b":0}}>
      	
      	<cfset structAppend(CameraProcessing.processing, Misc)>
      	<cfset structAppend(CameraProcessing.processing, {"ambientOcclusion":AmbientOccl})>
      	<cfset structAppend(CameraProcessing.processing, {"depthofField":DepthofField})>
      	<cfset structAppend(CameraProcessing.processing, {"bloom":Bloom})>
      	<cfset structAppend(CameraProcessing.processing, {"vignette":Vignette})>
      	<cfset structAppend(CameraProcessing.processing, {"colorGrading":ColorGrading})>
      	<cfset structAppend(CameraProcessing, {"renderingPath":0})>
      	
        <cfset structAppend(theObjModel[theKey].camera, CameraProcessing)>
        
        <!--- Augmented Reality --->
        <cfset ar = {"defaultScale": 1, "scaleMin":0, "scaleMax":0, "rotation":0}>
        <cfset structAppend(theObjModel[theKey], {"ar": ar})>
        
        <cfset structAppend(theObjModel[theKey],{"environment":{'ambientLightColor':0, "exposure":0, "lightingIntensity":0, "reflection":0,"model3D": [], "skyType": 0, "sky": {},"fog":{"color":"", "density":0, "fogStart":0, "fogEnd":0}}})>
        
        <cfset structAppend(theObjModel[theKey],{"light":{"color":"","shadowStrength":0,"timeOfDay":0, "intensity":0, "shadowType":0, "indirectMultiplier":0, "lightRotation":{"x":0, "y":0}}})>
       	
        <!--- Inject Data --->
       
        <cfif NOT structIsEmpty(objectData)>
        	
			<!--- Build Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>       
            
            <!--- Camera --->
            <cfif structKeyExists(objectData, "glow")>
            	<cfset theObjModel[theKey].camera.other.glow = objectData.glow>
			</cfif>
         	
          	<cfif structKeyExists(objectData, "farClippingPlane")>
           		
           		<cfif objectData.farClippingPlane IS ''>
           			<cfset objectData.farClippingPlane = 1000>
				</cfif>
           	
            	<cfset theObjModel[theKey].camera.other.farClippingPlane = objectData.farClippingPlane>
            	
			</cfif>
           
            <cfif structKeyExists(objectData, "zoomMin")>
            	<cfset theObjModel[theKey].camera.zoomMin = objectData.zoomMin>
			</cfif>
            <cfif structKeyExists(objectData, "zoomMax")>
            	<cfset theObjModel[theKey].camera.zoomMax = objectData.zoomMax>
            </cfif>
            <cfif structKeyExists(objectData, "zoomStart")>
            	<cfset theObjModel[theKey].camera.zoomStart = objectData.zoomStart>
            </cfif>
            <cfif structKeyExists(objectData, "zoom")>
            	<cfset theObjModel[theKey].camera.zoom = objectData.zoom>
            </cfif>
			
           	<cfif structKeyExists(objectData, "rotationHorizontal")>
            	<cfset theObjModel[theKey].camera.rotationHorizontal = objectData.rotationHorizontal>
            </cfif>
            <cfif structKeyExists(objectData, "rotationVertical")>
            	<cfset theObjModel[theKey].camera.rotationVertical = objectData.rotationVertical>
            </cfif>
            
            <cfif structKeyExists(objectData, "cameraTarget_X") OR structKeyExists(objectData, "cameraTarget_Y") OR structKeyExists(objectData, "cameraTarget_Z")>
            	<cfset theObjModel[theKey].camera.cameraTarget.x = objectData.cameraTarget_X>
            	<cfset theObjModel[theKey].camera.cameraTarget.y = objectData.cameraTarget_Y>
            	<cfset theObjModel[theKey].camera.cameraTarget.z = objectData.cameraTarget_Z>
            </cfif>
            
            <cfif structKeyExists(objectData, "cameraLookAt_X") OR structKeyExists(objectData, "cameraLookAt_Y") OR structKeyExists(objectData, "cameraLookAt_Z")>
            	<cfset theObjModel[theKey].camera.cameraLookAt.x = objectData.cameraLookAt_X>
            	<cfset theObjModel[theKey].camera.cameraLookAt.y = objectData.cameraLookAt_Y>
            	<cfset theObjModel[theKey].camera.cameraLookAt.z = objectData.cameraLookAt_Z>
            </cfif>
            
            <cfif structKeyExists(objectData, "positionSpeed")>
            	<cfset theObjModel[theKey].camera.positionSpeed = objectData.positionSpeed>
            </cfif>
            <cfif structKeyExists(objectData, "lookAtSpeed")>
            	<cfset theObjModel[theKey].camera.lookAtSpeed = objectData.lookAtSpeed>
			</cfif>
            <cfif structKeyExists(objectData, "renderingPath")>
            	<cfset theObjModel[theKey].camera.renderingPath = objectData.renderingPath>
			</cfif>

           <cfif structKeyExists(objectData, "heightOffset")>
            	<cfset theObjModel[theKey].camera.heightOffset = objectData.heightOffset>
			</cfif>
           
           <!--- cameraProcessing --->
           
           <!--- Color Grading --->
           <cfif structKeyExists(objectData, "colorGrading")>
           
           		<cfset colorGradingOptions = {}>
           		
          		<cfif objectData.colorGrading>
          			
          			<cfif structKeyExists(objectData, "gradingSaturation")>
						<cfset structAppend(colorGradingOptions, {"gradingSaturation": objectData.gradingSaturation})>
					</cfif>
          			<cfif structKeyExists(objectData, "gradingContrast")>
						<cfset structAppend(colorGradingOptions, {"gradingContrast": objectData.gradingContrast})>
					</cfif>
					
         			<cfset structAppend(colorGradingOptions, {"gradingMixer": {"r":objectData.gradingMixerR, "g":objectData.gradingMixerG, "b":objectData.gradingMixerB}})>
          			
           			<cfset theObjModel[theKey].camera.processing.colorGrading = colorGradingOptions>
           	
           		<cfelse>
           			<cfset StructDelete(theObjModel[theKey].camera.processing,"colorGrading")>
				</cfif>

           <cfelse>
           		<cfset StructDelete(theObjModel[theKey].camera.processing,"colorGrading")>
           </cfif>
           
           <!--- DepthOfField --->
           <cfif structKeyExists(objectData, "depthOfField")>
           
           		<cfset depthOfFieldOptions = {}>
           		
          		<cfif objectData.vignette>
          			
          			<cfif structKeyExists(objectData, "focusDistance")>
						<cfset structAppend(depthOfFieldOptions, {"focusDistance": objectData.focusDistance})>
					</cfif>
          			<cfif structKeyExists(objectData, "aperture")>
						<cfset structAppend(depthOfFieldOptions, {"aperture": objectData.aperture})>
					</cfif>
          			<cfif structKeyExists(objectData, "focalLength")>
						<cfset structAppend(depthOfFieldOptions, {"focalLength": objectData.focalLength})>
					</cfif>
          			
           			<cfset theObjModel[theKey].camera.processing.depthOfField = depthOfFieldOptions>
           	
           		<cfelse>
           			<cfset StructDelete(theObjModel[theKey].camera.processing,"depthOfField")>
				</cfif>

           <cfelse>
           		<cfset StructDelete(theObjModel[theKey].camera.processing,"depthOfField")>
           </cfif>
           
           <!--- AmbientOcclusion --->
           <cfif structKeyExists(objectData, "ambientOcclusion")>
           		
           		<cfset ambientOptions = {}>
           		
           		<cfif objectData.ambientOcclusion>
           		
					<cfif structKeyExists(objectData, "occl_intensity")>
						<cfset structAppend(ambientOptions, {"intensity": objectData.occl_intensity})>
					</cfif>
					<cfif structKeyExists(objectData, "occl_radius")>
						<cfset structAppend(ambientOptions, {"radius": objectData.occl_radius})>
					</cfif>
          		
          		<cfset theObjModel[theKey].camera.processing.ambientOcclusion = ambientOptions>
		  
			   <cfelse>
				  <cfset StructDelete(theObjModel[theKey].camera.processing,"ambientOcclusion")>
			   </cfif>
           
           <cfelse>
           		<cfset StructDelete(theObjModel[theKey].camera.processing,"ambientOcclusion")>
		   </cfif>
          
          <!--- Bloom --->
           <cfif structKeyExists(objectData, "bloom")>
           
           		<cfset bloomOptions = {}>
           		
          		<cfif objectData.bloom>
          			
          			<cfif structKeyExists(objectData, "bloomIntensity")>
						<cfset structAppend(bloomOptions, {"bloomIntensity": objectData.bloomIntensity})>
					</cfif>
          			<cfif structKeyExists(objectData, "bloomSoftKnee")>
						<cfset structAppend(bloomOptions, {"bloomSoftKnee": objectData.bloomSoftKnee})>
					</cfif>
          			<cfif structKeyExists(objectData, "bloomRadius")>
						<cfset structAppend(bloomOptions, {"bloomRadius": objectData.bloomRadius})>
					</cfif>
         			<cfif structKeyExists(objectData, "bloomAntiFlicker")>
						<cfset structAppend(bloomOptions, {"bloomAntiFlicker": objectData.bloomAntiFlicker})>
					</cfif>
          			
           			<cfset theObjModel[theKey].camera.processing.bloom = bloomOptions>
           	
           		<cfelse>
           			<cfset StructDelete(theObjModel[theKey].camera.processing,"bloom")>
				</cfif>

           <cfelse>
           		<cfset StructDelete(theObjModel[theKey].camera.processing,"bloom")>
           </cfif>
           
           <!--- Vignette --->
           <cfif structKeyExists(objectData, "vignette")>
           
           		<cfset vignetteOptions = {}>
           		
          		<cfif objectData.vignette>
          			
          			<cfif structKeyExists(objectData, "vignetteColor")>
						<cfset structAppend(vignetteOptions, {"vignetteColor": objectData.vignetteColor})>
					</cfif>
          			<cfif structKeyExists(objectData, "vignetteIntensity")>
						<cfset structAppend(vignetteOptions, {"vignetteIntensity": objectData.vignetteIntensity})>
					</cfif>
          			<cfif structKeyExists(objectData, "vignetteSmoothness")>
						<cfset structAppend(vignetteOptions, {"vignetteSmoothness": objectData.vignetteSmoothness})>
					</cfif>
         			<cfif structKeyExists(objectData, "vignetteRoundness")>
						<cfset structAppend(vignetteOptions, {"vignetteRoundness": objectData.vignetteRoundness})>
					</cfif>
          			
           			<cfset theObjModel[theKey].camera.processing.vignette = vignetteOptions>
           	
           		<cfelse>
           			<cfset StructDelete(theObjModel[theKey].camera.processing,"vignette")>
				</cfif>

           <cfelse>
           		<cfset StructDelete(theObjModel[theKey].camera.processing,"vignette")>
           </cfif>
			
          <!--- Misc --->
           <cfif structKeyExists(objectData, "antialias")>
			   <cfif objectData.antialias IS ""><cfset objectData.antialias = false></cfif>
           		<cfif objectData.antialias>
           			<cfset theObjModel[theKey].camera.processing.antiAliasing = objectData.antialias>
         		<cfelse>
         			<cfset StructDelete(theObjModel[theKey].camera.processing,"antiAliasing")>
          		</cfif>
           <cfelse>
           		<cfset StructDelete(theObjModel[theKey].camera.processing,"antiAliasing")>
           </cfif>
           
           <cfif structKeyExists(objectData, "screenReflection")>
           		<cfif objectData.ScreenReflection>
           			<cfset theObjModel[theKey].camera.processing.ScreenReflection = objectData.ScreenReflection>
           		<cfelse>
         			<cfset StructDelete(theObjModel[theKey].camera.processing,"screenReflection")>
          		</cfif>
           <cfelse>
           		<cfset StructDelete(theObjModel[theKey].camera.processing,"screenReflection")>
           </cfif>
           
           <cfif structKeyExists(objectData, "motionBlur")>
          		<cfif objectData.MotionBlur GT 0>
           			<cfset theObjModel[theKey].camera.processing.MotionBlur = objectData.MotionBlur>
           		<cfelse>
           			<cfset StructDelete(theObjModel[theKey].camera.processing,"motionBlur")>
				</cfif>
           <cfelse>
           		<cfset StructDelete(theObjModel[theKey].camera.processing,"motionBlur")>
           </cfif>

            <!--- Augmented Reality --->
            <cfif structKeyExists(objectData, "ar")>
				<cfif objectData.ar IS ''><cfset objectData.ar = 0></cfif>
				<cfif objectData.ar IS 1>
					<cfset theObjModel[theKey].ar.defaultScale = objectData.ar_scale>

					<cfif objectData.ar_scaleRange IS 1>
						<cfset theObjModel[theKey].ar.scaleMin = objectData.ar_scaleMin>
						<cfset theObjModel[theKey].ar.scaleMax = objectData.ar_scaleMax>
					<cfelse>
						<cfset structDelete(theObjModel[theKey].ar,"scaleMin")>
						<cfset structDelete(theObjModel[theKey].ar,"scaleMax")>
					</cfif>
					
					<cfset theObjModel[theKey].ar.rotation = objectData.ar_RotationRange>
					
          	 	<cfelse>
          	 		<cfset structDelete(theObjModel[theKey],"ar")>
           		</cfif>
           	<cfelse>
           		<cfset structDelete(theObjModel[theKey],"ar")>
            </cfif>
           
            <!--- Enviroment --->
            <cfif structKeyExists(objectData, "reflection")>
            	<cfset theObjModel[theKey].environment.reflection = objectData.reflection>
            </cfif>
           
          	
          	<cfif structKeyExists(objectData, "lightingIntensity")>
            	<cfset theObjModel[theKey].environment.lightingIntensity = objectData.lightingIntensity>
			</cfif>
          	 
          	 <cfif objectData.useFog>
          	 
				 <cfif structKeyExists(objectData, "fogColor")>
					<cfset theObjModel[theKey].environment.fog.color = objectData.fogColor>
				</cfif>

				<cfif structKeyExists(objectData, "fogDensity")>
					<cfset theObjModel[theKey].environment.fog.density = objectData.fogDensity>
				</cfif>

			  <cfif structKeyExists(objectData, "fogStart")>
					<cfset theObjModel[theKey].environment.fog.fogStart = objectData.fogStart>
				<cfelse>
					<cfset structDelete(theObjModel[theKey].fog,"fogStart")>
				</cfif>

			  <cfif structKeyExists(objectData, "fogEnd")>
					<cfset theObjModel[theKey].environment.fog.fogEnd = objectData.fogEnd>
				<cfelse>
					<cfset structDelete(theObjModel[theKey].fog,"fogEnd")>
				</cfif>
        	
        	<cfelse>
        		<cfset structDelete(theObjModel[theKey].environment,"fog")>
        	</cfif>
         
          <cfif structKeyExists(objectData, "ambientLightColor")>
            	<cfset theObjModel[theKey].environment.ambientLightColor = objectData.ambientLightColor>
			</cfif>
          
           <cfif structKeyExists(objectData, "exposure")>
            	<cfset theObjModel[theKey].environment.exposure = objectData.exposure>
			</cfif>
           
            <!--- Model Data --->
            <cfif structKeyExists(objectData, "model_id")>
				<cfinvoke  component="Content" method="getAssetContent" returnvariable="modelObj">
				  <cfinvokeargument name="assetID" value="#objectData.model_id#"/>
				</cfinvoke>
           
            	<cfset theObjModel[theKey].environment.model3D = [modelObj]>
            </cfif>
            
            <!--- Skybox Data --->
            <cfif structKeyExists(objectData, "skyboxType")>
            
            	<cfset theObjModel[theKey].environment.skyType = objectData.skyboxType>

				<cfif objectData.skyboxType IS 1 OR  objectData.skyboxType IS 3 OR objectData.skyboxType IS 4>

						<cfif structKeyExists(objectData,"bgColor")>
							<cfset theObjModel[theKey].environment.sky.color = objectData.bgColor>
						</cfif>
				</cfif>
  
				<cfif objectData.skyboxType IS 3 OR objectData.skyboxType IS 4 OR objectData.skyboxType IS 2>
						
						<!--- image asset OR 360 pano asset --->
						<cfif objectData.skyboxAsset_id GT 0>
							
							<cfinvoke  component="Content" method="getAssetContent" returnvariable="skyObj">
							  <cfinvokeargument name="assetID" value="#objectData.skyboxAsset_id#"/>
							</cfinvoke>

							<cfloop collection="#skyObj#" item="theKeyAsset"></cfloop>

							<cfif NOT structIsEmpty(skyObj)>
								<cfset structAppend(skyObj[theKeyAsset], {"name":theKeyAsset})>
								<cfset structAppend(theObjModel[theKey].environment,{ "skybox": skyObj[theKeyAsset]})>
							</cfif>
						
						</cfif>
						
						<cfif objectData.skyboxType IS 2><!--- image --->
							<cfset structAppend(theObjModel[theKey].environment,{ "skyRotation": objectData.imageRotation})>
						</cfif>

				</cfif>

				<cfif objectData.skyboxType IS 0>																		
					<cfset structDelete(theObjModel[theKey],"skybox")>
				</cfif>
          	
          	</cfif>
        	
            <!--- Light --->
            <cfif structKeyExists(objectData, "shadowIntensity")>
            	<cfset theObjModel[theKey].light.shadowStrength = objectData.shadowIntensity>
            </cfif>
            
            <cfif structKeyExists(objectData, "intensity")>
            	<cfset theObjModel[theKey].light.intensity = objectData.intensity>
            </cfif>
            <cfif structKeyExists(objectData, "shadowType")>
            	<cfset theObjModel[theKey].light.shadowType = objectData.shadowType>
            </cfif>
            <cfif structKeyExists(objectData, "indirectMultiplier")>
            	<cfset theObjModel[theKey].light.indirectMultiplier = objectData.indirectMultiplier>
            </cfif>
            <cfif structKeyExists(objectData, "lightRotationX") OR structKeyExists(objectData, "lightRotationY")>
            	<cfset theObjModel[theKey].light.lightRotation.x = objectData.lightRotationX>
            	<cfset theObjModel[theKey].light.lightRotation.y = objectData.lightRotationY>
            </cfif>
            
            <cfif structKeyExists(objectData,"lightColor")>
            	<cfset theObjModel[theKey].light.color = objectData.lightColor>
			</cfif>
            <cfif structKeyExists(objectData, "useTimeOfDay")>
            	<cfset theObjModel[theKey].light.timeOfDay = objectData.useTimeOfDay>
            </cfif>
            
            
            
            
            <!--- Use Other Settings from another Scene Asset --->
            <cfif objectData.sceneSettings GT 0>
               
                <cfinvoke  component="Content" method="getAssetContent" returnvariable="sceneObject">
				  <cfinvokeargument name="assetID" value="#objectData.sceneSettings#"/>
				</cfinvoke>   	
				
				<cfloop collection="#sceneObject#" item="theSceneKey"></cfloop>
				
				<cfset sceneObject[theSceneKey].actions = theObjModel[theKey].actions>
				<cfset sceneObject[theSceneKey].assets = theObjModel[theKey].assets>
				<cfset sceneObject[theSceneKey].access = theObjModel[theKey].access>
				<cfset sceneObject[theSceneKey].sharable = theObjModel[theKey].sharable>
				<cfset sceneObject[theSceneKey].cached = theObjModel[theKey].cached>
			
				<cfset theObjModel[theKey] = sceneObject[theSceneKey]>
				
			</cfif>
			
           
           
           
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
   			</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
          
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>

            
        </cfif>

        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- NONE 7 --->
 
 
 	<!--- PANORAMA 8 --->
    <cffunction	name="panoramaModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"panorama":{}}>
        <cfset structAppend(theObjModel.panorama,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 8>
        
        <cfinvoke component="ObjectModels" method="imageContainerModel" returnvariable="theImageModel" />
        
        <cfset vr = structNew()>
        
        <cfset lf = {"left":Duplicate(theImageModel)}>
        <cfset rt = {"right":Duplicate(theImageModel)}>
        <cfset up = {"up":Duplicate(theImageModel)}>
        <cfset fr = {"front":Duplicate(theImageModel)}>
        <cfset bk = {"back":Duplicate(theImageModel)}>
        <cfset dn = {"down":Duplicate(theImageModel)}>
        
        <!--- vr --->
        <cfset lf_vr = {"left":Duplicate(theImageModel)}>
        <cfset rt_vr = {"right":Duplicate(theImageModel)}>
        <cfset up_vr = {"up":Duplicate(theImageModel)}>
        <cfset fr_vr = {"front":Duplicate(theImageModel)}>
        <cfset bk_vr = {"back":Duplicate(theImageModel)}>
        <cfset dn_vr = {"down":Duplicate(theImageModel)}>
        
        <cfset structAppend(vr,lf_vr)>
        <cfset structAppend(vr,rt_vr)>
        <cfset structAppend(vr,up_vr)>
        <cfset structAppend(vr,fr_vr)>
        <cfset structAppend(vr,bk_vr)>
        <cfset structAppend(vr,dn_vr)>
        
        <!--- options --->
        <cfset structAppend(theObjModel.panorama,{"angle":0})>
        <cfset structAppend(theObjModel.panorama,{"viewRange":0})>
        <cfset structAppend(theObjModel.panorama,{"frontView":0})>
        <cfset structAppend(theObjModel.panorama,{"flipLR":0})>
        
        <!--- normal --->
        <cfset pano = {"pano":Duplicate(theImageModel)}>
        
        <cfset structAppend(theObjModel.panorama,pano)>
        
        <cfset structAppend(theObjModel.panorama,lf)>
        <cfset structAppend(theObjModel.panorama,rt)>
        <cfset structAppend(theObjModel.panorama,up)>
        <cfset structAppend(theObjModel.panorama,fr)>
        <cfset structAppend(theObjModel.panorama,bk)>
        <cfset structAppend(theObjModel.panorama,dn)>
        
        <cfset structAppend(theObjModel.panorama,{'vr':vr})>
    	
        <!--- Inject Data --->
      
        <cfif NOT structIsEmpty(objectData)>
                   
			<!--- Build Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>

            <cfset modelURL = structNew()>

            <!--- XDPI --->
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsLe">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.left#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsUp">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.up#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsFr">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.front#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsBa">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.back#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsDn">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.down#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsRi">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.right#"/>
            </cfinvoke>
            
            <!--- pano --->
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsPa">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.pano#"/>
            </cfinvoke>
            
            <!--- XDPI - VR --->
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsLe_vr">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.left_vr#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsUp_vr">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.up_vr#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsFr_vr">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.front_vr#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsBa_vr">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.back_vr#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsDn_vr">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.down_vr#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsRi_vr">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.right_vr#"/>
            </cfinvoke>
          
            <!--- Left --->
            <cfif objectData.left NEQ '' AND NOT structISEmpty(imgSpecsLe)>
                <cfset theObjModel[theKey].left.url.xdpi = assetPath & objectData.left>
                <cfif objectData.left NEQ ''>
					<cfset theObjModel[theKey].left.width = imgSpecsLe.width>
                    <cfset theObjModel[theKey].left.height = imgSpecsLe.height>
                    <cfset theObjModel[theKey].left.ratio = imgSpecsLe.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"left")>
                </cfif>
            </cfif>
 
            <!--- Up --->
            <cfif objectData.up NEQ '' AND NOT structISEmpty(imgSpecsUp)>
                <cfset theObjModel[theKey].up.url.xdpi = assetPath & objectData.up>
                <cfif objectData.up NEQ ''>
					<cfset theObjModel[theKey].up.width = imgSpecsUp.width>
                    <cfset theObjModel[theKey].up.height = imgSpecsUp.height>
                    <cfset theObjModel[theKey].up.ratio = imgSpecsUp.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"up")>
                </cfif>
            </cfif>
            
            <!--- Front --->
            <cfif objectData.front NEQ '' AND NOT structISEmpty(imgSpecsFr)>
                <cfset theObjModel[theKey].front.url.xdpi = assetPath & objectData.front>
                <cfif objectData.front NEQ ''>
					<cfset theObjModel[theKey].front.width = imgSpecsFr.width>
                    <cfset theObjModel[theKey].front.height = imgSpecsFr.height>
                    <cfset theObjModel[theKey].front.ratio = imgSpecsFr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"front")>
                </cfif>
            </cfif>
            
            <!--- Back --->
            <cfif objectData.back NEQ '' AND NOT structISEmpty(imgSpecsBa)>
                <cfset theObjModel[theKey].back.url.xdpi = assetPath & objectData.back>
                <cfif objectData.back NEQ ''>
					<cfset theObjModel[theKey].back.width = imgSpecsBa.width>
                    <cfset theObjModel[theKey].back.height = imgSpecsBa.height>
                    <cfset theObjModel[theKey].back.ratio = imgSpecsBa.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"back")>
                </cfif>
            </cfif>
            
            <!--- Down --->
            <cfif objectData.down NEQ '' AND NOT structISEmpty(imgSpecsDn)>
                <cfset theObjModel[theKey].down.url.xdpi = assetPath & objectData.down>
                <cfif objectData.down NEQ ''>
					<cfset theObjModel[theKey].down.width = imgSpecsDn.width>
                    <cfset theObjModel[theKey].down.height = imgSpecsDn.height>
                    <cfset theObjModel[theKey].down.ratio = imgSpecsDn.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"down")>
                </cfif>
            </cfif>
            
            <!--- Right --->
            <cfif objectData.right NEQ '' AND NOT structISEmpty(imgSpecsRi)>
                <cfset theObjModel[theKey].right.url.xdpi = assetPath & objectData.right>
                <cfif objectData.right NEQ ''>
					<cfset theObjModel[theKey].right.width = imgSpecsRi.width>
                    <cfset theObjModel[theKey].right.height = imgSpecsRi.height>
                    <cfset theObjModel[theKey].right.ratio = imgSpecsRi.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"right")>
                </cfif>
            </cfif>
            
            
            
            
            
            <!--- Pano --->
            <cfif objectData.pano NEQ '' AND NOT structISEmpty(imgSpecsPa)>
                <cfset theObjModel[theKey].pano.url.xdpi = assetPath & objectData.pano>
                <cfif objectData.pano NEQ ''>
					<cfset theObjModel[theKey].pano.width = imgSpecsPa.width>
                    <cfset theObjModel[theKey].pano.height = imgSpecsPa.height>
                    <cfset theObjModel[theKey].pano.ratio = imgSpecsPa.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"pano")>
                </cfif>
            </cfif>
            
            
            
            
            <!--- Left-VR --->
            <cfif objectData.left NEQ '' AND NOT structISEmpty(imgSpecsLe_vr)>
                <cfset theObjModel[theKey].vr.left.url.xdpi = assetPath & objectData.left_vr>
                <cfif objectData.left_vr NEQ ''>
					<cfset theObjModel[theKey].vr.left.width = imgSpecsLe_vr.width>
                    <cfset theObjModel[theKey].vr.left.height = imgSpecsLe_vr.height>
                    <cfset theObjModel[theKey].vr.left.ratio = imgSpecsLe_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"left")>
                </cfif>
            </cfif>
 
            <!--- Up-VR --->
            <cfif objectData.up NEQ '' AND NOT structISEmpty(imgSpecsUp_vr)>
                <cfset theObjModel[theKey].vr.up.url.xdpi = assetPath & objectData.up_vr>
                <cfif objectData.up_vr NEQ ''>
					<cfset theObjModel[theKey].vr.up.width = imgSpecsUp_vr.width>
                    <cfset theObjModel[theKey].vr.up.height = imgSpecsUp_vr.height>
                    <cfset theObjModel[theKey].vr.up.ratio = imgSpecsUp_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"up")>
                </cfif>
            </cfif>
            
            <!--- Front-VR --->
            <cfif objectData.front NEQ '' AND NOT structISEmpty(imgSpecsFr_vr)>
                <cfset theObjModel[theKey].vr.front.url.xdpi = assetPath & objectData.front_vr>
                <cfif objectData.front_vr NEQ ''>
					<cfset theObjModel[theKey].vr.front.width = imgSpecsFr_vr.width>
                    <cfset theObjModel[theKey].vr.front.height = imgSpecsFr_vr.height>
                    <cfset theObjModel[theKey].vr.front.ratio = imgSpecsFr_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"front")>
                </cfif>
            </cfif>
            
            <!--- Back-VR --->
            <cfif objectData.back NEQ '' AND NOT structISEmpty(imgSpecsBa_vr)>
                <cfset theObjModel[theKey].vr.back.url.xdpi = assetPath & objectData.back_vr>
                <cfif objectData.back_vr NEQ ''>
					<cfset theObjModel[theKey].vr.back.width = imgSpecsBa_vr.width>
                    <cfset theObjModel[theKey].vr.back.height = imgSpecsBa_vr.height>
                    <cfset theObjModel[theKey].vr.back.ratio = imgSpecsBa_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"back")>
                </cfif>
            </cfif>
            
            <!--- Down-VR --->
            <cfif objectData.down NEQ '' AND NOT structISEmpty(imgSpecsDn_vr)>
                <cfset theObjModel[theKey].vr.down.url.xdpi = assetPath & objectData.down_vr>
                <cfif objectData.down_vr NEQ ''>
					<cfset theObjModel[theKey].vr.down.width = imgSpecsDn_vr.width>
                    <cfset theObjModel[theKey].vr.down.height = imgSpecsDn_vr.height>
                    <cfset theObjModel[theKey].vr.down.ratio = imgSpecsDn_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"down")>
                </cfif>
            </cfif>
            
            <!--- Right-VR --->
            <cfif objectData.right NEQ '' AND NOT structISEmpty(imgSpecsRi_vr)>
                <cfset theObjModel[theKey].vr.right.url.xdpi = assetPath & objectData.right_vr>
                <cfif objectData.right_vr NEQ ''>
					<cfset theObjModel[theKey].vr.right.width = imgSpecsRi_vr.width>
                    <cfset theObjModel[theKey].vr.right.height = imgSpecsRi_vr.height>
                    <cfset theObjModel[theKey].vr.right.ratio = imgSpecsRi_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"right")>
                </cfif>
            </cfif>

            
             <!--- <cfset nonretina = assetPath &'nonretina/'> 
            
            <!--- MDPI --->
             <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsLe">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.left#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsUp">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.up#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsFr">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.front#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsBa">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.back#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsDn">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.down#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsRi">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.right#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsPa">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.pano#"/>
            </cfinvoke> 
            
            <!--- MDPI VR --->
             <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsLe_vr">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.left_vr#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsUp_vr">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.up_vr#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsFr_vr">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.front_vr#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsBa_vr">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.back_vr#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsDn_vr">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.down_vr#"/>
            </cfinvoke>
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsRi_vr">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.right_vr#"/>
            </cfinvoke> --->
         
            <!--- <!--- Left --->
             <cfif objectData.left NEQ '' AND NOT structISEmpty(imgSpecsLe)>
                <cfset theObjModel[theKey].left.url.mdpi = nonretina & objectData.left> 
                <cfif objectData.left NEQ ''>
					<cfset theObjModel[theKey].left.width = imgSpecsLe.width>
                    <cfset theObjModel[theKey].left.height = imgSpecsLe.height>
                    <cfset theObjModel[theKey].left.ratio = imgSpecsLe.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"left")>
                </cfif>
            </cfif>
            
            <!--- Up --->
            <cfif objectData.up NEQ '' AND NOT structISEmpty(imgSpecsUp)>
                 <cfset theObjModel[theKey].up.url.mdpi = nonretina & objectData.up> 
                <cfif objectData.up NEQ ''>
					<cfset theObjModel[theKey].up.width = imgSpecsUp.width>
                    <cfset theObjModel[theKey].up.height = imgSpecsUp.height>
                    <cfset theObjModel[theKey].up.ratio = imgSpecsUp.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"up")>
                </cfif>
            </cfif>
            
            <!--- front --->
            <cfif objectData.front NEQ '' AND NOT structISEmpty(imgSpecsFr)>
                 <cfset theObjModel[theKey].front.url.mdpi = nonretina & objectData.front> 
                <cfif objectData.front NEQ ''>
					<cfset theObjModel[theKey].front.width = imgSpecsFr.width>
                    <cfset theObjModel[theKey].front.height = imgSpecsFr.height>
                    <cfset theObjModel[theKey].front.ratio = imgSpecsFr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"front")>
                </cfif>
            </cfif>
            
            <!--- Back --->
            <cfif objectData.back NEQ '' AND NOT structISEmpty(imgSpecsBa)>
                 <cfset theObjModel[theKey].back.url.mdpi = nonretina & objectData.back> 
                <cfif objectData.back NEQ ''>
					<cfset theObjModel[theKey].back.width = imgSpecsBa.width>
                    <cfset theObjModel[theKey].back.height = imgSpecsBa.height>
                    <cfset theObjModel[theKey].back.ratio = imgSpecsBa.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"back")>
                </cfif>
            </cfif>
            
            <!--- Down --->
            <cfif objectData.down NEQ '' AND NOT structISEmpty(imgSpecsDn)>
                 <cfset theObjModel[theKey].down.url.mdpi = nonretina & objectData.down> 
                <cfif objectData.down NEQ ''>
					<cfset theObjModel[theKey].down.width = imgSpecsDn.width>
                    <cfset theObjModel[theKey].down.height = imgSpecsDn.height>
                    <cfset theObjModel[theKey].down.ratio = imgSpecsDn.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"down")>
                </cfif>
            </cfif>
            
            <!--- Right --->
            <cfif objectData.right NEQ '' AND NOT structISEmpty(imgSpecsRi)>
                 <cfset theObjModel[theKey].right.url.mdpi = nonretina & objectData.right> 
                <cfif objectData.right NEQ ''>
					<cfset theObjModel[theKey].right.width = imgSpecsRi.width>
                    <cfset theObjModel[theKey].right.height = imgSpecsRi.height>
                    <cfset theObjModel[theKey].right.ratio = imgSpecsRi.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"right")>
                </cfif>
            </cfif>
            
            
			
			
			<!--- Pano VR --->
            <cfif objectData.pano NEQ '' AND NOT structISEmpty(imgSpecsPa)>
                 <cfset theObjModel[theKey].pano.url.mdpi = nonretina & objectData.pano> 
                <cfif objectData.pano NEQ ''>
					<cfset theObjModel[theKey].pano.width = imgSpecsPa.width>
                    <cfset theObjModel[theKey].pano.height = imgSpecsPa.height>
                    <cfset theObjModel[theKey].pano.ratio = imgSpecsPa.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey],"pano")>
                </cfif>
            </cfif>

             <!--- Left VR --->
            <cfif objectData.left NEQ '' AND NOT structISEmpty(imgSpecsLe_vr)>
                 <cfset theObjModel[theKey].vr.left.url.mdpi = nonretina & objectData.left_vr> 
                <cfif objectData.left_vr NEQ ''>
					<cfset theObjModel[theKey].vr.left.width = imgSpecsLe_vr.width>
                    <cfset theObjModel[theKey].vr.left.height = imgSpecsLe_vr.height>
                    <cfset theObjModel[theKey].vr.left.ratio = imgSpecsLe_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"left")>
                </cfif>
            </cfif>
            
            <!--- Up VR --->
            <cfif objectData.up NEQ '' AND NOT structISEmpty(imgSpecsUp_vr)>
                 <cfset theObjModel[theKey].vr.up.url.mdpi = nonretina & objectData.up_vr> 
                <cfif objectData.up_vr NEQ ''>
					<cfset theObjModel[theKey].vr.up.width = imgSpecsUp_vr.width>
                    <cfset theObjModel[theKey].vr.up.height = imgSpecsUp_vr.height>
                    <cfset theObjModel[theKey].vr.up.ratio = imgSpecsUp_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"up")>
                </cfif>
            </cfif>
            
            <!--- Front VR --->
            <cfif objectData.front NEQ '' AND NOT structISEmpty(imgSpecsFr_vr)>
                 cfset theObjModel[theKey].vr.front.url.mdpi = nonretina & objectData.front_vr> 
                <cfif objectData.front_vr NEQ ''>
					<cfset theObjModel[theKey].vr.front.width = imgSpecsFr_vr.width>
                    <cfset theObjModel[theKey].vr.front.height = imgSpecsFr_vr.height>
                    <cfset theObjModel[theKey].vr.front.ratio = imgSpecsFr_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"front")>
                </cfif>
            </cfif>
            
            <!--- Back VR --->
            <cfif objectData.back NEQ '' AND NOT structISEmpty(imgSpecsBa_vr)>
                 <cfset theObjModel[theKey].vr.back.url.mdpi = nonretina & objectData.back_vr> 
                <cfif objectData.back_vr NEQ ''>
					<cfset theObjModel[theKey].vr.back.width = imgSpecsBa_vr.width>
                    <cfset theObjModel[theKey].vr.back.height = imgSpecsBa_vr.height>
                    <cfset theObjModel[theKey].vr.back.ratio = imgSpecsBa_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"back")>
                </cfif>
            </cfif>
            
            <!--- Down VR --->
            <cfif objectData.down NEQ '' AND NOT structISEmpty(imgSpecsDn_vr)>
                 <cfset theObjModel[theKey].vr.down.url.mdpi = nonretina & objectData.down_vr> 
                <cfif objectData.down_vr NEQ ''>
					<cfset theObjModel[theKey].vr.down.width = imgSpecsDn_vr.width>
                    <cfset theObjModel[theKey].vr.down.height = imgSpecsDn_vr.height>
                    <cfset theObjModel[theKey].vr.down.ratio = imgSpecsDn_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"down")>
                </cfif>
            </cfif>
            
            <!--- Right VR --->
            <cfif objectData.right NEQ '' AND NOT structISEmpty(imgSpecsRi_vr)>
                 <cfset theObjModel[theKey].vr.right.url.mdpi = nonretina & objectData.right_vr> 
                <cfif objectData.right_vr NEQ ''>
					<cfset theObjModel[theKey].vr.right.width = imgSpecsRi_vr.width>
                    <cfset theObjModel[theKey].vr.right.height = imgSpecsRi_vr.height>
                    <cfset theObjModel[theKey].vr.right.ratio = imgSpecsRi_vr.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].vr,"right")>
                </cfif>
            </cfif> --->
           
            <!--- options --->
            <cfset theObjModel[theKey].angle = objectData.angle>
            <cfset theObjModel[theKey].viewRange = objectData.viewRange>
            
            <cfset theObjModel[theKey].frontView = objectData.frontView>
            <cfset theObjModel[theKey].flipLR = objectData.flipLR>
            
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
      	
            <cfset structAppend(theObjModel[theKey],assetData)>
           
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
  
        <cfreturn theObjModel>
        
    </cffunction>
 

 	<!--- BALCONY 9 --->
    <cffunction	name="balconyModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"balcony":{}}>
        <cfset structAppend(theObjModel.balcony,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 9>
        
        <cfinvoke component="ObjectModels" method="imageContainerModel" returnvariable="theImageModel" />
        
        <cfset north = {"north":Duplicate(theImageModel)}>
        <cfset south = {"south":Duplicate(theImageModel)}>
        <cfset west = {"west":Duplicate(theImageModel)}>
        <cfset east = {"east":Duplicate(theImageModel)}>
        
        <cfset comp = {"comp":Duplicate(theImageModel)}>

        <cfset structAppend(theObjModel.balcony,{'floorName':''})>
        <cfset structAppend(theObjModel.balcony,{'defaultView':0})>
        
        
        <cfset structAppend(theObjModel.balcony,north)>
        <cfset structAppend(theObjModel.balcony,south)>
        <cfset structAppend(theObjModel.balcony,west)>
        <cfset structAppend(theObjModel.balcony,east)>
        
        <cfset structAppend(theObjModel.balcony,comp)>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    
			<!--- Build Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>

            <cfset modelURL = structNew()>
            
            <!--- XDPI --->
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsNorth">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.north#"/>
            </cfinvoke>
            
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsSouth">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.south#"/>
            </cfinvoke>
            
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsWest">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.west#"/>
            </cfinvoke>
            
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsEast">
                <cfinvokeargument name="imageSrc" value="#assetPath & objectData.east#"/>
            </cfinvoke>
            

            <cfif objectData.north NEQ '' AND NOT structISEmpty(imgSpecsNorth)>
                <cfset theObjModel[theKey].north.url.xdpi = assetPath & objectData.north>
                <cfset theObjModel[theKey].north.width = imgSpecsNorth.width>
                <cfset theObjModel[theKey].north.height = imgSpecsNorth.height>
                <cfset theObjModel[theKey].north.ratio = imgSpecsNorth.ratio>
            </cfif>
            <cfif objectData.east NEQ '' AND NOT structISEmpty(imgSpecsEast)>
                <cfset theObjModel[theKey].east.url.xdpi = assetPath & objectData.east>
                <cfset theObjModel[theKey].east.width = imgSpecsEast.width>
                <cfset theObjModel[theKey].east.height = imgSpecsEast.height>
                <cfset theObjModel[theKey].east.ratio = imgSpecsEast.ratio>
            </cfif>
            <cfif objectData.south NEQ '' AND NOT structISEmpty(imgSpecsSouth)>
                <cfset theObjModel[theKey].south.url.xdpi = assetPath & objectData.south>
                <cfset theObjModel[theKey].south.width = imgSpecsSouth.width>
                <cfset theObjModel[theKey].south.height = imgSpecsSouth.height>
                <cfset theObjModel[theKey].south.ratio = imgSpecsSouth.ratio>
            </cfif>
            <cfif objectData.west NEQ '' AND NOT structISEmpty(imgSpecsWest)>
                <cfset theObjModel[theKey].west.url.xdpi = assetPath & objectData.west>
                <cfset theObjModel[theKey].west.width = imgSpecsWest.width>
                <cfset theObjModel[theKey].west.height = imgSpecsWest.height>
                <cfset theObjModel[theKey].west.ratio = imgSpecsWest.ratio>
            </cfif>
            
            <!--- comp --->
            <cfset compName = "#LCase(listGetAt(objectData.north,1,'_'))#_comp">
			<cfset theCompImage = '#assetPath##compName#.jpg'>
            
            <cfif fileExists(theCompImage)>
            
                <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsComp">
                    <cfinvokeargument name="imageSrc" value="#theCompImage#"/>
                </cfinvoke>

				<cfif NOT structISEmpty(imgSpecsComp)>
                    <cfset theObjModel[theKey].comp.url.xdpi = theCompImage>
                    <cfset theObjModel[theKey].comp.width = imgSpecsComp.width>
                    <cfset theObjModel[theKey].comp.height = imgSpecsComp.height>
                    <cfset theObjModel[theKey].comp.ratio = imgSpecsComp.ratio>
                </cfif>
                
            </cfif>
            
             <cfset nonretina = assetPath &'nonretina/'> 
            
            <!--- MDPI --->
            <!--- <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsNorthL">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.north#"/>
            </cfinvoke>
            
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsSouthL">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.south#"/>
            </cfinvoke>
            
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsWestL">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.west#"/>
            </cfinvoke>
            
            <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecsEastL">
                <cfinvokeargument name="imageSrc" value="#nonretina & objectData.east#"/>
            </cfinvoke>
            
            <cfif objectData.north NEQ '' AND NOT structISEmpty(imgSpecsNorthL)>
                <cfset theObjModel[theKey].north.url.mdpi = nonretina & objectData.north>
                <cfset theObjModel[theKey].north.width = imgSpecsNorthL.width>
                <cfset theObjModel[theKey].north.height = imgSpecsNorthL.height>
                <cfset theObjModel[theKey].north.ratio = imgSpecsNorthL.ratio>
            </cfif>
            <cfif objectData.east NEQ '' AND NOT structISEmpty(imgSpecsEastL)>
                <cfset theObjModel[theKey].east.url.mdpi = nonretina & objectData.east>
                <cfset theObjModel[theKey].east.width = imgSpecsEastL.width>
                <cfset theObjModel[theKey].east.height = imgSpecsEastL.height>
                <cfset theObjModel[theKey].east.ratio = imgSpecsEastL.ratio>
            </cfif>
            <cfif objectData.south NEQ '' AND NOT structISEmpty(imgSpecsSouthL)>
                <cfset theObjModel[theKey].south.url.mdpi = nonretina & objectData.south>
                <cfset theObjModel[theKey].south.width = imgSpecsSouthL.width>
                <cfset theObjModel[theKey].south.height = imgSpecsSouthL.height>
                <cfset theObjModel[theKey].south.ratio = imgSpecsSouthL.ratio>
            </cfif>
            <cfif objectData.west NEQ '' AND NOT structISEmpty(imgSpecsWestL)>
                <cfset theObjModel[theKey].west.url.mdpi = nonretina & objectData.west>
                <cfset theObjModel[theKey].west.width = imgSpecsWestL.width>
                <cfset theObjModel[theKey].west.height = imgSpecsWestL.height>
                <cfset theObjModel[theKey].west.ratio = imgSpecsWestL.ratio>
            </cfif> --->

            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            <cfset theObjModel[theKey].floorName = objectData.floorName>
            <cfset theObjModel[theKey].defaultView = objectData.facing>
 
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>

    

 	<!--- XYZPOINT 10 --->
    <cffunction	name="xyzPointModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />

        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"xyzpoint":{}}>
        <cfset structAppend(theObjModel.xyzpoint,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 10>
        
		<cfset structAppend(theObjModel.xyzpoint,{"objectRef":""})>
		<cfset structAppend(theObjModel.xyzpoint,{"hideBehindGeometry":0, "cameraBehaviour":0})>
		<cfset structAppend(theObjModel.xyzpoint,{"disabled":0})>
		<cfset structAppend(theObjModel.xyzpoint,{"x_pos":0,"y_pos":0,"z_pos":0})>
        <cfset structAppend(theObjModel.xyzpoint,{"alignH":0,"alignV":0,"touchMargin":0, "scaleToMap":0, "absolutePos":0})>
 	
        <!--- Inject Data --->
   
        <cfif NOT structIsEmpty(objectData)>
                    	
			<cfset theObjModel[theKey].x_pos = objectData.x_pos>
            <cfset theObjModel[theKey].y_pos = objectData.y_pos>
            <cfset theObjModel[theKey].z_pos = objectData.z_pos>
            
            <cfset theObjModel[theKey].alignH = objectData.regPointH>
            <cfset theObjModel[theKey].alignV = objectData.regPointV>
            <cfset theObjModel[theKey].touchMargin = objectData.touchRegion>
            
            <cfset theObjModel[theKey].scaleToMap = objectData.scaleToMap>
            <cfset theObjModel[theKey].hideBehindGeometry = objectData.hideBehindGeometry>
            <cfset theObjModel[theKey].cameraBehaviour = objectData.cameraBehaviour>
            <cfset theObjModel[theKey].disabled = objectData.disabled>
            
            <cfset theObjModel[theKey].absolutePos = objectData.absolutePos>
            
            <cfset theObjModel[theKey].objectRef = objectData.objectRef>
            
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
  
        <cfreturn theObjModel>
        
    </cffunction>
    
    <!--- NOTHING 11 --->
	<!--- NOTHING 12 --->
    <!--- NOTHING 13 --->
    <!--- NOTHING 14 --->
    
    <!--- MARKER 15 --->
    <cffunction	name="markerModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"marker":{}}>
        <cfset structAppend(theObjModel.marker,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        
        <cfset theObjModel[theKey].assetType = 15>
        
        <cfinvoke component="ObjectModels" method="imageContainerModel" returnvariable="theImageModel" />
        
		<cfset structAppend(theObjModel.marker,{"visualizer":{'image':theImageModel, 'key':'', 'data':''}})>
        <cfset structAppend(theObjModel.marker,{"moodstocks":{'key':'','secret':''}})>
        <cfset structAppend(theObjModel.marker,{"vuforia":{'license':'','xml':'',"dat":''}})>
        
        <cfset structAppend(theObjModel.marker,{"arDefaultMode":0})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                        
			<cfset data = structNew()>
            
            <!--- Moodstocks --->
            <cfif objectData.useMoodstocks>
                <cfset moodstocks = {"key":objectData.APIKey, "secret":objectData.APISecret}>
                <cfset theObjModel[theKey].moodstocks = moodstocks>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'moodstocks')>
            </cfif>
            
            <!--- Visualizer --->
            <cfif objectData.useVisualizer>
            
            	<!--- Build Asset Path --->
                <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                    <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                    <cfinvokeargument name="server" value="yes"/>
                </cfinvoke>
                
                <cfset visualizer = {"key":objectData.keyData, "set":objectData.keySet, "url":""}>
                
                <cfif objectData.webURL NEQ ''>
                    <cfset ImageUrl = objectData.webURL>
                <cfelse>
                    <cfset ImageUrl = assetPath & objectData.url>
                </cfif>
                
                <cfset visualizer.url = ImageUrl>
                
                <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecs">
                    <cfinvokeargument name="imageSrc" value="#ImageUrl#"/>
                </cfinvoke>
                
                <cfif visualizer.url NEQ '' AND NOT structISEmpty(imgSpecs)>
					<cfset theObjModel[theKey].visualizer.image.url.xdpi = visualizer.url>
                    <cfset theObjModel[theKey].visualizer.image.width = imgSpecs.width>
                    <cfset theObjModel[theKey].visualizer.image.height = imgSpecs.height>
                    <cfset theObjModel[theKey].visualizer.image.ratio = imgSpecs.ratio>
                <cfelse>
                	<cfset structDelete(theObjModel[theKey].visualizer,'image')>
            	</cfif>
                
                <cfset theObjModel[theKey].visualizer.data = visualizer.set>
                <cfset theObjModel[theKey].visualizer.key = visualizer.key>
                
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'visualizer')>   
            </cfif>

            <!--- Vuforia --->
            <cfif objectData.useVuforia>
            
            	<!--- Build Asset Path --->
                <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                    <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                    <cfinvokeargument name="server" value="yes"/>
                </cfinvoke>
				
                <cfset vuforia = {"dat":"", "xml":""}>
                
                <cfif objectData.dat NEQ ''>
                    <cfset vuforia.dat = assetPath & objectData.dat>
                </cfif>
                
                <cfif objectData.xml NEQ ''>
                    <cfset vuforia.xml = assetPath & objectData.xml>
                </cfif>
                
                <cfset theObjModel[theKey].vuforia = vuforia>
                
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'vuforia')>   
            </cfif>
            
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- arMode --->
            <cfif structKeyExists(objectData,'arMode')>
            	<cfset theObjModel[theKey].arDefaultMode = objectData.arMode>
            </cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    <!--- NOTHING 16 --->
    
    
    <!--- ANIMATION 17 --->
    <cffunction	name="animationModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"animation":{}}>
        <cfset structAppend(theObjModel.animation,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 17>
        
		<cfset structAppend(theObjModel.animation,{"model3d":""})>
        <cfset structAppend(theObjModel.animation,{"animationSequence":""})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                        
			<cfset theObjModel[theKey].animationSequence = objectData.animationSequence>
            <cfset theObjModel[theKey].model3D = objectData.model3D>
        
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
 
    
    <!--- GROUP 18 --->
    <cffunction	name="groupModel" access="public" returntype="struct" output="true">
		<cfargument	name="objectData" type="array" required="no" default="#structNew()#" />
 
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
     
        <cfset theObjModel = {"group":{}}>
        <cfset structAppend(theObjModel.group,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        
        <cfset theObjModel[theKey].assetType = 18>
        
		<cfset structAppend(theObjModel[theKey],{"display":{}})>
		<cfset structAppend(theObjModel[theKey].display,{"type":'',"target":'',"assetID":0, "anchor":0, "alignment":0})>
        
        <cfset structAppend(theObjModel[theKey],{"assets":[]})>
        <cfset structAppend(theObjModel[theKey],{"actions":[]})>
   
        <!--- Inject Data --->		
        <cfif NOT arrayIsEmpty(objectData)>
        
			<cfif NOT structKeyExists(objectData[1],"asset_id")>
                
                <cfif structKeyExists(objectData[1],"assetID")>
                    <cfset structAppend(objectData[1],{"asset_id":objectData[1].assetID})>
                    <cfset structDelete(objectData[1],"assetID")>
                </cfif>
            
            </cfif>

            <cfset assetID = objectData[1].asset_id>
        
        	<cfquery name="contentAsset">  
                SELECT  displayType, instanceName, instanceAsset_id, alignment, className, methodName, params, popoverType, hero, cols, dismiss
                FROM	ContentAssets
                WHERE   asset_id = #assetID#           
            </cfquery>
            
            <cfset allTypes = ["3D Model","Modal","Panel","PopOver","Image","Load","Register","Container", "ExecuteMethod"]>
           	
            <cfif contentAsset.displayType GT 0>
				<cfset displayType = allTypes[contentAsset.displayType]>
                <cfset theObjModel[theKey].display.type = displayType>
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'display')>
  			</cfif>
            
            <cfif contentAsset.displayType GT 0>
            
            	<!--- 3D Model properties --->
               <!--- 3dModel OR Image OR Load type --->
                <cfif contentAsset.displayType IS 1 OR contentAsset.displayType IS 5 OR contentAsset.displayType IS 6 OR contentAsset.displayType IS 7 OR contentAsset.displayType IS 8 OR contentAsset.displayType IS 9>

                    <!--- model --->
                    <cfif contentAsset.instanceName NEQ ''>
                		<cfset theObjModel[theKey].display.target = contentAsset.instanceName>
                    <cfelse>
                    	<cfset structDelete(theObjModel[theKey].display,'target')>
                    </cfif>
 
                    <!--- asset id --->
					<cfif contentAsset.instanceAsset_id GT 0>
                   		<cfif contentAsset.displayType IS 7>
                   			<cfset structAppend(theObjModel[theKey].display, {"groupID":contentAsset.instanceAsset_id})>
                   		<cfelse>
                    		<cfset theObjModel[theKey].display.assetID = contentAsset.instanceAsset_id>
						</cfif>
                    <cfelse>
                    	<cfset structDelete(theObjModel[theKey].display,'assetID')>
                    </cfif>

                    <!--- alignment --->
					<cfif contentAsset.alignment NEQ '' OR contentAsset.alignment GT 0>
                  		
                   		<cfset anchor = Mid(contentAsset.alignment, 1, 1)>
                   		<cfset alignment = Mid(contentAsset.alignment, 2, 1)>
                   		
                   		<cfset alignTypes = ['left','top','right','bottom']>
                   		<cfset alignmentTypes = ['left', 'center','right', 'top','middle','bottom']>
                   		
                    	<cfset theObjModel[theKey].display.anchor = alignTypes[anchor]>
                    	<cfset theObjModel[theKey].display.alignment = alignmentTypes[alignment]>
                    	
                    <cfelse>
                    	<cfset structDelete(theObjModel[theKey].display,'anchor')>
                    	<cfset structDelete(theObjModel[theKey].display,'alignment')>
                    </cfif>
                    
                    <!--- Execute Method --->
                    <cfif contentAsset.displayType IS 9>
                    	<cfset structAppend(theObjModel[theKey],{"exec":{}})>
                    	<cfset objParams = DeserializeJSON(contentAsset.params)>
                    	<cfset structAppend(theObjModel[theKey].exec, {"className":contentAsset.className, "methodName":contentAsset.methodName, "param":objParams})>
					</cfif>
                    
                <cfelse>
                
                	<!--- Other Display types --->
                    <cfset structDelete(theObjModel[theKey].display,'target')>
                    <cfset structDelete(theObjModel[theKey].display,'assetID')>
                    
                    <cfset structDelete(theObjModel[theKey].display,'anchor')>
                    <cfset structDelete(theObjModel[theKey].display,'alignment')>
                    
                </cfif> 
                
                <!--- Popover Type --->
				<cfif contentAsset.displayType IS 4>
					<cfset structAppend(theObjModel[theKey].display, {"popoverType":contentAsset.popoverType})>
				</cfif>
               
               <!--- Style Grid --->
				<cfif contentAsset.displayType IS 3>
					<cfset structAppend(theObjModel[theKey].display, {"hero":contentAsset.hero, "cols": contentAsset.cols})>
					<cfset structAppend(theObjModel[theKey].display, {"dismiss":contentAsset.dismiss})>
				</cfif>
                
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'display')>
        	</cfif>
        
            <cfloop index="anObject" array="#objectData#">
				
                <cfif structKeyExists(anObject,'content_id')>
                
					<!--- Get Asset Data --->
                    <cfinvoke  component="Content" method="getAssetContent" returnvariable="assetData">
                        <cfinvokeargument name="assetID" value="#anObject.content_id#"/>
                    </cfinvoke>
                    
                    <!--- Add Assets to Group Object --->
                    <cfset arrayAppend(theObjModel[theKey].assets,assetData)>
                    
                <cfelse>
                	<!--- Object has Assets so Transfer --->
                	<cfif structKeyExists(anObject,'assets')>
                		<cfset theObjModel[theKey].assets = anObject.assets>
                    </cfif>
                    
                </cfif>
                
            </cfloop>

			<cfset theObjModel[theKey].assetID = anObject.asset_id>
            
            <cfif structKeyExists(anObject,'assetModified')>
            	<cfset theObjModel[theKey].modified = anObject.assetModified>
            <cfelseif structKeyExists(anObject,'modified')>
            	<cfset theObjModel[theKey].modified = anObject.modified>
            </cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#anObject.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
   
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
       
        </cfif>

        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- GAME 19 --->
    <cffunction	name="gameModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"game":{}}>
        <cfset structAppend(theObjModel.game,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 19>
        
		<cfset structAppend(theObjModel.game,{"silentmode":0})>
		
        <cfset structAppend(theObjModel.game,{"startmessage":''})>
        <cfset structAppend(theObjModel.game,{"endMessage":''})>
        
		<cfset structAppend(theObjModel.game,{"gameStart":''})>
        <cfset structAppend(theObjModel.game,{"gameEnd":''})>
        
        <cfset structAppend(theObjModel.game,{"sendplayeremail":''})>
        <cfset structAppend(theObjModel.game,{"sendclientemail":''})>
        <cfset structAppend(theObjModel.game,{"clientemail":''})>
        <cfset structAppend(theObjModel.game,{"emailmessage":''})>
        
        <cfset structAppend(theObjModel.game,{"includecode":''})>
        <cfset structAppend(theObjModel.game,{"sendassets":''})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>

            <cfset theObjModel[theKey].clientemail = objectData.email>
            
            <cfset theObjModel[theKey].emailMessage = objectData.emailMessage>
            <cfset theObjModel[theKey].endMessage = objectData.messageEnd>
            <cfset theObjModel[theKey].startMessage = objectData.messageStart>
            
            <cfset theObjModel[theKey].gameStart = objectData.dateStart>
            <cfset theObjModel[theKey].gameEnd = objectData.dateEnd>  
            
            <cfset theObjModel[theKey].includeCode = objectData.useCode>
            <cfset theObjModel[theKey].sendClientEmail = objectData.sendClientEmail>
            <cfset theObjModel[theKey].sendPlayerEmail = objectData.sendPlayerEmail>
            <cfset theObjModel[theKey].silentmode = objectData.silentmode> 
            
            <cfset theObjModel[theKey].sendAssets = objectData.sendTargets>
            
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>

            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- QUIZ 20 --->
    <cffunction	name="quizModel" access="public" returntype="struct" output="true">
        <cfargument	name="choices" type="array" required="no" default="#arrayNew(1)#" />
        <cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
		
        <!--- Get Colors --->
        <cfinvoke  component="ObjectModels" method="colorsModel" returnvariable="colors" />
   
        <cfif arrayLen(choices) IS 0>
        	<cfset aChoiceSelection = {"message":"A Selection","correct":true, "choice_id":0, "colors":Duplicate(colors.colors)}>
            <cfset choices = []>
            <cfset arrayAppend(choices,Duplicate(aChoiceSelection))>
            <cfset arrayAppend(choices,Duplicate(aChoiceSelection))>
        </cfif>
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"quiz":{}}>
    	<cfset structAppend(theObjModel.quiz,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 20>
        
		<!--- spacing: spacing between each grid item --->
        <!--- padding: padding inside grid container --->
        <!--- cols: number of cols h --->
        
        <cfset structAppend(theObjModel.quiz,{"selection":{}})>
        <cfset structAppend(theObjModel.quiz.selection,{"layout":{"spacing":10, "padding":20, "cols":2}})>
        <cfset structAppend(theObjModel.quiz.selection,{"colors":Duplicate(colors.colors)})>
		
        <!--- Image assets for Choice --->
        <cfinvoke component="ObjectModels" method="imageContainerModel" returnvariable="theImageModel" />
        
        <cfset choice = {"message":"The Answers Text", "image":Duplicate(theImageModel), "correct":false, "choice_id":0}>
        <cfset structAppend(theObjModel.quiz.selection,{"choices":[]})>

		<!--- Choices, Default Boolean --->
        <cfloop index="z" from="1" to="#arrayLen(choices)#">
            
            <cfset choiceSelection = Duplicate(choice)>
            <cfset choiceSelection.message = choices[z].message>			<!--- Text for Choice --->
            <cfset choiceSelection.correct = choices[z].correct>			<!--- Correct Flag --->
            <cfset choiceSelection.choice_id = choices[z].choice_id>		<!--- Choice ID --->
            
            <cfset arrayAppend(theObjModel.quiz.selection.choices,choiceSelection)>
            
        </cfloop>  
             
 		<!--- Highlight Correct immediatly after selection has been made --->
		<cfset structAppend(theObjModel.quiz,{"displayCorrect":true})>
        
        <!--- Display Response - Correct or Incorrect --->
        <cfset content = {"assets":[], "message":"Sample Message"}>
        <cfset structAppend(theObjModel.quiz,{"response":{"correct":Duplicate(content),"incorrect":Duplicate(content)}})>
        
        <!--- complete: is whether you want the quiz is optional --->
        <!--- tryagain: is whether you want the quiz have them try again until correct --->
        <!--- displaycorrect: is whether you want the quiz highlight the selection if it is correct --->
        
        <!--- Must Complete Question to continue otherwise optional --->
        <cfset structAppend(theObjModel.quiz,{"complete":true})>
        
        <!--- Retry Question --->
        <cfset structAppend(theObjModel.quiz,{"tryAgain":0})>
        
        <!--- Messages for Question, Incorrect and Correct --->
        <cfset structAppend(theObjModel.quiz,{"question":Duplicate(content)})>
        
        <!--- Allow selections to be reordered - movable --->
        <cfset structAppend(theObjModel.quiz,{"movable":false})>
		
        <!--- Inject Data --->
		
		<cfif NOT structIsEmpty(objectData)>
            
            <cfset dataRec = structNew()>
       
			<!--- Build Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
        
			<!--- Assets for Quiz, Correct and Incorrect --->
            <cfinvoke  component="Modules" method="getGroupAssetsActions" returnvariable="allQuizAssets">
              <cfinvokeargument name="assetID" value="#objectData.asset_id#"/>
             </cfinvoke>  
             
             
             <!--- Assets --->
             <cfset quizAssets = {"quiz":[], "correct":[], "incorrect":[]}>

             <cfloop query="allQuizAssets.assets">

                <cfinvoke component="Content" method="getAssetContent" returnvariable="AssetData">
                    <cfinvokeargument name="assetID" value="#content_id#"/>
                </cfinvoke>

                <cfif quiz>
                    <cfset arrayAppend(quizAssets.quiz,AssetData)>
                <cfelseif quizCorrect>
                    <cfset arrayAppend(quizAssets.correct,AssetData)>
                <cfelseif quizIncorrect>
                    <cfset arrayAppend(quizAssets.incorrect,AssetData)>
                </cfif>
              
             </cfloop>
            
			<!--- Quiz --->   
              
            <cfif objectData.complete>
                <cfset theObjModel[theKey].complete = objectData.complete>
             <cfelse>
            	<cfset structDelete(theObjModel[theKey],'complete')>
            </cfif>
            
            <cfset theObjModel[theKey].question= {"message":objectData.questionMessage,"assets":quizAssets.quiz}>
            
            <cfif objectData.needResponse>
           
                <cfset theObjModel[theKey].response.correct = {"message":objectData.correctMessage,"assets":quizAssets.correct}>
                <cfset theObjModel[theKey].response.incorrect = {"message":objectData.incorrectMessage,"assets":quizAssets.incorrect}>
                
                <cfif objectData.tryAgain GT 0>
                    <cfset theObjModel[theKey].tryAgain = objectData.tryAgain>
                <cfelse>
            		<cfset structDelete(theObjModel[theKey],'tryAgain')>
                </cfif>
            
            <cfelse>
            	
                <cfset structDelete(theObjModel[theKey],'response')>
            	<cfset structDelete(theObjModel[theKey],'tryAgain')>
              
            </cfif>
        
			<!--- Selection ---> 
            
            <cfif objectData.displayCorrect>
                <cfset theObjModel[theKey].displayCorrect = objectData.displayCorrect>
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'displayCorrect')>
            </cfif>
            
            <cfif objectData.movable>
                <cfset theObjModel[theKey].movable = objectData.movable>
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],'movable')>
            </cfif>

     
			<!--- Grid --->
            <cfquery name="selectionGrid">  
                SELECT  SelectionAssets.spacing, SelectionAssets.padding, SelectionAssets.numberOfCols, Colors.forecolor, Colors.backcolor, Colors.background
                FROM	SelectionAssets LEFT OUTER JOIN Colors ON SelectionAssets.color_id = Colors.color_id
                WHERE   selection_id = #objectData.selection_id#           
            </cfquery>
            
            <cfset theObjModel[theKey].selection.layout.cols = selectionGrid.numberOfCols>
            <cfset theObjModel[theKey].selection.layout.padding = selectionGrid.padding>
            <cfset theObjModel[theKey].selection.layout.spacing = selectionGrid.spacing>
            
            <cfset theObjModel[theKey].selection.colors.forecolor = selectionGrid.forecolor>
            <cfset theObjModel[theKey].selection.colors.backcolor = selectionGrid.backcolor>
            <cfset theObjModel[theKey].selection.colors.background = selectionGrid.background>
      
			<!--- Choices --->
            
            <cfquery name="selectionChoices">  
                SELECT  url, text, correct, choice_id, selection_id, width, height, aspectRatio
                FROM	ChoiceAssets
                WHERE   selection_id = #objectData.selection_id#           
            </cfquery>
     
			<cfset choices = arrayNew(1)>
            
            <cfloop query="selectionChoices">
                
                <cfset aChoice = structNew()>
                
                <cfset structAppend(aChoice,{"choiceID":choice_id})>
                
                <cfif url NEQ ''>
                
                    <!--- <cfset filePath = assetPath &'nonretina/'& url> --->
                    <cfset filePathRetina = assetPath & url>
                    
                    <cfset structAppend(aChoice,{"image":Duplicate(theImageModel)})>
						
                    <!--- <cfset aChoice.image.url.mdpi = filePath> --->
                    <cfset aChoice.image.url.xdpi = filePathRetina>
           
                    <cfif width IS '' OR height IS '' OR aspectRatio IS ''>
                    	
                        <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imgSpecs">
                            <cfinvokeargument name="imageSrc" value="#filePathRetina#"/>
                        </cfinvoke>

						<cfset aChoice.image.width = imgSpecs.width>
						<cfset aChoice.image.height = imgSpecs.height>
                        <cfset aChoice.image.ratio = imgSpecs.ratio>
                        
					<cfelse>
                    
                    	<cfset aChoice.image.width = width>
						<cfset aChoice.image.height = height>
                        <cfset aChoice.image.ratio = aspectRatio>
                        
                    </cfif>
                    
                </cfif>
               
                <!--- Colors 																						- TODO
				
				To make this happen, 
				1) I have to add colorID to the ChoiceAssets DB
				2) Add the Colors component to the choice to allow for forecolor, backcolor and background
				3) Insert the colorID in the DB, or update
				4) Display the colors for the choice
				
                --->
                
                <cfif text NEQ ''>
                    <cfset theMessage = JavaCast("string",text)>
                    <cfset structAppend(aChoice,{"message":theMessage})>
                </cfif>
                
                <cfif correct>
                    <cfset structAppend(aChoice,{"correct":correct})>
                </cfif>
                
                <cfset arrayAppend(choices,aChoice)>
            
            </cfloop>
        
			<cfset theObjModel[theKey].selection.choices = choices>
			
            
            <!--- Other --->
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
       			<cfset theObjModel[theKey].modified = objectData.assetModified>
            </cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
   	     
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- MATERIALS 21 --->
    <cffunction	name="materialModel" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />
        
        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"material":{}}>
        <cfset structAppend(theObjModel.material,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 21>
        
		<cfset structAppend(theObjModel.material,{"model3d":""})>
        <cfset structAppend(theObjModel.material,{"material":""})>
    	
        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
                    
			<cfset theObjModel[theKey].material = objectData.material>
            <cfset theObjModel[theKey].model3D = objectData.model3D>
        
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    
    <!--- GEO-FENCE 23 --->
    <cffunction	name="geofenceModel" access="public" returntype="struct" output="false">

        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"geofence":{}}>
        <cfset structAppend(theObjModel.geofence,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 22>
        
		<cfset structAppend(theObjModel.geofence,{"geofence":[]})>
        <cfset structAppend(theObjModel.geofence,{"message":""})>
    	
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <!--- PROGRAM SCEDULE 22 --->
    <cffunction	name="programSchedule" access="public" returntype="struct" output="false">
		<cfargument	name="objectData" type="struct" required="no" default="#structNew()#" />

        <cfinvoke component="ObjectModels" method="basicModel" returnvariable="theObjModelStuct" />
        
        <cfset theObjModel = {"programSchedule":{}}>
        <cfset structAppend(theObjModel.programSchedule,theObjModelStuct.objectName)>
        
        <cfloop collection="#theObjModel#" item="theKey"></cfloop>
        <cfset theObjModel[theKey].assetType = 22>
        
		<cfset structAppend(theObjModel.programSchedule,{"isScheduled":"", "startTime":0, "endTime":0, "duration":0, "assetID":0, "assets":[]})>

        <!--- Inject Data --->
        
        <cfif NOT structIsEmpty(objectData)>
             
            <cfset theObjModel[theKey].isScheduled = objectData.isScheduled>
            
            <cfif objectData.isScheduled IS ''><cfset objectData.isScheduled = false></cfif>
            
            <cfif objectData.isScheduled>
				<cfset theObjModel[theKey].startTime = Val(objectData.startTime)>
            	<cfset theObjModel[theKey].endTime = Val(objectData.endTime)>
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],"startTime")>
            	<cfset structDelete(theObjModel[theKey],"endTime")>
            </cfif>
            
            <cfif objectData.duration NEQ ''>
            	<cfset theObjModel[theKey].duration = int(objectData.duration*60)>
            <cfelse>
            	<cfset theObjModel[theKey].duration = 0>
            </cfif>
            
            <cfset theObjModel[theKey].assetID = objectData.asset_id>
            
            <cfif theObjModel[theKey].duration IS 0>
            	<cfset structDelete(theObjModel[theKey],'duration')>
            <cfelse>
                <cfset structDelete(theObjModel[theKey],'endTime')>
            </cfif>
          
            <cfif structKeyExists(theObjModel[theKey],'endTime')>
            
				<cfif theObjModel[theKey].endTime IS 0>
                    <cfset structDelete(theObjModel[theKey],"startTime")>
                    <cfset structDelete(theObjModel[theKey],"endTime")>
                </cfif>
            
            <cfelse>
            	<cfset structDelete(theObjModel[theKey],"startTime")>
            </cfif>
            
            <!--- getAsset for Display --->
            <cfif objectData.displayAsset_id GT 0>
            
                <cfinvoke component="Content" method="getAssetContent" returnvariable="displayAssetData">
                    <cfinvokeargument name="assetID" value="#objectData.displayAsset_id#"/>
                </cfinvoke>
            
            	<cfset arrayAppend(theObjModel[theKey].assets, displayAssetData)>
               
            </cfif>
             
            <cfif structKeyExists(objectData,'assetModified')>
            	<cfset theObjModel[theKey].modified = objectData.assetModified>
       		</cfif>
            
            <!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#objectData.asset_id#">
            </cfinvoke>
            
            <cfset structAppend(theObjModel[theKey],assetData)>
         
            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
            
        </cfif>
    
        <cfreturn theObjModel>
        
    </cffunction>
    
    
    
    <cffunction	name="buildImagePaths" access="public" returntype="struct" output="false">
      	
        <cfargument	name="assetID" type="numeric" required="yes" />
        <cfargument	name="fileName" type="string" required="yes" />
        
      	<cfset theUrls = structNew()>
		
		<!--- Build Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="retinaPath">
            <cfinvokeargument name="assetID" value="#assetID#"/>
            <cfinvokeargument name="server" value="yes"/>
        </cfinvoke>
        	
		<cfset theUrl = retinaPath & fileName>
        <cfset structAppend(theUrls,{'xdpi':theUrl})>

		<!--- <cfset nonretinaPath = retinaPath & 'nonretina/'> --->
        	
		<!--- <cfset theUrl = nonretinaPath & fileName>
        <cfset structAppend(theUrls,{'mdpi':theUrl})> --->
        
        <cfreturn theUrls>
        
    </cffunction>

</cfcomponent>