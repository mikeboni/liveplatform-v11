<cfcomponent>
	
	<cffunction name="getAssetImg" access="remote" returntype="struct">
            <cfargument name="assetID" type="numeric" required="no" default="0">
            
            <cfinvoke component="Modules" method="getGroupAssets" returnvariable="assetInfo">
			  <cfinvokeargument name="assetID" value="#assetID#"/>
			</cfinvoke>
			
           <cfset objectInfo = {'type': 0, 'url': ''}>
           
            <cfset theObj = assetInfo.assets>
    
            <cfif arrayLen(theObj) GT 0>

				<cfloop collection="#theObj[1]#" item="theKey"></cfloop>
				<cfset theAsset = theObj[1][theKey]>
				<cfset objectInfo.type = theAsset.assetType>
				
				<cfif structKeyExists(theAsset,'thumb')>
					<cfset urlImg = theAsset.thumb.url.xdpi>
				<cfelseif structKeyExists(theAsset,'url')>
					<cfset urlImg = theAsset.url.xdpi.url>
				<cfelseif structKeyExists(theAsset,'pano')>
					<cfset urlImg = theAsset.pano.xdpi.url>
				<cfelse>
					<cfset urlImg = "">
				</cfif>
				
           		<cfset objectInfo.url = urlImg>
           
            </cfif>
           	
            <cfreturn objectInfo>
            
	</cffunction>
	
	<!--- Remove Background Image from Colors --->
    <cffunction name="removeBackgroundImage" access="remote" returntype="boolean">
    		<cfargument name="groupID" type="numeric" required="no" default="0">
            <cfargument name="assetID" type="numeric" required="no" default="0">
            
            <cfif assetID IS 0 AND groupID IS 0><cfreturn false></cfif>
            
			<cfif assetID GT 0>
            	
                <!--- get Asset path --->
                 <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                  <cfinvokeargument name="assetID" value="#assetID#"/>
                  <cfinvokeargument name="server" value="no"/>
                </cfinvoke>
            
                <cfquery name="theImage">  
                    SELECT  Colors.background, Colors.color_id
                    FROM	Assets INNER JOIN Colors ON Assets.color_id = Colors.color_id
                    WHERE   asset_id = #assetID# 
                </cfquery>
                
            </cfif>
            
            <cfif groupID GT 0>
            	
                <!--- get Group path --->
                <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                  <cfinvokeargument name="groupID" value="#groupID#"/>
                  <cfinvokeargument name="server" value="no"/>
                </cfinvoke>
                
                <cfquery name="theImage">  
                    SELECT  Colors.background, Colors.color_id
                    FROM	Groups INNER JOIN Colors ON Groups.color_id = Colors.color_id
                    WHERE   group_id = #groupID#
                </cfquery>
            
            </cfif>
           
            <cfset bkImage = theImage.background>
            
            <cfif bkImage NEQ ''>
				<!--- Define Path to Background and Delete --->
                <cfset theImageBKPath = "#assetPath##bkImage#">
                <cfset theImageBKPathNonretina = "#assetPath#nonretina/#bkImage#">
                
                <!--- delete image bk --->
                <cfif fileExists(theImageBKPath)>
                      <cffile action="delete" file="#theImageBKPath#">
                </cfif>  
                <cfif fileExists(theImageBKPathNonretina)>
                      <cffile action="delete" file="#theImageBKPathNonretina#">
                </cfif>
            </cfif>
            
            <cfset colorID = theImage.color_id>
            
            <!--- Update Colors --->
            <cfquery name="updateColors">
                 UPDATE Colors
                 SET background = ''
                 WHERE (color_id = #colorID#)           
            </cfquery> 

            <cfreturn true>
    
    </cffunction>
    
	<!--- Quiz Choices Display --->
    <cffunction name="getAllChoices" access="remote" returntype="struct">
            <cfargument name="selectionID" type="numeric" required="yes">
            <cfargument name="contentHolder" type="string" required="yes">
            
            <cfquery name="selectionChoices">  
                SELECT  ChoiceAssets.url, ChoiceAssets.text, ChoiceAssets.correct, ChoiceAssets.sortorder, ChoiceAssets.choice_id, ChoiceAssets.aspectRatio, QuizAssets.asset_id
                FROM	QuizAssets LEFT OUTER JOIN ChoiceAssets ON QuizAssets.selection_id = ChoiceAssets.selection_id
                WHERE   ChoiceAssets.selection_id = #selectionID#    
                ORDER BY sortorder ASC         
            </cfquery>
			
            <cfset assetID = selectionChoices.asset_id>
            
            <!--- Asset Path --->
           <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
         	
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPathRel">
                <cfinvokeargument name="assetID" value="#assetID#"/>
                <cfinvokeargument name="relative" value="true"/>
            </cfinvoke>  
            
            <cfsavecontent variable="htmlCode">
            <cfoutput>
            <cfset z = 0>
            </form>
              <cfloop query="selectionChoices">
			
                <!--- File + Path --->
                <cfset imgFile = assetPath & url>
                <cfset imgFileR = assetPathRel & url>
                
                <form action="imageSubmit.cfm" method="post" enctype="multipart/form-data" name="choiceSelections" id="choiceSelections">
                
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                
                    <tr>
                      <td colspan="4"><hr size="1" /></td>
                      </tr>
                    <tr>
                      <td width="300">
                      <table width="100%" border="0" cellspacing="5" cellpadding="0">
                        <tr>
                          <td width="67" align="left" valign="top">
                            <cfif FileExists(imgFile)>
                              <img src="#imgFileR#" name="choiceImage" width="300" height="#300/aspectRatio#" border="1" id="choiceImage" />
                            </cfif>
                            
                          </td>
                        </tr>
                        <tr>
                          <td align="left" valign="top">
                          <textarea name="choiceMessage" class="formfieldcontent" id="choiceMessage_#z#" style="width:300px; height:80px;" onchange="updateQuizSelection(#choice_id#,this.name, this.value)">#text#</textarea>
                          </td>
                        </tr>
                        </table>
                      </td>
                      <td width="350" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>
                          <cfif FileExists(imgFile)>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="200" height="30"></td>
                          </tr>
                          <tr>
                            <td><span class="contentHilighted">File: #url#</span></td>
                            </tr>
                          <tr>
                            <td height="44"><a href="##" class="contentLink" onClick="deleteChoiceImage(#choice_id#)">Remove this Image</a>
                            </td>
                            </tr>
                          </table>
                          </cfif>
                          </td>
                        </tr>
                        <cfif FileExists(imgFile)><cfelse>
                        <tr>
                            <td width="200" height="10"></td>
                          </tr>
                        </cfif>
                        <tr>
                          <td>
                          <input type="file" name="imgURL" id="imgURL_#z#" value="" onchange="updateQuizSelection(#choice_id#, this.name, this.form)" />
                          </td>
                        </tr>
                        <tr>
                        <td>
                        <hr size="1">
                        </td>
                        </tr>
                        <tr>
                          <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="5">
                      <tr>
                        <td align="left" class="content">
                        
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                          <td width="80" align="left" valign="middle">
                            <input name="correct" type="checkbox" id="correct_#z#" value="1" onchange="updateQuizSelection(#choice_id#,this.name, this.checked)" <cfif correct>checked</cfif> /><label for="correct_#z#"></label>
                        <input name="choiceID" type="hidden" id="choiceID" value="#choice_id#">
                            </td>
                            <td height="44" align="left" valign="middle"><div id="respose_#z#"><cfif correct>
                          <span class="contentLinkRed">Choice is Correct</span>
                          <cfelse>
                          <span class="contentLinkGreen">Choice is Incorrect</span>
                            </cfif></div></td>
                            
                          </tr>
                        </table>
                        
                        </td>
                      </tr>
                      </table></td>
                        </tr>
                      </table>
                      </td>
                      <td width="150" align="left" valign="top">
                      <table width="100%" border="0" cellspacing="0" cellpadding="8">
                      <tr>
                        <td height="32" align="right">Order:</td>
                        <td width="10" align="right"><input name="sortOrder" id="sortorder_#z#" type="text" value="#sortorder#" size="2" maxlength="2" style="text-align:center;" onChange="updateQuizSelection(#choice_id#,this.name, this.value)" /></td>
                      </tr>
                    </table>
                    </td>
                      <td width="44" align="right" valign="top">
                      <input name="removeAsset" type="button" id="removeAsset" style="background:url(images/remove.png) no-repeat; border:0; width:44px; height:44px;" onclick="removeQuizChoice(#choice_id#)" value="" />
                      </td>
                    </tr>
                </table>
                
                </form>
                
            	<cfset z++>
                
              </cfloop>
            </cfoutput>
            
            </cfsavecontent>
            
            <cfset theData = {"html":htmlCode,'contentHolder':#contentHolder#}>
        
		<cfreturn theData>
            
            
    </cffunction>


	<cffunction name="getAllAssets" access="remote" returntype="struct">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="AssetTypeID" type="numeric" required="yes">
        <cfargument name="SelectedIDS" type="array" required="no" default="arrayNew(1)">
        <cfargument name="contentHolder" type="string" required="no" default="displayAssets">
        <cfargument name="assetID" type="numeric" required="no" default="0">
        
        <cfinvoke component="Assets" method="getAssetListing" returnvariable="assets">
          <cfinvokeargument name="appID" value="#appID#"/>
          <cfinvokeargument name="AssetTypeID" value="#AssetTypeID#"/>
        </cfinvoke>
        
        <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
            <cfinvokeargument name="clientID" value="#session.clientID#"/>
            <cfinvokeargument name="appID" value="#session.appID#"/>
        </cfinvoke>
             
        <cfsavecontent variable="htmlCode">
	
         <cfset cnt = '0'>
         
         <cfif assetTypeID GT '0'>
             <cfoutput>
             <cfif assets.recordCount IS '0'>
               <div style="height: 44px; padding:  5px; width:auto;">
                    <span class="contentLinkRed">No Assets in this Category Type</span>
                </div>
               </cfif>
             </cfoutput>
         </cfif>

				  <cfoutput query="assets">
                      <cfif assetID NEQ asset_id>
                      <cfset assetPath = assetPaths.types[assetType_ID]>
                    
                      <cfset dateCre = DateAdd("s", created ,DateConvert("utc2Local", "January 1 1970 00:00"))>
                      <cfset dateMod = DateAdd("s", modified ,DateConvert("utc2Local", "January 1 1970 00:00"))>
                      <div class="rowhighlighter" onmouseover="" onmouseout="" style="height: 44px; width:auto; padding-top:5px; padding-bottom:5px; cursor:pointer">
                      
                        
                            <div style="width:44px; height:44px; float:left">
                            <img src="images/#icon#" width="44" height="44" alt="#name#" />
                            </div>

                            <div style="height:32px; width:370px; padding-top:12px; padding-left:10px; float:left" class="contentLink">
                            #assetName#
                            </div>
                            <div style="height:32px; width:80px; padding-top:12px; padding-left:4px; float:left">
                            <cfif image NEQ ''>
                              <a href="#assetPath#thumbs/#image#" target="_new">
                              <img name="#assetName#" src="#assetPath#thumbs/#image#" width="60" height="44" class="imgLockedAspect" />
                              </a>
                            <cfelse>
                              <span class="contentGreyed">No Thumb</span>
                            </cfif>
                            </div>

                            <div style="height:32px; width:32px; padding-left:4px; padding-right:10px; float:right">
                            
                            <cfif ArrayFind(SelectedIDS,asset_id) IS 0>
								<cfset checked = "checkmarkNormal">
                            <cfelse>
                                <cfset checked = "checkmarkSelected">
                            </cfif>
                            
                            <input type="button" id="#asset_id#" class="#checked#" onClick="setCheckmarkState(this);" />
                            </div>
                    
                      </div>
                      </cfif>
                  </cfoutput>
		
        </cfsavecontent>
        
        <cfset theData = {"html":htmlCode,'contentHolder':#contentHolder#}>
        
		<cfreturn theData>
        
	</cffunction>
    
    
    
    <cffunction name="getAllAssetsSelection" access="remote" returntype="string">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="AssetTypeID" type="numeric" required="yes">
        <cfargument name="selectedID" type="numeric" required="no" default="0">
        
        <cfinvoke component="CFC.Assets" method="getAssetListing" returnvariable="assets">
          <cfinvokeargument name="appID" value="#appID#"/>
          <cfinvokeargument name="AssetTypeID" value="#AssetTypeID#"/>
        </cfinvoke>
       
        <cfsavecontent variable="htmlCode">
         <select name="markerAsset" id="markerAsset" style="background-image:url(images/noar.png); background-repeat:no-repeat; height:44px; margin:0px; padding-left:18px; background-position:6px 10px">
         	  <cfif assets.recordCount IS '0'>
              		<option value="0" style="background-image:url(images/noar.png); background-repeat:no-repeat; height:18px; margin:8px; padding-left:32px; padding-top:4px; width:32px, height:32px">No Assets</option>
               <cfelse>
                    <option value="0" style="background-image:url(images/noar.png); background-repeat:no-repeat; height:18px; margin:8px; padding-left:32px; padding-top:4px; width:32px, height:32px">No Object Selected</option>
                    <cfoutput query="assets">
                        <option value="#asset_id#" style="background-image:url(images/sm/#icon#); background-repeat:no-repeat; height:18px; margin:8px; padding-left:32px; padding-top:4px; width:32px, height:32px" <cfif selectedID IS asset_id>selected</cfif>>#assetName#</option>
                    </cfoutput>
              </cfif>
          </select> 
        </cfsavecontent>
        
		<cfreturn htmlCode>
        
	</cffunction>
    
    
    <cffunction name="getAssetTypeIcon" access="remote" returntype="string">
        <cfargument name="assetTypeID" type="numeric" required="no" default="0">
        
        
        <cfinvoke component="CFC.Assets" method="getAssetTypes" returnvariable="assetType">
          <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
        </cfinvoke>
      
        <cfsavecontent variable="htmlCode">
        	<img src="images/<cfoutput>#assetType.icon#</cfoutput>" width="44" height="44" />
        </cfsavecontent>
        
		<cfreturn htmlCode>
        
	</cffunction>
    
    
    
    <!--- Remove AR Marker Asset --->
    <cffunction name="removeARAsset" access="remote" returntype="boolean">
    <cfargument name="AssetID" type="numeric" required="yes">
    
        <cfquery name="updateAR">
            UPDATE ARAssets
            SET markerAsset_ID = NULL
            WHERE asset_id = #AssetID#
        </cfquery>
	
    <cfreturn true>
    
    </cffunction>
    
    
    
    <!--- generate balcony comp asset --->
    <cffunction name="generateBalconyComp" access="remote" returntype="boolean" output="false" hint="creat comp of balcony assets">
        <cfargument name="assetID" type="numeric" required="true"/>
    	<cfargument name="height" type="numeric" required="false" default="320"/>
        
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
                <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="thePath">
                <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfset allViews = arrayNew(1)>
        
        <cfset compName = "#LCase(listGetAt(asset.north,1,'_'))#_comp">
        <cfset rootPath = "../../../#thePath#">
		<cfset theCompImagePath = '#rootPath##compName#'>
        
        <cfset arrayAppend(allViews,rootPath & asset.north)>
        <cfset arrayAppend(allViews,rootPath & asset.east)>
        <cfset arrayAppend(allViews,rootPath & asset.south)>
        <cfset arrayAppend(allViews,rootPath & asset.west)>
        
        <cfset w = 0>
        
        <cfset imgViews = arrayNew(1)>
        
        <cfloop index="aView" array="#allViews#">
           
            <cfimage source="#aView#" name="anImage"> 
            <!--- Turn on antialiasing to improve image quality. ---> 
            <cfset ImageSetAntialiasing(anImage,"on")> 
            <cfset ImageScaleToFit(anImage, "", height,"lanczos")> 
            
            <cfset arrayAppend(imgViews, anImage)>
            <cfset w += anImage.width>
            
        </cfloop>
        
        <cfset newImg = ImageNew("",w,height,"rgb")> 
        <cfset l = 0>
        
        <cfloop index="aView" array="#imgViews#">
        
            <cfset ImagePaste(newImg,aView,l,0)>
            <cfset l+= aView.width>
        
        </cfloop>
        
        <!--- <cfimage source="#newImg#" action="writeToBrowser"> --->
        
        <cfset desPath = rootPath & compName>
        <cfimage source="#newImg#" action="write" destination="#desPath#.jpg" overwrite="yes">
    	
        <cfreturn true>
        
    </cffunction>
    
    
    
<!--- Update Program Asset --->
    <cffunction name="updateProgramAsset" access="remote" returntype="boolean">
    <cfargument name="AssetID" type="numeric" required="yes">
    <cfargument name="objectData" type="struct" required="yes">
    
    <cfif AssetID GT 0>

        <cfquery name="updateProgramAsset">
            UPDATE ProgramAssets
            SET 
            
            duration = <cfif objectData.duration IS 0>NULL<cfelse>#objectData.duration#</cfif>
            
            , isScheduled = #objectData.type#
            
            <cfif objectData.displayAssetID GT 0>
            	,displayAsset_id = #objectData.displayAssetID#
            <cfelse>
            	,displayAsset_id = NULL
            </cfif>
            
            , startTime = <cfif objectData.start GT 0>#Left(objectData.start,10)#<cfelse>NULL</cfif>
            , endTime = <cfif objectData.end GT 0>#Left(objectData.end,10)#<cfelse>NULL</cfif>
            
            WHERE asset_id = #AssetID#
        </cfquery>
	
    </cfif>
    
    <cfreturn true>
    
    </cffunction>
    
    
    
    
    
    <!--- Update Program Asset --->
    <cffunction name="getProgramDisplayAssets" access="remote" returntype="string">
    	<cfargument name="appID" type="numeric" required="yes">
    
    	<cfset displayAssets = {"videos":{}, "images":{}}>
        
        <cfif appID GT 0>
        
			<!--- videos --->
            <cfinvoke component="CFC.Assets" method="getAssets" returnvariable="videoAssets">
                    <cfinvokeargument name="AppID" value="#appID#"/>
                    <cfinvokeargument name="AssetTypeID" value="2"/>
            </cfinvoke>
            
            <cfloop query="videoAssets">
                <cfset structAppend(displayAssets.videos, {"#asset_id#":"#assetName#"})>
            </cfloop>
            
            <!--- images --->
            <cfinvoke component="CFC.Assets" method="getAssets" returnvariable="imageAssets">
                    <cfinvokeargument name="AppID" value="#appID#"/>
                    <cfinvokeargument name="AssetTypeID" value="1"/>
            </cfinvoke>
            
            <cfloop query="imageAssets">
                <cfset structAppend(displayAssets.images, {"#asset_id#":"#assetName#"})>
            </cfloop>
            
        </cfif>
        
        <cfset JSON = serializeJson(displayAssets)>   
        
        <cfreturn JSON>
    
    </cffunction>
    
    
    <!--- Get Asset --->
    <cffunction name="getDisplayAsset" access="remote" returntype="struct">
    	<cfargument name="assetID" type="numeric" required="yes">
        
        <cfinvoke component="Assets" method="getAsset" returnvariable="asset">
                <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfinvoke component="CFC.Assets" method="getAssetType" returnvariable="assetType">
                <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#assetID#"/>
                <cfinvokeargument name="server" value="true"/>
        </cfinvoke>
        
        <cfset assetPath = {'url':assetPath & asset.url, 'assetType':assetType}>
        
        <cfreturn assetPath>
    
    </cffunction>
    
    
    
    
    <!--- rotate Image --->
    <cffunction name="rotateImage" access="remote" returntype="boolean">
    	<cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="clockwise" type="boolean" required="no" default="true">
        
     <cfinvoke component="Misc" method="rotateImage" returnvariable="result">
          <cfinvokeargument name="assetID" value="#assetID#"/>
          <cfinvokeargument name="rotateClockwise" value="#clockwise#"/>
      </cfinvoke>
    
    	<cfreturn true>
    
    </cffunction>
    
</cfcomponent>