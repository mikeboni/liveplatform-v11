<cfcomponent>

	<!--- Assets --->
	<cffunction name="getAssetContent" access="remote" returntype="struct" output="yes">
        <cfargument name="assetID" type="numeric" required="no" default="0">
		<cfargument name="active" type="boolean" required="no" default="yes">
        <cfargument name="includeActionsAssets" type="boolean" required="no" default="yes">
   
        <!--- Get Asset Table --->
        <cfinvoke component="Assets" method="getAssetTable" returnvariable="assetTable">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
	
        <cfif assetTable IS 'GroupAssets'> 
     
			<!--- Get Group Asset Info --->
            <cfinvoke  component="Modules" method="getGroupAssets" returnvariable="objectData">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
		
            <cfif arrayLen(objectData.assets) GT 0>																								
                <cfloop collection="#objectData.assets[1]#" item="theKey"></cfloop>
                <cfset theGroupAssetData = objectData.assets[1][theKey]>
			</cfif>
            
            <!--- Get Asset Model --->
           <cfinvoke component="ObjectModels" method="getObjectModel" returnvariable="assetModel">
               <cfinvokeargument name="typeName" value="#assetTable#"/>
               <cfinvokeargument name="objectData" value="#theGroupAssetData#"/>
            </cfinvoke>     

							 										         
		<cfelse>
     
			<!--- Get Asset Data --->
            <cfinvoke  component="Content" method="getAsset" returnvariable="assetData">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
  						
  			<!--- Options Sharable state --->																																																					
  			<cfinvoke  component="Content" method="getAssetOptions" returnvariable="assetOptionsData">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
 																					
            <!--- QueryToStruct --->
             <cfinvoke component="Misc" method="QueryToStruct" returnvariable="objectData">
                <cfinvokeargument name="query" value="#assetData#"/>
             </cfinvoke>
   
            <!--- Get Asset Model --->
            <cfinvoke component="ObjectModels" method="getObjectModel" returnvariable="assetModel">
               <cfinvokeargument name="typeName" value="#assetTable#"/>
               <cfinvokeargument name="objectData" value="#objectData#"/>
               <cfinvokeargument name="includeActionsAssets" value="#includeActionsAssets#"/>
            </cfinvoke>
            
             <cfif structKeyExists(assetOptionsData,'sharable')>
             
				 <cfif assetOptionsData.sharable>  
				 	<cfloop collection="#assetModel#" item="theKey"></cfloop>
					<cfset structAppend(assetModel[theKey], {"sharable": assetOptionsData.sharable})>
				 </cfif>
			 </cfif>
            
        </cfif>
  	 
        <cfreturn assetModel>
        
    </cffunction>
    
    
    
    <!--- Get Assets Sharable, Modified, Access and cached --->
	<cffunction name="getAssetOptions" access="remote" returntype="struct" output="yes">
        <cfargument name="assetID" type="numeric" required="no" default="0">
		<cfargument name="groupID" type="numeric" required="no" default="0">
		
		<cfquery name="assetOptions"> 
            SELECT	accessLevel AS access, sharable, cached, modified, active
            FROM	Groups
            WHERE	0 = 0
			<cfif assetID GT 0>
            
                AND asset_id = #assetID#
                
                <cfif groupID GT 0>
                        AND subgroup_id = #groupID#
                </cfif>
            <cfelse>
            	   AND group_id = #groupID#
            </cfif>
        </cfquery>
     
        <!--- QueryToStruct --->
         <cfinvoke component="Misc" method="QueryToStruct" returnvariable="assetOptionsModel">
            <cfinvokeargument name="query" value="#assetOptions#"/>
         </cfinvoke>

         <cfif isStruct(assetOptionsModel)>
         
         <cfif assetOptionsModel.access LTE 0>
         	<cfset structDelete(assetOptionsModel,'access')>
         </cfif>
         
         <cfif assetOptionsModel.sharable LTE 0>
         	<cfset structDelete(assetOptionsModel,'sharable')>
         </cfif>
         
         <cfif assetOptionsModel.cached LTE 0>
         	<cfset structDelete(assetOptionsModel,'cached')>
         </cfif>
		 
         <!--- if asset not active then NO options or anything --->
         <cfif assetOptionsModel.active LTE 0>
         	<cfset assetOptionsModel = {}>
         </cfif>
         
         <cfelse>
         	<cfset assetOptionsModel = {}>
         </cfif>
  
        <cfreturn assetOptionsModel>
        
    </cffunction>
    


	<!---Get A Asset By Type--->
    <cffunction name="getAsset" access="public" returntype="query" output="yes">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <cfset anAsset = queryNew("name, modified, assetModified, dynamic","VarChar, BigInt, BigInt, bit")>
        
        <cfif assetID GT 0>
        
        	<!--- Get Asset Table --->
            <cfinvoke component="Assets" method="getAssetTable" returnvariable="assetTable">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
           
           	<cfif assetTable IS ''>
           		No Asset Table: <cfdump var="#assetTable#"><br>
				AssetID: <cfdump var="#assetID#"> 
				<cfinvoke component="Misc" method="CallStackDump">
				<cfabort>
			<cfelse>
           
			<!--- Get All Data from Asset Table--->
            <cfquery name="anAsset"> 
                SELECT	#assetTable#.*, Assets.name, Assets.modified AS assetModified,  Assets.dynamic AS dynamic
                FROM	#assetTable# LEFT OUTER JOIN Assets ON #assetTable#.asset_id = Assets.asset_id
                <cfif AssetID GT 0>
                WHERE 	#assetTable#.asset_id = #AssetID#
                </cfif>
            </cfquery>
            
			</cfif>
       
        </cfif>
     
        <cfreturn anAsset>
        
    </cffunction>



	<!--- Group Assets --->
	<cffunction name="getGroupContent" access="remote" returntype="array">
		<cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="appID" type="numeric" required="no" default="0">
		<cfargument name="active" type="boolean" required="no" default="yes">
        <cfargument name="filter" type="string" required="no" default="">
        <cfargument name="viewType" type="numeric" required="no" default="0">
        <cfargument name="reservedType" type="numeric" required="no" default="0">
      
		<!--- Get Group Assets --->
        <cfquery name="assets">
            SELECT   asset_id, accessLevel, cached, sharable, group_id, modified, name, viewType_id, reservedType_id
            FROM     Groups
            WHERE    (subgroup_id = #groupID#) <cfif groupID IS 0>AND app_id = #appID#</cfif>
            
            <cfif active>AND active = 1</cfif>
            
            <cfif filter NEQ ''>
            		AND NOT(
                <cfset cnt = 0>
            	<cfloop index="aFilter" list="#filter#">
                	<cfif cnt GT 0>OR</cfif> name = '#trim(aFilter)#' 
                <cfset cnt++>
                </cfloop>
                	)
            </cfif>
            
		   <cfif viewType GT 0>
		   		AND viewType_id = #viewType#
		   </cfif>
           <cfif reservedType GT 0>
           		AND reservedType_id = #reservedType#
           </cfif>
            
            ORDER BY sortOrder
        </cfquery>

        <!--- Get Root Group Active --->
        <cfquery name="assetsRoot">
            SELECT   active
            FROM     Groups
            WHERE    (group_id = #groupID#) <cfif groupID IS 0>AND app_id = #appID#</cfif>
            ORDER BY sortOrder
        </cfquery>
        
        <cfif assetsRoot.recordCount GT 0>
        	<cfset activeRoot = assetsRoot.active>
		<cfelse>
        	<cfset activeRoot = true>
        </cfif>
        
        <cfset theData = arrayNew(1)>
        
        <cfif activeRoot>

        <cfoutput query="assets">
         
			<!--- Thumbs, Details and Colors --->
            <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
                <cfinvokeargument name="groupID" value="#group_id#">
            </cfinvoke>

            <cfset theObj = {"assets":[], "modified":modified, "groupID":group_id, "assetID":asset_id}>
            <cfif viewType_id GT 0>
            	<cfset structAppend(theObj,{"viewType":viewType_id, "reservedType": reservedType_id})>
            </cfif>
            <cfset structAppend(theObj,assetData)>
            
            <cfif sharable IS 1>
            	<cfset structAppend(theObj,{"sharable":sharable})>
            </cfif>
            
            <cfif accessLevel GT 0>
            	<cfset structAppend(theObj,{"access":accessLevel})>
            </cfif>
            
            <cfif cached IS 1>
            	<cfset structAppend(theObj,{"cached":cached})>
            </cfif>
            
            <cfset theObjModel = {"#name#":theObj}>

            <!--- Clean --->
            <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
                <cfinvokeargument name="theStruct" value="#theObjModel#"/>
            </cfinvoke>
        
        	
            <!--- Check if a Code Exists --->
        	<cfinvoke  component="Access" method="getGroupAccessCodes" returnvariable="accessCodes">
                <cfinvokeargument name="groupID" value="#group_id#"/>
            </cfinvoke>
            
            <cfif accessCodes.recordCount GT 0>
            	<cfset structAppend(theObj,{"code":1})>
            </cfif>
            
            <cfset arrayAppend(theData,theObjModel)>
            
         </cfoutput>
		
        </cfif>

        <cfreturn theData>
        
    </cffunction>



	<!--- Group Info --->
	<cffunction name="getGroupInfo" access="remote" returntype="struct">
		<cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="appID" type="numeric" required="no" default="0">
		<cfargument name="active" type="boolean" required="no" default="yes">
        
		<!--- Get Group Assets --->
        <cfquery name="group">
            SELECT   asset_id, accessLevel, cached, sharable, modified, name
            FROM     Groups
            WHERE    (group_id = #groupID#) 
            
			<cfif active>
            	AND active = 1
            <cfelse>
           		AND active = 0
            </cfif>
            
            <cfif appID GT 0>AND app_id = #appID#</cfif>
        </cfquery>
		
        <cfif group.recordCount IS 0>
        	<cfreturn {}>
        </cfif>
        
		<!--- Thumbs, Details and Colors --->
        <cfinvoke component="ObjectModels" method="assetInfo" returnvariable="assetData">
            <cfinvokeargument name="groupID" value="#groupID#">
        </cfinvoke>
    
        <cfset theObj = {"assets":[], "actions":[], "modified":group.modified, "groupID":groupID, "access":group.accessLevel, "sharable":group.sharable, "cached":group.cached}>
    
        <cfset structAppend(theObj,assetData)>
        
        <!--- Sharable, Access and Cached --->
        <cfif theObj.sharable IS 0>
        	<cfset structDelete(theObj,'sharable')>
        </cfif>
        <cfif theObj.cached IS 0>
        	<cfset structDelete(theObj,'cached')>
        </cfif>
        <cfif theObj.access IS 0>
        	<cfset structDelete(theObj,'access')>
        </cfif>
        
        <!--- <cfset theObjModel = {"#group.name#":theObj}> --->
        <cfset theObjModel = theObj>
        
        <!--- Clean --->
        <cfinvoke  component="Modules" method="cleanUpStruct" returnvariable="theObjModel">
            <cfinvokeargument name="theStruct" value="#theObjModel#"/>
        </cfinvoke>
        
        <cfreturn theObjModel>
        
    </cffunction>




	<!--- Assets and Actions --->
	<cffunction name="getAssetsActions" access="remote" returntype="struct">
		<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="active" type="boolean" required="no" default="yes">
		
        <cfset assets_actions = {"actions":[],"assets":[]}>
      
        <cfif assetID GT 0>
        
			<!--- Get Group Assets --->
            <cfquery name="assets">
                SELECT   content_id AS asset_id, accessLevel, cached, sharable, sortOrder, action, quiz
                FROM     GroupAssets
                WHERE    (asset_id = #assetID#) 
                <cfif active> AND active = 1<cfelse>AND active = 0</cfif>
                ORDER BY sortOrder
            </cfquery>
    
            <!--- QueryToStruct --->
             <cfinvoke component="Misc" method="QueryToStruct" returnvariable="theData">
                <cfinvokeargument name="query" value="#assets#"/>
                <cfinvokeargument name="forceArray" value="true"/>
             </cfinvoke>
    
            <cfloop index="aRec" array="#theData#">
            
                <cfif aRec.action>
                    <cfset arrayAppend(assets_actions.actions,aRec)>
                <cfelseif aRec.action IS 0 AND aRec.quiz IS 0>
                    <cfset arrayAppend(assets_actions.assets,aRec)>
                </cfif>
            
            </cfloop>
        
        </cfif>

        <cfreturn assets_actions>
        
    </cffunction>
    
    
    
    <!--- Get All Content Groups --->
	<cffunction name="getGroups" access="remote" returntype="array">
		<cfargument name="appID" type="numeric" required="yes">
		
		<!--- Get Group Assets --->
        <cfquery name="groups">
            SELECT DISTINCT group_id, subgroup_id, active
  			FROM Groups
  			WHERE app_id = #appID# AND asset_id IS NULL
        </cfquery>
																									
        <cfset theData = arrayNew(1)>
        <cfset removeData = arrayNew(1)>

        <cfoutput query="groups">
      	
            <cfinvoke  component="Content" method="getOriginGroupState" returnvariable="originData">
                <cfinvokeargument name="groupID" value="#group_id#"/>
            </cfinvoke>
    	
        	<cfif originData.active IS 0 OR active IS 0>
				<!--- nothing --->
            <cfelse>
            
				<cfif arrayFind(removeData,subgroup_id) IS 0>
                    <cfset arrayAppend(theData,group_id)>
                </cfif>
            
            </cfif>
            
		</cfoutput>
 	
        <cfreturn theData>
        
    </cffunction>
    
    
     <!--- Get Origin Group info --->
	<cffunction name="getOriginGroupState" access="remote" returntype="struct" output="yes">
		<cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="trackGroups" type="array" required="no" default="#arrayNew(1)#">
           
            <cfset data = {}>
            
			<!--- Get Group Assets --->
            <cfquery name="group">
                SELECT subgroup_id, active, sharable, cached
                FROM Groups
                WHERE group_id = #groupID#
            </cfquery>
            
            <cfset subgroupID = group.subgroup_id>
  			
            <cfif subgroupID GT 0>
                
                <cfif ArrayFind(trackGroups, subgroupID) IS 0>
                
                	<cfset arrayAppend(trackGroups, subgroupID)>
                	
					<cfinvoke  component="Content" method="getOriginGroupState" returnvariable="data">
						<cfinvokeargument name="groupID" value="#subgroupID#"/>
						<cfinvokeargument name="trackGroups" value="#trackGroups#"/>
					</cfinvoke>
				<cfelse>
					<!--- recursive forever get out --->
					<cfset data = {"active":group.active,"sharable":group.sharable,"cached":group.cached}>
				</cfif>
                
            <cfelse>
            
				<cfset data = {"active":group.active,"sharable":group.sharable,"cached":group.cached}>
		
            </cfif>
            
            <cfreturn data>
            
     </cffunction>   
    
    
    
    
    
    <!--- Get All Content Groups --->
	<cffunction name="getJSONPath" access="remote" returntype="string" output="no">
		<cfargument name="appID" type="numeric" required="yes">
            
		<!--- Build JSON Client/App Path --->
        <cfquery name="appPath">
            SELECT       Clients.path + '/' + Applications.path + '/' AS filePath
            FROM         Clients INNER JOIN Applications ON Clients.client_id = Applications.client_id 
            WHERE        Applications.app_id = #appID#
        </cfquery> 
        
        <cfset path = appPath.filePath>

        <!--- Server API Version --->
        <cfinvoke  component="Apps" method="getAppPrefs" returnvariable="prefs">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
        
        <cfset apiVr = prefs.server_API>
        
        <cfif apiVr GTE 10>
            <cfset jsonPath = path & "JSON_" & apiVr &"/">
        <cfelse>
            <cfset jsonPath = path & "JSON/">
        </cfif>
        
        <cfreturn jsonPath>
        
	</cffunction>
    
    
    
    <!--- Get All Special Content --->
	<cffunction name="getSpecialContent" access="remote" returntype="struct">
		<cfargument name="appID" type="numeric" required="no">
        <cfargument name="groupName" type="string" required="no">   
		<!--- <cfargument name="array" type="numeric" required="no" default="0"> --->

           
        <!--- find all groups with groupName --->
         <cfquery name="specialContent"> 
            SELECT group_id AS groupID, subgroup_id AS subgroupID
            FROM Groups where app_id = #appID# 
            AND name = '#groupName#'
        </cfquery> 
        
         <cfif groupName IS 'Theme'>
			<cfset isTheme = true>
		<cfelse>
			<cfset isTheme = false>
		</cfif> 
 
		 <cfset specialStruct = structNew()>
 
        <!--- NEW --->
        <cfoutput query="specialContent">

            <cfinvoke component="Modules" method="GroupAssetIsActive" returnvariable="activeGroupAsset">
				<cfinvokeargument name="groupID" value="#groupID#"/>
			</cfinvoke> 
			
			<cfif activeGroupAsset>

				<cfquery name="assets">
					SELECT        Groups.asset_id AS assetID, group_id AS groupID
					FROM          Groups INNER JOIN Assets ON Groups.asset_id = Assets.asset_id
					WHERE         (Groups.subgroup_id = #groupID#) AND (Groups.active = 1)
					ORDER BY 	  SortOrder ASC
				</cfquery>

				<cfif isTheme>
					<cfset allAssets = {}>
				<cfelse>
					<cfset allAssets = []>
				</cfif>
				
				<cfloop query="assets">
					
					<cfinvoke  component="Content" method="getAssetContent" returnvariable="assetData">
						<cfinvokeargument name="assetID" value="#assetID#"/>
					</cfinvoke>
	
					<cfif isTheme>
						<cfset structAppend(allAssets, assetData)>
					<cfelse>
						<cfset arrayAppend(allAssets, assetData)>
					</cfif>
						
				</cfloop>
	
				<cfif groupName IS 'Breadcrumb'>
				
					<cfinvoke component="Content" method="getGroupInfo" returnvariable="theGroupInfo">
						<cfinvokeargument name="groupID" value="#groupID#"/>
					</cfinvoke>
				
					<cfset structAppend(specialStruct, {"#subgroupID#": [theGroupInfo]})>
					
				<cfelse>
					<cfset structAppend(specialStruct, {"#subgroupID#": allAssets})>
				</cfif>
				
				
				
			</cfif>
            	
 		 </cfoutput>

        <cfreturn specialStruct>
      
	</cffunction>
   
   
   <!--- Generate Special Content --->
    <cffunction name="generateSpecialContent" access="remote" returntype="string" returnformat="plain" output="no" hint="Gets special folder content like THEME">
        <cfargument name="bundleID" type="string" required="no" default="">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="groupName" type="string" required="no" default="">
        <cfargument name="array" type="numeric" required="no" default="0">
        <cfargument name="dev" type="numeric" required="no" default="0">

        <cfif appID IS 0>
        
			<cfif bundleID IS ''>
				<!---no bundle id --->
				<cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
					<cfinvokeargument name="error_code" value="1002"/>
				</cfinvoke>
			<cfelse>  
				<!--- Get AppID from BundleID --->
				<cfinvoke component="CFC.Apps" method="getAppID" returnvariable="appInfo">
					<cfinvokeargument name="bundleID" value="#bundleID#">
				</cfinvoke>

				<cfset appID = appInfo.app_id>

			</cfif>
			
		</cfif>
	
		<cfif appID IS 0 OR groupName IS ''>
            
            <!---no data--->
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1013"/>
            </cfinvoke>
            
        <cfelse>
            <!---ok--->
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
            
            <!--- Get JSON Path --->
			<cfinvoke  component="Apps" method="getJSONPath" returnvariable="JSONPath">
				<cfinvokeargument name="appID" value="#appID#">
			</cfinvoke>
			
			
			<cfset JSONPath = replace(JSONPath,"API\","","all")>

			<!--- if DEV then add DEV folder pto path --->
			<cfif dev IS 1>
				<cfset JSONPath = JSONPath & 'DEV/'>
			</cfif>

			<!--- build path --->          
			<cfset JSONFilePath = JSONPath & 'special_'& groupName &'.json'> 

			
			<cfset data = structNew()>
			<cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate"></cfinvoke>
			<!--- THREAD OPERATIONS --->
			<cfset uniqueThread = CreateUUID()>
			<cfthread action="run" name="special_#uniqueThread#" appID="#appID#" groupName="#groupName#" array="#array#" JSONFilePath="#JSONFilePath#" data="#data#" curDate="#curDate#">
			
				<!--- <cfloop query="#aliasAppID#"> --->

					<!--- <cfif aliasAppID.alias_app_id NEQ ''>
						<cfset appID = aliasAppID.alias_app_id>
					<cfelse> 
						<cfset appID = aliasAppID.app_id>
					</cfif> --->

					<cfinvoke component="Content" method="getSpecialContent" returnvariable="content">
						<cfinvokeargument name="appID" value="#appID#"/>
						<cfinvokeargument name="groupName" value="#groupName#"/>
						<cfinvokeargument name="array" value="#array#"/>
					</cfinvoke>
						
					<!--- ADD DATE TO STRUCT --->
					
					<cfset structAppend(content, {'modified':curDate})>
					
					<cfset structAppend(data, content)>
					<cfset structAppend(data,{"error":#error#})>

					<cfset dataContentJson = serializeJSON(data)>
				
					<cftry>
						<cffile action="write" file="#JSONFilePath#" output="#dataContentJson#" charset="utf-8" mode="777">
						<cfset fileuploaded = true>
						<cfcatch>
							<cfset fileuploaded = false>
						</cfcatch>
					</cftry>

					<!--- <cfif fileExists(JSONFilePath)>TRUE<cfelse>FALSE</cfif>
					<cfdump var="#error#"><br><cfdump var="#JSONFilePath#"><cfabort> --->

				<!--- </cfloop> --->
        
			</cfthread>
       
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})>
        
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>

  	</cffunction>
  	
  	
  	<!--- Generate Asset Libraryr --->
  	<cffunction name="generateProjectAssets" access="remote" returntype="struct" output="no" hint="project assets">
       <cfargument name="auth_token" type="string" required="no" default="">
       <cfargument name="appID" type="string" required="no" default="0">
       <cfargument name="byAssetID" type="string" required="no" default="true">
       <cfargument name="filePath" type="string" required="no" default="">
       
       <cfif auth_token NEQ ''>
       
			<!--- get appID from Token --->
			<cfinvoke component="CFC.Tokens" method="getToken" returnvariable="tokenInfo">
				<cfinvokeargument name="auth_token" value="#auth_token#"/>
			</cfinvoke>

			<cfset appID = tokenInfo.appID>
        
		</cfif>
        
		<!--- Get Data of Assets Lib --->
		<cfinvoke component="Content" method="generateAssetsLib" returnvariable="data">
			<cfinvokeargument name="appID" value="#appID#">
			<cfinvokeargument name="byAssetID" value="#byAssetID#">
		</cfinvoke>

		<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
			<cfinvokeargument name="error_code" value="4000">
		</cfinvoke> 

		<cfset structAppend(data,{"error":#error#})>
		<cfset JSON = serializeJSON(data)>
		
		
		
		<cfif filePath IS ''>
			
			<!--- Get JSON Path --->
			<cfinvoke component="CFC.Apps" method="getJSONPath" returnvariable="JSONPath">
				<cfinvokeargument name="appID" value="#appID#">
			</cfinvoke>
        
			<cfset filePath = JSONPath & "contentAssets.json">

		</cfif>
		
		<cffile action="write" file="#filePath#" output="#JSON#" charset="utf-8"> 
		
  		<cfreturn data>
   		
   </cffunction>
   
   
   <cffunction name="generateAssetsLib" access="remote" returntype="struct" output="no" hint="project assets">
       <cfargument name="appID" type="string" required="yes" default="">
       <cfargument name="byAssetID" type="string" required="no" default="true">
       
		<cfquery name="projectAssets">
			SELECT       Groups.asset_id AS assetID, Assets.name AS name, Groups.group_id AS groupID
			FROM         Groups INNER JOIN Assets ON Groups.asset_id = Assets.asset_id
			WHERE        (Groups.app_id = #appID#) AND (Groups.asset_id IS NOT NULL) AND (active = 1)
		</cfquery>

		<cfset data = {}>

		<cfloop query="#projectAssets#">

			<cfinvoke  component="CFC.Content" method="getAssetContent" returnvariable="assetData">
				<cfinvokeargument name="assetID" value="#assetID#"/>
			</cfinvoke>
			
			<cfif byAssetID>
			
				<cfloop collection="#assetData#" item="theKey"></cfloop>
				
				<cfset structAppend(assetData[theKey], {"name": "#name#"})>
				
				<cfset assetIDTag = assetData[theKey].assetID>
				
				<cfset assetDataStruct = {#assetIDTag#: assetData[theKey]}>
				
				<!--- <cfset assetData[theKey].assetID = groupID> --->
				
				<cfset structAppend(data, assetDataStruct)>

			<cfelse>
			
				<cfloop collection="#assetData#" item="theKey"></cfloop>
				<!--- <cfset data[theKey].assetID = groupID> --->
				<cfset structAppend(data, assetData)>
	
			</cfif>

		</cfloop>
		
  		<cfreturn data>
   		
   </cffunction>
    
        
</cfcomponent>



