<cfcomponent>

<!---Build App File Path--->
	<cffunction name="buildCurrentFileAppPath" access="public" returntype="string" output="yes">
    
		<cfargument name="assetTypeID" type="numeric" required="no" default="0">
        
        <cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="string" required="no" default="0">
        <cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        
        <cfargument name="images" type="boolean" required="no" default="no">
        <cfargument name="nonretina" type="boolean" required="no" default="no">
        <cfargument name="thumbs" type="boolean" required="no" default="no">

        <cfargument name="server" type="boolean" required="no" default="false">
        <cfargument name="relative" type="boolean" required="no" default="false">
        
        <!--- Current Relative Root Directory Path --->
        <cfinvoke component="File" method="getRootPath" returnvariable="rootpath" />
        
        <cfif appID IS ''><cfset appID = 0></cfif>
        <cfset path = "">
    
        <cfquery name="apps">
        	SELECT client_id 
            FROM Applications 
            WHERE 0 = 0
            <cfif clientID GT '0'>
            AND client_id = #clientID#
            </cfif>
            <cfif appID GT '0'>
            AND app_id = #appID#
            </cfif>
        </cfquery>
        
        <cfif apps.recordCount GT '0'>
        	<cfset appExists = true>
        <cfelse>
        	<cfset appExists = false>
        </cfif>
    
        <cfif clientID GT '0' OR appID GT '0' OR assetID GT '0' OR groupID GT '0'>
        	<!---nothing--->
        <cfelse>
        	<cfreturn path>
        </cfif>
  
        
        <cfif assetID GT '0'>
           
        <!---Path based on asset ID--->
        	<cfquery name="appPath">
                SELECT        Clients.path + '/' +  Applications.path + '/' + 'assets/' +  AssetTypes.path + '/' AS filePath, Assets.app_id
                FROM            Assets INNER JOIN
                                         AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id INNER JOIN
                                         Applications ON Assets.app_id = Applications.app_id INNER JOIN
                                         Clients ON Applications.client_id = Clients.client_id AND Applications.client_id = Clients.client_id
                WHERE        (Assets.asset_id = #assetID#)
            </cfquery>
           
           <cfset appID = appPath.app_id>
		
        <cfelseif groupID GT '0'>
        
        <!---Path based on group ID--->
        	<cfquery name="appPath">
                SELECT         Clients.path + '/' +  Applications.path + '/' + 'assets/other/' AS filePath, Groups.app_id
                FROM            Clients INNER JOIN
                                         Applications ON Clients.client_id = Applications.client_id AND Clients.client_id = Applications.client_id INNER JOIN
                                         Groups ON Applications.app_id = Groups.app_id
                WHERE        (Groups.group_id = #groupID#)
            </cfquery>
           
           <cfset appID = appPath.app_id>

        <cfelse>
        
        <!---Path based on ClientID, AppID and AssetTypeID--->
            <cfquery name="appPath">
                SELECT        Clients.path + '/' 
                            <cfif appID GT '0'>
                                + Applications.path + '/' 
                            </cfif>
                            <cfif assetTypeID GT '0'>
                                + 'assets/' + AssetTypes.path +'/'
                            </cfif>
                             AS filePath
                            
                FROM          Clients 
                              <cfif appExists>
                              INNER JOIN Applications ON Clients.client_id = Applications.client_id 
                                  <cfif assetTypeID GT '0'>
                              CROSS JOIN AssetTypes
                                  </cfif>
                              </cfif>
                              
                WHERE        0=0
                
                            <cfif clientID GT '0'>
                                AND (Clients.client_id = #clientID#) 
                            </cfif>
                            
                            <cfif appID GT '0'>
                                AND (Applications.app_id = #appID#) 
                            </cfif>
                            
                            <cfif assetTypeID GT '0'>
                                AND (AssetTypes.assetType_id = #assetTypeID#)
                            </cfif>
            </cfquery>  
        
        </cfif>
   	
        <!--- Check if any Data Returned --->
        <cfif appPath.recordCount IS '0'>
        	<cfset appPath = queryNew("filePath","VarChar")>
        </cfif>
    
        <cfset path = "">
        
        <!--- Path Type (Subfolder types) --->
        <cfif nonretina>
        	<cfset retinaFolder = 'nonretina/'>
        <cfelse>
        	<cfset retinaFolder = "">
        </cfif>
        
        <cfif thumbs>
        	<cfset folderType = 'thumbs/#retinaFolder#'>
        <cfelseif images AND assetID IS '0'>
        	<cfset folderType = 'images/#retinaFolder#'>
        <cfelseif assetID GT '0'>
        	<cfset folderType = '#retinaFolder#'>
        <cfelse>
        	<cfset folderType = ''>
        </cfif>
        
        <cfif appPath.filePath NEQ "">

            <cfif server>
            
                <cfquery name="appServerPath">
                    SELECT        Servers.server_url
                    FROM          Prefs INNER JOIN
                                  Servers ON Prefs.server_id = Servers.server_id INNER JOIN
                                  Applications ON Prefs.prefs_id = Applications.prefs_id
                    WHERE         0 = 0
                                  <cfif appID GT '0'>
                                  AND (Applications.app_id = '#appID#')
                                  </cfif>
                                  <cfif clientID GT '0'>
                                  AND (Applications.client_id = '#clientID#')
                                  </cfif>
                </cfquery>
       
            	<cfset path = "http://#appServerPath.server_url#/#appPath.filepath##folderType#">
    
            <cfelse>
            	<!--- Relative Path --->
            	<cfif relative>
                	<cfset path = "../../" & appPath.filePath & folderType>
                <cfelse>
                	<cfset path = appPath.filePath & folderType>
                </cfif>
                
            </cfif>
            
		<cfelse>
        	<cfset path = "">
        </cfif>
		
		<cfreturn path>    
        
	</cffunction>
    
    
    
    
<cffunction name="getFileSpecs" access="public" returntype="struct" output="yes">   
		<cfargument name="fullPath" type="string" required="yes">
            
		<!--- File --->
        <cfset fileName = GetFileFromPath(fullPath)>
        
        <!--- File Name and Ext --->
        <cfset theFileName = ListFirst( filename , '.' )>
        <cfset theFileExt = ListLast( filename , '.' )>
        
        <!--- Path --->
        <cfset theFilePath = GetDirectoryFromPath(fullPath)>
		
        <cfset fileSpec = structNew()>
        
        <cfif trim(theFileName) IS trim(theFileExt)>
        	<!--- Folder --->
        	<cfset fileSpec = {"type":"folder", "file":fileName, "folder":theFileName, "ext":"","path":theFilePath}>
        <cfelse>
        	<!--- File --->
        	<cfset fileSpec = {"type":"file", "file":fileName, "filename":theFileName, "ext":theFileExt,"path":theFilePath}>
        </cfif>
        
        <cfreturn fileSpec>
        
</cffunction>    
    

<!---Make Unique File Name--->
	<cffunction name="makeFileUnique" access="public" returntype="string">   
		<cfargument name="fullPath" type="string" required="yes">
        
        <!--- File Specs--->
        <cfinvoke component="File" method="getFileSpecs" returnvariable="fileSpecs">
            <cfinvokeargument name="fullPath" value="#fullPath#"/>
        </cfinvoke>
        
        <!--- File --->
        <cfset fileName = fileSpecs.file>
        
        <!--- File Name and Ext --->
        <cfset theFileName = fileSpecs.fileName>
        <cfset theFileExt = fileSpecs.ext>
        
        <!--- Path --->
        <cfset filePath = fileSpecs.path>

        <cfset i = 0>
     
        <cfloop condition="fileExists('#fileSpecs.path##fileName#')">
			<cfset fileName = '#fileSpecs.fileName#_#i#.#fileSpecs.ext#'>
            <cfset i += 1>
            <cfif i GT 100><cfbreak /></cfif>
        </cfloop>
        
        <cfif i IS '0'>
			<cfset fileName = fullPath>
        <cfelse>
        	<cfset fileName = fileSpecs.path & fileName>
        </cfif>

	<cfreturn fileName>

</cffunction>


	<!---File Exists--->
	<cffunction name="fileExists" access="public" returntype="struct">
    	<cfargument name="fullPath" type="string" required="yes">
		<cfargument name="makeUnique" type="boolean" required="no" default="no">
        
        <cfif fileExists(fullPath)>
        
        <cfif makeUnique>
        
            <cfinvoke component="File" method="makeFileUnique" returnvariable="fullPath">
                <cfinvokeargument name="fullPath" value="#fullPath#"/>
            </cfinvoke>
        
        </cfif>
        	
            <cfset error = {"exists":true,"path":fullPath}>
            
        <cfelse>
        	<cfset error = {"exists":false,"path":fullPath}>
        </cfif>
        
        <cfreturn error>
        
	</cffunction>
    
    
    
    
<!---Delete Temp File--->
	<cffunction name="deleteTempFile" access="public" returntype="string">
    
		<cfargument name="file" type="string" required="yes">
		
		<cfset theFilePath = GetTempDirectory() & file>
        
        <cfif fileExists(theFilePath)>
        
        	<cfset uniqueThread = RandRange( 1, 1000 )>
            
            <cfthread action="run" name="deletFile_#uniqueThread#" theFilePath="#theFilePath#">
            	<cffile action="delete" file="#theFilePath#">
            </cfthread>
            
        	<cfreturn true>
            
        <cfelse>
            <cfreturn false>
        </cfif>
        
	</cffunction>
  
  
  
    
    <!---Folder Exists--->
    <cffunction name="folderExists" access="public" returntype="boolean">   
		<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
		<cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="assetTypeID" type="numeric" required="no" default="0">
        
        <cfargument name="images" type="boolean" required="no" default="no">
        <cfargument name="nonretina" type="boolean" required="no" default="no">
        <cfargument name="thumbs" type="boolean" required="no" default="no">
        
        <cfargument name="createFolder" type="boolean" required="no" default="no">
        
        <!--- Get Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="folderPath">
            <cfinvokeargument name="assetID" value="#assetID#"/>
            <cfinvokeargument name="groupID" value="#groupID#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="clientID" value="#clientID#"/>
            <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
            
            <cfinvokeargument name="images" value="#images#"/>
            <cfinvokeargument name="nonretina" value="#nonretina#"/>
            <cfinvokeargument name="thumbs" value="#thumbs#"/>
        </cfinvoke>
		
        <cfset folderPath = expandPath('../../'&folderPath)>
        
		<cfif NOT directoryExists(folderPath)>
        
			<cfif createFolder AND folderPath NEQ ''>
           
            	<!--- Create Directory --->
                <cfdirectory action="create" directory="#folderPath#" mode="777">
                
                <!--- Folder Exist? --->
                <cfinvoke component="file" method="folderExists" returnvariable="dirExists">
                    <cfinvokeargument name="assetID" value="#assetID#"/>
                    <cfinvokeargument name="groupID" value="#assetID#"/>
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="clientID" value="#clientID#"/>
                    <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
                    
                    <cfinvokeargument name="images" value="#images#"/>
                    <cfinvokeargument name="nonretina" value="#nonretina#"/>
                    <cfinvokeargument name="thumbs" value="#thumbs#"/>
                </cfinvoke>
                
            <cfelse>
            	<cfset dirExists = false>
            </cfif>
                        
        <cfelse>
        	<cfset dirExists = true>
        </cfif>
        
        <cfreturn dirExists>
        
	</cffunction>
    
    
    
    
    <!---Delete Folder--->
    <cffunction name="folderDelete" access="public" returntype="boolean" output="yes">   
		<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
		<cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="assetTypeID" type="numeric" required="no" default="0">
        
        <cfargument name="nonretina" type="boolean" required="no" default="no">
        <cfargument name="thumbs" type="boolean" required="no" default="no">
        <cfargument name="images" type="boolean" required="no" default="no">
        
        <!--- Get Root Path --->
        <cfinvoke component="File" method="getRootPath" returnvariable="rootPath" />
        
        <!--- Get Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="rootFolderPath">
            <cfinvokeargument name="assetID" value="#assetID#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="clientID" value="#clientID#"/>
            <cfinvokeargument name="assetTypeID" value="#assetTypeID#"/>
            
            <cfinvokeargument name="nonretina" value="#nonretina#"/>
            <cfinvokeargument name="thumbs" value="#thumbs#"/>
            <cfinvokeargument name="images" value="#images#"/>
        </cfinvoke>

       <cfif NOT directoryExists(folderPath)>
             <cfreturn false>
        <cfelse>
        	
            <!--- <cfset DirectoryDelete(folderPath,true)> --->
			<cfreturn true>
            
        </cfif>
        
    </cffunction>
    
    
    
    
    
     <!---Get Image Size--->
    <cffunction name="getImageSize" access="remote" returntype="struct">   
		<cfargument name="imagePath" type="string" required="yes" default="">
		
        <cfset imageSize = {'width':0 ,'height':0}>
        
		<cfif imagePath NEQ ''>
        
			<cfif fileExists(imagePath)>
            
                <cfimage action="info" source = "#imagePath#" structname = "imageSrc">
                
                <cfset imageSize.width = imageSrc.width>
                <cfset imageSize.height = imageSrc.height>
            <cfelse>
                <cfreturn {"error":"File does not exist"}>
            </cfif>

        </cfif>
        
		<cfreturn imageSize>
        
    </cffunction>
    
    
    
    
    
    <!---Get AssetType Path--->
    <cffunction name="getAssetPath" access="remote" returntype="string">   
		<cfargument name="assetType" type="numeric" required="no" default="0">
		
        <cfif assetType GT 0>
        
            <cfquery name="assetPaths">
                SELECT        path
                FROM          AssetTypes
                WHERE        assetType_id = #assetType#
            </cfquery>
            
            <cfset assetPath = assetPaths.path>
        
        <cfelse>
        	<cfset assetPath = ''>
        </cfif>
        
		<cfreturn assetPath>
        
    </cffunction>
    
    
    
    <!--- Get Root Path --->
    <cffunction name="getRootPath" access="remote" returntype="string"> 
    
    <cfset theDir = getDirectoryFromPath(GetBaseTemplatePath())>
        
        <cfset serverPath = false>
        <cfif mid(theDir,1,1) IS "E">
			<cfset serverPath = true>
        </cfif>
 
        <cfif serverPath>
            <cfset dir = "">
            <cfset del = "\">
        <cfelse>
            <cfset dir = "/">
            <cfset del = "/">
        </cfif>
        
        <cfset APIDir = ListFind(theDir,"API",del) - 1>
        
        <cfloop index="z" from="1" to="#APIDir#">
            <cfset dir = dir & listGetAt(theDir,z,del) & del>
        </cfloop>
    	
        <cfreturn dir>
        
    </cffunction>
    
    
    
    
    <!--- Get App Root - Server Path --->
    <cffunction name="getServerPath" access="remote" returntype="string">   
		<cfargument name="appID" type="numeric" required="yes" default="0">
        
    	<cfquery name="appServerPath">
            SELECT        Servers.server_url
            FROM          Prefs INNER JOIN
                          Servers ON Prefs.server_id = Servers.server_id INNER JOIN
                          Applications ON Prefs.prefs_id = Applications.prefs_id
            WHERE        (Applications.app_id = '#appID#')
        </cfquery>
   		
        <cfquery name="appPath">
            SELECT        Clients.path + '/' + Applications.path + '/' AS filePath
            FROM          Clients INNER JOIN Applications ON Clients.client_id = Applications.client_id 
            WHERE        (Applications.app_id = #appID#) 
        </cfquery>
        
        <cfset path = "http://#appServerPath.server_url#/#appPath.filepath#">
        
        <cfreturn path>
            
    </cffunction>
    
    
    
     <!--- Get App Root - Server Path --->
    <cffunction name="getURLPath" access="remote" returntype="struct">   
		<cfargument name="url" type="string" required="yes" default="0">
        
        <cfset data = structNew()>
  		<cfset thePath = ''>
		
        <cfset aFile = getFileFromPath(url)>
         
        <cfloop index="anItem" list="#url#" delimiters="/">
        
        	<cfif findNoCase('.',anItem) OR findNoCase(':',anItem)>
            <!--- nothing --->
            <cfelse>
            	<cfset thePath = thePath & anItem & '/'>
            </cfif>
        
        </cfloop>
     
        <cfset pos = listLen(aFile,'.')>
        <cfset ext = listGetAt(aFile,pos,'.')>
        
        <cfset structAppend(data,{'path':thePath})>
        <cfset structAppend(data,{'filename':aFile})>
        <cfset structAppend(data,{'file':thePath & aFile})>
 		<cfset structAppend(data,{'ext':ext})>
 
		<cfreturn data>

    </cffunction>
    
    
    <!--- Get App Root - Server Path --->
    <cffunction name="getAssetPaths" access="remote" returntype="struct">   
		<cfargument name="assetID" type="numeric" required="no" default="0">
        
        
        <cfset assetPaths = {'asset':{}, 'thumb':{}}>
        
        <cfif assetID GT 0>
        
            <cfquery name="contentPath">
                SELECT        Applications.path AS appPath, Clients.path AS clientPath, AssetTypes.path AS typePath
                FROM          Assets INNER JOIN
                                        Applications ON Assets.app_id = Applications.app_id INNER JOIN
                                        Clients ON Applications.client_id = Clients.client_id INNER JOIN
                                        AssetTypes ON Assets.assetType_id = AssetTypes.assetType_id
                WHERE        (Assets.asset_id = #assetID#)
            </cfquery>
        
            <cfif contentPath.recordCount GT 0>
            
                <cfset assetPath = contentPath.clientPath &'/'& contentPath.appPath &'/assets/'& contentPath.typePath &'/'>
                <cfset assetNPath = contentPath.clientPath &'/'& contentPath.appPath &'/assets/'& contentPath.typePath &'/nonretina/'>
                
                <cfset assetThumbPath = assetPath &"thumbs/">
                <cfset assetNThumbPath = assetPath &"thumbs/nonretina/">
 
                <cfset structAppend(assetPaths.asset,{'xdpi':assetPath})>
                <cfset structAppend(assetPaths.asset,{'mdpi':assetNPath})>
                <cfset structAppend(assetPaths.thumb,{'xdpi':assetThumbPath})>
                <cfset structAppend(assetPaths.thumb,{'mdpi':assetNThumbPath})>
                
            </cfif>
    	
        </cfif>
        
    	<cfreturn assetPaths>
    
	</cffunction>



	<!--- Get App Root - Server Path --->
    <cffunction name="isLocalServer" access="remote" returntype="boolean">   
        
        <cfset localServer = false>
        
        <cfif cgi.server_name IS 'localhost'>
        	<cfset localServer = true>
        </cfif>
  
		<cfreturn localServer>

   </cffunction>
    
    
</cfcomponent>