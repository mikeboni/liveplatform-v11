<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>#company#</title>

<style type="text/css">

.imgLockedAspect {
	max-width: 100%;
    height: auto;
}

.imgLockedAspectHeight {
	max-height: 100%;
    width: auto;
}

.bannerHeading {
	padding-top:20px;
	padding-bottom:5px;
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 36px;
	color: #333;
}
.bundleid {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 18px;
	color: #333;
	text-decoration:none;
}
.category {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 16px;
	color: #CCC;
	text-decoration:none;
	background-color: #666;
	padding-bottom: 6px;
	padding-left: 12px;
	height: 26px;
}

.plainLinkGrey {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #999;
	text-decoration:none;
}
.content {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #333;
	text-decoration:none;
}
.note {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	color: #999;
	text-decoration:none;
}
div.centre
{
  width: 760px;
  display: block;
  margin-left: auto;
  margin-right: auto;
}

.contentLink {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #06C;
	text-decoration:none;
	cursor:pointer;
}
</style>
</head>

<body>

	<table width="800" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td colspan="2"><spacer width="20" height="20" /></td>
    </tr>
    <tr>
      <td width="20" align="left" valign="top">
      <cfoutput>
      <img src="#assetPaths.application.icon#" alt="#assetPaths.client.name#" width="100" height="100" border="0" style="padding-right:20px" />
      </cfoutput>
      </td>
      <td align="left" valign="bottom" class="mainHeading">
      <span class="bannerHeading">
      <cfoutput>#assetPaths.client.name#</cfoutput>
      </span>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="left" valign="top"><spacer></td>
    </tr>
    <tr>
      <td align="left" valign="top">&nbsp;</td>
      <td height="35" align="left" valign="top" class="bundleid">
      <cfoutput>#info.subject#</cfoutput>                   
    </tr>
    <tr>
      <td align="left" valign="top">&nbsp;</td>
      <td align="left" valign="top" class="content"><cfoutput>#info.message#</cfoutput></tr>
    <tr>
      <td align="left" valign="top">&nbsp;</td>
      <td align="left" valign="top" class="bundleid">                      
</tr>
	</table> 

</body>
</html>