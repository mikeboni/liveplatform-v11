<cfcomponent>
    
    <!--- GET BUNDLEID from ASSETID --->
    <cffunction name="getAppIDFromAssetID" access="public" returntype="string">
		<cfargument name="assetID" type="numeric" required="no" default="0">
        
        <cfquery name="appInfo"> 
            SELECT        Assets.asset_id, Applications.app_id
            FROM          Assets INNER JOIN Applications ON Assets.app_id = Applications.app_id
            WHERE        (Assets.asset_id = #assetID#)
        </cfquery>
        
        <cfreturn appInfo.app_id>
        
	</cffunction>
    
    <!--- GET OPTIONS --->
    <cffunction name="getOptions" access="public" returntype="query">
		<cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="optionID" type="numeric" required="no" default="0">
        
        <cfquery name="allOptionsAvailable"> 
            SELECT        Assets.asset_id AS materialID, Details.subtitle, Details.description, COALESCE(NULLIF(Details.title,''), Assets.name) as title
            FROM            GroupAssets INNER JOIN
                            Assets ON GroupAssets.content_id = Assets.asset_id INNER JOIN
                            Details ON Assets.detail_id = Details.detail_id
            WHERE        (GroupAssets.action = 0) AND (GroupAssets.active = 1)
            <cfif optionID GT 0>
            	AND (Assets.asset_id = #optionID#)
            <cfelseif assetID GT 0>
            	AND (GroupAssets.asset_id = #assetID#)
            </cfif>
            ORDER BY GroupAssets.sortOrder
        </cfquery>
        
		<cfreturn allOptionsAvailable>
        
	</cffunction>
    
    
    <!--- GET MATRIALS --->
    <cffunction name="getMaterials" access="public" returntype="query">
		<cfargument name="optionID" type="numeric" required="yes">
        <cfargument name="contentID" type="numeric" required="no" default="0">
        <cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="excludeDefault" type="boolean" required="no" default="FALSE">
 
        <cfquery name="optionsGroup"> 
            SELECT       	 Assets.asset_id AS id
            FROM             Details RIGHT OUTER JOIN
                             Assets ON Details.detail_id = Assets.detail_id RIGHT OUTER JOIN
                             GroupAssets ON Assets.asset_id = GroupAssets.content_id
            WHERE            GroupAssets.asset_id =#optionID#
         </cfquery>
         
         <cfset groupID = optionsGroup.id>
         
         <cfset status = arrayNew(1)>
            
		 <cfset code = arrayNew(1)>
         <cfset cost = arrayNew(1)>
         <cfset default = arrayNew(1)>
         <cfset grouping = arrayNew(1)>
         <cfset sortOrder = arrayNew(1)>
		 <cfset materialID = arrayNew(1)>

		 <cfif optionsGroup.recordCount GT 0>

             <cfquery name="allMaterialsAvailable">  
                SELECT       	Assets.asset_id, Assets.assetType_id AS assetType, Details.subtitle, Details.description, ConfigMaterials.cost, ConfigMaterials.material_id, ConfigMaterials.sortOrder, ConfigMaterials.grouping, ConfigMaterials.defaultOption, ConfigMaterials.code, COALESCE(NULLIF(Details.title,''), Assets.name) as title
                FROM            GroupAssets INNER JOIN
                                  Assets ON GroupAssets.content_id = Assets.asset_id INNER JOIN
                                  Details ON Assets.detail_id = Details.detail_id INNER JOIN
                                  ConfigMaterials ON Assets.asset_id = ConfigMaterials.asset_id
                WHERE       	(GroupAssets.asset_id = #groupID#) AND (GroupAssets.action = 0) AND (GroupAssets.active = 1)  
                
                <cfif contentID GT 0>
                AND (ConfigMaterials.group_id = #contentID#)
                </cfif>
                <cfif assetID GT 0>
                 AND (ConfigMaterials.asset_id = #assetID#)
                </cfif>
                
                ORDER BY GroupAssets.sortOrder, ConfigMaterials.grouping ASC
            </cfquery>
                                                                                                                    
            <cfquery name="allMaterials"> 
                SELECT          Assets.asset_id, Assets.assetType_id AS assetType, Details.subtitle, Details.description, COALESCE (NULLIF (Details.title, ''), Assets.name) AS title
                FROM            GroupAssets INNER JOIN
                                        Assets ON GroupAssets.content_id = Assets.asset_id INNER JOIN
                                        Details ON Assets.detail_id = Details.detail_id
                WHERE          (GroupAssets.asset_id = #groupID#) AND (GroupAssets.action = 0) AND (GroupAssets.active = 1)
                <cfif assetID GT 0>
                 AND (Assets.asset_id = #assetID#)
                </cfif>
                ORDER BY GroupAssets.sortOrder
            </cfquery>
                                                                                                                        
          
    		<!--- get and order materials --->
            <cfloop query="allMaterials">
                
                <cfset theAsset = allMaterials.asset_id>
                
                <cfquery dbtype="query" name="materialExists">
                    SELECT	asset_id, code, cost, defaultOption, grouping, sortOrder, material_id
                    FROM	allMaterialsAvailable
                    WHERE	allMaterialsAvailable.asset_id = #theAsset#
                </cfquery>
    
                <cfif materialExists.recordCount GT 0>
                    <cfset arrayAppend(status, 1)>
                    <cfset arrayAppend(code, materialExists.code)>
                    <cfset arrayAppend(cost, materialExists.cost)>
                    <cfset arrayAppend(default, materialExists.defaultOption)>
                    <cfset arrayAppend(grouping, materialExists.grouping)>
                    <cfset arrayAppend(sortOrder, materialExists.sortOrder)>
                    <cfset arrayAppend(materialID, materialExists.material_id)>
                <cfelse>
                    <cfset arrayAppend(status, 0)>
                    <cfset arrayAppend(code, 'NA')>
                    <cfset arrayAppend(cost, 0)>
                    <cfset arrayAppend(default, 0)>
                    <cfset arrayAppend(grouping, 0)>
                    <cfset arrayAppend(sortOrder, 0)>
                    <cfset arrayAppend(materialID, 0)>
                </cfif>
     
            </cfloop>
		
        <cfelse>
            <cfset allMaterials = queryNew("", "")> 
        </cfif>
        
        <cfset queryAddColumn(allMaterials, "CONNECTED", status) />
        <cfset queryAddColumn(allMaterials, "CODE", code) />
        <cfset queryAddColumn(allMaterials, "COST", cost) />
        <cfset queryAddColumn(allMaterials, "GROUPING", grouping) />
        <cfset queryAddColumn(allMaterials, "SORTORDER", sortOrder) />
      	<cfset queryAddColumn(allMaterials, "MATERIAL_ID", materialID) />
        
        <cfif NOT excludeDefault>
        	<cfset queryAddColumn(allMaterials, "DEFAULT", default) />
        </cfif>

        <cfquery dbtype="query" name="sortedMaterials">
            SELECT *
            FROM allMaterials
            ORDER BY grouping ASC
        </cfquery>
	 
		<cfreturn sortedMaterials>
        
	</cffunction>
    
    
    <!--- GET SPACE --->
    <cffunction name="getSpace" access="public" returntype="struct">
		<cfargument name="assetID" type="numeric" required="yes">
        
        <cfquery name="theSpace">   
            SELECT        Assets.asset_id, COALESCE(NULLIF(Details.title,''), Assets.name) as title
            FROM          Details RIGHT OUTER JOIN Assets ON Details.detail_id = Assets.detail_id
            WHERE        (Assets.asset_id = #assetID#)
        </cfquery> 
    	
        <cfset info = {"title":theSpace.title, "asset_id":theSpace.asset_id}>
        
        <cfreturn info>
        
    </cffunction>
    
    
    <!--- GET ALL SPACES --->
    <cffunction name="getAllSpaces" access="public" returntype="query">
        <cfargument name="appID" type="numeric" required="yes">
        
        <cfquery name="theSpaces">   
            SELECT DISTINCT         Groups.asset_id
            FROM            		ConfigMaterials INNER JOIN
                                    Groups ON ConfigMaterials.group_id = Groups.asset_id
            WHERE        (Groups.app_id = #appID#)
        </cfquery> 
    	
        <cfreturn theSpaces>
        
    </cffunction> 
    
    
    <!--- ADD MATRIAL --->
    <cffunction name="addMaterial" access="remote" returntype="boolean">
		<cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfquery name="foundMaterial">   
            SELECT asset_id    
            FROM   ConfigMaterials
            WHERE  asset_id = #assetID# AND group_id = #groupID#
        </cfquery>
        
        <cfquery name="theMaterial"> 
            SELECT        Assets.asset_id, Details.subtitle, Details.description, COALESCE (NULLIF (Details.title, ''), Assets.name) AS title
            FROM            GroupAssets INNER JOIN
                                    Assets ON GroupAssets.content_id = Assets.asset_id INNER JOIN
                                    Details ON Assets.detail_id = Details.detail_id
            WHERE        (Assets.asset_id = #assetID#)

        </cfquery>

        
        <cfif foundMaterial.recordCount IS 0>
        
            <cfquery name="theSpace">   
                INSERT INTO ConfigMaterials(cost, code, asset_id, group_id)
                VALUES(0, '', #assetID#, #groupID#)        
        	</cfquery> 
        	
            <cfreturn true>
            
        </cfif>
    	
        <cfreturn false>
        
    </cffunction> 
    
    <!--- REMOVE MATRIAL --->
    <cffunction name="removeMaterial" access="remote" returntype="boolean">
		<cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfquery name="foundMaterial">   
            SELECT asset_id    
            FROM   ConfigMaterials
            WHERE  asset_id = #assetID# AND group_id = #groupID#
        </cfquery> 
        
        <cfif foundMaterial.recordCount GT 0>
        
            <cfquery name="theSpace">   
                DELETE FROM ConfigMaterials
                WHERE  asset_id = #assetID# AND group_id = #groupID#      
            </cfquery> 
            
            <cfreturn true>
    	
        </cfif>
        
        <cfreturn false>
        
    </cffunction> 
	
	
	<!--- UPDATE MATRIAL --->
    <cffunction name="updateMaterial" access="remote" returntype="boolean">
		<cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        <cfargument name="data" type="struct" required="yes">
        
        <cfquery name="foundMaterial">   
            SELECT asset_id    
            FROM   ConfigMaterials
            WHERE  asset_id = #assetID# AND group_id = #groupID#
        </cfquery>
        
        <cfif foundMaterial.recordCount GT 0>
        
            <cfquery name="theSpace">   
                UPDATE ConfigMaterials
                SET cost = #data.cost#, code = '#data.code#', group_id = #groupID#, defaultOption = #data.default#, grouping = #data.group#, sortOrder = #data.order#
                WHERE asset_id = #assetID# AND group_id = #groupID#
            </cfquery> 
            
            <cfreturn true>
    	
        </cfif>
        
        <cfreturn false>
        
    </cffunction>
    
    

	<!--- GET ROOM MATRIALS --->
    <cffunction name="getAllMaterials" access="remote" returntype="struct">
		<cfargument name="assetID" type="numeric" required="yes">
        
        <cfargument name="modelID" type="numeric" required="no" default="0">
        <cfargument name="spaceID" type="numeric" required="no" default="0">
    	
        <cfset allMaterials = structNew()>
        
        <cfif modelID GT 0>
        
			<!--- get projectID from AssetID --->
            <cfinvoke component="CFC.Configurator" method="getProjectFromAssetID" returnvariable="projectInfo">
                <cfinvokeargument name="assetID" value="#modelID#"/>
            </cfinvoke>
    
            <cfset projectID = projectInfo.id>
            
            <!--- get Overrides --->
            <cfinvoke component="CFC.Configurator" method="getOverides" returnvariable="costOverides">
                  <cfinvokeargument name="projectID" value="#projectID#"/>
                  <cfinvokeargument name="assetID" value="#modelID#"/>
                  <cfinvokeargument name="spaceID" value="#assetID#"/>
            </cfinvoke>
  		
        </cfif>
        
        <!--- get space Options --->
        <cfinvoke component="Configurator" method="getOptions" returnvariable="options">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>  
 
        <cfoutput query="options">
        	
            <cfset materialType = options.materialID>
            
            <cfinvoke component="Configurator" method="getMaterials" returnvariable="materials">
                <cfinvokeargument name="optionID" value="#options.materialID#"/>
                <cfinvokeargument name="contentID" value="#assetID#"/>
                <cfinvokeargument name="excludeDefault" value="true"/>
            </cfinvoke>
        
            <!--- QueryToStruct --->
             <cfinvoke component="Misc" method="QueryToStruct" returnvariable="theData">
                <cfinvokeargument name="query" value="#materials#"/>
                <cfinvokeargument name="forceArray" value="true"/>
             </cfinvoke>
        
             <cfset allItems = arrayNew(1)>
                
             <cfloop index="i" array="#theData#">
             
             	<cfset theIndex = i.grouping + 1>
                
             	<cfif i.connected>
                
                	<cfif NOT ArrayIsDefined(allItems,theIndex)>
                		<cfset allItems[theIndex] = {}>
                    </cfif>
                    
                    <cfset theData = structNew()>
        
                    <!--- sort --->
                    <cfif i.sortOrder NEQ ''>
                    	<cfset structAppend(theData,{"order":i.sortOrder})>
                    </cfif>
                    <!--- code --->
                    <cfif i.code NEQ ''>
                    	<cfset structAppend(theData,{"code":i.code})>
                    </cfif>
                    <!--- cost --->
                    <cfif i.cost GT 0>
                    	
                        <cfif modelID GT 0>
                        
							<cfif structKeyExists(costOverides,i.material_id)>
                                <!--- calc multiplier --->
                                <cfset multi = costOverides[i.material_id].multiplier>
                                <cfset structAppend(theData,{"cost":i.cost * multi})>
                            <cfelse>
                                <cfset structAppend(theData,{"cost":i.cost})>
                            </cfif>
                        
                        <cfelse>
                        	<cfset structAppend(theData,{"cost":i.cost})>
                        </cfif>
                        
                    </cfif>
                          
             		<cfset structAppend(allItems[theIndex],{#i.asset_id#:theData})>
                    
                </cfif>
                
             </cfloop>
            
             <!--- remove any empty cells --->
             <cfset cleanData = arrayNew(1)>
             
             <cfloop index="anItem" array="#allItems#">
             	<cfset arrayAppend(cleanData,anItem)>
             </cfloop>

             <cfif arrayLen(cleanData) GT 0>
             	<cfset structAppend(allMaterials,{"#materialType#":cleanData})>
             </cfif>
        
        </cfoutput>
             
        <cfreturn allMaterials>
  
   </cffunction>  
   
   
   
   <!--- GET ROOM MATRIALS --->
    <cffunction name="getConfigAssetsAvailable" access="remote" returntype="query">
		<cfargument name="appID" type="numeric" required="yes">
        
       <cfquery name="theConfigAssets">  
        SELECT DISTINCT GroupAssets.asset_id, Assets.name
        FROM            GroupAssets LEFT OUTER JOIN
                                Assets ON GroupAssets.asset_id = Assets.asset_id
        WHERE        (Assets.app_id = #appID#) AND (Assets.assetType_id = 1)
       </cfquery>
       
       <cfreturn theConfigAssets>
        
   </cffunction>
   
   
   <!--- GET ROOM MATRIALS --->
    <cffunction name="getMaxDefaultOptions" access="remote" returntype="numeric">
		<cfargument name="assetID" type="numeric" required="yes">
        
        <cfquery name="theOption">    
            SELECT DISTINCT MAX(defaultOption) AS maxOption
            FROM            ConfigMaterials
            WHERE        (group_id = #assetID#)
        </cfquery>
        
        <cfif theOption.maxOption NEQ ''>
        	<cfreturn theOption.maxOption>
        <cfelse>
        	<cfreturn 0>
        </cfif>
    
    </cffunction>
    
    <!--- GET ROOM MATRIALS --->
    <cffunction name="getDefaultOptions" access="remote" returntype="query">
		<cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="defaultOption" type="numeric" required="yes">
    
    	<cfquery name="theDefaults"> 
        	
            SELECT        ConfigMaterials.asset_id, ConfigMaterials.group_id, Details.description, COALESCE (NULLIF (Details.title, ''), Assets.name) AS title, ConfigMaterials.cost, ConfigMaterials.code, ConfigMaterials.defaultOption
            
            FROM            ConfigMaterials INNER JOIN
                                    Assets ON ConfigMaterials.asset_id = Assets.asset_id INNER JOIN
                                    Details ON Assets.detail_id = Details.detail_id
            WHERE        (ConfigMaterials.defaultOption = #defaultOption#) AND (ConfigMaterials.group_id = #assetID#)

    	</cfquery>

        <cfreturn theDefaults>
    
    </cffunction>
   
   
   
    <!--- GET ROOM MATRIAL FROM ASSET --->
    <cffunction name="getRoomFromMaterial" access="remote" returntype="struct">
		<cfargument name="assetID" type="numeric" required="yes">
        
        <cfset theRoom = "">
        
            <cfquery name="theMaterialAsset"> 
                SELECT      GroupAssets.asset_id, COALESCE (NULLIF (Details.title, ''), Assets.name) AS title, Details.subtitle, Details.description
                FROM            GroupAssets INNER JOIN
                                        Assets ON GroupAssets.content_id = Assets.asset_id INNER JOIN
                                        Details ON Assets.detail_id = Details.detail_id
                WHERE        (Assets.asset_id = #assetID#)
            </cfquery>
        																																							
        <cfif theMaterialAsset.recordCount GT 0>
        																																			
        <cfset roomID = theMaterialAsset.asset_id>
        
            <cfquery name="theRoomAsset"> 
                SELECT        Assets.asset_id, Details.title, Details.subtitle, Details.description, Assets.name, GroupAssets.asset_id AS group_id
                FROM            Details RIGHT OUTER JOIN
                                        Assets ON Details.detail_id = Assets.detail_id RIGHT OUTER JOIN
                                        GroupAssets ON Assets.asset_id = GroupAssets.content_id
                WHERE        ( Assets.asset_id = #roomID#)
            </cfquery>
        																																																																							  
            <cfif theRoomAsset.recordCount GT 0>
           
            	<cfset theRoom = {"title":theRoomAsset.name, "asset_id":theRoomAsset.group_id}>
            
            </cfif>
        
        </cfif>
    
    <cfreturn theRoom>
    
    </cffunction>
    
    
    
    <!--- Get Room Default Options --->
    <cffunction name="getAllDefaultOptions" access="remote" returntype="struct" output="no" hint="Get Default Options for Room">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="spaceID" type="numeric" required="no" default="0">
        <cfargument name="option" type="numeric" required="no" default="0">
        
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>
        
        <cfinvoke component="CFC.Configurator" method="getAllSpaces" returnvariable="allSpaces">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>		
        
        <cfinvoke component="CFC.Misc" method="QueryToArray" returnvariable="theSpaces">
            <cfinvokeargument name="theQuery" value="#allSpaces#"/>
            <cfinvokeargument name="theColumName" value="asset_Id"/>
        </cfinvoke>					
        
        <cfif spaceID GT 0>
        	<cfset theSpace = arrayFind(theSpaces,spaceID)>
            <cfif theSpace GT 0>
        		<cfset theSpaces = [theSpaces[theSpace]]>
            </cfif>
        </cfif>

        <cfset spaces = structNew()>

        <cfloop index="assetID" array="#theSpaces#">
 
    	<cfinvoke component="CFC.Configurator" method="getMaxDefaultOptions" returnvariable="maxOptions">
          <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>
        
        <cfinvoke component="CFC.Configurator" method="getSpace" returnvariable="theSpace">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>  
																																
        
        <cfset theDefaults = arrayNew(1)>
        <cfset masterDefault = structNew()>
        
         <cfloop index="theOption" from="1" to="#maxOptions#">
    
            <cfinvoke component="CFC.Configurator" method="getDefaultOptions" returnvariable="defaultOptions">
              <cfinvokeargument name="assetID" value="#assetID#"/>
              <cfinvokeargument name="defaultOption" value="#theOption#"/>
            </cfinvoke>
																																
				<cfset theOptions = structNew()>

                   <cfloop query="defaultOptions">
                   
                    <!--- get room name --->
                    <cfinvoke component="CFC.Configurator" method="getRoomFromMaterial" returnvariable="theRoom">
                      <cfinvokeargument name="assetID" value="#asset_id#"/>
                    </cfinvoke>
                    
                    <!--- find group for asset --->
                    <cfinvoke component="Configurator" method="findGroupForAsset" returnvariable="groupContentID">
                      <cfinvokeargument name="assetID" value="#asset_id#"/>
                      <cfinvokeargument name="groupID" value="#assetID#"/>
                    </cfinvoke>
											
                    <cfset structAppend(theOptions, {#groupContentID#: #asset_id#})>
       
                  </cfloop>
                  
                  <!--- <cfdump var="#theOptions#"><br><cfdump var="#defaultOptions#"><br> --->
    			  
                  
              <cfif theOption GT 1>

                <cfloop collection="#masterDefault#" item="theKey">
            
					<cfif NOT StructKeyExists(theOptions, theKey)>
                    	<cfset structAppend(theOptions,{#theKey#:masterDefault[theKey]})>
                    </cfif>
                
                </cfloop>
                
              <cfelse>
                <cfset masterDefault = theOptions>
              </cfif>
              
              	<cfset arrayAppend(theDefaults, theOptions)>

          </cfloop>

		  <cfset allDefaults = {#theSpace.asset_id#:theDefaults}>

        <cfset structAppend(spaces,allDefaults)>
        
        </cfloop>

        <!--- get option if defined --->
        <cfif option GT 0>
        
			<cfset selectSpace = structNew()>
            
            <cfloop collection="#spaces#" item="spc">

              <cfif option GT arrayLen(spaces[spc])>
              	  <cfset lastOption = arrayLen(spaces[spc])>
              	  <cfset structAppend(selectSpace,{#spc#:spaces[spc][lastOption]})>
              <cfelse>
              	  <cfset structAppend(selectSpace,{#spc#:spaces[spc][option]})>
              </cfif>
              
            </cfloop>
            
            <cfreturn selectSpace>
            
        </cfif>

        <cfreturn spaces>
        
    </cffunction>
    
    
    
    <!--- Find Asset Group for Material --->
    <cffunction name="findGroupForAsset" access="remote" returntype="numeric" output="no" hint="">
        <cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="yes">
        
        <cfquery name="theAssets"> 
            SELECT        asset_id, group_id, content_id, accessLevel, cached, active, sharable, sortOrder, action, quiz, quizCorrect, quizIncorrect
            FROM          GroupAssets
            WHERE        (content_id = #assetID#)
        </cfquery>
        
        <cfif theAssets.recordCount GT 0>
        	
            <cfloop query="theAssets">
            	
                <cfset foundAssetID = theAssets.asset_id>
                
                <cfquery name="theGroup"> 
                    SELECT        *
                    FROM          GroupAssets where content_id = #foundAssetID#
                </cfquery>
            
                <cfset contentID = theGroup.asset_id>
                
                <cfquery name="theAsset"> 
                    SELECT        GroupAssets.*
                    FROM          GroupAssets where asset_id = #groupID# AND content_id = #contentID#
                </cfquery>
                
                <cfif theAsset.recordCount GT 0>
                	<cfreturn contentID>
            	</cfif>
                
            </cfloop>

    	</cfif>
        
        <cfreturn 0>
    	
    </cffunction>
    
    
    <!--- Save Unit Config Changes --->
    <cffunction name="saveConfig" access="remote" returntype="struct" output="no" hint="">
        <cfargument name="userID" type="numeric" required="yes">
        <cfargument name="custID" type="numeric" required="yes">
        <cfargument name="unitID" type="numeric" required="yes">
        <cfargument name="data" type="string" required="yes">
        
        	<cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="curDate" />
        	
            <!---ok--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
            
            <!--- check if exists --->
            <cfquery name="theUnitFound">
                SELECT unit_id
                FROM   UnitAssets
                WHERE user_id = #userID# AND cust_id = #custID# AND unit_id = #unitID#
            </cfquery>
            
            <cfif theUnitFound.recordCount GT 0>
            
				<!--- update if exists --->
                <cfquery name="theConfig">   
                    UPDATE UnitAssets
                    SET data = '#data#', modified = #curDate#
                    WHERE user_id = #userID# AND cust_id = #custID# AND unit_id = #unitID#
                </cfquery>

            <cfelse>
            	
                <!--- failed - no record, return error --->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1022"/>
                </cfinvoke>
                
            </cfif>
            
        <cfreturn error>
    	
    </cffunction>
    
    
    <!--- Save Unit Config Changes --->
    <cffunction name="clearConfig" access="public" returntype="boolean" output="no" hint="clears data in config - assets changed">
        <cfargument name="unitID" type="numeric" required="yes">
        
            <!--- check if exists --->
            <cfquery name="theUnitFound">
                SELECT unit_id
                FROM   UnitAssets
                WHERE  unit_id = #unitID#
            </cfquery>
            
            <cfset result = false>
            
            <cfif theUnitFound.recordCount GT 0>
            
				<!--- update if exists --->
                <cfquery name="theConfig">   
                    UPDATE UnitAssets
                    SET data = NULL, modified = NULL, created = NULL
                    WHERE unit_id = #unitID#
                </cfquery>
                
                <cfset result = true>

            <cfelse>
                
            </cfif>
            
        <cfreturn result>
    	
    </cffunction>
    
    
    
    <!---  --->
    <!--- PRINT/EMAIL FORM ROUTINES 															--->
    <!---  --->
    
    
    <cffunction name="getRoomDefaultFavouredSelection" access="remote" returntype="struct" output="no" hint="get room default options">
    	<cfargument name="spaceID" type="numeric" required="yes">
        <cfargument name="roomID" type="numeric" required="yes">
        <cfargument name="defaultSelection" type="numeric" required="no" default="1">

        <!--- AppID fromm Asset --->
        <cfinvoke component="CFC.Configurator" method="getAppIDFromAssetID" returnvariable="appID">
        	<cfinvokeargument name="assetID" value="#roomID#"/>
        </cfinvoke>
        
		<!--- Get Defaults --->
        <cfinvoke component="CFC.Configurator" method="getAllDefaultOptions" returnvariable="theDefaults">
             <cfinvokeargument name="appID" value="#appID#"/>
             <cfinvokeargument name="groupID" value="#spaceID#"/>
        </cfinvoke>
     
        <cfset theRoomDefaults = theDefaults[roomID]>
        
        <cfif defaultSelection GT arrayLen(theRoomDefaults)>
        	<cfset defaultSelection = arrayLen(theRoomDefaults)>
        </cfif>
        
        <cfset theRoomDefaults = theRoomDefaults[defaultSelection]>
               
		<cfreturn theRoomDefaults>
    
    </cffunction>
    
    
    
    <!--- Get Room Default --->
    <cffunction name="getRoomConfiguration" access="remote" returntype="struct" output="yes" hint="get room default options">
        <cfargument name="custSelection" type="struct" required="yes">
        <cfargument name="roomID" type="numeric" required="yes">
        <cfargument name="favouredOption" type="numeric" required="yes">
        
        <!--- Space --->
        <cfinvoke component="CFC.Configurator" method="getSpace" returnvariable="theSpace">
            <cfinvokeargument name="assetID" value="#roomID#"/>
        </cfinvoke>
        

        <!--- Default from Favoured Selection --->
        <cfinvoke component="CFC.Configurator" method="getRoomDefaultFavouredSelection" returnvariable="theDefaultFavoured">
        	<cfinvokeargument name="spaceID" value="#theSpace.asset_id#"/>
            <cfinvokeargument name="roomID" value="#roomID#"/>
            <cfinvokeargument name="defaultSelection" value="#favouredOption#"/>
        </cfinvoke>
      	
        <cfset optionData = {"selection":{}, "default":{}}>
        <cfset custSelection = custSelection[roomID]>
        
        <cfloop collection="#theDefaultFavoured#" item="theOption">
        		
              <!--- option materials --->
              <cfinvoke component="CFC.Configurator" method="getMaterials" returnvariable="allMaterials">
                  <cfinvokeargument name="optionID" value="#theOption#"/>
              </cfinvoke>
              
              <cfset materials = structNew()>
              
              <cfoutput query="allMaterials">
			  	
                <cfset data = {"code":code, "cost":cost, "group":grouping}>
              	<cfset structAppend(materials,{"#asset_id#":data})>
              
			  </cfoutput>
              
        	  <!--- get asset details --->
              <cfinvoke component="CFC.Modules" method="getContentAsset" returnvariable="asset">
                <cfinvokeargument name="assetID" value="#custSelection[theOption]#"/>
              </cfinvoke>
              
              <!--- get default asset details --->
              <cfinvoke component="CFC.Modules" method="getContentAsset" returnvariable="defaultAsset">
                <cfinvokeargument name="assetID" value="#theDefaultFavoured[theOption]#"/>
              </cfinvoke>
              
              <!--- get room name --->
              <cfinvoke component="CFC.Configurator" method="getRoomFromMaterial" returnvariable="theOptionTitle">
                <cfinvokeargument name="assetID" value="#custSelection[theOption]#"/>
              </cfinvoke>
              
              <!--- createRoomData --->
              <cfinvoke component="CFC.Configurator" method="createRoomData" returnvariable="theOptionData">
                <cfinvokeargument name="asset" value="#asset#"/>
                <cfinvokeargument name="materials" value="#materials#"/>
              </cfinvoke>
              
              <!--- createRoomData --->
              <cfinvoke component="CFC.Configurator" method="createRoomData" returnvariable="defaultOptionData">
                <cfinvokeargument name="asset" value="#defaultAsset#"/>
                <cfinvokeargument name="materials" value="#materials#"/>
              </cfinvoke>
              
              <!--- updagrade --->
              <cfif theDefaultFavoured[theOption] NEQ custSelection[theOption]>
               	 	<cfset structAppend(theOptionData,{"updagrade":1})>
              <cfelse>
              		<cfset structAppend(theOptionData,{"updagrade":0})>
              </cfif>
              <cfset materialLevel = materials[custSelection[theOption]].group>
              <cfset structAppend(theOptionData,{"group":materialLevel})>
      
              <!--- build structure --->
              <cfset structAppend(optionData.selection,{"#custSelection[theOption]#":theOptionData})>
              <cfset structAppend(optionData.default,{"#theDefaultFavoured[theOption]#":defaultOptionData})>
                
        </cfloop>

		<cfreturn optionData>
    	
    </cffunction>
    
    
    
    
    
    <!--- Display Room Selections --->
    <cffunction name="createRoomData" access="remote" returntype="struct" output="no" hint="">
        <cfargument name="asset" type="struct" required="yes">
        <cfargument name="materials" type="struct" required="yes">                             
                
    		  <!--- get reference image --->
              <cfloop collection="#asset#" item="theKey"></cfloop>
              <cfset data = asset[theKey]>

              <cfset theOptionData = {"thumb":"", "code":"", "cost":"", "description":"", "material":""}>
			  
              <!--- MATERIAL IMAGE --->
              <cfif structKeyExists(data,"thumb")>
                <cfset structAppend(theOptionData,{"thumb":data.thumb.xdpi.url})>
              </cfif>
            
              <!--- SCENE MATERIAL IMAGE --->
              <cfset theImage = asset[theKey].image>
              <cfset img = structNew()>
 			  <cfset structAppend(theOptionData,{"scene":theImage.url.xdpi})>
              
              <cfif structKeyExists(materials,data.assetID)>
                  
                  <!--- CODE --->
                  <cfset theOptionData.code = materials[data.assetID].code>
                               
                  <!--- COST --->
                  <cfset theOptionData.cost = materials[data.assetID].cost>
                    
              </cfif>
              
              <!--- DETAILS --->
              <cfif structKeyExists(data,"details")>
              
				   <!--- DESCRIPTION --->
                   <cfif structKeyExists(data.details,"description")>
                        <cfset theOptionData.description = data.details.description>
                   </cfif>
                    
                   <!--- TITLE --->
                   <cfif structKeyExists(data.details,"title")>
                        <cfset theOptionData.material = data.details.title>
                    </cfif>
                
              </cfif>

    	<cfreturn theOptionData>
    
    </cffunction>
    
    
    
    
    
    <!--- Display Room Selections --->
    <cffunction name="getRoomSelection" access="remote" returntype="any" output="no" hint="">
        <cfargument name="displayType" type="numeric" required="no" default=""><!--- default, upgrades or both --->
    
    	
        <cfsavecontent variable = "optionSelections"> 

        <div style="padding-top:20px">
              
            <table width="100%" border="0" cellpadding="4" cellspacing="1" class="bodytext" bgcolor="##333">
            <tr>
              <td height="32" colspan="4" bgcolor="##888" class="barSmallHeading">Standard Options</td>
              </tr>
            <tr class="barTableHead">
              <td width="200" height="32" bgcolor="##DDD" class="barTableHead">OPTION</td>
              <td width="80" align="center" bgcolor="##DDD">CODE</td>
              <td width="200" bgcolor="##DDD">MATERIAL</td>
              <td bgcolor="##DDD">DESCRIPTION</td>
            </tr>
            
            <!---line item--->
            <cfloop item="defaultItem" collection="#allDefaultOptions#">
        
                <tr class="bodytext">
                  <td height="32" valign="top" bgcolor="##FFF" style="padding-top:10px">#allDefaultOptions[defaultItem].option#</td>
                  <td align="center" valign="top" bgcolor="##FFF" style="padding-top:10px">#allDefaultOptions[defaultItem].code#</td>
                  <td valign="top" bgcolor="##FFF" style="padding:8px">
                  
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="20" valign="top">
                      <img src="#allDefaultOptions[defaultItem].thumb#" alt="" name="" width="30" height="22" style="border:thin; border-color:##666; border-style:solid" />
                      </td>
                      <td class="bodytext" style="padding-left:5px">#allDefaultOptions[defaultItem].material#</td>
                    </tr>
                  </table>
                  
                  </td>
                  <td valign="top" bgcolor="##FFF" style="padding-top:10px">#allDefaultOptions[defaultItem].description#</td>
                </tr>
                
            </cfloop>
            
            </table>
        
        </div>
        
    	</cfsavecontent>
        
        <cfreturn optionSelections>
    
    </cffunction>
    
    
    
    <!--- Create Room Image --->
    <cffunction name="createRoomImage" access="remote" returntype="any" output="no" hint="creates a room configuration and return image">
        <cfargument name="optionImages" type="struct" required="yes">
		<cfargument name="baseImage" type="string" required="yes">

          <cfimage source="#baseImage#" name="compImage">  
         
          <cfset finalImage = ImageNew("",compImage.width,compImage.height,"argb")>
          <cfset ImageDrawRect(finalImage,0,0,compImage.width,compImage.height,"yes")>
          
		  <cfset ImageOverlay(finalImage,compImage)>
            
          <!--- get a layer --->
          <cfloop collection="#optionImages#" item="anIMG">
              <cfimage source="#optionImages[anIMG].scene#" name="anImage">
              <cfset ImageOverlay(finalImage,anImage)>
          </cfloop>
          
          <cfreturn finalImage>
    	
    </cffunction>
    
    
    
    
    <!--- Create Room Image --->
    <cffunction name="getUnitInfo" access="remote" returntype="struct" output="no" hint="">
        <cfargument name="unitID" type="numeric" required="yes">
        
        <cfset data = structNew()>
        
        <!--- get unit data --->
        <cfinvoke component="CFC.units" method="getUnit" returnvariable="unitData">
            <cfinvokeargument name="unitID" value="#unitID#"/>
        </cfinvoke>
        
        <cfset theUnit = unitData[1]>
        <cfset cust = theUnit.cust>
        <cfset user = theUnit.user>
        <cfset refNum = 'NA'>
        
        <cfset config = theUnit.data>
        
        <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="created">
            <cfinvokeargument name="TheEpoch" value="#theUnit.created#"/>
        </cfinvoke>
        
        <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="modified">
            <cfinvokeargument name="TheEpoch" value="#theUnit.modified#"/>
        </cfinvoke>
        
        <cfif theUnit.ref IS ''>
            <cfset refNum = 'NA'>
        <cfelse>
            <cfset refNum = theUnit.ref>
        </cfif>
        
        <cfset info = {"created":created, "modified":modified, "project":theUnit.project, "group":theUnit.unit, "code":theUnit.unitCode, "unit":theUnit.unitName, "type":theUnit.unitType, "projectID":theUnit.groupID, "ref":refNum, "state":theUnit.state}>

        <cfset custInfo = {"name":"#cust.name#", "address":"#cust.company_address#", "phone":"#cust.phone#" ,"email":"#cust.email#"}>
        <cfset userInfo = {"name":"#user.name#", "phone":"#user.companyphone#" ,"email":"#user.email#"}>
        
        <cfset custSelection = structNew()>
        <cfset defaultSelection = structNew()>
       
        <cfloop collection="#config#" item="theKey">
            
            <cfset theSel = config[theKey].favouredConfigIndex + 1>
            <cfset theConfig = config[theKey].configs[theSel]>
            
            <cfset structAppend(custSelection,{"#theKey#":theConfig})>
            <cfset structAppend(defaultSelection,{"#theKey#":theSel})>
        
        </cfloop>
        
    	<cfset structAppend(data,{"info":info})>
        <cfset structAppend(data,{"customer":custInfo})>
        <cfset structAppend(data,{"user":userInfo})>
        <cfset structAppend(data,{"selections":custSelection,"default":defaultSelection})>
    	
        <cfreturn data>
    
    </cffunction>
    
    
    
    <!--- get Unit Configuration --->
    <cffunction name="getUnitConfiguration" access="remote" returntype="struct" output="no" hint="">
        <cfargument name="unitID" type="numeric" required="yes">
    
		<!--- Get Info Details --->
        <cfinvoke component="Configurator" method="getUnitInfo" returnvariable="info">
            <cfinvokeargument name="unitID" value="#unitID#"/>
        </cfinvoke>
        
        <!--- Define Selections and Defaults --->
        <cfset unitConfigs = structNew()>
        
        <cfloop collection="#info.selections#" item="theRoom"><!--- room --->
        
            <cfset defaultFavouredSelection = info.default[theRoom]>
            
            <cfinvoke component="Configurator" method="getRoomConfiguration" returnvariable="roomOptions">
                <cfinvokeargument name="custSelection" value="#info.selections#"/>
                <cfinvokeargument name="roomID" value="#theRoom#"/><!--- theRoom --->
                <cfinvokeargument name="favouredOption" value="#defaultFavouredSelection#"/>
            </cfinvoke>
            
            <!--- Space --->
            <cfinvoke component="Configurator" method="getSpace" returnvariable="theSpace">
                <cfinvokeargument name="assetID" value="#theRoom#"/>
            </cfinvoke>
            
            <!--- get base image asset --->
              <cfinvoke component="CFC.Modules" method="getContentAsset" returnvariable="asset">
                <cfinvokeargument name="assetID" value="#theRoom#"/>
              </cfinvoke>
              
              <cfloop collection="#asset#" item="theKey"></cfloop>
              
              <cfset baseImage = asset[theKey].image.url.xdpi>
              <cfset structAppend(roomOptions,{'base':baseImage})>
  
              <cfset structAppend(unitConfigs,{"#theSpace.title#":roomOptions})>
     
             
        </cfloop>
           
            <!--- remove selection and default from info --->
            <cfset structDelete(info,"selections")>
            <cfset structDelete(info,"default")>
            
            <cfset unitData = {"details":info ,"rooms":unitConfigs}>
       
       		<cfreturn unitData>
       
       </cffunction> 
       
       
       
       
       <!--- Generate JSON for Materials and Save --->
    	<cffunction name="generateMaterialsJSON" access="public" returntype="boolean" output="no" hint="Generate All Room Materials JSON">
        <cfargument name="appID" type="numeric" required="yes">
            
            <!--- Get JSON Path --->
            <cfinvoke  component="Apps" method="getJSONPath" returnvariable="JSONPath">
                <cfinvokeargument name="appID" value="#appID#">
            </cfinvoke>
            
            <cfinvoke component="Apps" method="getAppName" returnvariable="appName">
                <cfinvokeargument name="appID" value="#appID#">
            </cfinvoke>
            
            <!--- path of JSON materials --->
            <cfset JSONFilePath = JSONPath & 'materials.json'>
			
             <!--- IF OLD FILE EXISTS, delete --->
            <cfif fileExists(JSONFilePath)>
            	<cffile action="delete" file="#JSONFIlePath#">
            </cfif>
    
			<!--- Get Spaces --->
            <cfinvoke component="Configurator" method="getAllSpaces" returnvariable="theSpaces">
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
            
            <cfset spaces = structNew()>
    
            <cfloop query="theSpaces">
             
                <cfinvoke component="Configurator" method="getAllMaterials" returnvariable="data">
                    <cfinvokeargument name="assetID" value="#theSpaces.asset_id#"/>
                    <cfinvokeargument name="excludeDefault" value="TRUE"/>
                </cfinvoke>
               
                <cfif structCount(data) GT 0>
                    <cfset structAppend(spaces,{#theSpaces.asset_id#:#data#})>
                </cfif>
    
                <!--- Get Defaults --->
                <cfinvoke component="Configurator" method="getAllDefaultOptions" returnvariable="theDefaults">
                    <cfinvokeargument name="appID" value="#appID#"/>
                     <cfinvokeargument name="groupID" value="#theSpaces.asset_id#"/>
                </cfinvoke>
                                                                                                                                    
            </cfloop>
            
            <cfset materialStruct = structNew()>
            
            <cfset structAppend(materialStruct,{"materials":spaces})>
            <cfset structAppend(materialStruct,{"defaults":theDefaults})>
        
            <cfset JSON = serializeJSON(materialStruct)>
    
            <!--- write JSON file --->
            <cffile action="write" file="#JSONFIlePath#" output="#JSON#" charset="utf-8">
   			
            <cfreturn true>
            
        </cffunction>
        
        
        <!--- Get Asset Assets --->
    	<cffunction name="getGroupAssets" access="remote" returntype="query" output="no" hint="resturns contents of group">
        <cfargument name="groupID" type="numeric" required="yes">
        
            <cfquery name="contents"> 
                SELECT        Assets.name, Assets.asset_id
                FROM          Groups LEFT OUTER JOIN
                                Assets ON Groups.asset_id = Assets.asset_id RIGHT OUTER JOIN
                                Details ON Assets.detail_id = Details.detail_id
                WHERE       Groups.subgroup_id = #groupID#
            </cfquery>
            
            <cfreturn contents>
        
       </cffunction>
       
       
       
       <!--- Get Overides --->
    	<cffunction name="getOverides" access="public" returntype="struct" output="no" hint="resturns overide assets">
            <cfargument name="projectID" type="numeric" required="no" default="0">
            <cfargument name="assetID" type="numeric" required="no" default="0">
            <cfargument name="spaceID" type="numeric" required="no" default="0">
        
           <cfquery name="overideAssets"> 
                SELECT	material_id, overide_id, multiplier
                FROM	AssetOverides 
                WHERE	project_id = #projectID# AND asset_id = #assetID# AND space_id = #spaceID#
            </cfquery>
           
            <cfset overides = structNew()>
            
            <cfoutput query="overideAssets">
                <cfset theObj = {"multiplier":multiplier, "overideID":overide_id}>
                <cfset structAppend(overides,{"#material_id#":theObj})>
            </cfoutput>
       
       		<cfreturn overides>
       
       </cffunction>
       
       
       
       <!--- Delete Overids --->
    	<cffunction name="deleteOveride" access="remote" returntype="boolean" output="no" hint="deletes overide">
            <cfargument name="overideID" type="numeric" required="no" default="0">
        
           <cfquery name="overideAssets"> 
                DELETE FROM		AssetOverides 
                WHERE			overide_id = #overideID#
            </cfquery>

       		<cfreturn true>
       
       </cffunction>
       
       <!--- Update Overids --->
    	<cffunction name="updateOveride" access="remote" returntype="boolean" output="no" hint="deletes overide">
            <cfargument name="overideID" type="numeric" required="no" default="0">
        	<cfargument name="multiplier" type="numeric" required="no" default="0">
            
            <cfquery name="overideFound"> 
                SELECT	overide_id
                FROM	AssetOverides
                WHERE overide_id = #overideID#
            </cfquery>
            
            <cfif overideFound.recordCount GT 0>
            	<!--- update overide --->
               <cfquery name="overideAssets"> 
                    UPDATE AssetOverides
                    SET multiplier = #multiplier#
                    WHERE overide_id = #overideID#
                </cfquery>
    
                <cfreturn true>
       
       		<cfelse>
            	<cfreturn false>
            </cfif>
       
       </cffunction>
       
       
       <!--- Add Overids --->
    	<cffunction name="addOveride" access="remote" returntype="boolean" output="no" hint="deletes overide">
            <cfargument name="projectID" type="numeric" required="no" default="0">
        	<cfargument name="assetID" type="numeric" required="no" default="0">
            <cfargument name="spaceID" type="numeric" required="no" default="0">
            <cfargument name="materialID" type="numeric" required="no" default="0">
            
            <cfargument name="multiplier" type="numeric" required="no" default="1">
           
           <cfif projectID GT 0 AND assetID GT 0 AND spaceID GT 0 AND materialID GT 0>
           
               <cfquery name="theSpace">   
                    INSERT INTO AssetOverides(project_id, asset_id, space_id, material_id, multiplier)
                    VALUES(#projectID#, #assetID#, #spaceID#, #materialID#, 0)        
                </cfquery> 
			
            <cfelse>
            
            	<cfreturn false>
                
            </cfif>
            
       		<cfreturn true>
       
       </cffunction>
       
       
    <!--- Get Project from AssetID --->
 	<cffunction name="getProjectFromAssetID" access="public" returntype="struct" output="no">
        <cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <cfset crumb = arrayNew(1)>
        
		<!--- Others --->
        <cfloop condition="assetID GT 0">
        
            <cfquery name="theGroup">
                SELECT	name, subgroup_id
                FROM	Groups
                WHERE	group_id = #assetID# 
            </cfquery>
       
            <cfset arrayAppend(crumb,{"name":theGroup.name,"id":assetID})>
            
            <cfset assetID = theGroup.subgroup_id>

        </cfloop>
        
        <cfif arrayLen(crumb) IS 0>
        	ERROR:<cfdump var="#crumb#"><cfabort>
        <cfelse>
        	<cfset projectInfo = crumb[arrayLen(crumb)]>
        </cfif>
        
        <cfreturn projectInfo>
        
      </cffunction>
       
    
</cfcomponent>