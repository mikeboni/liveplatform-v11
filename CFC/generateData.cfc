<cfcomponent>

	<cffunction name="generateData" access="public" returntype="any">
		<cfargument name="appID" type="numeric" required="yes">
        <cfargument name="groupID" type="numeric" required="no" default="-1">
        	
        <cfinvoke  component="CFC.Modules" method="getGroupContent" returnvariable="data">
			<cfinvokeargument name="groupID" value="#groupID#"/>
			<cfinvokeargument name="appID" value="#appID#"/>
			<!--- <cfinvokeargument name="fullStructure" value="#fullStructure#"/> --->
		</cfinvoke>

		<!--- <cfdump var="#data#"> --->

		<cfif structKeyExists(data, 'assets')>

			<cfloop index="asset" array="#data.assets#">

				<cfdump var="#asset#">
				<cfabort>
			</cfloop>
		
		</cfif>

		
     
        <cfreturn data>
        
	</cffunction>
    
    
</cfcomponent>