<cfcomponent>
	
	<!--- create log --->
   <cffunction name="log" returntype="void" >
        <cfargument name="fileName" type="string" required="no" default="log" >
        <cfargument name="data" type="any" required="no" default="" >
        	
			<cfif isStruct(data)>
  				<cfset data = SerializeJSON(data)>
  			</cfif>
  				
   			<cfset logPath = GetTempDirectory() & fileName &'.txt'>
			<cffile action="write" output="#data#" file="#logPath#">

   	</cffunction>
   
    <!--- Merge Arrays --->
    <cffunction name="mergeArrays" returntype="array" >
        <cfargument name="array1" type="array" required="true" >
        <cfargument name="array2" type="array" required="true" >
        
        <cfloop array="#array2#" index="elem">
        	<cfif ArrayFind(array1,elem)>
            	<!--- nothing --->
            <cfelse>
            	<cfset arrayAppend(array1,elem) >
            </cfif>
        </cfloop>

        <cfreturn array1>
        
    </cffunction>


	<!--- CreateID --->
    <cffunction name="makeID" access="remote" returntype="string">
        <cfargument name="length" type="numeric" required="false" default="4"/>
        <cfargument name="type" type="numeric" required="false" default="0"/>
    
          <cfset theID="">
          
          <cfset theAlpha = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z">
          
          <cfloop index="z" from="1" to="#length#">
          
          <cfif type IS "0">
          <!---Both--->
              <cfif RandRange(0, 1, "SHA1PRNG") IS "0">
              		<!---Alpha--->
                    <cfset theObjPos = RandRange(1, listLen(theAlpha), "SHA1PRNG")>
                    <cfset theObj = listGetAt(theAlpha,theObjPos)>
              <cfelse>
              		<!---Numeric--->
                    <cfset theObj = RandRange(0, 9, "SHA1PRNG")>
              </cfif>
   
          <cfelseif type IS "2">
          <!---Alpha--->
                <cfset theObj = RandRange(1, listLen(theAlpha), "SHA1PRNG")>
          <cfelseif type IS "1">
          <!---Numeric--->
                <cfset theObj = RandRange(0, 9, "SHA1PRNG")>
          </cfif>
          
          <cfset theID = theID & theObj>
          
          </cfloop>
          
          <cfreturn theID>
          
    </cffunction>
    
    
    
    <!--- Mobile Device --->
    <cffunction name="isMobileDevice" access="public" returntype="boolean" output="true" hint="Detects Mobile Devices">

	<cfif findNoCase('Android', cgi.http_user_agent,1)>
    <!--- Android version of the mobile site --->
    	<cfreturn true>
	<cfelseif findNoCase('iPhone', cgi.http_user_agent,1)>
    <!--- iphone version of the mobile site --->
    	<cfreturn true>
    <cfelseif findNoCase('iPad', cgi.http_user_agent,1)>
    <!--- iPad version of the mobile site --->
    	<cfreturn true>
    <cfelse>
    	<cfreturn false>
	</cfif>
        
    </cffunction> 
    
    
    
    <!--- Make Short URL --->
    <cffunction name="shortUrl" returntype="string" output="no">
    
        <!--- Make URL --->
        <cfinvoke component="Misc" method="makeID" returnvariable="theLink">
            <cfinvokeargument name="length" value="8"/>
            <cfinvokeargument name="type" value="0"/>
        </cfinvoke>
        
        <!--- Check if URL exists --->
        <cfinvoke component="Carts" method="cartURLExists" returnvariable="exists">
            <cfinvokeargument name="url" value="#theLink#"/>
        </cfinvoke>
        
        <cfif exists>
            <cfinvoke component="Misc" method="shortUrl" returnvariable="theLink" />
        </cfif>
        
        <cfreturn theLink>
        
    </cffunction>
    
    
    
    
     <!--- Get Device  --->
    <cffunction name="getDevice" returntype="query" output="yes" access="remote">
      
      <cfargument name="device_id" type="numeric" required="no" default="0"/>
      
      <cfargument name="operating_system" type="string" required="no" default=""/>
      <cfargument name="device_name" type="string" required="no" default=""/>
      
      <cfargument name="screen_width" type="numeric" required="no" default="0"/>
      <cfargument name="screen_height" type="numeric" required="no" default="0"/>
      
      <!--- Get Device  --->  
      <cfquery name="devices">
      	SELECT device_id, name, os, width, height, size
        FROM 	Devices
        WHERE	0 = 0
        <cfif device_id GT '0'>
        	<!--- Device ID --->
        	 AND device_id = #device_id# 
        <cfelse>
			<!--- Other Params --->
            <cfif operating_system NEQ ''>
                AND os = '#operating_system#' 
            </cfif>
            <cfif device_name NEQ ''>
                AND name = '#device_name#' 
            </cfif>
            <cfif screen_width NEQ ''>
                AND width = #screen_width# 
            </cfif>
            <cfif screen_height NEQ ''>
                AND height = '#screen_height#'
            </cfif>
        </cfif>
      </cfquery>
      
      <cfreturn devices>
        
    </cffunction>
    
 
     <!--- Device Exists --->
    <cffunction name="deviceExists" returntype="boolean" output="no">
    
      <cfargument name="operating_system" type="string" required="yes" default=""/>
      <cfargument name="device_name" type="string" required="yes" default=""/>
      
      <cfargument name="screen_width" type="string" required="yes" default=""/>
      <cfargument name="screen_height" type="string" required="yes" default=""/>
      
      <!--- Check If Device Exists --->  
      <cfquery name="devices">
      	SELECT COUNT(device_id) AS total
        FROM 	Devices
        WHERE	os = '#operating_system#' AND width = #screen_width# AND height = #screen_height# AND name = '#device_name#'
      </cfquery>
      
      <cfset totalDevices = devices.total>
      
      <cfif totalDevices GT '0'>
      	<cfreturn true>
      <cfelse>
      	<cfreturn false>
      </cfif>
        
    </cffunction>
 


    <!--- Create Device Type --->
    <cffunction name="createDevice" returntype="numeric" output="yes" access="remote">
    
      <cfargument name="operating_system" type="string" required="yes" default=""/>
      <cfargument name="device_name" type="string" required="yes" default=""/>
      
      <cfargument name="screen_width" type="string" required="yes" default=""/>
      <cfargument name="screen_height" type="string" required="yes" default=""/>
      <cfargument name="screen_size" type="string" required="yes" default=""/>
      
      <!--- Check If Device Exists --->  
      <cfinvoke  component="Misc" method="deviceExists" returnvariable="deviceExists">
          <cfinvokeargument name="operating_system" value="#operating_system#"/>
          <cfinvokeargument name="device_name" value="#device_name#"/>
          <cfinvokeargument name="screen_width" value="#screen_width#"/>
          <cfinvokeargument name="screen_height" value="#screen_height#"/>
      </cfinvoke>  
      
      <cfif deviceExists>
      <!--- Device Exists --->
      <!--- Get Device --->
      <cfinvoke  component="Misc" method="getDevice" returnvariable="theDevice">
          <cfinvokeargument name="operating_system" value="#operating_system#"/>
          <cfinvokeargument name="device_name" value="#device_name#"/>
          <cfinvokeargument name="screen_width" value="#screen_width#"/>
          <cfinvokeargument name="screen_height" value="#screen_height#"/>
      </cfinvoke>
      
      <cfset deviceID = theDevice.device_id>
      
      <cfelse>
      <!--- If Not Exists, Create New Device --->
	  	<cfquery name="newDevice">
             INSERT INTO Devices (name, width, height, size, os)
             VALUES ('#trim(device_name)#',#screen_width#, #screen_height#, '#trim(screen_size)#', '#trim(operating_system)#') 
             SELECT @@IDENTITY AS deviceID
        </cfquery>
        
        <cfset deviceID = newDevice.deviceID>
      
      </cfif>
      
      <cfreturn deviceID>
        
    </cffunction>

    
    
    <!--- Epoch --->
    <cffunction name="convertDateToEpoch" returntype="date" output="no">
      <cfargument name="TheDate" type="string" required="false" default="0"/>
        
        <cfif TheDate GT 0>
            <cfset epoch = DateDiff("s", createdatetime(1970, 1, 1, 0, 0, 0), DateConvert("Local2utc", TheDate))>
        <cfelse>
        	<cfset localDate = now()>
            <cfset theDate = localDate.getTime()>
            <cfset epoch = Left(theDate,Len(theDate)-3)>
        </cfif>
        <cfreturn epoch/>
        
    </cffunction>
    
    <cffunction name="convertEpochToDate" returntype="date" output="yes">
        <cfargument name="TheEpoch" type="numeric" required="no" default="0" />
    
    	<cfif TheEpoch IS 0>
        	<cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="TheEpoch" />
        </cfif>
       
        <cfset theDate = DateAdd("s",TheEpoch,DateConvert("Local2utc", createdatetime(1970, 1, 1, 0, 0, 0)))>
        <cfreturn theDate/>
        
    </cffunction>
    
    
        <!---OS Type--->
    <cffunction name="getAppStore" access="remote" returntype="string" output="no">
        
        <cfargument name="os" type="string" required="no" default="0">

            <!---AppStore Link Types Filter--->            
            <cfswitch expression="#os#">
        
                <cfcase value="ios">
                    <cfset osType = "AppStore">
                </cfcase>
                
                <cfcase value="android">
                    <cfset osType = "GooglePlay">
                </cfcase>
                
                <cfcase value="osx">
                    <cfset osType = "AppStore">
                </cfcase>
                
                <cfcase value="win8">
                    <cfset osType = "MSStore">
                </cfcase>
                
                <cfdefaultcase>
                    <cfset osType = "">
                </cfdefaultcase>
            
        	</cfswitch>
            
            <cfreturn osType>
            
    </cffunction> 
    
    
    <!---Convert RGB to HEX--->
    <cffunction name="rgbToHex" access="remote" returntype="string" output="yes">
        
        <cfargument name="rgbColor" type="string" required="yes" default="rgb(0,0,0)">
         
        <cfif rgbColor IS ''><cfset rgbColor = "rgb(0,0,0)"></cfif>
    	
       <cfif NOT Find("rgb", rgbColor) GT 0>
			<cfreturn rgbColor>
        </cfif>
        
        <cfset a = listGetAt(rgbColor, 2, "(")>
        <cfset b = listGetAt(a, 1, ")")>
		<cfset rgbVal = trim(b)>

        <cfset r = listGetAt(rgbVal,1,",")>
        <cfset g = listGetAt(rgbVal,2,",")>
        <cfset b = listGetAt(rgbVal,3,",")>
        
        <cfinvoke component="Misc" method="componentToHex" returnvariable="red">
            <cfinvokeargument name="componentValue" value="#r#"/>
        </cfinvoke>
        
        <cfinvoke component="Misc" method="componentToHex" returnvariable="green">
            <cfinvokeargument name="componentValue" value="#g#"/>
        </cfinvoke>  
              
        <cfinvoke component="Misc" method="componentToHex" returnvariable="blue">
            <cfinvokeargument name="componentValue" value="#b#"/>
        </cfinvoke>
        
        <cfset hexColor = red & green & blue>
       
        <cfreturn hexColor>
    
    </cffunction>
    
    <!---Convert Component to HEX--->
    <cffunction name="componentToHex" access="remote" returntype="string" output="no">
        
        <cfargument name="componentValue" type="numeric" required="yes" default="0">
        
          <cfif IsNumeric(componentValue)>
 
          <cfset hex = formatBaseN( componentValue , 16)>
          
          <cfif len(hex) IS 1>
          	<cfset hex = "0" & hex>
          </cfif>
    	 
         <cfelse>
         	<cfset hex = "">
         </cfif>
            
        <cfreturn hex>
        
    </cffunction>
    
    
    <!---Convert HEX to RGB--->
    <cffunction name="hexToRGB" access="remote" returntype="string" output="yes">
        
        <cfargument name="hexValue" type="string" required="yes" default="#chr(34)&000000#">
	
        <cfset hexValue = listGetAt(hexValue,1,chr(35))>
     
        <cfset r = Mid(hexValue,1,2)>
        <cfset g = Mid(hexValue,3,2)>
        <cfset b = Mid(hexValue,5,2)>
        
			<cfif len(r) IS 0><cfset r = "00"></cfif>
			<cfif len(r) IS 1><cfset r = r&r></cfif>
			
			<cfif len(g) IS 0><cfset g = r></cfif>
			<cfif len(b) IS 0><cfset b = r></cfif>

           	<cfif len(g) IS 1><cfset r = g&"0"></cfif>
           	<cfif len(b) IS 1><cfset r = b&"0"></cfif>
           	
        <cfset red = InputBaseN( r , 16)>
        <cfset green = InputBaseN( g , 16)>
        <cfset blue = InputBaseN( b , 16)>
       
        <cfset rgb = "rgb(#red#,#green#,#blue#)">
    	
        <cfreturn rgb>
        
    </cffunction>   
    
    
<!--- CSV to JSON --->
	<cffunction name="CSVToJSON" access="public" output="yes" returntype="any">
    	<cfargument name="csv" type="string" required="yes">
        
        <cfset rows = ListToArray(csv,"",true)>
        <cfreturn rows>
    </cffunction>
 
<!---Convert Query to Array--->    
    <cffunction name="QueryToArray" access="public" returntype="array" output="false"
	hint="This turns a query into an array of structures.">

	<cfargument name="theQuery" type="query" required="yes"/>
	<cfargument name="theColumName" type="string" required="yes"/>

	<cfscript>
		var queryCount = theQuery.recordcount;
		var result = [];
		for(var i=1; i LTE queryCount; i++)
		{
			ArrayAppend(result, theQuery[theColumName][i]);
		}
		return result;
	</cfscript>
    
</cffunction>



<!---Convert Query To Struct--->
<cffunction name="QueryToStruct" returntype="any" hint="Converts a query to a struct, or an array of structs">
    <cfargument name="query" type="query" required="true"/>
    <cfargument name="row" type="query"/>
    <cfargument name="forceArray" type="boolean" default="false"/>
     
    <cfscript>
		var local = StructNew();
		var result = StructNew();
		var idx = "";
		var colName = "";
		var columnLabels = arguments.query.getMetaData().getColumnLabels();
    </cfscript>
     
    <cfif isDefined("arguments.row")>
		<cfset local.row = arguments.row/>
        <cfelseif arguments.query.recordCount eq 1>
        <cfset local.row = 1/>
    </cfif>
     
    <cfif isDefined("local.row") and not arguments.forceArray>
        <cfloop array="#columnLabels#" index="colName">
       		<cfset StructInsert(result, colName, Trim(arguments.query[colName][local.row]))/>
        </cfloop>
        
			<cfelseif isDefined("local.row")>
            <cfset result = ArrayNew(1)/>
            <cfset ArrayAppend(result, StructNew())/>
            
            <cfloop array="#columnLabels#" index="colName">
            	<cfset StructInsert(result[1], colName, Trim(arguments.query[colName][local.row]))/>
            </cfloop>
            
            <cfelse>
            
            <cfset result = ArrayNew(1)/>
             
            <cfloop from="1" to="#arguments.query.recordCount#" index="idx">
				<cfset local.tempStruct = StructNew()/>
                 
                <cfloop array="#columnLabels#" index="colName">
                <cfset StructInsert(local.tempStruct, colName, Trim(arguments.query[colName][idx]))/>
            </cfloop>
             
            <cfset ArrayAppend(result, local.tempStruct)/>
            
        </cfloop>
    </cfif>
     
    <cfreturn result/>
    
</cffunction>


<!---Generate Image Sizes--->
<cffunction name="createNonRetinaImage" access="public" returntype="boolean" output="true">
    
	<cfargument name="imageSrc" type="string" required="yes" default=""/>
    <cfargument name="imageDes" type="string" required="yes" default=""/>
	
    <!--- Specs --->
    <cfinvoke component="File" method="getFileSpecs" returnvariable="fileSpecs">
        <cfinvokeargument name="fullPath" value="#imageSrc#"/>
    </cfinvoke>
    
    <cfset ext = trim(fileSpecs.ext)>
    
   <cfif ext IS "jpg" OR ext IS "jpeg" OR ext IS "png">
	 
    <cfif fileExists(expandPath(imageSrc))>
    	
        <cfset uniqueThread = CreateUUID()>
        
        <cfthread action="run" name="saveNonRetina_#uniqueThread#" imageSrc="#imageSrc#" imageDes="#imageDes#" ext=#ext#>
        
            <cfimage source="#expandPath(imageSrc)#" name="myImage" action="read">
            
            <cfset w = myImage.width><cfset h = myImage.height>
            
            <cfset w = w / 2><cfset h = h / 2>
            <cfset ImageScaleToFit(myImage,w,h,"highestQuality")>
      
            <cfimage source="#myImage#" format="#ext#" destination="#expandPath(imageDes)#" action="write" quality="1" overwrite="true">
		
       </cfthread>
        
    <cfelse>
    	<cfreturn false>
    </cfif>
	
    <cfelse>
    	<cfreturn false>
    </cfif>
    
	<cfreturn true>
    
</cffunction>



<!---Generate Image Sizes--->
<cffunction name="getImageSpecs" access="public" returntype="struct" output="true">
    
	<cfargument name="imageSrc" type="string" required="yes" default=""/>
	
    <cfif findnocase("http:",imageSrc) GT 0>
    
		<cfset imageSrc = replace(imageSrc," ","%20","all")>
        
        <!--- Check if File Exists --->
        <cfhttp method="head" url="#imageSrc#" />
        <cfset error = listGetAt(cfhttp.Statuscode,1," ")>
        <cfif error IS 404>
            <cfset fileExists = false>
        <cfelse>
            <cfif IsNumeric(error)>
                <cfset fileExists = true>
            <cfelse>
                <cfset fileExists = false>
            </cfif>
        </cfif>
	
    <cfelse>
    	<cfset fileExists = fileExists(imageSrc)>
    </cfif>
    
    <cfset imageSize = structNew()>
    
    <cfif fileExists>
    	
        <cfset imageSize = {"width":0,"height":0, "ratio":0}>
        
        <cfif getFIleFromPath(imageSrc) NEQ ''>
        
        	<cfimage source="#imageSrc#" action="info" structName="myImage">
        	
            <cfset w = myImage.width>
			<cfset h = myImage.height>
            
            <cfif w GT 0 and h GT 0>
                <cfset r = w/h>
            <cfelse>
                <cfset r = 0>
            </cfif>
            
            <cfset imageSize.width = w>
            <cfset imageSize.height = h>
            <cfset imageSize.ratio = r>

        </cfif>

    </cfif>
    
    <cfreturn imageSize>
    
</cffunction>



<!--- Get File Info --->
<cffunction name="getFileInfo" returntype="struct" output="no">

    <cfargument name="filePath" type="string" required="yes">
    
    <cfset infoData = {"size":0, "file":"", "path":""}>
    
    <cfif fileExists(filePath)>
    
		<cfset info = GetFileInfo(filePath)>
        
        <cfset size = NumberFormat(info.size/1000000, "9.99")>
        
        <cfset infoData.size = size>
        <cfset infoData.file = info.name>
		<cfset infoData.path = info.parent>
    
    </cfif>
        
    <cfreturn infoData>
    
</cffunction>







<!--- Get A Query Row --->
<cffunction name="getQueryRow" returntype="query" output="no">

    <cfargument name="qry" type="query" required="yes">
    <cfargument name="row" type="numeric" required="yes">
    
    <cfset arguments.qryRow=QueryNew(arguments.qry.columnlist)>
    
    <cfset QueryAddRow(arguments.qryRow)>
    
    <cfloop list="#arguments.qry.columnlist#" index="arguments.column">
        <cfset QuerySetCell(arguments.qryRow,arguments.column,Evaluate("arguments.qry.#arguments.column#[arguments.row]"))>
    </cfloop>
    
    <cfreturn arguments.qryRow>
    
</cffunction>



<!--- Update All Unknow Locations --->
<cffunction name="updateAllUnknowLocations" returntype="struct" output="no">

    <cfquery name="allLocations">        
        SELECT ip
        FROM Locations
        WHERE longitude = 0 AND latitude = 0
    </cfquery>
    
    <cfset updated = []>
    
    <cfoutput query="allLocations">
    
		<!--- Get Location --->
        <cfinvoke component="Misc" method="getServerGeoLocation" returnvariable="location">
            <cfinvokeargument name="ip" value="#ip#"/>
        </cfinvoke>
   
    	<cfif location.longitude IS 0 OR location.latitude IS 0>
        	<!--- nothing --->
        <cfelse>
        
			<!--- Update Location --->
            <cfquery name="result">
                UPDATE	Locations
                SET 	city = "#location.city#", country_code ="#location.country_code#", country_name ="#location.country#", 
                		latitude =#location.latitude#, longitude =#location.longitude#, region_code ="#location.region_code#", region_name ="#location.region#"
                WHERE	ip = "#ip#"
            </cfquery>
            
            <cfset arrayAppend(updated,ip)>
            
        </cfif>
        
    </cfoutput>
    
    <cfset data = {"ips":updated, "total":arrayLen(updated)}>
    
    <cfreturn data>
    
</cffunction>



<!--- Get GeoLoaction from IP --->
<cffunction name="getServerGeoLocation" returntype="struct" output="no" access="remote">
	<cfargument name="ip" type="string" required="no" default="">
    
    <!--- Get Server IP if no IP provided --->
    <cfif ip IS "">
    	<cfset ip = CGI.REMOTE_ADDR>
    </cfif>
    
    <cfset dataStruct = {"ip":"#ip#","city":"unknown","country_code":"NA", "country":"unknown", "latitude":0, "longitude":0, "region_code":"NA", "region":"unknown"}>
    
    <!--- <cfset requestString = "http://freegeoip.net/json/#ip#"> --->
    
	<!--- <cfset requestString = "http://www.telize.com/geoip/#ip#"> --->
    
    <!--- <cfset requestString = "http://ip-api.com/json/#ip#"> --->
    
    <cfset apiKey = "dcec64de3c44deb133918a912ff5bc0e902de1a17ebdcd396358571b21ff867c">
    <cfset requestString = "http://api.ipinfodb.com/v3/ip-city/?key=#apiKey#&ip=#ip#&format=json&ip-country">
  
    <cftry>

		<!---Code tried --->
        <cfhttp url="#requestString#" method="get" result="httpResp" timeout="5" throwonerror="no" />
    
        <cfcatch type="Exception_Type">
        
        <!---Code executed when the code above fails--->
        
        </cfcatch>
    
    </cftry> 
    
		<cfif isDefined("httpResp.Statuscode")>
        
            <cfif httpResp.Statuscode GTE "404">
                <!--- nothing --->
            <cfelse>
          
                <cfset JSON = trim(httpResp.fileContent)>
                
                <cfif JSON is ''>
                	<cfreturn dataStruct>
                <cfelse>
                
					<cfset data = deserializeJSON(JSON)>
   
                    <cfif isDefined("data.city")>
                        <cfset dataStruct.city = data.city>
                    </cfif>
                    <cfif isDefined("data.countryCode")>
                        <cfset dataStruct.country_code = data.countryCode>
                    </cfif>
                    <cfif isDefined("data.country")>
                        <cfset dataStruct.country = data.country>
                    </cfif>
                    <cfif isDefined("data.lat")>
                        <cfset dataStruct.latitude = data.lat>
                    </cfif>
                    <cfif isDefined("data.lon")>
                        <cfset dataStruct.longitude = data.lon>
                    </cfif>
                    <cfif isDefined("data.region")>
                        <cfset dataStruct.region_code = data.region>
                    </cfif>
                    <cfif isDefined("data.regionName")>
                        <cfset dataStruct.region = data.regionName>
                    </cfif>
				
                </cfif>
                
            </cfif>
    
        </cfif>
     
    	<cfreturn dataStruct>
    
</cffunction>



    <!---Cleanup Temp Directory--->
    <cffunction name="cleanupTemp" access="remote" returntype="struct" output="yes">
        
        <cfdirectory directory="#GetTempDirectory()#" name="allFiles" action="LIST" />
        
        <cfset totalToDelete = 0>
        
        <cfset deletedFiles = arrayNew(1)>
        <cfset NOTdeletedFiles = arrayNew(1)>

        <cfoutput query="allFiles">
        
        	<cfset theFile = directory &"/"& name>
            
            <!--- Specs --->
            <cfinvoke component="File" method="getFileSpecs" returnvariable="fileSpecs">
                <cfinvokeargument name="fullPath" value="#theFile#"/>
            </cfinvoke>
            
            <cfif fileSpecs.type IS "file">
                <cffile action="delete" file="#theFile#">
                <cfset arrayAppend(deletedFiles,name)>
                <cfset totalToDelete += 1>
            <cfelse>
            	<cfset arrayAppend(NOTdeletedFiles,name)>
            </cfif>
            
        </cfoutput>
        
        <!--- delete folders --->
        <!--- <cfloop index="aFileFolder" array="#NOTdeletedFiles#">
        
        	<!--- Specs --->
            <cfinvoke component="File" method="getFileSpecs" returnvariable="fileSpecs">
                <cfinvokeargument name="fullPath" value="#theFile#"/>
            </cfinvoke>
        	
            <cfif fileSpecs.type NEQ "file">
        		<cfset DirectoryDelete(aFileFolder,true)> 
            </cfif>
            
        </cfloop> --->
        
        <cfset filesDeleted = {"files":deletedFiles,"total":totalToDelete, "notDeleted":NOTdeletedFiles}>
        
        <cfreturn filesDeleted>
        
    </cffunction>


    <!---Delete all Files in Directory--->
    <cffunction name="deleteAllFilesInDirectory" access="remote" returntype="boolean" output="yes">
        <cfargument name="directory" type="string" required="yes">
        <cfargument name="forceDelete" type="boolean" required="no" default="false">
        	
            <cfinvoke component="File" method="getRootPath" returnvariable="rootpath" />
            
        	<cfset pathDirectory = rootpath & directory>

            <cfif directoryExists(pathDirectory)>

            <cfdirectory directory="#pathDirectory#" name="allFiles" action="LIST" />

            <cfoutput query="allFiles">
            
                <cfset theFile = pathDirectory & name>
                
                <!--- Specs --->
                <cfinvoke component="File" method="getFileSpecs" returnvariable="fileSpecs">
                    <cfinvokeargument name="fullPath" value="#theFile#"/>
                </cfinvoke>
                
                <cfif fileSpecs.type IS "file" AND fileExists(theFile)>
                    <cfif forceDelete>
                        <cffile action="delete" file="#theFile#">
                    <cfelse>
                        <cfdump var="#theFile#"><br>
                    </cfif>
                </cfif>
                
            </cfoutput>
            
            </cfif>

        
        <cfreturn true>
        
    </cffunction>





    <!---Cleanup Temp Directory--->
    <cffunction name="emailSupportType" access="remote" returntype="boolean" output="no">
        <cfargument name="assetType" type="numeric" required="yes" default="0">

		<!--- Asset Types --->
        <cfswitch expression="#assetType#">
      
			<!--- Image --->    
            <cfcase value="1">
				<cfreturn true>
            </cfcase>
            
            <!--- Video --->
            <cfcase value="2">
 				<cfreturn true>
            </cfcase>
            
            <!--- Document --->
            <cfcase value="3"> 
				<cfreturn true>
            </cfcase>
            
            <!--- URL --->
            <cfcase value="4">
				<cfreturn true>
            </cfcase>
            
            <!--- GPS Location --->    
            <cfcase value="5">         
				<cfreturn false>
            </cfcase>
            
            <!--- 3D Point --->    
            <cfcase value="10">         
				<cfreturn false>
            </cfcase>
    
            <!--- 3D Model --->
            <cfcase value="6">
				<cfreturn false>
            </cfcase>
            
            <!--- Panorama --->    
            <cfcase value="8"> 
				<cfreturn true>
            </cfcase>
            
            <!--- Balcony --->    
            <cfcase value="9">  
            	<cfreturn true>
            </cfcase>
            
            <!--- Group Asset --->    
            <cfcase value="18">  
            	<cfreturn true>
            </cfcase>
            
                                    
            <!--- Nothing Error --->
            
            <cfdefaultcase>
    			<cfreturn false>
            </cfdefaultcase>
    
		</cfswitch>

	</cffunction>
    
    
    
    <cffunction name="findAll" type="public" output="no" returnType="struct">
	
        <cfargument name="stringToSearch" type="string" required="yes">
        <cfargument name="valueToFind" type="string" required="yes">
        
        <cfset foundItems = structNew()>
        <cfset dummy = structInsert (foundItems, "LEN", arrayNew(1))>
        <cfset dummy = structInsert (foundItems, "POS", arrayNew(1))>
        
        <cfset startPos = 1>
        
        <cfloop condition = "startPos gt 0">
        
			<cfset tempResult = REFind (valueToFind, stringToSearch, startPos,"true")>
            
            <cfif tempResult.pos[1] is not 0>
				<cfset dummy = arrayAppend (foundItems.LEN, tempResult.LEN)>
                <cfset dummy = arrayAppend (foundItems.POS, tempResult.POS)>
                <cfset startPos = tempResult.pos[1] + tempResult.len[1] + 1>
            <cfelse>
            	<cfset startPos = 0>
            </cfif>
            
        </cfloop>
        
        <cfreturn foundItems>
        
    </cffunction>




    <cffunction name="reSplit" access="public" returntype="array" output="false" hint="I split the given string using the given Java regular expression.">
        <cfargument name="regex" type="string" required="true" hint="I am the regular expression being used to split the string."/>
        <cfargument name="value" type="string" required="true" hint="I am the string being split."/>

		<cfset var local = {} />
    
        <cfset local.parts = javaCast( "string", arguments.value ).split(javaCast( "string", arguments.regex ), javaCast( "int", -1 )) />
    
        <cfset local.result = [] />

        <cfloop index="local.part" array="#local.parts#">
         	<cfif local.part NEQ ''>
            	<cfset arrayAppend( local.result, local.part )>
         	</cfif>
        </cfloop>

    	<cfreturn local.result>
        
    </cffunction> 
    
    
    
           <!--- get GPS location distance from 2 gps (long, latt) points --->
    <cffunction name="GetLatitudeLongitudeProximity" access="public" returntype="numeric" output="false"
    hint="I find the approximate distance in miles between the given pair of latititude and longitude values. The closer you get to the North or South Pole, the less accurate this becomes due to changes in distance between degrees. Math borrowed from http://zips.sourceforge.net.">

    <!--- Define arguments. --->
    <cfargument name="FromLatitude" type="numeric" required="true" hint="I am the starting latitude value." />
    <cfargument name="FromLongitude" type="numeric" required="true" hint="I am the starting longitude value." />
    <cfargument name="ToLatitude" type="numeric" required="true" hint="I am the target latitude value." />
    <cfargument name="ToLongitude" type="numeric" required="true" hint="I am the target longitude value." />
    <cfargument name="unit" type="string" required="no" default="" /><!--- feet, miles, kilometer, meter --->

		<!--- Define the local scope. --->
        <cfset var LOCAL = {} />

        <cfset LOCAL.MilesPerLatitude = 69.09 />
    
        <!---
            Calculate the distance in degrees between the two
            different latitude / longitude locations.
        --->
        <cfset LOCAL.DegreeDistance = RadiansToDegrees(
            ACos(
                    (
                        Sin( DegreesToRadians( ARGUMENTS.FromLatitude ) ) *
                        Sin( DegreesToRadians( ARGUMENTS.ToLatitude ) )
                    )
                +
                    (
                        Cos( DegreesToRadians( ARGUMENTS.FromLatitude ) ) *
                        Cos( DegreesToRadians( ARGUMENTS.ToLatitude ) ) *
                        Cos( DegreesToRadians( ARGUMENTS.ToLongitude - ARGUMENTS.FromLongitude ) )
                    )
                )
            ) />
    
    
        <!---
            Given the difference in degrees, return the approximate
            distance in miles.
        --->
        <cfset distance = LOCAL.DegreeDistance * LOCAL.MilesPerLatitude>
        
        <cfif unit IS "k">
			<cfset unitDistance = distance*1.60934>
        <cfelseif unit IS "m">
			<cfset unitDistance = distance*1609.33999997549>
        <cfelseif unit IS "ft">
			<cfset unitDistance = distance*5280>
        <cfelse>
        	<cfset unitDistance = distance>
        </cfif>
        
        <cfreturn unitDistance>
    
    </cffunction>
    
    
    <!--- Degrees to Radius Convertion --->
    <cffunction name="DegreesToRadians" access="public" returntype="numeric" output="false" hint="I convert degrees to radians.">
        <cfargument name="Degrees" type="numeric" required="true" hint="I am the degree value to be converted to radians." />
    
        <cfreturn (ARGUMENTS.Degrees * Pi() / 180) />
        
    </cffunction>
    
    
    <cffunction name="RadiansToDegrees" access="public" returntype="numeric" output="false" hint="I convert radians to degrees.">
        <cfargument name="Radians" type="numeric" required="true" hint="I am the radian value to be converted to degrees." />
    
        <cfreturn (ARGUMENTS.Radians * 180 / Pi()) />
        
    </cffunction>
    
    
    <!--- STACKTRACE --->
    <!--- <cfinvoke component="Misc" method="CallStackDump"> --->
    <cffunction name="CallStackDump" output="true" returntype="void">
      
      <cfscript>
        var lc = StructNew();
        lc.trace = CallStackGet();
        lc.op = ArrayNew(1);
        lc.elCount = ArrayLen(lc.trace);
        // Skip 1 (CallStackDump)
        for (lc.i = 2; lc.i lte lc.elCount; lc.i = lc.i + 1) {
          if (Len(lc.trace[lc.i]["Function"]) Gt 0) {
            ArrayAppend(lc.op, lc.trace[lc.i].Template & ":" & lc.trace[lc.i]["Function"] & ":" & lc.trace[lc.i].LineNumber);
          } else {
            ArrayAppend(lc.op, lc.trace[lc.i].Template & ":" & lc.trace[lc.i].LineNumber);
          }
        }
       
        
      </cfscript>
      
      <cfdump var="#lc.op#">
      
    </cffunction>
    
    
    <cffunction name="ArrayOfStructSort" returntype="array" access="public" output="no">
          <cfargument name="base" type="array" required="yes" />
          <cfargument name="sortType" type="string" required="no" default="text" />
          <cfargument name="sortOrder" type="string" required="no" default="ASC" />
          <cfargument name="pathToSubElement" type="string" required="no" default="" />
        
          <cfset var tmpStruct = StructNew()>
          <cfset var returnVal = ArrayNew(1)>
          <cfset var i = 0>
          <cfset var keys = "">
        
          <cfloop from="1" to="#ArrayLen(base)#" index="i">
            <cfset tmpStruct[i] = base[i]>
          </cfloop>
        
          <cfset keys = StructSort(tmpStruct, sortType, sortOrder, pathToSubElement)>
        
          <cfloop from="1" to="#ArrayLen(keys)#" index="i">
            <cfset returnVal[i] = tmpStruct[keys[i]]>
          </cfloop>
        
          <cfreturn returnVal>
          
    </cffunction>
    
    <!--- find Replace Stuct Value --->
    <cffunction name="findReplaceStuctValue" returntype="void" access="public" output="no">
          <cfargument name="theObject" type="struct" required="yes" />
          <cfargument name="keyToFind" type="any" required="yes" />
          <cfargument name="valueToReplace" type="any" required="yes" />
          <cfargument name="scope" type="string" required="no" default="all" />
          
          <cfif scope NEQ "all"><cfset scope = ''></cfif>
          
			<!--- findAll --->
            <cfset foundObj = StructFindKey(theObject, keyToFind, scope)>
            
            <cfif arrayLen(foundObj) GT 0>
            
                <cfloop index="anObj" array="#foundObj#">
                	<cfset anObj.owner[keyToFind] = valueToReplace>
                </cfloop>

            </cfif>
        
    </cffunction>
    
    
    <!--- find Replace Stuct Value --->
    <cffunction name="rotateImage" returntype="boolean" access="public" output="no">
          <cfargument name="assetID" type="numeric" required="yes" />
          <cfargument name="rotateClockwise" type="boolean" required="no" default="yes" />
          
          	<cfset uniqueThread = CreateUUID()>
            
            <!--- FILE --->
            <cfinvoke component="Content" method="getAsset" returnvariable="data">
                <cfinvokeargument name="assetID" value="10244"/>
            </cfinvoke>
            
            
             <!--- Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="10244"/>
            </cfinvoke>
            
            <!--- HIRES --->
            <cfset theFilePath = "#assetPath##data.url#">
            
            <!--- LOW-HIRES --->
            <cfset theFilePathNonretina = "#assetPath#nonretina/#data.url#">
        	
            <cfif rotateClockwise>
            	<cfset theAngle = 90>
            <cfelse>
            	<cfset theAngle = -90>
            </cfif>
            
        	<!--- <cfthread action="run" name="#uniqueThread#" theFilePath="#theFilePath#" imageHI="#imageHI#" imageLO=#imageLO#>   --->
            
          	<cfif fileExists(theFilePath)>
				<cfset imageHI=ImageNew(theFilePath)> 
                <cfset ImageSetAntialiasing(imageHI,"on")> 
                <cfset ImageRotate(imageHI,theAngle)>
                <cfimage source="#imageHI#" action="write" destination="#theFilePath#" overwrite="yes">
            <cfelse>
            	<cfdump var="No File Found"><cfabort>
            </cfif>
            
            <cfif fileExists(theFilePathNonretina)>
            	<cfset imageLO=ImageNew(theFilePathNonretina)> 
                <cfset ImageSetAntialiasing(imageLO,"on")> 
                <cfset ImageRotate(imageLO,theAngle)>
                <cfimage source="#imageLO#" action="write" destination="#theFilePathNonretina#" overwrite="yes">
            <cfelse>
            	<cfdump var="No File Found"><cfabort>
            </cfif>
    		
            <!--- </cfthread> --->
            
            <cfreturn true>
            
    </cffunction>
    
    <cffunction name="QueryAppend" access="public" returntype="query" output="false"
		hint="This takes two queries and appends the second one to the first one. Returns the resultant third query.">

		<!--- Define arguments. --->
		<cfargument name="QueryOne" type="query" required="true" />
		<cfargument name="QueryTwo" type="query" required="true" />
		<cfargument name="UnionAll" type="boolean" required="false" default="true" />

		<!--- Define the local scope. --->
		<cfset var LOCAL = StructNew() />

		<!--- Append the second to the first. Do this by unioning the two queries. --->
		<cfquery name="LOCAL.NewQuery" dbtype="query">
				<!--- Select all from the first query. --->
				(
					SELECT
						*
					FROM
						ARGUMENTS.QueryOne
				)
			<!--- Union the two queries together. --->
			UNION
			<!---
				Check to see if we are going to care about duplicates. If we don't
				expect duplicates then just union all.
			--->
			<cfif ARGUMENTS.UnionAll>
				ALL
			</cfif>
				<!--- Select all from the second query. --->
				(
					SELECT
						*
					FROM
						ARGUMENTS.QueryTwo
				)
		</cfquery>

		<!--- Return the new query. --->
		<cfreturn LOCAL.NewQuery />
	</cffunction>
   
    
</cfcomponent>