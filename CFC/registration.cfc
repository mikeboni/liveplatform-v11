<cfcomponent>

	<!--- How Hear --->
	<cffunction name="howHearTypes" access="remote" returntype="struct">
        
        <cfset result = {}>
        
        <!--- get how hear --->
        <cfquery name="allTypes">
            SELECT        howHear_id AS id, type
            FROM          HowHear
            ORDER BY 	  type ASC
        </cfquery>
        
        <cfoutput query="allTypes">
        	<cfset structAppend(result,{"#type#":#id#})>
        </cfoutput>
     
        <cfreturn result>
        
    </cffunction>
        

	<!--- authenticate user --->
	<cffunction name="authenticateUser" access="remote" returntype="any">
		<cfargument name="email" type="string" required="no" default="">
		<cfargument name="pass" type="string" required="no" default="">
        <cfargument name="token" type="string" required="no" default="">
        
        <cfset result = {'success':false}>
        
        <cfset apps = {}>
        
        <cfif token NEQ ''>
        <!--- authenticate with user token --->
        	<cfquery name="userAccess">
                SELECT      AccessLevels.accessLevel 
				FROM        Users INNER JOIN
							AccessLevels ON Users.access_id = AccessLevels.access_id LEFT OUTER JOIN
							Tokens ON Users.user_id = Tokens.user_id
				WHERE       Tokens.token = '#token#'
            </cfquery>
         
            <cfif userAccess.recordCount GT 0>
            <cfset result = {'success':true, 'access':userAccess.accessLevel}>
			</cfif>
            
        <cfelse>
        <!--- authenticate with user info --->
        <cfif email NEQ '' AND pass NEQ ''>
        
            <!--- validate user for admin --->
            <cfquery name="userAccess">
                SELECT        Users.user_id, Users.app_id AS appID, Applications.appName AS appName, AccessLevels.accessLevel AS accessLevel
                FROM          Users INNER JOIN
                                        AccessLevels ON Users.access_id = AccessLevels.access_id INNER JOIN
                                        Applications ON Users.app_id = Applications.app_id
                WHERE        (Users.email = '#email#') AND (Users.password COLLATE Latin1_General_CS_AS = '#pass#') AND (Users.active = 1) AND (AccessLevels.accessLevel >= 4)
                ORDER BY Applications.appName ASC
            </cfquery>
            
            <cfif userAccess.accessLevel IS 5>
            	<!--- Super, Get All Clients and Apps --->
                <cfinvoke component="Clients" method="getClientProjects" returnvariable="data" />
                
                <cfset result = {'success':true, 'apps':data.apps, 'clients':data.clients}>
            
            <cfelse>
            
				<!--- Normal Admin User --->
                <cfif userAccess.recordCount GT 0>
                
                    <!--- return query of apps --->
       
                    <cfloop query="userAccess">
                    <cfset structAppend(apps,{'#appID#':'#appName#'})>
                     
                        <!--- return query of projects --->
                        <cfquery name="projectList">
                            SELECT        name, group_id
                            FROM          Groups
                            WHERE        (app_id = #appID#) AND (subgroup_id = 0)
                            ORDER BY name ASC
                        </cfquery>
    
                    </cfloop>
                    
                    <cfset result = {'success':true, 'apps':apps}>
    
                </cfif>
            
            </cfif>
            
        </cfif>
		
       </cfif>
        
		<cfreturn result>
        
	</cffunction>
    
    
    <!--- authenticate user for Registrations --->
	<cffunction name="authenticateUserCms" access="remote" returntype="struct">
		<cfargument name="email" type="string" required="no" default="">
		<cfargument name="pass" type="string" required="no" default="">

        <!--- Authenticate User --->
 		<cfinvoke component="registration" method="authenticateUserCredentials" returnvariable="result">
            <cfinvokeargument name="email" value="#email#">
            <cfinvokeargument name="pass" value="#pass#">
        </cfinvoke>
        
        <cfset theURL = 'API/V11/ClentsView.cfm?apps=#arrayToList(result.apps)#&clients=#arrayToList(result.clients)#&useraccess=#result.access#&token=#result.token#&userDev=#result.dev#'>
        
        <cfset structAppend(result, {'url': theUrl})>
        
		<cfreturn result>
        
	</cffunction>
  
   <!--- authenticate user for Help Tickets --->
	<cffunction name="authenticateUserHelpTickets" access="remote" returntype="struct">
		<cfargument name="email" type="string" required="no" default="">
		<cfargument name="pass" type="string" required="no" default="">
        
        
        
        <!--- Authenticate User --->
 		<cfinvoke component="registration" method="authenticateUserCredentials" returnvariable="result">
            <cfinvokeargument name="email" value="#email#">
            <cfinvokeargument name="pass" value="#pass#">
        </cfinvoke>
        
        <cfset theURL = 'help/help.cfm?apps=#arrayToList(result.apps)#&clients=#arrayToList(result.clients)#&useraccess=#result.access#&token=#result.token#&userDev=#result.dev#'>
        
        <cfset structAppend(result, {'url': theUrl})>
        
		<cfreturn result>
        
	</cffunction>
   
   
   <!--- Authenticate User --->
   <cffunction name="authenticateUserCredentials" access="remote" returntype="struct">
		<cfargument name="email" type="string" required="no" default="">
		<cfargument name="pass" type="string" required="no" default="">
        <cfargument name="theUrl" type="string" required="no" default="">
        
        <cfset result = {'success':false}>
        
        <cfset apps = {}>
        
        <cfif email NEQ '' AND pass NEQ ''>
        
            <!--- validate user for admin --->
            <cfquery name="userAccess">
  				SELECT  Users.user_id AS userID, Users.app_id AS appID, Users.client_id AS clientID, Users.dev AS dev, Applications.appName, AccessLevels.accessLevel, Applications.client_id, Tokens.token AS atoken
				FROM    Users INNER JOIN
                        AccessLevels ON Users.access_id = AccessLevels.access_id INNER JOIN
                        Applications ON Users.app_id = Applications.app_id LEFT OUTER JOIN
                        Tokens ON Users.user_id = Tokens.user_id
                WHERE   (Users.email = '#email#') AND (Users.password COLLATE Latin1_General_CS_AS = '#pass#') AND (Users.active = 1) AND (AccessLevels.accessLevel >= 4)
                ORDER BY Applications.appName ASC
            </cfquery>
            
            <cfset adminAccess = false>
            <cfset superAccess = false>
            <cfset token = ''>
            
            <cfif userAccess.atoken IS ''>
				<!--- Create Token for User --->
				<cfinvoke component="Tokens" method="createToken" returnvariable="newToken"></cfinvoke>

				<!---Today Epoch--->
				  <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate"></cfinvoke>

				<!--- Create a New Token --->
				<cfquery name="tokenNew">
					INSERT INTO Tokens (token, app_id, created, modified, user_id)
                  	VALUES ('#newToken#', #userAccess.appID#, #curDate#, #curDate#, #userAccess.userID#)
				</cfquery>

				<cfset userToken = newToken>
				
			<cfelse>
           		<cfset userToken = userAccess.aToken>
            </cfif>
           
            <cfset clientsAccessible = []>
            <cfset appsAccessible = []>
            
            <cfoutput query="userAccess">
            	<cfset arrayAppend(appsAccessible, appID)>
            	<cfset arrayAppend(clientsAccessible, clientID)>
				<cfif accessLevel IS 4 AND NOT superAccess>
          			<cfset adminAccess = true>
					<cfif token NEQ ''><cfset token = atoken></cfif>
          		</cfif>
           		<cfif accessLevel IS 5>
           			<cfset superAccess = true>
           			<cfif token NEQ ''><cfset token = atoken></cfif>
           		</cfif>
            </cfoutput>
      
           <cfif superAccess> 
           <!--- all apps --->
           <cfinvoke  component="Apps" method="getClientApps" returnvariable="cmsApps" />
          	
          	<cfset appsAccessible = []>
          	<cfset clientsAccessible = []>
     
			<cfoutput query="cmsApps">
        		<cfset arrayAppend(appsAccessible, app_ID)>
        		<cfset arrayAppend(clientsAccessible, client_ID)>
			</cfoutput>
          	
           </cfif>
           
		  <cfif userAccess.dev IS ''>
         	<cfset dev = 0>
          <cfelse>
          	<cfset dev = 1>
          </cfif>
           
           <cfif adminAccess OR superAccess>
            	<!--- Super, Get All Clients and Apps --->
                <!--- <cfset theURL = 'API/V11/ClentsView.cfm?apps=#arrayToList(appsAccessible)#&clients=#arrayToList(clientsAccessible)#&useraccess=#userAccess.accessLevel#&userAccess=#userToken#&userDev=#dev#'> --->
                <cfset result.success = true>
                <cfset structAppend(result,{'apps':appsAccessible})>
                
                <cfset structAppend(result,{'clients':clientsAccessible})>
                <cfset structAppend(result,{'access':userAccess.accessLevel})>
                <cfset structAppend(result,{'token':userToken})>
           		<cfset structAppend(result,{'dev':dev})>
           		
            <cfelse>
            	<!--- No Permission --->
            	<cfset result = {'success':false}>
            </cfif>
            
        </cfif>
        
		<cfreturn result>
        
	</cffunction>
    
    
    
    <cffunction name="getAppProjects" access="remote" returntype="struct">
		<cfargument name="appID" type="numeric" required="no" default="0">
        
        <cfset result = {'success':false}>
        
        <cfif appID GT 0>
        
			<!--- return query of projects --->
            <!--- <cfquery name="projectList">
                SELECT        name, group_id
                FROM          Groups
                WHERE        (app_id = #appID#) AND (subgroup_id = 0)
                ORDER BY name ASC
            </cfquery> --->
            
            <cfquery name="projectList">
                SELECT	Groups.group_id, Groups.name AS name
                FROM	Groups RIGHT OUTER JOIN
                        ContentAssets ON Groups.group_id = ContentAssets.instanceAsset_id LEFT OUTER JOIN
                        Assets AS Assets_1 ON ContentAssets.instanceAsset_id = Assets_1.asset_id
                WHERE   (ContentAssets.instanceAsset_id IS NOT NULL) AND (ContentAssets.displayType = 7) AND (Groups.app_id = #appID#)
            </cfquery>
            
            <cfif projectList.recordCount GT 0>
            
                <cfset projects = {}>
                
                <cfif projectList.recordCount GTE 1>
                
                    <cfloop query="projectList">
                        <cfset structAppend(projects,{'#group_id#':'#name#'})>
                    </cfloop>
                    
                </cfif>
                
                <cfset result = {'success':true, 'projects':projects, 'appID':appID}>
                
            </cfif>
        
        </cfif>
            
		<cfreturn result>
        
	</cffunction>
    
    
    
    <cffunction name="getRegistrations" access="remote" returntype="array">
		<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="projID" type="numeric" required="no" default="0">
        
        <cfset data = arrayNew(1)>
        
        <cfif appID GT 0>
        
            <cfquery name="projectRegistrations">
                SELECT   Registration.firstName AS first, Registration.lastName AS last, Registration.email AS email, 
                		 Registration.phone AS phone, Registration.city AS city, Registration.postalZip AS zip, Registration.subscribe AS subscribed, 
                         Registration.confirmRegistration AS confirm, HowHear.type AS HowHear, Registration.created AS date
                FROM            Registration LEFT OUTER JOIN
                                                HowHear ON Registration.howHear_id = HowHear.howHear_id
                WHERE        (Registration.app_id = #appID#)
                <cfif projID GT 0>
                			 AND Registration.project_id = #projID#
                </cfif>
                ORDER BY Registration.project_id, Registration.created, Registration.city, Registration.lastName ASC
            </cfquery>
       
        	<cfoutput query="projectRegistrations">
			
				<cfif date IS ''>
					<cfset theDate = 0>
                <cfelse>
                	<cfset theDate = date>
                </cfif>
                
                 <!--- convertEpochToDate --->
                 <cfinvoke component="API.V11.CFC.Misc" method="convertEpochToDate" returnvariable="theDate">
                      <cfinvokeargument name="TheEpoch" value="#theDate#"/>
                 </cfinvoke>
                
                <cfif confirm IS ''>
					<cfset confirmVal = 0>
                <cfelse>
                	<cfset confirmVal = confirm>
                </cfif>
                
                <cfif HowHear IS ''>
					<cfset HowHearVal = 0>
                <cfelse>
                	<cfset HowHearVal = HowHear>
                </cfif>
                
                <cfif subscribed IS ''>
					<cfset subscribedVal = 0>
                <cfelse>
                	<cfset subscribedVal = subscribed>
                </cfif>

                <cfset theReg = {"firstName":first, "lastName":last, "email":email, "phone":phone, "city":city, "postalzip":zip, "subscribed":subscribedVal, "confirmed":confirmVal, "howHear":HowHearVal, "date":theDate}>
                <cfset arrayAppend(data,theReg)>
			
			</cfoutput>
            
        </cfif>

        <cfreturn data>
        
    </cffunction>    
 
 
 	<cffunction name="getClientIcon" access="remote" returntype="struct">
		<cfargument name="appID" type="numeric" required="no" default="0">
        
         <cfquery name="appsInfo">
            SELECT        Applications.app_id, path AS appPath, Applications.icon AS appIcon, Applications.appName, Applications.active, Applications.support, Applications.clientEmail, Applications.download, Applications.product_id,  Applications.client_id AS clientID, Applications.bundle_id, version, Applications.forced_update, Sessions.server_api AS api
            FROM          Applications INNER JOIN
                            Prefs ON Applications.prefs_id = Prefs.prefs_id INNER JOIN
                            Sessions ON Prefs.session_id = Sessions.session_id
            WHERE         (app_id = #appID#) 
        </cfquery>
        
        <cfset clientID = appsInfo.clientID>
        
        <!--- Get Paths from ClientID --->
        <cfquery name="clientInfo">
            SELECT        client_id, path + '/' AS filePath, company AS companyName, icon AS clientIcon, active
            FROM          Clients
            WHERE        (Clients.client_id = #clientID#) 
        </cfquery>
        
        <cfset clientPath = "http://www.liveplatform.net/" & clientInfo.filePath>
        
        <cfset clientIconPath = clientPath & 'images/'& clientInfo.clientIcon> 
        
        <cfset iconInfo = {"icon":clientIconPath,"name":clientInfo.companyName}>
       
        <cfreturn iconInfo>
        
 	</cffunction>
 
    
</cfcomponent>












