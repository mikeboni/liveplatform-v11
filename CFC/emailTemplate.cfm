        <!--- Get ClientInfo --->
        <cfset company = cartInfo.company>
        
        <!--- Get EMail Info --->
        <cfset email = cartInfo.email>
        
		<!--- Get User Info --->
        <cfset userName = cartInfo.email.name>
        
		<!--- Get Assets in Groups --->
        <cfset content = cartInfo.assets>
        
        <!--- Module Details --->
        <cfif isDefined("cartInfo.details")>	
			<cfset content = cartInfo.details>
        </cfif>   


 
<cfinvoke component="Assets" method="getAssetTypes" returnvariable="assetTypes" />

      
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><cfoutput>#company.name#</cfoutput></title>
<style type="text/css">

.imgLockedAspect {
	max-width: 100%;
    height: auto;
}

.imgLockedAspectHeight {
	max-height: 100%;
    width: auto;
}

.bannerHeading {
	padding-top:20px;
	padding-bottom:5px;
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 36px;
	color: #333;
}
.bundleid {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 18px;
	color: #333;
	text-decoration:none;
}
.category {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 16px;
	color: #CCC;
	text-decoration:none;
	background-color: #666;
	padding-bottom: 6px;
	padding-left: 12px;
	height: 26px;
}

.plainLinkGrey {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #999;
	text-decoration:none;
}
.content {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #333;
	text-decoration:none;
}
.note {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	color: #999;
	text-decoration:none;
}
div.centre
{
  width: 760px;
  display: block;
  margin-left: auto;
  margin-right: auto;
}

.contentLink {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 14px;
	color: #06C;
	text-decoration:none;
	cursor:pointer;
}
</style>
</head>

<body>   

<cfoutput>       
  <table width="805" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                        <td colspan="2"><spacer width="20" height="20" /></td>
                      </tr>
                      <tr>
                        <td width="120" rowspan="3" align="left" valign="top"><img src="#assetPaths.application.icon#" alt="assetPaths.client.name" width="100" height="100" border="0" style="padding-right:20px" /></td>
                        <td width="685" align="left" valign="bottom" class="mainHeading">
                        <table width="100%" border="0">
                      <tr>
                        <td align="right" valign="top"><table border="0" cellpadding="0" cellspacing="4">
                        <cfif isDefined('type')>
                         <cfif webLink.type GT 0>
                          <tr>
                            <td width="32">
                            <cfset assetIcon = assetTypes['#webLink.type#'].icon>
                            <a href="#webLink.url#" target="_new"><img src="http://liveplatform.net/images/icons/#assetIcon#" width="32" height="32" border="0" /></a>
                            </td>
                            <td><a href="#webLink.url#" target="_new" class="contentLink" style="padding-left:5px">#webLink.title#</a></td>
                          </tr>
                        </cfif>
                        </cfif>
                        <cfif locLink.type GT 0>
                          <tr>
                            <td>
                            <cfset assetIcon = assetTypes['#locLink.type#'].icon>
                            <a href="#locLink.url#" target="_new"><img src="http://liveplatform.net/images/icons/#assetIcon#" width="32" height="32" border="0" /></a>
                            </td>
                            <td><a href="#locLink.url#" target="_new" class="contentLink" style="padding-left:5px">#locLink.title#</a></td>
                          </tr>
                        </cfif>
                        </table></td>
                      </tr>
                      <tr>
                        <td>
                        <span class="bannerHeading">
                        <cfif IsDefined("cartInfo.details.title")>
                        #cartInfo.details.title#
                        </cfif>
                        </span>
                        </td>
                      </tr>
                    </table>
                        
                        </td>
                      </tr>
                      <tr>
                      <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top" class="bundleid">
                        <cfif IsDefined("cartInfo.details.subtitle")>
                        #cartInfo.details.subtitle# 
                        </cfif>                   
                      </tr>
                      <cfif cartInfo.email.message NEQ ''>
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td align="left" valign="middle" class="bundleid"><hr width="100%">                      
                      </tr>
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td align="left" valign="top" class="bundleid">
 						<cfset message = replace(cartInfo.email.message,chr(10),"<br />","all")>
                         #message#              
    					</tr>
                      </cfif>   
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td align="left" valign="top">
                        
                        <cfif config.message NEQ ''>
                        	<p class="bundleid">#config.message#</p>
                        </cfif>                     
                        <cfif IsDefined("cartInfo.details.description")>
                        	<p class="content">#cartInfo.details.description#</p>
                        </cfif>
                        
                      </tr>
                      <tr>
                      <td>&nbsp;</td><td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td align="left" valign="top">
                          
                          
                          
                          
                          <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <!--- FEATURED ITEMS --->
                            <cfif structKeyExists(cartInfo.assets,"features")>
                            	<cfset features = cartInfo.assets.features>
                                  
                            	<cfif NOT ArrayIsEmpty(features)>
                                
                                <cfloop index="z" from="1" to="#arrayLen(features)#">

                                    <!--- Balconies --->
                                    <cfif structKeyExists(features[z],"balconies")>
                                    <tr>
                                    <td height="32" align="left" valign="bottom" class="bundleid">
                                      Select a Balcony to view.<br /><hr size="1" width="100%" />
                                    </td>
                                  </tr>
										<cfset balconies = features[z].balconies> 
                                        <cfloop array="#balconies#" index="bal">
                                        <tr>
                                        <td>
                                          <a href="http://www.liveplatform.net/displayContent.cfm?link=#bal.URLLink#" target="_new">
                                          <img src="#bal.thumbnail#" width="800" border="1" class="imgLockedAspect" border="0" />
                                          </a>
                                          <br /><span class="content">#bal.name#</span><p />
                                        </td>
                                        </tr>
                                        </cfloop> 
                                    
									</cfif>
                                  
                                  </cfloop>  
                                    
                                </cfif>
                            </cfif>
                      </table>
            
                      <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <!--- FEATURED ITEMS --->
                            <cfif structKeyExists(cartInfo.assets,"features")>
                            	<cfset features = cartInfo.assets.features>
                                  
                            	<cfif NOT ArrayIsEmpty(features)>
                                
                                <cfloop index="z" from="1" to="#arrayLen(features)#">
                             
                                        <cfset cols = 3>
                                        <cfset cnt = 0>
                                        
                                        <tr>
                                           <td width="100%" align="left" valign="middle" colspan="2">
                                           
                                            <cfset cols = 3>
											<cfset cnt = 0>
                                            <cfset items = 1>
                                            
                                            <cfloop collection="#features[z]#" item="aFeature">#aFeature#</cfloop>
                                            <cfset theFeature = features[z][aFeature]>
                                            
                                            <table width="800" border="0" cellspacing="0">
                                                <tr>
                                                <cfloop index="z" from="1" to="#arrayLen(theFeature)#">
                                                
                                                    <cfif cnt GTE cols>
                                                    
                                                        </tr><tr>
                                                        <cfset cnt = 0>
                                                    
                                                    </cfif>
                                                    
                                                        <td width="33%">
                                                        <cfif items LTE arrayLen(theFeature)>
                                                        
                                                            <cfset docs = theFeature[z]>
                                                            <cfif structKeyExists(docs,'thumbnail')>
                                                            	<cfset assetIcon = docs.thumbnail>
                                                                <cfset theIcon = "#assetIcon#">
                                                                <cfset theImg = '<img src="#theIcon#" height="91" border="0" />'>
                                                            <cfelse>
                                                				<cfset assetIcon = assetTypes['#docs.type#'].icon>
                                                                <cfset theIcon = "http://www.liveplatform.net/images/icons/#assetIcon#">
                                                                <cfset theImg = '<img src="#theIcon#" width="32" height="32" border="0" />'>
                                                            </cfif>
                                                            
                                                            <table width="250" border="0">
                                                               <tr>
                                                                <td width="32">
                                                                    <a href="http://www.liveplatform.net/displayContent.cfm?link=#docs.urlLink#" target="_new">
                                                                    #theImg#
                                                                    </a>
                                                                </td>
                                                                <cfif NOT structKeyExists(docs,'thumbnail')>
                                                                <td>
                                                                <a href="http://www.liveplatform.net/displayContent.cfm?link=#docs.urlLink#" target="_new" class="contentLink" style="padding-left:5px">#docs.name#</a>
                                                                </td>
                                                                </cfif>
                                                              </tr>
                                                          </table>
                                                          
                                                        <cfelse>
                                                            &nbsp;
                                                        </cfif>
                                                        </td>
                           
                                                    <cfset cnt++>
                                                    <cfset items++>
                                                </cfloop>
                                                </tr>
                                            </table>
  
                                            </td>
                                        </tr>
                                  
                                  </cfloop>  
                                    
                                </cfif>
                            </cfif>
                      </table>

                      
                      </td>
                      </tr>
                      
                      <cfif structKeyExists(cartInfo.assets,"categories")>
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td height="32" align="left" valign="bottom" class="bundleid">
                          Select an image below to view in a larger size.
                        </td>
                      </tr>
                      </cfif>
                      
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td><hr size="1" /></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top">&nbsp;</td>
                        <td>
                        
                                                <!--- CATEGORIES --->
                      
						<!--- <cfif structKeyExists(cartInfo.assets,"categories")> --->
                        
						<cfloop collection="#cartInfo.assets#" item="aCategory">
                        
						<table width="100%" border="0" cellpadding="0" cellspacing="0">

                        	<cfset content = cartInfo.assets[aCategory]>
                          
							<cfif NOT ArrayIsEmpty(content)>
                            
                                
                             
                                <!--- <cfloop collection="#category#" item="item"> --->
                                
                                	<!--- <cfset categoryName = item> --->
                                    <cfset cols = 4>
                                    
                                  <tr>
                                      <td height="32" colspan="#cols#" class="category" style="padding-top:10px;">#aCategory#</td>
                                  </tr>
                                  <tr>
                                      <td height="4"><span></span></td>
                                  </tr>
                                  <cfset cnt = 0>
                                  <tr>
                                      <td>
                                   <cfloop array="#content#" index="theAsset"> 

                                    <cfset cnt++>
										<cfif cnt GT cols>
                                        <!--- ASSETS --->
                                          <a href="http://liveplatform.net/displayContent.cfm?link=#theAsset.urlLink#" target="_new" style="padding-right:2px"><img src="#theAsset.thumbnail#" alt="" height="91" border="1" class="imgLockedAspectHeight" /></a>
                                        	</td></tr>
                                            <tr><td height="4"><hr width="0" size="0" noshade="noshade" /></td></tr>
                                            <tr><td>
                                            
											<cfset cnt = 0>
                                        <cfelse>
                                            
                                            <!--- ASSETS --->
                                          <a href="http://liveplatform.net/displayContent.cfm?link=#theAsset.urlLink#" target="_new" style="padding-right:2px"><img src="#theAsset.thumbnail#" alt="" height="91" border="1" class="imgLockedAspectHeight" /></a>
                                            
                                        </cfif>  
   
                            </cfloop>
                            </td></tr>
     	
                        </cfif>
                        </table>
                        </cfloop>
                      <!---  </cfif> --->

                        </td>

                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td><p class="note">#config.disclaimer#</p></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
</cfoutput>

</body>
</html>