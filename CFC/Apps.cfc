<cfcomponent>
   
<cffunction name="getAppClientIDs" access="public" returntype="struct">
	<cfargument name="bundleID" type="string" required="yes" default="0">
   
    <cfquery name="apps">
		SELECT       Applications.app_id AS appID, Clients.client_id AS clientID
		FROM         Applications FULL OUTER JOIN
					 Clients ON Applications.client_id = Clients.client_id
		WHERE        (Applications.bundle_id = N'#bundleID#')
    </cfquery>
   
   <cfset ids = {}>
   
   <cfif apps.recordCount GT 0>
   		<cfset ids = {"appID":apps.appID,"clientID":apps.clientID}>
   </cfif>
   
   <cfreturn ids>
   
 </cffunction>
    
   
<!---Update Application Prefs--->
<cffunction name="getAppProductCode" access="public" returntype="string">

	<cfargument name="appID" type="numeric" required="yes" default="0">

	<cfquery name="product">
		SELECT       Products.abbr AS prodID
		FROM         Applications INNER JOIN
					 Products ON Applications.product_id = Products.product_id
		WHERE        (Applications.app_id = #appID#)
	</cfquery>
	
	<cfif product.recordCount GT 0>
		<cfreturn product.prodID>
	<cfelse>
		<cfreturn ''>
	</cfif>

</cffunction>
    
     
       
<!---Update Application Prefs--->
	<cffunction name="getSupportLinks" access="public" returntype="query">
    	
        <cfargument name="appID" type="numeric" required="yes" default="0">
        <cfargument name="type" type="numeric" required="no" default="0">
        
        <cfargument name="linkID" type="numeric" required="no" default="0">
        
        <cfquery name="supportLinks">
        	SELECT Support.support_id, Support.url, Support.urlName, Support.app_id, Support.type, 
                   Support.access_id, AccessLevels.accessLevel
			FROM            Support INNER JOIN
                         AccessLevels ON Support.access_id = AccessLevels.access_id
            WHERE	app_id = #appID# AND type = #type#
            <cfif linkID GT '0'>
            	AND support_id = #linkID#
            </cfif>
        </cfquery>
        
        <cfreturn supportLinks>
        
    </cffunction>
    
    
    
    
    <!--- getAppProducts --->
    <cffunction name="getAppProducts" access="public" returntype="query">
        
        <cfquery name="products">
        	SELECT name, abbr, product_id
			FROM   Products
        </cfquery>
        
        <cfreturn products>
        
    </cffunction>
    
    
    
    

<!---Update Application Prefs--->
	<cffunction name="updateAppPrefs" access="public" returntype="boolean">
    	
        <cfargument name="prefsID" type="numeric" required="yes" default="0">
        
		<cfargument name="serverID" type="numeric" required="no" default="0">
        <cfargument name="sessionT" type="numeric" required="no" default="0">
        <cfargument name="connectionT" type="numeric" required="no" default="0">
        <cfargument name="deviceT" type="numeric" required="no" default="0">
        <cfargument name="heartbeatT" type="numeric" required="no" default="0">
        <cfargument name="tokenExpiryT" type="numeric" required="no" default="0">
        <cfargument name="maxTokens" type="numeric" required="no" default="0">
        <cfargument name="tokenExpires" type="numeric" required="no" default="30">
        <cfargument name="serverAPI" type="numeric" required="no" default="0">
        <cfargument name="refresh" type="numeric" required="no" default="900000">
        
        <cfargument name="signinExpires" type="numeric" required="no" default="15">
        <cfargument name="guestSignin" type="numeric" required="no" default="0">
        <cfargument name="guestRegister" type="numeric" required="no" default="0">
        <cfargument name="codeSignin" type="numeric" required="no" default="0">
        <cfargument name="notificationTime" type="numeric" required="no" default="30">
        
        <cfargument name="authentication" type="struct" required="no" default="{}">
        
        <cfset refreshTime = refresh * 60>
     
        <cfquery name="appPrefs">
        	SELECT session_id, server_id
            FROM	Prefs
            WHERE	prefs_id = #prefsID#
        </cfquery>
   
        <cfquery name="UpdateSession">
                UPDATE Sessions
                SET session_timeout = #sessionT#, connection_timeout = #connectionT#, device_timeout = #deviceT#, heartbeat_timeout = #heartbeatT#, token_expiry = #tokenExpiryT#, max_tokens = #maxTokens#, token_expires = #tokenExpires#, server_api = #serverAPI#, session_refresh = #refreshTime#, signin_expires = #signinExpires#, guest_signin = #guestSignin#, guest_register = #guestRegister#, code_signin = #codeSignin#, notificationTime = #notificationTime#, 
                <cfif NOT structIsEmpty(authentication)>  
					authentication = 1,
					client_id_request = '#authentication.client.request#', 
					client_id_refresh = '#authentication.client.refresh#', 
					client_id_validate = '#authentication.client.validate#', 
					clientSecret = '#authentication.clientSecret#', 
					validator_id = '#authentication.validator_id#', 
					grantType_validate = '#authentication.grantType.validate#', 
					grantType_refresh = '#authentication.grantType.refresh#', 
					grantType_request = '#authentication.grantType.request#',
					authUrl = '#authentication.url#'
				<cfelse>
              		authentication = 0,
              		client_id_request = NULL, 
              		client_id_refresh = NULL, 
              		client_id_validate = NULL, 
					clientSecret = NULL, 
					validator_id = NULL, 
					grantType_validate = NULL, 
              		grantType_refresh = NULL, 
              		grantType_request = NULL,
              		authUrl = NULL
               </cfif>
                WHERE session_id = #appPrefs.session_id#
        </cfquery>
        
        <cfquery name="UpdateServer">
                UPDATE Prefs
                SET server_id = #serverID#
                WHERE	prefs_id = #prefsID#
        </cfquery>
		
        <cfreturn true>
        
    </cffunction>
    
    
    
    
  <!---Generate Application Prefs--->
	<cffunction name="getVersionUpdate" access="public" returntype="struct">  
    	
        <cfargument name="bundleID" type="string" required="no" default="">
        <cfargument name="appID" type="string" required="no" default="0">
        <cfargument name="version" type="numeric" required="yes" default="0">
        <cfargument name="os" type="string" required="no" default="" hint="specifying the os will provide only the appLinks for the specific os">
        
          
            <cfset data = structNew()>
  
			<!---AppStore Link Types Filter--->            
            <cfinvoke  component="Misc" method="getAppStore" returnvariable="osType">
                    <cfinvokeargument name="os" value="#os#"/>
            </cfinvoke>
          
            <cfquery name="appVersion">
            	SELECT  app_id,forced_update, version, active
                FROM	Applications
                WHERE	
                		<cfif appID IS 0>
                		bundle_id = '#bundleID#'
                        <cfelse>
                        app_id = #appID#
                        </cfif>
            </cfquery>
   
            <cfif appVersion.version GT version>
			
            <cfif appVersion.forced_update>
            <!---Forced Updated--->
                <cfinvoke  component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1007"/>
                </cfinvoke>
           <cfelse>
           <!---Optional Update--->    
                <cfinvoke  component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1006"/>
                </cfinvoke>
            </cfif>
         
                <cfquery name="appSupport">
                    SELECT 		url, urlName, type
                    FROM		Support
                    WHERE		app_id = #appVersion.app_id# AND type = 1
                </cfquery>
            
				<!---Get Support Links--->  
                <cfset appLinks = structNew()>
                
                <!--- OS Types --->
    			<cfset osTypes = {"AppStore":"appStore","GooglePlay":"googlePlay","PC":"pc","MAC":"mac","Registration":"registration","Other":"other"}>
                
               <cfoutput query="appSupport">
               
               		<cfset urlNameCase = osTypes[urlName]>
                    
                    <cfif urlName IS osType>
                        <cfset structAppend(appLinks,{"#urlNameCase#":"#url#"})>
                    <cfelseif osType IS ''>
                    	<cfset structAppend(appLinks,{"#urlNameCase#":"#url#"})>
                    </cfif>   
                    
                </cfoutput>       
                       
                <cfset structAppend(data,{"appLinks":appLinks})>     

            <cfelse>
            
            	<cfinvoke  component="Errors" method="getError" returnvariable="error"></cfinvoke>
                
            </cfif>
            
            <cfset structAppend(data,{"version":#appVersion.version#})>
            
            <cfif appVersion.recordCount GT '0'>
            
				<!---Check if App is Active--->
                <cfif appVersion.active>
                        <!---nothing--->
                <cfelse>
                
					<!---Not Active App--->
                    <cfinvoke  component="Errors" method="getError" returnvariable="error">
                        <cfinvokeargument name="error_code" value="1001"/>
                    </cfinvoke>
                    
                </cfif>  
                
			<cfelse>
            
				<!---App Not Found--->
                <cfinvoke  component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1002"/>
                </cfinvoke>    

            </cfif>
         
            <cfset structAppend(data,{"error":#error#})>
    
    	<cfreturn data>
    
    </cffunction>
    
    
    
    
    
  <!---Generate Application Prefs--->
	<cffunction name="getJSONPath" access="remote" returntype="string">  
    
    	<cfargument name="appID" type="numeric" required="no" default="0" hint="App">
        <cfargument name="addPath" type="string" required="no" default="">
        
    	<cfinvoke component="Apps" method="getAppName" returnvariable="appName">
            <cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
       
        <cfif appID IS 0><cfreturn ""></cfif>

        <!--- Build JSON Client/App Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="path">
            <cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
 
        <!--- Server API Version --->
        <cfinvoke  component="Apps" method="getAppPrefs" returnvariable="prefs">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
       
       <cfif addPath IS ''>
        	<cfset baseURL = ExpandPath("../../")>
        <cfelse>
        	<cfset baseURL = ExpandPath(addPath)>
		</cfif>
        <!--- <cfset baseURL = ExpandPath("../../../")> --->
        <!--- <cfset baseURL = ExpandPath(".")> --->

        <cfset apiVr = prefs.server_API>
        
        <cfif apiVr GTE 10>
        	<cfset jsonPath = baseURL &"/"& path & "JSON_" & apiVr &"/">
        <cfelse>
        	<cfset jsonPath = baseURL &"/"& path & "JSON/">
        </cfif>

        <cfif NOT directoryExists(jsonPath)>
            <cfdirectory action="create" directory="#jsonPath#" mode="777">
        </cfif>

        <cfreturn jsonPath>
    
	</cffunction>
    
    
    
    
    
    
 	<!---Generate Application Prefs--->
	<cffunction name="generateAppPrefs" access="public" returntype="struct">
    
    	<cfargument name="bundleID" type="string" required="no" default="">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="os" type="string" required="no" default="" hint="specifying the os will provide only the appLinks for the specific os">
        <cfargument name="serverFURL" type="string" required="no" default="" hint="Forwarding Server Link">
    	<cfargument name="override" type="boolean" required="no" default="false" hint="OVERRIDE ACTIVE STATE">
       
        <cfset data = structNew()>
	
        <!--- BundleID is Valid? --->
        <cfinvoke  component="Apps" method="bundleIDValid" returnvariable="appValid">
            <cfinvokeargument name="bundleID" value="#bundleID#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
  
        <cfif (bundleID IS '' AND appID IS 0) OR NOT appValid>
        
        	<!--- App Not Valid --->
			<cfif NOT appValid>
                <cfinvoke  component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1002"/>
                </cfinvoke>
            
            <cfelse>
        
				<!--- Missing Paramiters --->
                <cfinvoke  component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1005"/>
                </cfinvoke>
            
            </cfif>
        
        <cfelse>

			<!--- Get Data --->
            <cfquery name="appPrefs">
                SELECT        Applications.app_id, Applications.version, Applications.active, Sessions.session_timeout, Sessions.session_refresh, Sessions.connection_timeout, 
                                Applications.support, Applications.clientEmail, Sessions.device_timeout, Sessions.heartbeat_timeout, Sessions.token_expiry, Sessions.max_tokens, 
                                Sessions.token_expires, Servers.server_url AS serverURL, Sessions.server_api AS api, Applications.appName, Sessions.signin_expires, 
                                Sessions.guest_signin, Sessions.guest_register, Sessions.code_signin, Products.name AS product, Products.abbr AS abbr, Sessions.notificationTime, Sessions.authentication
        		FROM            Prefs INNER JOIN
                                Applications ON Prefs.prefs_id = Applications.prefs_id LEFT OUTER JOIN
                                Products ON Applications.product_id = Products.product_id LEFT OUTER JOIN
                                Sessions ON Prefs.session_id = Sessions.session_id LEFT OUTER JOIN
                                Servers ON Prefs.server_id = Servers.server_id
                                
                WHERE          <cfif appID IS 0>
                					(Applications.bundle_id = '#bundleID#') 
                               <cfelse>
                			   		(Applications.app_id = '#appID#')
                               </cfif>
            </cfquery>
        	
            <cfset appID = appPrefs.app_id>
           
           <!--- Get CLient ID from AppID --->
                <cfinvoke  component="Clients" method="getClientIDFromAppID" returnvariable="clientID">
                    <cfinvokeargument name="appID" value="#appPrefs.app_id#"/>
                </cfinvoke>
           
            <!--- Get Client Info --->
            <cfif appPrefs.support IS ''>
                
                <!--- Get Client Info --->
                <cfinvoke  component="Clients" method="getClientInfo" returnvariable="clientInfo">
                    <cfinvokeargument name="clientID" value="#clientID#"/>
                </cfinvoke>
            	
                <cfset supportEmail = clientInfo.support>
            
            <cfelse>
            
            	<cfset supportEmail = appPrefs.support>
                   
            </cfif>

            <cfoutput query="appPrefs">
            
				<!--- Get Options --->
                <cfinvoke component="Options" method="getAppOptions" returnvariable="allOptions">
                    <cfinvokeargument name="appID" value="#appPrefs.app_id#"/>
                </cfinvoke>
                
                <cfset appOptions = structNew()>
                
                <cfif allOptions.facebook>
                    <cfset structAppend(appOptions,{"facebook":allOptions.facebook_accessLevel})>
                </cfif>
                
                <cfif allOptions.instagram>
                	<cfset structAppend(appOptions,{"instagram":allOptions.instagram_accessLevel})>
                </cfif>
                
                <cfif allOptions.pintrest>
                	<cfset structAppend(appOptions,{"pintrest":allOptions.pintrest_accessLevel})>
                </cfif>
                
                <cfif allOptions.twitter>
                	<cfset structAppend(appOptions,{"twitter":allOptions.twitter_accessLevel})>
                </cfif>
                
                <cfif allOptions.sendEmail GT 0>
                	<cfset structAppend(appOptions,{"sendEmail":allOptions.email_accessLevel})>
                </cfif>
                
                <cfif allOptions.addressBook GT 0>
                	<cfset structAppend(appOptions,{"addressBook":allOptions.addressBook_accessLevel})>
                </cfif>
                
                <cfif allOptions.vuforiaLicense NEQ ''>
                    <cfset structAppend(appOptions,{"vuforia": '#allOptions.vuforiaLicense#'})>
                </cfif> 
               
                <!--- icons --->
                <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
                    <cfinvokeargument name="clientID" value="#clientID#"/>
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="server" value="true"/>
                </cfinvoke>
                
                <cfset appIcon = assetPaths.application.icon>
                <cfset companyIcon = assetPaths.client.icon>
                
                <cfif notificationTime IS ''>
					<cfset notifyTime = 0>
                <cfelse>
                	<cfset notifyTime = int(notificationTime * 60)>
                </cfif>
                
                <cfset data = {
					"session": 
						{
						"session_timeout":session_timeout, 
						"content_refresh":session_refresh, 
						"connection_timeout":connection_timeout, 
						"device_timeout":device_timeout, 
						"heartbeat_timeout":heartbeat_timeout, 
						"token_expiry":token_expiry, 
						"token_expires":token_expires, 
						"max_tokens":max_tokens,
						"notification":notifyTime
						}, 
					"application": 
						{
						"support":supportEmail, 
						"version":version, "api":api, 
						"appName":appName, 
						"email":clientEmail,
						"appIcon":appIcon,
						"companyIcon":companyIcon,
						"product":product,
						"productCode":abbr,
						"customerRegistration":allOptions.customerRegistration
						}, 
					"signin": #allOptions.registration#
					}>
                
                <!--- Server URL --->
                <cfif serverFURL NEQ ''>
                    <cfset structAppend(data.application,{"server": serverFURL})>
                <cfelse>
                    <cfset structAppend(data.application,{"server": serverURL})>
                </cfif>
                
                <cfif NOT structIsEmpty(appOptions)>
                    <cfset structAppend(data, {"options":appOptions})>
                </cfif>
            
            </cfoutput>
            
            <cfset structAppend(data,{"access":{'expires':appPrefs.signin_expires}})>
            
            <cfif appPrefs.guest_signin>
            	<cfset structAppend(data.access,{'allowGuest':appPrefs.guest_signin})>
            </cfif>
           
            <cfif appPrefs.guest_register>
            
            	<!--- <cfset structAppend(data.access,{'allowRegistration':appPrefs.guest_register})> --->
                
                <!--- Check if Registration Link is available --->
                <cfinvoke  component="Apps" method="getVersionUpdate" returnvariable="theLinks">
                    <cfinvokeargument name="appID" value="#appID#"/>
                </cfinvoke>
				
                <cfset structAppend(data,theLinks)>
                
                <!--- <cfif structKeyExists(theLinks.appLinks,'registration')>
                    <cfset structAppend(data,{"registration":"#theLinks.appLinks.registration#"})>
                </cfif> --->
     
            </cfif>
            
            <cfif appPrefs.code_signin>
            	<cfset structAppend(data.access,{'allowCode':appPrefs.code_signin})>
            </cfif>
            

 			<!--- If App Exists --->
			
            <cfif appPrefs.recordCount GT '0'>
            
                <cfquery name="appSupport">
                    SELECT Support.support_id, Support.url, Support.urlName, Support.app_id, Support.type, 
                           Support.access_id, AccessLevels.accessLevel
                    FROM            Support INNER JOIN
                                 AccessLevels ON Support.access_id = AccessLevels.access_id
                    WHERE	app_id = #appPrefs.app_id# AND Support.type = 0
                </cfquery>
                
                <!---AppStore Link Types Filter--->            
                <cfinvoke  component="Misc" method="getAppStore" returnvariable="osType">
                        <cfinvokeargument name="os" value="#os#"/>
                </cfinvoke>
                          
                <cfset structAppend(data,{"supportLinks":{}})>
                
                <cfoutput query="appSupport">
                    	<cfset structAppend(data.supportLinks,{'#urlName#':{"url":url, "access":accessLevel}})>
                </cfoutput>
	
                <cfif appPrefs.active OR override>
                    <cfinvoke  component="Errors" method="getError" returnvariable="error"></cfinvoke>
                <cfelse>
                    <cfset data = structNew()>
                    <cfinvoke  component="Errors" method="getError" returnvariable="error">
                        <cfinvokeargument name="error_code" value="1001"/>
                    </cfinvoke>
                </cfif>
			
            <cfelse>
            	<!--- No Application Found --->
            	<cfinvoke  component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1002"/>
                </cfinvoke>
            </cfif>
            
        </cfif>

    <cfset structAppend(data,{"error":#error#})>

    <cfreturn data>
    
    </cffunction>
    
    
    

    
 
    
	<!---Get Application Prefs--->
	<cffunction name="getAppPrefs" access="public" returntype="query">
    	<cfargument name="bundleID" type="string" required="no" default="">
		<cfargument name="appID" type="numeric" required="no" default="0">
		
        <cfif bundleID NEQ ''>
       		
       		<!--- Get AppID --->
			<cfinvoke component="Apps" method="getAppID" returnvariable="app">
				<cfinvokeargument name="bundleID" value="#bundleID#">
			</cfinvoke>
       		<cfset appID = app.app_id>
       		
	 	</cfif>
       
        <cfquery name="appPrefs">
        	SELECT       Applications.prefs_id, Servers.server_id, Sessions.session_timeout, Sessions.connection_timeout, Sessions.session_refresh, 
                         Sessions.device_timeout, Sessions.heartbeat_timeout, Sessions.token_expiry, Sessions.max_tokens, Sessions.token_expires, server_api,
                         Sessions.signin_expires, Sessions.guest_signin, Sessions.guest_register, Sessions.code_signin, Sessions.notificationTime, Sessions.authentication, 
                         Sessions.clientSecret, 
                         Sessions.validator_id, 
                         Sessions.grantType_validate, Sessions.grantType_refresh, Sessions.grantType_request, 
                         Sessions.client_id_validate, Sessions.client_id_request, Sessions.client_id_refresh,
                         Sessions.authUrl
            FROM         Applications INNER JOIN
                         Prefs ON Applications.prefs_id = Prefs.prefs_id AND Applications.prefs_id = Prefs.prefs_id INNER JOIN
                         Servers ON Prefs.server_id = Servers.server_id AND Prefs.server_id = Servers.server_id INNER JOIN
                         Sessions ON Prefs.session_id = Sessions.session_id AND Prefs.session_id = Sessions.session_id
            WHERE        (Applications.app_id = #appID#)
        </cfquery>
        
        <cfset time15min = 60*15>
         
        <cfif appPrefs.recordCount IS 0>
        
        	<!---Create New Prefs--->
            <cfquery name="defaultServer">
                 SELECT server_id AS serverID
                 FROM Servers
                 WHERE serverName = 'None'
            </cfquery>
            
            <!---Add Session--->
            <cfquery name="newSession">
                 INSERT INTO Sessions (session_timeout, connection_timeout, device_timeout, heartbeat_timeout, token_expiry, max_tokens, token_expires, server_api, Sessions.session_refresh, Sessions.signin_expires, Sessions.guest_signin, Sessions.guest_register, Sessions.code_signin, authentication)
                 VALUES (300,45,240,20,0,0,30,8,#time15min#, 15, 0, 0, 0, 0) 
                 SELECT @@IDENTITY AS sessionID
            </cfquery>
            
            <!---Add Prefs--->
            <cfquery name="newPrefs">
                 INSERT INTO Prefs (session_id, server_id)
                 VALUES (#newSession.sessionID#, #defaultServer.serverID#) 
                 SELECT @@IDENTITY AS prefsID
            </cfquery>
            
            <!---Update Applcation--->
            <cfquery name="UpdateApplication">
                UPDATE Applications
                SET prefs_id = #newPrefs.prefsID#
                WHERE app_id = #appID#
        	</cfquery>
            
            <cfinvoke  component="Apps" method="getAppPrefs" returnvariable="appPrefs">
              <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
            
        </cfif>

        <cfreturn appPrefs>
        
    </cffunction>
    
    
	<!---Get Client Application--->
	<cffunction name="getClientApps" access="public" returntype="query">
    
		<cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        
        <cfargument name="active" type="boolean" required="no" default="false">
		<cfargument name="released" type="boolean" required="no" default="false">
        <cfargument name="download" type="boolean" required="no" default="false">
        
        <cfquery name="clientsApps">
            SELECT        <cfif clientID IS '0'>
            				Clients.client_id, Clients.company, Clients.path, Clients.active, Clients.icon,
            			  </cfif>
            				Applications.app_id, Applications.appName, Applications.path AS appPath, 
                                     Applications.prefs_id, Applications.bundle_id, Applications.forced_update, Applications.version, Applications.active AS appActive, Applications.download AS download, 
                                     Applications.icon AS appIcon, Applications.created, Applications.modified
            FROM            Clients INNER JOIN
                                     Applications ON Clients.client_id = Applications.client_id
            WHERE        0=0
            <cfif appID GT '0'>
            			AND (Applications.app_id = #appID#)
            </cfif>
            <cfif clientID GT '0'>
            			 AND (Clients.client_id = #clientID#)
			</cfif>
            <cfif active>
            			AND Applications.active = 1
            </cfif>
            <cfif released>
            			AND released = 1
            </cfif>
            <cfif download>
            			AND Applications.download = 1
            </cfif>
            ORDER BY    Applications.modified ASC
        </cfquery>
        
        <cfreturn clientsApps>
        
	</cffunction>
    
    
    
    

	<!---Servers--->
	<cffunction name="getServers" access="public" returntype="query">
        
        <cfquery name="allServers">
            SELECT server_id, serverName, server_url
            FROM Servers
        </cfquery>
        
        <cfreturn allServers>
        
    </cffunction>
    
    
    
    
    <!---Update Servers--->
	<cffunction name="updateServer" access="public" returntype="boolean">
        
        
    </cffunction>
    
    
    
    
    <!---New Servers--->
	<cffunction name="addServer" access="public" returntype="boolean">
        
        
    </cffunction>
    
    
    
    
    <!---Application ID from BundleID--->
	<cffunction name="getAppName" access="public" returntype="string">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
        
        <cfquery name="app">
            SELECT appName
            FROM Applications
            WHERE	app_id = #appID#
        </cfquery>
        
        <cfreturn app.appName>
        
    </cffunction>   
    
    
    
    
    <!---BundleID Valid--->
	<cffunction name="bundleIDValid" access="public" returntype="boolean">
    	<cfargument name="bundleID" type="string" required="no" default="">
        <cfargument name="appID" type="numeric" required="no" default="0">
        
        <cfquery name="app">
            SELECT app_id
            FROM   Applications
            WHERE  
            <cfif appID GT 0>
            	app_id = #appID#
            <cfelse>
            	bundle_id = '#bundleID#'
            </cfif>
            
        </cfquery>
        
        <cfif app.recordCount GT '0'>
        	<cfreturn true>
        <cfelse>
        	<cfreturn false>
        </cfif>
        
    </cffunction> 
     
    
    
    

	<!---Application ID from BundleID--->
	<cffunction name="getAppID" access="public" returntype="struct">
    	<cfargument name="bundleID" type="string" required="no" default="">
        <cfargument name="override" type="boolean" required="no" default="false">
        
        <cfset data = structNew()>
     
		<cfquery name="app">
            SELECT app_id, active
            FROM Applications
            WHERE	bundle_id = '#bundleID#'
        </cfquery>
 
 		<cfif app.recordCount GT '0'>
    
			<cfif app.active IS 1 OR override>
            
                <!--- ok Error --->
                <cfinvoke component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1000">
                </cfinvoke>
                
                <cfset structAppend(data,{"app_id":#app.app_id#})>
                
            <cfelse>
                
                <!--- Not Active Error --->
                <cfinvoke component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1001">
                </cfinvoke>
                
            </cfif>
        
        <cfelse>
        	<!--- No App Exists --->
            <cfinvoke component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1002">
            </cfinvoke>
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})>
        
        <cfreturn data>
        
	</cffunction>


	<!---Application Exists--->
	<cffunction name="apptExists" access="public" returntype="boolean">
    	<cfargument name="bundleID" type="string" required="no" default="">
        <cfargument name="appID" type="numeric" required="yes" default="0">
		<cfargument name="path" type="string" required="no" default="">
        
        <cfset clientID = '0'>
        
        <cfquery name="currentApp">
        	SELECT app_id, appName
            FROM Applications
            WHERE 0=0
            <cfif appID GT '0'> 
            AND app_id = #appID#
            </cfif>
            <cfif bundleID NEQ ''>
            AND bundle_id = '#bundleID#'
            </cfif>
            <cfif path NEQ ''>
            AND path = '#path#'
            </cfif>
        </cfquery>

        <cfif currentApp.recordCount GT '0'>
        	<cfset appID = currentApp.app_id>
        </cfif>

		<cfreturn appID>
        
	</cffunction>
 
 
 
 <!--- Update Application --->   
   <cffunction name="updateApp" access="public" returntype="numeric">
    	
        <cfargument name="appID" type="numeric" required="yes" default="0">
        
		<cfargument name="appName" type="string" required="yes" default="">
        
        <cfargument name="image" type="string" required="no" default="">
        <cfargument name="bundleID" type="string" required="no" default="">
		<cfargument name="active" type="boolean" required="no" default="false">
        
        <cfargument name="forced" type="boolean" required="no" default="false">
		<cfargument name="version" type="numeric" required="no" default="">
        <cfargument name="support" type="string" required="no" default="">
        <cfargument name="clientEmail" type="string" required="no" default="">
        <cfargument name="download" type="boolean" required="no" default="false">
		<cfargument name="productID" type="numeric" required="no" default="0">
        
        <!---Active--->
        <cfif active><cfset active = '1'><cfelse><cfset active = '0'></cfif>
        
        <!---Download--->
        <cfif download><cfset download = '1'><cfelse><cfset download = '0'></cfif>
        
        <!---Force Update--->
        <cfif forced><cfset forced = '1'><cfelse><cfset forced = '0'></cfif>
        
        <!--- App and Images Folder Exists --->
        <cfinvoke  component="File" method="folderExists" returnvariable="folderCreated">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="images" value="true"/>
            <cfinvokeargument name="createFolder" value="true"/>
        </cfinvoke>
        
        <!--- App Folder--->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="path">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="images" value="true"/>
        </cfinvoke>
        
        <cfif image NEQ ''>
        
			<!---get old image--->
            <cfinvoke  component="Apps" method="getClientApps" returnvariable="appInfo">
              <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
			
            <!---Delete Old Image--->
            <cfset oldImage = path & appInfo.appIcon>
        
			<cfif fileExists(oldImage)>
                <cffile action="delete" file="#oldImage#">
            </cfif> 
            
        </cfif>
        
        <cfquery name="UpdateClient">
        	UPDATE Applications
            SET active = #active#
            <cfif appName NEQ ''>
            ,appName ='#appName#'
            </cfif>
            <cfif bundleID NEQ ''>
            ,bundle_id ='#bundleID#'
            </cfif>
            ,forced_update ='#forced#'
            ,support = '#support#'
            ,download = '#download#'
            ,product_id = '#productID#'
            , clientEmail = '#clientEmail#'
            <cfif version NEQ ''>
            ,version = #version#
            </cfif>
            <cfif image NEQ ''>
            ,icon = '#image#'
            </cfif>
            WHERE app_id = #appID#
        </cfquery>
		
        
        <cfif image NEQ ''>
        
        	<!---Move to Client/App Folder--->
            <cfset srcFile = GetTempDirectory() & image>
			<!--- <cfset srcFile = GetTempDirectory() & image> --->
            
            <cfset destFilePath = path & image>
            
            <!--- Move Asset --->
            <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
              <cfinvokeargument name="destPathFile" value="#destFilePath#"/>
              <cfinvokeargument name="srcPath" value="#srcFile#"/>
            </cfinvoke>
            
            <!---Delete Temp Image--->
            <cfinvoke component="file" method="deleteTempFile" returnvariable="deleted">
                <cfinvokeargument name="file" value="#image#"/>
            </cfinvoke> 
            
        </cfif>
        
		<cfreturn appID>
    
	</cffunction>  
    
    
	<!---Create New Application--->
	<cffunction name="createNewApp" access="public" returntype="numeric" output="yes">
    	
        <cfargument name="clientID" type="numeric" required="yes" default="0">
        
		<cfargument name="appName" type="string" required="yes" default="">
        <cfargument name="path" type="string" required="yes" default="">
        
        <cfargument name="image" type="string" required="no" default="">
        <cfargument name="bundleID" type="string" required="no" default="">
		<cfargument name="active" type="boolean" required="no" default="false">
        
        <cfargument name="forced" type="boolean" required="no" default="false">
		<cfargument name="version" type="string" required="no" default="100000">
        <cfargument name="support" type="string" required="no" default="">
        <cfargument name="clientEmail" type="string" required="no" default="">
        <cfargument name="productID" type="numeric" required="no" default="0">
        
        <cfset appID = '0'>

        <!---Current Date--->
        <cfinvoke component="misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <!---Active--->
        <cfif active><cfset active = '1'><cfelse><cfset active = '0'></cfif>
        
        <!---Force Update--->
        <cfif forced><cfset forced = '1'><cfelse><cfset forced = '0'></cfif>
		
        <!---Check if App Exists--->
        <cfinvoke component="Apps" method="apptExists" returnvariable="appID">
            <cfinvokeargument name="bundleID" value="#trim(bundleID)#"/>
            <cfinvokeargument name="path" value="#trim(path)#"/>
		</cfinvoke>
        
        <cfif appID IS '0'>
     		<!---App NOT Exists--->
            
			<!---Create a new App entry--->
            <cfquery name="newApp">
                 INSERT INTO Applications (appName, path, active, bundle_id, forced_update, version, client_id, created, modified, support, clientEmail, download, product_id <cfif image NEQ ''>, icon</cfif>)
                 VALUES ('#trim(appName)#','#trim(path)#',#active#, '#bundleID#', #forced#, '#version#', #clientID#, #curDate#, #curDate#, '#support#', '#clientEmail#', 0, #productID# <cfif image NEQ ''>, '#image#'</cfif>) 
                 SELECT @@IDENTITY AS appID
            </cfquery>
            
            <cfset appID = newApp.appID>
            
            <!---Create a new Root Group--->
            <cfquery name="newApp">
                 INSERT INTO Groups (app_id, subgroup_id, created, modified, active)
                 VALUES (#appID#, -1 , #curDate#, #curDate#,1) 
            </cfquery>
            
		</cfif>
		
        <!--- App and Images Folder Exists --->
        <cfinvoke  component="File" method="folderExists" returnvariable="folderCreated">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="images" value="true"/>
            <cfinvokeargument name="createFolder" value="true"/>
        </cfinvoke>
        
		<!--- App Folder--->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="path">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="images" value="true"/>
        </cfinvoke>
        
        <cfif image NEQ ''>
        
			<!---Move to Client/App Folder--->
            <cfset srcFIle = GetTempDirectory() & image>
            <cffile action="move" source="#srcFIle#" destination="#path##image#" nameconflict="makeunique">
            
            <!---Delete Temp Image--->
            <cfinvoke component="file" method="deleteTempFile" returnvariable="deleted">
                <cfinvokeargument name="file" value="#image#"/>
            </cfinvoke> 
        
        </cfif>
        
        <cfreturn appID>
        
     </cffunction>   
     
     
     
     
     <!---Generate JSON Content--->
	<!---generateContentJSON&appID=4&groupID=0--->

    <cffunction name="generateContentJSON" access="remote" output="no" returntype="boolean">
        
        <cfargument name="groupID" type="numeric" required="yes" default="0">
		<cfargument name="appID" type="numeric" required="yes" default="0">

        <!--- Get App Name --->
        <cfinvoke component="Apps" method="getAppName" returnvariable="appName">
            <cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
        
        <!--- Server API Version --->
        <cfinvoke  component="Apps" method="getAppPrefs" returnvariable="prefs">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
   	
        <cfset apiVr = prefs.server_API>
		
        <cfif groupID IS -1>
        	<cfset fullStructure = true>
            <cfset JSONFile = appName & '.json'>
        <cfelse>
        	<cfset fullStructure = false>
            <cfset JSONFile = appName & '_' & groupID & '.json'>
        </cfif>

        <!--- Build JSON Client/App Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="path">
            <cfinvokeargument name="appID" value="#appID#">
            <cfinvokeargument name="createFolder" value="true">
        </cfinvoke>
       
        <!--- App and Images Folder Exists --->
        <cfinvoke  component="File" method="folderExists" returnvariable="folderCreated">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="createFolder" value="true"/>
        </cfinvoke>
       	
        <cfif apiVr GTE 10>
        	<cfset jsonPath = path & "JSON_" & apiVr &"/">
        <cfelse>
        	<cfset jsonPath = path & "JSON/">
        </cfif>
          
        <cfif NOT directoryExists(jsonPath)>
			<!--- Create Directory --->
            <cfdirectory action="create" directory="#jsonPath#">
        </cfif>
        
        <cfset JSONFilePath = jsonPath & JSONFile>

        <cfif fileExists(JSONFIlePath)>
        
			<!--- Delete Old JSON File --->
            <cffile action="delete" file="#JSONFIlePath#">
        
        </cfif>
         
        <!--- Get JSON Path --->
        <cfinvoke component="CFC.Apps" method="getJSONPath" returnvariable="JSONPath">
        	<cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>


		<!--- Get PROD Type --->
		<cfinvoke  component="CFC.Apps" method="getAppProductCode" returnvariable="prodID">
			<cfinvokeargument name="appID" value="#appID#"/>
		</cfinvoke>
		
		<cfif prodID IS 'ON4'>
			<!--- Delete Content Asset File --->
			<cfset JSONFileContentPath = jsonPath & 'contentAssets.json'>
			<cfif fileExists(JSONFileContentPath)>
				<cffile action="delete" file="#JSONFileContentPath#">
			</cfif>
		</cfif>
       
        <cfset uniqueThread = CreateUUID()>
       
        <cfif folderCreated>
        	<!--- 																																											THREAD --->
           <cfthread action="run" name="saveJSON_#uniqueThread#" groupID="#groupID#" appID="#appID#" JSONFIlePath="#JSONFIlePath#">
       
                <!--- Get Content Group --->
                <cfinvoke  component="Modules" method="getGroupContent" returnvariable="data">
                    <cfinvokeargument name="groupID" value="#groupID#"/>
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <!--- <cfinvokeargument name="fullStructure" value="#fullStructure#"/> --->
                </cfinvoke>
                
                 <!--- Get Epoch --->
        		<cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
                <cfset structAppend(data,{"modified":curDate})>
          
                <!---OK--->
                <cfinvoke component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1000">
                </cfinvoke>
            
                <cfset structAppend(data,{"error":#error#})> 
    
                <cfset JSONdata = serializeJSON(data)>
                <cfset filePath = expandPath('../../'& JSONFIlePath)>
				
                <cfif fileExists(filePath)>
                	<cffile action = "delete" file = "#filePath#">
				</cfif>
           
                <cffile action="write" file="#filePath#" output="#JSONdata#" charset="utf-8"> 

			</cfthread>
			
			<cfif prodID IS 'ON4'>
				<cfthread action="run" name="saveCONT_#uniqueThread#" appID="#appID#" JSONFileContentPath="#JSONFileContentPath#">
					<!--- Get Data of Assets Lib --->
					<cfinvoke component="Content" method="generateProjectAssets" returnvariable="data">
						<cfinvokeargument name="appID" value="#appID#">
						<cfinvokeargument name="filePath" value="#JSONFileContentPath#">
					</cfinvoke>
				</cfthread>
			</cfif>
       
        </cfif>
       
        <cfreturn true>
        
     </cffunction>  
     
     
     
     <cffunction name="generateAllContentJSON" access="remote" output="yes" returntype="boolean">
		<cfargument name="appID" type="numeric" required="yes" default="0">
        <cfargument name="devEnviroment" type="numeric" required="no" default="0">
      
     	<!--- Get JSON Path --->
        <cfinvoke  component="Content" method="getJSONPath" returnvariable="dir">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
        
        <!--- Delete All JSON --->
        <!--- <cfinvoke  component="Misc" method="deleteAllFilesInDirectory">
            <cfinvokeargument name="directory" value="#dir#"/>
            <cfinvokeargument name="forceDelete" value="true"/>
        </cfinvoke> --->
																																								     
     	<!--- Get All Groups --->
     	<cfinvoke component="Content" method="getGroups" returnvariable="groupIDS">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>																					 																																								  
        <cfset ArrayPrepend(groupIDS,0)>
		
        <!--- Get App Name --->
        <cfinvoke component="Apps" method="getAppName" returnvariable="appName">
            <cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
   		
        <!--- Server API Version --->
        <cfinvoke  component="Apps" method="getAppPrefs" returnvariable="prefs">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
    	
        <cfset apiVr = prefs.server_API>
          
        <!--- Build JSON CLient/App Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="path">
            <cfinvokeargument name="appID" value="#appID#">
            <cfinvokeargument name="createFolder" value="true">
        </cfinvoke>
  	
        <!--- JSON Path --->
        <cfif apiVr GTE 10>
        	<cfset jsonPath = path & "JSON_" & apiVr &"/">
        <cfelse>
        	<cfset jsonPath = path & "JSON/">
        </cfif>
        
        <cfif devEnviroment IS 1>
        	<cfset jsonPath = jsonPath & "DEV/">
        </cfif>
       
        <cfset jsonPath = expandPath('./../../../'&jsonPath)>
      
        <cfif NOT directoryExists(jsonPath)>
			<!--- Create Directory --->
            <cfdirectory action="create" directory="#jsonPath#" mode="777">
        </cfif>
            
        <!--- App and Images Folder Exists --->
        <cfinvoke  component="File" method="folderExists" returnvariable="folderCreated">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="createFolder" value="true"/>
        </cfinvoke>

        <cfif folderCreated>
        
        <!--- Refreah Special Content --->
        
        <!--- Get All Special Content Groups --->
         <cfset specialContentGroups = ['Background', 'Breadcrumb', 'Theme', 'ScreenSaver']>
         
        <!--- Loop through All Special Content Groups --->
        <cfloop index="groupName" array="#specialContentGroups#">

			<cfinvoke  component="Content" method="generateSpecialContent" returnvariable="data">
				<cfinvokeargument name="appID" value="#appID#"/>
				<cfinvokeargument name="groupName" value="#groupName#"/>
				<!--- <cfinvokeargument name="array" value="#arrayType#"/> --->
				<cfinvokeargument name="dev" value="#devEnviroment#"/>
			</cfinvoke>
           
		</cfloop>

        <cfset uniqueThread = CreateUUID()>
        
        <!--- Group Specific --->
         <!--- 	<cfthread action="run" name="saveJSON_#uniqueThread#" appID="#appID#" jsonPath="#jsonPath#" groupIDS="#groupIDS#">  
  
            <cfloop index="groupID" array="#groupIDS#">
            	
                <cfset JSONFile = appName & '_' & groupID & '.json'> --->
        		
                <!--- get filters from applications table --->
  <!---                <cfquery name="filter">
                      SELECT filterGroups
                      FROM Applications
                      WHERE	app_id = #appID#
                  </cfquery>
                  
                  <cfset filters = filter.filterGroups> --->
             
                <!--- Get Content Group --->
                <!--- filter (comma delimited list) --->
                <!--- <cfinvoke  component="Modules" method="getGroupContent" returnvariable="data">
                    <cfinvokeargument name="groupID" value="#groupID#"/>
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="filter" value="#filters#"/>
                </cfinvoke> --->
     
                <!--- Get Epoch --->
<!---         	<cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
                <cfset structAppend(data,{"modified":curDate})> --->
                
                <!--- ok Error --->
                <!--- <cfinvoke  component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1000"/>
                </cfinvoke>  
                
                <cfset structAppend(data,{"error":#error#})>
           
                <cfset JSONdata = serializeJSON(data)>
         
     			<cfset filePath = jsonPath & JSONFile>
                <cffile action="write" file="#filePath#" output="#JSONdata#" charset="utf-8">  --->
			
           <!---  </cfloop> 
         
 		     </cfthread>  ---> 
 		          	    
			<!--- Everthing --->
            <cfset uniqueThread = CreateUUID()>
            
                <cfthread action="run" name="saveJSON_#uniqueThread#" appID="#appID#" jsonPath="#jsonPath#">
         
				<cfset JSONFilePath = jsonPath & appName & '.json'>
      		
                <!--- get filters from applications table --->
                 <cfquery name="filter">
                      SELECT filterGroups
                      FROM Applications
                      WHERE	app_id = #appID#
                  </cfquery>
                  
                  <cfset filters = filter.filterGroups>
          
                <!--- Get Content Group --->
                <cfinvoke  component="Modules" method="getGroupContent" returnvariable="data">
                    <cfinvokeargument name="groupID" value="-1"/>
                    <cfinvokeargument name="appID" value="#appID#"/>
                    <cfinvokeargument name="fullStructure" value="yes"/>
                    <cfinvokeargument name="filter" value="#filters#"/><!--- filter (comma delimited list) --->
                </cfinvoke>

                <!--- Get Epoch --->
        		<cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
                <cfset structAppend(data,{"modified":curDate})>
                
                <!--- ok Error --->
                <cfinvoke  component="Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1000"/>
                </cfinvoke>  
           		     
                <cfset structAppend(data,{"error":#error#})>
           
                <cfset JSONdata = serializeJSON(data)>
   				<cfset success = false>
   				
					<cftry>
                		<cffile action="write" file="#JSONFIlePath#" output="#JSONdata#" nameconflict="OVERWRITE" charset="utf-8"> 
                		<cfset success = true>
					<cfcatch>
						<cfset success = false>
					</cfcatch>
               
                </cftry>
 
              </cfthread>
        
     	<cfelse>
        
        	<cfreturn false>
        
        </cfif>
        
        <!--- generateMaterialsJSON --->
        <!--- <cfinvoke component="Configurator" method="generateMaterialsJSON" returnvariable="error">
            <cfinvokeargument name="appID" value="#appID#">
        </cfinvoke> --->       

        <cfreturn true>
     
     </cffunction>
     
     
     
     <!--- Gets AppIcon + AppPath, ClientIcon + ClientPath --->

    <cffunction name="getPaths" access="remote" output="yes" returntype="struct">
        
        <cfargument name="appID" type="numeric" required="no" default="0">
		<cfargument name="clientID" type="numeric" required="no" default="0">
        <cfargument name="auth_token" type="string" required="no" default="">
        <cfargument name="omitAppInfo" type="boolean" required="no" default="no">
        <cfargument name="server" type="boolean" required="no" default="no">
        <cfargument name="download" type="boolean" required="no" default="false">
      
        <!--- Current Relative Root Directory Path --->
        <cfinvoke component="File" method="getRootPath" returnvariable="rootpath" />
      
        <!--- If Auth Token Provided - Get User and then ClientID --->
        <cfif auth_token NEQ ''>
       
			<!--- Get Client ID from Token--->
            <cfquery name="clientInfo">
            
                SELECT       Users.client_id
                FROM         Tokens INNER JOIN
                             Users ON Tokens.user_id = Users.user_id
                WHERE        (Tokens.token = '#auth_token#')
            
            </cfquery>     
                   
            <cfif clientInfo.recordCount GT 0>
                <cfset clientID = clientInfo.client_id>
            <cfelse>
                <cfset clientID = 0>
            </cfif>
            
        
        </cfif>
	 	
        <!--- No Client ID --->
        <cfif clientID IS '0'>
        	
            <!--- Get Paths from ClientID --->
            <cfquery name="clientInfo">
                SELECT        client_id, path + '/' AS filePath, company AS companyName, icon AS clientIcon, active
                FROM          Clients
                ORDER BY	companyName
            </cfquery>
            
            <cfset clients = structNew()>
            
            <cfoutput query="clientInfo">

                <cfinvoke  component="Apps" method="getPaths" returnvariable="clientData">
                    <cfinvokeargument name="clientID" value="#client_id#"/>
                    <cfinvokeargument name="omitAppInfo" value="yes"/>
                </cfinvoke>
                
                <cfset structAppend(clients,{"#companyName#":{"icon":clientData.client.icon,"name":clientData.client.name, "id":clientInfo.client_id, "active":clientData.client.active}})>
                
            </cfoutput>

            <cfreturn clients>
            
        <cfelse>
       
        	<!--- Get Paths from ClientID --->
            <cfquery name="clientInfo">
                SELECT        client_id, path + '/' AS filePath, company AS companyName, icon AS clientIcon, active
                FROM          Clients
                WHERE        (Clients.client_id = #clientID#) 
            </cfquery>
           
			<!--- Relative Path --->
            <cfif server>
            	<cfset clientPath = "http://www.liveplatform.net/" & clientInfo.filePath>
            <cfelse>
            	<cfset clientPath = "../../" & clientInfo.filePath>
			</cfif>
            
            <cfset data = structNew()>
            
            <cfset clientIconPath = clientPath & 'images/'& clientInfo.clientIcon> 
            <cfset structAppend(data,{"client":{"icon":clientIconPath,"name":clientInfo.companyName,"active":clientInfo.active}})>
           
            <cfif omitAppInfo>
            	<!--- No App Data --->
            <cfelse>
            
            	<cfquery name="appsInfo">
                    SELECT        Applications.app_id, path AS appPath, Applications.icon AS appIcon, Applications.appName, Applications.active, Applications.support, Applications.clientEmail, Applications.download, Applications.product_id
								  <cfif appID GT '0'>, Applications.bundle_id, version, Applications.forced_update</cfif>, 
                    			  Sessions.server_api AS api
                    FROM          Applications LEFT OUTER JOIN
                        			Prefs ON Applications.prefs_id = Prefs.prefs_id LEFT OUTER JOIN
                        			Sessions ON Prefs.session_id = Sessions.session_id
                    WHERE        <cfif appID GT '0'>
                                  	(app_id = #appID#)
                                  <cfelseif clientID GT '0'>
                    			  	(client_id = #clientID#) 
                                  </cfif>  
                                  <cfif download>
                                  	AND Applications.download = 1
                                  </cfif> 
            	</cfquery>
             
				<cfif appID GT '0'>
                  
                    <!--- AppID provided --->
                    <cfset appPath = clientPath & appsInfo.appPath>
                    <cfset appIconPath = clientPath & appsInfo.appPath & '/images/'& appsInfo.appIcon>        
                    <cfset structAppend(data,{"application":{"api":appsInfo.api, "icon":appIconPath,"name":appsInfo.appName,"download":appsInfo.download,"active":appsInfo.active,"bundleID":appsInfo.bundle_id, "productID":appsInfo.product_id,"path":appPath,"version":appsInfo.version,"forced":appsInfo.forced_update, "support":#appsInfo.support#, "clientEmail":#appsInfo.clientEmail#}})>
                  
                    <!--- Asset Types --->
                    <cfquery name="assetTypes">
                        SELECT        assetType_id,path
                        FROM          AssetTypes
                    </cfquery>
                    
                    <cfset types = structNew()>
                    
                    <cfoutput query="assetTypes">
                        <cfset typePath = clientPath & appsInfo.appPath &'/assets/'& path &'/'>
                        <cfset structAppend(types,{"#assetType_id#":typePath})> 
                    </cfoutput>
                    
                    <cfset structAppend(data,{"types":types})>
                 
                <cfelse>
                	
                    <!--- Multiple Apps if no AppID Provided --->
                    
                    <cfset apps = arrayNew(1)>
                  
                    <cfoutput query="appsInfo">
            
                        <cfset appIconPath = clientPath & appPath & '/images/'& appIcon> 
                        <cfset arrayAppend(apps,{"icon":appIconPath,"name":appName,"active":active,"id":app_id})>
            
                    </cfoutput>
                    
                    <cfset structAppend(data,{"apps":apps})>
                    
                </cfif>
        
        	</cfif>
            
        </cfif>

        <cfreturn data>
        
    </cffunction>
    
    
    
    <!--- Gets Asset Path --->

    <cffunction name="getAssetPath" access="remote" output="yes" returntype="string">
        
        <cfargument name="assetID" type="numeric" required="no" default="0">
        <cfargument name="assetTypeID" type="numeric" required="no" default="0">
        
        <cfquery name="assetPath">
            SELECT	Clients.path + '/' + Applications.path + '/assets/' AS path
            FROM	Assets LEFT OUTER JOIN
                    Applications ON Assets.app_id = Applications.app_id LEFT OUTER JOIN
                    Clients ON Applications.client_id = Clients.client_id
            WHERE 	Assets.asset_id = #assetID#
        </cfquery>
        
		<!--- Asset Types --->
        <cfquery name="assetTypePath">
            SELECT        path
            FROM          AssetTypes
            WHERE 		  assetType_id = #assetTypeID#
        </cfquery>
        
        <cfset theAssetPath = "http://www.liveplatform.net/" & assetPath.path & assetTypePath.path &'/'>
        
        <cfreturn theAssetPath>

    </cffunction>
    
</cfcomponent>


     