<cfcomponent>

<!--- Get AppID, ClientID, UserID from Token--->
<cffunction name="importAccountsXLS" access="remote" output="yes" returntype="string">
    	<cfargument	name="appID" type="numeric" required="no" default="0" />
    	<cfargument	name="importFile" type="string" required="no" default="" />
		
        <cfif importFile IS '' OR appID IS 0>
        	<cfreturn "missing (zip) file that contains data and import instructions">
        </cfif>
        
        <!--- Read and Parse CSV File --->
		<cfset tempDir = GetTempDirectory()>
        
        <cfset error = false>
        
        <cffile action="upload" fileField="importFile" destination="#tempDir#" nameconflict="overwrite" result="newFile">
        
		<cfset importFileXLS = newFile.serverDirectory &'/'& newFile.serverFile>

        <cfif fileExists(importFileXLS)>
        
			<!--- Read spreadsheet --->
            <cfspreadsheet action="read" src="#importFileXLS#" headerrow="1" excludeHeaderRow="true" query="importXLS">
            
            <cfset failed = false>
      		
            <!--- check if all colums exists --->
            <cfif NOT ListFindNoCase( importXLS.ColumnList, "name" )>
                	<cfset queryAddColumn(importXLS, "name", "VarChar", ArrayNew(1)) />
            </cfif>
            
            <cfif NOT ListFindNoCase( importXLS.ColumnList, "email")>
                	<cfset queryAddColumn(importXLS, "email", "VarChar", ArrayNew(1)) />
            </cfif>
            
            <cfif NOT ListFindNoCase( importXLS.ColumnList, "password")>
                	<cfset queryAddColumn(importXLS, "password", "VarChar", ArrayNew(1)) />
            </cfif>
            
            <cfif NOT ListFindNoCase( importXLS.ColumnList, "access" )>
                	<cfset queryAddColumn(importXLS, "access", "Integer", ArrayNew(1)) />
            </cfif>
            
            <!--- get clientID from appID --->
            <cfinvoke component="Clients" method="getClientIDFromAppID" returnvariable="clientID">
              <cfinvokeargument name="appID" value="#appID#"/>  
            </cfinvoke>
  
            <cfif clientID GT 0 AND appID GT 0 AND importXLS.recordCount GT 0>
            
				<!--- create accounts --->
                <cfoutput query="importXLS">
                        
                    <cfif name NEQ '' AND email NEQ '' AND access LT 4>
                    
                        <cfinvoke component="Users" method="registerUser" returnvariable="info">
                              <cfinvokeargument name="name" value="#name#"/>
                              <cfinvokeargument name="email" value="#email#"/>
                              <cfinvokeargument name="password" value="#password#"/>
                              <cfinvokeargument name="accessLevel" value="#access#"/>
                              
                              <cfinvokeargument name="clientID" value="#clientID#"/>
                              <cfinvokeargument name="appID" value="#appID#"/>
                              <cfinvokeargument name="active" value="1"/>      
                        </cfinvoke>
                    	
                        <cfset success = true>
                        
                    </cfif>
                    
                </cfoutput>
            
            <cfelse>
            	<cfset success = false>
            </cfif>
            
       </cfif>         
       
       <cflocation url="../usersView.cfm?assetTypeTab=0">
       <cfreturn success>
        
</cffunction>



<!--- Get AppID, ClientID, UserID from Token--->
<cffunction name="importAccounts" access="public" returntype="struct" output="yes">
	<cfargument	name="csvFile" type="string" required="true" />
	<cfargument	name="appID" type="numeric" required="true" />
    
    <cfargument	name="accessLevel" type="numeric" required="true" />
    <cfargument	name="active" type="boolean" required="true" />

	<!--- Get ClientID from AppID --->
    <cfinvoke component="Clients" method="getClientIDFromAppID" returnvariable="clientID">
      <cfinvokeargument name="appID" value="#appID#"/>
    </cfinvoke>
    
    <!--- Read and Parse CSV File --->
	<cfset tempDir = GetTempDirectory()>
    
    <cffile action="upload" fileField="csvFile" destination="#tempDir#" nameconflict="makeunique" result="newFile">
    
    <cfset csvFileLoc = newFile.serverDirectory &'/'& newFile.serverFile>
    
    <cffile action = "read" file = "#csvFileLoc#" variable = "csvData">
    
    <!--- Convert to Query --->
    <cfinvoke component="CSV" method="CSVToQuery" returnvariable="accounts">
      <cfinvokeargument name="CSV" value="#csvData#"/>
      <cfinvokeargument name="Delimiter" value=","/>
    </cfinvoke>
    
    <!--- Not Valid Accounts --->
    <cfquery name="accountValid" dbtype="query">
    	SELECT email,name, pass
        FROM accounts
        WHERE email <> ''
    </cfquery>
    
    <!--- Valid Accounts --->
    <cfquery name="accountNotValid" dbtype="query">
    	SELECT email,name, pass
        FROM accounts
        WHERE email = ''
    </cfquery>
    
    <cfdump var="#accountNotValid#">
    
    <cfoutput query="accountValid">
	
		<cfif listLen(name, ' ') GT 1>
            <cfset pass = listGetAt(name,2,' ')>
        <cfelse>
        	
            <cfif pass IS ''>
                
                <!--- Make a Password Random --->
                <cfinvoke component="Misc" method="makeID" returnvariable="pass">
                  <cfinvokeargument name="length" value="8"/>
                  <cfinvokeargument name="type" value="0"/>
                </cfinvoke>
            
            </cfif>
            
        </cfif>
        
        <cfset name = REReplace( "#name#" , "\b(\S)(\S*)\b" , "\u\1\L\2" , "all" )>
        
            <!--- Make a Password Random --->
            
            <cfinvoke component="Users" method="registerUser" returnvariable="info">
                  <cfinvokeargument name="name" value="#name#"/>
                  <cfinvokeargument name="email" value="#email#"/>
                  <cfinvokeargument name="password" value="#pass#"/>
                  <cfinvokeargument name="accessLevel" value="#accessLevel#"/>
                  <cfinvokeargument name="clientID" value="#clientID#"/>
                  <cfinvokeargument name="appID" value="#appID#"/>
                  <cfinvokeargument name="active" value="#active#"/>      
            </cfinvoke>
            
            <!--- <cfdump var="#userCreated#"> --->
        
    </cfoutput>

    <cfreturn {}>
        
</cffunction>

    
    

<!--- Get AppID, ClientID, UserID from Token--->
	<cffunction name="getUserProfileIDs" access="public" returntype="struct" output="yes">
      <cfargument name="auth_token" type="string" default="" required="no">
     	
        <cfset user = {"clientID":0, "appID":0, "userID":0}>
        
        <cfquery name="theUserInfo">
            SELECT        Users.client_id, Tokens.user_id, Tokens.app_id
            FROM          Tokens INNER JOIN Users ON Tokens.user_id = Users.user_id
            WHERE        (Tokens.token = '#auth_token#')
    	</cfquery>
        
        <cfif theUserInfo.recordCount GT 0>
        	<cfset user.clientID = theUserInfo.client_id>
            <cfset user.userID = theUserInfo.user_id>
            <cfset user.appID = theUserInfo.app_id>
        </cfif>
      
      <cfreturn user>
      
    </cffunction> 
    
    
    
    <!--- Get User EMail from Token--->
	<cffunction name="getUserEmail" access="public" returntype="string" output="yes">
      
      <cfargument name="auth_token" type="string" default="" required="no">
      <cfargument name="userID" type="string" default="0" required="no">
        
        <cfset email = ''>

        <cfif auth_token NEQ ''>
        
            <cfinvoke component="Users" method="getUserProfileIDs" returnvariable="userInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
        
        	<cfset userID = userInfo.userID>
        
        </cfif>
        
        <cfif userID GT 0>
        
            <cfquery name="theUserInfo">
                SELECT        email
                FROM          Users
                WHERE         user_id = #userID#
            </cfquery>
		
        	<cfset email = theUserInfo.email>
        	
        </cfif>
      
      <cfreturn email>
      
    </cffunction>  



	<!--- Get User Email and Name --->
	<cffunction name="getUserInfo" access="public" returntype="query" output="yes">
      <cfargument name="userID" type="numeric" default="0" required="no">
      <cfargument name="auth_token" type="string" default="" required="no">
      <cfargument name="details" type="boolean" default="no" required="no">
     
     <cfif details>
     	<cfset theUserInfo = queryNew("user_id, name, email, code, password, app_id, phone, company, companyphone, companyfax, company_address, company_province, country", "Integer, VarChar, VarChar, VarChar, VarChar, Integer, VarChar, VarChar, VarChar, VarChar, VarChar, VarChar, VarChar")>
     <cfelse>
     	<cfset theUserInfo = queryNew("user_id, name, email, password, app_id", "Integer, VarChar, VarChar, VarChar, Integer")>
     </cfif>
       
       
     <cfif userID NEQ '0' OR auth_token NEQ ''>
       		
           <cfif auth_token NEQ ''>
			   <!--- Get UserID from Auth Token--->
               <cfinvoke component="Users" method="getUserID" returnvariable="userID">
                    <cfinvokeargument name="auth_token" value="#auth_token#">
               </cfinvoke>
          </cfif>
           
          <cfquery name="theUserInfo">
          		<cfif details>
                SELECT user_id, name, email, code, password, app_id, phone, company, companyphone, companyfax, company_address
                <cfelse>
                SELECT user_id, name, email, code, password, app_id
                </cfif>
                FROM Users
                WHERE user_id = #userID#
          </cfquery>
	
    </cfif>
 
    <cfreturn theUserInfo>

	</cffunction>
	
	
	<!---GetAccessLevel ID--->
	<cffunction name="getAccessLevel" access="public" returntype="query" output="yes">
      
      <cfargument name="accessID" type="numeric" default="0" required="no">
      <cfargument name="accessLevel" type="numeric" default="-1" required="no">
      
      <cfquery name="theLevels">
            SELECT access_ID, accessLevel, levelName
            FROM AccessLevels
            WHERE 0=0
            <cfif accessID GT '0'>
            AND access_id = #accessID#
            </cfif>
            <cfif accessLevel GTE '0'>
            AND accessLevel = #accessLevel#
            </cfif>
            ORDER BY accessLevel
      </cfquery>
	
    <cfreturn theLevels>

	</cffunction>
    
    
    
    
    
    <!--- Get All Active Users for Email Support --->
	<cffunction name="getActiveUsers" access="public" returntype="query" output="yes">
      <cfargument name="clientID" type="numeric" default="0" required="no">
      <cfargument name="appID" type="numeric" default="0" required="no">
      <cfargument name="accessLevel" type="numeric" default="0" required="no">
      
      <cfquery name="activeUsers">
          SELECT  	name, email, active, client_id, app_id
          FROM   	Users
          WHERE		active = 1 AND email <> ''

		  <!--- normal accounts --->
          <cfif clientID GT '0'>
            AND client_id = #clientID# 
          </cfif>
          
          <cfif appID GT '0'>
            AND app_id = #appID#
          </cfif>
          
          <cfif accessLevel GT '0'>
          
          	<!--- Get Access Info --->
          	<cfinvoke component="Users" method="getAccessLevel" returnvariable="accessInfo">
          	    <cfinvokeargument name="accessLevel" value="#accessLevel#"/>
          	</cfinvoke>
          
          	<cfset accessID = accessInfo.accessLevel>
          
            AND access_id = #accessID#
            
          </cfif>
               
      </cfquery>
	
    <cfreturn activeUsers>

	</cffunction>
 
    
   
   
   <!--- Check if User Exists --->
  <cffunction name="userExists" access="remote" returntype="boolean" output="yes">
   	
    <cfargument name="name" type="string" default="" required="no">
    <cfargument name="userID" type="numeric" default="0" required="no">
    <cfargument name="appID" type="numeric" default="0" required="no">
    <cfargument name="clientID" type="numeric" default="0" required="no">
    <cfargument name="code" type="string" default="" required="no">
    <cfargument name="email" type="string" default="0" required="no">
  
    <!--- <cfif (code NEQ '') OR (email NEQ '' AND code NEQ '')>
    	<!--- ok --->
    <cfelse>
    	 <cfreturn false>
    </cfif> --->
   
       <cfquery name="userExists">
            SELECT COUNT(user_id) AS UserExist
            FROM Users
            WHERE 0=0
            <cfif userID GT '0'>
            AND user_id = #userID#
            </cfif>
            <cfif code NEQ ''>
            	AND code = '#code#'
            </cfif>
            <cfif email NEQ ''>
            	AND email = '#email#'
            </cfif>
            <cfif appID GT '0'>
            	AND app_id = #appID#
            </cfif>
            <cfif clientID GT '0'>
            	AND client_id = #clientID#
            </cfif>
            <cfif name NEQ ''>
            	AND name = '#trim(name)#'
            </cfif>

        </cfquery>
  
        <cfif userExists.UserExist GT '0'>
            <cfreturn true>
        <cfelse>
            <cfreturn false>
        </cfif>
    
    </cffunction>
    
    
    
      <!--- Get UserID if User Exists --->
  <cffunction name="getUserIDIfExists" access="remote" returntype="numeric" output="no">
   	
    <cfargument name="appID" type="numeric" required="yes">
    <cfargument name="clientID" type="numeric" required="yes">
    <cfargument name="email" type="string" required="yes">
		
        <cfset userID = 0>
   
       <cfquery name="userExists">
            SELECT user_id
            FROM Users
            WHERE email = '#trim(email)#' AND app_id = #appID# AND client_id = #clientID#
        </cfquery>
  
        <cfif userExists.recordCount GT 0>
            <cfset userID = userExists.user_id>
        </cfif>
        
        <cfreturn userID>
    
    </cffunction>
    
    
    

   <!--- Get User Device from Token --->
  <cffunction name="getDeviceFromToken" access="remote" returntype="numeric" output="no">
   	
    <cfargument name="auth_token" type="string" default="0" required="yes">
    
       <cfquery name="userDevice">
            SELECT        device_id
            FROM          Tokens
            WHERE        (token = '#auth_token#')
        </cfquery>
		
        <cfif userDevice.recordCount GT '0'>
        	<cfset deviceID = userDevice.device_id>
		<cfelse>
        	<cfset deviceID = 0>
        </cfif>
         
    	<cfreturn deviceID>
    
    </cffunction>





	<!--- Check User Info --->
    <cffunction name="userEmailExists" access="remote" returntype="boolean" output="no">
          
          <cfargument name="email" type="string" required="yes" default="">
          <cfargument name="bundleID" type="string" required="yes" default="">
     
            
          <!--- Get AppID --->
            <cfinvoke component="Apps" method="getAppID" returnvariable="app">
                <cfinvokeargument name="bundleID" value="#bundleID#">
            </cfinvoke>
			
            <cfset appID = app.app_id>

          <!--- Get User --->
          <cfquery name="theUser">
                SELECT	email
                FROM 	Users
                WHERE	email = '#email#' AND app_id = #appID# 
          </cfquery>
         
          <cfif theUser.recordCount GT 0>

			  <cfreturn true>
		  
          <cfelse>
          
          	  <cfreturn false>
          
          </cfif>
         
	</cffunction>




	<!--- Check User Info --->
    <cffunction name="validateUserInfo" access="remote" returntype="struct" output="yes">
          
          <cfargument name="code" type="string" required="no" default="">
          <cfargument name="pass" type="string" required="no" default="">
          <cfargument name="email" type="string" required="no" default="">
          <cfargument name="bundleID" type="string" required="no" default="">
    		
          <cfset appID = 0>  
            
          <cfif bundleID NEQ ''>  
            
          <!--- Get AppID --->
            <cfinvoke component="Apps" method="getAppID" returnvariable="app">
                <cfinvokeargument name="bundleID" value="#bundleID#">
            </cfinvoke>
			
            <cfif app.error.error_code IS 1000>
            	<cfset appID = app.app_id>
            <cfelse>
            	<cfreturn app>
            </cfif>
            
          </cfif>
          
          <cfquery name="theUser">
                SELECT	name, password, email, dev
                FROM 	Users
                WHERE	0=0
                <cfif code NEQ ''>
                	AND code = '#code#'
                <cfelse>
                	AND email = '#email#' AND password COLLATE Latin1_General_CS_AS = '#pass#'
                </cfif>
                AND app_id = #appID# 
          </cfquery>

          <cfset data = structNew()>
         
          <!--- ok Error --->
          <cfinvoke component="Errors" method="getError" returnvariable="errorOK">
              <cfinvokeargument name="error_code" value="1000">
          </cfinvoke>
         
          <cfif theUser.recordCount GT '0'>

          <cfset errors = arrayNew(1)>

              <cfif theUser.email IS ''>
              
                  <!--- Missing Email --->
                  <cfinvoke component="Errors" method="getError" returnvariable="error">
                      <cfinvokeargument name="error_code" value="1012">
                  </cfinvoke>
                  <cfset structAppend(data,{"email":{"error":#error#}})>
              <cfelse>
              	  <cfset structAppend(data,{"email":{"data":#theUser.email#, "error":#errorOK#}})>
              </cfif>
                  
              <cfif theUser.password IS ''>
              
                  <!--- Missing Password --->
                  <cfinvoke component="Errors" method="getError" returnvariable="error">
                      <cfinvokeargument name="error_code" value="1012">
                  </cfinvoke>
                  
                  <cfset structAppend(data,{"password":{"error":#error#}})>
              <cfelse>
              	  <cfset structAppend(data,{"password":{"data":#theUser.password#, "error":#errorOK#}})>
              </cfif>
              
              <cfif theUser.name IS ''>
              
                  <!--- Missing Name --->
                  <cfinvoke component="Errors" method="getError" returnvariable="error">
                      <cfinvokeargument name="error_code" value="1012">
                  </cfinvoke>
              		
				  <cfset structAppend(data,{"name":{"error":#error#}})>
              <cfelse>
              	  <cfset structAppend(data,{"name":{"data":#theUser.name#, "error":#errorOK#}})>
              </cfif>
              
              <!--- user is dev --->
              <cfif theUser.dev NEQ ''>
				  <cfif theUser.dev IS 1>
                    <cfset structAppend(data,{"isDev":theUser.dev})>
                  </cfif>
              </cfif>
              
          <cfelse>
          
              <!--- No User Match --->
              <cfinvoke component="Errors" method="getError" returnvariable="error">
                  <cfinvokeargument name="error_code" value="1011">
              </cfinvoke>
          
              <cfset structAppend(data,{"error":#error#})>
          		<cfreturn data>
                
          </cfif>
        
          <cfset structAppend(data,{"error":#errorOK#})>
        
          <cfreturn data>
          
    </cffunction>
    
    
    
    
    <!--- Set User Info --->
    <cffunction name="setUserInfo" access="remote" returntype="boolean" output="yes">
          
          <cfargument name="code" type="string" required="no" default="">
          
          <cfargument name="name" type="string" required="no" default="">
          <cfargument name="email" type="string" required="no" default="">
          <cfargument name="pass" type="string" required="no" default="">
    	  
          <cfargument name="auth_token" type="string" required="yes" default="">
          
          <cfargument name="reset" type="boolean" required="no" default="no">
          
		  <!--- Get User ID with token --->
          <cfinvoke component="Users" method="getUserID" returnvariable="userID">
              <cfinvokeargument name="auth_token" value="#auth_token#"/>
          </cfinvoke>

          <cfif reset>
          
          		<!--- Reset User --->
                <cfquery name="result">
                      UPDATE	Users
                      SET 		name = NULL, email = NULL
                      WHERE		user_id = #userID#
                  </cfquery>
                  
          <cfelse>
          	
              <cfquery name="userName">
                  SELECT name
                  FROM Users
                  WHERE		user_id = #userID#
              </cfquery>
                
              <cfif userName.name IS ''>
			  	<cfset name = "Unknown">
              <cfelse>
			  	<cfset name = userName.name>
              </cfif>  
			  
              <cfquery name="result">
                  UPDATE	Users
                  SET 		name = '#trim(name)#'
                  WHERE		user_id = #userID#
              </cfquery>
              
              <cfif email NEQ ''>
                  <cfquery name="result">
                      UPDATE	Users
                      SET 		email = '#trim(email)#'
                      WHERE		user_id = #userID#
                  </cfquery>
              <cfelse>
              	  <cfreturn false>
              </cfif>
              
              <cfif pass NEQ ''>
                  <cfquery name="result">
                      UPDATE	Users
                      SET 		password = '#trim(pass)#'
                      WHERE		user_id = #userID#
                  </cfquery>
              <cfelse>
              	 <cfreturn false>
              </cfif>
          
          </cfif>
          
          <cfreturn true>
          
     </cffunction>
     
     
     <!--- Upadte User Info --->																				
	<cffunction name="updateUserInfo" access="public" returntype="boolean" output="no">
      <cfargument name="userInfo" type="struct" required="yes">
      <cfargument name="userID" type="numeric" required="no" default="0">
      <cfargument name="auth_token" type="string" required="no" default="">
      <cfargument name="userType" type="numeric" required="no" default="0">
      
      <cfif auth_token NEQ ''>
      
		  <!--- Get User ID with token --->
          <cfinvoke component="Users" method="getUserID" returnvariable="userID">
              <cfinvokeargument name="auth_token" value="#auth_token#"/>
          </cfinvoke>
 
      </cfif>

      <cfif userID GT 0>
 
         <cfquery name="updateInfo">
            UPDATE		Users
            SET 		
                        <cfif isDefined('userInfo.password')>
							<cfif userInfo.password NEQ ''>
                            password = '#trim(userInfo.password)#',
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.phone')>
							<cfif userInfo.phone NEQ ''>
                            phone = '#trim(userInfo.phone)#',
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.company')>
							<cfif userInfo.company NEQ ''>
                            company = '#trim(userInfo.company)#',
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.companyPhone')>
							<cfif userInfo.companyPhone NEQ ''>
                            companyphone = '#trim(userInfo.companyPhone)#',
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.companyFax')>
							<cfif userInfo.companyFax NEQ ''>
                            companyfax = '#trim(userInfo.companyFax)#',
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.company_address')>
							<cfif userInfo.company_address NEQ ''>
                            company_address = '#trim(userInfo.company_address)#',
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.code')>
							<cfif userInfo.code NEQ ''>
                            code = '#trim(userInfo.code)#',
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.country')>
							<cfif userInfo.country NEQ ''>
                            	country = '#trim(userInfo.country)#',
                            <cfelse>
                            	country = 'United States',
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.state')>
							<cfif userInfo.state NEQ ''>
                            	company_province = '#trim(userInfo.state)#',
                            <cfelse>
                            	company_province = 'NA',
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.name')>
							<cfif userInfo.name NEQ ''>
                            name = '#trim(userInfo.name)#', 
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.company_province')>
							<cfif userInfo.company_province NEQ ''>
                            company_province = '#trim(userInfo.company_province)#', 
                            </cfif>
                        </cfif>
                        
                        <cfif isDefined('userInfo.email')>
							<cfif userInfo.email NEQ ''>
                            email = '#trim(userInfo.email)#',
                            </cfif>
                        </cfif>
                            userType = #userType#
                       
            WHERE		user_id = #userID#
         </cfquery>
 	  	
        <cfreturn true>
        
      <cfelse>
      
      	<cfreturn false>
        
      </cfif>

	</cffunction>
     
     
     
     <!--- Get User ID --->
    <cffunction name="getUserID" access="remote" returntype="boolean" output="yes">
          
          <cfargument name="auth_token" type="string" required="no" default="">
			
          <cfif auth_token NEQ ''>
            
              <cfquery name="result">
                  SELECT	User_id AS userID
                  FROM 		Tokens
                  WHERE		token = '#trim(auth_token)#'
              </cfquery>
             
              <cfif result.recordCount IS '0'><cfreturn 0></cfif>
              <cfreturn result.userID>
          
          <cfelse>
          		<cfreturn 0>
          </cfif>
          
          
          
     </cffunction>
    
    
    
    
   <!--- Get Valid Users --->
  <cffunction name="getAllUsers" access="public" returntype="query" output="no">
   	<cfargument name="clientID" type="numeric" default="0" required="no">
    <cfargument name="appID" type="numeric" default="0" required="yes">
    <cfargument name="active" type="boolean" default="false" required="no">
    <cfargument name="activeUser" type="numeric" default="0" required="no">
    <cfargument name="userType" type="numeric" default="0" required="no">
    <cfargument name="search" type="string" default="" required="no">
    <cfargument name="accessed" type="boolean" default="false" required="no">
    <cfargument name="accessLevel" type="numeric" default="4" required="no">
    <cfargument name="searchByLast" type="string" default="" required="no">
     
       <cfquery name="users">
            SELECT user_id, name, email, modified, access_id, active, code, company, phone, companyPhone, companyFax, company_address, company_province, password, dev
            FROM Users
            
            WHERE 0=0
            AND name <> 'guest'
            <!--- AND access_id > 0 --->
            <cfif clientID GT '0'>
            	AND client_id = #clientID#
            </cfif>
            
            <cfif appID GT '0'>
            	AND app_id = #appID#
            </cfif>
            
            <cfif active>
            	AND active = #active#
            </cfif>
            
            <cfif search NEQ ''>
            	AND (name LIKE '%#search#%') OR (email LIKE '%#search#%')
            </cfif>
            
            <cfif activeUser IS 1>
            	AND (name <> '') AND (email <> '')
            <cfelseif activeUser IS -1>
            	AND (name = '') AND (email = '')
            </cfif>
            
            <cfif accessed>
            	AND modified IS NOT NULL
            </cfif>
            	<!--- user type --->
            	AND userType = #userType#
                
            <cfif accessLevel GT -1>
            	AND access_id = #accessLevel#
            </cfif>
               
            <cfif searchByLast NEQ ''>
                AND SUBSTRING(Name,CHARINDEX(' ',Name)+1,LEN(Name)) LIKE '#searchByLast#%'
            </cfif>
            
            	ORDER BY access_id, LTrim(Reverse(Left(Reverse(name), CharIndex(' ', Reverse(name))))) Asc, name Asc
                
        </cfquery>
     		
     	<cfreturn users>
    
    </cffunction>
    
    
    
    
    <!--- Get Last User Accessed Date --->
    <cffunction name="getUserLastAccesssedDate" access="public" returntype="query" output="no">
   	<cfargument name="userID" type="numeric" required="yes">
    
       <cfquery name="users">
            SELECT modified
            FROM Users
            WHERE user_id = #userID#
        </cfquery>
     		
     	<cfreturn accessDate>
    
    </cffunction>
    
    
    
    
   <!--- Validate User --->
    <cffunction name="validateUserID" access="public" returntype="boolean" output="no">
   	<cfargument name="userID" type="numeric" required="yes">
    
       <cfquery name="users">
            SELECT modified
            FROM Users
            WHERE user_id = #userID#
        </cfquery>
     		
        <cfif users.recordCount GT 0>
     		<cfreturn true>
    	<cfelse>
        	<cfreturn false>
        </cfif>
        
    </cffunction>
    
    
    <!--- setUserDevState --->
    
    <cffunction name="setUserDevState" access="remote" returntype="boolean" output="no">
   	<cfargument name="userID" type="numeric" required="yes">
    <cfargument name="devState" type="numeric" required="no" default="0">
    
        <cfquery name="userDev">
            UPDATE Users
            SET dev = #devState#
            WHERE user_id = #userID#
        </cfquery>
        
        <cfreturn true>
        
    </cffunction>
    
 
 
 
  	<!--- Delete User --->
	<cffunction name="deleteUser" access="remote" returntype="boolean" output="no">
    
      <cfargument name="userID" type="numeric" required="yes">
      
  			<!--- Delete User --->
          <cfquery name="theUserInfo">
                DELETE FROM Users
                WHERE user_id = #userID# 
          </cfquery>
	
    <cfreturn true>

	</cffunction>
 
    
    
    
   <!--- Get User Total Sessions --->
  <cffunction name="getUsersTotalSessions" access="public" returntype="numeric" output="yes">
   	
    <cfargument name="userID" type="numeric" default="0" required="yes">
    <cfargument name="appID" type="numeric" default="0" required="yes">
    
       <cfquery name="users">
            SELECT COUNT(trackSession_id) AS userSessions
            FROM SessionTracking
            WHERE 0=0
            AND user_id = #userID#
        </cfquery>
        
     	<!--- AppID Needed ?--->

     	<cfreturn users.userSessions>
    
    </cffunction>
    
    
    
    
  <!--- Get Guest Users --->
  <cffunction name="getGuestUsers" access="public" returntype="numeric" output="no">
    <cfargument name="appID" type="numeric" default="0" required="yes">
    
       <cfquery name="users">
            SELECT COUNT(user_id) AS guestUsers
            FROM Users
            WHERE 0=0
            AND access_id = 0
            <cfif appID GT '0'>
            AND app_id = #appID#
            </cfif>
        </cfquery>
     
        <cfreturn users.guestUsers>
    
    </cffunction>
 
 
   <!--- Get Guest Users Location and Device Info --->
  <cffunction name="getGuestGeneralInfo" access="public" returntype="query" output="no">
  	<cfargument name="clientID" type="numeric" default="0" required="no">
    <cfargument name="appID" type="numeric" default="0" required="no">
    
       <cfquery name="guestInfo">
            SELECT DISTINCT Users.user_id, Users.created, Devices.name, Locations.country_name, Locations.region_name, Locations.city, Devices.os, Users.client_id, Users.app_id
            FROM            Locations RIGHT OUTER JOIN
                            Users INNER JOIN
                            Tracking ON Users.user_id = Tracking.user_id LEFT OUTER JOIN
                            Devices ON Tracking.device_id = Devices.device_id ON Locations.location_id = Tracking.location_id
            WHERE        (Users.access_id = 0) 
            <cfif appID GT '0'>
            AND (Users.app_id = #appID#)
            </cfif>
            <cfif clientID GT '0'> 
            AND (Users.client_id = #clientID#)
            </cfif>
            
            ORDER BY Locations.country_name, Locations.region_name, Locations.city, Devices.os
            <cfif appID GT '0'>
            ,Users.client_id
            </cfif>
            <cfif clientID GT '0'>
            ,Users.app_id
            </cfif>

        </cfquery>
     
        <cfreturn guestInfo>
    
    </cffunction>
 
 
 
 
    <!--- Get Guest User Content Accessed Info --->
  <cffunction name="getUserContentAccessed" access="public" returntype="struct" output="yes">
  <cfargument name="userID" type="numeric" default="0" required="yes">
  	<cfargument name="clientID" type="numeric" default="0" required="no">
    <cfargument name="appID" type="numeric" default="0" required="no">
    
       <cfquery name="guestInfo">
       		SELECT        Tracking.user_id, Assets.name, Tracking.app_id, Tracking.client_id, Tracking.length, Groups.subgroup_id, 
                         Assets.assetType_id, Tracking.created, Thumbnails.image
            FROM            Groups LEFT OUTER JOIN
                                     Assets INNER JOIN
                                     Thumbnails ON Assets.thumb_id = Thumbnails.thumb_id ON Groups.asset_id = Assets.asset_id RIGHT OUTER JOIN
                                     Tracking ON Groups.group_id = Tracking.content_id
       		
            WHERE        (Tracking.user_id = #userID#) AND (Tracking.app_id = #appID#) AND (Tracking.client_id = #clientID#)

            ORDER BY created
			<cfif appID GT '0'>
            ,Tracking.app_id
            </cfif>
            <cfif clientID GT '0'>
            ,Tracking.client_id
            </cfif>
            
        </cfquery>
        
        <!--- Build Struct --->
        
        <cfset guestInfoData = structNew()>
        
        <!--- Get Paths --->
        <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
            <cfinvokeargument name="clientID" value="#session.clientID#"/>
            <cfinvokeargument name="appID" value="#session.appID#"/>
            <cfinvokeargument name="server" value="yes"/>
        </cfinvoke>
        
        <cfset typePaths = assetPaths.types>        
        
        <cfoutput query="guestInfo">
        
        <!--- Get Main Module --->
        <cfinvoke component="Modules" method="getGrouptPath" returnvariable="module">
            <cfinvokeargument name="groupID" value="#subgroup_id#">
        </cfinvoke>
        
        <cfset theImg = typePaths[assetType_id] &"thumbs/"& image>
        
        <cfset data = {"#name#":{"length":length,"date":created,"image":theImg}}>
        <cfset moduleName = module[arrayLen(module)].name>
        
        <cfif structKeyExists(guestInfoData,"#moduleName#")>
			<cfset structAppend(guestInfoData["#module[arrayLen(module)].name#"],data)> 
        <cfelse>
        	<cfset structAppend(guestInfoData,{"#module[arrayLen(module)].name#":data})>
        </cfif>
        
        </cfoutput>
        
        <cfreturn guestInfoData>
    
    </cffunction>
 
    
    
    
  <!--- Get last session --->
  <cffunction name="getLastUserSession" access="public" returntype="any" output="yes">
    <cfargument name="userID" type="numeric" default="0" required="yes">
    <cfargument name="returnEpoch" type="boolean" default="no" required="no">
    
       <cfquery name="lastSession">
            SELECT TOP 1 created AS date
            FROM SessionTracking
            WHERE user_id = #userID#
            ORDER BY created DESC
        </cfquery>
    
     <cfif returnEpoch>
     	<cfset date = lastSession.date>
     <cfelse>
     
     	<cfif lastSession.recordCount GT '0'>
     
			<!--- convert epoch --->
             <cfinvoke component="Misc" method="convertEpochToDate" returnvariable="date">
                <cfinvokeargument name="TheEpoch" value="#lastSession.date#">
            </cfinvoke>
        
        <cfelse>
            <cfset date = CreateDate(1900,1,1)>
        </cfif>
    
    </cfif>
    
		<cfreturn date>
    
    </cffunction>
    
    
    
   <!--- Get All Active Users --->
  <cffunction name="getAllActiveUsers" access="public" returntype="query" output="yes">
    <cfargument name="appID" type="numeric" default="0" required="yes">
    <cfargument name="clientID" type="numeric" default="0" required="no">
    <cfargument name="activeUser" type="numeric" default="1" required="no">
    <cfargument name="search" type="string" default="" required="no">
    
    <cfquery name="allUsers">
        SELECT        Users.user_id, Users.name, Users.company, Users.email, SessionTracking.created,User.access_id
        FROM            Users LEFT OUTER JOIN
                                 SessionTracking ON Users.user_id = SessionTracking.user_id
        WHERE        (Users.active = #activeUser#) 
        <cfif appID GT 0>
        AND (Users.app_id = #appID#) 
        </cfif>
        <cfif clientID GT 0>
        AND (Users.client_id = #clientID#) 
        </cfif>
        <cfif search NEQ ''>
        AND (name LIKE '%#search#%') OR (email LIKE '%#search#%')
        </cfif>
        AND (SessionTracking.created IS NOT NULL)
        
    </cfquery>
    	ORDER BY Users.name ASC
        
    <cfreturn allUsers>
    
    </cffunction>




	<!--- Get User OR App Access Active State --->
 	<cffunction name="getUserAccess" access="remote" returntype="struct" output="yes">
    	<cfargument name="userID" type="numeric" required="no" default="0">
		     
        <cfquery name="userInfo">
            SELECT access_id
            FROM Users
            WHERE user_id = #userID#
        </cfquery>
        
        <cfif userInfo.recordCount GT '0'>
        
			<!--- Get Access Info --->
            <cfinvoke component="Users" method="getAccessLevel" returnvariable="accessInfo">
                <cfinvokeargument name="accessID" value="#userInfo.access_id#"/>
            </cfinvoke>
            
            <cfset userAccess = {"accessID":accessInfo.access_id , "level":accessInfo.levelName, "accessLevel":accessInfo.accessLevel}>
        <cfelse>
        	<cfset userAccess = {"accessLevel":0}>
        </cfif>
        
        <cfreturn userAccess>  
        
	</cffunction>




<!--- Get User OR App Access Active State --->
 	<cffunction name="getUserActiveState" access="remote" returntype="numeric" output="yes">
    	<cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
		
        <cfset activeState = ''>
        
        <cfif userID GT '0' OR appID GT '0'>
        
			<cfif appID GT '0'>
                
                <cfquery name="result">
                    SELECT active
                    FROM Users
                    WHERE app_id = #appID#
                </cfquery>
            	
                <cfif result.recordCount GT '0'>
                	<cfset activeState = result.active>
                </cfif>
                
            <cfelse>
            
                <cfquery name="result">
                    SELECT active
                    FROM Users
                    WHERE user_id = #userID#
                </cfquery>
                
                <cfif result.recordCount GT '0'>
                	<cfset activeState = result.active>
                </cfif>
                
            </cfif>  

        </cfif>

        <cfreturn activeState>  
        
	</cffunction>
    
    
    
    <!--- Set User OR App Access Active State --->
 	<cffunction name="setUserActiveState" access="remote" returntype="numeric" output="yes">
    	<cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="active" type="numeric" required="no" default="0">
        
        <cfif appID GT '0' OR userID GT '0'>

                <cfquery name="result">
                    UPDATE Users
                    SET active = #active#
                    WHERE 0 = 0
                    
                    <cfif userID GT '0'>
                        AND(user_id = #userID#)
                    <cfelseif appID GT '0'>
                        AND(app_id = #appID#)
                    </cfif> 
                    
                </cfquery>

        </cfif>

        <!--- Get Share state --->
        <cfinvoke component="Users" method="getUserActiveState" returnvariable="activeState">
            <cfinvokeargument name="userID" value="#userID#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
        
        <cfreturn activeState>
    
    </cffunction>
    
    
    
    <!--- deleteUsers --->
 	<cffunction name="deleteUsers" access="remote" returntype="boolean" output="no">
    	<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="UserIDs" type="array" required="yes">
        
        <cfif appID GT 0 AND arrayLen(UserIDs) GT 0>
			
			<cfloop array="#UserIDs#" index="UserID">
				 <cfinvoke component="Users" method="deleteUser" returnvariable="deletedUsers">
					<cfinvokeargument name="userID" value="#UserID#"/>
				</cfinvoke>
        	</cfloop>
        	
			<cfreturn true>
			
		<cfelse>
			<cfreturn false>
        </cfif>
        
     </cffunction>
        
        
	<!--- deleteUsers --->
	<cffunction name="migrateUsers" access="remote" returntype="boolean" output="no">
		<cfargument name="appIDFrom" type="numeric" required="no" default="0">
		<cfargument name="appIDTo" type="numeric" required="no" default="0">
		<cfargument name="UserIDs" type="array" required="yes">

		<cfif appIDFrom GT 0 OR appIDTo GT 0 AND arrayLen(UserIDs) GT 0>
			
				<cfquery name="result">
				UPDATE Users
                SET app_id = #appIDTo#
                WHERE app_id = #appIDFrom# AND user_id = #UserIDs[1]#
                <cfloop array="#UserIDs#" index="UserID">
                OR user_id = #UserID# 
                </cfloop>
                   
				</cfquery>
			<cfabort>
			<cfreturn true>
			
		<cfelse>
			<cfreturn false>	
		</cfif>
		
	</cffunction>
    
    
    <!--- Set User App Access --->
 	<cffunction name="setUserAccessState" access="remote" returntype="boolean" output="no">
    	<cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="userID" type="numeric" required="no" default="0">       
        <cfargument name="accessID" type="numeric" required="no" default="0">
    
        <!--- Get Access Info --->
        <cfinvoke component="Users" method="getAccessLevel" returnvariable="accessInfo">
            <cfinvokeargument name="accessID" value="#accessID#"/>
        </cfinvoke>
        
        <cfset access = accessInfo.accessLevel>
        
        <cfif appID GT '0' OR userID GT '0'>

            <cfquery name="result">
                UPDATE Users
                SET access_id = #access#
                WHERE 0 = 0
                
                <cfif userID GT '0'>
                    AND(user_id = #userID#)
                <cfelseif appID GT '0'>
                    AND(app_id = #appID#)
                </cfif> 
                
            </cfquery>
		
        	<cfreturn true>
        
        </cfif>
        
        <cfreturn false>
    
    </cffunction>
   
 
 
 
 
 
 	<!--- Get User Info - Client and AppID --->
	<cffunction name="getUser" access="public" returntype="query">
      <cfargument name="userID" type="numeric" default="0" required="no">
      
      <cfargument name="email" type="string" default="" required="no">
      <cfargument name="appID" type="numeric" default="0" required="no">
      <cfargument name="clientID" type="numeric" default="0" required="no">
      
      <cfargument name="pass" type="string" default="" required="no">
      <cfargument name="code" type="string" default="" required="no">
      
      <cfset theUserInfo = queryNew("client_id, app_id", "Integer, Integer")>
	  
      
      <cfif userID NEQ '0' OR email NEQ '' OR appID GT '0' OR clientID GT '0' OR code NEQ ''>
        
          <cfquery name="theUserInfo">
                SELECT app_id, client_id, user_id, access_id
                FROM Users
                WHERE 0=0
                <cfif userID GT '0'>
                AND user_id = #userID#
                </cfif>
                <cfif appID GT '0'>
                AND app_id = #appID#
                </cfif>
                <cfif clientID GT '0'>
                AND client_id = #clientID#
                </cfif>
                <cfif email NEQ ''>
                AND email = '#email#'
                </cfif>
                <cfif pass NEQ ''>
                AND pass = '#pass#'
                </cfif>
                <cfif code NEQ ''>
                AND code = '#code#'
                </cfif>
          </cfquery>
	
    </cfif>
    
    <cfreturn theUserInfo>

	</cffunction>
 
 
 
 
   
   
   <!---Forgot General ID Resend--->
    <cffunction name="forgotGeneralID" access="public" returntype="void" output="yes">
    	<cfargument name="email" type="string" required="yes" default="">
        
        <!--- <cfset data = structNew()>
        
        <!---Check if User EMail Exists--->
        <cfinvoke component="users" method="findUser" returnvariable="foundUsers">
			<cfinvokeargument name="email" value="#email#"/>
        </cfinvoke>
        
        <cfif foundUsers.recordCount GT 0>
        	<cfinvoke component="errors" method="getError" returnvariable="errorStruct">
            	<cfinvokeargument name="error_code" value="1005">
			</cfinvoke>

            <cfset data['error'] = errorStruct>
        </cfif>
        
        <!---Send Email- TO DO--->
        
        <cfset data = SerializeJSON(data)>
        
        <cfoutput>#data#</cfoutput> --->

	</cffunction>

<!---Register User--->
    <cffunction name="registerUser" access="remote" returntype="struct" output="no">
    	
        <cfargument name="name" type="string" required="no" default="">
        <cfargument name="email" type="string" required="no" default="">
        <cfargument name="code" type="string" required="no" default="">

        <cfargument name="phone" type="string" required="no" default="">
        
        <cfargument name="password" type="string" required="no" default="">
        <cfargument name="accessLevel" type="numeric" required="no" default="0">

        <cfargument name="clientID" type="numeric" required="no" default="0">
    	<cfargument name="appID" type="numeric" required="yes" default="0">
        <cfargument name="userType" type="numeric" required="no" default="0">
        
        <cfargument name="active" type="boolean" required="no" default="no">
        
        <cfargument name="reco" type="string" required="no" default="">
      

        <!---Check if User EMail Exists--->
        <cfinvoke component="Users" method="userExists" returnvariable="exists">
        	<cfinvokeargument name="name" value="#name#"/>
			<cfinvokeargument name="email" value="#email#"/>
            <cfinvokeargument name="clientID" value="#clientID#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
	
        <cfif len(reco) GT 7><cfset reco = ''></cfif>
        
        <cfset data = structNew()>
		<cfset userID = 0>
        
        <cfif exists>
        	<!--- User Not Exists Error --->
        	<cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1014"/>
            </cfinvoke>
                 
        <cfelse>

            <!--- Get Client ID if not provided --->
            <cfif clientID IS 0 OR appID IS 0>
            	 
                <!--- Get User Info --->
                <cfinvoke component="Users" method="getUser" returnvariable="user">
                    <cfinvokeargument name="userID" value="#userID#"/>
                </cfinvoke>
               
                <cfif clientID IS 0>
                	<cfset clientID = user.clientID>
                </cfif>
                <cfif appID IS 0>
                	<cfset appID = user.app_id>
                </cfif>
                
            </cfif>
    	
            <cfif code IS ''>
			<!---Create GeneralID--->
                <cfinvoke component="Misc" method="makeID" returnvariable="code">
                    <cfinvokeargument name="length" value="8"/>
                    <cfinvokeargument name="type" value="1"/>
                </cfinvoke>
            </cfif>
            
            <!---CurDate--->
            <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate"></cfinvoke>
 			
            <cfif active>
				<cfset active = 1>
            <cfelse>
            	<cfset active = 0>
            </cfif>
            
            <!---Create Record for User--->
            <cfquery name="newUser">
                INSERT INTO 
                    Users 
                    (name, email, access_id, client_id, app_id, code, created, password, active, userType, reco, phone)
                VALUES 
                    ('#name#','#email#', #accessLevel#, #clientID#, #appID#, '#code#', #curDate#, <cfif trim(password) IS ''>NULL<cfelse>'#trim(password)#'</cfif>, #active#, #userType#, '#reco#', '#phone#')
                
                SELECT @@Identity AS userID  
                SET NOCOUNT OFF
            </cfquery>
            
            <cfset userID = newUser.userID>
            
            <cfinvoke  component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
		
        </cfif>
        
        <cfset structAppend(data,{'error':error, "userid":userID})>
        
        <cfreturn data>
    
    </cffunction>
   
   
   
   <!--- Resend User Information for Registration --->
   <cffunction name="resendUserRegistrationInfo" access="remote" returntype="boolean" output="yes">
   		<cfargument name="email" type="string" required="yes" default="">
        <cfargument name="clientID" type="numeric" required="yes" default="0">
   
    	<!--- Get User, CLientID and AppID --->
        <cfinvoke  component="Users" method="getUser" returnvariable="user">
          <cfinvokeargument name="email" value="#email#"/>
          <cfinvokeargument name="clientID" value="#clientID#"/>
        </cfinvoke>
        
        <cfset userID = user.user_id>
        <cfset appID = user.app_id>
        
        <cfif user.recordCount GT '0'>
        
            <cfinvoke component="Apps" method="getClientApps" returnvariable="apps">
                <cfinvokeargument name="clientID" value="#clientID#"/>
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
            
            <cfinvoke  component="Clients" method="getClientInfo" returnvariable="info">
              <cfinvokeargument name="clientID" value="#clientID#"/>
            </cfinvoke>
            
            <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
                <cfinvokeargument name="clientID" value="#clientID#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
            
            <cfinvoke component="Users" method="getUserInfo" returnvariable="userInfo">
                <cfinvokeargument name="userID" value="#userID#"/>
            </cfinvoke>
            
            <cfset name = trim(userInfo.name)>
            <cfset email = trim(userInfo.email)>
            <cfset code = trim(userInfo.code)>
            <cfset password = trim(userInfo.password)>
        	
            <!--- Send Email to User that they are Approved --->
            <cfmail server="cudaout.media3.net"
                    username="support@wavecoders.ca"
                    from="#assetPaths.client.name# Registration <#info.support#>"
                    to="#name# <#email#>"
                    subject="User Registration - #assetPaths.client.name#"
                    replyto="Support <#info.support#>"
                    type="HTML"> 
            
                    <!--- HTML RegUser --->
                    <link href="http://www.liveplatform.net/register/regStyles.css" rel="stylesheet" type="text/css">
                    
                    <cfinclude template="../../../register/approved.cfm">
            
            </cfmail>
            
            <cfset icon = assetPaths.client.icon>
            
            <cfreturn true>
            
        </cfif>
		
        <cfreturn false>
   
   </cffunction>
   
   
   <!--- Resend Set User Password for Account --->
   <cffunction name="resendSetPassword" access="remote" returntype="boolean" output="yes">
   		<cfargument name="email" type="string" required="yes" default="">
        <cfargument name="clientID" type="numeric" required="yes" default="0">
   
    	<!--- Get User, CLientID and AppID --->
        <cfinvoke  component="Users" method="getUser" returnvariable="user">
          <cfinvokeargument name="email" value="#email#"/>
          <cfinvokeargument name="clientID" value="#clientID#"/>
        </cfinvoke>
        
        <cfset userID = user.user_id>
        <cfset appID = user.app_id>
        
        <cfif user.recordCount GT '0'>
        
            <cfinvoke component="Apps" method="getClientApps" returnvariable="apps">
                <cfinvokeargument name="clientID" value="#clientID#"/>
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
            
            <cfinvoke  component="Clients" method="getClientInfo" returnvariable="info">
              <cfinvokeargument name="clientID" value="#clientID#"/>
            </cfinvoke>
            
            <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
                <cfinvokeargument name="clientID" value="#clientID#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
            
            <cfinvoke component="Users" method="getUserInfo" returnvariable="userInfo">
                <cfinvokeargument name="userID" value="#userID#"/>
            </cfinvoke>
            
            <cfset name = userInfo.name>
            <cfset email = userInfo.email>
            <cfset code = userInfo.code>
        
            <!--- Send Email to User that they are Approved --->
            <cfmail server="cudaout.media3.net"
                    username="support@wavecoders.ca"
                    from="#assetPaths.client.name# Support <#info.support#>"
                    to="#name# <#email#>"
                    subject="Password Update - #assetPaths.client.name#"
                    replyto="Support <#info.support#>"
                    type="HTML"> 
            
                    <!--- HTML RegUser --->
                    <link href="http://www.liveplatform.net/register/regStyles.css" rel="stylesheet" type="text/css">
                    
                    <cfinclude template="../../../register/setPasswordEMail.cfm">
            
            </cfmail>
            
            <cfreturn true>
            
        </cfif>
		
        <cfreturn false>
   
   </cffunction>
   
</cfcomponent>


