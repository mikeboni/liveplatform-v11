<cfcomponent>
	<!--- New Choice for Quiz --->
	<cffunction name="newQuizChoice" access="remote" returntype="boolean">
    
		<cfargument name="selectionID" type="numeric" required="yes">
        
        <cfargument name="text" type="string" required="no" default="Question Choice Text">
        <cfargument name="imageURL" type="string" required="no" default="">
        <cfargument name="correct" type="boolean" required="no" default="false">
        
        <cfif correct><cfset correct = 1><cfelse><cfset correct = 0></cfif>
        
        <cfquery name="selectionChoices">  
            SELECT  url, text, correct, sortorder, choice_id
            FROM	ChoiceAssets
            WHERE   selection_id = #selectionID#           
        </cfquery>
        
        <cfset sortOrder = selectionChoices.recordCount + 1>
        
		<cfquery name="choiceAsset">
            INSERT INTO ChoiceAssets (selection_id , url, text, sortOrder, correct)
            VALUES (#selectionID#,
            		<cfif imageURL IS ''> 
                    NULL
                    <cfelse>
            		'#imageURL#'
                    </cfif>
                    , '#text#', #sortOrder#, #correct#) 
        </cfquery>
        
		<cfreturn true>
        
	</cffunction>
    
    
    
    
    <!--- Remove Choice for Quiz --->
    <cffunction name="removeQuizChoice" access="remote" returntype="boolean">
    
		<cfargument name="selectionID" type="numeric" required="yes">
        <cfargument name="choiceID" type="numeric" required="yes">
        <cfargument name="deleteAll" type="boolean" required="no" default="false">
        
        <cfquery name="totalChoices">  
            SELECT  url, text, correct, sortorder, choice_id
            FROM	ChoiceAssets
            WHERE   selection_id = #selectionID#           
        </cfquery>
        
        <cfset totalChoices = totalChoices.recordCount>
        
        <cfif totalChoices-1 GTE 2 OR deleteAll>
        	
            <!--- Delete Image for Choice --->
            <cfinvoke component="Quiz" method="deleteChoiceImage" returnvariable="deleted">
                <cfinvokeargument name="choiceID" value="#choiceID#"/>
            </cfinvoke>
            
            <cfif deleted>
            
                <cfquery name="deleteDetails">
                    DELETE FROM ChoiceAssets
                    WHERE selection_id = #selectionID# AND choice_id = #choiceID#
                </cfquery>
			
            	<cfreturn true>
        	
            </cfif>
            
        <cfelse>
        	<cfreturn false>
        </cfif>
        
    </cffunction>
 
    
    
	<!--- Import Image for Quiz --->
	<cffunction name="uploadFile" access="remote" returntype="boolean">
    
        <cfargument name="fileToUpload" type="any" required="yes">
        <cfargument name="choiceID" type="numeric" required="yes">  
        
        <cfquery name="Assets">  
            SELECT  DISTINCT(QuizAssets.asset_id) AS assetID, url
            FROM	QuizAssets RIGHT OUTER JOIN ChoiceAssets ON QuizAssets.selection_id = ChoiceAssets.selection_id
            WHERE   choice_id = #choiceID#           
        </cfquery>
        
        <cfset assetID = Assets.assetID>
        <cfset imgUrl = Assets.url>
        
		<cfset tempPath = GetTempDirectory()>
        
        <cffile action="upload" filefield="#fileToUpload#" destination="#tempPath#" nameconflict="makeunique" result="uploadFile">
        <cfset imageFile = uploadFile.serverFile>
		
        
		<!--- Image Size --->
        <cfinvoke component="CFC.File" method="getImageSize" returnvariable="imageSize">
            <cfinvokeargument name="imagePath" value="#tempPath##uploadFile.serverFile#"/>
        </cfinvoke>
        
        <!--- Asset Path --->
        <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>   
        
        <!--- Is it a new file? --->
        <cfif Assets.url NEQ uploadFile.clientFile>
        
        <!--- Delete old file --->
		<cfset fileSrcPath = assetPath & Assets.url>
        	<cfif FileExists(fileSrcPath)>
        		<cffile action = "delete" file = "#fileSrcPath#">
            </cfif>
        </cfif>

        <!--- New File + Path --->
		<cfset fileDestPath = assetPath & uploadFile.clientFile>
        
        <!--- Move Asset --->
        <cfinvoke component="Assets" method="moveContent" returnvariable="ImageFileName">
          <cfinvokeargument name="destPathFile" value="#fileDestPath#"/>
        </cfinvoke>
        
        <!---Check Path--->
		<cfset nonRetinaPath = GetDirectoryFromPath(fileDestPath) & "nonretina/">
  
		<!--- Create Directory if NOT exist --->
        <cfif NOT directoryExists(nonRetinaPath)>
            <cfdirectory action="create" directory="#nonRetinaPath#" mode="777">
        </cfif>
        
        <!--- Specs --->
        <cfinvoke component="File" method="getFileSpecs" returnvariable="fileSpecs">
            <cfinvokeargument name="fullPath" value="#fileDestPath#"/>
        </cfinvoke>
        
        <!--- Copy and Scale Image 1/2 --->
        <cfset nonRetinaDest = nonRetinaPath & fileSpecs.file>
		
        <!--- Create non-retina image --->
        <cfinvoke component="Misc" method="createNonRetinaImage" returnvariable="scaled">
            <cfinvokeargument name="imageSrc" value="#fileDestPath#"/>
            <cfinvokeargument name="imageDes" value="#nonRetinaDest#"/>
        </cfinvoke>
        
        <!--- get image size --->
        <cfinvoke component="Misc" method="getImageSpecs" returnvariable="imageSpecs">
            <cfinvokeargument name="imageSrc" value="#fileDestPath#"/>
        </cfinvoke>
       
        <!--- Update URL for Choice --->
        <cfquery name="updateURL">
            UPDATE ChoiceAssets
            SET url = '#ImageFileName#', width = #imageSpecs.width#, height = #imageSpecs.height#, aspectRatio = #imageSpecs.ratio#
            WHERE choice_id = #choiceID#
        </cfquery>
		
        <cflocation url="AssetsView.cfm?assetID=#assetID#&assetTypeID=20">
        
        <cfreturn true>
    
 	</cffunction>
 
 
 
     <!--- Remove Choice Image --->
    <cffunction name="deleteChoiceImage" access="remote" returntype="boolean">
        <cfargument name="choiceID" type="numeric" required="yes">
		
        <cfquery name="Assets">  
            SELECT  DISTINCT(QuizAssets.asset_id) AS assetID, url
            FROM	QuizAssets RIGHT OUTER JOIN ChoiceAssets ON QuizAssets.selection_id = ChoiceAssets.selection_id
            WHERE   choice_id = #choiceID#           
        </cfquery>
        
        <cfset assetID = Assets.assetID>
        <cfset imgUrl = Assets.url>
       
        <cfif assetID GT 0>
        
			<!--- Asset Path --->
            <cfinvoke component="File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke> 
    
            <cfset fileSrcPathRetina = assetPath & imgUrl>
            <cfset fileSrcPath = assetPath & 'nonretina/' & imgUrl>
            
            <cfif FileExists(fileSrcPathRetina)>
                <cffile action = "delete" file = "#fileSrcPathRetina#">
            </cfif>
            
            <cfif FileExists(fileSrcPath)>
                <cffile action = "delete" file = "#fileSrcPath#">
            </cfif>
            
            <cfquery name="updateURL">
                UPDATE ChoiceAssets
                SET url = NULL
                WHERE choice_id = #choiceID#
            </cfquery>
    
            <cfreturn true>
        
        <cfelse>
        	<cfreturn false>
        </cfif>
        
    </cffunction>
 
 
    
    
    
    <!--- Update Choice for Quiz --->
    <cffunction name="updateQuizChoice" access="remote" returntype="boolean">
    
		<cfargument name="selectionID" type="numeric" required="yes">
        <cfargument name="choiceID" type="numeric" required="yes">
        <cfargument name="choiceData" type="struct" required="yes" default="">
        
        <cfquery name="updateColors">
            UPDATE ChoiceAssets
            SET 
            <cfif structKeyExists(choiceData,'url')>
            	url = '#choiceData.url#'
            </cfif>
            <cfif structKeyExists(choiceData,'text')>
            	text = '#choiceData.text#'
            </cfif>
            <cfif structKeyExists(choiceData,'sortOrder')>
            	sortOrder = #choiceData.sortOrder#
            </cfif>
            <cfif structKeyExists(choiceData,'correct')>
            	
                <cfif choiceData.correct>
					<cfset choiceData.correct = 1>
                <cfelse>
					<cfset choiceData.correct = 0>
                </cfif>
            
            	correct = #choiceData.correct#
            </cfif>
            WHERE selection_id = #selectionID# AND choice_id = #choiceID#
        </cfquery>
        
        <cfreturn true>
        
    </cffunction>
    
    
    
    
</cfcomponent>