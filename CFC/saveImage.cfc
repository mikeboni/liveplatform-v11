<cfcomponent>
  	
   	<cffunction name="ImageSave" access="remote" returntype="boolean">
    	<cfargument name="data" type="binary" required="true" />
    	<cfargument name="fileName" type="string" required="false" default="" />
    	<cfargument name="assetID" type="numeric" required="false" default="0" />
 		
 		<cfif fileName IS ''>
    		 <cfset fileName = "untitled.jpg">
		</cfif>
		
   		<cfset theFilePath = GetTempDirectory() & fileName>
   		
    	<cftry>
			
			<cfset myImage=ImageNew(data)>
			<cfset imageWrite(myImage, theFilePath) />
   		
   			<!--- thumb specs --->
   			<cfset assetData = {'assetID':assetID}>
   			<cfset imageSize = {'width':myImage.width ,'height':myImage.height}>
   			<cfset thumbData = {'thumb':{'file':fileName, 'size': imageSize}, 'thumbnail':theFilePath}>
			
			<!---Update Thumbnail--->
            <cfinvoke component="Assets" method="updateThumbnail" returnvariable="updatedThumbnail">
                <cfinvokeargument name="assetData" value="#assetData#"/>
                <cfinvokeargument name="thumbData" value="#thumbData#"/>
            </cfinvoke>

    		<cfreturn true>
    		
			<cfcatch type="any">
				<cfreturn false>
			</cfcatch>
		</cftry>
 		
  	</cffunction>
  	
</cfcomponent>