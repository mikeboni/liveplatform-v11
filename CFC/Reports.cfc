<cfcomponent>

	<!--- Total Carts --->
	<cffunction name="getNumberOfCartsReport" access="public" returntype="numeric">
		<cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        
		<cfset carts="0">
        
        <cfquery name="totalCarts"> 
            SELECT        COUNT(cart_id) AS carts
            FROM          SessionCart
            WHERE         0=0
            <cfif userID GT 0>
			AND user_id = #userID#
			</cfif>
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
        </cfquery>
        
		<cfreturn totalCarts.carts>
        
	</cffunction>
 

	<!--- Total Carts Viewed --->
	<cffunction name="getCartsViewedReport" access="public" returntype="struct">
		<cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        
		<cfset carts="0">
        
        <!--- Total Carts --->
        <cfinvoke component="Reports" method="getNumberOfCartsReport" returnvariable="totalCarts">
            <cfinvokeargument name="userID" value="#userID#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
        
        <!--- Carts Viewed --->
        <cfquery name="carts"> 
            SELECT        sum(case when viewed = 1 THEN 1 ELSE 0 END) AS totalViewed
            FROM          SessionCart
            WHERE         0=0
            <cfif userID GT 0>
            AND user_id = #userID#
            </cfif>
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
        </cfquery>
     	
        <!--- Get Views --->
        <cfinvoke component="Reports" method="getCartViewsReport" returnvariable="views">
            <cfinvokeargument name="userID" value="#userID#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
        
        <cfset cartsInfo = {"total":totalCarts,"viewed":carts.totalViewed, "views":views}>
        
		<cfreturn cartsInfo>
        
	</cffunction>
 


	<!--- Total Carts Views --->
	<cffunction name="getCartViewsReport" access="public" returntype="numeric">
    	<cfargument name="appID" type="numeric" required="no" default="0">
		<cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="cartID" type="numeric" required="no" default="0">
        <cfargument name="cartToken" type="string" required="no" default="">
        
		<cfset carts="0">
        
        <cfquery name="cartViews"> 
            SELECT        sum(viewes) AS views
            FROM          SessionCart
            WHERE		 0 = 0
            <cfif userID GT 0>
            AND user_id = #userID#
            </cfif>
            <cfif cartID GT 0>
            AND cart_id = #cartID#
            </cfif>
            <cfif cartToken NEQ ''>
            AND token = '#cartToken#'
            </cfif>
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
        </cfquery>
        
        <cfif cartViews.views IS ''>
			<cfset views = 0>
        <cfelse>
        	<cfset views = cartViews.views>
        </cfif>

		<cfreturn views>
        
	</cffunction>



	<!--- Total Carts Assets Sent --->
	<cffunction name="getCartAssetsReport" access="public" returntype="numeric">
    	<cfargument name="cartID" type="numeric" required="no" default="0">
        <cfargument name="cartToken" type="string" required="no" default="">
		<cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        
		<cfset assets="0">
        
        <!--- Carts Viewed --->
        <cfquery name="allCarts"> 
            SELECT        contentIDs
            FROM          SessionCart
            WHERE         0=0
            <cfif cartID GT 0>
            AND cart_id = #cartID#
            </cfif>
            <cfif cartToken NEQ ''>
            AND token = '#cartToken#'
            </cfif>
            
            <cfif userID GT 0>
            AND user_id = #userID#
            </cfif>
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
        </cfquery>
     	
        <cfoutput query="allCarts">
			<cfset assets += listLen(allCarts.contentIDs,",")>
		</cfoutput>
   
		<cfreturn assets>
        
	</cffunction>



	<!--- Total Assets Viewed --->
	<cffunction name="getTrackedAssetsReport" access="public" returntype="struct">
    	<cfargument name="cartID" type="numeric" required="no" default="0">
		<cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="clientID" type="numeric" required="no" default="0">
        
        <cfargument name="cartToken" type="string" required="no" default="">
        
		<cfset assets="0">
        
        <!--- Carts User Viewed --->
        <cfquery name="allAssets"> 
            SELECT          COUNT(track_id) AS viewedApp
            FROM            Tracking
            WHERE        user_id <> 0 AND (cart_id = 0) 

            <cfif userID GT 0>
            AND (user_id = #userID#) 
            </cfif>
            <cfif appID GT 0>
            AND (app_id = #appID#) 
            </cfif>
            <cfif clientID GT 0>
            AND (client_id = #clientID#)
            </cfif>
        </cfquery>
        
        <cfif allAssets.recordCount IS 0>
        	<cfset viewedApp = 0>
        <cfelse>
        	<cfset viewedApp = allAssets.viewedApp>
        </cfif>
        
        
        <!--- Carts EMail Viewed --->
        <cfquery name="allAssets"> 
            SELECT          COUNT(track_id) AS viewedGuest
            FROM            Tracking
            WHERE        0 = 0 AND (cart_id <> 0) 
            <cfif userID GT 0>
            AND (Tracking.user_id = #userID#) 
            </cfif>
            <cfif appID GT 0>
            AND (Tracking.app_id = #appID#) 
            </cfif>
            <cfif clientID GT 0>
            AND (Tracking.client_id = #clientID#)
            </cfif>
        </cfquery>
        
        <cfif allAssets.recordCount IS 0>
        	<cfset viewedGuest = 0>
        <cfelse>
        	<cfset viewedGuest = allAssets.viewedGuest>
        </cfif>
        
        <cfset viewed = {"app":viewedApp, "guest":viewedGuest}>
        
		<cfreturn viewed>
        
	</cffunction>




	<!--- Assets Viewed Trend--->
	<cffunction name="getAssetTrendReport" access="public" returntype="array" output="yes">
    	<cfargument name="cartID" type="numeric" required="no" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="groupID" type="numeric" required="no" default="0">
        <cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="top" type="numeric" required="no" default="0">
        <cfargument name="cartToken" type="string" required="no" default="">
        
		<cfset assets="0">
        
        <!--- All Assets --->
        <cfquery name="allAssets"> 
            SELECT          DISTINCT asset_id AS assetID
            FROM            Groups
            WHERE        0=0
            <cfif cartID GT 0>
            AND (group_id = #cartID#) 
            </cfif>
            <cfif appID GT 0>
            AND (app_id = #appID#) 
            </cfif>
            <cfif groupID GT 0>
            AND (group_id = #groupID#) 
            </cfif>
            AND asset_id IS NOT NULL
        </cfquery>
 
        <!--- Get Carts Assets Sent --->
        <cfquery name="totalCarts"> 
            SELECT        contentIDs
            FROM          SessionCart
            WHERE         0=0
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
            <cfif cartID GT 0>
            AND cart_id = #cartID#
            </cfif>
            <cfif userID GT 0>
            AND user_id = #userID#
            </cfif>
        </cfquery>
        
        <cfset assetList = structNew()>
    
        <!--- Get All Assets - Distinct --->
        <cfoutput query="allAssets">
        
			<!--- Get Asset Details --->
            <cfinvoke component="Assets" method="getAssetTitle" returnvariable="theName">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>

            <cfif theName IS ''><cfset theName = "unknown"></cfif>
            
            <!--- add asset --->
			<cfset structAppend(assetList,{"#assetID#":{"emailView":0, "appViewed":0, "name":theName, "sent":0}})>
        
		</cfoutput>

        <!--- Carts Viewed --->
        <cfquery name="allAssetsTracked"> 
            SELECT          Assets.asset_id AS assetID, Tracking.user_id, Tracking.cart_id
            FROM            Groups LEFT OUTER JOIN
                                     Assets ON Groups.asset_id = Assets.asset_id RIGHT OUTER JOIN
                                     Tracking ON Groups.group_id = Tracking.content_id
            WHERE        0=0
            <cfif cartID GT 0>
            AND (Tracking.content_id = #cartID#) 
            </cfif>
            <cfif appID GT 0>
            AND (Tracking.app_id = #appID#) 
            </cfif>
            <cfif userID GT 0>
            AND ( Assets.user_id = #userID#)
            </cfif>
            AND Assets.asset_id IS NOT NULL
           
        </cfquery>

        
        <!--- Assets Viewed --->
		<cfoutput query="allAssetsTracked">

            <cfif cart_id GT 0>
                
                <cfset theVal1 = assetList[assetID].emailView>
                <cfset theCounterEmail = theVal1+1>
				<cfset findPos = StructUpdate(assetList[assetID],"emailView",theCounterEmail)>
            
            <cfelse>
            	
                <cfset theVal2 = assetList[assetID].appViewed>
                <cfset theCounterApp = theVal2+1>
                <cfset findPos = StructUpdate(assetList[assetID],"appViewed",theCounterApp)>
                
            </cfif> 
        
        </cfoutput>

       
        <!--- Assets Sent --->
        <cfoutput query="totalCarts">
		
            <cfloop index="contentID" list="#contentIDs#" delimiters=",">
            
                <!--- Get AssetID from ContentID --->
                <cfinvoke component="Modules" method="getAssetIDFromContentGroup" returnvariable="theAsset">
                    <cfinvokeargument name="groupID" value="#contentID#"/>
                </cfinvoke>
                
                <cfset assetID = theAsset.assetID>
                
                <cfif StructKeyExists(assetList,"#assetID#")>
                
					<cfset theVal = assetList[assetID].sent>
                    <cfset theCounter = theVal+1>
        
                    <cfset findPos = StructUpdate(assetList[assetID],"sent",theCounter)>
                    
				</cfif>
                
            </cfloop>
		
		</cfoutput>
       
         <cfset sortedAssets = StructSort( assetList, "numeric", "DESC", "emailView" )> 

         <cfset sortedData = arrayNew(1)>
	 	  
         <cfset cnt = 0>	
	 
         <cfloop index="z" from="1" to="#arrayLen(sortedAssets)#">
			 <cfif cnt GT top><cfbreak></cfif>
             <cfset assetID = sortedAssets[z]>
             
             <cfset theObject = assetList[assetID]>
             <cfset structAppend(theObject,{"assetID":assetID})>
             
            <cfset arrayAppend(sortedData,theObject)>
             
             <cfset cnt++>
         </cfloop>

		<cfreturn sortedData>
        
	</cffunction>




	<!--- Assets Viewed Trend--->
	<cffunction name="getTopTrendReport" access="public" returntype="query">
    	<cfargument name="trend" type="struct" required="yes">
        
        <cfset assets = arrayNew(1)>
        <cfset views = arrayNew(1)>
        <cfset sents = arrayNew(1)>
        <cfset names = arrayNew(1)>
        
        
        <cfloop collection="#trend#" item="assetID">
        
        	<cfset arrayAppend(assets,Val(assetID))>
			<cfset arrayAppend(views,trend[assetID].viewed)>
            <cfset arrayAppend(sents,trend[assetID].sent)>
            <cfset arrayAppend(names,trend[assetID].name)>
         
        </cfloop> 
        
        <cfset trendQ = QueryNew("")>
        
        <cfset QueryAddColumn(trendQ, "asset_id", "integer", assets)>
		<cfset QueryAddColumn(trendQ, "viewed", "integer", views)>
        <cfset QueryAddColumn(trendQ, "sent", "integer", sents)> 
        <cfset QueryAddColumn(trendQ, "name", "VarChar", names)>

        <cfreturn trendQ>
        
    </cffunction>
 
 
<!--- Admin Reports ---> 
    
    
    
    
<!--- Super Admin Reports --->

</cfcomponent>