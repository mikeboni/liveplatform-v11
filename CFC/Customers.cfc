<cfcomponent>
	
    
    <!--- get HowHear List for Registration --->
	<cffunction name="getHowHear" access="public" returntype="array">
    
		<cfset howHearList = arrayNew(1)>
        
        <cfquery name="howHear">
            SELECT      howHear_id, type
            FROM        HowHear
            ORDER BY	howHear_id DESC
        </cfquery>
        
        <cfoutput query="howHear">
		
        	<cfset arrayAppend(howHearList, {'id':#howHear_id#, 'name':'#type#'})>
		
		</cfoutput>

        <cfreturn howHearList>
    
    </cffunction>
    
	<!--- Save Customer Registration --->
	<cffunction name="customerRegistration" access="public" returntype="string">
    
		<cfargument name="firstName" type="string" required="yes" default="unknown">
        <cfargument name="lastName" type="string" required="no" default="">
		<cfargument name="email" type="string" required="yes">
        <cfargument name="phone" type="string" required="no" default="">
        <cfargument name="postalZip" type="string" required="no" default="">
        <cfargument name="city" type="string" required="no" default="">
        
        <cfargument name="howHear" type="numeric" required="no" default="1">
        <cfargument name="auth_token" type="string" required="yes">
        
        <cfargument name="ip" type="string" required="no" default="">
        <cfargument name="projectID" type="numeric" required="no" default="0">
        
        <!--- Current Date --->
        <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <!--- Get ClientID and AppID from Token --->
        
        <cfquery name="token">
            SELECT      Tokens.app_id AS appID, Applications.client_id AS clientID
			FROM        Tokens INNER JOIN
                        Applications ON Tokens.app_id = Applications.app_id
            WHERE	    token = '#auth_token#'
        </cfquery>
        
        <cfset appID = token.appID>
        <cfset clientID = token.clientID>
        	
        <!--- GET Location from IP --->
        
        <!--- Get GPS Location --->
        <cfinvoke component="Tracking" method="getIPLocation" returnvariable="locationID">
			<cfinvokeargument name="ip" value="#ip#">
        </cfinvoke>
        
        <!---Check if EMAIL exists --->
        <cfquery name="registrations">
             SELECT registration_id
             FROM	Registration
             WHERE 	email = '#email#'
        </cfquery>
        
        <cfif registrations.recordCount GT 0>
        	<cfset regExists = true>
        <cfelse>
        	<cfset regExists = false>
        </cfif>
       
        <cfif NOT regExists AND projectID GT 0>
        
			<!--- add registration entry to DB --->
            <cfquery name="newRegistration">
                 INSERT INTO Registration (firstName, lastName, email, phone, city, postalZip, howHear_id, app_id, location_id, project_id, created)
                 VALUES ('#firstName#', '#lastName#', '#email#', '#phone#', '#city#', '#postalZip#', #howHear#, #appID#, #locationID#, #projectID#, #curDate#)
            </cfquery>
        
        </cfif>
     
        <!--- send email thanks --->
        <cfinvoke component="Customers" method="sendCustomerThanksRegistrationEmail" returnvariable="sentEmail">
			<cfinvokeargument name="name" value="#firstName# #lastName#">
            <cfinvokeargument name="email" value="#email#">
            <cfinvokeargument name="projectID" value="#projectID#">
        </cfinvoke>
       
		<cfreturn sentEmail>
        
	</cffunction>
    
    
    <!--- Get ALL Customers --->
    <cffunction name="getCustomerRegistrations" access="public" returntype="query">
    	<cfargument name="appID" type="numeric" required="no" default="0">
    	<cfargument name="auth_token" type="string" required="no" default="">
        
        <cfset clientID = 0>
        
        <!--- get infor from auth token --->
        <cfif appID GT 0>
        	<!--- get clientID --->
            
            <cfquery name="clients">
                SELECT      client_id
                FROM        Applications
                WHERE	    app_id = #appID#
            </cfquery>
            
            <cfif clients.recordCount GT 0>
            	<cfset clientID = clients.client_id>
            </cfif>
            
        <cfelse>
        
        	<cfif auth_token NEQ ''>
				<!--- Get ClientID and AppID from Token --->
                
                <cfquery name="token">
                    SELECT        Tokens.app_id AS appID, Applications.client_id AS clientID, Users.name, Users.access_id, AccessLevels.accessLevel, AccessLevels.levelName
					FROM          Tokens INNER JOIN
                        Applications ON Tokens.app_id = Applications.app_id INNER JOIN
                        Users ON Tokens.user_id = Users.user_id INNER JOIN
                        AccessLevels ON Users.access_id = AccessLevels.access_id
                    WHERE	    token = '#auth_token#' AND AccessLevels.accessLevel = 4
                </cfquery>
                
                <cfif token.recordCount GT 0>
					<cfset appID = token.appID>
                    <cfset clientID = token.clientID>
				</cfif>
                
            </cfif>
        
        </cfif>
 
        <!--- build empty query --->
        <cfset allRecords = queryNew("fistName, lastName, email, phone, postalZip, howHear, location", "VarChar, VarChar, VarChar, VarChar, VarChar, VarChar, VarChar")>
        
        <cfif appID GT 0 AND clientID GT 0>
        
        	<!--- get QUERY for all Registrations, order by date --->
        
    	</cfif>
        
        <cfreturn allRecords>
    
    </cffunction>
    
    
    
    <cffunction name="sendCustomerThanksRegistrationEmail" access="public" returntype="boolean">
    	<cfargument name="name" type="string" required="no" default="unknown">
    	<cfargument name="email" type="string" required="no" default="">
        <cfargument name="projectID" type="numeric" required="no" default="0">
        
        <cfset success = false>
           
        <cfif projectID GT 0 AND email NEQ ''>
       	
            <cfquery name="emailInfo">
                SELECT	Applications.app_id AS appID, Applications.client_id AS clientID, Clients.contactEmail, Clients.company, 
                    IIF(Details.title != '', Details.title,Groups.name) AS projectName
					FROM Applications LEFT OUTER JOIN
                         Clients ON Applications.client_id = Clients.client_id RIGHT OUTER JOIN
                         Details RIGHT OUTER JOIN
                         Groups ON Details.detail_id = Groups.detail_id ON Applications.app_id = Groups.app_id LEFT OUTER JOIN
                         EMailMessages RIGHT OUTER JOIN
                         EMail ON EMailMessages.message_id = EMail.message_id ON Groups.group_id = EMail.group_id
                WHERE        (Groups.group_id = #projectID#)
            </cfquery>
            
       
            <!--- Get Paths --->
            <cfinvoke component="Apps" method="getPaths" returnvariable="assetPaths">
                <cfinvokeargument name="clientID" value="#emailInfo.clientID#"/>
                <cfinvokeargument name="appID" value="#emailInfo.appID#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
            
            <!--- EMAIL TEMPLATE HERE --->
            
            <!--- Get Website --->
             <cfinvoke component="One2" method="getFeatureID" returnvariable="asset">
                <cfinvokeargument name="appID" value="#emailInfo.appID#"/>
                <cfinvokeargument name="moduleID" value="#projectID#"/>
                <cfinvokeargument name="featureName" value="website"/>
            </cfinvoke>  
            
            <cfset assetID = asset.asset_id>
             <!--- Get Website by finding an asset called website --->           
            <cfquery name="websiteID">
                SELECT        Assets.asset_id AS assetID
                FROM            Groups INNER JOIN
                                        Assets ON Groups.asset_id = Assets.asset_id
                WHERE        (Groups.app_id = #emailInfo.appID#) AND (Assets.name = N'website')
			</cfquery>
           
           	<cfinvoke component="Assets" method="getAssetInfo" returnvariable="website">
                <cfinvokeargument name="assetID" value="#websiteID.assetID#"/>
            </cfinvoke> 
          
            <cfif website.title IS ''>
				<cfset websiteName = website.assetName>
            <cfelse>
				<cfset websiteName = website.title>
            </cfif>
            
            <cfset webLink = {"title":websiteName, "url":website.url,"type":website.assetType_id}>      
			
            <cfset subject = "Project Registration - #emailInfo.projectName#">
 
			<!--- HTML --->
    		<cfmail server="cudaout.media3.net"
                        username="support@wavecoders.ca"
                        from="#emailInfo.contactEmail#"
                        to="#email#"
                        subject="#subject#"
                        replyto="#emailInfo.contactEmail#"
                        type="HTML">
                        
				<cfinclude template="emailRegistration.cfm">
                
            </cfmail>
            
            <cfset success = true>
            
			<!---  --->
            
        </cfif>
      
        <cfreturn success>
        
    </cffunction>    
    
</cfcomponent>
