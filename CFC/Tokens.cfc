<cfcomponent>


<!--- Update AppVr from Tokens --->
	<cffunction name="updateAppVersion" access="public" returntype="boolean" output="yes">
      <cfargument name="auth_token" type="string" default="0" required="no">
      <cfargument name="version" type="string" default="1.0" required="no">
      
          <cfquery name="updateToken">
              UPDATE Tokens
              SET version = '#version#'
              WHERE token = '#auth_token#'
          </cfquery>
        
        <cfreturn true>
      
     </cffunction>
     
     

<!--- Clear Guest Tokens --->
	<cffunction name="cleanUpUserTokens" access="public" returntype="boolean" output="yes">
      <cfargument name="clientID" type="numeric" default="0" required="no">
      <cfargument name="appID" type="numeric" default="0" required="no">
      
      <!--- Get All Users --->
      <cfquery name="allUsers">
        
        	SELECT user_id FROM Users
            WHERE 0=0
            <cfif clientID GT '0'>
            	AND client_id = #clientID#
            </cfif>
            <cfif appID GT '0'>
            	AND app_id = #appID#
            </cfif>
 			AND access_id > 0 
 		</cfquery>
      
      
      
      <!--- Find NOT Users AND Delete Tokens --->
      <cfquery name="allTokens">
        
        	DELETE FROM Tokens
            WHERE 0=0
			<cfloop query="allUsers">
            AND (user_id <> #allUsers.user_id#)
            </cfloop>
            
 		</cfquery>
        
      
      <cfif allUsers.recordCount GT '0'>
      	<cfreturn true>
      <cfelse>
      	<cfreturn false>
      </cfif>
      
     </cffunction>


	<!--- Clear Guest Tokens --->
	<cffunction name="resetGuestTokens" access="public" returntype="boolean" output="yes">
      <cfargument name="clientID" type="numeric" default="0" required="no">
      <cfargument name="appID" type="numeric" default="0" required="no">
      <cfargument name="userID" type="numeric" default="0" required="no">
      
     	<!--- Get All Users to Delete --->
        <cfquery name="theTokens">
            SELECT        Tokens.token_id, Tokens.app_id, Users.user_id, Users.client_id
            FROM          Tokens INNER JOIN Users ON Tokens.user_id = Users.user_id
            WHERE        (Users.access_id = 0)
            <cfif clientID GT '0'>
            	AND Users.client_id = #clientID#
            </cfif>
            <cfif appID GT '0'>
            	AND Tokens.app_id = #appID#
            </cfif>
            <cfif userID GT '0'>
            	AND Users.user_id = #userID#
            </cfif>
        </cfquery>
      
		<cfif theTokens.recordCount GT '0'>
        
			<!--- Delete Tokens --->
            <cfquery name="deleteTokens">
            
                DELETE FROM Tokens
                WHERE (user_id = #theTokens.user_id#)
                
                <cfloop query="theTokens">
                OR (user_id = #theTokens.user_id#)
                </cfloop>
                
            </cfquery>
    
            <!--- Delete Users --->
            <cfquery name="deleteUsers">
            
                DELETE FROM Users
                WHERE (user_id = #theTokens.user_id#)
                
                <cfloop query="deleteTokens">
                OR (user_id = #theTokens.user_id#)
                </cfloop>
                
            </cfquery>

      		<cfreturn true>
            
		<cfelse>
        	<cfreturn false>
        </cfif>
        
	</cffunction>




<!--- Create a Guest Token --->
	<cffunction name="createGuestToken" access="remote" returntype="struct"> 
    	<cfargument name="bundleID" type="string" required="yes" default="">
       <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="deviceID" type="string" required="no" default="">
        
        <cfset data = structNew()>
      	
         	<!--- Error ok --->
              <cfinvoke component="Errors" method="getError" returnvariable="error">
                  <cfinvokeargument name="error_code" value="1000">
              </cfinvoke>
        
        	<!--- Get AppID --->
            <cfif appID GT 0>
            	<!--- all good --->
            <cfelse>
				<cfinvoke component="Apps" method="getAppID" returnvariable="appID">
					<cfinvokeargument name="bundleID" value="#bundleID#">
				</cfinvoke>
				
				<cfif appID.error.error_code IS 1000>
					<cfset appID = appID.app_ID>
				<cfelse>
					<cfset data = appID.error>
					<cfreturn data>
				</cfif>
				
			</cfif>
           
		  <!---Today Epoch--->
		  <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate"></cfinvoke>

		  <!--- Get ClientID for Guest --->
		  <cfinvoke component="Clients" method="getClientIDFromAppID" returnvariable="clientID">
			  <cfinvokeargument name="appID" value="#appID#">
		  </cfinvoke>

		  <!--- Create Guest User --->
		  <cfquery name="theUser">
			  INSERT INTO Users (name, app_id, created, modified, active, access_id, client_id)
			  VALUES ('guest', #appID#, #curDate#, #curDate#, 1, 0, #clientID#)
			  SELECT @@IDENTITY AS userID
		  </cfquery>

		  <cfset userID = theUser.userID>

		  <!--- Create Token for New User --->
		  <cfinvoke component="Tokens" method="createToken" returnvariable="token"></cfinvoke>

		  <!--- Create a New Token --->
		  <cfquery name="aToken">
			  INSERT INTO Tokens (token, app_id, created, modified, active, user_id, device_id)
			  VALUES ('#token#', #appID#, #curDate#, #curDate#, 1, #userID#, <cfif deviceID IS ''>NULL<cfelse>#deviceID#</cfif>)
			  SELECT @@IDENTITY AS ID
		  </cfquery>

		  <cfset structAppend(data,{"token":#token#})>
                  
          <cfreturn data>

	</cffunction>





<!---Create Token for USER--->
	<cffunction name="createUserToken" access="remote" returntype="struct"> <!--- returnformat="plain" output="yes"> --->
    	<cfargument name="bundleID" type="string" required="yes" default="">
        <cfargument name="appID" type="numeric" required="no" default=0>
        <cfargument name="code" type="string" required="no" default="">
        <cfargument name="pass" type="string" required="no" default="">
        <cfargument name="email" type="string" required="no" default="">
        <cfargument name="deviceID" type="string" required="no" default="">
        <cfargument name="old_token" type="string" required="no" default="">
        
        <cfset data = structNew()>
      
        <cfif bundleID IS '' AND appID IS 0>
        
        	<!--- Missing Bundle Error --->
            <cfinvoke component="Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1005">
            </cfinvoke>
            
            <cfset structAppend(data,{"error":#error#})>    
    		<cfreturn data>
        
        <cfelse>
			
           <!--- Get AppID --->
            <cfif appID GT 0>
            	<!--- appID is Good --->
            <cfelse>
            	<!--- Get AppID --->
				<cfinvoke component="Apps" method="getAppID" returnvariable="app">
					<cfinvokeargument name="bundleID" value="#bundleID#">
				</cfinvoke>
           
           		<cfif app.error.error_code IS 1000>
                	<cfset appID = app.app_id>
               	<cfelse>
               		<cfset error = app.error>
               		<cfset structAppend(data,{"error":#error#})>
        			<cfreturn data>
				</cfif>
            
			</cfif>

		  <!---Today Epoch--->
		  <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate"></cfinvoke>

		  <!--- User Exists --->
		  <cfquery name="theUser">
				SELECT	name, password, email, active, user_id
				FROM 	Users
				WHERE	0=0
				<cfif code NEQ ''>
					AND code = '#code#'
				<cfelse>
					AND email = '#email#' AND password COLLATE Latin1_General_CS_AS = '#pass#'
				</cfif>
				AND app_id = #appID# 
		  </cfquery>

		  <cfif theUser.recordCount IS 0>

				<cfif email IS '' AND pass IS ''>

					<!--- Create a guest token --->
					<cfinvoke component="CFC.Tokens" method="createGuestToken" returnvariable="data">
						<!--- <cfinvokeargument name="bundleID" value="#bundleID#"/> --->
						<cfinvokeargument name="appID" value="#appID#"/>
						<cfinvokeargument name="deviceID" value="#deviceID#"/>
					</cfinvoke>

					<!--- ok Error --->
					<cfinvoke component="Errors" method="getError" returnvariable="error">
						<cfinvokeargument name="error_code" value="1000">
					</cfinvoke>

					<cfset structAppend(data,{"token":data.token})>
					<cfset structAppend(data,{"error":error})>

				<cfelse>

					<!--- User Code Invalid Error, User not exist --->
					<cfinvoke component="Errors" method="getError" returnvariable="error">
						<cfinvokeargument name="error_code" value="1009">
					</cfinvoke>

					<!--- no token if invalid --->
					<cfset structAppend(data,{"error":error})>

				</cfif>

				<cfreturn data>

		  <cfelse>

			<!--- User Exists --->
			<cfif NOT theUser.active>

				<!--- ok Error --->
				<cfinvoke component="Errors" method="getError" returnvariable="error">
					<cfinvokeargument name="error_code" value="1004">
				</cfinvoke>

				<cfset structAppend(data,{"error":#error#})>

				<cfreturn data>

			</cfif>

			<cfset userID = theUser.user_id>

		  </cfif>

		  <!--- Get Token --->
			<cfquery name="aToken">
				SELECT	token
				FROM	Tokens
				WHERE app_id = #appID# AND user_id = #userID#
			</cfquery>

			<cfif aToken.recordCount IS 0>

				<!--- Create Token for User --->
				<cfinvoke component="Tokens" method="createToken" returnvariable="token"></cfinvoke>

				<!--- Create a New Token --->
				<cfquery name="aToken">
					INSERT INTO Tokens (token, app_id, created, modified, active, user_id, device_id)
					VALUES ('#token#', #appID#, #curDate#, #curDate#, 1, #userID#, <cfif deviceID IS ''>NULL<cfelse>#deviceID#</cfif>)
				</cfquery>

				<!--- Get Token --->
				<cfquery name="aToken">
					SELECT	token
					FROM	Tokens
					WHERE app_id = #appID# AND user_id = #userID#
				</cfquery>

				<!--- Delete Guest Token and Guest User --->
				<cfquery name="OldToken">
					SELECT        Tokens.token_id, Tokens.app_id, Users.user_id, Users.client_id
					FROM          Tokens INNER JOIN Users ON Tokens.user_id = Users.user_id
					WHERE         Tokens.token = '#old_token#'
				</cfquery>

				<cfif OldToken.user_id NEQ ''>

					<!--- Delete Guest User --->
					<cfquery name="deleteUser"> 
						DELETE FROM Users
						WHERE (user_id = #OldToken.user_id#)
					</cfquery>

					<!--- Delete Tokens --->
					<cfquery name="deleteToken">
						DELETE FROM Tokens
						WHERE (user_id = #OldToken.user_id#)
					</cfquery>

				</cfif>

			<cfelse>

				<!--- Update Token Date --->

				<cfquery name="aToken">
					UPDATE Tokens
					SET modified = #curDate# 
					WHERE app_id = #appID# AND user_id = #userID#
				</cfquery>

				<!--- Get Token --->
				<cfquery name="aToken">
					SELECT	token
					FROM	Tokens
					WHERE app_id = #appID# AND user_id = #userID#
				</cfquery>

			</cfif>

			<cfset token = aToken.token>

			<cfif token NEQ ''>

				<!--- token ok --->
				<cfinvoke component="Errors" method="getError" returnvariable="error">
					<cfinvokeargument name="error_code" value="1000">
				</cfinvoke>

				<cfset structAppend(data,{"token":#token#})>

			<cfelse>

				<!--- no token Error --->
				<cfinvoke component="Errors" method="getError" returnvariable="error">
					<cfinvokeargument name="error_code" value="1010">
				</cfinvoke>

				<cfset structAppend(data,{"token":#token#})>
				<cfset structDelete(data,"token")>

			</cfif>
        
        </cfif>
                   
        <cfset structAppend(data,{"error":#error#})>
        
        <cfreturn data>


	</cffunction>
    
    

    
    
    <!---Get Token Data--->
<!---http://localhost:8501/wavecoders_new/API/v8/CFC/liveAPI.cfc?method=getGroups&auth_token=2BC66C70-BA9B-F3FA-FECC680E3E73E619--->

    <cffunction name="getToken" access="remote" returntype="struct" output="yes">
        
        <cfargument name="auth_token" type="string" required="yes" default="0">
        
        <cfset data = structNew()>
    
            <cfquery name="token">
                SELECT        Applications.bundle_id, Users.user_id, Users.app_id, Tokens.device_id, Users.code, Users.access_id, Users.active, Users.dev
				FROM            Users RIGHT OUTER JOIN
                         Tokens ON Users.user_id = Tokens.user_id RIGHT OUTER JOIN
                         Applications ON Tokens.app_id = Applications.app_id
                WHERE 	token = '#trim(auth_token)#' AND tokens.active=1 AND Users.active=1
            </cfquery>
        
            <cfif token.recordCount GT 0>
            	<cfif token.active IS 1>
            		<cfset data = {"appID":token.app_id, "userID":token.user_id, "deviceID":token.device_id, "code":#token.code#, "access":token.access_id, "bundleID":token.bundle_id}>
                    <cfif token.dev IS 1>
						<cfset structAppend(data,{"dev":1})>
                    </cfif>
                </cfif>
			</cfif>

        <cfreturn data>
   
    </cffunction>
 
 
 
     <!---Get Token Data--->
<!---http://localhost:8501/wavecoders_new/API/v8/CFC/liveAPI.cfc?method=getGroups&auth_token=2BC66C70-BA9B-F3FA-FECC680E3E73E619--->

    <cffunction name="getAuthToken" access="remote" returntype="query" output="yes">
        
        <cfargument name="userID" type="string" required="yes" default="0">
        <cfargument name="appID" type="string" required="yes" default="0">
               
            <cfquery name="tokens">
                SELECT	token
                FROM 	tokens
                WHERE 	0 = 0
                <cfif userID GT 0>
                AND user_id = #userID#
                </cfif>
                <cfif appID GT 0>
                AND app_id = #appID#
                </cfif>
            </cfquery>

        <cfreturn tokens>
   
    </cffunction>   
    

            
    
    
    
<!---Create Token--->
    <cffunction name="createToken" access="public" returntype="string" output="no">
        
        <cfset token = CreateUUID()>
        
		<cfreturn token>
        
	</cffunction>
    
    
    
    
    
<!---Is Token Valid--->
    <cffunction name="tokenValid" access="remote" returntype="boolean">
        <cfargument name="auth_token" type="string" required="yes">
        
        <cfset data = structNew()>
        
        <!--- Get Token --->
        <cfinvoke component="Tokens" method="getToken" returnvariable="tokenExists">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>

        <cfif structCount(tokenExists) GT 0>
			
			<!---Token Exists--->
            <cfinvoke component="Misc" method="convertDateToEpoch" returnvariable="curDate"></cfinvoke>
            
            <cfquery name="updateToken">
            	UPDATE 	tokens
                SET 	modified = #curDate#
                WHERE 	token = '#auth_token#'
            </cfquery>
         
         	<cfreturn true>
         
        <cfelse>
        
           <cfreturn false>
           
        </cfif>
    
    </cffunction>




 <!---Get User Tokens--->
    <cffunction name="getTotalUserTokens" access="remote" returntype="numeric">
        <cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="userEmail" type="string" required="no" default="">
        <cfargument name="appID" type="numeric" required="yes" default="0">
        
        <cfset cntTokens = 0>
        
        <!--- Get Tokens --->
        <cfif userID GT '0' OR userEmail NEQ ''>
            <cfinvoke component="Tokens" method="getUserTokens" returnvariable="tokens">
                <cfinvokeargument name="userID" value="#userID#">
                <cfinvokeargument name="userEmail" value="#userEmail#">
                <cfinvokeargument name="appID" value="#appID#">
            </cfinvoke>

			<cfset cntTokens = tokens.recordCount>	
            
        </cfif>
           
		<cfreturn cntTokens>
        
	</cffunction>
    
    
 <!---Get User Tokens--->
    <cffunction name="getUserTokens" access="remote" returntype="query">
        <cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="userEmail" type="string" required="no" default="">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="active" type="boolean" required="no" default="true">
        
        <cfif active><cfset active = 1><cfelse><cfset active = 0></cfif>
        
         <cfquery name="tokens">
            SELECT       Users.name, Users.email, Users.access_id, Tokens.token, Tokens.app_id, Tokens.active, Tokens.user_id, Tokens.device_id, Tokens.modified
			FROM         Tokens INNER JOIN
                         Users ON Tokens.user_id = Users.user_id
            WHERE 0=0
            
            <cfif userID GT '0'>
            	AND Tokens.user_id = #userID#
            </cfif>
            <cfif userEMail NEQ ''>
            	AND Users.email = '#userEmail#'
            </cfif>
            <cfif appID GT '0'>
            	AND Tokens.app_id = #appID#
            </cfif>
            <cfif active IS 1>
            	AND Tokens.active = #active#
            </cfif>
            ORDER BY Tokens.modified
        </cfquery>
  
		<cfreturn tokens>
        
	</cffunction>
        
    
    
 <!---Get All Tokens--->
    <cffunction name="getAllTokens" access="remote" returntype="query">
        <cfargument name="appID" type="numeric" required="no" default="0">
        
       <cfinvoke component="Tokens" method="getUserTokens" returnvariable="tokens">
           <cfinvokeargument name="appID" value="#appID#">
       </cfinvoke>
       
       <cfreturn tokens>
        
	</cffunction>
    
    
    
    
    
    
  	<!---Get ClientID, AppID and UserID from Token --->
    <cffunction name="getTokenInfo" access="remote" returntype="struct">
        <cfargument name="auth_token" type="string" required="yes">
        
        <cfquery name="token">  
            SELECT        Tokens.token, Applications.app_id AS appID, Applications.client_id AS clientID, Tokens.user_id AS userID
            FROM            Tokens INNER JOIN
                                     Applications ON Tokens.app_id = Applications.app_id
            WHERE        (Tokens.token = '#auth_token#')
        </cfquery>
        
        <cfset info = {"userID":token.userID,"clientID":token.clientID,"appID":token.appID}>
        
        <cfreturn info>
        
    </cffunction>
    
    
    
    
    
    


 	<!---Get Tokens ID--->
    <cffunction name="getTokenID" access="remote" returntype="struct">
        
        <cfargument name="auth_token" type="string" required="yes">
        
          <cfquery name="token">
              SELECT	token_id, user_id, device_id, app_id
              FROM 		tokens
              WHERE 	token = '#trim(auth_token)#'
          </cfquery>
          
          <cfset data = structNew()>
          
          <cfif token.recordCount GT '0'> 
            <cfset data = {"token_id":token.token_id, "user_id":token.user_id, "device_id":token.device_id, "app_id":token.app_id}>
          </cfif>
            
          <cfreturn data>
        
	</cffunction>



<!--- Set User Prefs --->
    <cffunction name="setUserPrefs" access="remote" returntype="string" returnformat="plain" output="yes">
    	<cfargument name="auth_token" type="string" required="yes">
        <!--- info --->
        <cfargument name="email" type="string" required="no" default="">
        <cfargument name="name" type="string" required="no" default="">
		<cfargument name="password" type="string" required="no" default="">
        
        
			<!--- Get User ID from Auth Token --->
            <cfinvoke component="Tokens" method="getToken" returnvariable="userInfo">
               <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>
   
            <!--- User Exists --->
            <cfinvoke component="Users" method="userExists" returnvariable="userExists">
               <cfinvokeargument name="userID" value="#userInfo.userID#">
               <cfinvokeargument name="code" value="#userInfo.code#">
            </cfinvoke>
            
            <cfif userExists>
            
            <cfif email NEQ '' OR name NEQ '' OR password NEQ ''>
            
				<!--- Update User Data --->
                <cfquery name="userInfoUpdated">
                    UPDATE Users
                    SET code = #userInfo.code#
                    <cfif email NEQ ''>
                    , email = '#email#'
                    </cfif>
                    <cfif password NEQ ''>
                    , password = '#password#'
                    </cfif>
                    <cfif name NEQ ''> 
                    , name = '#name#'
                    </cfif>
                    
                    WHERE app_id = #userInfo.appID# AND user_id = #userInfo.userID#
                </cfquery>
            
            </cfif>
                
                <!---OK--->
                <cfinvoke component="Errors" method="getError" returnvariable="errorStruct">
                    <cfinvokeargument name="error_code" value="1000">
                </cfinvoke>
            
            <cfelse>
            	<!---User Not Found--->
                <cfinvoke component="Errors" method="getError" returnvariable="errorStruct">
                    <cfinvokeargument name="error_code" value="1009">
                </cfinvoke>
            </cfif>
        
        <cfset data['error'] = errorStruct>
        
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
        
	</cffunction>
    
    
 
 
 
 
 	<!--- Token String Valid --->
    <cffunction name="tokenStringValid" access="remote" returntype="boolean">
    
    	<cfargument name="token" type="string" required="yes">
 	
        <!--- Check String Length --->
    	<cfif len(token) GTE len("B427477E-9119-E936-9A50B1F5622BED3E")>
        
            <cfinvoke component="Misc" method="findAll" returnvariable="delimiters">
                <cfinvokeargument name="stringToSearch" value="#token#"/>
                <cfinvokeargument name="valueToFind" value="-"/>
            </cfinvoke>
            
            <cfset tokenDelimiters = arrayLen(delimiters.len)>
         
            <cfif tokenDelimiters GTE 3 OR tokenDelimiters LTE 5>
            	<cfreturn true>
            <cfelse>
            	<cfreturn false>
            </cfif>
        
        <cfelse>  
    		<cfreturn false>
        </cfif>
    
    
    </cffunction>
    
    
</cfcomponent>
