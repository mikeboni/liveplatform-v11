<cfcomponent>
	
	<cffunction name="getAccessLevels" access="remote" returntype="string" returnformat="plain" output="no">
		
		<cfset data = structNew()>
		
		<cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
		
		<cfquery name="accessLevels"> 
            SELECT      access_id AS id, levelName AS name, icon, accessLevel AS access
              
			FROM        AccessLevels
       		ORDER BY 	accessLevel 
        </cfquery>
		
		<cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="levels">
            <cfinvokeargument name="query" value="#accessLevels#"/>
         </cfinvoke>
		
		<cfset structAppend(data,{"data":#levels#})>
		
		<cfheader name="Access-Control-Allow-Origin" value="*">
   	
    	<cfset structAppend(data,{"error":#error#})>
    	<cfset JSON = serializeJSON(data)>
    	
		<cfreturn JSON>
		
	</cffunction>
	
	
	
	
	<cffunction name="getProducts" access="remote" returntype="string" returnformat="plain" output="no">
		
		<cfset data = structNew()>
		
		<cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
		
		<cfquery name="productTypes"> 
            SELECT      liveinventory.dbo.ProductTypes.productType_id AS id,
                        liveinventory.dbo.ProductTypes.productType AS type
              
			FROM        liveinventory.dbo.ProductTypes
        </cfquery>
		
		<cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="products">
            <cfinvokeargument name="query" value="#productTypes#"/>
         </cfinvoke>
		
		<cfset structAppend(data,{"data":#products#})>
		
		<cfheader name="Access-Control-Allow-Origin" value="*">
   	
    	<cfset structAppend(data,{"error":#error#})>
    	<cfset JSON = serializeJSON(data)>
    	
		<cfreturn JSON>
		
	</cffunction>
	
	
	
	
	<cffunction name="getTypes" access="remote" returntype="string" returnformat="plain" output="no">
		<cfargument name="type" type="numeric" required="no" default="-1">
		
		<cfset data = structNew()>
		
		<cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
		
		<cfquery name="areaTypes"> 
            SELECT      liveinventory.dbo.AreaTypes.areaType_id AS id, 
                        liveinventory.dbo.AreaTypes.areaType AS type, 
                        liveinventory.dbo.AreaTypes.icon AS icon
                        <cfif type IS -1>
                        ,liveinventory.dbo.ProductTypes.productType AS product
						</cfif>
			FROM        liveinventory.dbo.AreaTypes LEFT OUTER JOIN
                        liveinventory.dbo.ProductTypes ON liveinventory.dbo.AreaTypes.productType_id = liveinventory.dbo.ProductTypes.productType_id
                        <cfif type NEQ -1>
			WHERE       liveinventory.dbo.AreaTypes.productType_id = #type#
						</cfif>
            ORDER BY 	liveinventory.dbo.ProductTypes.productType_id DESC
            
        </cfquery>
		
		<cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="info">
            <cfinvokeargument name="query" value="#areaTypes#"/>
         </cfinvoke>
		
		<cfset structAppend(data,{"data":#info#})>
		
		<cfheader name="Access-Control-Allow-Origin" value="*">
   	
    	<cfset structAppend(data,{"error":#error#})>
    	<cfset JSON = serializeJSON(data)>
    	
		<cfreturn JSON>
		
	</cffunction>
	
	
	
	
	
	<cffunction name="getOptions" access="remote" returntype="string" returnformat="plain" output="no">
		<cfargument name="type" type="numeric" required="no" default="-1">
		
		<cfset data = structNew()>
		
		<cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
		
		<cfquery name="areaTypes">
			SELECT		liveinventory.dbo.Options.option_id AS id, 
						liveinventory.dbo.Options.name, 
						liveinventory.dbo.Options.cost, 
						liveinventory.dbo.Options.size
						<cfif type IS -1>
						,liveinventory.dbo.ProductTypes.productType AS product
						</cfif>
			FROM        liveinventory.dbo.Options INNER JOIN
						liveinventory.dbo.ProductTypes ON liveinventory.dbo.Options.productType_id = liveinventory.dbo.ProductTypes.productType_id
			<cfif type NEQ -1>
			WHERE       (liveinventory.dbo.Options.productType_id = #type#)
			</cfif>
		</cfquery>

			<cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="options">
				<cfinvokeargument name="query" value="#areaTypes#"/>
			 </cfinvoke>
			
			<cfset structAppend(data,{"data":#options#})>

			<cfheader name="Access-Control-Allow-Origin" value="*">

			<cfset structAppend(data,{"error":#error#})>
			<cfset JSON = serializeJSON(data)>

			<cfreturn JSON>
			
	</cffunction>
	
	
	
	<cffunction name="createBreadcrumb" access="remote" returntype="array">
		<cfargument name="contentID" type="numeric" required="no" default="0">
		
		<cfset crumbs = []>
		
		<!--- Crumb --->
        <cfloop condition="contentID GT 0">
        
            <cfquery name="crumb">
				SELECT	 	content_id AS id, parent_id, assetType_id AS type
				FROM 		liveinventory.dbo.Contents
				WHERE	 	parent_id = #contentID#
			</cfquery>
			
			<cfinvoke component="API.V11.CFC.Misc" method="QueryToStruct" returnvariable="content">
				<cfinvokeargument name="query" value="#crumb#"/>
			</cfinvoke>
		
			<cfset id = crumb.id>
			<cfset type = crumb.type>
 			
 			<!--- find type to get DB Table --->
			<cfinvoke  component="liveinventory" method="getAssetType" returnvariable="contentType">
				<cfinvokeargument name="type" value="#type#"/>
			</cfinvoke>
			
			<cfinvoke  component="cfc.#contentType#" method="getObject" returnvariable="contentData">
				<cfinvokeargument name="props" value="#content#"/>
			</cfinvoke>
                 
            <cfset arrayAppend(crumbs,{"name":contentData.name,"id":id})>
            
            <cfset contentID = id>

        </cfloop>
		
		<cfreturn crumbs>
		
	</cffunction>
		
	
	
	
 	<cffunction name="updateInventory" access="remote" returntype="string" returnformat="plain" output="no">
		<cfargument name="content" type="array" required="no" default="#arrayNew(1)#">
		<cfargument name="appID" type="string" required="yes">
		
		<cfset data={}>
		
		<cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
		
		<!--- SUBMIT OBJECTS TO CREATE/UPDATE/DELETE --->
		<cfloop index="obj" array="#content#">
			<cfdump var="#obj#"><br>
		</cfloop>

		<cfheader name="Access-Control-Allow-Origin" value="*">
    	<cfset structAppend(data,{"error":#error#})> 
    	<cfset JSON = serializeJSON(data)>
    	
		<cfreturn JSON>
		
	</cffunction>
	
	
	<cffunction name="getInventory" access="remote" returntype="string" returnformat="plain" output="no">
		<cfargument name="contentID" type="numeric" required="no" default="0">
		<cfargument name="appID" type="string" required="no" default="100">
		
		<cfif contentID IS 0>
	 	
	 		<cfquery name="contentRoot">
				SELECT 		content_id AS id
				FROM 		liveinventory.dbo.Contents
				WHERE 		app_id = #appID# AND (parent_id = #contentID#)
			</cfquery>
			
			<cfset contentID = contentRoot.id>
			
		</cfif>
			
		<cfset data={}>
		
		<cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
		
		<cfquery name="groupContent">
			SELECT	 	content_id AS id, asset_id AS assetID, assetType_id AS type, modified AS date, sortorder AS [order], name, active, accesslevel AS access
			FROM 		liveinventory.dbo.Contents
			WHERE	 	app_id = #appID# AND (parent_id = #contentID# OR content_id = #contentID#)
			ORDER BY	parent_id, sortorder ASC
		</cfquery>
		
		<cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="content">
			<cfinvokeargument name="query" value="#groupContent#"/>
		</cfinvoke>
		 
		<cfif isStruct(content)>
			<cfset content = [content]>
		</cfif>
			 
		<cfset structAppend(data,{"data":#content#})>
			
		<cfheader name="Access-Control-Allow-Origin" value="*">
    	<cfset structAppend(data,{"error":#error#})> 
    	<cfset JSON = serializeJSON(data)>
    	
		<cfreturn JSON>
		
	</cffunction>
	
	
	<cffunction name="deleteData" access="remote" returntype="string" returnformat="plain" output="no">     
	<cfargument name="contentID" type="numeric" required="no" default="0">
	    
	    <cfinvoke  component="API.V11.CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <cfset result={}>
       
      	<!--- Rec Exists? --->
		<cfinvoke  component="liveInventory" method="foundRec" returnvariable="recExists">
            <cfinvokeargument name="id" value="#contentID#"/>
        </cfinvoke>
    
        <cfif recExists>
			
			<!--- GET ASSET ID --->
			<cfinvoke  component="liveInventory" method="getRec" returnvariable="foundRec">
				<cfinvokeargument name="id" value="#contentID#"/>
			</cfinvoke>

			<!--- DELETE from ASSET --->
		
			<cfinvoke  component="liveInventory" method="deleteAsset" returnvariable="deleteOldAsset">
				<cfinvokeargument name="data" value="#foundRec#"/>
			</cfinvoke>

			<!--- find type to get DB Table and insert record --->
			<!--- <cfinvoke  component="content" method="getAssetType" returnvariable="type">
				<cfinvokeargument name="type" value="#foundRec.type#"/>
			</cfinvoke> --->

			<!--- UPDATE --->
			<!--- <cfinvoke  component="cfc.#type#" method="delete" returnvariable="success">
				<cfinvokeargument name="props" value="#foundRec#"/>
			</cfinvoke> --->
			
			<!--- DELETE from Content --->
			<cfquery name="deleteContent">
				DELETE FROM		liveinventory.dbo.Contents

				WHERE			content_id = #contentID#
			</cfquery>
		
		<cfelse>
		
			<!--- No Record Found --->
        	<cfinvoke  component="API.V11.CFC.Errors" method="getError" returnvariable="error">
				<cfinvokeargument name="error_code" value="5000"/><!--- Record Not Found --->
			</cfinvoke>
				
		</cfif>
		
		<cfset structAppend(result,{"error":#error#})>
		
		<cfset JSON = serializeJSON(result)>
    	
		<cfreturn JSON>
			
	</cffunction>
		
		
	<!--- DELETE Asset --->
	
	<cffunction name="deleteAsset" access="public" returntype="any" output="yes">
		<cfargument name="data" type="struct" required="yes">
        
		<!--- find type to get DB Table and insert record --->
		<cfinvoke  component="liveInventory" method="getAssetType" returnvariable="type">
			<cfinvokeargument name="type" value="#data.type#"/>
		</cfinvoke>

		<!--- UPDATE --->
		<cfinvoke  component="#type#" method="delete" returnvariable="success">
			<cfinvokeargument name="props" value="#data#"/>
		</cfinvoke>
		
		<cfreturn success>
		
	</cffunction>
	
	
	<!--- Record Exists --->
	
	<cffunction name="getRec" access="public" returntype="any">
		<cfargument name="id" type="numeric" required="yes">
		
		<cfquery name="found">
			 SELECT 	content_id AS id, asset_id AS assetID, assetType_id AS type
			 FROM		liveinventory.dbo.Contents
	
             WHERE		content_id = #id#
		</cfquery>

		<cfreturn {"assetID": found.assetID, "type": found.type}>

	</cffunction>
	
	
	
	<!--- Record Exists --->
	
	<cffunction name="foundRec" access="public" returntype="boolean">
		<cfargument name="id" type="numeric" required="yes">
		
		<cfquery name="found">
			 SELECT 	content_id
			 FROM		liveinventory.dbo.Contents
	
             WHERE		content_id = #id#
		</cfquery>
		
		<cfif found.recordCount GT 0>
			<cfreturn true>
		<cfelse>
			<cfreturn false>
		</cfif>

		
	</cffunction>
	
	
	<!--- find type to get DB Table and insert record --->
	
	<cffunction name="getAssetType" access="public" returntype="string">
		<cfargument name="type" type="numeric" required="yes">
		
		<cfset dbTable = "">
		
		<cfquery name="dbTableData">
			SELECT	 	dbTable
			FROM 		liveinventory.dbo.AssetTypes
			WHERE	 	assetType_id = #type#
		</cfquery>
	
		<cfif dbTableData.recordCount GT 0>
			<cfset dbTable = dbTableData.dbTable>
		</cfif>
			
		<cfreturn dbTable>
		
	</cffunction>
	
	
	
	<cffunction name="getIDFromRefID" access="remote" returntype="array" output="no">
		<cfargument name="appID" type="numeric" required="yes">
		<cfargument name="refID" type="numeric" required="no" default="0">

		<!--- find token --->
		<cfquery name="foundRefObj">
			SELECT 		content_id
			FROM 		liveplatform.dbo.contents
			WHERE 		ref_id = #refID# AND app_id = #appID#
		</cfquery>

		<cfset content = []>

		<cfif foundRefObj.recordCount GT 0>

			<cfinvoke component="API.V11.CFC.Misc" method="QueryToStruct" returnvariable="content">
				<cfinvokeargument name="query" value="#foundRefObj#"/>
				<cfinvokeargument name="forceArray" value="true"/>
			</cfinvoke>

		</cfif>

		<cfreturn content>

	</cffunction>
	
</cfcomponent>


