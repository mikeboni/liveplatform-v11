<cfquery name="RSVPDates"> 
	SELECT DISTINCT date
	FROM   RSVP
	WHERE app_id = #session.appID#
</cfquery>

<cfquery name="RSVPInvitess"> 
	SELECT rsvp_id, name, email, confirm_rsvp, confirmedDate
	FROM   RSVP
	WHERE app_id = #session.appID#
	ORDER BY date
</cfquery>


<link href="styles.css" rel="stylesheet" type="text/css" />

<style>
.sqButton
	{ 
	border: 1px solid #666;  
	height:32px;
	background-color:#FFF;
	padding-top:2px;
	}
</style>

<script language="javascript">

function setConfirm(id, state) {
	console.log(id, state)
}
	
function resendInvite(id) {
	console.log(id)
}
	
function saveUser(name, email) {
	console.log(name, email)
}

</script>

<cfset selectedDate = ''>

<cfoutput>

<div class="contentLinkGrey" style="padding: 8px">

<cfif RSVPDates.recordCount IS 1>
	<cfset selectedDate = RSVPDates.date>
</cfif>

<cfif RSVPDates.recordCount GT 0>
	FILTER:
	<select>
		<cfloop query="RSVPDates"> 
		
		<cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="theDate">
			<cfinvokeargument name="TheEpoch" value="#RSVPDates.date#"/>
		</cfinvoke>
		
			<option>#dateFormat(theDate,'MMM DD, YYYY')#- #timeFormat(theDate,'HH:MM tt')#</option>
			
		</cfloop>
	</select>
	<input type="button" class="formfieldcontent" value="Create New Event" style="height: 32px">
<cfelse>
	No Events Available:
	<input type="button" class="formfieldcontent" value="Create New Event" style="height: 32px">
</cfif>
	<input type="button" class="formfieldcontent" value="Add New Invite" style="height: 32px">
</div>	

<div class="contentLinkGrey" style="padding:10px 10px 10px 10px; width:800px">

<div class="contentLinkGrey" id="newUser" style="clear:left; height: 100px">
	<div style="height: 20px; background-color: ##eee; padding: 5px">Create New User</div>
	<div style="margin: 10px">
	<form>
	<label>Name:</label><input name="name" type="text" class="formfieldcontent" id="name" style="width:200px" value="">
	<label style="margin-left:10px">EMail:</label><input name="email" type="text" class="formfieldcontent" id="email" style="width:300px" value="">
	<input type="button" class="formfieldcontent" value="Save" style="height: 32px; width: 60px; float: right" onClick="saveUser(this.form.name.value, this.form.email.value)">
	</form>
	</div>
</div>

<table width="800" border="0" cellpadding="5" cellspacing="1">
  <tbody>
    <tr class="contentGreyed">
      <td height="32" bgcolor="##7A7A7A">Name</td>
      <td bgcolor="##7A7A7A">Email</td>
      <td bgcolor="##7A7A7A">Date Confirmed</td>
      <td align="center" bgcolor="##7A7A7A">Resend</td>
      <td align="center" bgcolor="##7A7A7A">Confirmed</td>
    </tr>
    
    <cfset totalInvites = RSVPInvitess.recordCount>
    
    <cfloop query="RSVPInvitess"> 
    
    <cfif RSVPInvitess.confirmedDate NEQ ''>
    <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="theDate">
		<cfinvokeargument name="TheEpoch" value="#RSVPInvitess.confirmedDate#"/>
	</cfinvoke>
		<cfset date = "#dateFormat(theDate,'MMM DD, YYYY')# - #timeFormat(theDate,'HH:MM tt')#">
	<cfelse>
		<cfset date = 'NA'>
	</cfif>
   
    <tr class="contentLinkGrey">
      <td>#RSVPInvitess.name#</td>
      <td>#RSVPInvitess.email#</td>
      <td>#date#</td>
		<td align="center"><a onClick="resendInvite(#RSVPInvitess.rsvp_id#)"><img src="images/resend.png"></a></td>
      <td align="center">
		<cfif RSVPInvitess.confirm_rsvp IS 1>
			<span style="color:forestgreen; cursor: pointer"><a onClick="setConfirm(#RSVPInvitess.rsvp_id#, 0)">YES</a><span/>
		<cfelse>
			<span style="color:red; cursor: pointer"><a onClick="setConfirm(#RSVPInvitess.rsvp_id#,1)">NO</a><span/>
			<cfset totalInvites-->
		</cfif>
      </td>
    </tr>
	</cfloop>
	<tr class="contentLinkGrey">
	  <td height="5" colspan="5" align="right"></td>
	  </tr>
	<tr class="contentLinkGrey">
      <td height="32" colspan="4" align="right" bgcolor="##eee" style="padding-right: 8px;">Total Invites</td>
      <td align="center" bgcolor="##eee">#totalInvites#</td>
      </tr>
  </tbody>
</table>

</div>

</cfoutput>	

