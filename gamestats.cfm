<cfparam name="bundleID" default="com.liveadz.pennwellar">

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
</head>

<body>

<link href="styles.css" rel="stylesheet" type="text/css" />

<style>
    #container { display:block; margin:0; padding:0; width:800px; height:44px;}
	#left-element { float:left; display:block; width:50px; height:44px;}
	#middle-element { float:left; display:block; margin-left:10px; padding-top:10px; width:250px; height:34px;}
	#right-element { float:left; display:block; margin-left:10px; padding-top:10px;; width:160px; height:34px;}
	#done-element { float:left; display:block; margin-left:10px; padding-top:10px;; width:80px; height:34px;}
</style>

<cfinvoke  component="CFC.Games" method="getAllGames" returnvariable="games">
    <cfinvokeargument name="bundleID" value="#bundleID#"/>
</cfinvoke>

<cfoutput query="games">
  
    <cfinvoke  component="CFC.Misc" method="convertEpochToDate" returnvariable="startDate">
        <cfinvokeargument name="TheEpoch" value="#games.dateStart#"/>
    </cfinvoke>
    
    <cfinvoke  component="CFC.Misc" method="convertEpochToDate" returnvariable="endDate">
        <cfinvokeargument name="TheEpoch" value="#games.dateEnd#"/>
    </cfinvoke>
    
    <div id="container" style="padding-top:10px;">
    <span class="bannerHeading">#name#</span><br />
    </div>
    <div id="container" style="padding-top:10px;">
    <div id="left-element" style="width:400px">
    	<span class="contentLinkGrey">#dateFormat(startDate,'DDDD, MMMM DD, YYYY')# - #dateFormat(endDate,'DDDD, MMMM DD, YYYY')#</span>
    </div>
    
   <cfinvoke  component="CFC.Games" method="getGameUsers" returnvariable="gameUsers">
     <cfinvokeargument name="gameID" value="#asset_id#"/>
   </cfinvoke>
    
    <div id="right-element" style="width:100px; float:right">
    	<span class="contentLinkGrey">Total Players #gameUsers.recordCount#</span>
    </div>
    </div>
    
    <cfset gameID = asset_id>



  <cfloop query="gameUsers">
    
    <cfinvoke  component="CFC.Games" method="getGameUserAssetCount" returnvariable="userGame">
      <cfinvokeargument name="gameID" value="#gameID#"/>
      <cfinvokeargument name="userID" value="#user_id#"/>
    </cfinvoke>
    
    
    <div style="background-color:##EEE; width:800px; padding-bottom:5px; margin-bottom:10px">
        <cfinvoke  component="CFC.Games" method="getGameUserAssets" returnvariable="gameInfo">
            <cfinvokeargument name="gameID" value="#gameID#"/>
            <cfinvokeargument name="userID" value="#user_id#"/>
        </cfinvoke>

        <div id="container" style="margin-top:10px; background-color:##666">
            <div id="middle-element" style="width:500px"><a href="mailto:#email#" class="contentLinkWhite" style="color:##EEE">#name#</a> <span class="contentLinkGrey">(#dateFormat(gameInfo.date,'DDDD, MMMM DD, YYYY')#)</span></div>
          <div class="contentLinkGrey" id="done-element" style="width:110px;color:##CCC">#country#</div> 
            <div class="contentLinkGrey" id="right-element" style="padding:0">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" class="contentLinkWhite" style="color:##CCC">Completed</td>
                <td><cfif gameInfo.done><img src="images/checkmark-selected.png" /><cfelse><img src="images/checkmark-normal.png" /></cfif></td>
              </tr>
            </table>
            </div>
        </div>
        
    <cfloop index="collectedObject" array="#gameInfo.assets#">

			<!--- Date --->
            <cfinvoke  component="CFC.Misc" method="convertEpochToDate" returnvariable="objDate">
                <cfinvokeargument name="TheEpoch" value="#collectedObject.date#"/>
            </cfinvoke>
        
        	<cfset imageURL = collectedObject.url>
        	<cfset objName = collectedObject.title>
          
          	<cfset theDate = TimeFormat(objDate, "hh:mm:sstt")>
            
          <div id="container" style="margin:4px; padding:4px;">
            <div id="left-element"><img src='#imageURL#' width="44" height="44" border="1" /></div>
            <div class="contentLink" id="middle-element" style="width:550px">#objName#</div>
                <div class="contentLinkGrey" id="right-element">#dateFormat(objDate,'DDD, DD')# at #theDate#</div>
          </div>
        	
            
        </cfloop>
    </div>    
        
        
  </cfloop>

</cfoutput>

</body>
</html>