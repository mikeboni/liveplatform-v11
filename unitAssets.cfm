<cfparam name="subgroupID" default="0">
<cfparam name="unitID" default="0">

<cfajaxproxy cfc="CFC.Units" jsclassname="units">

<script type="text/javascript" value="Create Group">
<cfoutput>
	var jsUnits = new units();
	
	
	//------------------GROUPS
	
	//edit Group
	function editGroupName(subgroupID)
	{
		var theForm = document.getElementById("G_"+subgroupID);
		var theField = theForm.groupNameEdit;
		
		var theText = document.getElementById('groupName_'+subgroupID);
		
		var theEditMode = document.getElementById('editGroup_'+subgroupID);
		var theViewMode = document.getElementById('viewGroup_'+subgroupID);
		
		theField.style.display = 'block';
		theText.style.display = 'none';
		
		theEditMode.style.display = 'block';
		theViewMode.style.display = 'none';
		
	}
	
	//add group
	function createGroup(subgroupID,groupName)
	{
		if(groupName === ''){
			alert("No Group Name has been Defined");
		}else{
			jsUnits.setSyncMode();
			jsUnits.setCallbackHandler(successGroupAdded);
			jsUnits.createGroup(#session.appID#, subgroupID, groupName);
		}
	}
	
	function successGroupAdded(result)
	{
		location.reload();
	}
	
	//delete group
	function deleteGroup(subgroupID)
	{
		
		if(confirm('Are you sure you wish to Delete this Group?'))
		{
			jsUnits.setSyncMode();
			jsUnits.setCallbackHandler(successGroupDeleted);
			jsUnits.deleteGroup(#session.appID#,subgroupID);
			
		}else{ 
			//nothing
		}
		
	}
	
	function successGroupDeleted(result)
	{
		location.reload();
	}
	
	
	//update group
	function updateGroup(subgroupID)
	{
		var theForm = document.getElementById("G_"+subgroupID);
		var theNewGroupName = theForm.groupNameEdit.value;
		
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successGroupUpdated);
		jsUnits.updateGroup(#session.appID#,subgroupID,theNewGroupName);
		
	}
	
	function successGroupUpdated(theResult)
	{
		
		var subgroupID = theResult.subgroupID;
		var groupName = theResult.groupName;
		
		var theForm = document.getElementById("G_"+subgroupID);
		var theField = theForm.groupNameEdit;
		
		var theText = document.getElementById('groupName_'+subgroupID);
		
		var theEditMode = document.getElementById('editGroup_'+subgroupID);
		var theViewMode = document.getElementById('viewGroup_'+subgroupID);
		
		theField.style.display = 'none';
		theText.style.display = 'block';
		
		if(groupName != ''){
			theText.innerHTML = groupName;
		}
		
		theEditMode.style.display = 'none';
		theViewMode.style.display = 'block';
	}
	
	//Cancel Group Edit
	function cancelGroupEdit(subgroupID)
	{
		
		var theText = document.getElementById('groupName_'+subgroupID);
		var theGroupName = theText.innerHTML;
		
		var theForm = document.getElementById("G_"+subgroupID);
		theForm.groupNameEdit.value = theGroupName;
		
		var theObj = {"subgroupID":subgroupID, "groupName":""}
		
		successGroupUpdated(theObj);
	}
	
	
	//move group
	function moveItem(groupID,subgroupID)
	{
		
		if(confirm('Are you sure you wish to Move this Group?'))
		{
			jsUnits.setSyncMode();
			jsUnits.setCallbackHandler(successGroupMoved);
			jsUnits.moveGroup(#session.appID#,groupID,subgroupID);
			
		}else{ 
			//nothing
		}
			
	}
	
	function successGroupMoved(result)
	{
		location.reload();
	}
	
	
	
	
	//--------------------UNITS
	
	//edit unit
		function editUnitName(unitID)
	{
		var theForm = document.getElementById("U_"+unitID);
		var theField = theForm.unitNameEdit;
		
		var theText = document.getElementById('unitName_'+unitID);
		
		var theEditMode = document.getElementById('editUnit_'+unitID);
		var theViewMode = document.getElementById('viewUnit_'+unitID);
		
		theField.style.display = 'block';
		theText.style.display = 'none';
		
		theEditMode.style.display = 'block';
		theViewMode.style.display = 'none';
		
	}
	
	//delete unit
	function deleteUnit(unitID)
	{
		
		if(confirm('Are you sure you wish to Delete this Unit?'))
		{
			jsUnits.setSyncMode();
			jsUnits.setCallbackHandler(successUnitDeleted);
			jsUnits.deleteUnit(unitID);
			
		}else{ 
			//nothing
		}
		
	}
	
	function successUnitDeleted(result)
	{
		location.reload();
	}
	
	//update unit
	function updateUnit(unitID)
	{
		var theForm = document.getElementById('U_'+unitID);
		var theNewUnitName = theForm.unitNameEdit.value;
		
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successUnitUpdated);
		jsUnits.updateUnit(#session.appID#,unitID,theNewUnitName);
		
	}
	
	function successUnitUpdated(theResult)
	{
		
		var unitID = theResult.unitID;
		var unitName = theResult.unitName;
		
		var theForm = document.getElementById("U_"+unitID);
		var theField = theForm.unitNameEdit;
		
		var theText = document.getElementById('unitName_'+unitID);
		
		var theEditMode = document.getElementById('editUnit_'+unitID);
		var theViewMode = document.getElementById('viewUnit_'+unitID);
		
		theField.style.display = 'none';
		theText.style.display = 'block';
		
		if(unitName != ''){
			theText.innerHTML = unitName;
		}
		
		theEditMode.style.display = 'none';
		theViewMode.style.display = 'block';
	}
	
	//Cancel Unit Edit
	function cancelUnitEdit(subgroupID)
	{
		var theText = document.getElementById('unitName_'+subgroupID);
		var theGroupName = theText.innerHTML;
		
		var theForm = document.getElementById("U_"+subgroupID);
		theForm.unitNameEdit.value = theGroupName;
		
		var theObj = {"unitID":subgroupID, "unitName":""}
		
		successUnitUpdated(theObj);
	}
	
	//move unit
	function moveUnitItem(unitID,groupID)
	{
		
		if(confirm('Are you sure you wish to Move this Unit?'))
		{
			jsUnits.setSyncMode();
			jsUnits.setCallbackHandler(successUnitMoved);
			jsUnits.moveUnit(unitID,groupID);
			
		}else{ 
			//nothing
		}
			
	}
	
	function successUnitMoved(result)
	{
		location.reload();
	}
	
	//Create Unit
	function createUnit(groupID, unitName)
	{
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successUnitCreated);
		jsUnits.createUnit(groupID,unitName);
	}
	
	function successUnitCreated(result)
	{
		location.reload();
	}
	
	
	//Update Unit Assignment
	function updateAssignment(unitID, userID, custID)
	{
		if( custID == 0 ){
			userID = userID.options[userID.selectedIndex].value;
		}else{
			custID = custID.options[custID.selectedIndex].value;
		}
		
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successAssignmentUpdated);
		jsUnits.updateUnitAssignment(unitID,userID, custID);
	}
	
	
	function successAssignmentUpdated(result)
	{
		if(result == 'user'){
			document.getElementById("editGroupUser").src = "images/saveupdate.png";
		}
		if(result == 'cust'){
			document.getElementById("editGroupCust").src = "images/saveupdate.png";
		}
		
		setTimeout(function(){
			document.getElementById("editGroupUser").src = "images/replace.png";
			document.getElementById("editGroupCust").src = "images/replace.png";
		}, 1000);
		
	}
	
	//Edit Unit Details
	function editUnitDetails() {
	
		var theDetails = document.getElementById('unitDetails');
		
		if (theDetails.style.display == 'block') {
			theDetails.style.display = 'none';
		}else{
			theDetails.style.display = 'block';
		}
		
	}
	
	//update unit Type assignment
	function updateUnitAssignment(unitID, assetID)
	{
		assetID = assetID.options[assetID.selectedIndex].value;
		
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successUnitTypeAssignmentUpdated);
		jsUnits.updateUnitTypeAssignment(unitID, assetID);
	}
	
	
	function successUnitTypeAssignmentUpdated(result)
	{
		document.getElementById("editUnitType").src = "images/saveupdate.png";
		
		setTimeout(function(){
			document.getElementById("editUnitType").src = "images/replace.png";
		}, 1000);
		
	}
	
	//update unit asset assignment
	function updateAssetAssignment(unitID, assetID)
	{
		assetID = assetID.options[assetID.selectedIndex].value;
		
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successAssetAssignmentUpdated);
		jsUnits.updateAssetAssignment(unitID,assetID);
	}
	
	
	function successAssetAssignmentUpdated(result)
	{
		document.getElementById("editAsset").src = "images/saveupdate.png";
		
		setTimeout(function(){
			document.getElementById("editAsset").src = "images/replace.png";
		}, 1000);
		
	}
	
	
	function setProject(groupID, projectID) {
	
		console.log(projectID, groupID, projectID);
	
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successProjectUpdated);
		jsUnits.setProjectAssignment(groupID, projectID);
		
	}
	
	function successProjectUpdated(result)
	{
		console.log(result);
	}
	
	
	function defineBalconyView(groupID, balconyID)	{
	
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successBalconyUpdated);
		jsUnits.setFloorBalconyAssignment(groupID, balconyID);
	}
	
	function successBalconyUpdated(result)
	{
		location.reload();
	}
	
	
	function updateBalconyViewAssignment(groupID)	{
		
		N = document.getElementById("N").checked;
		E = document.getElementById("E").checked;
		S = document.getElementById("S").checked;
		W = document.getElementById("W").checked;
		
		n = 0;
		e = 0;
		s = 0;
		w = 0;
		
		if(N) { n = 1; }
		if(E) { e = 1; }
		if(S) { s = 1; }
		if(W) { w = 1; }
		
		d = document.getElementById("defaultView");
		defaultDirection = d.options[d.selectedIndex].value;
		
		views = n.toString() + e.toString() + s.toString() + w.toString() + defaultDirection;
		
		document.getElementById("direction").innerHTML = defaultDirection;
		
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successUpdateBalconyViewAssignment);
		jsUnits.setBalconyViewAssignment(groupID, views);
		
		document.getElementById("updateViews").src = "images/saveupdate.png";
	}
	
	function successUpdateBalconyViewAssignment(result)
	{
		console.log(result);
		
		document.getElementById("updateViews").src = "images/replace.png";
	}
	
	//change active states for units and groups
	function setUnitState(unitID)	{
		
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successUpdateUnitState);
		jsUnits.setUnitActiveState(unitID);
	}
	
	function successUpdateUnitState(result)
	{
		theState = "NO";
		theClass = "contentLinkGreen";
		theActiveState = 1;
		
		if(result.active == 1)	{
			theState = "YES";
			theClass = "contentLinkRed";
			theActiveState = 0
		}
		
		theObj = document.getElementById("US_"+result.unitID);
		
		theObj.innerHTML = '<a class="'+ theClass +'" onclick="setUnitState('+ result.unitID +','+ theActiveState +')">' + theState +'</a>';
	}
	
	function setGroupState(groupID)	{
	
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successUpdateGroupState);
		jsUnits.setGroupActiveState(groupID);
	}
	
	function successUpdateGroupState(result)
	{
		theState = "NO";
		theClass = "contentLinkGreen";
		theActiveState = 1;
		
		if(result.active == 1)	{
			theState = "YES";
			theClass = "contentLinkRed";
			theActiveState = 0
		}
		
		theObj = document.getElementById("GS_"+result.groupID);
		
		theObj.innerHTML = '<a class="'+ theClass +'" onclick="setGroupState('+ result.groupID +','+ theActiveState +')">' + theState +'</a>';
	}
	
	//sortOrder - group
	function setGroupSortOrder(groupID, theIndex)	{
	
		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successUpdateGroupOrder);
		jsUnits.setGroupOrder(groupID, theIndex);
	}
	
	function successUpdateGroupOrder(result)
	{
		location.reload();
	}
	
	//sortOrder - asset
	function setUnitSortOrder(unitID, theIndex)	{

		jsUnits.setSyncMode();
		jsUnits.setCallbackHandler(successUpdateUnitOrder);
		jsUnits.setUnitOrder(unitID, theIndex);
	}
	
	function successUpdateUnitOrder(result)
	{
		location.reload();
	}
	
	function editUnitAsset(theAssetGroupID)	{
		theLink = "AppsView.cfm?subgroupID=" + theAssetGroupID;
		location.href = theLink;
	}
	
	//image
	function moves(e){
		var cordx = 0;
		var cordy = 0;
		if (!e) {
			var e = window.event;
		 }
		 if (e.pageX || e.pageY){
			 cordx = e.pageX;
			 cordy = e.pageY;
		 }else if (e.clientX || e.clientY){
			 cordx = e.clientX;
			 cordy = e.clientY;
		 }
		 moveViewer(cordx, cordy)
	 }
  
	function ShowBiggerImage(obj) {
			var img = "<img src='" + obj + "'+'height=320' >"
			var theObj = document.getElementById("LargeImageContainerDiv");
			theObj.style.display = 'block';
            theObj.innerHTML = img;
        }

	function ShowDefaultImage() {
			var theObj = document.getElementById("LargeImageContainerDiv");
            theObj.innerHTML = "";
			theObj.style.display = 'none';
        }
		
	function moveViewer(x_pos, y_pos) {
	  var d = document.getElementById('LargeImageContainerDiv');
	  d.style.position = "absolute";
	  d.style.left = x_pos+'px';
	  d.style.top = y_pos+'px';
	}
	
	function activateAsset(assetID)	{
	
	theObj = document.getElementById("GP_"+assetID);
	
	if(theObj.innerHTML === 'YES')	{
		state = 0;
	}else{
		state = 1;
	}
	
	jsUnits.setSyncMode();
	jsUnits.setCallbackHandler(updatedActiveResult);
	jsUnits.setAssetActiveState(assetID,state);
		
	}
	
	function updatedActiveResult(result)	{
		
		theObj = document.getElementById("GP_"+result.assetID);
		
		if(result.state === 1)	{
			theObj.innerHTML = 'YES';
			theObj.className = "contentLinkRed";
		}else{
			theObj.innerHTML = 'NO';
			theObj.className = "contentLinkGreen";
		}
		
	}
	
</cfoutput>
</script>
<style>
	.displayImg {
	width:auto; 
	height:320; 
	display:none; 
	z-index:2; 
	background-image: unitAssets.cfm;
	position:absolute;
	left:0; top:0;
	border:thin solid;
	}
</style>
<link href="styles.css" rel="stylesheet" type="text/css" />

<cfif unitID GT 0>
	
    <cfinvoke component="CFC.units" method="getUnit" returnvariable="unitData">
        <cfinvokeargument name="unitID" value="#unitID#"/>
    </cfinvoke>
   
    <cfinvoke component="CFC.Units" method="getUnitPlans" returnvariable="unitInfo">
        <cfinvokeargument name="unitID" value="#unitID#"/>
    </cfinvoke>
    
    <cfloop collection="#unitInfo#" item="theKey"></cfloop>
    <cfset theUnitSize = unitInfo[theKey].size & " sq.ft">
    <cfset theUnitAsset = unitInfo[theKey].assetID>
	
<cfelse>

    <cfinvoke component="CFC.units" method="getGroups" returnvariable="groupData">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="subgroupID" value="#subgroupID#"/>
    </cfinvoke>
    
    <cfinvoke component="CFC.units" method="getAllGroups" returnvariable="allGroups">
        <cfinvokeargument name="appID" value="#session.appID#"/>
    </cfinvoke>
    
    <cfinvoke component="CFC.units" method="getUnits" returnvariable="unitData">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="groupID" value="#subgroupID#"/>
    </cfinvoke>

</cfif>

<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#666" style="margin-top:1px">
    <tr>
      <td width="44"><a href="AppsView.cfm?edit=true&amp;tab=5"><img src="images/home.png" width="44" height="44" border="0" /></a></td>
      <td class="contentLinkWhite">
      <cfinvoke component="CFC.units" method="getBreadcrumb" returnvariable="crumb">
          <cfinvokeargument name="groupID" value="#subgroupID#"/>
      </cfinvoke>
      </td>
      <cfif unitID GT 0>
      <td align="right" class="contentLinkWhite"><cfoutput>#unitData.unitName# <span class="contentLinkDisabled">(#unitData.unitCode# - #unitData.unitType#) #theUnitSize#</span></cfoutput>
      </td>
      <td width="44" align="right" class="contentLinkWhite">
      <input name="editUnit" type="image" src="images/details.png" onclick="editUnitDetails();return false;" />
      </td>
      </cfif>
    </tr>
</table>

<cfif unitID GT 0>
	
    <cfinvoke component="CFC.Units" method="getProjectID" returnvariable="projectID">
      <cfinvokeargument name="unitID" value="#unitID#"/>
    </cfinvoke>
    
    <cfinvoke component="CFC.Modules" method="getCMSAppGroups" returnvariable="groups">
      <cfinvokeargument name="appID" value="#session.appID#"/>
      <cfinvokeargument name="groupID" value="#projectID#"/>
      <cfinvokeargument name="omit" value="Theme,Background,Categories"/><!--- omit these folders --->
    </cfinvoke>

	<div class="contentLinkDisabled" id="unitDetails" style="display:<cfif unitData.groupID IS "">block<cfelse>none</cfif>; height:auto">
    <form>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFF">
	<cfif unitData.groupID IS ''>
            
    </cfif>
       <tr>
        <td width="50%" align="left" class="formText">
        <!--- unit Type --->
        <cfoutput>
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="90" align="right" class="contentLinkGrey"><label style="padding-right:5px">Unit Type: </label></td>
            <td width="50">
            <cfif unitData.unitTypeID IS ''><cfset unitData.unitTypeID = -1></cfif>
            <cfinvoke component="CFC.units" method="getUnitTypes">
                <cfinvokeargument name="selected" value="#unitData.unitTypeID#"/>
            </cfinvoke>
            </td>
            <td width="44">
            <input name="editUnitType" id="editUnitType" type="image" src="images/replace.png" onclick="updateUnitAssignment(#unitID#,this.form.unitTypes);return false" />
            </td>
          </tr>
        </table>
        </cfoutput>
        </td>
        <td align="right" class="formfieldcontent" style="border:none; padding-left:0;">
        <!--- unit asset group --->
        <cfoutput>
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="90" align="right" class="contentLinkGrey"><label>Asset: </label></td>
            <!--- edit asset link --->
		  <cfif unitData.groupID GT 0>
          <td width="44" align="right" class="contentLinkWhite">
          <cfoutput>
          <input name="editUnit" type="image" src="images/editmode.png" onclick="editUnitAsset(#theUnitAsset#);return false;" /></cfoutput>
          </td>
          </cfif>
            <td width="50">
            
            <select name="unitAsset">
              <option value="-1" <cfif unitData.groupID IS ''> selected</cfif>>None</option>
                <cfloop query="groups">
                <option value="#group_id#"<cfif unitData.groupID IS group_id> selected</cfif>>#name#</option>
                </cfloop>
            </select>
            
            </td>
            <td width="44">
            <input name="editAsset" id="editAsset" type="image" src="images/replace.png" onclick="updateAssetAssignment(#unitID#,this.form.unitAsset);return false" />
            </td>
          </tr>
        </table>
        </cfoutput>
        </td>
      </tr> 
    
    </table>
    </form>
    
    <!--- views --->
      <cfinvoke  component="CFC.Units" method="getUnitFloorBalcony" returnvariable="balconyInfo">
          <cfinvokeargument name="unitID" value="#UnitID#"/>
      </cfinvoke>
      
       <!--- <cfdump var="#balconyInfo#"> --->
       <cfif structKeyExists(balconyInfo,'views')>
       
       <cfset views = balconyInfo.views> <!--- 0000N --->
       
       <cfset NState = Mid(views, 1, 1)>
       <cfset EState = Mid(views, 2, 1)>
       <cfset SState = Mid(views, 3, 1)>
       <cfset WState = Mid(views, 4, 1)>
       
       <cfset D = Mid(views, 5, 1)>
      
      <cfif balconyInfo.floorName IS ''>
          <cfset floorName = 'Balcony Floor'>
      <cfelse>
          <cfset floorName = 'Floor ' & balconyInfo.floorName>
      </cfif>
      
   	  <cfoutput>
  	  	<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="##FFF">
                <tr>
                  <td align="left" class="contentLinkGrey" style="padding-left:5px" height="44" bgcolor="##DDD" colspan="3"><span class="contentLinkGrey" style="padding-left:5px">Balcony View</span></td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td align="right" class="contentLinkGrey">#floorName#</td>
                  <td>
                  <img src="#balconyInfo.comp#" width="660" border="1" style="margin-left:4px; margin-top:10px" />
                  </td>
                  <td rowspan="2" align="right" valign="top">
                  <input name="updateViews" type="image" id="updateViews" onclick="updateBalconyViewAssignment(#unitID#);return false" src="images/replace.png" />
                  </td>
                </tr>
                <tr>
                  <td width="90" height="66" align="right" class="contentLinkGrey">Visible Views:</td>
                  <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td class="content">
           
                        <table width="100%" border="0" cellpadding="4" cellspacing="0" class="content">
                          <tr>
                            <td><input name="N" type="checkbox" id="N" value="1" <cfif NState> checked</cfif>/>
                        <label for="N">North</label></td>
                            <td><input name="E" type="checkbox" id="E" value="1" <cfif EState> checked</cfif>/>
                        <label for="E">East</label></td>
                            <td><input name="S" type="checkbox" id="S" value="1" <cfif SState> checked</cfif>/>
                        <label for="S">South</label></td>
                            <td><input name="W" type="checkbox" id="W" value="1" <cfif WState> checked</cfif>/>
                        <label for="W">West</label></td>
                          </tr>
                        </table>
                        
                        </td>
                      <td width="44" align="right" class="content">Point At</td>
                      <td width="44">
                      <select name="defaultView" class="content" id="defaultView" style="background:url(images/compass.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" />
                      <option value="N" style="color:##333; padding:4px" <cfif D IS 'N'> selected</cfif>>North</option>
                        <option value="E" style="color:##333; padding:4px" <cfif D IS 'E'> selected</cfif>>East</option>
                        <option value="S" style="color:##333; padding:4px" <cfif D IS 'S'> selected</cfif>>South</option>
                        <option value="W" style="color:##333; padding:4px" <cfif D IS 'W'> selected</cfif>>West</option>
                        </select>
                      </select>
                        </td>
                        <td class="contentLinkGrey"><div id="direction">#D#</div></td>
                      </tr>
                  </table></td>
                </tr>
                
              </table>
      </cfoutput>
       
       </cfif>
          
    </div>
</cfif>

<cfif unitID GT 0>

<cfelse>
	<cfoutput>
    <form>
    <div style="background-color: ##fff; margin-top:10px; padding-bottom:10px;">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="padding-bottom:10px">
      <tr>
        <td width="90" height="60" align="right" class="contentLinkGrey"><label style="padding-right:5px; padding-left:5px">Unit Group: </label></td>
        <td width="300" class="contentLinkGrey"><input name="name" type="text" class="formfieldcontent" id="name" style="width:300px" placeholder="group name"></td>
        <td width="44"><input name="editGroup" type="image" src="images/include.png" onClick="createGroup(#subgroupID#,this.form.name.value);return false"></td>
         <td align="right" class="contentLinkGrey"><label style="padding-right:5px">Unit Name: </label></td>
        <td width="100" align="right" class="contentLinkGrey"><input name="unitname" type="text" class="formfieldcontent" id="unitname" style="width:100px" placeholder="unit identifier"></td>
        <td width="44"><input name="editGroup" type="image" src="images/include.png" onclick="createUnit(#subgroupID#,this.form.unitname.value);return false;" /></td>
      </tr>
    </table>  
    </div>
    </form>
	</cfoutput>
</cfif>

<cfif unitID GT 0>
 <div>
<form> 
	<cfset cust = unitData.cust>
    <cfset user = unitData.user>
    
    <cfset config = unitData.data>
    
    <!--- convert epoch --->
    <cfif unitData.created NEQ ''>
         <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="dateCreated">
            <cfinvokeargument name="TheEpoch" value="#unitData.created#">
        </cfinvoke>
    <cfelse>
    	<cfset dateCreated = 0>
    </cfif>
    
    <cfif unitData.modified NEQ ''>
		<!--- convert epoch --->
         <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="dateModified">
            <cfinvokeargument name="TheEpoch" value="#unitData.modified#">
        </cfinvoke>
    <cfelse>
    	<cfset dateModified = 0>
    </cfif>

	<!--- User List --->
    <cfinvoke component="CFC.Users" method="getAllUsers" returnvariable="theCustList">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="userType" value="1"/>
        <cfinvokeargument name="accessLevel" value="1"/>
    </cfinvoke>  
    
    <cfinvoke component="CFC.Users" method="getAllUsers" returnvariable="theUserList">
        <cfinvokeargument name="appID" value="#session.appID#"/>
        <cfinvokeargument name="userType" value="0"/>
        <cfinvokeargument name="accessLevel" value="4"/>
    </cfinvoke>  
    
    <cfif structIsEmpty(cust)>
    	<cfset custSel = -1>
    <cfelse>
    	<cfset custSel = cust.user_id>
    </cfif>
    
    <cfif structIsEmpty(user)>
    	<cfset userSel = -1>
    <cfelse>
    	<cfset userSel = user.user_id>
    </cfif> 
    
      <cfoutput>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contentLinkGrey" style="margin-top:10px; margin-bottom:10px" bgcolor="##b9e7f9">
          <tr>
            <td height="32" align="left">
             
            <table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="90" align="right" class="contentLinkGrey">
				Customer: </td>
                <td width="100">
                <select name="customers" id="customers" style="margin-left:5px">
                  <option value="-1">None</option>
                  <cfloop query="theCustList">
                    <option value="#user_id#" <cfif user_id IS custSel>selected="selected"</cfif>>#name#</option>
                  </cfloop>
                </select>
                </td>
                <td width="44">
                <input name="editGroupCust" id="editGroupCust" type="image" src="images/replace.png" onclick="updateAssignment(#unitID#,0,this.form.customers);return false" />
                </td>
              </tr>
            
            </table>
            
            </td>
            <td align="right">
          
            <table border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="90" align="right" class="contentLinkGrey">
				Vendor: </td>
                <td width="100">
                <select name="users" id="users" style="margin-left:5px">
                  <option value="-1">None</option>
                  <cfloop query="theUserList">
                    <option value="#user_id#" <cfif user_id IS userSel>selected="selected"</cfif>>#name#</option>
                  </cfloop>
                </select>
                </td>
                <td width="44">
                <input name="editGroupUser" id="editGroupUser" type="image" src="images/replace.png" onclick="updateAssignment(#unitID#,this.form.users,0);return false" />
                </td>
              </tr>
            </table>
            
            </td>
          </tr>
        </table>        
        </cfoutput>
</form>
</div>
   
    <cfif NOT structIsEmpty(config)>
    
    
    <!--- Get Defaults --->
  <!---   <cfinvoke component="CFC.Configurator" method="getAllDefaultOptions" returnvariable="theDefaults">
        <cfinvokeargument name="appID" value="63"/>
        <cfinvokeargument name="option" value="1"/>
    </cfinvoke> --->
	
    <!--- Get Cust Choice Config --->
    <cfset custSelection = structNew()>
    <cfloop collection="#config#" item="theKey">
    	
        <cfset theSel = config[theKey].favouredConfigIndex + 1>
        <cfset theConfig = config[theKey].configs[theSel]>
    	
        <cfset structAppend(custSelection,{#theKey#:theConfig})>
    
    </cfloop>
    
    <cfoutput>
    
    <cfset totalCost=0>
    <cfset grandTotalCost=0>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="##CCC">
      <tr>
        <td width="50%" height="32" class="contentLink" style="padding-left:5px">Modified: <cfif dateCreated IS 0>NA<cfelse>#dateFormat(dateCreated,'DDD MMM, DD/YY')#</cfif></td>
        <td width="44" align="right" class="contentLinkDisabled" style="padding-right:5px">Created: <cfif dateCreated IS 0>NA<cfelse>#dateFormat(dateModified,'DDD MMM, DD/YY')#</cfif></td>
      </tr>
    </table>
    
    <cfif NOT structIsEmpty(config)>
    
		<!--- Get Root Key --->
        <cfloop collection="#custSelection#" item="theRoom">
        
            <cfset subTotalCost=0>
            
            <cfinvoke component="CFC.Configurator" method="getSpace" returnvariable="theSpace">
                <cfinvokeargument name="assetID" value="#theRoom#"/>
            </cfinvoke>
            
			<!--- config - balcony --->
            <table width="100%" border="0" cellpadding="0" cellspacing="1" class="contentLinkGrey" bgcolor="##666" style="margin-top:10px">
              <tr>
                <td height="44" align="left" colspan="6" bgcolor="##DDD">
                
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="contentLinkGrey" style="padding-left:5px">#theSpace.title# Configuration</td>
                      <td width="44"><a href="viewUnitConfigs.cfm?unitID=#unitID#&roomID=#theSpace.asset_id#" target="_new"><img src="images/view.png" width="44" height="44" border="0" /></a></td>
                    </tr>
                  </table>
                  
                  </td>
              </tr>
              </table>
              
            <!--- Materials List --->
            <table width="100%" border="0" cellpadding="5" cellspacing="1" class="contentLinkGrey" bgcolor="##666">
              <tr class="contentLinkWhite">
                <td width="200" align="left" bgcolor="##333333">Option</td>
                <td width="100" align="center" bgcolor="##333333">Code</td>
                <td width="200" align="left" bgcolor="##333333">Material</td>
                <td align="left" bgcolor="##333333">Description</td>
                <td width="100" align="right" bgcolor="##333333">Cost</td>
              </tr>
        
            <cfset optionImages = arrayNew(1)>
         
            <cfloop collection="#custSelection[theRoom]#" item="theOption">
            
                <cfset theMaterial = custSelection[theRoom][theOption]>
           
                <cfinvoke component="CFC.Configurator" method="getOptions" returnvariable="options">
                    <cfinvokeargument name="optionID" value="#theOption#"/>
                </cfinvoke>  
            
                <cfinvoke component="CFC.Configurator" method="getMaterials" returnvariable="material">
                    <cfinvokeargument name="optionID" value="#theOption#"/>
                    <cfinvokeargument name="assetID" value="#theMaterial#"/>
                </cfinvoke>
                
                <cfif material.recordCount GT 0>
                
                    <cfinvoke component="CFC.Modules" method="getContentAsset" returnvariable="asset">
                        <cfinvokeargument name="assetID" value="#material.asset_id#"/>
                    </cfinvoke>
                
                <cfelse>
                    <cfset asset = structNew()>
                    <tr>
                     <td height="32" colspan="5" align="left" bgcolor="##DDD">Materials Asset Not Found - #theOption#</td>
                     </tr>
                </cfif>
                 
                <cfif structIsEmpty(asset) >
                    
                    <!--- assets assigned dont match, something has changed - clear data --->
                    <cfinvoke component="CFC.Configurator" method="clearConfig" returnvariable="cleared">
                        <cfinvokeargument name="unitID" value="#unitID#"/>
                    </cfinvoke>
                
                <cfelse>
                 <!--- Get Root Key --->
                 <cfloop collection="#asset#" item="theKey"></cfloop>
                
                    <cfset theMaterial = asset[theKey].thumb.xdpi.url>
                    <cfset theImage = asset[theKey].image>
                    <cfset arrayAppend(optionImages,theImage)>
                    
                    <!--- Line Item --->
                          <tr>
                            <td width="200" align="left" bgcolor="##FFF">#options.title#</td>
                            <td width="100" align="center" bgcolor="##FFF">#material.code#</td>
                            <td width="200" align="left" bgcolor="##FFF">
                            
                            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                              <tr>
                                <td width="44"><img src="#theMaterial#" height="32" style="border:thin; border-color:##666; border-style:solid" onmouseover="moves(event); ShowBiggerImage(this.src);" onmouseout="ShowDefaultImage();" /></td>
                                <td align="left">#material.title#</td>
                              </tr>
                            </table>
                            
                            </td>
                            <td align="left" bgcolor="##FFF">#material.description#</td>
                            <td width="100" align="right" bgcolor="##FFF">#numberFormat(material.cost,'__,__.__')#</td>
                    </tr>
                          
                          <cfset subTotalCost +=material.cost>
                          <cfset totalCost +=material.cost>
                  </cfif>
                   
            </cfloop>
                 
                 <tr>
                 <td height="32" colspan="4" align="right" bgcolor="##DDD">Subtotal</td>
                 <td align="right" bgcolor="##DDD">#numberFormat(subTotalCost,'__,__.__')#</td>
                 </tr>
                </table>
            
            <cfset grandTotalCost +=subTotalCost>
            
         </cfloop>
         
         <table width="100%" border="0" cellpadding="5" cellspacing="1" class="contentLinkGrey" bgcolor="##666" style="margin-top:10px;">
         <tr>
             <td height="32" colspan="4" align="right" bgcolor="##666666" class="contentLinkWhite">Grand Total</td>
             <td width="100" align="right" bgcolor="##666666" class="contentLinkWhite">#numberFormat(grandTotalCost,'__,__.__')#</td>
            </tr>
          </table>
     
     </cfif>
     
    </cfoutput>
     
  </cfif>
    
<cfelse>

<cfif arrayLen(groupData) IS 0 AND arrayLen(unitData) IS 0>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFF" class="contentLink material">
	<tr>
        <td height="44" colspan="2" class="contentLinkDisabled" style="padding-left:10px">No Unit Groups or Units Defined</td>
    </tr>
</table>
</cfif>

<!--- Groups --->
<cfif arrayLen(groupData) GT 0>

<cfif subgroupID GT 0>

	<!--- balcony assets --->
    <cfinvoke  component="CFC.Assets" method="getAssets" returnvariable="balconies">
        <cfinvokeargument name="AssetTypeID" value="9"/><!--- balcony asset is 9 --->
        <cfinvokeargument name="AppID" value="#session.appID#"/>
        <cfinvokeargument name="groupID" value="#session.subgroupID#"/>
    </cfinvoke>

</cfif>

<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFF" class="contentLink material">
    <tr>
    <td height="44" bgcolor="#DDD" class="contentLinkGrey" style="padding-left:10px">Groups</td>
    <td height="44" bgcolor="#DDD" class="contentLinkGrey" style="padding-left:10px">
    
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="310" style="border:none; padding-left:0">&nbsp;</td>
            <td width="50" align="center" class="content" style="border:none; padding-left:0">ACTIVE</td>
            <td width="50" align="center" class="content" style="border:none; padding-left:0">ORDER</td>
            <td align="right" style="border:none; padding-left:0">&nbsp;</td>
          </tr>
        </table>
    
    </td>
    <td height="44" bgcolor="#DDD" class="contentLinkGrey" style="padding-left:10px">&nbsp;</td>
</tr>
    <cfloop index="aRow" array="#groupData#">
    	<cfoutput>
        <form id="G_#aRow.group_id#">
        
        <tr>
          <td width="44"><img src="images/unitgroup.png" width="44" height="44" ondblclick="editGroupName(#aRow.group_id#)" /></td>
            <td style="padding-left:10px">

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="310" style="border:none; padding-left:0">
                
                <input type="text" class="formfieldcontent" id="groupNameEdit" style="width:300px; display:none" value="#trim(aRow.name)#" />
              <a href="AppsView.cfm?edit=true&tab=5&subgroupID=#aRow.group_id#" class="contentLinkGrey" id="groupName_#aRow.group_id#" name="groupLink">#trim(aRow.name)#
              	  <cfif listLen(aRow.name, ' ') GT 1>
             		 <div class="contentLinkDisabled" style="float:right">(#listGetAt(aRow.name,2,' ')#)</div>
				  </cfif>
              </a>
                
                </td>
                <td width="50" align="center" style="border:none; padding-left:0">
                <div style="cursor:pointer" id="GS_#aRow.group_id#">
					<cfif aRow.active IS 1>
                        <a class="contentLinkRed" onclick="setGroupState(#aRow.group_id#)">YES</a>
                    <cfelse>
                        <a class="contentLinkGreen" onclick="setGroupState(#aRow.group_id#)">NO</a>
                    </cfif>
                </div>
                </td>
                <td width="50" align="center" style="border:none; padding-left:0">
                <select name="sortOrder" id="sortOrder" style="width:32px; text-align:center; text-indent:0; height:32px;" type="button" onchange="setGroupSortOrder(#aRow.group_id#,this.options[this.selectedIndex].value)" />
                  <cfloop index="order" from="0" to="#arrayLen(groupData)#">
                    <option value="#order#" style="color:##333; padding:4px" <cfif order IS aRow.order> selected</cfif>>#order#</option>
                  </cfloop>
                  </select>
                </td>
                <td align="right" style="border:none; padding-left:0">
                
                <!--- subgroup assignment --->
				<cfif subgroupID IS 0>
                    
                    <cfinvoke component="CFC.Modules" method="getGroups" returnvariable="allModules">
                        <cfinvokeargument name="appID" value="#session.appID#"/>
                        <cfinvokeargument name="subgroupID" value="-1"/>
                        <cfinvokeargument name="active" value="1"/>
                    </cfinvoke>
                    
                    <cfset groupUnitID = aRow.group_id>
                    <cfif aRow.project_id IS ''><cfset aRow.project_id = -1></cfif>
                    
                    <select name="theProject" style="text-indent:1px" onChange="setProject(#groupUnitID#,this.options[this.selectedIndex].value)">
                      <option value="-1" <cfif aRow.project_id IS -1> selected</cfif>>None</option>
                        <cfloop index="project" array="#allModules#">
                            <option value="#project.group_id#"<cfif aRow.project_id IS project.group_id> selected</cfif>>#project.name#</option>
                        </cfloop>
                    </select>
                <cfelse>
                
					<cfif balconies.recordCount GT 0>
                        
						<cfif aRow.balcony_id IS '' OR aRow.balcony_id IS 0>
                        <span class="contentGreyed">No View Assigned</span>
                        <cfelse>
                        
                        <!--- get balcony name --->
                        <cfquery name="theBalconyName" dbtype="query"> 
                            SELECT	assetName  
                            FROM	balconies
                            WHERE	asset_id = #aRow.balcony_id#
                        </cfquery>
                        
                       #theBalconyName.assetName#
                        
                    </cfif>
                    <select name="balconyView" id="balconyView" style="background:url(images/balcony-view.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onchange="defineBalconyView(#aRow.group_ID#,this.options[this.selectedIndex].value)" />
                      <option value="0" style="color:##333" <cfif aRow.balcony_id IS 0> selected="selected"</cfif>>No Balcony</option>
						<cfloop query="balconies">
                          <option value="#asset_id#" style="color:##333; padding:4px" <cfif asset_id IS aRow.balcony_id> selected</cfif>>#AssetName#</option>
                        </cfloop>
                        </select> 

                    </cfif>
                
                </cfif>
                
                </td>
              </tr>
            </table>
            
            </td>  
            <td width="132" height="44">
            
            <div id="viewGroup_#aRow.group_id#" style="display:block">
               
                  <div style="width:44px; float:right; cursor: pointer;" onclick="deleteGroup(#aRow.group_id#)">
                        <img src="images/remove.png" />
                  </div>
               
            </div>
            
            <div id="editGroup_#aRow.group_id#" style="display:none">
            
            	<div style="width:44px; float:right;cursor: pointer;" onclick="cancelGroupEdit(#aRow.group_id#)">
                    <img src="images/remove.png" />
                </div>
                
            	<div style="width:44px; float:right; cursor: pointer;" onclick="updateGroup(#aRow.group_id#)">
                    <img src="images/saveupdate.png" />
                </div>
                
                <div style="width:44px; float:right;">
                    <select name="groups" id="groups" style="background:url(images/moveItem.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onChange="moveItem(#aRow.group_id#, this.value)" />
                        <cfloop query="#allGroups#">
                       	  <option value="#group_id#" style="color:##333; padding:4px" <cfif subgroupID IS group_id>selected</cfif>>#name#</option>
                        </cfloop>
                    </select>
               </div>
                
            </div>
            
            </td>
        </tr>
        
        </form>
        </cfoutput>
    </cfloop>
    </table>
</cfif>


<!--- units --->
<cfif arrayLen(unitData) GT 0>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFF" class="contentLink material">
    <tr>
        <td width="455" height="44" bgcolor="#DDD" class="contentLinkGrey" style="padding-left:10px">Units</td>
        <td width="100" align="center" bgcolor="#DDD" class="content">ASSET ACTIVE</td>
        <td width="50" height="44" align="center" bgcolor="#DDD" class="content">ACTIVE</td>
        <td width="50" height="44" align="center" bgcolor="#DDD" class="content">ORDER</td>
        <td height="44" bgcolor="#DDD" class="content" style="padding-left:10px">&nbsp;</td>
    </tr>
    </table>
    <cfloop index="aRow" array="#unitData#">
    	<cfoutput>
        <form id="U_#aRow.unit_id#">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="##FFF" class="contentLink material">
        <tr>
        <td width="44"><img src="images/unit.png" width="44" height="44" ondblclick="editUnitName(#aRow.unit_id#)" /></td>
            <td width="410" style="padding-left:10px">
              <input type="text" class="formfieldcontent" id="unitNameEdit" style="width:400px; display:none" value="#trim(aRow.name)#" />
              <a href="AppsView.cfm?edit=true&tab=5&subgroupID=#subgroupID#&unitID=#aRow.unit_id#" class="contentLinkGrey" id="unitName_#aRow.unit_id#" name="unitLink">#trim(aRow.name)#</a>
            </td>
            <td width="100" align="center" valign="middle">
            <cfif aRow.assetActive IS 1> 
            	<cfset state = 0>
                <cfset stateType = "YES">
                <cfset stateColor = "contentLinkRed">
            <cfelse>
            	<cfset state = 1>
                <cfset stateType = "NO">
                <cfset stateColor = "contentLinkGreen">
            </cfif>
            <div style="cursor:pointer" id="GP_#aRow.groupID#" class="#stateColor#" onclick="activateAsset(#aRow.groupID#)">#stateType#</div>
            <!--- <cfif aRow.assetActive IS 1> 
                <a class="contentLinkRed" style="cursor:pointer" onclick="activateAsset(#aRow.groupID#,0)">YES</a>
            <cfelse>
                <a class="contentLinkGreen" style="cursor:pointer" onclick="activateAsset(#aRow.groupID#,1)">NO</a>
            </cfif> --->
           
            </td>
            <td width="50" align="center" valign="middle">
            <div style="cursor:pointer" id="US_#aRow.unit_id#">
				<cfif aRow.active IS 1>
                    <a class="contentLinkRed" onclick="setUnitState(#aRow.unit_id#)">YES</a>
                <cfelse>
                    <a class="contentLinkGreen" onclick="setUnitState(#aRow.unit_id#)">NO</a>
                </cfif>
            </div>
            </td> 
            <td width="50" align="center" style="border:none; padding-left:0">
                <select name="sortOrder" id="sortOrder" style="width:32px; text-align:center; text-indent:0; height:32px;" type="button" onchange="setUnitSortOrder(#aRow.unit_id#,this.options[this.selectedIndex].value)" />
                  <cfloop index="order" from="0" to="#arrayLen(unitData)#">
                    <option value="#order#" style="color:##333; padding:4px" <cfif order IS aRow.order> selected</cfif>>#order#</option>
                  </cfloop>
                  </select>
                </td>
            <td height="44" valign="middle">
            <div class="contentGreyed" style="float:left; padding-top:14px">
            <cfif aRow.type IS 0>(unit type not assigned)</cfif>
            <cfif aRow.groupID IS 0>(asset not assigned)</cfif>
            </div>
            <div id="viewUnit_#aRow.unit_id#" style="display:block; float:right">
               
               <div style="width:44px; float:right; cursor: pointer;" onclick="deleteUnit(#aRow.unit_id#)">
                    <img src="images/remove.png" />
               </div>
              
            </div>
            
            <div id="editUnit_#aRow.unit_id#" style="display:none">
            
            	<div style="width:44px; float:right; cursor: pointer;" onclick="cancelUnitEdit(#aRow.unit_id#)">
                    <img src="images/remove.png" />
                </div>
                
            	<div style="width:44px; float:right; cursor: pointer;" onclick="updateUnit(#aRow.unit_id#)">
                    <img src="images/saveupdate.png" />
                </div>
                
                <div style="width:44px; float:right;">
                    <select name="groups" id="groups" style="background:url(images/moveItem.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" type="button" onChange="moveUnitItem(#aRow.unit_id#, this.value)" />
                        <cfloop query="#allGroups#">
                       	  <option value="#group_id#" style="color:##333; padding:4px" <cfif subgroupID IS group_id>selected</cfif>>#name#</option>
                        </cfloop>
                    </select>
               </div>
                
            </div>
            
            </td>
        </tr>
        </table>
        </form>
        </cfoutput>
    </cfloop>
</cfif>

</cfif>

<div class="displayImg" id="LargeImageContainerDiv">

<!--- <cfif unitID GT 0>

	<!--- create composite image --->
    <cfimage source="#optionImages[1].url.mdpi#" name="compImage">
    
    <cfset finalImage = ImageNew("",compImage.width,compImage.height,"argb")>
    <cfset ImageDrawRect(finalImage,0,0,compImage.width,compImage.height,"yes")> 
    
    <cfloop index="img" from="1" to="#arrayLen(optionImages)#">
        <cfimage source="#optionImages[img].url.mdpi#" name="anImage">
        <cfset ImageOverlay(finalImage,anImage)>
    </cfloop>
    
    <cfset ImageSetDrawingColor(finalImage,"333333")> 
    <cfset ImageDrawRect(finalImage,0,0,compImage.width-1,compImage.height-1,"no")> 
    
    <p>
    <cfimage source="#finalImage#" action="writeToBrowser">
    
    <!--- <!--- Write the second ColdFusion image to result.jpg. ---> 
    <cfimage source="#finalImage#" action="write" destination="test_myImage.jpg" overwrite="yes"> 
    
    <!--- Display the two source images and the new image. ---> 
    <img src="test_myImage.jpg"/> --->

</cfif>
 --->