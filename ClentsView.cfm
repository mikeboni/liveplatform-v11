<cfparam name="ClientID" default="">
<cfparam name="appID" default="0">
<cfparam name="edit" default="false">
<cfparam name="new" default="false">
<cfparam name="error" default="">
<cfparam name="newApp" default="false">
<cfparam name="newApp" default="false">
<cfparam name="assetID" default="0">
<cfparam name="userDev" default="1">

<cfparam name="apps" default="">
<cfparam name="clients" default="">
<cfparam name="userAccess" default="">

<cfparam name="showInactiveClients" default="false">

<!--- <cfif NOT IsDefined("session.userDev")><cfset session.userDev = 1></cfif>
<cfif userDev IS 1><cfset session.userDev = 1><cfelse><cfset session.userDev = 0></cfif> --->

<cfif NOT IsDefined("session.appID")><cfset session.appID = '0'></cfif>
<cfif appID NEQ ''><cfset session.appID = appID></cfif>

<cfif NOT IsDefined("session.clientID")><cfset session.clientID = '0'></cfif>
<cfif ClientID NEQ ''><cfset session.ClientID = ClientID></cfif>

<cfif NOT IsDefined("session.userAccess")><cfset session.userAccess = ''></cfif>

<cfif userAccess NEQ ''>
	<cfset session.userAccess = userAccess>
</cfif>

<cfif NOT IsDefined("session.clients")><cfset session.clients = []></cfif>
<cfif NOT IsDefined("session.apps")><cfset session.apps = []></cfif>

<cfif clients NEQ ''>
	<cfset session.clients = listToArray(clients)>
</cfif>

<cfif apps NEQ ''>
	<cfset session.apps = listToArray(apps)>
</cfif>

<cfif ArrayLen(session.clients) IS 0 OR ArrayLen(session.apps) IS 0>
<cflocation url="../../CMS/default.cfm">
</cfif>

<cfinvoke component="CFC.Assets" method="getAssetsTypes" returnvariable="assetTypes" />
<cfset session.AssetsTypes = assetTypes>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Live-Clients</title>
<link href="styles.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
function updateClient(form)	{
	if(form==0)
	{
		//new client
		document.forms['newClient'].submit();
			
	}else{
		//update client
		document.forms['updateClient'].submit();
		
	}
}	
function updateApp(form)	{
	if(form==0)
	{
		//new client
		document.forms['newApp'].submit();
			
	}else{
		//update client
		document.forms['updateApp'].submit();
		
	}
}
</script>

</head>

<body>    

<cfinclude template="Assets/header.cfm">

<cfif session.userAccess LT 4>
	<cflocation url="../../CMS/default.cfm">
</cfif>

<div style="width:800px;">  

<cfif new>

<div style="background-color:#069; margin-top:5px; height:32px; padding-left:10px; padding-top:10px; padding-right:10px; width:790px">
<div style="width:400px;float:left"><span class="sectionLink">New Client</span></div>
<div class="sectionLink" style="float:right"><a href="#" class="function" onclick="updateClient(0);">Save</a> | <a href="ClentsView.cfm" class="function">Cancel</a></div>
</div>

<div style="background-color:#EEE; width:810px;">
 <form action="updateClient.cfm" method="post" enctype="multipart/form-data" id="newClient">
    
    <table width="800" border="0" cellspacing="10">
      <tr>
        <td width="100" align="right"><span class="content">Company</span></td>
        <td><label for="company"></label>
        <input name="name" type="text" class="formfieldcontent" id="name" size="60" maxlength="50" /></td>
      </tr>
      <tr>
        <td align="right" class="content">Root Server Path</td>
        <td><input name="rootPath" type="text" class="formfieldcontent" id="rootPath" size="100" maxlength="128" /></td>
      </tr>
      <tr>
        <td align="right"><span class="content">Server Path</span></td>
        <td><input name="folder" type="text" class="formfieldcontent" id="folder" size="60" maxlength="50" />
          /</td>
      </tr>
      <tr>
        <td align="right" class="content">Support EMail</td>
        <td><input name="supportEMail" type="text" class="formfieldcontent" id="supportEMail" size="60" maxlength="50" /></td>
      </tr>
      <tr>
        <td align="right" class="content">Client Email</td>
        <td><input name="clientEmail" type="text" class="formfieldcontent" id="clientEmail" size="60" maxlength="50" /></td>
      </tr>
      <tr>
        <td align="right"><span class="content">Active</span></td>
        <td><label for="active"></label>
          <select name="active" id="active" style="width:80px">
            <option value="yes">Yes</option>
            <option value="no">No</option>
        </select></td>
      </tr>
      <tr>
        <td align="right" class="content">Company Icon</td>
        <td><input name="imageFile" type="file" class="formfieldcontent" id="imageFile" size="60" maxlength="50" style="padding-left:0" /></td>
      </tr>
    </table>
  </form>
</div>
<cfelse>

<cfif session.clientID IS 0>
    <cfoutput>
	<div style="width: 100%; text-align: right; padding: 5px; text-transform: uppercase;">
		<a href="ClentsView.cfm?clientID=#clientID#&showInactiveClients=0" class="<cfif showInactiveClients iS 0>contentLinkRed<cfelse>contentLinkDisabled</cfif>" style="font-size: 10pt">Active</a> 
		| 
		<a href="ClentsView.cfm?clientID=#clientID#&showInactiveClients=1" class="<cfif showInactiveClients iS 1>contentLinkRed<cfelse>contentLinkDisabled</cfif>" style="font-size: 10pt">Inactive</a>
	</div>
  </cfoutput>
  <cfloop collection="#assetPathsCur#" item="z">
	
    <cfset company = assetPathsCur[z].name>
	<cfset iconPath = assetPathsCur[z].icon>
    <cfset clientID = assetPathsCur[z].id>
    <cfset active = assetPathsCur[z].active>
  	
    <cfif arrayFind(session.clients, clientID) GT 0><!--- CLients --->
   	
    <cfif active IS ''><cfset active = false></cfif>
    
    <cfif NOT showInactiveClients AND active IS 1>
	<cfoutput>
        <a href="ClentsView.cfm?clientID=#clientID#">
        <div class="contentContainer">
		<cfif active>
        <div><img src="#iconPath#" width="68" height="68" style="padding-bottom:8px" /></div>
        <cfelse>
        <div style="margin-left:42px; background-image:url(#iconPath#); background-repeat: no-repeat; background-size:contain; width:68px; height:68px; margin-bottom:8px"><img src="images/inactive-app.png" width="68" height="68" style="padding-bottom:8px;" /></div>
        </cfif>
            <span class="content">#company#</span>
        </div>
       </a>
     </cfoutput>
    <cfelseif showInactiveClients AND active IS 0>
    	<cfoutput>
        <a href="ClentsView.cfm?clientID=#clientID#">
        <div class="contentContainer">
		<cfif active>
        <div><img src="#iconPath#" width="68" height="68" style="padding-bottom:8px" /></div>
        <cfelse>
        <div style="margin-left:42px; background-image:url(#iconPath#); background-repeat: no-repeat; background-size:contain; width:68px; height:68px; margin-bottom:8px"><img src="images/inactive-app.png" width="68" height="68" style="padding-bottom:8px;" /></div>
        </cfif>
            <span class="content">#company#</span>
        </div>
       </a>
     </cfoutput>
	</cfif>
   
    </cfif>
    </cfloop>  
    <!--- </cfoutput> --->
    
    
   	<a href="ClentsView.cfm?new=true">
        <div class="contentContainer">
            <img src="images/addObject.png" width="68" height="68" style="padding-bottom:8px" /><br />
            <span class="content">New Client</span>
        </div>
   	</a>
<cfelse>

	<cfinvoke  component="CFC.Apps" method="getClientApps" returnvariable="clientApps">
    	<cfinvokeargument name="clientID" value="#session.clientID#"/>
	</cfinvoke>
    
	<cfset apps = assetPathsCur.apps>
    <cfset iconPath = assetPathsCur.client.icon>
    <cfset company = assetPathsCur.client.name>
	
	<!--- <cfset iconPath = clientPath & clients.icon> --->
    
    <div style="width:810px; background-color:##333; height:70px; padding-left:0px; padding-top:8px">
		
		<!--- <cfoutput query="clients"> --->
        <cfoutput>
        <div>
          <img src="#iconPath#" width="68" height="68" style="float:left; padding-right:10px" />
          <div class="bannerHeading" style="padding-top:10px;">#company#</div>
        </div>
    </div>
          <cfset editState = NOT edit>
          <div class="optionButtons"> 
          <a href="ClentsView.cfm?edit=#editState#"><img src="images/info.png" width="44" height="44" alt="users" /></a>
          </div>
        </cfoutput>
        <!--- </cfoutput> --->
        

    
    <cfif edit>
		<!---Edit Compant Info--->
        <div style="background-color:#EEE; width:810px;float:none">
        
        
<div style="background-color:#069; margin-top:5px; height:32px; padding-left:10px; padding-top:10px; padding-right:10px; width:790px">
        <div style="width:100%;"><span class="sectionLink">Client Information</span>
            <div class="sectionLink" style="float:right">
            <a href="##" class="function" onclick="updateClient(1);">Update</a> | <a href="ClentsView.cfm" class="function">Cancel</a>
            </div>
        </div>
</div>
        
            <cfoutput query="clientsQuery">
            <form action="updateClient.cfm" method="post" enctype="multipart/form-data" id="updateClient">
            
            <table width="800" border="0" cellspacing="10">
               <tr>
                <td align="right" valign="bottom"><span class="content">Company Icon</span></td>
                <td><img src="#iconPath#" width="68" height="68" /></td>
              </tr>
              <tr>
                <td width="100" align="right">&nbsp;</td>
                <td><input name="imageFile" type="file" class="formfieldcontent" id="imageFile" size="60" maxlength="50" style="padding-left:0" /></td>
              </tr>
              <tr>
                <td width="80" align="right"><span class="content">Company</span></td>
                <td><label for="name"></label>
                <input name="name" type="text" class="formfieldcontent" id="name" value="#company#" size="60" maxlength="50" />
                <input name="clientID" type="hidden" id="clientID" value="#session.clientID#" /></td>
              </tr>
              <tr>
                <td align="right"><span class="content">Root Server Path</span></td>
                <td><input name="rootPath" type="text" class="formfieldcontent" id="rootPath" size="100" maxlength="128" value="#rootPath#" /></td>
              </tr>
              <tr>
                <td align="right"><span class="content">Server Path</span></td>
                <td><input type="text" disabled="disabled" class="formfieldcontent" value="#path#" size="60" maxlength="50" />
                  /
                  <input name="folder" type="hidden" id="folder" value="#path#" /></td>
              </tr>
              <tr>
                <td align="right" class="content">Support Email</td>
                <td><input name="supportEMail" type="text" class="formfieldcontent" id="supportEMail" value="#support#" size="60" maxlength="50" /></td>
              </tr><tr>
                <td align="right" class="content">Client Email</td>
                <td><input name="clientEMail" type="text" class="formfieldcontent" id="clientEMail" value="#contactEmail#" size="60" maxlength="50" /></td>
              </tr>
              <tr>
                <td align="right"><span class="content">Active</span></td>
                <td><label for="active"></label>
                  <select name="active" id="active" style="width:80px">
                    <option value="yes" <cfif active IS 1>selected</cfif>>Yes</option>
                    <option value="no" <cfif active IS 0>selected</cfif>>No</option>
                </select></td>
              </tr>
            </table>
          </form>
            </cfoutput>
        </div>
    </cfif>
    
    <cfif newApp>
    
    <!---New Application--->
    
    <div style="background-color:#069; margin-top:5px; height:32px; padding-left:10px; padding-top:10px; padding-right:10px; width:790px">
<div style="width:400px;float:left"><span class="sectionLink">New Application</span></div>
<div class="sectionLink" style="float:right"><a href="#" class="function" onclick="updateApp(0);">Save</a> | <a href="ClentsView.cfm" class="function">Cancel</a></div>
</div>
    
    </div>
	  <cfoutput query="clientsQuery">
      <form action="updateApp.cfm" method="post" enctype="multipart/form-data" id="newApp">
      
      <table width="800" border="0" cellspacing="10">
        <tr>
          <td width="100" align="right" class="content">App Icon</td>
          <td><input name="imageFile" type="file" class="formfieldcontent" id="imageFile" size="60" maxlength="50" style="padding-left:0" /></td>
        </tr>
        <tr>
          <td width="80" align="right"><span class="content">App Name</span></td>
          <td><label for="name"></label>
          <input name="name" type="text" class="formfieldcontent" id="name" size="60" maxlength="50" />
          <input name="clientID" type="hidden" id="clientID" value="#session.clientID#" /></td>
        </tr>
         <tr>
          <td align="right" class="content">Bundle ID</td>
          <td><input name="bundleID" type="text" class="formfieldcontent" id="bundleID" value="" size="60" maxlength="50" /></td>
        </tr>
         <tr>
           <td align="right"><span class="content">Root Server Path</span></td>
           <td><input name="rootPath" type="text" class="formfieldcontent" id="rootPath" size="100" maxlength="128" /></td>
         </tr>
         <tr>
          <td align="right"><span class="content">Server Path</span></td>
          <td><input name="folder" type="text" class="formfieldcontent" id="folder" size="60" maxlength="50" />
            /            </td>
        </tr>
        <tr>
          <td align="right" class="content">Version</td>
          <td><input name="version" type="text" class="formfieldcontent" id="version" value="100000" size="10" maxlength="6" /></td>
        </tr>
        <tr>
          <td align="right" class="content">&nbsp;</td>
          <td><table width="100%" border="0">
            <tr>
              <td width="80" align="right" class="content">App Active</td>
              <td width="90"><select name="active" id="active" style="width:80px">
              <option value="yes">Yes</option>
              <option value="no" selected>No</option>
          </select></td>
              <td width="80" align="right" class="content">Force Update</td>
              <td><select name="forced" id="forced" style="width:80px">
                <option value="yes">Yes</option>
                <option value="no" selected>No</option>
              </select></td>
            </tr>
          </table></td>
        </tr>
      </table>
    </form>
      </cfoutput>
    </div>
    
    <cfelse>
    
    <!---Applcaitions List--->
    <div style="background-color:#666; margin-top:5px; height:32px; padding-left:10px; padding-top:8px; width:800px">
    <span class="sectionLink">Applications</span>
    </div>
  
	<div style="width:800px; background-color:##333; height:32px;">

         <cfloop index="z" from="1" to="#arrayLen(assetPathsCur.apps)#">
        
            <cfset iconPath = assetPathsCur.apps[z].icon>
            <cfset appActive = assetPathsCur.apps[z].active>
            <cfset appID = assetPathsCur.apps[z].id>
            <cfset appName = assetPathsCur.apps[z].name>
            
			<cfif arrayFind(session.apps, appID) GT 0>
           
            <cfoutput>
			  <a href="AppsView.cfm?appID=#appID#">
				<div class="contentContainer">
			   <cfif appActive>
				  <img src="#iconPath#" width="68" height="68" style="margin-bottom:10px" />
				  <br /><span class="contentHilighted">#appName#</span><br />
			   <cfelse>
					<div style="margin-left:41px; width:68px; height:68px; background-image:url(#iconPath#); background-repeat: no-repeat; background-size:contain; background-position: 50% 50%;margin-bottom:10px">
					<img src="images/inactive-app.png" width="68" height="68" />
					</div>
					<span class="contentHilighted">#appName#</span><br />
			   </cfif>

				   </div>
			  </a>
           </cfoutput>
           
				</cfif>
           </cfloop>
      
      <a href="ClentsView.cfm?newApp=true">
        <div class="contentContainer">
            <img src="images/addObject.png" width="68" height="68" style="padding-bottom:8px" /><br />
            <span class="content">New Application</span>
        </div>
   	  </a>
      
      
    </div>
    
    </cfif>
    
</cfif>

</cfif>

</div>

</body>
</html>