<cfparam name="groupID" default="0">
<cfparam name="assetID" default="0">

<!--- Delete Group and Alll Assets and or Subgroups --->
<cfif groupID GT '0'>

 	<cfinvoke component="CFC.Modules" method="deleteGroupAssets" returnvariable="assetsToDelete">
        <cfinvokeargument name="subgroupID" value="#groupID#"/>
    </cfinvoke> 
	
	<cflocation url="AppsView.cfm?subgroupID=#session.subgroupID#&amp;error=Asset Group and All Assets in Group" addtoken="no">

</cfif>

<!--- Delete Asset --->
<cfif assetID GT '0'>

	<cfinvoke component="CFC.Modules" method="deleteAssetInGroup" returnvariable="deleted">
        <cfinvokeargument name="assetID" value="#assetID#"/>
        <cfinvokeargument name="groupID" value="#session.subgroupID#"/>
    </cfinvoke>
	
    <cflocation url="AppsView.cfm?subgroupID=#session.subgroupID#&amp;error=Asset Deleted" addtoken="no">
    
</cfif>

