<cfparam name="appID" default="">
<cfparam name="assetTypeTab" default="0">
<cfparam name="newUser" default="no">
<cfparam name="error" default="">
<cfparam name="search" default="">
<cfparam name="emailsupport" default="no">
<cfparam name="activeUser" default="0">
<cfparam name="userType" default="0">

<cfparam name="access" default="4">
<cfparam name="searchByLast" default="A">

<cfif search NEQ ''>
	<cfset access = -1>
    <cfset searchByLast = 'ALL'>
</cfif>


<cfif search NEQ ''><cfset activeUser = 1></cfif>

<cfif NOT IsDefined("session.appID")><cfset session.appID = '0'></cfif>
<cfif appID NEQ ''><cfset session.appID = appID></cfif>

<cfif NOT IsDefined("session.clientID")><cflocation url="ClentsView.cfm" addtoken="no"></cfif>
<!--- <cfif appID IS '0' OR appID IS ''><cflocation url="CientsView.cfm" addtoken="no"></cfif> --->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Live-Applications</title>
<link href="styles.css" rel="stylesheet" type="text/css" />

<link href="JS/mcColorPicker/mcColorPicker.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="JS/classSelector.js"></script>
<script type="text/javascript" src="JS/randomGenerator.js"></script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="JS/multiple-dropdown/jquery.sumoselect.js"></script>
<link href="JS/multiple-dropdown/sumoselect.css" rel="stylesheet" />

<script type="text/javascript">
	$(document).ready(function () {
		window.asd = $('.SlectBox').SumoSelect({ csvDispCount: 3 });
		window.test = $('.testsel').SumoSelect({okCancelInMulti:true });
		window.testSelAll = $('.testSelAll').SumoSelect({okCancelInMulti:true, selectAll:true });
		window.testSelAll2 = $('.testSelAll2').SumoSelect({selectAll:true });
	});
</script>
<style type="text/css">
	body{font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;color:#444;font-size:13px;}
	p,div,ul,li{padding:0px; margin:0px;}
</style>

<script type="text/javascript">

function showDelete(theObj, state)	{
	
	theObjRef = document.getElementById(theObj);
	
	if(state)
	{
		removeClass(theObjRef,"itemHide");
		addClass(theObjRef, "itemShow");
	}else{
		removeClass(theObjRef,"itemShow");
		addClass(theObjRef, "itemHide");
	}
	
}

function deleteUser(userID) {
		if(confirm('Are you sure you wish to Delete this User?'))
		{
			deleteUserFromDB(userID);
		}else{ 
			//nothing
		}
}

function showReports(token)
{
	window.open('http://liveplatform.net/reports.cfm?auth_token='+token	, '_new');
}

</script>

<!--- Access Levels --->
<cfinvoke component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />


<!--- Group Access--->
<cfajaxproxy cfc="CFC.Access" jsclassname="accessManager">

<!--- Active State and Order--->
<cfajaxproxy cfc="CFC.Users" jsclassname="setMyActiveState">

<!--- LIVE--->
<cfajaxproxy cfc="LiveAPI" jsclassname="regApi">

<script>




toggleActiveState = function(theObject,appID,userID)
{
	var jsState = new setMyActiveState();

    var theState = document.getElementById('activeState_'+theObject).innerHTML;
	theState = theState.trim();
	var theObjState;

	if(theState == 'YES')
	{
		theObjState = 'NO';
		theNewState = 0;
		CSSStyle = 'contentLinkGreen';
		CSStyleContent = 'contentLinkDisabled';
		
		jsState.setUserActiveState(userID,appID,theNewState);
	
		document.getElementById('activeState_'+theObject).innerHTML = theObjState;
		document.getElementById('activeState_'+theObject).className = CSSStyle;
		
	}else{
		theObjState = 'YES';
		theNewState = 1;
		CSSStyle = 'contentLinkRed';
		CSStyleContent = 'contentLink';
	}
	
	jsState.setUserActiveState(userID,appID,theNewState);
	
	document.getElementById('activeState_'+theObject).innerHTML = theObjState;
	
	document.getElementById('activeState_'+theObject).className = CSSStyle;
	
	document.getElementById('contentAsset_'+theObject).className = CSStyleContent;
	document.getElementById('contentEmail_'+theObject).className = CSStyleContent;
	
}

deleteUserFromDB = function(userID)
{
	
	var jsState = new setMyActiveState();

	<cfoutput>
	var clientID = #session.clientID#;
	var appID = #session.appID#;
	</cfoutput>
	
	jsState.deleteUser(userID,clientID,appID);
	jsState.setCallbackHandler(showStateResult);
	
	location.reload();	
	<!--- document.location.href = "usersView.cfm?assetTypeTab=0&userType=<cfoutput>#userType#</cfoutput>" --->
}

userRegisteredResult = function(result){
	//callback
}

createNewUser = function(clientType)
{
	var jsState = new setMyActiveState();

	var form = document.newUser;
	
	var userName = form.userName.value;
	var userEMailAddr = form.userEmailAddress.value;
	var userLevel = parseInt(form.userLevel[form.userLevel.selectedIndex].value);
	var userCode = form.userCode.value;
	var userPass = form.userPass.value;
	
	<cfoutput>
	var clientID = #session.clientID#;
	var appID = #session.appID#;
	</cfoutput>
	
	if(clientType) {
		jsState.registerUser(userName,userEMailAddr,userCode,userPass,userLevel,clientID,appID,1);
		jsState.setCallbackHandler(userRegisteredResult);
		
		document.location.href = "usersView.cfm?assetTypeTab=0&userType=<cfoutput>#userType#</cfoutput>";
	}else{
		
		jsState.registerUser(userName,userEMailAddr,userCode,'',userPass,userLevel,clientID,appID,0);
		jsState.setCallbackHandler(userRegisteredResult);
	
		document.location.href = "usersView.cfm";
	}
}

showStateResult = function(result){
	//alert(result);
}

<!--- Resend Registration --->
resendRegInfo = function(email)
{
	var jsRegAccess = new regApi();
	
	jsRegAccess.setSyncMode();
	jsRegAccess.setCallbackHandler(resendSuccess);
	jsRegAccess.resendUserRegistrationInfo(<cfoutput>#session.clientID#</cfoutput>,email);
}

resendSuccess = function(result){
	alert('Resent Registration EMail to user');
}


<!--- setPassword --->
resendSetPassword = function(email)
{
	var jsRegAccess = new regApi();
	
	jsRegAccess.setSyncMode();
	jsRegAccess.setCallbackHandler(resendPassSuccess);
	jsRegAccess.resendSetPassword(<cfoutput>#session.clientID#</cfoutput>,email);
}

resendPassSuccess = function(result){
	alert('Resent Define Password EMail to User');
}


function addUsers(groupID)	{
	
	//console.log(groupID);
		
}

function removeUser(userID)	{
	
	//console.log(userID);
		
}

function checkmarkUser(formButton, groupID, userID)	{

	//console.log(formButton, groupID, userID);
	
	if(formButton.className == "checkmarkNormal") {
		formButton.className ="";
		formButton.classList.add("checkmarkSelected");
	}else{
		formButton.className ="";
		formButton.classList.add("checkmarkNormal");
	}
	
}

function removeUser(groupID, userID)	{
	
	var jsAccess = new accessManager();
	
	if(confirm('Are you sure you wish to Remove User from this Group Access?'))
	{
		jsAccess.setSyncMode();
		jsAccess.setCallbackHandler(removeUserSuccess);
		jsAccess.removeUsersAccess(groupID, userID);
	}else{
		//nothing	
	}
}

function removeUserSuccess(result)	{
	
	location.reload();	
}

//set user access
function setGroupAccessForUser(groupID, userID)	{
	
	var jsAccess = new accessManager();
	
	if(groupID == null) { 
		groupID = [];
	}
	//console.log(groupID);
	jsAccess.setSyncMode();
	jsAccess.setCallbackHandler(setGroupAccessForUserSuccess);
	jsAccess.setUserGroupAccess(userID,groupID);
	
}

function setGroupAccessForUserSuccess(result)	{
	
	//location.reload();	
}

function importCreateAccounts()	{
	
	var jsAccess = new accessManager();
	
	jsAccess.setSyncMode();
	jsAccess.setCallbackHandler(importCreateNewUsersSuccess);
	jsAccess.setUserGroupAccess(userID,groupID);
}	

function importCreateNewUsersSuccess()	{
	location.reload();	
}

//dev state
function setDevState(userID, devState)	{
	var jsState = new setMyActiveState();

	jsState.setSyncMode();
	jsState.setCallbackHandler(setDevStateSuccess);
	jsState.setUserDevState(userID, devState);
}

function setDevStateSuccess(result)	{
	console.log(result)	
}


function displayImport()	{

	var theObjFile = document.getElementById("importFile");
	
	if(theObjFile.value != '' && theObjFile.value.indexOf('.xlsx') > -1 || theObjFile.value.indexOf('.xls') > -1)	{
	
		var theObj = document.getElementById("import");
		theObj.style.display = "block";
		theObjFile.style.display = "none";
	
	}else{
		theObjFile.value = "";
		alert('The file must be a XLS (Excel) file');	
	}
	
}

function importConfirm()	{
	
	if(confirm('Are you sure you wish to Import and Create these users for this client/app'))
	{
		return true
	}else{ 
		return false
	}
}

<!--- User Access --->
updateUserAccess = function(theForm, userID) {
	var jsState = new setMyActiveState();

	theSel = theForm.userLevel.selectedIndex;
	accessLevel = parseInt(theForm.userLevel[theSel].value);
	
	theForm.userLevel.style.backgroundImage = "url('images/access_locked-"+ accessLevel +".png')";
	
	if(userID > 0)
	{
		jsState.setSyncMode();
		jsState.setCallbackHandler(userAccessUpdatedSuccess);
		jsState.setUserAccessState(<cfoutput>#session.appID#</cfoutput>,userID,accessLevel);
	}
	
}

userAccessUpdatedSuccess = function(result){
	location.reload();
}

</script>

</head>

<body>

<!--- Main Header --->
<cfinclude template="Assets/header.cfm">

<cfset iconPath = assetPathsCur.client.icon>
<cfset company = assetPathsCur.client.name>

<div style="width:810px; background-color:##333; height:32px; padding-left:0px; padding-top:8px; height:80px; float:none">

		<cfoutput>
        <div>
          <img src="#iconPath#" width="68" height="68" style="float:left; padding-right:10px" />
              <div class="bannerHeading" style="padding-top:10px;">#company#</div>
        </div>
          <div class="optionButtons" style="margin-top:16px"><a href="AppsView.cfm?appID=#session.appID#"><img src="images/app.png" width="44" height="44" alt="users" /></a></div>
          
        </cfoutput>
        
    </div>
    
     <div style="background-color:#666; padding-left:5px; margin-top:5px; height:46px; width:805px" class="sectionLink">

    <table width="800" border="0" style="padding-top:5px;position: absolute;">
      <tr>
        <td>
        <a href="usersView.cfm?assetTypeTab=0" class="<cfif assetTypeTab IS 0>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Users</a>
        <cfif assetTypeTab IS 0>
        <span class="subcontentLinkWhite">
[ <a href="usersView.cfm?assetTypeTab=0" class="subcontentLinkWhite" <cfif activeUser IS 0 AND userType IS 0>style="color:#FFF"</cfif>>All</a> | <a href="usersView.cfm?assetTypeTab=0&activeUser=1" class="subcontentLinkWhite" <cfif activeUser IS 1>style="color:#FFF"</cfif>>Active</a> | <a href="usersView.cfm?assetTypeTab=0&activeUser=-1" class="subcontentLinkWhite" <cfif activeUser IS -1>style="color:#FFF"</cfif>>InActive</a> |<a href="usersView.cfm?assetTypeTab=0&userType=1" class="subcontentLinkWhite" <cfif userType IS 1>style="color:#FFF"</cfif>> Clients</a> ]
        </span>
        </cfif>
         |
    <a href="usersView.cfm?assetTypeTab=1" class="<cfif assetTypeTab IS 1>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Guests</a> | 
    <a href="usersView.cfm?assetTypeTab=2" class="<cfif assetTypeTab IS 2>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Support</a>
    | <a href="usersView.cfm?assetTypeTab=3" class="<cfif assetTypeTab IS 3>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Group Access</a>
    </td> 
        <td align="right" class="function" style="color:#999">
        <cfif assetTypeTab IS 0>
        <table border="0">
          <tr>
            <td>Search</td>
            <td>
              <form action="usersView.cfm" method="post" name="searchUsers" id="searchUsers">
              	<cfoutput>
                <input name="search" type="text" id="search" value="#search#" />
                </cfoutput>
              </form>
            </td>
          </tr>
        </table>
        </cfif>
        </td>
      </tr>
    </table>

      </div>
<cfif assetTypeTab IS 0 OR assetTypeTab IS 1>
      <!--- Users --->
  <cfif assetTypeTab IS 0>
  	
    	<cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="curDate" />
    
  	  <!--- Modules --->
      <cfinvoke component="CFC.Modules" method="getAccessAppGroups" returnvariable="groups">
          <cfinvokeargument name="appID" value="#session.appID#"/>
      </cfinvoke>
  
       
       <cfif NOT newUser>
     <table width="810" style="margin-top:5px" border="0" cellpadding="5" cellspacing="0" bgcolor="#AAA">
       <tr>
         <td height="44" bgcolor="#AAAAAA" class="contentLinkWhite">
          <div style="width:auto; float:left; padding-top:5px; margin-right:10px" class="contentLinkWhite">
          <a href="exportAccounts.cfm" class="contentLinkWhite">Export User List</a></div> 
          <form action="CFC/Users.cfc?method=importAccountsXLS&appID=<cfoutput>#session.appID#</cfoutput>" method="post" enctype="multipart/form-data" name="importAssets" onsubmit="return importConfirm();">
              <div style="width:auto; float:left" class="contentLinkWhite" onclick="importCreateAccounts()">
              | <input name="importFile" id="importFile" type="file" class="formText sqButton" style="padding: 0px 0px 0px 0px; margin-right:5px; width:180px" onchange="displayImport();">
              <div style="float:right; width:auto; display:none; padding-left:5px" id="import">
              <input type="submit" value="Import Accounts" class="formText sqButton" style="height:28px; padding:4 4 4 4; border:hidden; background-color:#EEE"> Select Import to start Importing the Accounts
              </div>
              </div>
          </form>
          </td>
         <td width="44" align="right" bgcolor="#AAAAAA" class="contentLinkWhite"><a href="usersView.cfm?newUser=yes&assetTypeTab=0&userType=<cfoutput>#userType#</cfoutput>">
            <img src="images/add.png" width="44" height="44" border="0" />
            </a></td>
       </tr>
       </table>
    </cfif>
     
       <cfif newUser>
       
       <cfinvoke  component="CFC.Users" method="getAccessLevel" returnvariable="levels" />
       
       
       <form action="usersView.cfm" method="post" name="newUser" id="newUser">
     <table width="810" border="0" cellpadding="5" cellspacing="0" bgcolor="#EEE" style="margin-top:1px">

       <tr>
         <td height="32" colspan="9" align="left" bgcolor="#666666" class="subheading"><span class="contentGreyed">Create New User</span></td>
         <td height="32" align="left" bgcolor="#666666" class="subheading"><a href="http://localhost:8501/liveplatform-net/API/v11/usersView.cfm?assetTypeTab=0&amp;userType=1" class="contentLinkWhite">Cancel</a></td>
         </tr>
       <tr>
         <td width="20" align="right" class="subheading">Name</td>
         <td width="100" align="left" class="subheading"><input name="userName" type="text" class="formfieldcontent" id="userName" style="width:150px" maxlength="255" /></td>
         <td width="60" align="right" class="subheading">EMail</td>
         <td align="left" class="subheading"><input name="userEmailAddress" type="text" class="formfieldcontent" id="userEmailAddress" style="width:175px" maxlength="255" /></td>
         <td width="60" align="right" class="subheading">Pass</td>
         <td width="75" align="left" class="subheading"><input name="userPass" type="text" class="formfieldcontent" id="userPass" style="width:75px" maxlength="20" /></td>
         <td width="75" align="left" class="subheading">
           <select name="userLevel" id="userLevel" style="background:url(images/access_locked-1.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" onchange="updateUserAccess(this.form,0)">
              <cfoutput query="accessLevels">
              <cfif accessLevel NEQ 0>
              <option value="#accessLevel#" style="color:##333" <cfif levelName IS 'User'>selected="selected"</cfif>>#levelName#</option>
              </cfif>
              </cfoutput>
          </select>
          
           </td>
         <td width="75" align="left" class="subheading">Code</td>
         <td align="left" class="subheading">
           <input name="userCode" type="text" class="formfieldcontent" id="userCode" style="width:75px" maxlength="10" />
           <script type="text/javascript">
			  document.newUser.userCode.value = makeID(10,1);
		   </script>
         </td>
         <td width="69" align="right" class="subheading">
         <a onclick="createNewUser(<cfoutput>#userType#</cfoutput>)"><img src="images/ok.png" width="44" height="44" border="0" /></a>
         </td>
       </tr>
       <tr>
         <td colspan="10" align="right" bgcolor="#FFFFFF" class="subheading">&nbsp;</td>
         </tr>
     </table>
     </form>
     </cfif>
     
     <cfset cnt = '1'>
	
       <cfinvoke  component="CFC.Users" method="getallUsers" returnvariable="users">
      	   <cfinvokeargument name="appID" value="#session.appID#"/>
           <cfinvokeargument name="clientID" value="#session.clientID#"/>
           <cfinvokeargument name="activeUser" value="#activeUser#"/>
           <cfinvokeargument name="userType" value="#userType#"/>
           <cfinvokeargument name="search" value="#search#"/>
           <cfinvokeargument name="searchByLast" value="#searchByLast#"/>
           <cfinvokeargument name="accessLevel" value="#access#"/>
       </cfinvoke>
   
   	   <cfset userCnt = 0>
    
    <!--- user search params --->
    	<cfset searchLet = "ALL,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z">
       <cfset theLink = "usersView.cfm?assetTypeTab=0&activeUser=#activeUser#&userType=#userType#">
    
    <cfoutput>
    <table width="810" border="0" cellpadding="5" cellspacing="0">
       <cfif userCnt IS 0>
       <tr>
         <td height="44" colspan="7" align="center" bgcolor="##333333">
         <cfloop index="aLet" list="#searchLet#">
         <cfif searchByLast IS aLet>
         	<div style="width:auto; padding:0 5px 0 0; float:left; cursor:pointer">
         	 <span class="contentLinkWhite">#aLet#</span>
             </div>
			<cfelse>
             <a href="#theLink#&searchByLast=<cfif aLet NEQ 'All'>#aLet#</cfif>&access=#access#" class="contentLinkDisabled">
             <div style="width:auto; padding:0 5px 0 0; float:left; cursor:pointer">#aLet#</div>
             </a>
         </cfif>
         </cfloop>
         | 
         <cfif access NEQ -1>
         <a href="#theLink#&searchByLast=#searchByLast#&access=-1" class="contentLinkDisabled">All</a> |
         <cfelse>
         <span class="contentLinkWhite">All</span> |
		 </cfif>
         <cfloop query="accessLevels">
         	<cfif access IS accessLevel>
            <span class="contentLinkWhite">#levelName#</span> |
            <cfelse>
         	<a href="#theLink#&access=#accessLevel#&searchByLast=#searchByLast#" class="contentLinkDisabled">#levelName#</a> |
            </cfif>
         </cfloop>
         </td>
       </tr>
    </cfif>
    </cfoutput>
       
    <cfoutput query="users">
 
       <!--- <cfinvoke  component="CFC.Users" method="getLastUserSession" returnvariable="lastSession">
      	   <cfinvokeargument name="userID" value="#user_id#"/>
       </cfinvoke> --->
       
	   <cfset appID = session.appID>
       
      <cfif active>
        	<cfset state = "contentLink">
        <cfelse>
        	<cfset state = "contentLinkDisabled">
      </cfif>
        
        <cfinvoke  component="CFC.Tokens" method="getUserTokens" returnvariable="userToken">
      	   <cfinvokeargument name="userID" value="#user_id#"/>
       </cfinvoke>
       
        <cfset aToken = userToken.token> 
       
           <div class="rowhighlighter" onmouseover="showDelete('deleteUser_#cnt#',1)" onmouseout="showDelete('deleteUser_#cnt#',0)">
           <table width="810" border="0" cellpadding="5" cellspacing="0">
           <cfif userCnt IS 0>
             <tr>
             <td bgcolor="##666" class="contentGreyed">User Name</td>
             <td width="10" align="center" bgcolor="##666" class="contentGreyed">DEV</td>
             <td bgcolor="##666" class="contentGreyed">&nbsp;</td>
             <td bgcolor="##666" class="contentGreyed">&nbsp;</td>
             <td width="100" align="right" bgcolor="##666" class="contentGreyed">Password</td>
             <td width="70" align="center" bgcolor="##666" class="contentGreyed">Access</td>
             <td width="70" align="center" bgcolor="##666" class="contentGreyed">Active</td>
             <td width="44" align="center" bgcolor="##666" class="contentGreyed">&nbsp;</td>
           </tr>
           </cfif>
           <tr>
             <td class="#state#">
             <div id="contentAsset_#cnt#" style="height:32px; width:210px; padding-left:4px; cursor:pointer" onclick="alert('me')">
             <cfif name IS ''>No Name<cfelse>
               <span class="contentLinkGrey">#name#</span>
             </cfif><br />
             <cfif email IS ''>No Email Address<cfelse>
               <a href="mailto:#email#?subject=ONE2 Support" class="#state#" id="contentEmail_#cnt#">#email#</a>
             </cfif>
             </div>
             </td>
             <td align="left" class="content" width="10">
                 <cfif dev IS 1>
                    <cfset devState = 0>
                 <cfelse>
                    <cfset devState = 1>
                 </cfif>
                 <input name="dev" type="checkbox" id="dev_#user_id#" value="#user_id#" <cfif dev IS 1> checked</cfif> onClick="setDevState(this.value,#devState#)"/><label for="dev_#user_id#"></label>
              </td>
             <cfif groups.recordCount GT 0><cfset w = 200><cfelse><cfset w = 44></cfif>
             <td align="left" class="content" width="#w#">
                 
                 <table width="100%" border="0" cellpadding="0" cellspacing="5">
                 <tr>
                  
                         <td width="44" align="center" valign="middle">
                         <cfif users.access_id GT 0 AND aToken NEQ ''>
                         <div onclick="showReports('#aToken#')" style="width:44px">
                         <img src="images/reports.png" width="22" height="22" border="0" />
                         </div>
                         <cfelse>
                         <div style="width:44px"></div>
                         </cfif>
                         </td>
                        
                        <cfif groups.recordCount GT 0>
                         <td width="44" align="center" valign="middle">
                         
                         
                        <cfinvoke  component="CFC.Access" method="getUserGroupAccess" returnvariable="groupAccess">
                            <cfinvokeargument name="userID" value="#user_id#"/>
                        </cfinvoke> 
                        <!--- group access for user --->
                         <select multiple="multiple" placeholder="Groups" onchange="setGroupAccessForUser($(this).val(),#user_id#);" class="testSelAll">
                               <cfloop query="#groups#">
                                        
                                  <cfif arrayFind(groupAccess,groups.group_id)>
                                      <option selected value="#groups.group_id#">#groups.name#</option>
                                  <cfelse>
                                      <option value="#groups.group_id#">#groups.name#</option>
                                  </cfif>
                               
                               </cfloop>
                        </select>
                        
                      </td>
                      </cfif>
                   </tr>
                 </table>
    
             </td>
             
             <td colspan="-1" align="right" class="content" width="80">
             <!--- <cfif year(lastSession) IS '1900'> --->
             <div onclick="resendRegInfo('#trim(email)#')" style="width:44px">
             <table width="100%" border="0" cellpadding="0" cellspacing="5">
              <tr>
                <td align="right" valign="middle">RS</td>
                <td width="22" valign="middle"><img src="images/resend.png" width="22" height="22" border="0" /></td>
              </tr>
            </table>
              </div>
    <!---          <cfelse>
             #dateFormat(lastSession,'MMM DD YY')#
             </cfif> --->
             </td>
             <td width="100" align="left" class="content">
             <cfif password NEQ ''>
             <div style="width:44px">
             #password#
             </div>
             <cfelse>
             <div onclick="resendSetPassword('#trim(email)#')">
             <table width="100%" border="0" cellpadding="0" cellspacing="5">
              <tr>
                <td align="right" valign="middle">Set</td>
                <td width="22" valign="middle"><img src="images/resend.png" width="22" height="22" border="0" /></td>
              </tr>
            </table>
              </div>
             </cfif>
             </td>
             <td width="70" align="center" class="content">
             <cfif access_id IS 0>
                  <cfset lock = 'access_unlocked'>
              <cfelse>
                  <cfset lock = 'access_locked'>
              </cfif>
              <form>
             <select name="userLevel" id="userLevel" style="background:url(images/#lock#-#users.access_id#.png) no-repeat; border:0; width:44px; height:44px;color:rgba(0,0,0,0);" onchange="updateUserAccess(this.form,#user_id#)">
                  <cfloop query="accessLevels">
                  <cfif accessLevel GTE 0>
                  <option value="#accessLevel#" style="color:##333" <cfif users.access_id IS accessLevels.accessLevel>selected="selected"</cfif>>#levelName#</option>
                  </cfif>
                  </cfloop>
              </select>
              </form>
             </td>
             <td width="70" align="center" class="content">
             <cfif active IS '0'>
				<cfset stateCSS = "contentLinkGreen">
              <cfelse>
                <cfset stateCSS = "contentLinkRed">
             </cfif>
             <div id="contentActive">
              <a id="activeState_#cnt#" class="#stateCSS#" style="cursor:pointer" onclick="toggleActiveState('#cnt#',#appID#,#user_id#)">
              <cfif active IS '0'>
              NO
              <cfelse>
              YES
              </cfif>
              </a>
              </div>
             </td>
             <!--- <td width="75" align="center" class="content">#code#</td> --->
             <td width="44" align="right" class="content">
             <input name="deleteUser_#cnt#" type="button" class="itemHide" id="deleteUser_#cnt#" style="background:url(images/remove.png) no-repeat; border:0; width:44px; height:44px;" onClick="deleteUser(#user_id#);" value="" />
             </td>
           </tr>
           </table>
      </div>
      
       <cfset cnt += 1>
       <cfset userCnt = 1>
       
    </cfoutput>
       
       </cfif>
      
  <!--- Guests --->
          <cfif assetTypeTab IS 1>
          
          <cfinvoke  component="CFC.Users" method="getGuestUsers" returnvariable="questCount">
               <cfinvokeargument name="appID" value="#session.appID#"/>
           </cfinvoke>
          
            <cfinvoke component="CFC.Users" method="getGuestGeneralInfo" returnvariable="guestInfo">
                <cfinvokeargument name="appID" value="#session.appID#"/>
                <cfinvokeargument name="clientID" value="#session.clientID#"/>
            </cfinvoke>
          
            <cfoutput>
              <table width="810" style="margin-top:5px" border="0" cellpadding="0" cellspacing="10" bgcolor="##EEE">
                <tr>
                  <td class="plainLinkDkGrey"> Total Guests #questCount#</td>
                </tr>
              </table>
            </cfoutput>
            
           <table width="810" style="margin-top:5px" border="0" cellpadding="10" cellspacing="0">
			
            <cfset theCountry = "">
            <cfset theCity = "">
            <cfset theRegion = "">

            
           <cfoutput query="guestInfo">
           
           <cfif theCountry NEQ country_name>
              <tr>
                    <td colspan="5" bgcolor="##FFF">&nbsp;</td>
             </tr>
               <tr>
                    <td colspan="5" bgcolor="##CCC" class="plainLinkDkGrey">#country_name#</td>
             </tr>
               <cfset theCountry = country_name>
           
           </cfif>
           <cfif theRegion NEQ region_name>
           
                <tr>
                	<td width="20">&nbsp;</td>
                    <td colspan="4" bgcolor="##999" class="plainLinkDkGrey">#region_name#</td>
             </tr>
                <cfset theRegion = region_name>
            
            </cfif> 
           <cfif theCity NEQ city>
               <tr>
               		<td width="20">&nbsp;</td>
               	    <td width="20">&nbsp;</td>
                    <td colspan="3" bgcolor="##BBB" class="plainLinkDkGrey">#city#</td>
             </tr>
               <cfset theCity = city>
           
           </cfif>    
           
            
              <tr bgcolor="###iif(currentrow MOD 2,DE('ffffff'),DE('efefef'))#">
              <td width="20">&nbsp;</td>
              <td width="20">&nbsp;</td>
                <td width="20" align="center" class="subheading">
                <cfif os IS 'ios'>
                	<img src="http://www.liveplatform.net/images/icons/apple.png" />
                <cfelseif os IS 'android'>
                	<img src="http://www.liveplatform.net/images/icons/android.png" />
                <cfelse>
                </cfif>
                </td>
                <td align="left" class="contentLinkGrey">#name#</td>
                <td align="right" class="subheading"><a href="##" class="contentLink">Details</a></td>
              </tr>

            </cfoutput> 
            </table>
            
          </cfif>
     	
         <cfelseif assetTypeTab IS 3>
         
        	<!--- Modules --->
          <cfinvoke component="CFC.Modules" method="getAccessAppGroups" returnvariable="groups">
              <cfinvokeargument name="appID" value="#session.appID#"/>
          </cfinvoke>
      
            <cfloop query="groups">
			
            <cfinvoke component="CFC.Access" method="getUserAccess" returnvariable="accessUsers">
                <cfinvokeargument name="appID" value="#session.appID#"/>
                <cfinvokeargument name="groupID" value="#group_id#"/>
            </cfinvoke>
            
            <cfoutput>
            
              <div style="width:810px">
              <!--- Group Name --->
           	  <div class="content" style="float:left; height:32px; width:100%; background-color:##CCC; border-top:thin ##666 solid">
                <div class="contentLinkGrey" style="float:left; padding-top:6px; width:600px; padding-left:5px;">#name#</div>
              </div>
              
              <!--- a user added to group --->
              <cfif arrayLen(accessUsers) GT 0>
              <cfset allGuests = arrayNew(1)>
              
              <cfloop index="aUser" array="#accessUsers#">
               
                <cfif aUser.name IS 'guest'>
                <!--- compile guests together --->
                
                  <cfset arrayAppend(allGuests,aUser.user_id)>
                  
               	<cfelse>
                <!--- normal users --->
                  <div class="content" style="float:left; height:44px; width:100%;border-top:thin ##666 solid">
                    <div class="contentLinkGrey" style="float:left; padding-top:12px; width:750px; padding-left:5px;">
                    	<div style="float:left; width:290px; padding-left:5px;">#aUser.name#</div>
                    	<div style="float:right; width:290px; text-align:right;">#aUser.email#</div>
                    </div>
                    <div style="float:right">
                    <a onclick="removeUser(#group_id#,[#aUser.user_id#])">
                    <img src="images/remove.png" width="44" height="44" border="0" />
                    </a>
                    </div>
                  </div>
                 
                 </cfif>

              </cfloop>
              
              <cfif arrayLen(allGuests) GT 0>
                 <div class="content" style="float:left; height:44px; width:100%;border-top:thin ##666 solid">
                    <div class="contentLinkGrey" style="float:left; padding-top:12px; width:750px; padding-left:5px;">
                    	<div style="float:left; width:290px; padding-left:5px;">Guests</div>
                    	<div style="float:right; width:290px; text-align:right;">Total #arrayLen(allGuests)#</div>
                    </div>
                    <div style="float:right">
                    <a onclick="removeUser(#group_id#,[#arrayToList(allGuests)#])">
                    <img src="images/remove.png" width="44" height="44" border="0" />
                    </a>
                    </div>
                  </div>
                  </cfif>

              </cfif>
              
              </cfoutput>

            </cfloop>

            
         <cfelse>
         <div style="float:left">
         <!--- Support --->
          <form method="post" action="sendsupportemail.cfm">
           <table width="800" border="0" cellpadding="5" cellspacing="5">
          <tr class="content">
            <td width="60" align="right" valign="top">&nbsp;</td>
            <td class="bundleid">The following message will be sent to all current active users</td>
          </tr>
          <tr class="content">
            <td align="right" valign="middle">Level</td>
            <td>
              <select name="accessLevel" id="accessLevel">
              	  <option value="0" style="color:##333">All Users</option>
                <cfoutput query="accessLevels">
				  <cfif accessLevel GT 0>
                  <cfif accessLevel IS 1>
                  	<option value="#accessLevel#" style="color:##333" selected>#levelName#</option>
                  <cfelse>
                  	<option value="#accessLevel#" style="color:##333">#levelName#</option>
                  </cfif>
                  </cfif>
                </cfoutput>
            </select>
            </td>
          </tr>
          <tr class="content">
            <td align="right" valign="middle">From Email</td>
            <td><label for="supportEmail"></label>
            <input name="supportEmail" type="text" class="formfieldcontent" id="supportEmail" value="support@liveplatform.net" size="60" /></td>
          </tr>
          <tr>
            <td align="right" valign="middle"><span class="content">Subject</span></td>
            <td><input name="supportSubject" type="text" class="formfieldcontent" id="supportSubject" value="Support Notification" size="60" /></td>
          </tr>
          <tr>
            <td align="right" valign="top"><span class="content">Message</span></td>
            <td><label for="supportMessage"></label>
            <textarea name="supportMessage" cols="100" rows="10" class="content" id="supportMessage"></textarea></td>
          </tr>
          <tr>
            <td align="right" valign="top">&nbsp;</td>
            <td><input type="submit" name="button" id="button" value="Send Support EMail" /></td>
          </tr>
           </table>
 		 </form>
         </div>
      
</cfif>



</body>
</html>