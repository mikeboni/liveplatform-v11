<cfcomponent>
 
<!--- <cfheader name="Access-Control-Allow-Origin" value="*"> --->

<!--- method=trackQuizResponse&auth_token=2BC66C70-BA9B-F3FA-FECC680E3E73E618&groupID=1888&assetID=2561&date=1436446358&duration=120&selection=133,134,117,118&numberOfTries=3 --->
<!--- Save Quiz Response --->
<cffunction name="trackQuizResponse" access="remote" returntype="string" returnformat="plain" output="no" hint="Caputres Response from Quiz Asset">

	<cfargument name="groupID" type="numeric" required="yes" default="0">
    <cfargument name="assetID" type="numeric" required="yes" default="0">
    <cfargument name="auth_token" type="string" required="yes" default="">
    <cfargument name="date" type="numeric" required="no" default="0">
    <cfargument name="duration" type="numeric" required="no" default="0">
    <cfargument name="selection" type="string" required="no" default="">
    <cfargument name="numberOfTries" type="numeric" required="no" default="-1">
    <cfargument name="currentTime" type="numeric" required="no" default="0">

    <cfinvoke component="CFC.Tracking" method="trackQuizResponse" returnvariable="success">
        <cfinvokeargument name="groupID" value="#groupID#"/>
        <cfinvokeargument name="assetID" value="#assetID#"/>
        <cfinvokeargument name="auth_token" value="#auth_token#"/>
        <cfinvokeargument name="date" value="#date#"/>
        <cfinvokeargument name="duration" value="#duration#"/>
        <cfinvokeargument name="selection" value="#selection#"/>
        <cfinvokeargument name="numberOfTries" value="#numberOfTries#"/>
        <cfinvokeargument name="currentTime" value="#currentTime#"/>
    </cfinvoke>
    
    <cfset data = structNew()>
    
    <cfif success>
		<!---ok--->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
    <cfelse>
    	<!--- Failed --->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="3000"/>
        </cfinvoke>
    </cfif>
    
    <cfheader name="Access-Control-Allow-Origin" value="*">
    <cfset structAppend(data,{"error":#error#})> 
    <cfset JSON = serializeJSON(data)>
    
    <cfreturn JSON>

</cffunction>


<!---Get Content Asset--->

    <cffunction name="getContentAsset" access="remote" returntype="string" returnformat="plain" output="no" hint="Gets application localization file">
    
        <cfargument name="auth_token" type="string" required="yes" default="0">
		<cfargument name="assetID" type="numeric" required="yes" default="0">
        
        <cfset theAsset = structNew()>
        
        <!--- OK Error --->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <cfset  data = structNew()>

        <!--- Token Info --->
        <cfinvoke component="CFC.Tokens" method="tokenValid" returnvariable="validToken">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
     
        <cfif validToken>
        
            <cfinvoke component="CFC.Modules" method="getGroupAssets" returnvariable="data">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
            
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})> 
        
        <cfset JSON = serializeJSON(data.assets[1])>
        <cfreturn JSON> 
        
    </cffunction> 




<!--- Get Bookmarks --->
<cffunction name="getBookmarks" access="remote" returntype="string" returnformat="plain" output="no" hint="Adds Video Bookmarks">

    <cfargument name="auth_token" type="string" required="yes">
    <cfargument name="groupID" type="numeric" required="no" default="0">
    <cfargument name="assetID" type="numeric" required="no" default="0">
    
    <cfinvoke component="CFC.Tracking" method="getBookmarks" returnvariable="allBookmarks">
        <cfinvokeargument name="groupID" value="#groupID#"/>
        <cfinvokeargument name="assetID" value="#assetID#"/>
        <cfinvokeargument name="auth_token" value="#auth_token#"/>
    </cfinvoke>    
	
   <cfheader name="Access-Control-Allow-Origin" value="*">
    <cfset JSON = serializeJSON(allBookmarks)>
    
    <cfreturn JSON>
    
</cffunction>    


<!--- Customer Registration --->
<cffunction name="customerRegistration" access="remote" returntype="string" returnformat="plain" output="no" hint="Capture Registration Info">

    <cfargument name="firstName" type="string" required="no" default="unknown">
    <cfargument name="lastName" type="string" required="no" default="">
    <cfargument name="fullName" type="string" required="no" default="">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="phone" type="string" required="no" default="">
    <cfargument name="postalZip" type="string" required="no" default="">
    <cfargument name="city" type="string" required="no" default="">
    <cfargument name="howHear" type="numeric" required="no" default="1">
    <cfargument name="auth_token" type="string" required="yes">
    
    <cfargument name="ip" type="string" required="no" default="">
    <cfargument name="projectID" type="numeric" required="no" default="0">
    <cfargument name="groupID" type="numeric" required="no" default="0">
    
    <cfif groupID NEQ 0> <cfset projectID = groupID></cfif>
    
    <!---ok--->
    <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
        <cfinvokeargument name="error_code" value="1000"/>
    </cfinvoke>
    
    <cfset data = structNew()>
    
    <cfif fullName NEQ ''>
    	<cfset firstName = ListGetAt(fullName,1,' ')>
        <cfif listLen(fullName,' ') GT 1>
        	<cfset lastName = ListGetAt(fullName,2,' ')>
        </cfif>
    </cfif>
    
    <cfinvoke component="CFC.Customers" method="customerRegistration" returnvariable="sucess">
        <cfinvokeargument name="firstName" value="#firstName#"/>
        <cfinvokeargument name="LastName" value="#lastName#"/>
        <cfinvokeargument name="email" value="#email#"/>
        <cfinvokeargument name="phone" value="#phone#"/>
        <cfinvokeargument name="postalZip" value="#postalZip#"/>
        <cfinvokeargument name="city" value="#city#"/>
        <cfinvokeargument name="howHear" value="#howHear#"/>
        <cfinvokeargument name="auth_token" value="#auth_token#"/>
        <cfinvokeargument name="ip" value="#ip#"/>
        <cfinvokeargument name="projectID" value="#projectID#"/>
    </cfinvoke>    
	
   <cfheader name="Access-Control-Allow-Origin" value="*">
    <cfset structAppend(data,{"error":#error#})>
    <cfset JSON = serializeJSON(data)>
    
    <cfreturn JSON>
    
</cffunction> 


<!--- Customer Registration --->
<cffunction name="getHowHearList" access="remote" returntype="string" returnformat="plain" output="no" hint="Returns how hear list for registration">
	
    <cfinvoke component="CFC.Customers" method="getHowHear" returnvariable="howHearList" />
    
    <!---ok--->
    <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
        <cfinvokeargument name="error_code" value="1000"/>
    </cfinvoke>
    
    <cfset data = structNew()>
    
    <cfset structAppend(data,{"list":#howHearList#})>
    <cfset structAppend(data,{"error":#error#})>
    
    <cfheader name="Access-Control-Allow-Origin" value="*">
    <cfset JSON = serializeJSON(data)>
    
    <cfreturn JSON>
    
</cffunction>


<!--- Sync Bookmarks for User --->
<cffunction name="syncBookmarks" access="remote" returntype="string" returnformat="plain" output="no" hint="Clear alll Video Bookmarks for user">

    <cfargument name="auth_token" type="string" required="yes" default="">
	<cfargument name="bookmarks" type="string" required="yes" default="">
    
    <cfset data = structNew()>
    
    <cfset bookmarks = deserializeJSON(bookmarks)>
    
    <!---ok--->
    <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
        <cfinvokeargument name="error_code" value="1000"/>
    </cfinvoke>
 
    <cfloop item="theKey" collection="#bookmarks#">
		
		<cfloop index="anObject" array="#bookmarks[theKey]#">
        
			<!--- Add --->   
            <cfinvoke component="CFC.Tracking" method="setBookmark" returnvariable="success">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
                <cfinvokeargument name="groupID" value="#theKey#"/>
                <cfinvokeargument name="assetID" value="#anObject.assetID#"/>
                <cfinvokeargument name="currentTime" value="#anObject.marker#"/>
                <cfinvokeargument name="watched" value="#anObject.watched#"/>
            </cfinvoke>
    	
        </cfloop>
        
    </cfloop>
    
    <cfset structAppend(data,{"error":#error#})>
    
    <cfheader name="Access-Control-Allow-Origin" value="*">
    <cfset JSON = serializeJSON(data)>
    
    <cfreturn JSON>

</cffunction>



<!--- Clear All Bookmarks for User --->
<cffunction name="clearBookmarks" access="remote" returntype="string" returnformat="plain" output="no" hint="Clear alll Video Bookmarks for user">

    <cfargument name="auth_token" type="string" required="yes" default="">
	
    <cfinvoke component="CFC.Tracking" method="removeBookmarks" returnvariable="success">
        <cfinvokeargument name="auth_token" value="#auth_token#"/>
    </cfinvoke>
    
    <cfif success>
    
		<!---ok--->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
    
    <cfelse>
    	
        <!---failed--->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1003"/>
        </cfinvoke>
        
    </cfif>
       
    <cfset structAppend(data,{"error":#error#})>
    
    <cfheader name="Access-Control-Allow-Origin" value="*">
    <cfset JSON = serializeJSON(theData)>
    
    <cfreturn JSON>

</cffunction>




<!--- Get Type Paths --->
<cffunction name="objectTypes" access="remote" returntype="string" returnformat="plain" output="no" hint="Dumps all asset IDs for tracking in the database. This is for offline mode where asset IDS are sotres and  when online dumps all asset IDs">
        
		<cfquery name="typePaths">
            SELECT       assetType_id, name, path, icon
            FROM         AssetTypes
            WHERE        active = 1
        </cfquery>
        
        <!--- QueryToStruct --->
         <cfinvoke component="CFC.Misc" method="QueryToStruct" returnvariable="theData">
            <cfinvokeargument name="query" value="#typePaths#"/>
         </cfinvoke>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(theData)>
        
        <cfreturn JSON>
        
</cffunction>

<!--- Check User Info --->
<cffunction name="trackAssets" access="remote" returntype="string" returnformat="plain" output="no" hint="Dumps all asset IDs for tracking in the database. This is for offline mode where asset IDS are sotres and  when online dumps all asset IDs">
    	
        <cfargument name="auth_token" type="string" required="yes" default="">
        <cfargument name="assets" type="string" required="yes" default="0">
        
		<!--- [ { assetID:0, groupID:0, date:12123432, length:140 } , ... --->
        
        <cfset data = structNew()>
        
        <cfinvoke component="CFC.Tracking" method="trackContentAssets" returnvariable="success">
            <cfinvokeargument name="assets" value="#assets#">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
        
        <!---OK--->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>
        
        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
        
</cffunction>


<!--- Check User Info --->
<cffunction name="validateUserInfo" access="remote" returntype="string" returnformat="plain" output="no" hint="Checks whether all user information has been provided.">
    	
        <cfargument name="code" type="string" required="yes" default="">
        <cfargument name="email" type="string" required="no" default="">
        <cfargument name="bundleID" type="string" required="yes" default="">
        
        <cfinvoke component="CFC.Users" method="validateUserInfo" returnvariable="data">
            <cfinvokeargument name="code" value="#code#">
            <cfinvokeargument name="email" value="#email#">
            <cfinvokeargument name="bundleID" value="#bundleID#">
        </cfinvoke>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
        
        <cfreturn userInfo>
        
</cffunction>



<!--- Set User Info --->
<cffunction name="setUserInfo" access="remote" returntype="string" returnformat="plain" output="no" hint="Sets user information">
    	
        <cfargument name="code" type="string" required="yes" default="">
        
        <cfargument name="name" type="string" required="yes" default="">
        <cfargument name="email" type="string" required="yes" default="">
        <cfargument name="pass" type="string" required="yes" default="">
        
        <cfargument name="auth_token" type="string" required="yes" default="">
        
        <cfargument name="reset" type="boolean" required="no" default="no">
        
        <cfset data = structNew()>
        
        <cfinvoke component="CFC.Users" method="setUserInfo" returnvariable="result">
        	<cfinvokeargument name="code" value="#code#">
            <cfinvokeargument name="name" value="#name#">
            <cfinvokeargument name="email" value="#email#">
            <cfinvokeargument name="pass" value="#pass#">
            <cfinvokeargument name="auth_token" value="#auth_token#">
            <cfinvokeargument name="reset" value="#reset#">
        </cfinvoke>
        
        <cfif NOT result>
        
        	<!---Missing Data--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1005"/>
            </cfinvoke>

        
        <cfelse>
        
			<!---ok--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
        
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
        
</cffunction>


<!--- Update Customer User Info --->
<cffunction name="UpdateCustomerInfo" access="remote" returntype="string" returnformat="plain" output="no" hint="Sets user information">
    	
        <cfargument name="userInfo" type="string" required="yes">
        <cfargument name="auth_token" type="string" required="yes">
        <cfargument name="custID" type="numeric" required="no" default="0">
        <cfargument name="unitID" type="numeric" required="no" default="0">
        
        <cfset Info = deserializeJSON(userInfo)>
        
        <!---ok--->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <!--- check clientID --->
        <cfif custID IS 0>
        	
            <!---no user match--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1009"/>
            </cfinvoke>
            
        <cfelse>
        
			<!--- update customer info --->
            <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
    
            <cfset userID = userInfo.user_id>
        
        	<cfif userID IS ''>
        	
            <!---no user match--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1009"/>
            </cfinvoke>
            
        <cfelse>
        
			<!--- accessLevel --->
            <cfinvoke component="CFC.Users" method="getUserAccess" returnvariable="userAccess">
                <cfinvokeargument name="userID" value="#userID#"/>
            </cfinvoke>
           
            <cfset accessLevel = userAccess.accessLevel>
 
            <cfif accessLevel IS 4>
 
                <!--- update info --->
                <cfinvoke component="LiveAPI" method="UpdateUserInfo" returnvariable="updated">
                    <cfinvokeargument name="userInfo" value="#Info#"/>
                    <cfinvokeargument name="userID" value="#custID#"/>
                    <cfinvokeargument name="userType" value="1"/>
                </cfinvoke>
                
                <cfif structKeyExists(Info,'ref')>
                
					<!--- update ref info on suite --->
                    <cfinvoke component="CFC.Units" method="UpdateRefNumber" returnvariable="updated">
                        <cfinvokeargument name="unitID" value="#unitID#"/>
                        <cfinvokeargument name="refNum" value="#Info.ref#"/>
                    </cfinvoke>
                    
            	</cfif>
                
            <cfelse>
            
                <!---Access Denied--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1021"/>
                </cfinvoke>
            
            </cfif>
		
        </cfif>
        
        </cfif>
        
        <cfset data = structNew()>

        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
        
</cffunction>



<!--- Update User Info --->
<cffunction name="UpdateUserInfo" access="remote" returntype="string" returnformat="plain" output="no" hint="Sets user information">
    	
        <cfargument name="userInfo" type="struct" required="yes">
        <cfargument name="auth_token" type="string" required="no" default="">
        <cfargument name="userID" type="numeric" required="no" default="0">
        <cfargument name="userType" type="numeric" required="no" default="0">
        
        <!--- update user info --->
        <cfinvoke component="CFC.Users" method="updateUserInfo" returnvariable="updated">
            <cfinvokeargument name="userInfo" value="#userInfo#"/>
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
            <cfinvokeargument name="userID" value="#userID#"/>
            <cfinvokeargument name="userType" value="#userType#"/>
        </cfinvoke>
      
        <cfset data = structNew()>
        
        <cfif updated>
			<!---ok--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
        <cfelse>
        	<!--- failed --->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1009"/>
            </cfinvoke>
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
        
</cffunction>



<!--- Get App Support Links --->
<cffunction name="getAppSupportLinks" access="remote" returntype="query" hint="Gets Support Links for Web, AppStore and GooglePlay Store - If defined in setup">
    	
        <cfargument name="clientID" type="numeric" required="yes" default="0">
        <cfargument name="appID" type="numeric" required="no" default="0">
        <cfargument name="type" type="string" required="no" default="website">
	
    	<cfquery name="appLinks">
            SELECT        Applications.app_id, Applications.client_id, Support.url, Support.urlName, AccessLevels.accessLevel AS accessLevel
            FROM          AccessLevels INNER JOIN
                          Support ON AccessLevels.access_id = Support.access_id RIGHT OUTER JOIN
                          Applications ON Support.app_id = Applications.app_id
            WHERE        (Support.type = 1)
            <cfif clientID GT '0'>
            AND client_id = #clientID#
            </cfif>
            <cfif appID GT '0'> 
            AND Applications.app_id = #appID#
            </cfif>
        
        </cfquery>
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfreturn appLinks>

</cffunction>


<!---Get Application Prefs--->
<!---getAppPrefs&bundleID=com.liveads.sindicatocigars&os=ios--->

	<cffunction name="getAppPrefs" access="remote" returntype="string" returnformat="plain" output="no" hint="Gets all App Preferences needed for Application setup">
    	
        <cfargument name="bundleID" type="string" required="no" default="">
        <cfargument name="appID" type="numeric" required="no" default="0" hint="AppID if no BundleID">
        <cfargument name="os" type="string" required="no" default="" hint="specifying the os will provide only the appLinks for the specific os">
        <cfargument name="serverFURL" type="string" required="no" default="" hint="Forwarding Server Link">
        <cfargument name="update" type="boolean" required="no" default="no" hint="Update Json">
        <cfargument name="override" type="boolean" required="no" default="false" hint="Override Active State">
        <cfargument name="addPath" type="string" required="no" default="">
        
        <cfif appID IS 0>
        
			<!--- Get AppID from BundleID --->
            <cfinvoke component="CFC.Apps" method="getAppID" returnvariable="appInfo">
                <cfinvokeargument name="bundleID" value="#bundleID#">
                <cfinvokeargument name="override" value="#override#">
            </cfinvoke>
         
            <cfif appInfo.error.error_code NEQ 1000>
            
            	<cfset JSON = serializeJSON(appInfo)>
                <cfreturn JSON>
        	</cfif>
            
            <cfset appID = appInfo.app_id>
         
        </cfif>

        <!--- Get JSON Path --->
        <cfinvoke component="CFC.Apps" method="getJSONPath" returnvariable="JSONPath">
        	<cfinvokeargument name="appID" value="#appID#">
        	<cfinvokeargument name="addPath" value="#addPath#">
        </cfinvoke>

		<cfset prefsPath = JSONPath & "appPrefs.json">
		
        <cfif update>
        	<!--- Force Update --->
            
            <!--- Get JSON from Server --->
            <cfinvoke component="CFC.Apps" method="generateAppPrefs" returnvariable="data">
                <cfinvokeargument name="bundleID" value="#bundleID#">
                <cfinvokeargument name="appID" value="#appID#">
                <cfinvokeargument name="serverFURL" value="#serverFURL#">
                <cfinvokeargument name="os" value="#os#">
                <cfinvokeargument name="override" value="#override#">
            </cfinvoke>
 																																												    
            <cfset JSON = serializeJSON(data)>
            <cffile action="write" file="#prefsPath#" output="#JSON#" charset="utf-8"> 

        <cfelse> 
        	<!--- Normal Checks --->
			<cfif fileExists(prefsPath) AND NOT override>
                <!--- Read JSON File --->
                <cffile action = "read" file = "#prefsPath#" variable = "JSON" charset="utf-8">
  
            <cfelse>
    			
                <!--- Get JSON from Server --->
                <cfinvoke component="CFC.Apps" method="generateAppPrefs" returnvariable="data">
                    <cfinvokeargument name="bundleID" value="#bundleID#">
                    <cfinvokeargument name="serverFURL" value="#serverFURL#">
                    <cfinvokeargument name="os" value="#os#">
                    <cfinvokeargument name="override" value="#override#">
                </cfinvoke>
   																																										  		
                <cfset JSON = serializeJSON(data)>
                <cffile action="write" file="#prefsPath#" output="#JSON#" charset="utf-8"> 
        
            </cfif>
		
        </cfif>
	
       <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfreturn JSON>
        
    </cffunction>


<!---Check Version MB--->
<!---getVersionUpdate&app_id=com.liveadz.libertymutual&app_version=1000000--->
    <cffunction name="getVersionUpdate" access="remote" returntype="string" returnformat="JSON" output="no" hint="Checks whether application is up-to-date">
            
            <cfargument name="bundleID" type="string" required="yes" default="0">
            <cfargument name="version" type="numeric" required="yes" default="0">
        	<cfargument name="os" type="string" required="no" default="" hint="specifying the os will provide only the appLinks for the specific os">
        	
			<cfinvoke component="CFC.Apps" method="getVersionUpdate" returnvariable="data">
                <cfinvokeargument name="bundleID" value="#bundleID#">
                <cfinvokeargument name="version" value="#version#">
                <cfinvokeargument name="os" value="#os#">
            </cfinvoke>
            
            <!--- Get AppID from BundleID --->
            <cfinvoke component="CFC.Apps" method="getAppID" returnvariable="appInfo">
                <cfinvokeargument name="bundleID" value="#bundleID#">
            </cfinvoke>
            
            <cfset appID = appInfo.app_id>
            
            <cfinvoke  component="CFC.Apps" method="getVersionUpdate" returnvariable="theLinks">
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>

            <!--- <cfif structKeyExists(theLinks.appLinks,'registration')>
                <cfset structAppend(data,{"registration":"#theLinks.appLinks.registration#"})>
            </cfif> --->
            <cfheader name="Access-Control-Allow-Origin" value="*">
            <cfset JSON = serializeJSON(data)>
     
        	<cfreturn JSON>
            
            <!---{"error":{"error_code":1007,"error_message":"Forced version update"},"download_url":"http://www.klokwerks.com/apps/lanterraapp/"}--->
            
    </cffunction>
    
    
    <!---getVersionDate--->
    <cffunction name="getJSONDate" access="remote" returntype="any" returnformat="plain" output="no" hint="Checks whether application is up-to-date">
            
        <cfargument name="bundleID" type="string" required="yes" default="0">
		<cfargument name="version" type="numeric" required="yes" default="11">
       <cfargument name="override" type="boolean" required="no" default="false">
        
        <cfinvoke  component="LiveAPI" method="getAppPrefs" returnvariable="prefs">
			<cfinvokeargument name="bundleID" value="#bundleID#"/>
			<cfinvokeargument name="override" value="#override#"/>
		</cfinvoke>
																																								
		<cfset objData = deserializeJSON(prefs)>
		<cfset serverVR =objData.application.api>
   
        <!--- Get AppID from BundleID --->
		<cfinvoke component="CFC.Apps" method="getAppID" returnvariable="appInfo">
			<cfinvokeargument name="bundleID" value="#bundleID#">
			<cfinvokeargument name="override" value="#override#"/>
		</cfinvoke>

		<cfset appID = appInfo.app_id>
        <cfset dates = {"dev": 'NA', "prod": 'NA'}>
            
		<!--- read json from version --->
		<!--- Get JSON Path --->
        <cfinvoke  component="CFC.Apps" method="getJSONPath" returnvariable="JSONPath">
        	<cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
   
        <cfinvoke component="CFC.Apps" method="getAppName" returnvariable="appName">
            <cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
    																														
        <cfset JSONFile = appName & '.json'>
        
        <cfset JSONFilePathDev = JSONPath & 'DEV/' & JSONFile>
        <cfset JSONFilePath = JSONPath & JSONFile>
        
        <cfset dates = {"dev": 0, "prod": 0}>
         
		<!--- PROD --->
		<cfif fileExists(JSONFilePath)>
       		<cffile action = "read" file = "#JSONFilePath#" variable = "JSONProd" charset="utf-8">
 
       		<cfset JSONProd = deserializeJSON(JSONProd)>
      		
      		<cfinvoke component="LiveAPI" method="ago" returnvariable="prodDate">
            	<cfinvokeargument name="epoch" value="#JSONProd.modified#">
			</cfinvoke>
			
       		<cfset dates.prod = prodDate>
       		
        </cfif>
        
        <!--- DEV --->
        <cfif fileExists(JSONFilePathDev)>
			<cffile action = "read" file = "#JSONFilePathDev#" variable = "JSONDev" charset="utf-8">
       		
       		<cfset JSONDev = deserializeJSON(JSONDev)>
        
   			<!--- dates --->
       		<cfinvoke component="LiveAPI" method="ago" returnvariable="DevDate">
            	<cfinvokeargument name="epoch" value="#JSONDev.modified#">
			</cfinvoke>

        	<cfset dates.dev = DevDate>
        	
        </cfif>
			
       	<cfheader name="Access-Control-Allow-Origin" value="*">
        	<cfreturn dates>
            
    </cffunction>


	<cffunction name="ago" access="remote" returntype="any" output="no" hint="time passed">
            
        <cfargument name="epoch" type="numeric" required="yes" default="0">
        
        <cfscript>
			
  			dayAgo = {"months":0, "weeks":0, "days":0, "hours":0, "minutes":0};
   			
    		secs = int((Now().getTime() / 1000) - epoch);

			minutes = secs / 60;
			hours = minutes / 60;
			minutes = int(minutes % 60);
			if (hours < 1) dayAgo.minutes = minutes;
		
			days = hours / 24;
			hours = int(hours % 24);
			if (days < 1) dayAgo.hours = hours;
		
			weeks = days / 7;
			days = int(days % 7);
			if (weeks < 1) dayAgo.days = days;

			months = weeks / 4.35;
			weeks = int(weeks % 4.35);
			if (months < 1) dayAgo.weeks = weeks;
			
			years = months / 12;
			months = int(months % 12);
			if (years < 1) dayAgo.months = months;
			
		</cfscript>
		
		<cfinvoke component="LiveAPI" method="dateFormat" returnvariable="ago">
			<cfinvokeargument name="dateFormat" value="#dayAgo#">
		</cfinvoke>
		
		<cfreturn ago>
			
	</cffunction>
			    
	<cffunction name="dateFormat" access="remote" returntype="any" output="no" hint="time passed">
            
        <cfargument name="dateFormat" type="struct" required="yes" default="">
  		
  		<cfif dateFormat.months GT 0>
  			<cfreturn dateFormat.months &' mn'>
		</cfif>
		
		<cfif dateFormat.weeks GT 0>
  			<cfreturn dateFormat.weeks &' wks'>
		</cfif>
		
		<cfif dateFormat.days GT 0>
  			<cfreturn dateFormat.days &' d'>
		</cfif>
		
		<cfif dateFormat.hours GT 0>
  			<cfreturn dateFormat.hours &' h'>
		</cfif>
		
		<cfif dateFormat.minutes GT 0>
  			<cfreturn dateFormat.minutes &' m'>
		</cfif>
		
		<cfreturn 'NA'>
			
	</cffunction>
				    

<!---Create New User--->		
<!--- --->
<cffunction name="registerNewUser" access="remote" returntype="string" returnformat="plain" output="no" hint="Create a New Registered User">
	
    <cfargument name="name" type="string" required="no" default="Unknown">
    <cfargument name="email" type="string" required="yes">
    <cfargument name="pass" type="string" required="yes">

    <cfargument name="auth_token" type="string" required="yes" default="">
    
    <cfset data = structNew()>
      
	<!---OK--->
    <cfinvoke component="CFC.Errors" method="getError" returnvariable="okError">
        <cfinvokeargument name="error_code" value="1000">
    </cfinvoke>
	
    
		<!--- Token Info --->
        <cfinvoke component="CFC.Tokens" method="tokenValid" returnvariable="validToken">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
    
        
        <cfif validToken>

            <cfinvoke component="CFC.Tokens" method="getToken" returnvariable="tokenInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfset userID = tokenInfo.userID>
            <cfset bundleID = tokenInfo.bundleID>
           
            <!--- validateUserInfo --->
            <cfinvoke component="CFC.Users" method="validateUserInfo" returnvariable="validateUser">
                <cfinvokeargument name="bundleID" value="#bundleID#">
                <cfinvokeargument name="pass" value="#pass#">
                <cfinvokeargument name="email" value="#email#">
            </cfinvoke>
  
            
            <!--- No User Exists that is Registered --->
            <cfif validateUser.error.error_code IS 1011>

				<!--- Update current Guest Token for User with Registered Info --->
                <cfinvoke component="CFC.Users" method="setUserInfo" returnvariable="userInfo">
                    <cfinvokeargument name="auth_token" value="#auth_token#"/>
                    <cfinvokeargument name="name" value="#name#"/>
                    <cfinvokeargument name="email" value="#email#"/>
                    <cfinvokeargument name="pass" value="#pass#"/>
                </cfinvoke>
            
            
				<!--- Send Email to Approve --->
                <cfinvoke  component="CFC.Users" method="getUserProfileIDs" returnvariable="userDetails">
                  <cfinvokeargument name="auth_token" value="#auth_token#"/>
                </cfinvoke>
                
                <cfset clientID = userDetails.clientID>
                <cfset userID = userDetails.userID>
                <cfset appID = userDetails.appID>
                
                
                <cfinvoke  component="CFC.Apps" method="getAppName" returnvariable="appName">
                  <cfinvokeargument name="appID" value="#appID#"/>
                </cfinvoke>
                
                <cfinvoke  component="CFC.Clients" method="getClientInfo" returnvariable="info">
                  <cfinvokeargument name="clientID" value="#clientID#"/>
                </cfinvoke>
                
                <cfinvoke component="CFC.Apps" method="getPaths" returnvariable="assetPaths">
                    <cfinvokeargument name="clientID" value="#clientID#"/>
                    <cfinvokeargument name="server" value="yes"/>
                </cfinvoke>
                
                <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userInfo">
                    <cfinvokeargument name="userID" value="#userID#"/>
                </cfinvoke>
            	
                <cfset email = trim(email)>
                
                <!--- Send Email to User that they are Approved --->
                <cfmail server="cudaout.media3.net"
                        username="support@wavecoders.ca"
                        from="#assetPaths.client.name# Registration <#info.support#>"
                        to="#name# <#email#>"
                        subject="User Registration - #assetPaths.client.name#"
                        replyto="Support <#info.support#>"
                        type="HTML"> 
                
                        <!--- HTML RegUser --->
                        <link href="http://www.liveplatform.net/register/regStyles.css" rel="stylesheet" type="text/css">
                        
                        <cfinclude template="newRegistration.cfm">
                
                </cfmail>

            	<cfset structAppend(data,{"error":#okError#})>
            
            <cfelse>
            
                <!--- User Already Exists --->
                <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1014">
                </cfinvoke>
                
                <cfset structAppend(data,{"error":#error#})>
                
            </cfif>
            
        <cfelse>
            
            <!--- Token not valid --->
            <cfinvoke component="CFC.Errors" method="getError" returnvariable="okError">
				<cfinvokeargument name="error_code" value="1003">
			</cfinvoke>
			
            <cfset structAppend(data,{"error":#error#})>
            
        </cfif>

    <cfheader name="Access-Control-Allow-Origin" value="*">
    <cfset JSON = serializeJSON(data)>
    
    <cfreturn JSON>
    

</cffunction>


    
<!---Auth User--->
<!---authenticateUser&code=1111&bundleID=com.liveadz.sindicatocigars&operating_system=Macintosh%20OS%2010.9.4&screen_width=1280&screen_height=720&device_name=desktop&screen_size=15%27--->
    <cffunction name="authenticateUser" access="remote" returntype="string" returnformat="plain" output="no" 
    hint="Check whether user is authenticated and provides a token. If not authenticated with a code, a guest token is generated">
    	<!--- user and app --->
        <cfargument name="email" type="string" required="no" default="">
        <cfargument name="pass" type="string" required="no" default="">
        <cfargument name="code" type="string" required="no" default="">
        <cfargument name="bundleID" type="string" required="no" default="">
        <cfargument name="auth_token" type="string" required="no" default="">
        <!--- device --->
        <cfargument name="operating_system" type="string" required="no" default="">
        <cfargument name="device_name" type="string" required="no" default="">
        <!--- screen --->
        <cfargument name="screen_width" type="numeric" required="no" default="0">
        <cfargument name="screen_height" type="numeric" required="no" default="0">
        <cfargument name="screen_size" type="string" required="no" default="0">   
        <!--- appVr --->
        <cfargument name="version" type="numeric" required="no" default="1000000">   
        
        <cfset data = structNew()>
      	
        <!---OK--->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="okError">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>
        
        <!--- getAppPrefs --->
		<cfinvoke  component="CFC.Apps" method="getAppPrefs" returnvariable="appPrefs">
			<cfinvokeargument name="bundleID" value="#bundleID#"/>
		</cfinvoke> 
        
        <cfif appPrefs.authentication NEQ 1><cfset appPrefs.authentication = 0></cfif>
        <cfset useOAuth = appPrefs.authentication>
		
        <cfif useOAuth IS 1>
        	<!--- Thirdparty Authentication --->
        	<!--- <cfinvoke component="CFC.oAuth" method="oAuth" returnvariable="data">
				<cfinvokeargument name="bundleID" value="#bundleID#"/>
				<cfinvokeargument name="userCode" value="#email#"/>
				<cfinvokeargument name="userPass" value="#pass#"/>
				<cfinvokeargument name="auth_token" value="#auth_token#"/>
			</cfinvoke>

			Finished)<cfdump var="#data#">
			<cfabort> --->
        
        <cfelse>
        	<!--- Normal Authentication --->
		</cfif>
        
        <!--- Normal Authentication --->
        <cfif (email IS "" AND pass IS "")>
        	<cfif auth_token IS "">
        	
        	<!---Missing Param--->
            <!--- <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1005"/>
            </cfinvoke>  --->
         
            <!--- Create Token if no auth token provided --->
			<cfinvoke component="CFC.Tokens" method="createUserToken" returnvariable="token">
				  <cfinvokeargument name="bundleID" value="#bundleID#">
				  <cfinvokeargument name="code" value="#code#">
				  <cfinvokeargument name="pass" value="#pass#">
				  <cfinvokeargument name="email" value="#email#">
			</cfinvoke>
			
            <cfset structAppend(data, token)>
           
            <cfset structAppend(data,{"error":#okError#})>
			<cfset JSON = serializeJSON(data)>

			<cfreturn JSON>
        	
         	</cfif>   
		</cfif>
  
        <!--- Check if Client is Active --->
        <cfinvoke  component="CFC.Clients" method="clientActive" returnvariable="clientActive">
            <cfinvokeargument name="bundleID" value="#bundleID#"/>
        </cfinvoke>
   
        <cfif NOT clientActive>
        
            <!--- client not active --->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1030"/>
            </cfinvoke>
            
            <cfset structAppend(data,{"error":#error#})>
			<cfset JSON = serializeJSON(data)>
        
        	<cfreturn JSON>
            
        </cfif>
        <!---  --->
           
        <cfif email NEQ '' AND pass NEQ ''>
        	<cfset auth_token = ''>
        </cfif>
        
        <cfset deviceID = "">
        
        <cfif operating_system NEQ '' AND device_name NEQ '' AND screen_width NEQ '' AND screen_height NEQ '' AND screen_size NEQ ''>

            <!--- Create Device --->
            <cfinvoke component="CFC.Misc" method="createDevice" returnvariable="deviceID">
                <cfinvokeargument name="operating_system" value="#operating_system#">
                <cfinvokeargument name="device_name" value="#device_name#">
                <cfinvokeargument name="screen_width" value="#screen_width#">
                <cfinvokeargument name="screen_height" value="#screen_height#">
                <cfinvokeargument name="screen_size" value="#screen_size#">
            </cfinvoke>
        
         </cfif>
          
         <cfif bundleID NEQ ''>
         
		   <cfset tokenAuthenticationFailed = true>
           
           <cfif auth_token NEQ ''>
           
              <!--- validate authtoken if provided --->
              
              <!--- Get Token --->
              <cfinvoke component="CFC.Tokens" method="getToken" returnvariable="token">
                  <cfinvokeargument name="auth_token" value="#auth_token#">
              </cfinvoke>
			  
              <cfif structIsEmpty(token)>
              	<cfset tokenAuthenticationFailed = true>
              <cfelse>
              	<cfset tokenAuthenticationFailed = false>
              </cfif>
            															 
           </cfif>
   
           <cfif tokenAuthenticationFailed>
           
              <!--- Create Token if no auth token provided --->
              <cfinvoke component="CFC.Tokens" method="createUserToken" returnvariable="token">
                  <cfinvokeargument name="bundleID" value="#bundleID#">
                  <cfinvokeargument name="code" value="#code#">
                  <cfinvokeargument name="pass" value="#pass#">
                  <cfinvokeargument name="email" value="#email#">
                  <cfinvokeargument name="deviceID" value="#deviceID#">
                  <cfinvokeargument name="old_token" value="#auth_token#">
              </cfinvoke>
    
           <cfelse>
                
              <!--- get user name --->
              <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
              </cfinvoke>
              <!--- get user accessLevel --->
              <cfinvoke component="CFC.Users" method="getUserAccess" returnvariable="userAccess">
                <cfinvokeargument name="userID" value="#userInfo.user_id#"/>
              </cfinvoke>
              
              <cfif userAccess.accessLevel GT 0>
              	<cfset token = {"token":auth_token, "access":userAccess.accessLevel}>
              <cfelse>
              	<cfset token = {"token":auth_token}>
              </cfif>
              
              <cfif userInfo.name NEQ 'guest'>
              	<cfset structAppend(token,{"name":userInfo.name})>
                <cfset structAppend(token,{"access":userAccess.accessLevel})>
                <cfset structAppend(token,{"isGuest":1})>
              <cfelse>
              	<cfset structAppend(token,{"isGuest":0})>
              </cfif>
              
              <cfset structAppend(token,{"error":okError})>
           
           </cfif>
        
            <cfif token.error.error_code NEQ 1000>
                
            	<cfset JSON = serializeJSON(token)>
				<cfreturn JSON>
                
            </cfif>
   		  
            <cfif email IS "" OR pass IS "">
           		<!--- User guest Token --->
                <cfset structAppend(token,{"isGuest":1})>
                <cfset JSON = serializeJSON(token)>
                <cfreturn JSON>   
            <cfelse>
    
				<!--- validateUserInfo --->
                <cfinvoke component="CFC.Users" method="validateUserInfo" returnvariable="userInfo">
                    <cfinvokeargument name="bundleID" value="#bundleID#">
                    <cfinvokeargument name="code" value="#code#">
                    <cfinvokeargument name="pass" value="#pass#">
                    <cfinvokeargument name="email" value="#email#">
                </cfinvoke>
				
                <cfset structAppend(token,{"name":userInfo.name.data})>
                <cfset structAppend(token,{"isGuest":0})>
                
                <cfif structKeyExists(userInfo,"isDev")>
                	<cfset structAppend(token,{"isDev":1})>
                </cfif>
                
                <cfif userInfo.error.error_code NEQ 1000>   	
                    <cfset JSON = serializeJSON(userInfo)>
                    <cfreturn JSON>   
                </cfif>
            
            </cfif>
     	
            <cfset userName = "">
            
            <cfif structKeyExists(userInfo,"name")>
                <cfset userName = userInfo.name.data>
            </cfif>
            
            <cfif userName IS ''>
            	<cfif structKeyExists(userInfo,"email")>
                	<cfset userName = userInfo.email>
                </cfif>
            </cfif>

            <cfset data = token>
            
            <cfif structKeyExists(userInfo, 'email.data')>
            	<cfset structDelete(userInfo.email,'data')>
            </cfif>
            
            <cfif structKeyExists(userInfo, 'password.data')>
            	<cfset structDelete(userInfo.password,'data')>
            </cfif>

			<!--- Get Token --->
            <cfinvoke component="CFC.Tokens" method="getToken" returnvariable="aToken">
                <cfinvokeargument name="auth_token" value="#token.token#">
            </cfinvoke>
        
            <!--- Add Access Level --->
            <cfif aToken.access GT 0>
                <cfset structAppend(data,{"access":aToken.access})> 
            </cfif>
            
            <!--- Add User Name  --->
            <cfif aToken.access GT 0>
                <cfif userName NEQ ''>
                    <cfset structAppend(data,{"name":userName})> 
                </cfif>
            </cfif>
            
            <!---Update AppVersion --->
            <cfinvoke  component="CFC.Tokens" method="updateAppVersion" returnvariable="ok">
                <cfinvokeargument name="auth_token" value="#token.token#"/>
                <cfinvokeargument name="version" value="#version#"/>
            </cfinvoke> 
            
            <cfset JSON = serializeJSON(data)>
            <cfreturn JSON>
        
        <cfelseif auth_token NEQ ''>
        	
            <!--- Get Token --->
            <cfinvoke component="CFC.Tokens" method="getToken" returnvariable="aToken">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>
        	
            <cfset structAppend(data,{"error":#okError#})>
            
        <cfelse>
        	
        	<!---Missing Param--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1005"/>
            </cfinvoke>   
                
        	<cfset structAppend(data,{"error":#error#})>

        </cfif> 
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>

          
    </cffunction>
    
    
    
    

<!---Get Localization--->
<!---getLocalization&app_id=com.liveadz.libertymutual--->

    <cffunction name="getLocalization" access="remote" returntype="string" returnformat="plain" output="no" hint="Gets application localization file">
        
        <cfargument name="bundleID" type="string" required="yes" default="0">

            
            
    </cffunction>  




<!---Get Module Content--->
<!---getContent&auth_token=12&auth_token=34E62369-9DEC-1EEB-55349F0BE5D83BCE--->

    <cffunction name="getContent" access="remote" returntype="string" returnformat="plain" output="no" hint="Gets all Assets and Groups data">
        
        <cfargument name="auth_token" type="string" required="yes" default="0">
		<cfargument name="groupID" type="numeric" required="yes" default="0">
        
        <!--- OK Error --->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <!--- Authenticate User --->
        <cfset  data = structNew()>
        
        <!--- Token Info --->
        <cfinvoke component="CFC.Tokens" method="tokenValid" returnvariable="validToken">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
     
        <cfif validToken>
      
			<!--- Get Token User Info --->
            <cfinvoke component="CFC.Tokens" method="getToken" returnvariable="token">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfset dev = 0>
            <cfif structKeyExists(token,'dev')>
   				<cfset dev = 1>
            </cfif>
       
            <!--- Token Exists and IS OK --->
            <cfset appID = token.appID>
            <cfset accessLevel = token.access>
      
            <!--- Get Content Group --->
            <cfinvoke component="LiveAPI" method="getContentJSON" returnvariable="data">
                <cfinvokeargument name="groupID" value="#groupID#"/>
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="dev" value="#dev#"/>
            </cfinvoke>
            
            <cfreturn data>
        
        <cfelse>
        
        	<!---Not Valid--->
        	<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            	<cfinvokeargument name="error_code" value="1003">
			</cfinvoke>
            
        </cfif>
        
  		<cfset structAppend(data,{"error":#error#})> 
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        
        <cfset JSON = serializeJSON(data)>
        <cfreturn JSON> 
            
            
    </cffunction>
    
    
    
    

<!---isDateModified--->

    <cffunction name="isAppDataModified" access="remote" returntype="string" returnformat="plain" output="no" hint="Checks if app data modified">
        <cfargument name="bundleID" type="string" required="no" default="">
		<cfargument name="date" type="numeric" required="yes">
        <cfargument name="auth_token" type="string" required="no" default="">
		
        <cfset data = structNew()>
         
		<!--- OK Error --->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
		<cfset appOK = false>
        
        <cfif auth_token NEQ ''>
        
			<!--- Get Token User Info --->
            <cfinvoke component="CFC.Tokens" method="getToken" returnvariable="token">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
   			
            <cfif NOT structIsEmpty(token)>
            	<cfset appOK = true>
                <cfset appID = token.appID>
            </cfif> 
                  
        <cfelse>
        	
            <!--- AppID --->
            <cfinvoke  component="CFC.Apps" method="getAppID" returnvariable="appInfo">
                <cfinvokeargument name="bundleID" value="#bundleID#"/>
            </cfinvoke>
       	
            <cfif appInfo.error.error_code IS 1000>
            	<cfset appOK = true>
                <cfset appID = appInfo.app_id>
            </cfif>
            
		</cfif>

        <cfset changed = false>
        
        <cfif appOK>

        	<cfset dev = 0>
            
            <cfif auth_token NEQ ''>
            
				<!--- Get Token User Info --->
                <cfinvoke component="CFC.Tokens" method="getToken" returnvariable="token">
                    <cfinvokeargument name="auth_token" value="#auth_token#"/>
                </cfinvoke>

                <cfif structKeyExists(token,'dev')>
                    <cfset dev = 1>
                </cfif>

            </cfif>
           
            <!--- Get Content Group --->
            <cfinvoke component="LiveAPI" method="getContentJSON" returnvariable="appData">
                <cfinvokeargument name="groupID" value="0"/>
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="dev" value="#dev#"/>
            </cfinvoke>
          																																											
            <cfif error.error_code IS 1000>
             
				<cfset jsonData = deserializeJSON(appData)>
                
                <cfif structKeyExists(jsonData,"modified")>
                	<cfset curDate = jsonData.modified>
              	<cfelse>
              		<cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="curDate" />
              		<cfset structAppend(jsonData, {"modified": curDate})>
				</cfif>
               
                <cfif date LT curDate>
                    <cfset changed = true>
                </cfif>
             	
				<cfset structAppend(data,{"modified":#changed#})>
                <cfset structAppend(data,{"error":#error#})> 
            
            <cfelse>
            	<cfset data = deserializeJSON(appData)>
            </cfif>
		
        <cfelse>
        	
            <!--- App not found Error --->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1002"/>
            </cfinvoke>
            
            <cfset data = error>
            
        </cfif>

        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON> 
            
            
    </cffunction>
    
    
    
    <!---Is Token Valid--->
    <cffunction name="isTokenValid" access="remote" returntype="string" returnformat="plain" output="no" hint="Check if the token is valid">
        <cfargument name="auth_token" type="string" required="yes">
        <cfargument name="version" type="numeric" required="no" default="1.0"> 
        
        <cfset data = structNew()>
        
        <!--- Get Token --->
        <cfinvoke component="CFC.Tokens" method="tokenValid" returnvariable="validToken">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
	
        <cfif validToken>
		
        	<!--- Token Valid --->
        	<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            	<cfinvokeargument name="error_code" value="1000">
			</cfinvoke>
            
			<!--- Get Token --->
            <cfinvoke component="CFC.Tokens" method="getToken" returnvariable="aToken">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>
           
            <cfif structIsEmpty(aToken)>
            <!--- Account inactive --->
            	<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1009">
                </cfinvoke>
            </cfif>
            
			<!--- Add Access Level --->
            <cfif aToken.access GT '0'>
            	<cfset structAppend(data,{"access":aToken.access})> 
            </cfif>
            
        <cfelse>
        	
            <!--- Get Token --->
            <cfinvoke component="CFC.Tokens" method="getToken" returnvariable="aToken">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>

			<cfif structIsEmpty(aToken)>
                <!--- Account inactive --->
                <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1004">
                </cfinvoke>
                
            <cfelse>   
            
                <!---Not Valid--->
                <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1003">
                </cfinvoke>
        
            </cfif>
            
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})> 
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
    
    </cffunction>




   <!--- GAME START --->
    <cffunction name="gameStart" access="remote" returntype="string" returnformat="plain" output="yes" hint="Start Game Instance">
        <cfargument name="auth_token" type="string" required="yes">
        <cfargument name="gameID" type="numeric" required="yes">
        <cfargument name="assetID" type="numeric" required="yes">
        <cfargument name="userInfo" type="struct" required="no" default="structNew()">
        
        <cfset data = structNew()>
        
        <cfif structIsEmpty(userInfo)>
			<cfset userInfo = {'name':'','email':'','country':''}>
        </cfif>
        
        <!--- Get Current Date --->
        <cfinvoke  component="CFC.Misc" method="convertDateToEpoch" returnvariable="currentDate" />

		<!--- error ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>
		

        <!--- Check if Game Instance Already Exists --->
        <cfquery name="checkGameActive">
            SELECT	asset_id, dateStart, dateEnd
            FROM	GameAssets 
            WHERE   asset_id = #gameID# AND (#currentDate# >= dateStart AND #currentDate# <= dateEnd)
        </cfquery>
        
        <cfif checkGameActive.recordCount GT 0>
        	<cfset gameActive = true>
        <cfelse>
        	<cfset gameActive = false>
        </cfif>
       
        <cfif gameActive>
        
			<!--- Get Token --->
            <cfinvoke component="CFC.Tokens" method="tokenValid" returnvariable="validToken">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>
        
            <cfif validToken>

                <!--- Check if Games is Completed --->
				
				<!--- Get get Game Assets Count --->
                <cfinvoke  component="CFC.Games" method="getGameUserAssetCount" returnvariable="userCount">
                  <cfinvokeargument name="auth_token" value="#auth_token#"/>
                  <cfinvokeargument name="gameID" value="#assetID#"/>
                </cfinvoke>
                
                <!--- Get Total Game Assets --->
                <cfinvoke  component="CFC.Games" method="getGameAssetsCount" returnvariable="targetCount">
                  <cfinvokeargument name="gameID" value="#gameID#"/>
                </cfinvoke>
                
                
                <!--- If Game Assets >= TotalAssets then return error 2002 --->
                <cfif userCount GTE targetCount>
                
					<!---Game Played Already --->
                    <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                        <cfinvokeargument name="error_code" value="2002">
                    </cfinvoke>
                	
                    <cfset structAppend(data,{"error":#error#})>
       
					<cfset JSON = serializeJSON(data)>
                    
                    <cfreturn JSON>
                    
                </cfif>
                
                
                <!--- Get User, ClientID and AppID --->
                <cfinvoke  component="CFC.Tokens" method="getTokenInfo" returnvariable="user">
                  <cfinvokeargument name="auth_token" value="#auth_token#"/>
                </cfinvoke>

                <cfset userID = user.userID>
                <cfset appID = user.appID>
                
                <!--- Update User Info --->
                <cfif userInfo.name NEQ '' OR userInfo.email NEQ ''>
                    <cfinvoke  component="CFC.Users" method="updateUserInfo" returnvariable="updatedUser">
                      <cfinvokeargument name="auth_token" value="#auth_token#"/>
                      <cfinvokeargument name="userInfo" value="#userInfo#"/>
                    </cfinvoke>
                </cfif>
                
                <!--- Check if Game Instance Already Exists --->
                <cfquery name="checkGameState">
                    SELECT	asset_id
                    FROM	GameStats
                    WHERE asset_id = #gameID# AND user_id = #userID# AND app_id = #appID#
                </cfquery>
               
                <cfif checkGameState.recordCount IS 0>
                
                    <!---Create New Record for GameState--->
                    <cfquery name="newGameState">
                        INSERT INTO GameStats (asset_id, collectedAsset_id, collectedAssetDate, user_id, app_id)
                        VALUES (#gameID#,#assetID#,#currentDate#,#userID#,#appID#)
                    </cfquery>

                <cfelse>
               
                    <!--- error game registered --->
                    <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                        <cfinvokeargument name="error_code" value="2000">
                    </cfinvoke>
                
                </cfif>
        	
            <cfelse>
            
				<!---Not Valid--->
                <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1003">
                </cfinvoke>
                    
            </cfif>

        <cfelse>
        
        	<!--- Game NOT Active --->
            <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="2001">
            </cfinvoke>
    		
        </cfif>
       
        <cfset structAppend(data,{"error":#error#})>
       
       <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
    
    </cffunction>


   <!--- GAME END --->
    <cffunction name="gameEnd" access="remote" returntype="string" returnformat="plain" output="no" hint="Game Completed">
        <cfargument name="auth_token" type="string" required="yes">
        <cfargument name="game" type="string" required="yes">
        
        <cfset data = structNew()>
        
        <!--- Get Current Date --->
        <cfinvoke  component="CFC.Misc" method="convertDateToEpoch" returnvariable="currentDate" />
        
        <cfset gameData = deserializeJSON(game)>
    
        <!--- Check if Game Instance Already Exists --->
        <cfquery name="checkGameActive">
            SELECT	asset_id, dateStart, dateEnd
            FROM	GameAssets 
            WHERE   asset_id = #gameData.assetID# AND (#currentDate# >= dateStart AND #currentDate# <= dateEnd)
        </cfquery>
        
        <cfif checkGameActive.recordCount GT 0>
        	<cfset gameActive = true>
        <cfelse>
        	<cfset gameActive = false>
        </cfif>
        
        <!--- error ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>
        
        
        <!--- Get Game Options --->
        <cfinvoke  component="CFC.Games" method="getGameOptions" returnvariable="theGameOptions">
          <cfinvokeargument name="gameID" value="#gameData.assetID#"/>
        </cfinvoke>

        <cfif gameActive>
        
			<!--- Get Token --->
            <cfinvoke component="CFC.Tokens" method="tokenValid" returnvariable="validToken">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>
        
            <cfif validToken>
            
                <cfset gameData = deserializeJSON(game)>
                
                <cfset gameID = gameData.assetID>
                <cfset gameResults = gameData.results>
                <cfset userInfo = gameData.userInfo>
                
                <cfif IsDefined('userInfo.country')>
                	<cfset country = userInfo.country>
                <cfelse>
                	<cfset country = ''>
                </cfif>
                
                <cfset userInfo = {"name":userInfo.name, "email":userInfo.email, "country":country}>
                
                <!--- Get User, ClientID and AppID --->
                <cfinvoke  component="CFC.Tokens" method="getTokenInfo" returnvariable="user">
                  <cfinvokeargument name="auth_token" value="#auth_token#"/>
                </cfinvoke>
                
                <cfset userID = user.userID>
                <cfset appID = user.appID>
                
                <!--- Update User Info --->
                <cfinvoke  component="CFC.Users" method="updateUserInfo" returnvariable="updatedUser">
                  <cfinvokeargument name="auth_token" value="#auth_token#"/>
                  <cfinvokeargument name="userInfo" value="#userInfo#"/>
                </cfinvoke>
                
                <!--- Add Games Results --->
                <cfloop index="z" from="1" to="#arrayLen(gameResults)#">
                    
                    <cfset theAsset = gameResults[z]>

                    <!--- Convert ts to Epoch --->
                    <cfset theDate = theAsset.ts>
                    
                    <!--- Check if Rec Exists --->
                    <cfquery name="checkRecExists">
                        SELECT	asset_id
                        FROM	GameStats 
                        WHERE   asset_id = #gameID# AND collectedAsset_id = #theAsset.assetID# AND user_id = #userID# AND app_id = #appID#
                    </cfquery>
                    
                    <cfif checkRecExists.recordCount IS 0>
                    
						<!---Create Record for User--->
                        <cfquery name="newGameStat">
                            INSERT INTO GameStats (asset_id, collectedAsset_id, collectedAssetDate, user_id, app_id)
                            VALUES (#gameID#,#theAsset.assetID#,#theDate#,#userID#,#appID#)
                        </cfquery>
                    <cfelse>
                    	<!--- Record Exists --->
                	</cfif>
                    
                </cfloop>
                
                
                
                <!--- Get Paths --->
				<cfinvoke component="CFC.Apps" method="getPaths" returnvariable="assetPaths">
                    <cfinvokeargument name="clientID" value="#user.clientID#"/>
                    <cfinvokeargument name="appID" value="#user.appID#"/>
                    <cfinvokeargument name="server" value="yes"/>
                 </cfinvoke>
                
                
                <!---Create GeneralID--->
                <cfinvoke component="CFC.Misc" method="makeID" returnvariable="code">
                    <cfinvokeargument name="length" value="8"/>
                    <cfinvokeargument name="type" value="1"/>
                </cfinvoke>
                
                <cfset structAppend(userInfo,{"code":"#code#"})>
                
                
                <!--- Get User Info --->
                <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userDetails">
                    <cfinvokeargument name="auth_token" value="#auth_token#"/>
                </cfinvoke>
                
                <cfif userDetails.code NEQ ''>
                    <cfset userInfo.code = userDetails.code>
                </cfif>
                
                <cfif theGameOptions.useCode AND userInfo.email NEQ ''>
                    	 
					<!--- Update User Code --->
                    <cfinvoke component="CFC.Users" method="updateUserInfo" returnvariable="updatedUser">
                        <cfinvokeargument name="userInfo" value="#userInfo#"/>
                        <cfinvokeargument name="auth_token" value="#auth_token#"/>
                    </cfinvoke>
                        
                 </cfif>
                
                
                <!--- Send Email Confirmation to User --->
                
                
                <!--- Send Player EMail Info --->
                <cfif theGameOptions.send.player AND userInfo.email NEQ '' AND theGameOptions.silentMode IS 0>

                     <cfmail server="cudaout.media3.net"
                      username="support@wavecoders.ca"
                      from="#assetPaths.client.name# <#theGameOptions.send.email#>"
                      to="#userInfo.email#"
                      subject="Congratulations!"
                      replyto="#assetPaths.client.name# <#theGameOptions.send.email#>"
                      type="HTML">

 
						<!--- HTML --->
    
                        <!--- if message the user the var #theGameOptions.send.message# in the template --->
                        
                        <!--- if code is used, use the var #userInfo.code# in the template --->
						
                        <cfinclude template="CFC/emailGame.cfm"><!--- EMail Template --->
                    
                    </cfmail>
                
                </cfif>

                
                
                <!--- Send Client EMail Info --->
                <cfif theGameOptions.send.client AND theGameOptions.send.email NEQ ''>

                     <cfmail server="cudaout.media3.net"
                      username="support@wavecoders.ca"
                      from="#userInfo.email#"
                      to="#assetPaths.client.name# <#theGameOptions.send.email#>"
                      subject="Submission Recieved"
                      type="HTML">
 
                    <!--- HTML --->
                    <cfinclude template="CFC/emailGame.cfm"><!--- EMail Template --->
                    
                    </cfmail>
                
                </cfif>
                
                

            
            <cfelse>
                <cfset error = validToken.error>
            </cfif>
        
		<cfelse>
        	
			<!--- Game NOT Active --->
            <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="2001">
            </cfinvoke>
     
        </cfif>
     
         <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
    
    </cffunction>




<!---Get Apps --->
<cffunction name="getApps" access="remote" returntype="string" returnformat="plain" output="no" hint="Gets Cached JSON">

	<cfinvoke  component="CFC.Clients" method="getApps" returnvariable="apps" />
    
    <cfinvoke  component="CFC.Misc" method="QueryToStruct" returnvariable="data">
        <cfinvokeargument name="query" value="#apps#"/>
    </cfinvoke>
    
    
    <cfheader name="Access-Control-Allow-Origin" value="*">
    <cfset JSON = serializeJSON(data)>

	<cfreturn JSON>  

</cffunction>



<!---Get Cached JSON Content--->

<cffunction name="getCachedContent" access="remote" returntype="string" returnformat="plain" output="no" hint="Gets Cached JSON">

    <cfargument name="bundleID" type="string" required="yes">
	
    <!---OK--->
    <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
        <cfinvokeargument name="error_code" value="1000">
    </cfinvoke>
    
    <!--- AppID --->
    <cfinvoke  component="CFC.Apps" method="getAppID" returnvariable="appInfo">
        <cfinvokeargument name="bundleID" value="#bundleID#"/>
    </cfinvoke>

    <cfif appInfo.error.error_code IS 1000>
    
        <cfset appID = appInfo.app_id>
    
        <cfinvoke  component="CFC.Modules" method="getCachedContent" returnvariable="cachedObjects">
            <cfinvokeargument name="appID" value="#appID#"/>
        </cfinvoke>
        
	<cfelse>
    	
        <cfset JSON = serializeJSON(appInfo)>
        <cfreturn JSON>
        
    </cfif>
	
    <cfset data = structNew()>
    
    <cfset structAppend(data,{"cache":#cachedObjects#})> 
    <cfset structAppend(data,{"error":#error#})> 
	
	<cfheader name="Access-Control-Allow-Origin" value="*">
	<cfset JSON = serializeJSON(data)>

	<cfreturn JSON>  
    
</cffunction>




  

<!---Generate JSON Content--->
<!---generateContentJSON&appID=4&groupID=0--->

    <cffunction name="getContentJSON" access="public" returntype="string" output="no" hint="Gets JSON structure from Content Data. Not used in API">
        
        <cfargument name="groupID" type="numeric" required="yes" default="0">
		<cfargument name="appID" type="numeric" required="yes" default="0">
        <cfargument name="dev" type="numeric" required="no" default="0">
        
        <!--- Get JSON Path --->
        <cfinvoke  component="CFC.Apps" method="getJSONPath" returnvariable="JSONPath">
        	<cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
   
        <!--- if DEV then add DEV folder pto path --->
        <cfif dev IS 1>
        	<cfset JSONPath = JSONPath & 'DEV/'>
		</cfif>
        
        <cfinvoke component="CFC.Apps" method="getAppName" returnvariable="appName">
            <cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>
    																														
        <cfif groupID IS -1 OR groupID IS 0>
        	<cfset JSONFile = appName & '.json'>
        <cfelse>
        	<cfset JSONFile = appName & '_' & groupID & '.json'>
		</cfif>

        <cfset JSONFilePath = JSONPath & JSONFile>

		<cfif fileExists(JSONFilePath)>
        	<!--- Read JSON File --->
			<cffile action = "read" file = "#JSONFilePath#" variable = "JSON" charset="utf-8">
        <cfelse>
			
            <!---OK--->
        	<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            	<cfinvokeargument name="error_code" value="1013">
			</cfinvoke>
          
            <cfset data = structNew()>
            <cfset structAppend(data,{"error":#error#})> 
        
        	<cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
            
			<!--- Get JSON from Server --->
           <!---  <cfinvoke  component="CFC.Apps" method="generateContentJSON" returnvariable="data">
                <cfinvokeargument name="groupID" value="#groupID#"/>
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke> --->
	
			<!--- Read JSON File --->
            <!--- <cffile action = "read" file = "#JSONFilePath#" variable = "JSON" charset="utf-8"> --->

          </cfif>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfreturn JSON>   
        
     </cffunction>   

 
 	<!--- Session Tracking --->
    
    <!--- Start Session --->
     <cffunction name="startSession" access="remote" returntype="string" returnformat="plain" output="no" hint="Creates a New Session. If session exists, Close Old session and Start New Session">
        
        <cfargument name="auth_token" type="string" required="yes" default="">
			
            <cfset data = structNew()>
            
            <cfinvoke component="CFC.Tracking" method="startSession" returnvariable="success">
                <cfinvokeargument name="auth_token" value="#auth_token#">
            </cfinvoke>            
            
            <!---OK--->
        	<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            	<cfinvokeargument name="error_code" value="1000">
			</cfinvoke>
            
            <cfset structAppend(data,{"error":#error#})> 
        	
        	<cfheader name="Access-Control-Allow-Origin" value="*">
        	<cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
     </cffunction>  
       
     
     <!--- End Session --->
     <cffunction name="endSession" access="remote" returntype="string" returnformat="plain" output="no" hint="Ends a current session">
        
        <cfargument name="auth_token" type="string" required="yes" default="">
        <cfargument name="sessionLength" type="numeric" required="yes" default="0">
			
            <cfset data = structNew()>
            
            <cfinvoke component="CFC.Tracking" method="endSession" returnvariable="success">
                <cfinvokeargument name="auth_token" value="#auth_token#">
                <cfinvokeargument name="sessionLength" value="#sessionLength#">
            </cfinvoke>            
            
            <!---OK--->
        	<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            	<cfinvokeargument name="error_code" value="1000">
			</cfinvoke>
            
            <cfset structAppend(data,{"error":#error#})> 
        	
        	<cfheader name="Access-Control-Allow-Origin" value="*">
        	<cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
     </cffunction>  



     <!--- Save Multiple Sessions --->
     <cffunction name="saveSessions" access="remote" returntype="string" returnformat="plain" output="no" hint="Dumps all sessions in the database. This is for offline mode where sesions are stored and when online dumps all session data">
        
        <cfargument name="auth_token" type="string" required="yes" default="">
        <cfargument name="sessions" type="string" required="yes" default="0">
			
            <cfset data = structNew()>
            
            <cfinvoke component="CFC.Tracking" method="saveSessions" returnvariable="success">
                <cfinvokeargument name="auth_token" value="#auth_token#">
                <cfinvokeargument name="sessions" value="#sessions#">
            </cfinvoke>            
            
            <!---OK--->
        	<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            	<cfinvokeargument name="error_code" value="1000">
			</cfinvoke>
            
            <cfset structAppend(data,{"error":#error#})> 
        	
        	<cfheader name="Access-Control-Allow-Origin" value="*">
        	<cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
     </cffunction>         






	 <!--- Save Social Cart Session --->
     <cffunction name="sendSocial" access="remote" returntype="string" returnformat="plain" output="no" hint="Returns a tinyURL for Social Networks">
        
        <cfargument name="auth_token" type="string" required="yes" default="">
		<cfargument name="content" type="string" required="yes" default="0">
        <cfargument name="message" type="string" required="no" default="">
        <cfargument name="socialType" type="numeric" required="yes" default="0">
		
        <cfset data = structNew()>
        
        <!--- Get AppID --->
        <cfinvoke component="CFC.Tokens" method="getTokenID" returnvariable="userInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
   		
        <cfset appID = userInfo.app_id>
          
        <!--- Create Cart --->
        <cfinvoke component="CFC.Carts" method="createCart" returnvariable="cartID">
            <cfinvokeargument name="auth_token" value="#auth_token#">
            <cfinvokeargument name="content" value="#content#">
            <cfinvokeargument name="message" value="#message#">
        </cfinvoke> 
        
        <!--- Get Module ID --->
        <cfinvoke component="API.v8.CFC.One2" method="getModuleFromCartID" returnvariable="moduleID">																			
            <cfinvokeargument name="cartID" value="#cartID#"/>
        </cfinvoke>
        
        <!--- Provide Shortened Link --->
        <cfinvoke component="CFC.Misc" method="shortUrl" returnvariable="tinyUrl" />
        
        <!--- Update the Cart with Short URL --->
        <cfquery name="UpdateLink">
            UPDATE SessionCart
            SET tinyurl = '#tinyUrl#', socialType = #socialType#
            WHERE	cart_id = #cartID#
        </cfquery>
        
        <!--- Make Link --->
        <cfset structAppend(data,{"url":'http://liveplatform.net/social.cfm?link='& #tinyUrl#})> 
        
        <cfinvoke component="CFC.Tokens" method="getTokenInfo" returnvariable="info">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
        
        
        <!--- Get Client/App Paths --->
        <cfinvoke component="CFC.Apps" method="getPaths" returnvariable="assetPaths">
            <cfinvokeargument name="appID" value="#info.appID#"/>
            <cfinvokeargument name="clientID" value="#info.clientID#"/>
            <cfinvokeargument name="server" value="yes"/>
        </cfinvoke>
        
        
        <!--- Get Cart Assets --->
        <cfinvoke component="CFC.Carts" method="getCartAssets" returnvariable="info">
            <cfinvokeargument name="cartID" value="#cartID#"/> 
        </cfinvoke>    
    
        <!--- Get Social Message --->
        <cfinvoke component="CFC.Options" method="getSocialMessage" returnvariable="socialMessage">
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="groupID" value="#moduleID.id#"/> 
        </cfinvoke>    
        
        <cfquery dbtype="query" name="theMessage">
            SELECT		message, LOWER(networkName)
            FROM        socialMessage
            WHERE		socialType = #socialType#
        </cfquery>
        
        
        <!--- Info --->
        <cfset structAppend(data,{"title":'#info.company.name#'})> 
        <cfset structAppend(data,{"caption":'#info.details.title#'})> 
        <cfset structAppend(data,{"description":'#theMessage.message#'})> 
        <cfset structAppend(data,{"image":'#info.company.icon#'})> 
        
		<!---OK--->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>
        
        <cfset structAppend(data,{"error":#error#})> 
  
       <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
        
    </cffunction>
    
    
    


     <!--- Save Cart Session --->
     <cffunction name="sendCart" access="remote" returntype="string" returnformat="plain" output="no" hint="Sends a Cart of Assets for EMail">
        
        <cfargument name="auth_token" type="string" required="yes" default="">
        <cfargument name="content" type="string" required="yes" default="0">
        <cfargument name="email" type="string" required="yes" default="0">
        <cfargument name="message" type="string" required="no" default="">
        
        	
            <cfset data = structNew()>
            
			<!--- check if email address is valid --->
            <cfhttp result="result" method="GET" charset="utf-8" url="http://apilayer.net/api/check?access_key=fc44ff1c93937cdcb210b7e87e3b7e40&email=#email#&smtp=1&format=1"></cfhttp>
            <cfset theData = deserializeJson(result.Filecontent)>
           
           <cfif structKeyExists(theData,'error')>
				<cfif theData.error.code IS 104>
					<cfset found = "1">
				<cfelse>
					<cfset found = theData.mx_found>
				</cfif>
			<cfelse>
				<cfset found = theData.mx_found>
			</cfif>
           
            <cfif found IS 0>
            	<!--- email not valid --->
                
                <!---EMAIL Address Failed--->
                <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1026">
                </cfinvoke>
                
            <cfelse> 

                <!--- Get AppID --->
                <cfinvoke component="CFC.Tokens" method="getTokenID" returnvariable="userInfo">
                    <cfinvokeargument name="auth_token" value="#auth_token#">
                </cfinvoke>
                
                <cfset appID = userInfo.app_id>
                
                <!--- Create Cart --->
                <cfinvoke component="CFC.Carts" method="createCart" returnvariable="cartID">
                    <cfinvokeargument name="auth_token" value="#auth_token#">
                    <cfinvokeargument name="content" value="#content#">
                    <cfinvokeargument name="email" value="#email#">
                    <cfinvokeargument name="message" value="#message#">
                </cfinvoke>            
            																									
                <!--- Send Email of Cart with CartID --->
                <cfinvoke component="CFC.EMail" method="sendEmail" returnvariable="sentEmail">
                    <cfinvokeargument name="cartID" value="#cartID#"/>
                    <cfinvokeargument name="emailVerified" value="#found#"/>
                </cfinvoke>
                
                <cfif sentEmail>
                
                    <!---OK--->
                    <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                        <cfinvokeargument name="error_code" value="1000">
                    </cfinvoke>
                
                <cfelse>
                
                    <!---EMAIL Address Failed--->
                    <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                        <cfinvokeargument name="error_code" value="1015">
                    </cfinvoke>
                
                </cfif>
			
            </cfif>
            
            <cfset structAppend(data,{"error":#error#})> 
        
        	<cfheader name="Access-Control-Allow-Origin" value="*">
        	<cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
     </cffunction> 




	<!--- Get EMail Asset from Session --->
     <cffunction name="getEmailAsset" access="public" returntype="string" returnformat="plain" output="no" hint="Gets an Asset in HTML to be inserted into the EMail. Not Used for API">
        
        <cfargument name="token" type="string" required="yes" default="">
        
        <!--- Get Cart Asset --->
       <cfinvoke component="CFC.Carts" method="getCartAsset" returnvariable="info">
       <cfinvokeargument name="token" value="#token#">
        </cfinvoke>
  
        <!--- Display Asset --->
        <cfoutput>
          <table width="400" border="0" cellpadding="0" cellspacing="10">
            <tr>
                <td><img src="#info.url#" width="400" height="400" class="imgLockedAspect" /></td>
            </tr>
            <cfif info.details.title NEQ ''>
              <tr>
                <td class="mainheading">#info.details.title#</td>
              </tr>
             </cfif>
             <cfif info.details.subtitle NEQ ''>
              <tr>
                <td class="contentHilighted">#info.details.subtitle#</td>
              </tr>
              </cfif>
              <cfif info.details.description NEQ ''>
              <tr>
                <td class="content">#info.details.description#</td>
            </tr>
              </cfif>
          </table>
       </cfoutput>

        <!--- Get User AuthToken --->
        <cfinvoke component="CFC.Carts" method="getUserAuthToken" returnvariable="auth_token">
            <cfinvokeargument name="token" value="#token#">
            <cfinvokeargument name="appID" value="#info.appID#">
        </cfinvoke>
        
        <!--- Track Asset --->
        <cfinvoke component="CFC.Tracking" method="trackContentAsset" returnvariable="result">
        <cfinvokeargument name="assetID" value="#info.assetID#">
        <cfinvokeargument name="groupID" value="#info.groupID#">
        <cfinvokeargument name="length" value="30">
        <cfinvokeargument name="auth_token" value="#auth_token#">
        </cfinvoke>
        
        
	</cffunction>
    
    
    
    
    
    <!--- Reset Tracking --->
    <cffunction name="resetTracking" access="remote" returntype="string" returnformat="plain" output="no" hint="Resets all Tracking for a Client or App to nil">
        
        <cfargument name="clientID" type="numeric" required="yes">
        <cfargument name="appID" type="numeric" required="no">
        <cfargument name="userID" type="numeric" required="no">
        
        
        <!--- Reset All Tracking --->
        <cfinvoke component="CFC.Tracking" method="resetTrackingData" returnvariable="result">
            <cfinvokeargument name="clientID" value="#clientID#"/>
            <cfinvokeargument name="appID" value="#appID#"/>
            <cfinvokeargument name="userID" value="#userID#"/>
        </cfinvoke>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfreturn result>
    
    </cffunction>  
    
    
    
    
    


	 <!--- Fix Clients in Tracking --->
    <cffunction name="cleanUpTrackingClients" access="remote" returntype="boolean" returnformat="plain" output="no" hint="Cleanup Clients in Tracking">
         <cfargument name="appID" type="numeric" required="no">
         
        <cfquery name="trackingInfo">
            SELECT        track_id, client_id, app_id AS appID
            FROM          Tracking
            WHERE		  client_id = 0
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
    	</cfquery>

        <cfoutput query="trackingInfo">
        
            <cfinvoke component="CFC.Apps" method="getClientApps" returnvariable="clientInfo">
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
			
            <cfset clientID = clientInfo.client_id>

            <cfquery name="updateClient">
                UPDATE Tracking
                SET client_id = #clientID#
                WHERE	track_id = #track_id#
            </cfquery>
        
		</cfoutput>
        
        
        <cfreturn true>
    
    </cffunction> 





 <!--- Fix Carts Sent --->
    <cffunction name="cleanUpCartsViewed" access="remote" returntype="boolean" returnformat="plain" output="yes" hint="Cleanup Carts">
        
        <cfargument name="appID" type="numeric" required="no">
        
        <cfquery name="carts">
            SELECT        cart_id, user_id, email, ContentIDs, location_id
            FROM          SessionCart
            WHERE		  app_id = #appID#
    	</cfquery>
        
        
        <cfquery name="tracked">
            SELECT        user_id, location_id, content_id
            FROM          Tracking
            WHERE		  app_id = #appID#
    	</cfquery>

		<!--- <cfdump var="#carts#"><cfdump var="#tracked#"><cfabort> --->
		
        <cfloop query="carts">

            <cfset location = location_id>
        	
            <cfloop index="anAsset" list="#contentIDs#" delimiters=",">
            
				<cfquery name="foundCart" dbtype="query">
                
                    SELECT  count(content_id)
                    FROM	tracked
                    WHERE	content_id = #anAsset# AND user_id = #user_id#
                    
                </cfquery>
                
                <cfset found = foundCart.recordCount>
                
                <cfif found GTE 1>
                	<cfdump var="#cart_id#"><br>
                </cfif>

            
            </cfloop>
 
        </cfloop>
        

        <!--- <cfoutput query="trackingInfo">
        
            <cfinvoke component="CFC.Apps" method="getClientApps" returnvariable="clientInfo">
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
			
            <cfset clientID = clientInfo.client_id>

            <cfquery name="updateClient">
                UPDATE Tracking
                SET client_id = #clientID#
                WHERE	track_id = #track_id#
            </cfquery>
        
		</cfoutput> --->
        
        
        <cfreturn true>
    
    </cffunction> 
    
    
    
    <!--- Get Cart Assets --->
<cffunction name="getCartAssets" output="no" access="remote" returntype="struct" description="Returns Asset Thumbs from Cart">
    	<cfargument name="cartID" type="numeric" required="yes" default="0">

        <cfquery name="content">   
            SELECT  contentIDs
            FROM	SessionCart
            WHERE	cart_id = #cartID#
        </cfquery>
  
  <!--- Get Assets From Tracked Content --->
  	<cfinvoke component="CFC.Tracking" method="getTrackedContentFromCart" returnvariable="trackedContent">
        <cfinvokeargument name="cartID" value="#cartID#"/>
    </cfinvoke>
  
        
  <cfset allAssets = content.contentIDs>
        
  <cfset thumbs = structNew()>
        
  		<cfloop index="theContentID" list="#allAssets#" delimiters=",">
        
			<!--- Get AssetID from Content ID --->
            <cfinvoke component="CFC.Modules" method="getAssetIDFromContentGroup" returnvariable="assetInfo">
            <cfinvokeargument name="groupID" value="#theContentID#"/>
            </cfinvoke>
            
            <cfset assetID = assetInfo.assetID>
            
            <!--- Get Asset Info --->
    		<cfinvoke component="CFC.Assets" method="getAssets" returnvariable="assetDetails">
                <cfinvokeargument name="assetID" value="#assetID#"/>
            </cfinvoke>
            
            <!--- Asset Path --->
            <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                <cfinvokeargument name="assetID" value="#assetID#"/>
                <cfinvokeargument name="server" value="yes"/>
            </cfinvoke>
            
            <cfif assetDetails.assetType_id IS 3>
			    <cfset thumbImage = 'http://liveplatform.net/API/v8/images/document.png'>  
            <cfelse>
            	<cfset thumbImage = assetPath &'thumbs/nonretina/'& assetDetails.thumbnail>
            </cfif>

            <cfif assetDetails.title IS ''>
           	  <cfset title = assetDetails.assetName>
            <cfelse>
            	<cfset title = assetDetails.title>
            </cfif>
			
            <cfif ArrayFind(trackedContent,theContentID)>
            	<cfset viewed = 1>
            <cfelse>
            	<cfset viewed = 0>
            </cfif>
            
    		<cfset structAppend(thumbs,{'#assetID#':{"name":title, "thumb":thumbImage, "contentID":theContentID, "viewed":viewed}})>
       		
            <cfset viewed = 0>
            
		</cfloop>
        
  <cfset cols = 6>
  <cfset theTable = ''>
  <cfset space = 100 / cols>
        <cfset cnt = 0>
    
    <cfset theTable = theTable& '
	
      <div id="D-#cartID#">
    <table width="100%" border="0" cellspacing="5">
       	<tr>
        '>    
      <cfloop collection="#thumbs#" item="theThumb">
      
      	<cfset theAsset = thumbs[theThumb]>
   
        <cfif cnt GTE cols>
       	  <cfset cnt = 0>
          <cfset theTable = theTable& '
          </tr>
          <tr>
		  '>
        </cfif>
        
        <cfif theAsset.viewed iS 0>
        	<cfset border = 1>
            <cfset color = "666">
        <cfelse>
        	<cfset border = 2>
            <cfset color = "C00">
        </cfif>
        
          <cfset theTable = theTable& '
        <cfoutput>
          <td align="center" valign="top" width="#space#%"><img border="#border#" name="#theAsset.name#" src="#theAsset.thumb#" alt="#theAsset.name#" height="80" style="max-height: 100%; width: auto; border-color: ###color#"><div style="padding-top:4px;font-family: Tahoma, Geneva, sans-serif;font-size: 11px;color: ##333;height:18px;overflow:hidden">#theAsset.name#</div></td>
        </cfoutput>
          '>
       	<cfset cnt++>
            
      </cfloop>
      
      <cfif cnt NEQ cols>
      	   <cfset dif = cols - cnt>
          <cfloop index="z" from="1" to="#dif#">
          	<cfset theTable = theTable& '<td width="#space#%"></td>'>
          </cfloop>
      </cfif>
          
          <cfset theTable = theTable& '
      </tr>
    </table>
     </div>   
       '>  
    
     <cfreturn {'cartID':#cartID#, 'data':'#theTable#'}>
        
    </cffunction>  
    
    
    
    
    
    
    <!--- Display Social Media Cart --->
    <cffunction name="displaySocialCart" output="yes" access="remote" returntype="void" description="Creates a Page from a URL Cart">
        <cfargument name="link" type="string" required="yes" default="">    
        
        <!--- Get CartID from Link --->
        <cfinvoke component="CFC.carts" method="getCartIDFromTinyUrl" returnvariable="cartID">
            <cfinvokeargument name="link" value="#link#"/>
        </cfinvoke>
        
        <cfinvoke component="CFC.email" method="createCartPage" returnvariable="htmlPage">
            <cfinvokeargument name="cartID" value="#cartID#"/>
        </cfinvoke>
        
        <div style="width: 70%; margin: 20px auto; position: relative">
         <cfoutput>#htmlPage#</cfoutput>
         </div>
    
    </cffunction>





<!--- Send All Carts Unsent --->
    <cffunction name="sendUnsentCarts" output="no" access="remote" returntype="numeric" description="Sends All UnSent Carts">
        <cfargument name="appID" type="numeric" required="yes" default="">   
		
            <cfquery name="cartsToSend">
                SELECT  cart_id
                FROM	SessionCart
                WHERE	app_id = #appID# AND sent IS NULL
            </cfquery>
        	
            <cfif cartsToSend.recordCount GT 0>
            <!--- Send Carts --->
            <cfoutput query="cartsToSend">
            	
                <cfinvoke component="CFC.EMail" method="sendEmail" returnvariable="sentEmail">
                    <cfinvokeargument name="cartID" value="#cart_id#"/>
                </cfinvoke>
                
			</cfoutput>
            
            </cfif>
            
            <cfreturn cartsToSend.recordCount>
            
	</cffunction>
    
    
    
 <!--- Update Unknown Locations --->
    <cffunction name="cleanUpUnknownLocations" output="no" access="remote" returntype="boolean" description="Updates all 0 locations">

            <cfinvoke component="CFC.Misc" method="updateAllUnknowLocations">
            </cfinvoke>
            
            <cfreturn true>
            
	</cffunction>   
    
    


 <!--- Update Unknown Locations --->
    <cffunction name="cleanUpUnknownCarts" output="no" access="remote" returntype="numeric" description="Deletes non verified emails">
		<cfargument name="appID" type="numeric" required="no" default="0"> 
        
        <cfquery name="emptyCarts"> 
            SELECT COUNT(cart_id) AS deletedCarts
            FROM   sessionCart
            WHERE  email = ''
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
        </cfquery>
        
        <cfset deletedCarts = emptyCarts.deletedCarts>
        
        <cfquery name="emptyCarts"> 
            DELETE FROM   sessionCart
            WHERE         email = ''
            <cfif appID GT 0>
            AND app_id = #appID#
            </cfif>
        </cfquery>
          
        <cfreturn deletedCarts>
            
	</cffunction>     
    
    
    
    
     <!--- Update Unknown Locations --->
    <cffunction name="getAllObjectModels" access="remote" returntype="string" returnformat="plain" output="yes" hint="Returns all Object Types">

            <cfinvoke component="CFC.ObjectModels" method="getAllObjectModels" returnvariable="data" />
			<cfset allModels = structNew()>
          
            <cfloop collection="#data#" item="aModel">
            	
                <cfset typeID = data[aModel]>
         
                <cfinvoke component="CFC.ObjectModels" method="getObjectModel" returnvariable="aModelType">
                	<cfinvokeargument name="typeID" value="#typeID#"/>
                </cfinvoke>
                
                <cfloop collection="#aModelType#" item="theKey"></cfloop>
                <cfif StructKeyExists(aModelType,theKey)>
					<cfset theModel = aModelType[theKey]>
					<cfset structAppend(theModel,{"assets":[],"actions":[]})>

					<cfset structAppend(theModel,{"typeName":theKey})>

					<cfset structAppend(allModels,{#typeID#:theModel})>
				</cfif>
           
            </cfloop>
			
       	<cfheader name="Access-Control-Allow-Origin" value="*">
        	<cfset JSON = serializeJSON(allModels)>
         
            <cfreturn JSON>
            
	</cffunction>  
    
    
    
     <!--- Update Unknown Locations --->
    <cffunction name="getObjectModel" access="remote" returntype="string" returnformat="plain" output="yes" hint="Returns a Object Structure">
			<cfargument name="typeID" type="numeric" required="no" default="0"> 
            <cfargument name="typeName" type="string" required="no" default=""> 
			
            <!--- Get Model Struct --->
            <cfinvoke component="CFC.ObjectModels" method="getObjectModel" returnvariable="data">
            	<cfinvokeargument name="typeID" value="#typeID#"/>
                <cfinvokeargument name="typeName" value="#typeName#"/>
            </cfinvoke>
        	
            <cfif structIsEmpty(data)>
            	<!--- No Data --->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1013"/>
                </cfinvoke>
                
            	<cfset structAppend(data,{"error":#error#})> 
            
            <cfelse>

            <cfloop collection="#data#" item="theKey"></cfloop>
            <cfset data = data[theKey]>
            <cfset structAppend(data,{"assets":[],"actions":[]})>
            
            </cfif>
            
            <cfheader name="Access-Control-Allow-Origin" value="*">
        	<cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
	</cffunction>  
    
    
 
      <!--- Update Unknown Locations --->
    <cffunction name="getMarkers" access="remote" returntype="string" returnformat="plain" output="yes" hint="Returns a Object Structure">
			<cfargument name="auth_token" type="string" required="yes" default=""> 
 
            <cfset data = structNew()>

            <!--- get token info --->
            <cfinvoke  component="CFC.Tokens" method="getTokenInfo" returnvariable="tokenInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <!--- <cfdump var="#tokenInfo#"> --->
            
            <cfset appID = tokenInfo.appID>
            
            <cfif appID IS ''>
            	
                <!--- invalid token --->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1020"/>
                </cfinvoke>
                
            <cfelse>
                
            	<!--- Get Targets --->
                <cfquery name="allVisualizerTargets">
                    SELECT        Assets.asset_id, Assets.assetType_id, Assets.modified,
								  MarkerAssets.useVisualizer, MarkerAssets.url, MarkerAssets.webURL, 
                                  MarkerAssets.keySet, MarkerAssets.keyData
                    FROM          Assets INNER JOIN
                                  MarkerAssets ON Assets.asset_id = MarkerAssets.asset_id
                    WHERE        (Assets.app_id = #appID#) AND (MarkerAssets.useVisualizer = 1)
                </cfquery>
                
                <cfset data = {"targets":[]}>
                
                <!--- create data struct --->
                <cfoutput query="allVisualizerTargets">
                
					<!--- Build Asset Path --->
                    <cfinvoke component="CFC.File" method="buildCurrentFileAppPath" returnvariable="assetPath">
                        <cfinvokeargument name="assetID" value="#asset_id#"/>
                        <cfinvokeargument name="server" value="yes"/>
                    </cfinvoke>
             
                    <!--- Non Retina  Asset Path--->
                    <cfset assetPathNonRetina = assetPath & "nonretina/">
                                            
                    <cfif webURL NEQ ''>
                        <cfset theUrl = webURL>
                    <cfelse>
                        <cfset theUrl = assetPath & url>
                    </cfif>
                    
                    <cfset rec1 = {"url":theUrl, "data":keyData, "key":keySet, "modified":modified}>
                    <cfset arrayAppend(data.targets,rec1)>
				
				</cfoutput>
                 
                <!---ok--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1000"/>
                </cfinvoke>
            
            </cfif>
            
            <cfset structAppend(data,{"error":#error#})> 
            
            <cfheader name="Access-Control-Allow-Origin" value="*">
            <cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
  
    </cffunction> 
    
    
    
    
    <!--- Get Address Book --->
    <cffunction name="getAddressBook" access="remote" returntype="string" returnformat="plain" output="no" hint="Returns Sorted AddressBook">
			<cfargument name="auth_token" type="string" required="yes" default=""> 
            
            <!--- Get Address Book --->
            <cfinvoke component="CFC.AddressBook" method="getAddressBook" returnvariable="addressBook">
            	<cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>

            <!---ok--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
            
            <cfset data = {"addressbook":addressBook}>
            
    		<cfset structAppend(data,{"error":#error#})> 
            <cfheader name="Access-Control-Allow-Origin" value="*">
            <cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
            
    </cffunction>
    
    
    <!--- Update Address Book Entry --->
    <cffunction name="updateAddressBookEntry" access="remote" returntype="string" returnformat="plain" output="no" hint="Updates AddressBook Entry">
			<cfargument name="addressBookID" type="numeric" required="yes" default="0">
            <cfargument name="name" type="string" required="no" default="">
            <cfargument name="email" type="string" required="yes" default="">
            
            <!--- Update Address Book Entry --->
            <cfinvoke component="CFC.AddressBook" method="updateAddressBookEntry" returnvariable="success">
            	<cfinvokeargument name="addressBookID" value="#addressBookID#"/>
                <cfinvokeargument name="name" value="#name#"/>
                <cfinvokeargument name="email" value="#email#"/>
            </cfinvoke>
			
            <cfif success>
            
				<!---ok--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1000"/>
                </cfinvoke>
            
            <cfelse>
            
            	<!---failed--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1016"/>
                </cfinvoke>
            
            </cfif>
            
            <cfset data = structNew()>
            
            <cfset structAppend(data,{"data":{"name":name,"email":email}})> 
    		<cfset structAppend(data,{"error":#error#})> 
            
            <cfheader name="Access-Control-Allow-Origin" value="*">
            <cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
            
    </cffunction>
    
    
    <!--- Remove Address Book Entry --->
    <cffunction name="removeAddressBookEntry" access="remote" returntype="string" returnformat="plain" output="no" hint="Updates AddressBook Entry">
			<cfargument name="addressBookID" type="numeric" required="yes" default="0">
            <cfargument name="auth_token" type="string" required="yes" default=""> 
            
            <!--- Update Address Book Entry --->
            <cfinvoke component="CFC.AddressBook" method="removeAddressBookEntry" returnvariable="success">
            	<cfinvokeargument name="addressBookID" value="#addressBookID#"/>
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
			
            <cfif success>
            
				<!---ok--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1000"/>
                </cfinvoke>
            
            <cfelse>
            
            	<!---failed--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1017"/>
                </cfinvoke>
            
            </cfif>
            
            <cfset data = structNew()>

    		<cfset structAppend(data,{"error":#error#})> 
            
            <cfheader name="Access-Control-Allow-Origin" value="*">
            <cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
            
    </cffunction>
    
    
    
    <!--- Add/Update Address to Address Book --->
    <cffunction name="addToAddressBook" access="remote" returntype="string" returnformat="plain" output="no" hint="Returns Sorted AddressBook">
			<cfargument name="auth_token" type="string" required="yes" default=""> 
            <cfargument name="name" type="string" required="no" default="">
            <cfargument name="email" type="string" required="yes" default="">
            
            <!--- Get Address Book --->
            <cfinvoke component="CFC.AddressBook" method="addEntryToAddressBook" returnvariable="success">
                <cfinvokeargument name="name" value="#name#"/>
                <cfinvokeargument name="email" value="#email#"/>
            	<cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfif success>
				<!---ok--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1000"/>
                </cfinvoke>
            <cfelse>
            
            	<!---failed to add to address book--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1015"/>
                </cfinvoke>

            </cfif>
            
            <cfset data = structNew()>
            
    		<cfset structAppend(data,{"error":#error#})> 
            
            <cfheader name="Access-Control-Allow-Origin" value="*">
            <cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
            
    </cffunction>
    
    
    
   <!--- Add/Update Batch Address to Address Book --->
    <cffunction name="addBatchToAddressBook" access="remote" returntype="string" returnformat="plain" output="no" hint="Returns Sorted AddressBook">
			<cfargument name="auth_token" type="string" required="yes" default=""> 
            <cfargument name="addresses" type="string" required="no" default="">

            <!--- mikeboni@hotmail.com;wavecoders@gmail.com --->
            
            <cfset entries = structNew()>
            <cfset failedEntries = arrayNew(1)>
            <cfset successEntries = arrayNew(1)>
            
            <cfloop index="z" list="#addresses#" delimiters=";">
            	
            	<!--- Add Address --->
            	<cfinvoke component="LiveAPI" method="addToAddressBook" returnvariable="success">
                    <cfinvokeargument name="auth_token" value="#auth_token#"/>
                    <cfinvokeargument name="email" value="#z#"/>
                </cfinvoke>
            	
                <cfset result = deserializeJSON(success)>
                
                <cfif result.error.error_code IS 1000>
                	<cfset arrayAppend(successEntries,z)>
                <cfelse>
                	<cfset arrayAppend(failedEntries,z)>
                </cfif>
            	
            </cfloop>
            
            <cfset entries = {"success":successEntries, "failed":failedEntries}>
            
            <cfset data = structNew()>
            
            <!---ok--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
            
    		<cfset structAppend(data,{"error":#error#})> 
            <cfset structAppend(data,{"entries":#entries#})> 
            
            <cfheader name="Access-Control-Allow-Origin" value="*">
            <cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
    </cffunction>
    
    
    
        <!--- Import Address Book CSV or VCARD --->
    <cffunction name="importAddressBook" access="remote" returntype="string" returnformat="plain" output="no" hint="Returns Sorted AddressBook">
			<cfargument name="auth_token" type="string" required="yes" default=""> 
            <cfargument name="csvFile" type="string" required="no" default="">
            
            <cfset data = structNew()>
            
            <cfif csvFile NEQ ''>
            
                <cfinvoke component="CFC.AddressBook" method="importAddressBook" returnvariable="success">
                    <cfinvokeargument name="auth_token" value="#auth_token#"/>
                    <cfinvokeargument name="csvFile" value="#csvFile#"/>
                </cfinvoke>
                        
                <cfset structAppend(data,{"imported":#success#})> 
                
                <!---ok--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1000"/>
                </cfinvoke>
            
            <cfelse>
            	
                <!---no addressbook data --->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1013"/>
                </cfinvoke>
                
            </cfif>
            
    		<cfset structAppend(data,{"error":#error#})> 
            
            <cfheader name="Access-Control-Allow-Origin" value="*">
            <cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
            
            
    </cffunction>
    
    
    
    


        <!--- Import Addresses from Carts --->
    <cffunction name="getAddressesFromCarts" access="remote" returntype="struct" output="no" hint="Returns Imported Addresses from Carts">
			<cfargument name="auth_token" type="string" required="yes" default=""> 
            
            <!---no addressbook data --->
            <cfinvoke  component="CFC.AddressBook" method="getAddressesFromCarts" returnvariable="importedAddresses">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfreturn importedAddresses>
            
    </cffunction>    
    
    
    
    
    
    
  <!--- Resend Registration Info --->
    <cffunction name="resendUserRegistrationInfo" access="remote" returntype="boolean" output="no" hint="Send Reg Info to user through email. Returns true if success">
          <cfargument name="clientID" type="numeric" required="no" default="0">
          <cfargument name="email" type="string" required="no" default="">
          
          <cfargument name="auth_token" type="string" required="no" default="">  
            
		<!--- Get User, CLientID and AppID from Token --->
        <cfif auth_token NEQ ''>
        	
            <!--- Get UserInfo from AuthToken --->
            <cfinvoke component="CFC.Users" method="getUserProfileIDs" returnvariable="userIDs">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
			<cfset email = userInfo.email>
            <cfset clientID = userInfo.userIDs>
        
        </cfif>
        
        <cfif clientID GT 0 AND email NEQ ''>     
            
            <cfinvoke component="CFC.Users" method="resendUserRegistrationInfo" returnvariable="success">
                <cfinvokeargument name="clientID" value="#clientID#"/>
                <cfinvokeargument name="email" value="#email#"/>
            </cfinvoke>
              
            <cfreturn success>
                
       </cfif>
        
        <cfreturn false>
        
    </cffunction>
    
    
    
    
    
    <!--- http://localhost:8501/liveplatform-net/API/v10/liveAPI.cfc?method=forgotPassword&auth_token=72FF67ED-E862-7F42-BDA66FFDA3918904&email=mikeboni@hotmail.com --->
    <!--- ForgotPassword Resend --->
    <cffunction name="forgotPassword" access="remote" returntype="string" returnformat="plain" output="no" hint="Send Reg Info to user through email.">
        
          <cfargument name="auth_token" type="string" required="yes" default="">  
          <cfargument name="email" type="string" required="no" default="">  
         
        <cfset data = structNew()>
      
		<!---OK--->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="okError">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>
        
        <!--- Resend based on token --->
        <cfif auth_token NEQ ''>
        
            <!--- Token Info --->
            <cfinvoke component="CFC.Tokens" method="tokenValid" returnvariable="validToken">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
        
            
            <cfif validToken>
    
                <cfinvoke component="CFC.Tokens" method="getToken" returnvariable="tokenInfo">
                    <cfinvokeargument name="auth_token" value="#auth_token#"/>
                </cfinvoke>
                
                <cfset userID = tokenInfo.userID>
                <cfset bundleID = tokenInfo.bundleID>

				<!--- Get Token Info --->
                <cfinvoke  component="CFC.Users" method="getUserProfileIDs" returnvariable="userDetails">
                  <cfinvokeargument name="auth_token" value="#auth_token#"/>
                </cfinvoke>
                
                <cfset clientID = userDetails.clientID>
                <cfset appID = userDetails.appID>
            
            <cfelse>
            
            	<!---Token Not Vaild--->
                <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1003">
                </cfinvoke>
            	
                <cfset structAppend(data,{"error":#error#})> 
                
            </cfif>
            
        </cfif>
        
        <!--- Check if no email, if so then find email --->
        <cfif email IS ''>
	
        	<cfinvoke component="CFC.Users" method="getUserProfileIDs" returnvariable="userInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
        	
            <cfset clientID = userInfo.clientID>
            <cfset appID = userInfo.appID>
            
            <!--- Get User Email --->
            <cfinvoke component="CFC.Users" method="getUserEmail" returnvariable="email">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>

        </cfif>
        
 
        <cfif clientID GT 0 AND email NEQ ''>     
            
            <cfinvoke  component="CFC.Apps" method="getAppName" returnvariable="appName">
                  <cfinvokeargument name="appID" value="#appID#"/>
                </cfinvoke>
                
                <cfinvoke  component="CFC.Clients" method="getClientInfo" returnvariable="info">
                  <cfinvokeargument name="clientID" value="#clientID#"/>
                </cfinvoke>
                
                <cfinvoke component="CFC.Apps" method="getPaths" returnvariable="assetPaths">
                    <cfinvokeargument name="clientID" value="#clientID#"/>
                    <cfinvokeargument name="server" value="yes"/>
                </cfinvoke>
                
                <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userInfo">
                    <cfinvokeargument name="userID" value="#userID#"/>
                </cfinvoke>
            
                <!--- Send Email to User that they are Approved --->
                <cfmail server="cudaout.media3.net"
                        username="support@wavecoders.ca"
                        from="#assetPaths.client.name# Registration <#info.support#>"
                        to="#email#"
                        subject="User Registration - #assetPaths.client.name#"
                        replyto="Support <#info.support#>"
                        type="HTML"> 
                
                        <!--- HTML RegUser --->
                        <link href="http://www.liveplatform.net/register/regStyles.css" rel="stylesheet" type="text/css">
                        
                        <cfinclude template="newRegistration.cfm">
                
                </cfmail> 
            
            <cfset structAppend(data,{"error":#okError#})> 
       
       <cfelse>
       		<!--- Error, No email --->
        	<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1015">
            </cfinvoke>
        
       		<cfset structAppend(data,{"error":#error#})>         
       </cfif>
 
 <cfheader name="Access-Control-Allow-Origin" value="*">
		<cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
        
        
    </cffunction>
    
    
    
    
    
    <!--- Send Password Registration Request --->
    <cffunction name="resendSetPassword" access="remote" returntype="boolean" output="no" hint="Send Reg Info to user through email. Returns true if success">
          <cfargument name="clientID" type="numeric" required="no" default="0">
          <cfargument name="email" type="string" required="no" default="">
          
          <cfargument name="auth_token" type="string" required="no" default="">  
            
		<!--- Get User, CLientID and AppID from Token --->
        <cfif auth_token NEQ ''>
        	
            <!--- Get UserInfo from AuthToken --->
            <cfinvoke component="CFC.Users" method="getUserProfileIDs" returnvariable="userIDs">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
			<cfset email = userInfo.email>
            <cfset clientID = userInfo.userIDs>
        
        </cfif>
        
        <cfif clientID GT 0 AND email NEQ ''>     
            
            <cfinvoke component="CFC.Users" method="resendSetPassword" returnvariable="success">
                <cfinvokeargument name="clientID" value="#clientID#"/>
                <cfinvokeargument name="email" value="#email#"/>
            </cfinvoke>
              
            <cfreturn success>
                
       </cfif>
        
        <cfreturn false>
        
    </cffunction>
    
    
    <!--- Get Room Materials --->
    <cffunction name="getMaterials" access="remote" returntype="string" returnformat="plain" output="no" hint="Get All Room Materials">
        <cfargument name="bundleID" type="string" required="no" default="">
        <cfargument name="groupID" type="numeric" required="no" default="0">


		<!---OK--->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="errorOK">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>

        <!--- Get AppID from BundleID --->
        <cfinvoke component="CFC.Apps" method="getAppID" returnvariable="appInfo">
            <cfinvokeargument name="bundleID" value="#bundleID#">
        </cfinvoke>
      
        <cfif appInfo.error.error_code NEQ 1000>       
     
            <cfset JSON = serializeJSON(appInfo)>
            
            <cfreturn JSON>
  
        </cfif>
  
        <cfset appID = appInfo.app_id>
        
        
        <!--- if groupID provided, get spaces for model --->
        <cfif groupID GT 0>
        
			<!--- get all rooms for this model space--->
            <cfinvoke component="CFC.Configurator" method="getGroupAssets" returnvariable="theAssets">
                <cfinvokeargument name="groupID" value="#groupID#"/>
            </cfinvoke>
            
            <!--- convert to array --->
            <cfinvoke component="CFC.Misc" method="QueryToArray" returnvariable="theSpaces">
                <cfinvokeargument name="theQuery" value="#theAssets#"/>
                <cfinvokeargument name="theColumName" value="asset_Id"/>
            </cfinvoke>
        
        <cfelse>
        
        	<!--- Get All Spaces --->
            <cfinvoke component="CFC.Configurator" method="getAllSpaces" returnvariable="allSpaces">
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
            
            <cfinvoke component="CFC.Misc" method="QueryToArray" returnvariable="theSpaces">
                <cfinvokeargument name="theQuery" value="#allSpaces#"/>
                <cfinvokeargument name="theColumName" value="asset_Id"/>
            </cfinvoke>
            
        </cfif>
        
        
        <cfset theDefaults = structNew()>
        
        <cfset spaces = structNew()>
     	
        <cfloop index="aSpace" array="#theSpaces#">
         
        <cfif groupID GT 0>
    
         	<!--- NEW --->
            <cfinvoke component="CFC.Configurator" method="getAllMaterials" returnvariable="data">
                <cfinvokeargument name="modelID" value="#groupID#"/><!--- model --->
                <cfinvokeargument name="assetID" value="#aSpace#"/><!--- space --->
                <cfinvokeargument name="excludeDefault" value="TRUE"/>
            </cfinvoke>

         <cfelse>
         
         	<!--- OLD --->
            <cfinvoke component="CFC.Configurator" method="getAllMaterials" returnvariable="data">
                <cfinvokeargument name="assetID" value="#aSpace#"/>
                <cfinvokeargument name="excludeDefault" value="TRUE"/>
            </cfinvoke>
         
         </cfif>
           
            <cfif structCount(data) GT 0>
                <cfset structAppend(spaces,{#aSpace#:#data#})>
            </cfif>
  
            <!--- Get Defaults --->
            <cfinvoke component="CFC.Configurator" method="getAllDefaultOptions" returnvariable="allDefaults">
                <cfinvokeargument name="appID" value="#appID#"/>
                <cfinvokeargument name="spaceID" value="#aSpace#"/>
            </cfinvoke>
  
            <cfset structAppend(theDefaults,allDefaults)>
                                                                                                                              
        </cfloop>
     
        <cfset materialStruct = structNew()>
  
        <cfset structAppend(materialStruct,{"materials":spaces})>
        <cfset structAppend(materialStruct,{"defaults":theDefaults})>
        
        <cfset structAppend(materialStruct,{"error":#errorOK#})> 
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(materialStruct)>
        
        <cfreturn JSON>

    
    </cffunction>
    
    
    <!--- Search --->
    <cffunction name="searchUnits" access="remote" returntype="string" returnformat="plain" output="no" hint="Get All Room Materials">        
        <cfargument name="projectID" type="numeric" required="no" default="0">
        <cfargument name="search" type="string" required="no">
        <cfargument name="columns" type="string" required="no" default="unitName">
        <cfargument name="exactMatch" type="boolean" required="no" default="false">
        <cfargument name="maxReturns" type="numeric" required="no" default="0">

        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>
		
        <cfset results = structNew()>
    	
        <cfif projectID GT 0>
        
			<!--- Search --->
            <cfinvoke component="CFC.Units" method="searchUnit" returnvariable="searchResults">
                <cfinvokeargument name="projectID" value="#projectID#"/>
                <cfinvokeargument name="search" value="#search#"/>
                <cfinvokeargument name="columns" value="#columns#"/>
                <cfinvokeargument name="exactMatch" value="#exactMatch#"/>
                <cfinvokeargument name="maxReturns" value="#maxReturns#"/>
            </cfinvoke>
			
            <!--- check if any result - otherwise return no data error --->
            <cfif arrayLen(searchResults) GT 0>
            	<cfset structAppend(results,{"results":#searchResults#})> 
			<cfelse>
            
				<!--- no data 1013--->
                <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1013">
                </cfinvoke>
                
            </cfif>
            
        <cfelse>
        
        	<!--- project not specified 1023 --->
            <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1023">
            </cfinvoke>
        
        </cfif>
        
        <cfset structAppend(results,{"error":#error#})> 
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(results)>
            
        <cfreturn JSON>
    
    </cffunction>  
    
    
    
    
    
    <!--- Get Unit Info --->
    <cffunction name="getUnit" access="remote" returntype="string" returnformat="plain" output="no" hint="Get All Room Materials">
        <cfargument name="unitID" type="numeric" required="yes">
		<cfargument name="auth_token" type="string" required="no" default="">
        
        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="errorOK">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>      
        
        <cfset results = structNew()>

		<!--- get Unit Info --->
        <cfinvoke component="CFC.Units" method="getUnit" returnvariable="data">
            <cfinvokeargument name="unitID" value="#unitID#"/>
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>

        <cfif NOT structIsEmpty(data)>

            <cfset structAppend(results,{"info":#data#})> 
            <cfset structAppend(results,{"error":#errorOK#})> 
        
        <cfelse>
        
            <!--- no data --->
            <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1013">
            </cfinvoke>
            
            <cfset structAppend(results,{"error":#error#})> 
            
        </cfif>
        
        <cfif results.error.error_code IS 1000>
        
			<!--- check if state is 5, if so, change error 1021 --->
            <cfif results.info.state IS 5>
                
                <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1021">
                </cfinvoke>
                
                <cfset results.error = error>
                
            </cfif>
        
        </cfif>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
		<cfset JSON = serializeJSON(results)>
        
        <cfreturn JSON>
    	
        
	</cffunction>
    
    <!--- Get Unit Assignments --->
    <cffunction name="getUnitAssignments" access="remote" returntype="string" returnformat="plain" output="no" 
    hint="Gets All Room Assignments based on User the unit has been assigned to. If you add the assetID (which represents the model) then it returns only the model that matches">
        <cfargument name="projectID" type="numeric" required="no" default="0">
        <cfargument name="auth_token" type="string" required="yes">
        <cfargument name="assetID" type="numeric" required="no" default="0"><!--- returns only this model --->
       
        <cfset results = structNew()>
        
        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>
        
        <cfif projectID GT 0>

            <!--- get user from auth token --->
            <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userInfo">
                <cfinvokeargument name="auth_token" value="#auth_token#"/>
            </cfinvoke>
            
            <cfset userID = userInfo.user_id>
            
            <cfif userID IS ''>
                
                <!---no user match--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1009"/>
                </cfinvoke>
                
            <cfelse>
           
                <!--- Get Assignments --->
                <cfinvoke  component="CFC.Units" method="getAssignments" returnvariable="results">
                  <cfinvokeargument name="projectID" value="#projectID#"/>
                  <cfinvokeargument name="assetID" value="#assetID#"/>
                  <cfinvokeargument name="userID" value="#userID#"/>
                 </cfinvoke> 
                
            </cfif>
        
        <cfelse>
        
        	<!--- No Project Specified --->
            <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1023">
            </cfinvoke>
        
        </cfif>
        
 		<cfset structAppend(results,{"error":#error#})>
         
         <cfheader name="Access-Control-Allow-Origin" value="*">
		<cfset JSON = serializeJSON(results)>
        
        <cfreturn JSON>
    	
        
	</cffunction>
	
    
    <!--- Set Assignment --->
    <cffunction name="setAssignment" access="remote" returntype="string" returnformat="plain" output="no" hint="Assigns a client to a unit">
    	<cfargument name="auth_token" type="string" required="yes"><!--- UserID --->
		<cfargument name="unitID" type="numeric" required="no" default="0"><!--- UnitID --->
        <cfargument name="custID" type="numeric" required="no" default="0"><!--- CustID --->
        <cfargument name="pass" type="string" required="no" default=""><!--- Cust PASS to Override --->
        
        <cfset results = structNew()>
        
        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>
        
        <!--- get user from auth token --->
        <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
     
        <cfset userID = userInfo.user_id>
        <cfset userPass = userInfo.password>
        
        <cfif userID IS ''>
            
            <!---no user match--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1009"/>
            </cfinvoke>
            
        <cfelse>
       		
            <!--- check user AccessLevel IS 4 --->
            
            <!--- accessLevel --->
            <cfinvoke component="CFC.Users" method="getUserAccess" returnvariable="userAccess">
                <cfinvokeargument name="userID" value="#userID#"/>
            </cfinvoke>
            
            <cfif userAccess.accessLevel IS 4>
            
				<!--- override --->
                <cfif Compare(userPass,pass) EQ 0>
                    <cfset override = true>
                <cfelse>
                    <cfset override = false>
                </cfif>
            	
                <!--- setAssignment --->
                <cfinvoke component="CFC.Units" method="setAssignment" returnvariable="assigned">
                    <cfinvokeargument name="unitID" value="#unitID#"/>
                    <cfinvokeargument name="custID" value="#custID#"/>
                    <cfinvokeargument name="userID" value="#userID#"/>
                    <cfinvokeargument name="override" value="#override#"/>
                </cfinvoke>
                
                <cfif NOT assigned AND NOT override>
                   
					<!--- 1024 Already Assigned --->
                    <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                        <cfinvokeargument name="error_code" value="1024">
                    </cfinvoke>
                
                </cfif>
                
            <cfelse>
            
				<!--- 1021 Access Denied --->
                <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1021">
                </cfinvoke>
                
            </cfif>
  
        </cfif>

        <cfset structAppend(results,{"error":#error#})>
         
         <cfheader name="Access-Control-Allow-Origin" value="*">
		<cfset JSON = serializeJSON(results)>
        
        <cfreturn JSON>
        
     </cffunction>
     
    
    
    <!--- Search Len Valid--->
    <cffunction name="validSearchLen" access="remote" returntype="string" returnformat="plain" output="no" hint="Get All Room Materials">
        <cfargument name="bundleID" type="string" required="yes">
    	
    	<!--- Get AppID from BundleID --->
        <cfinvoke component="CFC.Apps" method="getAppID" returnvariable="appInfo">
            <cfinvokeargument name="bundleID" value="#bundleID#">
        </cfinvoke>
		
        <cfset appID = appInfo.app_id>
        <cfset theSearchLen = 0>
        
        <cfif appID GT 0>
        
            <cfinvoke component="CFC.Units" method="searchRequirement" returnvariable="theSearchLen">
                <cfinvokeargument name="appID" value="#appID#"/>
            </cfinvoke>
        
        </cfif>
        
        <cfset results = {"length":#theSearchLen#}> 
    	
       <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(results)>
        
        <cfreturn JSON>
    
    </cffunction>
    
    
    
    <!--- Save Unit Config Changes --->
    <cffunction name="saveConfig" access="remote" returntype="string" returnformat="plain" output="no" hint="Save Config">
        <cfargument name="auth_token" type="string" required="yes">
        <cfargument name="custID" type="numeric" required="yes">
        <cfargument name="unitID" type="numeric" required="yes">
        <cfargument name="data" type="string" required="yes">
        
        
        <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
        
        <cfset userID = userInfo.user_id>
        
        <cfif userID IS ''>
        	
            <!---no user match--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1009"/>
            </cfinvoke>
            
        <cfelse>
        
			<!--- accessLevel --->
            <cfinvoke component="CFC.Users" method="getUserAccess" returnvariable="userAccess">
                <cfinvokeargument name="userID" value="#userID#"/>
            </cfinvoke>
           
            <cfset accessLevel = userAccess.accessLevel>
            
            <!---ok--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
            
            <cfif accessLevel IS 4>
   
                <!--- update Config --->
                <cfinvoke  component="CFC.Configurator" method="saveConfig" returnvariable="error">
                    <cfinvokeargument name="userID" value="#userID#"/>
                    <cfinvokeargument name="custID" value="#custID#"/>
                    <cfinvokeargument name="unitID" value="#unitID#"/>
                    <cfinvokeargument name="data" value="#data#"/>
                </cfinvoke>
            
            <cfelse>
            
                <!---Access Denied--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1021"/>
                </cfinvoke>
            
            </cfif>
		
        </cfif>
        
        <cfset data = structNew()>

        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
    	
    </cffunction>



    <!--- create new Customer --->
    <cffunction name="createCustomer" access="remote" returntype="string" returnformat="plain" output="no" hint="Create a Customer record and return custID">
        <cfargument name="auth_token" type="string" required="yes">
        <cfargument name="bundleID" type="string" required="yes">
        <cfargument name="clientInfo" type="string" required="yes">
        
        <!---ok--->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000"/>
        </cfinvoke>
        
        <cfset data = structNew()>
        
        <cfset clientInfo = deserializeJSON(clientInfo)>
     
        <!--- get userID --->
        <cfinvoke component="CFC.Users" method="getUserInfo" returnvariable="userInfo">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
        
        <cfset userID = userInfo.user_id>
        
        <cfif userID IS ''>
        	
            <!---no user match--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1009"/>
            </cfinvoke>
            
        <cfelse>
        
			<!--- accessLevel --->
            <cfinvoke component="CFC.Users" method="getUserAccess" returnvariable="userAccess">
                <cfinvokeargument name="userID" value="#userID#"/>
            </cfinvoke>
           
            <cfset accessLevel = userAccess.accessLevel>
            
            <!---ok--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
            
            <cfif accessLevel IS 4>
   
                <!--- create new user --->
                <!--- Get AppID from BundleID --->
                <cfinvoke component="CFC.Apps" method="getAppID" returnvariable="appInfo">
                    <cfinvokeargument name="bundleID" value="#bundleID#">
                </cfinvoke>
                
                <cfset appID = appInfo.app_id>
          
                <cfif appID GT 0>
                
                	<!--- Get ClientID from AppID --->
                    <cfinvoke component="CFC.Clients" method="getClientIDFromAppID" returnvariable="clientID">
                        <cfinvokeargument name="appID" value="#appID#">
                    </cfinvoke>
                   
                    <!--- check if user already exists --->
        
                    <cfinvoke  component="CFC.Users" method="getUserIDIfExists" returnvariable="userID">
                        <cfinvokeargument name="email" value="#trim(clientInfo.email)#"/>
                        <cfinvokeargument name="clientID" value="#clientID#"/>
                        <cfinvokeargument name="appID" value="#appID#"/>
                    </cfinvoke>

                    <cfif userID GT 0>
                    	
                        <!---user already exists--->
                        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                            <cfinvokeargument name="error_code" value="1014"/>
                        </cfinvoke>
            			
                        <cfset structAppend(data,{"custID":#userID#})>
                        
                    <cfelse>
                    
                    	<cfif NOT structKeyExists(clientInfo,'pass')>
                        	<cfset structAppend(clientInfo,{'pass':''})>
                        </cfif>
                        
						<!--- {name, email, pass} --->
                        <cfinvoke  component="CFC.Users" method="registerUser" returnvariable="user">
                            <cfinvokeargument name="name" value="#clientInfo.name#"/>
                            <cfinvokeargument name="email" value="#clientInfo.email#"/>
                            <cfinvokeargument name="password" value="#clientInfo.pass#"/>
                            
                            <cfinvokeargument name="accessLevel" value="1"/>
                            <cfinvokeargument name="clientID" value="#clientID#"/>
                            <cfinvokeargument name="appID" value="#appID#"/>
                            <cfinvokeargument name="userType" value="1"/>
                            <cfinvokeargument name="active" value="yes"/>
                        </cfinvoke>
                        
						<!--- custID --->
                        <cfset structAppend(data,{"custID":#user.userid#})>
                        
                    </cfif>
                    
                <cfelse>
                	
                    <!---no such app--->
                    <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                        <cfinvokeargument name="error_code" value="1002"/>
                    </cfinvoke>
            
                </cfif>
                
            <cfelse>
            
                <!---Access Denied--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1021"/>
                </cfinvoke>
            
            </cfif>
		
        </cfif>

        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>

  	</cffunction>
    
    
    
    
    
    <!--- get Special Content --->
    <cffunction name="getSpecialContent" access="remote" returntype="string" returnformat="plain" output="no" hint="Gets special folder content like THEME">
        <cfargument name="bundleID" type="string" required="no" default="">
        <cfargument name="groupName" type="string" required="no" default="">
        <cfargument name="array" type="numeric" required="no" default="0">
        <cfargument name="dev" type="numeric" required="no" default="0">
        <cfargument name="date" type="numeric" required="no" default="0">
        <cfargument name="rebuildSpecialContent" type="boolean" required="no" default="false">
        
        <cfset data = structNew()>
        
        <cfif bundleID IS ''>
			<!---no bundle id --->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1002"/>
            </cfinvoke>
        <cfelse>  
			<!--- Get AppID from BundleID --->
            <cfinvoke component="CFC.Apps" method="getAppID" returnvariable="appInfo">
                <cfinvokeargument name="bundleID" value="#bundleID#">
            </cfinvoke>
            
            <cfset appID = appInfo.app_id>
            
        </cfif>
           
		<cfif appID IS 0 OR groupName IS ''>
            
            <!---no data--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1013"/>
            </cfinvoke>
            
        <cfelse>
            <!---ok--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1000"/>
            </cfinvoke>
            
            <!--- GET Cached Data --->
            <!--- Get JSON Path --->
			<cfinvoke  component="CFC.Apps" method="getJSONPath" returnvariable="JSONPath">
				<cfinvokeargument name="appID" value="#appID#">
			</cfinvoke>

			<!--- if DEV then add DEV folder pto path --->
			<cfif dev IS 1>
				<cfset JSONPath = JSONPath & 'DEV/'>
			</cfif>

			<!--- build path --->          
			<cfset JSONFilePath = JSONPath & 'special_'& groupName &'.json'> 


			<!--- check if data exists --->
			<cfif fileExists(JSONFilePath)>

				<cffile action = "read" file = "#JSONFilePath#" variable = "JSONData" charset="utf-8">
				<cfset data = deserializeJson(JSONData)>

			 <cfelse>
				<cfset rebuildSpecialContent = true>
			</cfif> 
			
			<!--- REBUILD SPECIAL CONTENT --->
			<cfif rebuildSpecialContent>
				
				<cfinvoke  component="CFC.Content" method="generateSpecialContent" returnvariable="data">
					<cfinvokeargument name="bundleID" value="#bundleID#"/>
					<cfinvokeargument name="groupName" value="#groupName#"/>
					<!--- <cfinvokeargument name="array" value="1"/> --->
				</cfinvoke>
																											
				<cfset data = deserializeJson(data)>
				
				<!--- <cfif data.error.error_code IS 1000>
					<cffile action = "read" file = "#JSONFilePath#" variable = "JSONData" charset="utf-8">
					<cfset data = deserializeJson(JSONData)>
				<cfelse>
					<cfdump var="#data#"><cfabort>
				</cfif> --->
				
			</cfif>
           
        </cfif>
 
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>

  	</cffunction>
    
    
    <!--- Send Password Registration Request --->
    <cffunction name="validateAccessCode" access="remote" returntype="string" returnformat="plain" output="no" hint="validates a new access code for a user">
          <cfargument name="auth_token" type="string" required="no" default="">
          <cfargument name="groupID" type="numeric" required="no" default="0">
          <cfargument name="unlockCode" type="string" required="no" default="">
          
        <cfinvoke  component="CFC.Access" method="validateAccessCode" returnvariable="data">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
            <cfinvokeargument name="groupID" value="#groupID#"/>
            <cfinvokeargument name="unlockCode" value="#unlockCode#"/>
        </cfinvoke>  
    	
       <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
    	<cfreturn JSON>
    
    </cffunction>
    
    
    
    <!--- get User Access Codes --->
    <cffunction name="getUserAccessCodes" access="remote" returntype="string" returnformat="plain" output="no" hint="gets all access codes for a user">
          <cfargument name="auth_token" type="string" required="no" default="">
    
    
        <cfinvoke  component="CFC.Access" method="getUserAccessGroups" returnvariable="data">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke> 
		
       <cfheader name="Access-Control-Allow-Origin" value="*">	
        <cfset JSON = serializeJSON(data)>
    
    	<cfreturn JSON>
    
    </cffunction>
    
    
    <!--- get User Group IDs accessible --->
    <cffunction name="getContentByUserAccess" access="remote" returntype="string" returnformat="plain" output="no" hint="gets all access codes for a user">
          <cfargument name="auth_token" type="string" required="no" default="">
    
    
        <cfinvoke  component="CFC.Access" method="getContentByUserAccess" returnvariable="groupData">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
        </cfinvoke>
        
        <!--- TS from Server --->
        <cfinvoke component="CFC.Misc" method="convertDateToEpoch" returnvariable="curDate" />
        
        <cfset structAppend(groupData,{"systemTime":#curDate#})>
		
       <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(groupData)>
        
    	<cfreturn JSON>
    
    </cffunction>
    
    
    
    <!--- get dynamic content --->
    <cffunction name="getDynamicContent" access="remote" returntype="string" returnformat="plain" output="no" hint="gets asset that is dynamic" >
          <cfargument name="auth_token" type="string" required="no" default="">
    	  <cfargument name="date" type="numeric" required="no" default="0">
          <cfargument name="assetID" type="numeric" required="no" default="0">
          
        <cfinvoke  component="CFC.Access" method="getDynamicContent" returnvariable="assetData">
            <cfinvokeargument name="auth_token" value="#auth_token#"/>
            <cfinvokeargument name="date" value="#date#"/>
            <cfinvokeargument name="assetID" value="#assetID#"/>
        </cfinvoke>

        <cfset JSON = serializeJSON(assetData)>
   		<cfheader name="Access-Control-Allow-Origin" value="*">
    	<cfreturn JSON>
    
    </cffunction>
    
    
    
    
    
     <!--- send config email --->
    <cffunction name="sendConfig" access="remote" returntype="string" returnformat="plain" output="no" hint="send email of room configs">
          <cfargument name="unitID" type="numeric" required="no" default="0">
    
    	<cfset data = structNew()>
        
        <!---no data--->
        <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1013"/>
        </cfinvoke>
        
        
        <cfif unitID GT 0>
        	<!--- check if exists --->
            <!--- unit exists --->
        	<cfinvoke component="CFC.Units" method="unitExists" returnvariable="unitExists">
              <cfinvokeargument name="unitID" value="#unitID#"/>
            </cfinvoke>
            
            <!--- send email --->
            <cfif unitExists>
            
				<!---ok--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1000"/>
                </cfinvoke>
                
                <!--- send email template --->
            
            <cfelse>
            
				<!---unit not found--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1025"/>
                </cfinvoke>	
            
            </cfif>
		
        <cfelse>
        
			<!---unit not found--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1025"/>
            </cfinvoke>	
        
        </cfif>
		
        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
    
    </cffunction>
    
    
    
    <!--- Set Unit state --->
    <cffunction name="setUnitState" access="remote" returntype="string" returnformat="plain" output="no" hint="Set Unit State">
        <cfargument name="unitID" type="numeric" required="no" default="0">
        <cfargument name="state" type="numeric" required="no" default="-1">
        <cfargument name="pass" type="string" required="no" default="">
		
        <cfset data = structNew()>
        
        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke>      
        
        <!--- unit exists --->
        <cfinvoke component="CFC.Units" method="unitExists" returnvariable="unitExists">
          <cfinvokeargument name="unitID" value="#unitID#"/>
        </cfinvoke>
            
        <cfif unitExists>
        
            <!--- get unit info --->
            <cfinvoke component="CFC.Units" method="getUnit" returnvariable="unit">
                <cfinvokeargument name="unitID" value="#unitID#"/>
                <cfinvokeargument name="noPass" value="false"/>
            </cfinvoke>
            
            <cfset unitInfo = unit[1]>
            
            <cfset theState = unitInfo.state>
            <cfset userPass = unitInfo.user.password>
            
            <!--- check pass --->
            <cfif pass IS userPass>
            	
                <!--- upadte unit state --->
                <cfquery name="unit">
                      UPDATE unitAssets
                      SET state = #state#
                      WHERE	 unit_id = #unitID#
                </cfquery>
                
                <cfset structAppend(data,{"state":#state#})>
                
            <cfelse>
            
				<!---access denied--->
                <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                    <cfinvokeargument name="error_code" value="1021"/>
                </cfinvoke>	
            	
            </cfif>
 
        <cfelse>
            
			<!---unit not found--->
            <cfinvoke  component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1025"/>
            </cfinvoke>	
            
            <cfset structAppend(data,{"error":#error#})>
        	
        	<cfheader name="Access-Control-Allow-Origin" value="*">
			<cfset JSON = serializeJSON(data)>
            
            <cfreturn JSON>
        
        </cfif>
        
		<cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
    	
        
	</cffunction>
    
    
    
    <!--- get all unit views --->
    <cffunction name="searchForUnits" access="remote" returntype="string" returnformat="plain" output="no" hint="ge tUnits With Views">
        <cfargument name="projectID" type="numeric" required="no" default="0" hint="Project ID">
        <cfargument name="groupID" type="numeric" required="no" default="0" hint="Floor ID">
        <cfargument name="views" type="string" required="no" default="" hint="(0000 = NESW)">
        <cfargument name="typeCode" type="string" required="no" default="" hint="type of suite - 1BD, 2BD">
        <cfargument name="assetID" type="numeric" required="no" default="0" hint="assetID of unit">
        <!--- range of size unit --->
        <cfargument name="sizeMin" type="numeric" required="no" default="0" hint="Min Size of unit">
        <cfargument name="sizeMax" type="numeric" required="no" default="0" hint="Max Size of unit">
        
        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke> 
        
        <cfset data = structNew()>
        
        <cfif projectID GT 0>
        
			<!--- get views --->
            <cfinvoke component="CFC.Units" method="searchForUnits" returnvariable="unitViews">
                <cfinvokeargument name="projectID" value="#projectID#"/>
                <cfinvokeargument name="groupID" value="#groupID#"/>
                <cfinvokeargument name="views" value="#views#"/>
                <cfinvokeargument name="typeCode" value="#typeCode#"/>
                <cfinvokeargument name="assetID" value="#assetID#"/>
                <cfinvokeargument name="sizeMin" value="#sizeMin#"/>
                <cfinvokeargument name="sizeMax" value="#sizeMax#"/>
            </cfinvoke>
          
            <cfset structAppend(data,{"zones":#unitViews#})>
    	 
        <cfelse>
        
        	<!--- project id needed --->
            <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1023">
            </cfinvoke> 
            	
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
      
        <cfreturn JSON>
    
    </cffunction>
    
    
     <!--- Unit Type Codes --->
     <cffunction name="getUnitTypeCodes" access="remote" returntype="string" returnformat="plain" output="no" hint="get tUnits types codes">
        <cfargument name="projectID" type="numeric" required="no" default="0" hint="Project ID">
        
        <!--- <cfinvoke component="CFC.Units" method="getUnitTypeCodes" returnvariable="typeCodes" /> --->
        
        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke> 
        
        <!--- get root project group from projectID --->
        <cfinvoke  component="CFC.Units" method="getGroups" returnvariable="rootProject">
            <cfinvokeargument name="projectID" value="#projectID#"/>
        </cfinvoke>
         
         <cfif arrayLen(rootProject) GT 0>
                
			<cfset rootGroupID = rootProject[1].group_id>
        
			<!--- get all floors --->
            <cfinvoke  component="CFC.Units" method="getGroups" returnvariable="allGroups">
                <cfinvokeargument name="subgroupID" value="#rootGroupID#"/>
            </cfinvoke>
            
            <cfset groupIDs = arrayNew(1)>
            <!--- all groupIDs to search in --->
            <cfloop index="aGroup" array="#allGroups#">
                <cfset arrayAppend(groupIDs,aGroup.group_id)>
            </cfloop>
  
			<!--- get Types used in units for project --->
            <cfquery name="typeCodes">
                SELECT DISTINCT UnitAssets.unitType_id, UnitTypes.name, UnitTypes.code, UnitTypes.sortOrder
                FROM            UnitAssets INNER JOIN
                                        UnitTypes ON UnitAssets.unitType_id = UnitTypes.unitType_id
                WHERE        (UnitAssets.active = 1) AND (UnitAssets.state = 0)
                
                            AND UnitAssets.group_id IN(
                                <cfloop index="groupID" array="#groupIDs#">
                                     #groupID#,
                                </cfloop>
                                    0)
            </cfquery>
            
            <cfset data = structNew()>
            <cfset unitCodes = arrayNew(1)>
            
            <cfset arrayAppend(unitCodes,{'name':'All Types', 'code':'', 'order':0})>
            
            <cfoutput query="typeCodes">
            
                <cfset arrayAppend(unitCodes,{'name':name, 'code':code, 'order':sortOrder})>
            
            </cfoutput>
            
            <!--- sort ArrayOfStructs --->
            <cfinvoke  component="CFC.Misc" method="ArrayOfStructSort" returnvariable="sortedCodes">
                <cfinvokeargument name="base" value="#unitCodes#"/>
                <cfinvokeargument name="sortType" value="numeric"/>
                <cfinvokeargument name="sortOrder" value="asc"/>
                <cfinvokeargument name="pathToSubElement" value="order"/>
            </cfinvoke>
 
            <cfset structAppend(data, {"codes":sortedCodes})>
        
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
    
        <cfreturn JSON>
            
     </cffunction>
     
    
    <!--- get all unit views --->
    <cffunction name="getUnits" access="remote" returntype="string" returnformat="plain" output="no" hint="get tUnits">
        <cfargument name="projectID" type="numeric" required="no" default="0" hint="Project ID">
        <cfargument name="groupID" type="numeric" required="no" default="0" hint="Floor ID">
        
        <cfset data = structNew()>
        
        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke> 
            
        <cfif projectID GT 0>
            
            <cfinvoke component="CFC.Units" method="getGroupedUnits" returnvariable="allUnitsAndFloors">
                <cfinvokeargument name="projectID" value="#projectID#"/>
                <cfinvokeargument name="groupID" value="#groupID#"/>
            </cfinvoke>
            
            <cfset structAppend(data,{"units":#allUnitsAndFloors#})>
		
        <cfelse>
        	
            <!--- project id needed --->
            <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
                <cfinvokeargument name="error_code" value="1023">
            </cfinvoke> 
            
        </cfif>
        
        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
    
    </cffunction>
    
    
    
    
    <!--- get all Zones --->
    <cffunction name="getZones" access="remote" returntype="string" returnformat="plain" output="no" hint="get all groups">
        <cfargument name="projectID" type="numeric" required="no" default="0" hint="Project ID">
    	
        
        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke> 
        
        <cfset data = structNew()>
    	
		 <!--- get root project group from projectID --->
            <cfinvoke  component="CFC.Units" method="getGroups" returnvariable="rootProject">
                <cfinvokeargument name="projectID" value="#projectID#"/>
            </cfinvoke>
            
            <cfif arrayLen(rootProject) GT 0>
            
                <cfset rootGroupID = rootProject[1].group_id>
            
                <!--- get all floors --->
                <cfinvoke  component="CFC.Units" method="getGroups" returnvariable="allGroups">
                    <cfinvokeargument name="subgroupID" value="#rootGroupID#"/>
                </cfinvoke>
                
           </cfif>
       
		   <cfset groupInfo = arrayNew(1)>
               
           <cfloop index="aGroup" array="#allGroups#">
           
                <cfset arrayAppend(groupInfo,{'groupID':aGroup.group_id, 'name':aGroup.name, 'code':listGetAt(aGroup.name,2,' ')})>
           
           </cfloop>

		<cfset structAppend(data, {"zones":groupInfo})>
        <cfset structAppend(data,{"error":#error#})>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
        <cfset JSON = serializeJSON(data)>
        
        <cfreturn JSON>
    
    
    </cffunction>
    
    
    <!--- get RSVP --->
    <!--- LiveAPI.cfc?method=confirmRSVP&id=12 --->
    <cffunction name="confirmRSVP" access="remote" returntype="string" returnformat="plain" output="no" hint="confirm rsvp">
        <cfargument name="rsvpID" type="numeric" required="no" default="0" hint="User ID">
        
        <cfset data = structNew()>
        
        <cfif rsvpID GT 0>
        
        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke> 
        
        <cfquery name="updateRSVP">
		  UPDATE RSVP
		  SET confirm_rsvp = 1
		  WHERE	 rsvp_id = #rsvpID#
	    </cfquery>
       
       <!--- open thanks page --->
       <cfquery name="userInfo">
			SELECT        Applications.client_id, RSVP.app_id
			FROM          RSVP LEFT OUTER JOIN
					Applications ON RSVP.app_id = Applications.app_id LEFT OUTER JOIN
					Clients ON Applications.client_id = Clients.client_id
			WHERE         rsvp_id = #rsvpID#
		 </cfquery>
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
    	
    	<cfelse>
    		<!--- return error --->
    		<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
				<cfinvokeargument name="error_code" value="4000">
			</cfinvoke> 
		</cfif>
  		
  		<cfset appID = userInfo.app_id>
  		<cfset clientID = userInfo.client_id>
   		
   		<!--- Get Paths --->
		<cfinvoke component="CFC.Apps" method="getPaths" returnvariable="assetPaths">
			<cfinvokeargument name="clientID" value="#clientID#"/>
			<cfinvokeargument name="appID" value="#appID#"/>
			<cfinvokeargument name="server" value="yes"/>
		</cfinvoke>

		<cfset path = assetPaths.application.path>
            
   		<cfset clientFolder = "#listGetAt(path,listLen(path,'/')-1,'/')#">
   		<cfset appFolder = "#listGetAt(path,listLen(path,'/'),'/')#">
   		<cfset rsvpFolder = "../../#clientFolder#/#appFolder#/rsvp/">
   		<cfset rsvpInviteThanks = "#rsvpFolder#thanks.html">
   		
        <cflocation url="#rsvpInviteThanks#">
    	
    	<cfset structAppend(data,{"error":#error#})>
    	<cfset JSON = serializeJSON(data)>
    	<cfreturn JSON>
    	
    </cffunction>
    
    <!--- Send Email --->
    <cffunction name="sendRSVP" access="remote" returntype="string" returnformat="plain" output="no" hint="confirm rsvp">
        <cfargument name="rsvpID" type="numeric" required="no" default="0" hint="User ID">
        
        <cfset data = structNew()>
        
        <cfif rsvpID GT 0>
        
        <!--- ok --->
        <cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
            <cfinvokeargument name="error_code" value="1000">
        </cfinvoke> 

       <!--- email page --->
       <cfinvoke component="CFC.email" method="sendInviteEmail" returnvariable="error">
            <cfinvokeargument name="inviteID" value="#rsvpID#">
        </cfinvoke> 
        
        <cfheader name="Access-Control-Allow-Origin" value="*">
    	
    	<cfelse>
    		<!--- return error --->
    		<cfinvoke component="CFC.Errors" method="getError" returnvariable="error">
				<cfinvokeargument name="error_code" value="4000">
			</cfinvoke> 
		</cfif>
    	
    	<cfset structAppend(data,{"error":#error#})>
    	<cfset JSON = serializeJSON(data)>
    	<cfreturn JSON>
    	
    </cffunction>
    
    
  <!--- Get Content Asset Library --->
   <cffunction name="getProjectAssets" access="remote" returntype="string" returnformat="plain" output="no" hint="project assets">
       <cfargument name="auth_token" type="string" required="yes" default="">
       <cfargument name="byAssetID" type="string" required="no" default="true">
   		
   		<!--- get appID from Token --->
		<cfinvoke component="CFC.Tokens" method="getToken" returnvariable="tokenInfo">
			<cfinvokeargument name="auth_token" value="#auth_token#"/>
		</cfinvoke>

		<cfset appID = tokenInfo.appID>
   
   		<!--- Get JSON Path --->
        <cfinvoke component="CFC.Apps" method="getJSONPath" returnvariable="JSONPath">
        	<cfinvokeargument name="appID" value="#appID#">
        </cfinvoke>

		<cfset assetPath = JSONPath & "contentAssets.json">

		<!--- Normal Checks --->
		<cfif fileExists(assetPath)>
			<!--- Read JSON File --->
			<cffile action = "read" file = "#assetPath#" variable = "JSON" charset="utf-8">

		<cfelse>

			<!--- Get JSON from Server --->
			<cfinvoke component="CFC.Content" method="generateProjectAssets" returnvariable="data">
				<cfinvokeargument name="auth_token" value="#auth_token#">
				<cfinvokeargument name="byAssetID" value="#byAssetID#">
			</cfinvoke>

			<cfset JSON = serializeJSON(data)>
			<cffile action="write" file="#assetPath#" output="#JSON#" charset="utf-8"> 

		</cfif>
  	
  		<cfreturn JSON>

    </cffunction>

</cfcomponent>






