<cfset clientID = SESSION.clientID>
<cfset appID = SESSION.appID>

<cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="curDate" />
<cfset theReportDate = dateFormat(curDate,'MMM-DD-YYYY')>

<cfinvoke component="CFC.Apps" method="getAppName" returnvariable="appName">
    <cfinvokeargument name="appID" value="#appID#"/>
</cfinvoke>

<cfinvoke component="CFC.Clients" method="getClientInfo" returnvariable="clientName">
    <cfinvokeargument name="clientID" value="#clientID#"/>
</cfinvoke>

<cfset CompanyName = clientName.company>
    

<cfif clientID GT 0 AND appID GT 0>

	<cfset filename = '#CompanyName#-#appName#-#theReportDate#.csv'>
    
    <cfcontent type="text/plain"> 
    <cfheader name="Content-Disposition" value="attachment; filename=#filename#">
    <cfsetting showdebugoutput="no">
    
    <!--- Get Group Assets --->
    <cfinvoke component="CFC.Users" method="getAllUsers" returnvariable="users">
        <cfinvokeargument name="clientID" value="#clientID#"/>
        <cfinvokeargument name="appID" value="#appID#"/>
        <cfinvokeargument name="active" value="1"/>
    </cfinvoke>
    
    <cfset br = "#chr(13)##chr(10)#">
    
    Name,Company,Email, Password, Last Session Date, Sessions
    
    <cfoutput query="users">
    
        <cfinvoke  component="CFC.Users" method="getLastUserSession" returnvariable="lastSession">
           <cfinvokeargument name="userID" value="#user_id#"/>
           <cfinvokeargument name="returnEpoch" value="yes" />
        </cfinvoke>
        
        <cfinvoke  component="CFC.Users" method="getUsersTotalSessions" returnvariable="sessionCount">
           <cfinvokeargument name="userID" value="#user_id#"/>
           <cfinvokeargument name="appID" value="#session.appID#"/>
        </cfinvoke> 
        
        <cfif Name IS ''>
            <cfset theName = 'No Name'>
        <cfelse>
            <cfset theName = Name>
        </cfif>
        
        <cfif Company IS ''>
            <cfset theCompany = 'No Company'>
        <cfelse>
            <cfset theCompany = Company>
        </cfif>
        
        <cfif Email IS ''>
            <cfset theEmail = 'No EMail'>
        <cfelse>
            <cfset theEmail = Email>
        </cfif>
        
        <cfif Password IS ''>
            <cfset thePass = 'No Password'>
        <cfelse>
            <cfset thePass = Password>
        </cfif>
        
        <cfif lastSession IS ''>
            <cfset theDate = "Not Active">
        <cfelse>
        
            <!--- convert epoch --->
             <cfinvoke component="CFC.Misc" method="convertEpochToDate" returnvariable="lastSession">
                <cfinvokeargument name="TheEpoch" value="#lastSession#">
            </cfinvoke>
            
            <cfset theDate = dateFormat(lastSession,'MMM. DD, YYYY')>
            
        </cfif>
        
        #theName#, #theCompany#, #theEmail#, #thePass#, #theDate#, #sessionCount# #br#

    </cfoutput>

</cfif>

