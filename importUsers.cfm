<cfparam name="csvFile" default="">
<cfparam name="accessLevel" default="0">

<cfparam name="appID" default="0">
<cfset curPage = "#getFileFromPath( cgi.script_name )#">

<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
</head>

<body>

<cfif csvFile NEQ ''>

<!--- Import --->

<cfinvoke component="CFC.Users" method="importAccounts" returnvariable="success">
    <cfinvokeargument name="appID" value="#appID#"/>
    <cfinvokeargument name="csvFile" value="#csvFile#"/>
    <cfinvokeargument name="accessLevel" value="#accessLevel#"/>
    <cfinvokeargument name="active" value="yes"/>
</cfinvoke>

<cfelse>

<!--- Display --->

<cfinvoke component="CFC.Apps" method="getClientApps" returnvariable="clientApps">
    <cfinvokeargument name="appID" value="#appID#"/>
</cfinvoke>


<form action="<cfoutput>#curPage#</cfoutput>" method="post" enctype="multipart/form-data" name="importAddressBook" id="importAddressBook" onSubmit="updateStateImport();">
<div id="importInput" style="display:block">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="middle" colspan="2">
    Import Accounts To:
    <select name="appID" class="formfieldcontent" id="appID">
      <cfoutput query="clientApps">
      	<option value="#app_id#">#appName#</option>
      </cfoutput>
    </select>
    <select name="accessLevel" class="formfieldcontent" id="accessLevel">
      	<option value="0">Guest</option>
        <option value="1">Level 1</option>
        <option value="2">Level 2</option>
        <option value="3">Level 3</option>
        <option value="4">Admin</option>
    </select>
    </td>
    </tr>
  <tr>
    <td width="51%" align="left" valign="middle"><input name="csvFile" type="file" class="formfieldcontent" id="csvFile" style="width:400px; padding-left:0px" size="60" maxlength="128" /></td>
    <td width="49%" align="left" valign="middle"><input name="Submit" type="submit" class="formfieldcontent" value="Import Address Book" style="height:34px; margin-left:5px"></td>
  </tr>
</table>
</div>
</form> 

<!--- <cflocation url="usersView.cfm"> --->

</cfif>


</body>
</html>