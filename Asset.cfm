<cfparam name="error" default="">
<cfparam name="assetID" default="0">
<cfparam name="appID" default="">
<cfparam name="assetTypeTab" default="3">
<cfparam name="assetTypeID" default="0">
<cfparam name="importAssets" default="false">

<cfif NOT IsDefined("session.appID")><cfset session.appID = '0'></cfif>
<cfif appID NEQ ''><cfset session.appID = appID></cfif>

<cfif NOT IsDefined("session.clientID")><cflocation url="ClentsView.cfm" addtoken="no"></cfif>

<cfinvoke  component="CFC.Users" method="getAccessLevel" returnvariable="accessLevels" />


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Live-Assets</title>
<link href="styles.css" rel="stylesheet" type="text/css" />

<link href="JS/mcColorPicker/mcColorPicker.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="JS/mcColorPicker/mcColorPicker.js"></script>
<script type="text/javascript" src="JS/classSelector.js"></script>





<!--- Check File Extension --->
<script type="text/javascript">

function hasExtension(inputID, exts) {
    var fileName = document.getElementById(inputID).value;
    return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
}

function checkFileExtention(theObj,theExt)
{
	allFileTypes = theExt.split(",");
	
	if (!hasExtension(theObj, allFileTypes) ) 
	{
	   alert('This File Extention does NOT match the requested Extentions - '+theExt);
	   document.getElementById(theObj).value = '';
		
	}

}
</script>

</head>

<body>

<cfinclude template="Assets/header.cfm">

<cfset clientAppPath = assetPaths.application.path>
<cfset active = assetPaths.application.active>
<cfset iconPath = assetPaths.application.icon>
<cfset appName = assetPaths.application.name>
<cfset bundleID = assetPaths.application.bundleID>
<cfset assetPath = assetPaths.types[assetTypeID]>

<cfinvoke component="CFC.Assets" method="getAssets" returnvariable="assets">
    <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<div style="width:810px; background-color:##333; padding-top:10px; height:auto">
	<cfoutput>
      <div>
        <cfif active>
        	<img src="#iconPath#" width="68" height="68" style="float:left; padding-right:10px" />
        <cfelse>
        	<div style="width:68px; height:68px; float: left; background-image:url(#iconPath#); background-repeat: no-repeat; background-size:contain; background-position: 50% 50%;">
            <img src="images/inactive-app.png" width="68" height="68" style="float:left; padding-right:10px" />
            </div>
        </cfif>
        	<div style="padding-top:4px; padding-bottom:4px">
              <div class="bannerHeading">#appName#<span class="bundleid"> (#bundleID#)</span></div>
              <div class="content">Path: <span class="contentHilighted">#clientAppPath#</span></div>
            </div>
      </div>
	

    <div style="background-color:##666; margin-top:5px; height:32px; padding-left:10px; padding-top:8px; width:800px">
        <span class="sectionLink">
            <a href="AppsView.cfm?assetTypeTab=0" class="<cfif assetTypeTab IS 0>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Application Content</a> | 
            <a href="AppsView.cfm?assetTypeTab=3" class="<cfif assetTypeTab IS 3>sectionLinkHighlighted<cfelse>sectionLink</cfif>">Library of Assets</a>
        </span>
    </div>

    <div class="contentLinkGrey" style="background-color:##EEE; width:auto; float:left; margin-top:4px; width:100%">
        <div style="width:400px;float:left; padding-top:12px; padding-left:10px">Asset - #assets.name#</div>
            <div style="float:right">
            <img src="images/remove.png" style="cursor:pointer; onClick="" />
            <img src="images/saveupdate.png" style="cursor:pointer; onClick="" />
            </div>
        </div>
    </div>
	</cfoutput>
</div>




<!--- <cfdump var="#assets#"><cfabort> --->

</body>
</html>