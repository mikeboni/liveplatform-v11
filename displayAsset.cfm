<cfinvoke component="CFC.Assets" method="getAssets" returnvariable="anAsset">
    <cfinvokeargument name="assetID" value="#assetID#"/>
</cfinvoke>

<!---<cfdump var="#anAsset#"><br />--->
<!---<cfdump var="#anAsset.detail_id#"><br />--->

<cfinvoke component="CFC.file" method="buildCurrentFileAppPath" returnvariable="location">
    <cfinvokeargument name="assetTypeID" value="#anAsset.assetType_ID#"/>
    <cfinvokeargument name="clientID" value="#session.clientID#"/>
    <cfinvokeargument name="appID" value="#session.appID#"/>
</cfinvoke>

<!---<cfdump var="#location#"><br />--->

<cfinvoke component="CFC.Assets" method="getAssetDetails" returnvariable="details">
    <cfinvokeargument name="detailID" value="#anAsset.detail_id#"/>
</cfinvoke>

<!---<cfdump var="#details#"><br />--->

<cfset assetUrl = "../../" & location & details.url>
<cfset assetThumbUrl = "../../" & location & "thumbs/" & details.thumbnail>

<cfset allAssetsTypes.type = anAsset.assetType_ID>

<cfset assetName = anAsset.name>
<cfset assetURL = assetUrl>
<cfset thumbURL = assetThumbUrl>

<cfset fileImageURL = details.url>
<cfset fileThumbURL = details.thumbnail>

<cfset color = details.color>
<cfset backgroundcolor = details.backgroundcolor>
<cfset title = details.title>
<cfset subtitle = details.subtitle>
<cfset description = details.description>
<cfset other = "">

<cfset assetID = anAsset.asset_id>

<cfinclude template="../v10/imageAsset.cfm">

